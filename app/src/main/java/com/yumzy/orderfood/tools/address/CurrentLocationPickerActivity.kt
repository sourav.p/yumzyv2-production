package com.yumzy.orderfood.tools.address

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.home.SelectedAddress
import com.yumzy.orderfood.databinding.ActivityCurrentLocationPickerBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*
import javax.inject.Inject

class CurrentLocationPickerActivity :
    BaseActivity<ActivityCurrentLocationPickerBinding, LoginViewModel>(),
    OnMapReadyCallback, ILoginView {

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            for (location in locationResult.locations) {
                if (!mUpdatedCurrentLocation) {
                    setMarkerPosition(location.latitude, location.longitude)
                    mUpdatedCurrentLocation = true
                }
            }
        }
    }

    private var locationRequest: LocationRequest = LocationRequest().let {
        it.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        it.interval = (10 * 1000).toLong()
        it.fastestInterval = 2000
        return@let it
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var mPrevious: LatLng = LatLng(0.0, 0.0)
    private var mMarker: Marker? = null
    private var mCurrentLocation: SelectedAddress? = null
    private var mUpdatedCurrentLocation: Boolean = false
    private var isNew: Boolean = false


    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
    }

    
    override val vm: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_current_location_picker)
        vm.attachView(this)


        isNew = intent.getBooleanExtra(IConstants.ActivitiesFlags.NEW_USER, false)
        applyDebouchingClickListener(bd.btnMyLocation, bd.btnPicAddress)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapFragment.getMapAsync(this)
        bd.imvMarker.setOnTouchListener { v, e -> return@setOnTouchListener false }

    }
    override val enableEdgeToEdge: Boolean=true
    override val navigationPadding: Boolean=false

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(this)

        mMap = googleMap ?: return

        //initializing marker
        mMarker = mMap.addMarker(
            MarkerOptions()
                .position(
                    LatLng(
                        mMap.cameraPosition.target.latitude,
                        mMap.cameraPosition.target.longitude
                    )
                )
                .icon(
                    bitmapDescriptorFromVector(
                        this,
                        R.drawable.location_pick
                    )
                )
                .draggable(false)
        )

        mMap.let {
            it.isMyLocationEnabled = false
            it.uiSettings.isZoomControlsEnabled = true
            it.uiSettings.isCompassEnabled = true
            it.setOnCameraIdleListener {
                //getting center position of map
                mMarker?.isVisible = true
                bd.imvMarker.visibility = View.GONE

                if (mPrevious.latitude != mMap.cameraPosition.target.latitude && mPrevious.longitude != mMap.cameraPosition.target.longitude) {
                    setMarkerPosition(
                        mMap.cameraPosition.target.latitude,
                        mMap.cameraPosition.target.longitude
                    )
                    mPrevious = mMap.cameraPosition.target
                }
            }
            it.setOnCameraMoveListener {
                //updating marker when map being moved.
            }
            it.setOnCameraMoveStartedListener {
                mMarker?.isVisible = false
                bd.imvMarker.visibility = View.VISIBLE
            }

        }

        getCurrentLocation()


    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    private fun setMarkerPosition(
        lat: Double,
        lng: Double,
        isFirst: Boolean = false
    ) {
        val point = CameraUpdateFactory.newLatLng(
            LatLng(
                lat
                ,lng
            )
        )

// moves camera to coordinates
        mMap.moveCamera(point)

        if (isFirst) {
            bd.tvLocality.text = getString(R.string.location_please_wait)
            bd.tvAddress.text = getString(R.string.location_please_desc)
        }

        var googleAddress: Address? = null

        val gcd = Geocoder(this, Locale.getDefault())
        val addresses: List<Address>
        try {
            addresses = gcd.getFromLocation(lat, lng, 1)
            if (addresses.isNotEmpty()) {
                bd.tvLocality.text = addresses.get(0).subLocality ?: addresses.get(0).locality
                bd.tvAddress.text = addresses[0].getAddressLine(0)
                googleAddress = addresses[0]

                mCurrentLocation = SelectedAddress(
                    locality = googleAddress.subLocality ?: getString(R.string.locality),
                    city = googleAddress.locality ?: getString(R.string.locality),
                    country = googleAddress.countryName ?: getString(R.string.india),
                    state = googleAddress.adminArea ?: "",
                    fullAddress = googleAddress.getAddressLine(0)
                        ?: "Trying to get your location...",
                    knownName = googleAddress.featureName ?: "",
                    latitude = googleAddress.latitude,
                    longitude = googleAddress.longitude
                )

            } else {
                Handler().postDelayed({ getCurrentLocation() }, 500)
            }
        } catch (e: IOException) {
            Handler().postDelayed({ getCurrentLocation() }, 500)
        }

        if (mCurrentLocation != null) {

            //update marker info and position
            mMarker?.let {
                it.position = LatLng(lat, lng)
                it.title = mCurrentLocation?.knownName
                it.snippet = "Near ${mCurrentLocation?.fullAddress}"
            }

            val cameraPosition = CameraPosition.Builder()
                .target(LatLng(lat, lng))
                .zoom(15f)
                .build()

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }

    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {

        mFusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        val locationSettingsRequest = builder.build()

        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(locationSettingsRequest)

        result.addOnSuccessListener { settingResponse ->
            getLastLocation()
        }

        result.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }

        }


/*
        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                if (response!!.locationSettingsStates.isLocationPresent) {
                    getLastLocation()
                }
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        try {
                            val resolvable = exception as ResolvableApiException
                            resolvable.startResolutionForResult(
                                this,
                                ConfirmLocationDialog.REQUEST_CHECK_SETTINGS
                            )
                        } catch (e: IntentSender.SendIntentException) {
                        } catch (e: ClassCastException) {
                        }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        }*/
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient.lastLocation.addOnSuccessListener { task ->
            if (task != null) {
                setMarkerPosition(task.latitude, task.longitude)
            } else {
                Handler().postDelayed({ getLastLocation() }, 500)
            }
        }
    }

    override fun navHomeActivity() {
        val intent = Intent(this, HomePageActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, isNew)

        startActivity(intent)
        finishAffinity()
    }


    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_my_location -> getCurrentLocation()
            R.id.btn_pic_address -> {
                if (mCurrentLocation == null) {
                    showToast(getString(R.string.please_wait))
                } else {
//                    vm.saveUserCurrentLocation(
//                        mCurrentLocation!!
//                    ).observeForever {
//                        RHandler<Boolean>(this, it) {
//                            if (true) {
//                                navHomeActivity()
//                            } else {
//                                showToast("Something Happened...")
//                            }
//                        }
//                    }
                }
            }
        }
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }
}