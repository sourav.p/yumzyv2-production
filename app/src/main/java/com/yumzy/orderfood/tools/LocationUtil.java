package com.yumzy.orderfood.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;

public class LocationUtil {
    private static final String TAG = LocationUtil.class.getSimpleName();

    public static LocationManager getLocationManager(final Context context) {
        return (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public static boolean isNetworkProviderEnabled(final Context context) {
        return getLocationManager(context).isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isGpsProviderEnabled(final Context context) {
        return getLocationManager(context).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Returns true even if the location services are disabled. Do not use this method to detect location services are enabled.
    private static boolean isPassiveProviderEnabled(final Context context) {
        return getLocationManager(context).isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
    }

    public static boolean isLocationModeOn(final Context context) throws Exception {
        int locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    public static boolean isLocationEnabled(final Context context) {
        try {
            return isNetworkProviderEnabled(context) || isGpsProviderEnabled(context) || isLocationModeOn(context);
        } catch (Exception e) {
            Log.e(TAG, "[isLocationEnabled] error:", e);
        }
        return false;
    }

    public static void gotoLocationSettings(final Activity activity, final int requestCode) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivityForResult(intent, requestCode);
    }

    public static String getEnabledProvidersLogMessage(final Context context) {
        try {
            return "[getEnabledProvidersLogMessage] isNetworkProviderEnabled:" + isNetworkProviderEnabled(context) +
                    ", isGpsProviderEnabled:" + isGpsProviderEnabled(context) +
                    ", isLocationModeOn:" + isLocationModeOn(context) +
                    ", isPassiveProviderEnabled(ignored):" + isPassiveProviderEnabled(context);
        } catch (Exception e) {
            Log.e(TAG, "[getEnabledProvidersLogMessage] error:", e);
            return "provider error";
        }
    }
}