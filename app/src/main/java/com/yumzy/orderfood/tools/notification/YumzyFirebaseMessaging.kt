package com.yumzy.orderfood.tools.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.clevertap.android.sdk.CleverTapAPI
import com.clevertap.android.sdk.Logger
import com.clevertap.android.sdk.pushnotification.fcm.FcmMessageListenerService
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.messaging.RemoteMessage
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.appCrashlytics
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.zoho.livechat.android.ZohoLiveChat
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.atomic.AtomicInteger


class YumzyFirebaseMessaging : FcmMessageListenerService() {


    private val PENDING_INTENT_REQUEST_CODE = 54

    companion object {

        private val NOTIFICATION_ID = "id"
        private val NOTIFICATION_TITLE = "title"
        private val NOTIFICATION_MESSAGE = "message"
        private val NOTIFICATION_IMAGEURL = "imageUrl"
        val NOTIFICATION_URL = "url"

        private val NOTIFICATION_CHANNEL_ID = "channel_id"
        private val NOTIFICATION_CHANNEL_NAME = "channel_name"

        private val NOTIFICATION_CHANNEL_IMPORTANCE = "importance"
        private val NOTIFICATION_CHANNEL_IMPORTANCE_HIGH = "high"
        private val NOTIFICATION_CHANNEL_IMPORTANCE_MEDIUM = "medium"

        private val NOTIFICATION_TYPE = "type"
        private val NOTIFICATION_TYPE_SIMPLE = "simple"
        private val NOTIFICATION_TYPE_LARGEIMAGE = "large_image"

        const val NOTIFICATION_DESTINATION = "destination"

        const val NOTIFICATION_DESTINATION_OPEN_APP_HOME = "home"
        const val NOTIFICATION_DESTINATION_OPEN_REWARD = "reward"
        const val NOTIFICATION_DESTINATION_OPEN_EXPLORE = "explore"
        const val NOTIFICATION_DESTINATION_OPEN_PLATE = "plate"
        const val NOTIFICATION_DESTINATION_WALLET = "wallet"
        const val NOTIFICATION_DESTINATION_OPEN_INVITE = "invite"
        const val NOTIFICATION_DESTINATION_OPEN_FAVOURITES = "favourites"
        const val NOTIFICATION_DESTINATION_OPEN_FOOD_PREF = "food_pref"
        const val NOTIFICATION_DESTINATION_OPEN_OFFER = "offer"
        const val NOTIFICATION_DESTINATION_OPEN_ORDER_TRACKING = "order_tracking"
        const val NOTIFICATION_DESTINATION_UPDATE = "update"

        const val NOTIFICATION_DESTINATION_PROMO_OUTLET = "promo_outlet"
        const val NOTIFICATION_DESTINATION_PROMO_DISH = "promo_dish"

        const val NOTIFICATION_DESTINATION_DISH = "dish"
        const val NOTIFICATION_DESTINATION_OUTLET = "outlet"

        const val NOTIFICATION_DESTINATION_REFERRAL = "referral"
        const val NOTIFICATION_DESTINATION_OPEN_URL = "url"

        private val ACTION_ORDER_TRACKING = "order_tracking"
        private val ACTION_PROMO_OUTLET = "promo_outlet"
        private val ACTION_PROMO_DISH = "promo_dish"
        private val ACTION_OUTLET = "outlet"
        private val ACTION_DISH = "dish"
        var channelId = "8989"
        var channelName = "Yumzy"

        fun getToken(context: Context): String? {
            return context.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty")
        }

    }

    private var mTitle = ""
    private var mSubtitle = ""

    //default values
    val id = AtomicInteger(4545)

    var channelImportance = NOTIFICATION_CHANNEL_IMPORTANCE_MEDIUM


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val handleCleverMessage = handleCleverMessage(remoteMessage)
        if (handleCleverMessage) return
        handleZohoNotification(remoteMessage)
        val notification = remoteMessage.notification
        if (notification != null) {
            mTitle = notification.title ?: getString(R.string.app_name)
            mSubtitle = notification.body ?: getString(R.string.food_order_at_finger_your_tip)
        } else {
            return
        }

        if (validateNotificationData(remoteMessage.data)) {
            buildNotification(remoteMessage.data)
        } else {
            //send default notification if validation fails.
            sendBaiscNotification()
        }
    }

    private fun handleZohoNotification(remoteMessage: RemoteMessage) {
        try {
            val extras = remoteMessage.data
            ZohoLiveChat.Notification.handle(this.applicationContext, extras, R.mipmap.ic_launcher)
        } catch (exception: Exception) {
            appCrashlytics.log("Zoho Exception Notification Exception")
        }
    }

    private fun handleCleverMessage(remoteMessage: RemoteMessage): Boolean {
        try {
            if (remoteMessage.data.isNotEmpty()) {
                val extras = Bundle()
                for ((key, value) in remoteMessage.data) {
                    extras.putString(key, value)
                }
                val info = CleverTapAPI.getNotificationInfo(extras)
                return if (info.fromCleverTap) {
                    CleverTapAPI.createNotification(
                        applicationContext,
                        extras
                    )
                    CleverTapAPI.getDefaultInstance(this)?.pushNotificationViewedEvent(extras)
                    true
                } else {
                    false
                    // not from CleverTap handle yourself or pass to another provider
                }
            }
        } catch (t: Throwable) {
            Logger.d("Error parsing FCM message", t)
            return false
        }
        return false
    }


    private fun sendBaiscNotification() {

        val pendingIntent: PendingIntent
        val notificationBuilder: NotificationCompat.Builder

        val intent = Intent(this, StartedActivity::class.java)

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        pendingIntent = PendingIntent.getActivity(
            this, PENDING_INTENT_REQUEST_CODE, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        notificationBuilder = NotificationCompat.Builder(this, id.toString() + "")
            .setContentTitle(mTitle)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    applicationContext.resources,
                    getNotificationIcon(-1)
                )
            )
            .setContentText(mSubtitle)
            .setAutoCancel(true)
            .setSmallIcon(R.drawable.ic_stat_notification)
            .setColor(ContextCompat.getColor(applicationContext, R.color.pinkies))
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setContentIntent(pendingIntent)

        notificationBuilder.setStyle(
            NotificationCompat.BigTextStyle().bigText(mSubtitle)
        )

        sendNotification(
            id.getAndIncrement(),
            channelId,
            channelName,
            channelImportance,
            notificationBuilder
        )
    }

    private fun validateNotificationData(data: Map<String, String>): Boolean {
        var valid = true

        if (data.containsKey(NOTIFICATION_TYPE)) {
            if (data[NOTIFICATION_TYPE].equals(NOTIFICATION_TYPE_LARGEIMAGE)) {
                if (!data.containsKey(NOTIFICATION_IMAGEURL) || data[NOTIFICATION_IMAGEURL].isNullOrBlank()) {
                    valid = false
                    FirebaseCrashlytics.getInstance()
                        .setCustomKey("functionName", "validateNotificationData($data)")
                    FirebaseCrashlytics.getInstance().setCustomKey(
                        "Too Large Image",
                        "Received Large Image Notification without Image URL"
                    )
                }
            }
        }

        if (data.containsKey(NOTIFICATION_DESTINATION)) {
            if (data[NOTIFICATION_DESTINATION].equals(NOTIFICATION_DESTINATION_OPEN_URL)) {
                if (!data.containsKey(NOTIFICATION_URL) || data[NOTIFICATION_URL].isNullOrBlank()) {
                    valid = false
                    FirebaseCrashlytics.getInstance()
                        .setCustomKey("functionName", "validateNotificationData($data)")
                    FirebaseCrashlytics.getInstance().setCustomKey(
                        "notification_without_url",
                        "Received URL Destination Notification without URL"
                    )
                }
            }

            if (data[NOTIFICATION_DESTINATION].equals(NOTIFICATION_DESTINATION_OPEN_ORDER_TRACKING)) {
                if (!data.containsKey(NOTIFICATION_URL) || data[NOTIFICATION_URL].isNullOrBlank()) {
                    valid = false
                    FirebaseCrashlytics.getInstance()
                        .setCustomKey("functionName", "validateNotificationData($data)")
                    FirebaseCrashlytics.getInstance().setCustomKey(
                        "notification_without_url",
                        "Received ORDER TRACKING Destination Notification without URL"
                    )
                }
            }
        }

        if (data.containsKey(NOTIFICATION_CHANNEL_ID) && !data[NOTIFICATION_CHANNEL_ID].isNullOrBlank()) {
            channelId = data[NOTIFICATION_CHANNEL_ID].toString()
        }

        if (data.containsKey(NOTIFICATION_CHANNEL_NAME) && !data[NOTIFICATION_CHANNEL_NAME].isNullOrBlank()) {
            channelName = data[NOTIFICATION_CHANNEL_NAME].toString()
        }

        if (data.containsKey(NOTIFICATION_CHANNEL_IMPORTANCE) && !data[NOTIFICATION_CHANNEL_IMPORTANCE].isNullOrBlank()) {
            channelImportance = data[NOTIFICATION_CHANNEL_IMPORTANCE].toString()
        }

        return valid
    }

    private fun buildNotification(data: Map<String, String>) {

        try {

            val intent: Intent
            val pendingIntent: PendingIntent
            val notificationBuilder: NotificationCompat.Builder
            var id: Int? = null

            if (data.containsKey(NOTIFICATION_ID)) {
                id = Integer.parseInt(data.getValue(NOTIFICATION_ID))
            }

            intent = Intent(this, StartedActivity::class.java)
            intent.putExtra(NOTIFICATION_DESTINATION, data[NOTIFICATION_DESTINATION])
            intent.putExtra(
                NOTIFICATION_URL,
                if (data[NOTIFICATION_URL].isNullOrEmpty()) "empty" else data[NOTIFICATION_URL]
            )

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            pendingIntent = PendingIntent.getActivity(
                this, PENDING_INTENT_REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT
            )

            notificationBuilder = NotificationCompat.Builder(this, id.toString() + "")
                .setContentTitle(mTitle)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setColor(ContextCompat.getColor(applicationContext, R.color.pinkies))
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        applicationContext.resources,
                        getNotificationIcon(-1)
                    )
                )
                .setContentText(mSubtitle)
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigPictureStyle().bigPicture(getBitmap("")))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)



            when (data[NOTIFICATION_TYPE]) {
                NOTIFICATION_TYPE_LARGEIMAGE -> {
                    notificationBuilder.setStyle(
                        NotificationCompat.BigPictureStyle()
                            .bigPicture(getBitmap(data.getValue(NOTIFICATION_IMAGEURL)))
                            .setBigContentTitle(
                                data[NOTIFICATION_TITLE]
                            )
                    )
                }
                else -> notificationBuilder.setStyle(
                    NotificationCompat.BigTextStyle().bigText(mSubtitle)
                )
            }

            sendNotification(
                id ?: this.id.getAndIncrement(),
                channelId,
                channelName,
                channelImportance,
                notificationBuilder
            )

        } catch (e: Exception) {
            FirebaseCrashlytics.getInstance()
                .setCustomKey("unknown_exception", "buildNotification($data)")
            FirebaseCrashlytics.getInstance().setCustomKey("unknown_exception_cause", "$e")
        }

    }

    private fun getNotificationIcon(iconCode: Int): Int {
        return when (iconCode) {
            1 -> R.drawable.ic_white_logo
            else -> R.mipmap.ic_launcher_foreground
        }

    }

    private fun sendNotification(
        notificationId: Int,
        channelId: String,
        channelName: String,
        channelImportance: String,
        notificationBuilder: NotificationCompat.Builder
    ) {

        try {
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                if (notificationManager.getNotificationChannel(channelId) == null) {

                    val channelImportanceValue = when (channelImportance) {
                        NOTIFICATION_CHANNEL_IMPORTANCE_HIGH -> NotificationManager.IMPORTANCE_HIGH
                        else -> NotificationManager.IMPORTANCE_LOW
                    }

                    val notificationChannel =
                        NotificationChannel(
                            channelId,
                            channelName,
                            channelImportanceValue
                        )
                    notificationManager.createNotificationChannel(notificationChannel)

                }

                notificationBuilder.setChannelId(channelId)

            }

            notificationManager.notify(notificationId, notificationBuilder.build())
        } catch (e: Exception) {
            FirebaseCrashlytics.getInstance().setCustomKey(
                "unknown_exception",
                "sendNotification($notificationId, NOT NULL notificationBuilder)"
            )
            FirebaseCrashlytics.getInstance().setCustomKey("unknown_exception_cause", "$e")
        }
    }

    private fun getBitmap(urlString: String): Bitmap? {
        var myBitmap: Bitmap? = null
        try {
            val url = URL(urlString)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val inputStream = connection.inputStream
            myBitmap = BitmapFactory.decodeStream(inputStream)
        } catch (e: Exception) {
            FirebaseCrashlytics.getInstance()
                .setCustomKey("unknown_exception", "getBitmap(NOT NULL urlString)")
            FirebaseCrashlytics.getInstance().setCustomKey("unknown_exception_cause", "$e")

        }

        return myBitmap
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        if (token.isNotEmpty()) {

            ZohoLiveChat.Notification.enablePush(
                token,
                !BuildConfig.DEBUG
            )

            application.getSharedPreferences("_", MODE_PRIVATE).edit().putString("fb", token)
                .apply();
            CleverTapAPI.getDefaultInstance(this)?.pushFcmRegistrationId(token, true)

        }

    }



}
