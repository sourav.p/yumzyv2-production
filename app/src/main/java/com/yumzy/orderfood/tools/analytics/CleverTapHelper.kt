package com.yumzy.orderfood.tools.analytics

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Handler
import android.os.Looper
import com.clevertap.android.geofence.CTGeofenceAPI
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.appCleverTap
import com.yumzy.orderfood.appCrashlytics
import com.yumzy.orderfood.data.SearchItemDTO
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingRegularMenuItem
import java.io.IOException
import java.util.*
private val globalUser: UserDTO? = null

interface CleverEvents {
    companion object {
        const val app_permission = "app_permission"
        const val user_permission = "user_permission"
        const val signup_location = "signup_location"
        const val user_signup = "user_signup" //checked
        const val user_login = "user_login" //checked
        const val user_location = "user_location"//checked
        const val user_referral = "user_referral"//checked current user referral properties
        const val user_logout =
            "user_logout"// issue with clearing db which cause logout event trigger failed due
        const val support_chat_requested: String = "support_chat_requested"
        const val app_started = "app_started"//checked
        const val app_exited = "app_exited"//checked
        const val outlet_viewed = "outlet_viewed"//checked
        const val cart_cleared = "cart_cleared"//checked
        const val menu_search = "menu_search"//removed in meeting by akash sir
        const val explore_search = "explore_search"//removed in meeting by akash sir
        const val food_preference = "food_preference"
        const val shared_outlet = "shared_outlet"
        const val coupon_applied = "coupon_applied"
        const val coupon_removed = "coupon_removed"
        const val order_rated = "order_rated"
        const val item_removed_cart = "item_removed_cart"
        const val item_added_cart = "item_added_cart"
        const val see_more = "see_more"
        const val event_issue = "event_issue"
        const val show_outlet = "show_outlet"

        /* todo some issue in version 2.0.53 with repeating charged event*/
        const val payment_initiated = "payment_initiated"
        const val payment_canceled = "placing_order"
        const val payment_success = "payment_success"
        const val payment_failed = "payment_failed"
        const val promo_click = "promo_click"

        const val user_started = "user_started"
        const val used_referral = "used_referral"//todo

        const val order_deliveryfailed: String = "order_deliveryfailed"
        const val order_completed: String = "order_completed"
        const val order_cancelled: String = "order_cancelled"
        const val order_restorejected: String = "order_restorejected"
        const val app_background = "app_background"
        const val profile_change = "cart_cleared"

        const val unable_to_checkout = "unable_to_checkout"
        const val order_tracking = "tracking_order"
        const val user_explore_first = "user_explore_first"

        //for any unknown event push failed
//        const val addon_item_added = "addon_item_added"

    }
}

object CleverTapHelper {
//    private val enableClever: Boolean = BuildConfig.DEBUG


    fun setCleaverUser(data: LoginDTO) {
//        if (enableClever) return

        val user = data.user
        val profileUpdate =
            HashMap<String, Any>()
        profileUpdate["Identity"] = user.userId // String or number
        profileUpdate["Name"] = user.name // String
        profileUpdate["Email"] = user.email // Email address of the user
        profileUpdate["Phone"] = user.mobile // Phone (with the country code, starting with +)
        val latitude: Double = user.currentLocation?.coordinates?.get(1) ?: 0.0
        val longitude: Double = user.currentLocation?.coordinates?.get(0) ?: 0.0

        var picture = user.picture
        if (picture?.isEmpty() == true)
            picture =
                "https://engineering.ewr1.vultrobjects.com/hosted_images/place_holder.jpg"
        profileUpdate["Photo"] = picture ?: "" // URL to the Image
        appCleverTap.onUserLogin(profileUpdate)

        val location: android.location.Location = Location(LocationManager.GPS_PROVIDER)
        location.latitude = latitude
        location.longitude = longitude
        appCleverTap.location = location
        zohoSetupVisitor(user)
    }

    fun updateUserLocation(locality: String, city: String, lat: Double, lng: Double) {
        val profileUpdate =
            HashMap<String, Any>()
        profileUpdate["City"] = city // String or number
        profileUpdate["Location"] = locality // Email address of the user
        profileUpdate["Address"] = "$locality, $city" // String
        val location: android.location.Location = Location(LocationManager.GPS_PROVIDER)
        location.latitude = lat
        location.longitude = lng
        appCleverTap.location = location //android.location.Location

        appCleverTap.pushProfile(profileUpdate)
        val propertyMap = HashMap<String, Any>()
        propertyMap["latlong"] = arrayOf(lat, lng)
        propertyMap["address"] = AppGlobalObjects.selectedAddress?.fullAddress ?: ""
        propertyMap["city"] = AppGlobalObjects.selectedAddress?.city ?: "unknown"
        propertyMap["locality"] = AppGlobalObjects.selectedAddress?.googleLocation ?: "unknown"
        appCleverTap.pushEvent(CleverEvents.user_location, propertyMap)
    }

    /*check again */
    fun updateLatLongUserLocation(locality: String, city: String, lng: Double, lat: Double) {
        val profileUpdate =
            HashMap<String, Any>()
        profileUpdate["City"] = city // String or number
        profileUpdate["Location"] = locality // Email address of the user
        profileUpdate["Address"] = "$locality, $city" // String
        val location: android.location.Location = Location(LocationManager.GPS_PROVIDER)
        location.latitude = lat
        location.longitude = lng
        appCleverTap.location = location //android.location.Location

        appCleverTap.pushProfile(profileUpdate)
        val propertyMap = HashMap<String, Any>()
        propertyMap["latlong"] = arrayOf(lat, lng)
        propertyMap["address"] = AppGlobalObjects.selectedAddress?.fullAddress ?: ""
        propertyMap["city"] = AppGlobalObjects.selectedAddress?.city ?: "unknown"
        propertyMap["locality"] = AppGlobalObjects.selectedAddress?.googleLocation ?: "unknown"
        appCleverTap.pushEvent(CleverEvents.user_location, propertyMap)
    }

    fun searchQuery(
        query: String,
        searchResult: ArrayList<SearchItemDTO>
    ) {
        val propertyMap =
            HashMap<String, Any>()
        propertyMap["search_result"] = searchResult
        propertyMap["search_size"] = searchResult.size
        propertyMap["search_keyword"] = query

        appCleverTap.pushEvent(CleverEvents.explore_search, propertyMap)
    }

    /*TODO CALL IN APPLY REFFERAL*/
    fun referralLink(
        name: String,
        mobile: String,
        referred_by: String
    ) {
        appCleverTap.pushEvent(
            CleverEvents.used_referral, mapOf(
                "user_name" to name,
                "mobile" to mobile,
                "referred_by" to referred_by,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun seeMoreClicked(title: String?, layoutID: Int, promoId: String) {
        val propertyMap =
            HashMap<String, Any>()
        propertyMap["clicked_banner"] = title ?: ""
        propertyMap["layout_id"] = layoutID
        propertyMap["promo_id"] = promoId
        appCleverTap.pushEvent(CleverEvents.see_more, propertyMap)
    }

    fun seePromoClicked(
        sectionHeading: String,
        homeItem: HomeListDTO,
        layoutType: Int,
        item: String?
    ) {
        val propertyMap =
            HashMap<String, Any>()
        propertyMap["layout_name"] = sectionHeading
        propertyMap["layout_id"] = layoutType
        propertyMap["layout_item_name"] = item ?: homeItem.title
        propertyMap["promo_id"] = homeItem.promoId ?: ""
        appCleverTap.pushEvent(CleverEvents.promo_click, propertyMap)
    }


    /*"category, menu_size, offer, cart_size,promo_id, cost_for_two,
    availability"*/
    fun viewedOutlet(restaurant: RestaurantDTO) {
        val restaurantDetail = restaurant.restaurantDetailDTO ?: return
        val propertyMap =
            HashMap<String, Any>()
//        propertyMap["category"] = restaurantDetail.cuisines
        propertyMap["menu_size"] = restaurant.menuSectionDTOS?.size ?: ""
        propertyMap["outlet_id"] = restaurantDetail.outletId
        propertyMap["cost_for_two"] = restaurantDetail.costForTwo
        propertyMap["availability"] = restaurantDetail.available
        appCleverTap.pushEvent(CleverEvents.show_outlet, propertyMap)

    }

    /* fun itemAddonAdded(itemDTO: NewAddItemDTO) {
         val propertyMap =
             HashMap<String, Any>()
         propertyMap["id"] = itemDTO.item.itemID
         propertyMap["cart_id"] = itemDTO.cartID
         propertyMap["addon"] = itemDTO.item.addons ?: listOf<Variant>()
         appCleverTap.pushEvent(CleverEvents.addon_item_added, propertyMap)
     }
 */
    fun itemQuantityChange(
        itemDto: RestaurantItem,
        count: Int
    ) {

        try {
            val propertyMap =
                HashMap<String, Any>()
            propertyMap["quantity"] = count
            propertyMap["price"] = itemDto.price
            propertyMap["item_id"] = itemDto.itemId
            propertyMap["item_name"] = itemDto.name
            propertyMap["outlet_name"] = itemDto.outletName
            propertyMap["item_discount"] = itemDto.currentOfferPrice
            propertyMap["addons"] = itemDto.addons
            propertyMap["outlet_id"] = itemDto.outletId
            propertyMap["city"] = AppGlobalObjects.selectedAddress?.city ?: "unknown"
            propertyMap["locality"] = AppGlobalObjects.selectedAddress?.googleLocation ?: "unknown"
            if (count == 0) {
                appCleverTap.pushEvent(CleverEvents.item_removed_cart, propertyMap)
            } else {
                appCleverTap.pushEvent(CleverEvents.item_added_cart, propertyMap)
            }
        } catch (e: Exception) {
            appCrashlytics.log("$e")
        }
    }

    fun plateItemChange(
        adapterPosition: Int, itemId: String?, count: Int, price: String, outlet: String
    ) {
        val propertyMap =
            HashMap<String, Any>()
        propertyMap["quantity"] = count
        propertyMap["price"] = price
        propertyMap["item_id"] = itemId ?: ""
        propertyMap["outletName"] = outlet
        propertyMap["city"] = AppGlobalObjects.selectedAddress?.city ?: "unknown"
        propertyMap["locality"] = AppGlobalObjects.selectedAddress?.googleLocation ?: "unknown"
        if (count == 0) {
            appCleverTap.pushEvent(CleverEvents.item_removed_cart, propertyMap)
        } else {
            appCleverTap.pushEvent(CleverEvents.item_added_cart, propertyMap)
        }
    }

    fun pushEvent(eventName: String) {
        appCleverTap.pushEvent(eventName)
    }

    fun pushEventMap(eventName: String, property: Map<String, Any>) {
        appCleverTap.pushEvent(eventName, property)
    }

    fun userFoodPreference(foodPref: FoodPersReqDTO) {
        appCleverTap.pushEvent(
            CleverEvents.food_preference, mapOf(
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "cuisine_list" to foodPref.likeCategories.toString(),
                "cuisine_list_tag" to foodPref.likeTags.toString()
            )
        )
    }

    fun pushChargeEvent(
        userCart: CartResDTO?,
        sharedPrefsHelper: SharedPrefsHelper,
        earnedScratch: Boolean
    ) {
        if (userCart?.outletDTOS?.size == 0) return

        val cartOutletDetails = userCart!!.outletDTOS?.get(0)

        val chargeDetails = HashMap<String, Any>()

        val items =
            ArrayList<HashMap<String, Any>>()

        cartOutletDetails?.let {
            chargeDetails["Amount"] = userCart.billing?.totalAmount ?: 0.0
//            chargeDetails["Payment Mode"] = "Credit card"
            chargeDetails["Charged ID"] = userCart.cartId ?: "INVALID"

            it.itemDTOS.forEach { dishItem ->
                val item = HashMap<String, Any>()
                item["name"] = dishItem.name
                item["addons"] = dishItem.addons
                item["id"] = dishItem.itemID
                item["quantity"] = dishItem.quantity
                item["price"] = dishItem.price
                items.add(item)
            }
        }

        try {
            appCleverTap.pushChargedEvent(chargeDetails, items)
        } catch (e: Exception) {
            // You have to specify the first parameter to push()
            // as CleverTapAPI.CHARGED_EVENT
            pushEvent(CleverEvents.event_issue)
        }
    }


    private fun zohoSetupVisitor(user: UserDTO) {
//        ZohoSalesIQ.registerVisitor(user.mobile?:"")
//        ZohoSalesIQ.Visitor.setName(user.name)
//        ZohoSalesIQ.Visitor.setEmail(user.email)
//        ZohoSalesIQ.Visitor.setContactNumber(user.mobile)

    }

    fun onAppStarted(message: String, googleLocation: String, size: Int, code: Int) {
        appCleverTap.pushEvent(
            CleverEvents.app_started, mapOf(
                "message" to message,
                "start_code" to code,
                "locality" to googleLocation,
                "home_list_size" to size
            )
        )

    }

    fun outletOpened(outletId: String, outletName: String) {
        appCleverTap.pushEvent(
            CleverEvents.outlet_viewed, mapOf(
                "outlet_id" to outletId,
                "outlet_name" to outletName,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "outlet_open" to true
            )
        )
    }

    fun outletClosed(outletId: String, outletName: String) {
        appCleverTap.pushEvent(
            CleverEvents.outlet_viewed, mapOf(
                "outlet_id" to outletId,
                "outlet_name" to outletName,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "outlet_open" to false
            )
        )
    }

    fun menuSearch(results: List<BindingRegularMenuItem>?) {
        val restaurantItems = arrayListOf<RestaurantItem>()
        results?.forEach {
            it.restaurantItem?.let { it1 -> restaurantItems.add(it1) }

        }

        if (restaurantItems.size > 0) {
            val restaurantItem = restaurantItems[0]
            appCleverTap.pushEvent(
                CleverEvents.menu_search, mapOf(
                    "search_item" to restaurantItems,
                    "search_size" to restaurantItems.size,
                    "outlet_id" to restaurantItem.outletId,
                    "outlet_name" to restaurantItem.outletName,
                    "locality" to AppGlobalObjects.selectedAddress?.googleLocation
                )
            )
        }

    }


    fun appExited() {
        appCleverTap.pushEvent(
            CleverEvents.app_exited, mapOf(
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "city" to AppGlobalObjects.selectedAddress?.city
            )
        )
    }

    fun clearCart(cartResDTO: CartResDTO?) {
        if (cartResDTO?.outletDTOS?.size == 0) return
        val cartOutletDetails = cartResDTO?.outletDTOS?.get(0)

        appCleverTap.pushEvent(
            CleverEvents.cart_cleared, mapOf(
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "total price" to cartOutletDetails?.billing?.subTotal,
                "outlet_id" to cartOutletDetails?.outletId,
                "outlet_name" to cartOutletDetails?.outletName,
                "no_of_items" to cartOutletDetails?.itemDTOS?.size,
                "cart_discount" to cartOutletDetails?.billing?.discount,
                "city" to AppGlobalObjects.selectedAddress?.city

            )
        )
    }

    fun clearCartResponse(cartResDTO: CartResDTO?) {
        if (cartResDTO?.outletDTOS?.size == 0) return
        val cartOutletDetails = cartResDTO?.outletDTOS?.get(0)

        appCleverTap.pushEvent(
            CleverEvents.cart_cleared, mapOf(
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "total price" to cartOutletDetails?.billing?.subTotal,
                "outlet_id" to cartOutletDetails?.outletId,
                "outlet_name" to cartOutletDetails?.outletName,
                "no_of_items" to cartOutletDetails?.itemDTOS?.size,
                "cart_discount" to cartOutletDetails?.billing?.discount,
                "city" to AppGlobalObjects.selectedAddress?.city

            )
        )
    }

    fun userLogout() {
        appCleverTap.pushEvent(
            CleverEvents.user_logout, mapOf(
                "user_id" to globalUser?.userId,
                "user_name" to globalUser?.name,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun shareOutlet(restaurantDTO: RestaurantDetailDTO?) {
        if (restaurantDTO == null) return
        appCleverTap.pushEvent(
            CleverEvents.shared_outlet, mapOf(
                "outlet_name" to restaurantDTO.outletName,
                "outlet_id" to restaurantDTO.outletId,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun paymentFailed(cartResDTO: CartResDTO?) {
        val cartOutletDetails = cartResDTO?.outletDTOS?.get(0)
        appCleverTap.pushEvent(
            CleverEvents.payment_failed, mapOf(
                "user_id" to globalUser?.userId,
                "user_name" to globalUser?.name,
                "total_price" to cartOutletDetails?.billing?.subTotal,
                "cart_discount" to cartOutletDetails?.billing?.discount,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun paymentSuccess(
        cartResDTO: CartResDTO?,
        sharedPrefsHelper: SharedPrefsHelper,
        earnedScratch: Boolean
    ) {
        val cartOutletDetails = cartResDTO?.outletDTOS?.get(0)
        /*
        food

         */

        if (cartResDTO != null) {
            val outletInfo = cartResDTO.outletDTOS?.get(0)
            val coupon = if (cartResDTO.coupons?.size ?: 0 > 0) cartResDTO.coupons?.get(0) else null
            var gst = outletInfo?.gst ?: "0"
            var delivery_charges = ""
            var restaurant_charges = ""
            var packaging_charges = ""
            var restaurant_discount = cartResDTO.billing?.discount

            cartResDTO.billing?.charges?.forEach {
                when (it.name) {
                    "restaurantCharges" -> restaurant_charges = it.value.toString()
                    "packingCharges" -> packaging_charges = it.value.toString()
                    "deliveryCharges" -> delivery_charges = it.value.toString()
                }

            }


            val couponType =
                if (cartResDTO.coupons?.size ?: 0 > 0) coupon?.discountType else ""
            val outletName = outletInfo?.outletName ?: "unknown"
            val outletId = outletInfo?.outletId ?: "unknown"
            val discount = outletInfo?.billing?.discount ?: ""
            val couponApplied = cartResDTO.coupons?.size == 0
            var coupon_value = ""
            val coupon_applied = if (couponApplied) {
                "N"
            } else {
                coupon_value = cartResDTO.coupons?.get(0).toString()
                "Y"
            }
            val items =
                ArrayList<HashMap<String, Any>>()
            cartOutletDetails?.let {

                it.itemDTOS.forEach { dishItem ->
                    val item = HashMap<String, Any>()
                    item["name"] = dishItem.name
                    item["addons"] = dishItem.addons
                    item["id"] = dishItem.itemID
                    item["quantity"] = dishItem.quantity
                    item["price"] = dishItem.price
                    items.add(item)
                }
            }
            val eventMap = mapOf(
                "user_id" to globalUser?.userId,
//                "user_id" to globalUser?.userId,

                "user_name" to globalUser?.name,
//                "user_name" to globalUser?.name,

                "cart_id" to sharedPrefsHelper.getCartId(),
                "outlet_id" to sharedPrefsHelper.getOutletID(),
                "total_price" to cartOutletDetails?.billing?.subTotal,
                "cart_discount" to cartOutletDetails?.billing?.discount,
                "item_value" to items,
                "charges" to cartOutletDetails?.billing?.charges,
                "taxes" to cartOutletDetails?.billing?.taxes,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation,
                "coupon_code" to cartResDTO.coupons,
                "total price" to outletInfo?.billing?.subTotal,
                "cart_discount" to outletInfo?.billing?.discount,
                "delivery_charges" to delivery_charges,
                "restaurant_charges" to restaurant_charges,
                "packaging_charges" to packaging_charges,
                "coupon_type" to couponType,
                "gst" to outletInfo?.gst,
                "discount" to discount,
                "coupon_amount" to coupon?.maxDiscount,
                "coupon_applied" to coupon_applied,
                "coupon_value" to coupon_value,
                "coupon_type" to coupon?.discountType,
                "no_of_items" to outletInfo?.itemDTOS?.size,
                "outlet_id" to outletId,
                "outlet_name" to outletName,
                "earn_scratchcard" to earnedScratch,
                "no_of_items" to items.size,
                "item_total_amount" to items.size,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )




            appCleverTap.pushEvent(
                CleverEvents.payment_success, eventMap
            )
        }
    }

    fun couponApplied(cartResDTO: CartResDTO) {
        if (cartResDTO.outletDTOS?.size == 0) return
        val cartOutletDetails = cartResDTO.outletDTOS?.get(0)

        val couponType =
            if (cartResDTO.coupons?.size ?: 0 > 0) cartResDTO.coupons?.get(0)?.discountType else ""
//        val gst=cartOutletDetails?.gst?:"0"
//        val gst=cartOutletDetails?.gst?:"0"

        appCleverTap.pushEvent(
            CleverEvents.coupon_applied, mapOf(
                "user_id" to globalUser?.userId,
                "user_name" to globalUser?.name,
                "coupon_code" to cartResDTO.coupons?.get(0)?.code,
                "total price" to cartOutletDetails?.billing?.subTotal,
                "cart_discount" to cartOutletDetails?.billing?.discount,
                "coupon_type" to couponType,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun paymentInitiated(cartResDTO: CartResDTO?) {
        if (cartResDTO != null) {
            val outletInfo = cartResDTO.outletDTOS?.get(0)
            val coupon =
                if (cartResDTO.coupons?.size ?: 0 > 0) cartResDTO.coupons?.get(0) else null
            val gst = outletInfo?.gst ?: "0"
            var delivery_charges = ""
            var restaurant_charges = ""
            var packaging_charges = ""
            var restaurant_discount = cartResDTO.billing?.discount

            cartResDTO.billing?.charges?.forEach {
                when (it.name) {
                    "restaurantCharges" -> restaurant_charges = it.value.toString()
                    "packingCharges" -> packaging_charges = it.value.toString()
                    "deliveryCharges" -> delivery_charges = it.value.toString()
                }

            }


            val couponType =
                if (cartResDTO.coupons?.size ?: 0 > 0) coupon?.discountType else ""
            val outletName = outletInfo?.outletName ?: "unknown"
            val outletId = outletInfo?.outletId ?: "unknown"
            val discount = outletInfo?.billing?.discount ?: ""
            val couponApplied = cartResDTO.coupons?.size == 0
            val coupon_applied = if (couponApplied)
                "N"
            else
                "Y"

            val eventMap = mapOf(
                "user_id" to globalUser?.userId,
                "user_name" to globalUser?.name,
                "coupon_code" to cartResDTO.coupons,
                "total_price" to outletInfo?.billing?.subTotal,
                "gst" to gst,
                "cart_discount" to outletInfo?.billing?.discount,
                "delivery_charges" to delivery_charges,
                "restaurant_charges" to restaurant_charges,
                "packaging_charges" to packaging_charges,
                "coupon_type" to couponType,
                "gst" to outletInfo?.gst,
                "discount" to discount,
                "coupon_amount" to coupon?.maxDiscount,
                "coupon_applied" to coupon_applied,
                "coupon_type" to coupon?.discountType,
                "no_of_items" to outletInfo?.itemDTOS?.size,
                "outlet_id" to outletId,
                "outlet_name" to outletName,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
            appCleverTap.pushEvent(
                CleverEvents.payment_initiated, eventMap
            )
        }
    }


    fun couponRemoved(cartResDTO: CartResDTO?) {
        if (cartResDTO == null) return
        if (cartResDTO.outletDTOS?.size == 0) return
        val cartOutletDetails = cartResDTO.outletDTOS?.get(0)
        var coupon: Coupon? = null
        val couponType =
            if (cartResDTO.coupons?.size ?: 0 > 0) {
                coupon = cartResDTO.coupons?.get(0)
                coupon?.discountType
            } else ""
        val maxDiscount = coupon?.maxDiscount ?: 0.0
        appCleverTap.pushEvent(
            CleverEvents.coupon_removed, mapOf(
                "user_id" to globalUser?.userId,
                "user_name" to globalUser?.name,
                "coupon_code" to cartResDTO.coupons,
                "total price" to cartOutletDetails?.billing?.subTotal,
                "coupon_amount" to maxDiscount,
                "coupon_type" to couponType,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun submitRatting(selRatingFeedback: RatingFeedbackDTO) {
        appCleverTap.pushEvent(
            CleverEvents.order_rated, mapOf(
                "foodRating" to selRatingFeedback.foodRating.joinToString(separator = ","),
                "orderNum" to globalUser?.name,
                "deliveryrating" to selRatingFeedback.deliveryrating.joinToString(separator = ","),
                "comment" to selRatingFeedback.comment,
                "orderId" to selRatingFeedback.orderId,
            )
        )
    }

    fun userReferral(name: String, mobile: String, refferalCode: String) {
        appCleverTap.pushEvent(
            CleverEvents.user_referral, mapOf(
                "user_name" to name,
                "mobile" to mobile,
                "referral_code" to refferalCode,
                "city" to AppGlobalObjects.selectedAddress?.city,
                "locality" to AppGlobalObjects.selectedAddress?.googleLocation
            )
        )
    }

    fun pushSignUpLocation(address: Address?) {
        if (address != null) {
            val addressLine = address.getAddressLine(0) ?: "unknown"
            pushEventMap(
                CleverEvents.signup_location,
                mapOf(

                    "city" to address.locality,
                    "locality" to address.locality,
                    "sub_locality" to address.subLocality,
                    "admin_area" to address.adminArea,
                    "address" to addressLine,
                )
            )
        }

    }

    fun pushInstallLocation(context: Context, latitude: Double, longitude: Double) {
        val address = getAddress(context, latitude, longitude)
        if (address != null) {
            val addressLine = address.getAddressLine(0) ?: "unknown"
            pushEventMap(
                CleverEvents.user_started,
                mapOf(

                    "city" to address.locality,
                    "locality" to address.locality,
                    "sub_locality" to address.subLocality,
                    "admin_area" to address.adminArea,
                    "address" to addressLine,
                )
            )
        }

    }

    private fun getAddress(context: Context, lat: Double, lang: Double): Address? {
        var userAddress: Address? = null
        val gcd = Geocoder(context, Locale("en", "IN"))
        val addressList: List<android.location.Address>?

        try {
            addressList = gcd.getFromLocation(lat, lang, 2)
            if (addressList != null && addressList.isNotEmpty()) {

                val address = addressList[0]
                if (address.locality != null && address.locality != "") {
                    userAddress = address
                } else if (addressList.size > 1 && addressList[1].locality != null && addressList[1].locality != "") {
                    userAddress = addressList[1]

                } else {
                    Handler(Looper.getMainLooper()).postDelayed(
                        { getAddress(context, lat, lang) },
                        200
                    )
                    return null
                }

//                bd.addressText = mSelectedGoogleAddress?.getAddressLine(0)
            }
            return userAddress
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    fun userSignUpEvent(context: Context, user: UserDTO) {
        var latitude = 0.0
        var longitude = 0.0
        var address: Address? = null
        CTGeofenceAPI.getInstance(context)
            .setCtLocationUpdatesListener {
                latitude = it.latitude
                longitude = it.longitude
                address = getAddress(context, latitude, longitude)
                pushSignUpLocation(address)
            }


        val split = user.name.split(" ")
        val first_name = split[0]
        var last_name = ""
        if (split.size > 1) {
            last_name = split[1]
            if (split.size > 2) {
                last_name = last_name + " " + split[2]
            }
        }

        val subLocality = address?.subLocality ?: "unknown"
        val locality = address?.locality ?: "unknown"
        pushEventMap(
            CleverEvents.user_signup, mapOf(
                "userid" to user.userId,
                "first_name" to first_name,
                "last_name" to last_name,
                "email" to user.email,
                "mobile" to user.mobile,
                "city" to subLocality,
                "locality" to locality,
                "lat" to latitude,
                "long" to longitude,
            )
        )

    }

    fun deniedPermission(permission: String?) {
        val vPermission = permission ?: "unknown"
        appCleverTap.pushEvent(
            CleverEvents.app_permission, mapOf(
                CleverEvents.user_permission to vPermission
            )
        )

    }

}
