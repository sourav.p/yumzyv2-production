package com.yumzy.orderfood.ui.screens.module.address

import android.content.DialogInterface
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.libraries.places.api.net.PlacesClient
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogDeliveryLocationBinding
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.ui.component.AddressCardView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class SelectDeliveryLocationDialog :
    BaseSheetFragment<DialogDeliveryLocationBinding, SelectDeliveryLocationViewModel>() {
    private lateinit var placesClient: PlacesClient

    
    override val vm: SelectDeliveryLocationViewModel by viewModel()

    override fun getLayoutId() = R.layout.dialog_delivery_location


    private fun setupSaveAddress() {

        vm.getAddressList().observe(
            viewLifecycleOwner, {
                RHandler<List<AddressDTO>>(this, it) { addressList ->
                    if (!addressList.isNullOrEmpty()) {
                        setSavedAddresses(addressList)
                    } else {
                        setNoAddressView()
                    }
                }
            })
    }

    private fun setNoAddressView() {
        val noAddressLayout = UIHelper.showMessageWithImage(
            requireContext(),
            R.drawable.ic_placeholder,
            "\nNo Saved Addresses!!\nPlease Click To Add New Address"
        )
        val layoutParams = ConstraintLayout.LayoutParams(
            0,
            ViewConst.MATCH_PARENT

        )

        layoutParams.topToBottom = R.id.section_save_address
        layoutParams.startToStart = R.id.section_save_address
        layoutParams.endToEnd = R.id.section_save_address
        noAddressLayout.layoutParams = layoutParams
        noAddressLayout.setOnClickListener {
            addressPicker()
            dismiss()
        }

        bd.addressContainer.addView(noAddressLayout)
    }

    private fun setSavedAddresses(addressList: List<AddressDTO>) {
        bd.sectionSaveAddress.setUpList(
            "Your Addresses ",
            addressList,
            fun(value: AddressDTO): AddressCardView {
                val addressCardView = AddressCardView(context)
                addressCardView.getFontIconView().text =
                    context?.resources?.getString(R.string.icon_location_point)
                addressCardView.getTitleView().text = value.name
                addressCardView.getSubTitleView().text =
                    "${value.houseNum}, ${value.landmark}, ${value.fullAddress}"

                addressCardView.setClickScaleEffect()
                addressCardView.setOnClickListener { onItemSelected(value) }
                return addressCardView
            }
        )
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            val noAddressLayout = UIHelper.showMessageWithImage(
                requireContext(),
                R.drawable.ic_internet,
                IConstants.OrderTracking.NO_INTERNET + "\n" + IConstants.OrderTracking.NO_INTERNET_MSG
            )
            val layoutParams = ConstraintLayout.LayoutParams(
                0,
                ViewConst.MATCH_PARENT

            )

            layoutParams.topToBottom = R.id.section_save_address
            layoutParams.startToStart = R.id.section_save_address
            layoutParams.endToEnd = R.id.section_save_address
            noAddressLayout.layoutParams = layoutParams
            noAddressLayout.setOnClickListener {
                addressPicker()
                dismiss()
            }

            bd.addressContainer.addView(noAddressLayout)


        }
    }

    private fun onItemSelected(value: AddressDTO) {
        actionHandler?.onSuccess(
            actionId = moduleId,
            data = value
        )
        dialog?.dismiss()
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        when (actionId) {
            IConstants.ActionModule.CONFIRM_LOCATION -> {
                data as AddressDTO
                showToast(data.fullAddress)
            }
        }
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_back -> dialog?.dismiss()
            R.id.container_current_location -> {
                /*   actionHandler?.onSuccess(
                       IConstants.ActionModule.ADD_ADDRESS,
                       IConstants.ManagerAddress.UPDATE_AUTOMATICALLY
                   )*/
                addressPicker()
                dialog?.dismiss()

            }
            /*   R.id.search_container -> {
                   val fields =
                       Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
                   val intent = Autocomplete.IntentBuilder(
                       AutocompleteActivityMode.FULLSCREEN,
                       fields
                   ).setCountry("IN").setTypeFilter(TypeFilter.ESTABLISHMENT).build(requireActivity())
                   (requireActivity() as HomePageActivity).startActivityForResult(
                       intent,
                       IConstants.ActivitiesFlags.AUTOCOMPLETE_REQUEST_CODE
                   )
               }*/
        }

    }

    private fun addressPicker() {
        /*val intent = Intent(context, LocationPickerActivity::class.java)
        (context as AppCompatActivity).startActivityForResult(
            intent,
            IConstants.ActivitiesFlags.ADDRESS_PICKER_REQUEST

        )*/
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        actionHandler?.onSuccess(
            actionId = IConstants.ManagerAddress.CLOSE_DIALOG,
            data = null
        )
    }


    override fun onDialogCreated(view: View) {
        /*if (!Places.isInitialized()) {
            Places.initialize(requireContext(), IConstants.Map.GOOGLE_PLACES_KEY)
        }*/
//        placesClient = Places.createClient(requireContext())

        //setup current location
//        bd.containerCurrentLocation.background = DrawableBuilder()
//            .rectangle()
//            .hairlineBordered()
//            .strokeWidth(2)
//            .cornerRadius(6)
//            .rippleColor(ThemeConstant.lightAlphaGray)
//            .strokeColor(ThemeConstant.lightAlphaGray)
//            .strokeColorPressed(ThemeConstant.lightAlphaGray)
//            .ripple()
//            .build()


        /* (activity as AppCompatActivity).run {
             setSupportActionBar(bd.addressToolbar)
             supportActionBar?.setDisplayHomeAsUpEnabled(true)
             supportActionBar?.setHomeButtonEnabled(true)
         }
         bd.addressToolbar.setNavigationOnClickListener { this.dismiss() }
         bd.appBarLayout2.stateListAnimator = AnimatorInflater.loadStateListAnimator(
             context,
             R.animator.app_bar_elevation
         )*/
//        ViewCompat.setElevation(appBarLayout, getResources().getDimension(R.dimen.toolbar_elevation));
        setupSaveAddress()
        applyDebouchingClickListener(
            bd.containerCurrentLocation
        )

    }
    /*  fun setPlace(place: Place) {
          val address = AddressDTO()
          address.name = place.name.toString()
          address.fullAddress = place.address.toString()
          address.googleLocation = address.fullAddress.split(" ")[0] ?: "Other"
          address.latLong = LatLong().apply {
              coordinates = place.latLng?.let { listOf(it.latitude, it.longitude) }
          }
          onItemSelected(address)
      }*/

}