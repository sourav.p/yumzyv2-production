package com.yumzy.orderfood.ui.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.api.session.SessionManager
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.templetes.InfoTemplate
import com.yumzy.orderfood.util.IConstants
import org.koin.android.ext.android.inject


abstract class BaseFragment<B : ViewDataBinding, T : BaseViewModel<*>> : Fragment(), IView {

    abstract val vm: T
    open lateinit var bd: B

    private var baseActivity: BaseActivity<*, *>? = null
    private var fragmentView: View? = null

    private val mClickListener =
        View.OnClickListener { v -> onDebounceClick(v) }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun onFragmentReady(view: View)

    open val sessionManager: SessionManager by inject()

    open val sharedPrefsHelper: SharedPrefsHelper by inject()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            baseActivity = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (fragmentView == null) {
            bd = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
            bd.lifecycleOwner = this
            fragmentView = bd.root
        }

        return fragmentView
    }

    /*
        override fun setUserVisibleHint(isVisibleToUser: Boolean) {
            super.setUserVisibleHint(isVisibleToUser)
        }*/
    fun applyDebouchingClickListener(vararg views: View?) {
        ClickUtils.applySingleDebouncing(views, 300, mClickListener)
        ClickUtils.applyPressedViewScale(*views)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onFragmentReady(view)
//        val navController = findNavController();
//        // We use a String here, but any type that can be put in a Bundle is supported
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("key")?.observe(
//            viewLifecycleOwner
//        ) { result ->
//            // Do something with the result.
//        }
    }


    override fun showProgressBar() {
        baseActivity?.showProgressBar()
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun hideProgressBar() {
        baseActivity?.hideProgressBar()
    }

    open fun showError(error: String) {
        baseActivity?.showSnackBar(error, 0)
    }

    open fun showSnackBar(message: String, statusColor: Int) {
        baseActivity?.showSnackBar(message, statusColor)

    }

    open fun showToast(message: String) {
        baseActivity?.showToast(message)
    }

    open fun onDebounceClick(view: View) {

    }

    override fun onResponse(code: Int?, response: Any?) {

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        /*val codeColor = CodeConstant.COLOR_CODE.getCodeColor(code)
        if (codeColor == null) {
            showToast(message)
            return
        }*/
        when (code) {
            APIConstant.Status.NO_NETWORK -> {
//                ActionModuleHandler.showAnimSizedDialog(
//                    requireContext(),
//                    IConstants.LottieImage.NO_INTERNET,
//                    "No Internet Connection",
//                    error,true
//                )
                showSnackBar(IConstants.ResponseError.NO_INTERNET, Color.BLACK)
            }
            APIConstant.Status.SESSION_EXPIRED -> {
                val dialog = DetailActionDialog()
                dialog.detailsTemplate = InfoTemplate(
                    requireContext(),

                    R.string.icon_chili_pepper,
                    getString(R.string.please_login_again),
                    getString(R.string.your_session_expired)
                )

                dialog.show((context as AppCompatActivity).supportFragmentManager, "$code")
            }
            else -> {
                showError(message)
            }
        }
    }


}