package com.yumzy.orderfood.ui.screens.orderhistory

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository

class OrderHistoryViewModel  constructor(
    private val repository: OrderRepository
) : BaseViewModel<IOrderHistory>() {
    fun getOrderHistoryList(skip: Int) = repository.getOrderHistoryList(skip)

}