package com.yumzy.orderfood.ui.screens.invite.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.models.ContactModelDTO
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

class InviteRepository  constructor(
    private val remoteSource: InviteRemoteDataSource
) {

    fun profileUser() = networkLiveData<Any>(
        networkCall = {

            remoteSource.profileUser()
        }
    ).distinctUntilChanged()

    fun saveUserContacts(userPhoneNumber: String, contacts: List<ContactModelDTO>) = networkLiveData<Any>(
        networkCall = {
            remoteSource.saveUserContacts(userPhoneNumber, contacts)
        }
    )
}