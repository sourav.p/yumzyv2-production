package com.yumzy.orderfood.ui.component.coupon

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutApplyCouponBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant

class ApplyCouponView : FrameLayout {

    var appliedCode: CharSequence? = null
        set(value) {
            field = value
            if (field == null || field!!.isEmpty()) {
                binding.couponCode = ""
                binding.fontIcon = resources.getString(R.string.icon_angle_right_b1)
                binding.fontIconColor = ThemeConstant.textGrayColor
                setCouponClick()


            } else {
                binding.couponCode = appliedCode.toString()
                binding.fontIcon = resources.getString(R.string.icon_error)
                binding.fontIconColor = ThemeConstant.pinkies
                removeCouponClick()

                // binding.couponCode = resources.getString(R.string.coupon_applied)+"("+appliedCode+")"
                //  binding.tvAppliedMsg.setTextColor(ThemeConstant.greenConfirmColor)


//                binding.tvCouponCode.invisibleEmptyTextView(field)
            }

        }
    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutApplyCouponBinding.inflate(layoutInflater, this, true)
    }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.ApplyCouponView,
                    defStyleAttr,
                    0
                )


            appliedCode = a.getString(R.styleable.ApplyCouponView_couponAppliedCode) ?: ""
            a.recycle()

        }

        setCouponClick()
        binding.iconAction.setOnClickListener {
            if (!appliedCode.isNullOrEmpty()) {
                couponRemove?.invoke(it)
            } else {
                if (appliedCode.isNullOrEmpty()) {
                    addCoupon?.invoke(it)
                }
            }
        }
    }

    private fun setCouponClick() {
        this.setOnClickListener {
            if (appliedCode.isNullOrEmpty()) {
                addCoupon?.invoke(it)
            }
        }
    }

    private fun removeCouponClick() {
        this.setOnClickListener(null)
    }

    var couponRemove: ((view: View) -> Unit)? = null
    var addCoupon: ((view: View) -> Unit)? = null


}