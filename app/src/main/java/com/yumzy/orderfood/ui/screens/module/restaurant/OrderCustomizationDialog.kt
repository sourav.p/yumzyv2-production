package com.yumzy.orderfood.ui.screens.module.restaurant

import android.content.DialogInterface
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.databinding.DialogCutomizeOrderBinding
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.ui.common.IOrderCustomizationHandler
import com.yumzy.orderfood.ui.component.OrderCustomizationView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class OrderCustomizationDialog :
    BaseSheetFragment<DialogCutomizeOrderBinding, OrderCustomizationViewModel>() {

    internal lateinit var mAddOnDTOList: ArrayList<RestaurantAddOnDTO>
    
    override val vm: OrderCustomizationViewModel by viewModel()

    lateinit var mCustomizationActionHandler: IOrderCustomizationHandler

    internal var dishName: String = ""

    internal var selectedVariant: Variant? = null
    internal var selectedAddOnList = ArrayList<Variant>()

    internal var selectionMap: MutableMap<String, List<CustomizeOrderItemDTO>> = mutableMapOf()
    internal var itemPrice: Double = 0.0
    internal var restaurantItem: RestaurantItem? = null

    var total = 0.0
    var itemCount = 0
    var selectedItemList = ""

    override fun getLayoutId(): Int = R.layout.dialog_cutomize_order

    override fun onDialogCreated(view: View) {
        /*bd.scrollingAddons.viewTreeObserver.addOnScrollChangedListener {
            bd.constraintLayout4.isSelected = bd.scrollingAddons.canScrollVertically(-1)
        }*/
        val split = dishName.split(" ")
        split[0].capitalize()
        bd.tvDishName.text = split.joinToString(" ")

        val customizationList = mutableListOf<CustomizeOrderSectionDTO>()

        var restaurantVariantDTO: RestaurantAddOnDTO? = null
        var variantIndex = -1
        for ((index, value) in mAddOnDTOList.withIndex()) {
            if (value.isVariant) {
                variantIndex = index
                restaurantVariantDTO = value
                mAddOnDTOList.removeAt(index)
                break
            }
        }
        if (variantIndex != -1) {
            mAddOnDTOList.add(0, restaurantVariantDTO!!)
        }

        for (addOn in mAddOnDTOList) {
            val items = mutableListOf<CustomizeOrderItemDTO>()

            addOn.options?.forEach { option ->
                var offerPrice = 0.0
                if (addOn.isVariant) {

                    val zoneOffer =
                        YumUtil.setRecZoneOfferString(restaurantItem?.offers, option.cost)
                    if (zoneOffer != null) {
                        offerPrice = zoneOffer.second
                    }
                }

                items.add(
                    CustomizeOrderItemDTO(
                        option.optionName,
                        option.isVeg,
                        option.cost,
                        offerPrice,
                        addOn.addonId,
                        option.isSelected
                    ).run {
                        iconText = requireContext().resources.getString(R.string.icon_veg_non_veg)
                        return@run this
                    }
                )
            }

            customizationList.add(
                CustomizeOrderSectionDTO(
                    addOn.displayName,
                    !addOn.isVariant /*|| !(addOn.minSelection == 1 && addOn.maxSelection == 1)*/,
                    items,
                    addOn.addonId,
                    addOn.addonName,
                    addOn.minSelection,
                    addOn.maxSelection,
                    addOn.doInitialSelection
                )
            )
        }



        for (customizeSection in customizationList) {
            val sectionTitle = getSectionTitleView(customizeSection)
            val checkBoxMultiSelectionView =
                OrderCustomizationView(requireContext()).withAddonSection(
                    object : CustomizationChangeListener {
                        override fun onCustomisationChange(
                            addonId: String,
                            selectedList: List<CustomizeOrderItemDTO>
                        ) {
                            updateUI(addonId, selectedList)
                        }
                    },
                    customizeSection
                )
            bd.customizeContainer.addView(sectionTitle)
            bd.customizeContainer.addView(checkBoxMultiSelectionView)
        }

        applyDebouchingClickListener(bd.btnAdd)

    }


    private fun getSectionTitleView(section: CustomizeOrderSectionDTO): View {
        val titleContainer = LinearLayout(context).run {
            layoutParams = LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
            orientation = LinearLayout.HORIZONTAL


            val titleTextView = TextView(context)
            titleTextView.setPadding(UIHelper.i8px, UIHelper.i8px, UIHelper.i8px, UIHelper.i8px)
            titleTextView.setInterBoldFont()
            titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            titleTextView.text = section.menuSectionTitile.capitalize()
            titleTextView.setTextColor(ThemeConstant.textBlackColor)

            val optionTextView = TextView(context)
            optionTextView.setTextColor(ThemeConstant.alphaBlackColor)
            optionTextView.setPadding(UIHelper.i4px, UIHelper.i4px, UIHelper.i4px, UIHelper.i4px)
            optionTextView.setRalewayMediumFont()
            optionTextView.setTextColor(ThemeConstant.textGrayColor)
            optionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
            val displayString = "";

            val minSelection = section.min
            val maxSelection = section.max
            optionTextView.text = generateAddonsString(minSelection, maxSelection)
            /*when {
       *//* section.min == 0 -> {
                    if(section.max == 1) {
                        "( Select one add-on )"
                    }else {
                        "( Select up to ${section.max.getWordCount()} add-ons )"
                    }
                }

                section.min == 1 && section.max == 1 -> {
                    "( Select any one option )"
                }

                else -> {
                    "( Select ${section.min.getWordCount()} to ${section.max.getWordCount()} add-on(s) )"
                }*//*
                if (minSelection === 0) {
                    when (maxSelection) {
                        case 1:
                        displayString = "Select one add-on:";
                        break;
                        default:
                        displayString = "Select up to ${maxSelection} add-ons:";
                    }
                } else if (minSelection === maxSelection) {
                    displayString = `Select any ${minSelection} option:`;
                } else {
                    displayString = `Select ${minSelection} to ${maxSelection} add-on(s)`;
                }
                    return displayString;
            }*/

            this.addView(titleTextView)
            this.addView(optionTextView)
            return@run this
        }
        return titleContainer
    }

    private fun generateAddonsString(minSelection: Int? = 0, maxSelection: Int? = 0): String {
        var displayString = "";
        if (minSelection == 0) {
            when (maxSelection) {
                1 -> displayString = "Select one add-on:";
                else -> displayString = "Select up to ${maxSelection} add-ons:";
            }
        } else if (minSelection == maxSelection) {
            displayString = "Select any ${minSelection} option:";
        } else {
            displayString = "Select ${minSelection} to ${maxSelection} add-on(s)";
        }
        return displayString;
    }

    private fun updateUI(addonId: String, selectedList: List<CustomizeOrderItemDTO>) {

        selectionMap.put(addonId, selectedList)

        total = 0.0
        itemCount = 0
        selectedItemList = ""

        for ((key, value) in selectionMap) {
            value.forEach { item ->
                total += if (item.itemOfferPrice != 0.0) item.itemOfferPrice else item.itemPrice
                selectedItemList += if (selectedItemList.isNotEmpty()) ", ${item.itemName.capitalize()}" else item.itemName.capitalize()
                itemCount++
            }
        }

        var itemCountTotol =
            if (itemCount > 0) " $itemCount ${
                if (itemCount > 1) "Items (Customization Total: ₹${
                    total.withPrecision(
                        2
                    )
                })" else "Item (Customization Total: ₹${total.withPrecision(2)})"
            } " else ""

        bd.tvSelectedItemCount.text = itemCountTotol
        bd.tvSelectedItems.text = selectedItemList
        bd.btnAdd.text = "ADD"

    }

    var dissmisType = 2

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_add -> {

                for (i in 0 until bd.customizeContainer.childCount) {
                    val child = bd.customizeContainer.getChildAt(i)
                    if (child is OrderCustomizationView) {
                        val selectedOptionsPair = child.getSelectedOptions() ?: continue
                        val variant = Variant(
                            child.section?.addOnId.toString(),
                            child.section?.addOnName.toString(),
                            selectedOptionsPair.second,
                            selectedOptionsPair.first,
                            selectedOptionsPair.second.size
                        )
                        if (child.section?.isMultiSelectEnable == true) {
                            selectedAddOnList.add(variant)
                        } else {
                            selectedVariant = variant
                        }
                    }
                }
                mCustomizationActionHandler.onSuccess(selectedVariant, selectedAddOnList)
                dissmisType = 1
                dialog?.dismiss()
                //1 for add
                //2 for back or user closed
            }
        }

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mCustomizationActionHandler.onDismiss(dissmisType)

    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }


    interface CustomizationChangeListener {
        fun onCustomisationChange(
            addonId: String,
            selectedList: List<CustomizeOrderItemDTO>
        )
    }

}