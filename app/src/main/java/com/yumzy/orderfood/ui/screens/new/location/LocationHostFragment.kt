package com.yumzy.orderfood.ui.screens.new.location

import android.app.Dialog
import android.view.View
import androidx.navigation.fragment.findNavController
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentLaunchBinding
import com.yumzy.orderfood.databinding.FragmentLocationHostBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LocationHostFragment : BaseFragment<FragmentLocationHostBinding, LoginViewModel>(), ILoginView {

    override val vm: LoginViewModel by viewModel()

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_location_host

    override fun onFragmentReady(view: View) {
        vm.attachView(this)



    }

    override fun navHomeActivity() {

    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {

    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {

    }
}