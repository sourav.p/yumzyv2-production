package com.yumzy.orderfood.ui.screens.restaurant

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.RestaurantMenuCategoryDTO
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.ViewConst

/**
 * Created by Bhupendra Kumar Sahu.
 */
class AlertFilterMenuAdapter(val context: Context) :
    RecyclerView.Adapter<AlertFilterMenuAdapter.MyViewHolder>() {
    var filerList: List<RestaurantMenuCategoryDTO> = mutableListOf()
    private var selectedItem: Int = -1
    var callback: RecyclerviewCallbacks<RestaurantMenuCategoryDTO>? = null
    fun addAlertFilter(filers: List<RestaurantMenuCategoryDTO>) {
        filerList = filers.toMutableList()
        notifyDataSetChanged()
    }

    fun selectedItem(position: Int) {
        selectedItem = position
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = filerList[position]
        (holder.item as LinearLayout).let {
            val nameTextView = it.getChildAt(0) as TextView
            nameTextView.text = item.key

            val countTextView = it.getChildAt(1) as TextView
            countTextView.text = item.value.toString()

            if (position == selectedItem) {
                nameTextView.setTextColor(ContextCompat.getColor(context, R.color.colorBlue))
            }

        }
    }

    fun setOnClick(click: RecyclerviewCallbacks<RestaurantMenuCategoryDTO>) {
        callback = click
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view = LinearLayout(context).apply {
            layoutParams = LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
            setPadding(UIHelper.i8px,UIHelper.i8px,UIHelper.i8px,UIHelper.i8px)
            gravity = Gravity.CENTER_VERTICAL
            this.addView(TextView(context).apply {
                setInterFont()
                setTextColor(ThemeConstant.textBlackColor)
                layoutParams = LinearLayout.LayoutParams(0, ViewConst.WRAP_CONTENT, 1f)
            })
            this.addView(TextView(context).apply {
                setInterFont()
                setTextColor(ThemeConstant.textBlackColor)
                layoutParams = LinearLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
            })
        }
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filerList.size
    }

    inner class MyViewHolder(val item: View) : RecyclerView.ViewHolder(item) {

        init {
            setClickListener(item)
        }

        fun setClickListener(view: View) {
            view.setOnClickListener {
                callback?.onItemClick(it, adapterPosition, filerList[adapterPosition])
            }
        }
    }
}