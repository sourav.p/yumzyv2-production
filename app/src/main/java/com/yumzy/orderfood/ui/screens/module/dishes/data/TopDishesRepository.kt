package com.yumzy.orderfood.ui.screens.module.dishes.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.data.models.ClearCartReqDTO
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class TopDishesRepository  constructor(
    private val remoteSource: TopDishesRemoteDataSource
) {
    fun getTopDishes(promoID: String, skip: Int, limit: Int) = networkLiveData(
        networkCall = {
            val lng = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(0).toString()
            val lat = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(1).toString()
            remoteSource.getTopDishes(promoID, lat, lng, skip, limit)
        }
    ).distinctUntilChanged()


    fun addToCart(addCart: NewAddItemDTO) = networkLiveData(
        networkCall = {
            remoteSource.addToCart(addCart)
        }
    ).distinctUntilChanged()

    fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = networkLiveData<Any>(
        networkCall = {

            remoteSource.itemCount(position, count, itemCount)
        }
    ).distinctUntilChanged()

    fun getCart(
        cartId: String
    ) = networkLiveData<Any>(
        networkCall = {
            remoteSource.getCart(cartId)
        }
    )

    fun clearCart(cartId: ClearCartReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteSource.clearCart(cartId)
        }
    ).distinctUntilChanged()
}