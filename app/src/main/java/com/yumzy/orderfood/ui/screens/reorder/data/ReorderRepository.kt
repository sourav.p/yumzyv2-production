package com.yumzy.orderfood.ui.screens.reorder.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ReorderRepository  constructor(
    private val remoteSource: ReorderRemoteDataSource
) {
    fun orderDetails(orderId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.orderDetails(orderId)
        }
    ).distinctUntilChanged()
}