package com.yumzy.orderfood.ui.screens.module.dishes.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.data.models.ClearCartReqDTO
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class TopDishesRemoteDataSource  constructor(private val serviceAPI: HomeServiceAPI) :
    BaseDataSource() {
    suspend fun getTopDishes(
        promoID: String, lat: String,
        lng: String, skip: Int, limit: Int
    ) = getResult {
        serviceAPI.getTopDishes(promoID, lat, lng, skip, limit)
    }

    suspend fun addToCart(addCart: NewAddItemDTO) = getResult {
        serviceAPI.addToCart(addCart)
    }

    suspend fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = getResult {
        serviceAPI.itemCount(position, count, itemCount)
    }

    suspend fun getCart(cartId: String) = getResult {
        serviceAPI.getCart(cartId)
    }


    suspend fun clearCart(cartId: ClearCartReqDTO) = getResult {
        serviceAPI.clearCart(cartId)
    }
}