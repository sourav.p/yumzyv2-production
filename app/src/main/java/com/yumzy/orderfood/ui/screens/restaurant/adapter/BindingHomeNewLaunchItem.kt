package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterHomeNewLaunchBinding
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

class BindingHomeNewLaunchItem(private val item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterHomeNewLaunchBinding>(item) {

//    private var item: HomeListDTO? = null

    override val type: Int = R.id.home_new_launch_item

    /*fun withNewLaunchItem(item: HomeListDTO): BindingHomeNewLaunchItem {
        this.item = item
        return this
    }*/


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterHomeNewLaunchBinding {
        return AdapterHomeNewLaunchBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterHomeNewLaunchBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.materialCardview.radius = UIHelper.cornerRadius
        binding.title = item.title.capitalize(Locale.getDefault())
        binding.location = item.subTitle.toString()
        binding.cuisines =
            item.cuisines?.joinToString(separator = ", ", postfix = "") ?: "New Arrivals"
        val zoneOffer = YumUtil.setZoneOfferString(item.meta?.offers)
        binding.offer = zoneOffer

//        binding.isVeg = homeList?.meta?.isVeg ?: true
        val ratting = item.rating?.toString() ?: ""
        if (ratting.isNotEmpty() && !ratting.equals("null")) {
            binding.rating = ratting
        }


        binding.duration = YumUtil.getDurationAndCostForTwo(item)

        binding.imageUrl = model.heroImage?.url ?: model.image?.url ?: ""
    }

    override fun unbindView(binding: AdapterHomeNewLaunchBinding) {
        binding.title = null
        binding.location = null
        binding.cuisines = null
        binding.offer = null
        binding.rating = null
        binding.duration = null
    }


    class HomeNewLaunchClickEvent(val callBack: (item: HomeListDTO, view: View) -> Unit) :
        ClickEventHook<BindingHomeNewLaunchItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return viewHolder.itemView
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<BindingHomeNewLaunchItem>,
            item: BindingHomeNewLaunchItem
        ) {
            callBack(item.item, v)
        }
    }
}