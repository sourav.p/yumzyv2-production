package com.yumzy.orderfood.ui.screens

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ActivityTestBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.screens.home.HomePageViewModel
import com.yumzy.orderfood.ui.screens.home.IHomePageView
import org.koin.android.ext.android.inject

class TestActivity : BaseActivity<ActivityTestBinding, HomePageViewModel>(), IHomePageView {

    override val vm: HomePageViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_test)
    }

    override fun displayNewEvent() {
        
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return false
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
        
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
        
    }

    override fun onUpdateNeeded(updateUrl: String?) {
        
    }
}