/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.helpfaq.faq

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HelpSubHeadingDTO
import com.yumzy.orderfood.data.models.SubHeadingDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityHelpSubheadingBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class HelpSubheadingActivity :
    BaseActivity<ActivityHelpSubheadingBinding, HelpSubHeadingViewModel>(),
    IHelpAndSupport {

    private var mSubHeadingId = -1

    override val vm: HelpSubHeadingViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_help_subheading)

        vm.attachView(this)


        setSupportActionBar(bd.tbHelp)
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        intent?.let { mSubHeadingId = it.getIntExtra(IConstants.Help.INTENT_HELP, 1) } ?: finish()

        getSubheadings()

    }

    override fun showZohoChatButton(isVisible: Boolean) {
        super.showZohoChatButton(true)
    }

    private fun getSubheadings() {

        vm.getHelpSubheadings(mSubHeadingId).observe(this, { result ->
            RHandler<SubHeadingDTO>(
                this,
                result
            ) {
                val data = result?.data
                supportActionBar?.title = data?.title

                setUpExpandList(data?.subItems)
            }
        })

    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }


    private fun setUpExpandList(items: List<HelpSubHeadingDTO>?) {

        val map = mutableMapOf<String, Any>()
        items?.forEachIndexed { index, helpSubHeadingDTO ->
            map[helpSubHeadingDTO.question] = helpSubHeadingDTO

        }

        bd.expandListFaq.setupList(
            map,

            fun(data: Any): View { return getExpandView(data as HelpSubHeadingDTO) }
        )

    }


    private fun getExpandView(data: HelpSubHeadingDTO): View {
        return when (mSubHeadingId) {
            1 -> getExpandTextView(data)
            2 -> getExpandTextView(data)
            3 -> getExpandTextView(data)
            4 -> getExpandTextView(data)
            5 -> getExpandTextView(data)
            else -> getExpandTextView(data)
        }
    }


    private fun getExpandTextView(data: HelpSubHeadingDTO): TextView {
        return MaterialTextView(this).apply {
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            text = data.answer
            setTextColor(ThemeConstant.textDarkGrayColor)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
            } else {
                setTextAppearance(context, R.style.TextAppearance_MyTheme_SemiBold)
            }
            background = DrawableUtils.getRoundDrawable(ThemeConstant.lightAlphaGray, 10f)
            setPadding(VUtil.dpToPx(12), VUtil.dpToPx(12), VUtil.dpToPx(12), VUtil.dpToPx(12))
            setLineSpacing(4f, 1f)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        vm.detachView()
    }

    companion object

}