package com.yumzy.orderfood.ui.screens.profile.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.updateDatabase
import javax.inject.Inject

class ProfileRemoteDataSource  constructor(
    private val serviceAPI: ProfileServiceAPI,
    private val selectedAddressDao: SelectedAddressDao? = null,
) :
    BaseDataSource() {

    suspend fun logoutUser() = getResult {
        serviceAPI.logoutUser()
    }

    suspend fun profileUser() = getResult {
        serviceAPI.profileUser()
    }

    fun removeSelectedAddress() = updateDatabase {
        selectedAddressDao?.removePickedAddress()
    }

    suspend fun updateProfileUser(map: Map<String, String>) = getResult {
        serviceAPI.updateProfileUser("update", map)
    }

    suspend fun userUpdateProfileImage(pro: MutableMap<String, Any>) = getResult {
        serviceAPI.userUpdateProfileImage("update", pro)
    }

    /*
     * Address related queries.
     */
    suspend fun getAddressList() = getResult {
        serviceAPI.getAddressList()
    }

    suspend fun updateAddress(address: AddressDTO) = getResult {
        serviceAPI.updateAddress(address)
    }

    suspend fun addAddress(address: AddressDTO) = getResult {
        serviceAPI.addAddress(address)
    }

    suspend fun deleteAddress(addressId: String) = getResult {
        serviceAPI.deleteAddress(addressId)
    }

    suspend fun getHelpHeadings() = getResult {
        serviceAPI.getHelpHeadings()
    }

    suspend fun getHelpSubheadings(headingId: Int) = getResult {
        serviceAPI.getHelpSubheadings(headingId)
    }

    suspend fun getWalletTransactions(skip: Int, limit: Int, mobile: Long) = getResult {
        serviceAPI.getWalletTransactions(skip, limit, mobile)
    }

    suspend fun getUserWallet(mobile: Long) = getResult {
        serviceAPI.getUserWallet(mobile)
    }

    suspend fun verifyVPA(vpa: String) = getResult {
        serviceAPI.verifyVPA(vpa)
    }

    suspend fun withdrawBalance(withDraw: MutableMap<String, Any>): ResponseDTO<Any> = getResult {
        serviceAPI.withdrawBalance(withDraw)
    }

}