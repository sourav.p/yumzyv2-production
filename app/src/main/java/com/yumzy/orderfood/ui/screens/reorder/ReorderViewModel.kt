package com.yumzy.orderfood.ui.screens.reorder

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ReorderViewModel  constructor(
    private val repository: OrderRepository
) : BaseViewModel<IReorder>() {
    fun orderDetails(orderId: String) = repository.getOrderDetails(orderId)
}