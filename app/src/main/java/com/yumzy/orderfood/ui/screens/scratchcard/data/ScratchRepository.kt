package com.yumzy.orderfood.ui.screens.scratchcard.data

import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

class ScratchRepository  constructor(
    private val remoteSource: ScratchDataSource
) {
    fun fetchScratchCardList(skip: Int, limit: Int) = networkLiveData(
        networkCall = {
            remoteSource.fetchScratchCardList(skip, limit)
        }
    )

    fun getScratchCardDetails() = networkLiveData(
        networkCall = {
            remoteSource.getScratchDetails()
        }
    )

    fun redeemScratchCard(scratchCard: ScratchCardDTO, phone: String?) = networkLiveData(
        networkCall = {
            remoteSource.redeemScratchCard(scratchCard, phone)
        }
    )
}