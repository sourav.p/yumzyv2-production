package com.yumzy.orderfood.ui.screens.login

import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler

interface ILoginView : IView, IModuleHandler, ConfirmActionDialog.OnActionPerformed {
    fun navHomeActivity()
}