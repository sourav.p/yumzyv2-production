package com.yumzy.orderfood.ui.screens.rewards

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ScratchSummaryDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityMyRewardsBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.setLightStatusBar
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardActivity
import com.yumzy.orderfood.ui.screens.wallet.WalletActivity
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class RewardsActivity : BaseActivity<ActivityMyRewardsBinding, RewardsViewModel>(),
    IMyRewards {
    
     override val vm: RewardsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_my_rewards)
        vm.attachView(this)

//        setSupportActionBar(bd.tbMyRewards)
        setSupportActionBar(bd.mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        setLightStatusBar(this, Color.WHITE)
        val typeface = ResourcesCompat.getFont(this, R.font.poppins_medium)
        bd.mainCollapsing.setCollapsedTitleTypeface(typeface)
        bd.mainCollapsing.title = resources.getString(R.string.rewards)
        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarBlackText)
        //  bd.headerTitle.text = "EAT & EARN"
        //  bd.headerSubtitle.text = "Your \nReward"

        bd.viewEarnReward.cardElevation = 8f
        bd.viewWalletCard.cardElevation = 8f

        applyDebouchingClickListener(
            bd.viewWalletCard,
            bd.viewEarnReward,
        )
        bd.inviteButton.setOnClickListener { onDebounceClick(it) }
        // bd.inviteLayout.setOnClickListener { onDebounceClick(it) }

        getScratchCardSummary()
        animateHeader()


    }

    private fun animateHeader() {
        Handler(baseContext.mainLooper).postDelayed({
//            bd.contentLayout.background = ShapeHelper.deformBubbleDrawable(
//                color1 = 0xffffd15c.toInt(),
//                color2 = 0xfff8b64c.toInt(),
//                shadowColor = 0x80ffff91.toInt()
//            )

//            bd.inviteLayout.background = ShapeHelper.deformBubbleRightBottom(
//                color1 = 0xffffd15c.toInt(),
//                color2 = 0xfff8b64c.toInt(),
//                shadowColor = 0x80ffff91.toInt()
//            )
//            UIHelper.revealInvisibleView(
//                bd.includeBubbleDrawable,
//                0,
//                bd.includeBubbleDrawable.height
//            )
//            UIHelper.revealInvisibleView(
//                bd.inviteLayout,
//                bd.inviteLayout.width,
//                bd.inviteLayout.height
//            )

            /*bd.blobTitleStyleDTO = YumUtil.getBlobStyleObject(
                resources.getString(R.string.n_my_rewards),
                ThemeConstant.white,
                R.style.TextAppearance_MyTheme_Headline4_Black,
                true,
                3, ThemeConstant.alphaBlackColor
            )

            bd.blobHeaderStyleDTO = YumUtil.getBlobStyleObject(
                resources.getString(R.string.eat_earn),
                ThemeConstant.white,
                R.style.TextAppearance_MyTheme_Headline6_Black,
                true,
                2,
                ThemeConstant.alphaBlackColor
            )*/
        }, 700L)
    }

    override fun onBackPressed() {
        // UIHelper.hideVisibleView(bd.includeBubbleDrawable)
//        UIHelper.hideVisibleView(
//            bd.inviteLayout,
//            bd.inviteLayout.width,
//            bd.inviteLayout.height
//        )
        super.onBackPressed()
    }

    private fun getScratchCardSummary() {
        vm.getScratchCardInfo().observe(this, { result ->
            if (result.type == -1) return@observe
            RHandler<ScratchSummaryDTO>(this, result) {
                if (it.unscratched != 0) {
                    bd.scratchCardCount.visibility = View.VISIBLE
                    bd.scratchCardCount.text = it.unscratched.toString()
                    YumUtil.animateSizeScale(bd.scratchCardCount, 2000, 6)
                } else
                    bd.scratchCardCount.visibility = View.GONE
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override val enableEdgeToEdge: Boolean =true
    override val navigationPadding: Boolean = true
    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.view_wallet_card -> {
                AppIntentUtil.launchWithBaseAnimation(view, this, WalletActivity::class.java)
            }
            R.id.view_earn_reward -> {
                AppIntentUtil.launchForResultWithBaseAnimation(
                    view,
                    this,
                    ScratchCardActivity::class.java
                ) { _, _ ->
                    getScratchCardSummary()
                }
            }
            R.id.inviteButton -> {
                AppIntentUtil.launchWithBaseAnimation(view, this, InviteActivity::class.java)

            }

        }


    }

}
