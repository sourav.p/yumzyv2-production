/*
 * Created By Shriom Tripathi 1 - 5 - 2020
 */

/*
 * Created By Shriom Tripathi 1 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component.groupview

/*
 This class contains the actions which is necessary for view to be child of GroupView
 */
interface GroupItemActions<T> {

    //gets override in view and initialize the state
    var mIsSelected: Boolean

    //changes the background of the view when selected
    fun setSelectedBackground()

    //changes the background of the view when deselected
    fun setDeselectedBackground()

    //this sets the listener of the specified type
    fun setOnItemClickListener(value: OnGroupItemClickListener<T>)

    //get called when want to compare the data of view with other view
    operator fun compareTo(other: GroupItemActions<*>): Int

    //updates the selection state of view to true
    fun setStateSelected() = run { mIsSelected = true }

    //updates the selection state of view to false
    fun setStateDeselected() = run { mIsSelected = false }

    //returns the current state of the view.
    fun getState(): Boolean = mIsSelected

}