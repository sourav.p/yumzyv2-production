/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */
package com.yumzy.orderfood.ui.screens.home.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Repository module for handling data operations.
 */
class HomeRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val remoteDataSource: HomeRemoteDataSource,
    private val orderLocalDataSource: OrdersDao
) {

    suspend fun getFlatHomeData(
        view: IView,
        latitude: String,
        longitude: String,
        skip: Int,
        limit: Int
    ) = networkData(
        networkCall = {
            remoteDataSource.getFlatHomeData(latitude, longitude, skip, limit)
        }
    )

    fun getHomeData(
        latitude: String,
        longitude: String,
        skip: Int,
        limit: Int
    ) = networkLiveData(
        networkCall = {
            remoteDataSource.getHomeData(latitude, longitude, skip, limit)
        }
    ).distinctUntilChanged()


    fun flatHomeList(
        latitude: String,
        longitude: String,
        skip: Int,
        limit: Int
    ) = networkLiveData(
        networkCall = {
            remoteDataSource.getFlatHomeData(latitude, longitude, skip, limit)
        }
    ).distinctUntilChanged()


    fun getUserDTO() = databaseLiveData {
        userInfoDao.getUser()
    }

    fun getOrders() = databaseLiveData { orderLocalDataSource.getOrders() }

}