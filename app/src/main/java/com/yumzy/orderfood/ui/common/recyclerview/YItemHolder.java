package com.yumzy.orderfood.ui.common.recyclerview;

import java.io.Serializable;

public class YItemHolder implements Serializable {

	//	private static final long serialVersionUID = 7637881803L;
	private String sAreaName = null;
	private int iItemId = 0;
	private String sSectionName = null;
	private int iSectionId = 0;
	private String sCompanyName = null;
	private String sName = null;
	private String sBillNo = null;
	private String sBillDate = null;


	public YItemHolder() {

	}

	public YItemHolder(String sAreaName, int iItemId) {
		this.sAreaName = sAreaName;
		this.iItemId = iItemId;
	}

	public YItemHolder(String sAreaName, int iItemId, String sSectionName, int iSectionId) {
		this.sAreaName = sAreaName;
		this.iItemId = iItemId;
		this.sSectionName = sSectionName;
		this.iSectionId = iSectionId;
	}

	public YItemHolder(String sAreaName, int iItemId, String sBillNo, String sBillDate, int iSectionId) {
		this.sAreaName = sAreaName;
		this.iItemId = iItemId;
		this.iSectionId = iSectionId;
		this.sBillNo = sBillNo;
		this.sBillDate = sBillDate;
	}

	public YItemHolder(String sAreaName, int iItemId, String sSectionName, int iSectionId, String sCompanyName, String sName, String sBillNo, String sBillDate) {
		this.sAreaName = sAreaName;
		this.iItemId = iItemId;
		this.sSectionName = sSectionName;
		this.iSectionId = iSectionId;
		this.sCompanyName = sCompanyName;
		this.sName = sName;
		this.sBillNo = sBillNo;
		this.sBillDate = sBillDate;
	}

	public String getAreaName() {
		return sAreaName;
	}

	public void setAreaName(String sAreaName) {
		this.sAreaName = sAreaName;
	}

	public int getItemId() {
		return iItemId;
	}

	public void setItemId(int iItemId) {
		this.iItemId = iItemId;
	}

	public String getSectionName() {
		return sSectionName;
	}

	public void setSectionName(String sSectionName) {
		this.sSectionName = sSectionName;
	}

	public int getSectionId() {
		return iSectionId;
	}

	public void setSectionId(int iSectionId) {
		this.iSectionId = iSectionId;
	}

	public String getCompanyName() {
		return sCompanyName;
	}

	public void setCompanyName(String sCompanyName) {
		this.sCompanyName = sCompanyName;
	}

	public String getName() {
		return sName;
	}

	public void setName(String sName) {
		this.sName = sName;
	}

	public String getBillNo() {
		return sBillNo;
	}

	public void setBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}

	public String getBillDate() {
		return sBillDate;
	}

	public void setBillDate(String sBillDate) {
		this.sBillDate = sBillDate;
	}


}
