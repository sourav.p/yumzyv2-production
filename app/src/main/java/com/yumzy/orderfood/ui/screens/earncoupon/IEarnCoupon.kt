package com.yumzy.orderfood.ui.screens.earncoupon

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IEarnCoupon : IView {
    fun fetchEarnList()
    fun fetchCouponList()

}