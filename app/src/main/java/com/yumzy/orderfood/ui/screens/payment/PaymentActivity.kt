package com.yumzy.orderfood.ui.screens.payment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import com.yumzy.orderfood.data.models.NewOrderResDTO
import com.yumzy.orderfood.data.models.PostCheckoutResDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityPaymentBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.util.IConstants
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import javax.inject.Inject

class PaymentActivity : BaseActivity<ActivityPaymentBinding, PaymentViewModel>(), IWebPayment,
    PaymentResultWithDataListener {
    
    override val vm: PaymentViewModel by viewModel()
    
    val cartHelper: CartHelper by inject()

    var earnedScratch: Boolean = false

    private lateinit var mOrder: NewOrderReqDTO
    var orderId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_payment)
        vm.attachView(this)


        getIntentData(intent)
//        setUpData()
        newOrder(mOrder)
        showZohoChatButton(false)


    }

    override fun showZohoChatButton(isVisible: Boolean) {
        super.showZohoChatButton(false)
    }

    override fun getIntentData(intent: Intent?) {
        if (intent != null) {
            try {
                val cartString = intent.getStringExtra(INTENT_ORDER)
                mOrder = Gson().fromJson(cartString, NewOrderReqDTO::class.java)
            } catch (e: ClassCastException) {
                Timber.e(e.toString())
//                animPaymentFail()
                closeActivity(
                    IConstants.PaymentConst.PAYMENT_FAILED,
                    resources.getString(R.string.oops),
                    e.toString()
                )
            }
        }
    }


//    override fun setUpData() {}

    /*
        override fun newOrder() {
            vm.newOrder(mOrder).observe(this, Observer { result ->
                RHandler<NewOrderResDTO>(
                    this,
                    result
                ) {
                    orderId = it.orderId
                    if (it.sdkOrderId.isEmpty()) {
                       // closeActivity(IConstants.PaymentConst.PAYMENT_NOT_REQUIRED, it.orderId)
                        onPaymentSuccess("",null)
                    } else {
                        startPayment(it)
                    }
                }
            })
        }
    */
    override fun newOrder(mOrder: NewOrderReqDTO) {
        vm.newOrder(mOrder).observe(this, { result ->
            if (result.type == APIConstant.Status.ERROR) {
                if (result.data != null) {
                    val errorMsg = ((result.data as Map<*, *>).toMap())["error"]
                    closeActivity(
                        IConstants.PaymentConst.PAYMENT_FAILED,
                        resources.getString(R.string.oops),
                        errorMsg.toString()
                    )
                }
            } else {
                if (result.data is NewOrderResDTO) {
                    orderId = result.data.orderId
                    if (result.data.sdkOrderId.isEmpty()) {
// closeActivity(IConstants.PaymentConst.PAYMENT_NOT_REQUIRED, it.orderId)
                        onPaymentSuccess("", null)
                    } else {
//                        startPayment(result.data)
                    }
                }
            }
        })
    }

/*    override fun startPayment(order: NewOrderResDTO) {
        val checkout = Checkout()
        val orderRequest = JSONObject()
        orderRequest.put(
            "prefill.email",
            sharedPrefsHelper.getUserEmail()
        )
        orderRequest.put(
            "prefill.contact",
            sharedPrefsHelper.getUserPhone()
        )
        orderRequest.put("name", "Paying For")
        orderRequest.put("description", " #${order.orderNum}")
        orderRequest.put("amount", order.totalAmount) // amount in the smallest currency unit
        orderRequest.put("currency", "INR")
        orderRequest.put("order_id", order.sdkOrderId)
        orderRequest.put("payment_capture", false)

        checkout.open(this, orderRequest)
    }*/

    override fun postCheckout(orderId: String) {

    }

    override fun onPaymentError(p0: Int, p1: String?, p2: PaymentData?) {
        sharedPrefsHelper.saveCurrentOrderId("")
        showToast("Payment Cancelled")
//        animPaymentFail()
        CleverTapHelper.pushEvent(CleverEvents.payment_canceled)
        closeActivity(IConstants.PaymentConst.PAYMENT_CANCELED, "", "Payment Canceled")

    }

    override fun onPaymentSuccess(p0: String?, p1: PaymentData?) {

        vm.postCheckout(orderId).observe(this, { result ->
            RHandler<PostCheckoutResDTO>(
                this,
                result
            ) {
                cartHelper.getCart().observeForever {
                }
                earnedScratch = it.earnedScratch
                gotoPlateFragment()


            }
        })
    }


    private fun gotoPlateFragment() {
        closeActivity(IConstants.PaymentConst.PAYMENT_SUCCESS, "", orderId)
    }

    override fun onSuccess(actionId: Int, data: Any?) {}

    override fun showCodeError(code: Int?, title: String?, message: String) {
        super.showCodeError(code, title, message)
//        animPaymentFail()
        closeActivity(
            IConstants.PaymentConst.PAYMENT_FAILED,
            title ?: resources.getString(R.string.oops),
            message
        )

    }

    override fun showError(error: String) {
        super.showError(error)
//        animPaymentFail()
        closeActivity(
            IConstants.PaymentConst.PAYMENT_FAILED,
            resources.getString(R.string.oops),
            error
        )

    }

    override fun onBackPressed() {
        ActionModuleHandler.showChoseActionDialog(
            this,
            IConstants.ActionModule.EXIT_PAYMENT,
            "",
            "Do you want's Cancel payment.",
            arrayListOf(
                ActionItemDTO(
                    key = ActionConstant.CLOSE,
                    textColor = ThemeConstant.blueJeans,
                    buttonColor = ThemeConstant.pinkies,
                    text = getString(R.string.close),
                ),
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.exit),
                )
            ),
            object : ActionDialog.OnActionPerformed {
                override fun onSelectedAction(
                    dialog: Dialog?,
                    actionType: Int,
                    action: ActionItemDTO?
                ) {
                    if (action != null && action.key == ActionConstant.POSITIVE) {
                        CleverTapHelper.pushEvent(CleverEvents.payment_canceled)
                        super@PaymentActivity.onBackPressed()
                    }
                }


            }
        )
    }



    private fun closeActivity(resultCode: Int, title: String, message: String) {
        val intent = Intent()
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_RESPONSE, resultCode)
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_BODY, message)
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_TITLE, title)
        intent.putExtra(IConstants.ActivitiesFlags.EARNED_SCRATCH, earnedScratch)
        setResult(Activity.RESULT_OK, intent)
        finish()
//        this.finishActivity(IConstants.PaymentConst.PROCESS_PAYMENT)
//        Handler().postDelayed({ this.finish()}, 3000)
    }

    companion object {
        const val INTENT_ORDER = "cart_intent"
    }
}