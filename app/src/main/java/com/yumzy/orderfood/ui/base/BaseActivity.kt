package com.yumzy.orderfood.ui.base

import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.color.MaterialColors
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.google.android.material.transition.platform.MaterialContainerTransformSharedElementCallback
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.laalsa.laalsaui.utils.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.api.session.SessionManager
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.databinding.LayoutBaseNoInternetBinding
import com.yumzy.orderfood.tools.RemoteConfigHelper
import com.yumzy.orderfood.ui.SharedTransition.EXTRA_TRANSITION_NAME
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.home.fragment.updateProfile
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.worker.internetdetect.NetworkStateReceiver
import com.zoho.livechat.android.MbedableComponent
import com.zoho.livechat.android.ZohoLiveChat
import com.zoho.salesiqembed.ZohoSalesIQ
import org.koin.android.ext.android.inject


abstract class BaseActivity<B : ViewDataBinding, T : BaseViewModel<*>> :
    AppCompatActivity(), IView, NetworkStateReceiver.NetworkStateReceiverListener {

    open val enterTransDuration: Long = 300
    open val returnTransDuration: Long = 275

    open val enableEdgeToEdge = true
    open val navigationPadding = true

    open lateinit var bd: B
    abstract val vm: T

    private lateinit var mProgressBar: ProgressBar
    private lateinit var frameLayout: FrameLayout

    lateinit var rootView: ConstraintLayout
    private var networkStateReceiver: NetworkStateReceiver? = null
    private val noInternetBinding by lazy {
        LayoutBaseNoInternetBinding.inflate(layoutInflater, rootView, true)
    }

    
    open val sessionManager: SessionManager by inject()

    open val sharedPrefsHelper: SharedPrefsHelper by inject()

    protected fun bindView(layoutId: Int) {
        bd = DataBindingUtil.setContentView(this, layoutId)
        bd.lifecycleOwner = this
        adjustContainer()
    }

    private fun adjustContainer() {
        BarUtils.setNavBarVisibility(this, navigationPadding)
        val navColor = if (navigationPadding) Color.WHITE else Color.TRANSPARENT
        BarUtils.setStatusBarLightMode(this, false)
        BarUtils.setNavBarColor(this, navColor)
    }

    override fun onStart() {
        super.onStart()
        RemoteConfigHelper.fetchRemoteData(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setupContainerTransform()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        networkStateReceiver = NetworkStateReceiver()
        networkStateReceiver!!.addListener(this)
        this.registerReceiver(
            networkStateReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    open fun setupContainerTransform() {
        val transitionName = intent.getStringExtra(EXTRA_TRANSITION_NAME)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && transitionName != null) {
            findViewById<View>(android.R.id.content).transitionName = transitionName
            setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            window.sharedElementEnterTransition = buildContainerTransform(enterTransDuration)
            window.sharedElementReturnTransition = buildContainerTransform(returnTransDuration)
        }

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    open fun buildContainerTransform(duration: Long): MaterialContainerTransform? {
        val transform = MaterialContainerTransform()
        transform.duration = duration
        transform.addTarget(android.R.id.content)
        transform.containerColor =
            MaterialColors.getColor(findViewById(android.R.id.content), R.attr.colorSurface)
        transform.fadeMode = MaterialContainerTransform.FADE_MODE_THROUGH
        return transform
    }

    protected open fun showZohoChatButton(isVisible: Boolean = false) {
        ZohoLiveChat.Chat.setVisibility(MbedableComponent.CHAT, isVisible)
        ZohoLiveChat.FAQ.setVisibility(isVisible)
        ZohoSalesIQ.showLauncher(isVisible)
    }

    /**warning:
     * The root of the activity container must be of type ConstraintLayout
     *  DO NOT CHANGE THIS CONSISTENCY
     * */
    override fun setContentView(layoutResID: Int) {
        rootView =
            LayoutInflater.from(this).inflate(layoutResID, null) as ConstraintLayout
        val initFrame = initFrame()
        rootView.addView(initFrame)
        super.setContentView(rootView)
    }

    override fun setContentView(view: View?) {
        rootView = getConstraintLayout()
        rootView.setBackgroundColor(Color.WHITE)
        rootView.addView(view, 0)
        rootView.addView(frameLayout, 1)
//        rootView.fitsSystemWindows = true
        super.setContentView(rootView)

    }


    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        rootView = getConstraintLayout()
        rootView.setBackgroundColor(Color.WHITE)
        rootView.addView(view, 0)
        rootView.addView(frameLayout, 1)
        super.setContentView(rootView, params)
    }

    fun getProgressBar(): ProgressBar {
        return mProgressBar
    }


    private fun showProgressBar(visibility: Boolean) {
        mProgressBar.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
        if (visibility) frameLayout.setBackgroundColor(0x3CFFFFFF)
        else frameLayout.background =
            null
        frameLayout.isClickable = visibility
    }

    override fun hideProgressBar() {
        frameLayout.setOnClickListener(null)
        showProgressBar(false)
    }

    override fun showProgressBar() {
        frameLayout.setOnClickListener { }
        showProgressBar(true)

    }

    override fun onResponse(code: Int?, response: Any?) {
        when (code) {
            APIConstant.Code.LOGGED_OUT -> {
                showSnackBar("Opp's! ${response.toString()} ", 1)
                sessionManager.removeAuthToken()
                this.deleteDatabase(AppDatabase.DATABASE_NAME)
                navToStartedScreen()

            }
        }
    }

    private fun navLoginScreen() {
        AlerterHelper.showToast(
            this,
            "Multiple Login Detected",
            "Due to user privacy & security. We don't allow multiple login.\n Please login again and continue"
        )
        val intent = Intent(this, StartedActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }


    private fun navToStartedScreen() {
        val intent = Intent(this, StartedActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    private fun getConstraintLayout(): ConstraintLayout {
        val constraintLayout = ConstraintLayout(this)
        constraintLayout.layoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        constraintLayout.id = R.id.base_constraintLayout
        initFrame()
        return constraintLayout
    }

    private fun initFrame(): FrameLayout {
        frameLayout = FrameLayout(this)
        val frameParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        frameLayout.background = null
        frameParam.gravity = Gravity.CENTER
        frameLayout.layoutParams = frameParam
        frameLayout.id = R.id.base_framelayout
        mProgressBar = ProgressBar(this)
        mProgressBar.background = null
        mProgressBar.visibility = View.GONE
//        mProgressBar.elevation = 6f
        val pbParam: FrameLayout.LayoutParams = FrameLayout.LayoutParams(45.px, 45.px)
        pbParam.gravity = Gravity.CENTER
        mProgressBar.layoutParams = pbParam
        frameLayout.addView(mProgressBar)
        return frameLayout

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEach { fragment ->
            fragment.onActivityResult(requestCode, resultCode, data)
        }


    }

    open fun showSnackBar(message: String, statusColor: Int) {
        val snackBar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT)
        val viewGroup = snackBar.view as ViewGroup
        viewGroup.minimumHeight = VUtil.dpToPx(0)
        viewGroup.getChildAt(0).background = null
        /* TODO user Color code from ui developer*/
        viewGroup.background = ColorDrawable(Color.BLACK)
        val textView = (viewGroup.getChildAt(0) as ViewGroup).getChildAt(0) as TextView
        textView.background = null
        textView.setTextColor(ThemeConstant.white)
        snackBar.setActionTextColor(Color.WHITE)
//        ViewCompat.setOnApplyWindowInsetsListener(snackBar.view, null)
        snackBar.show()
    }

    open fun showError(message: String) {
        val snackBar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT)
        val viewGroup = snackBar.view as ViewGroup
        viewGroup.minimumHeight = 0
        viewGroup.background = ColorDrawable(Color.BLACK)
        val textView = (viewGroup.getChildAt(0) as ViewGroup).getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.maxLines = 3
        textView.textSize = 12f
        textView.gravity = Gravity.CENTER
        snackBar.show()
    }


    open fun showToast(message: String) {
        AlerterHelper.showToast(this, "", message)
//        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun applyDebouchingClickListener(vararg views: View?) {
        ClickUtils.applyPressedViewScale(*views)
        ClickUtils.applySingleDebouncing(views, 300, mClickListener)
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        /*val codeColor = CodeConstant.COLOR_CODE.getCodeColor(code)
        if (codeColor == null) {
            showToast(error)
            return
        }*/

        updateProfile = true
        when (code) {
            APIConstant.Code.LOGGED_OUT, APIConstant.Code.INVALID_USER -> {
                showSnackBar(getString(R.string.session_expired), 1)
                sessionManager.removeAuthToken()
                sharedPrefsHelper.clearAllData()
                navLoginScreen()

            }

            APIConstant.Status.SESSION_EXPIRED -> {
                showSnackBar(getString(R.string.session_expired), 1)
                sessionManager.removeAuthToken()
                sharedPrefsHelper.clearAllData()
                navLoginScreen()
            }
            else -> {
                showError(message)
            }
        }

    }

    override fun networkAvailable() {
        if (internetDisconnected) {
            noInternetBinding.tvInternetMessage.text = "We are back Online"
            noInternetBinding.root.setBackgroundColor(ThemeConstant.greenConfirmColor)
            Handler(mainLooper).postDelayed(
                {
                    noInternetBinding.root.visibility = View.GONE
                    internetDisconnected = false
                }, 1500
            )
        }
    }

    private var internetDisconnected = false
    override fun networkUnavailable() {
        noInternetBinding.root.visibility = View.VISIBLE
        noInternetBinding.tvInternetMessage.text = "Could not connect to Internet"
        noInternetBinding.root.setBackgroundColor(ThemeConstant.redRibbonColor)
        internetDisconnected = true
    }

    override fun onDestroy() {
        super.onDestroy()
        networkStateReceiver?.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    private val mClickListener =
        View.OnClickListener { onDebounceClick(it) }

    /**
     *
     * you can write logic to handle click listener her of floating view or view inside constraintLayout
     *
     * */
    open fun onDebounceClick(view: View) {
    }


}