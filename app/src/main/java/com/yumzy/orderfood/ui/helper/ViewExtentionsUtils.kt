package com.yumzy.orderfood.ui.helper

import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.forEach
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

//-------------------------------------------------------------------------------------------------------------
const val CLICK_COLOR_CHANGE_TIME = 250L

//-------------------------------------------------------------------------------------------------------------
//------------------------------------------VIEW-------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------

fun View.gapTop(margin: Int) {
    if (this.layoutParams is ViewGroup.MarginLayoutParams) {

        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(0, VUtil.dpToPx(margin), 0, 0)
        this.layoutParams = menuLayoutParams
    } else {
        this.setPadding(0, VUtil.dpToPx(margin), 0, 0)
    }
}

fun View.gapLeft(margin: Int) {
    if (this.layoutParams is ViewGroup.MarginLayoutParams) {
        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(VUtil.dpToPx(margin), 0, 0, 0)
        this.layoutParams = menuLayoutParams
    } else {
        this.setPadding(VUtil.dpToPx(margin), 0, 0, 0)
    }
}

fun View.gapBottom(margin: Int) {

    if (this.layoutParams is ViewGroup.MarginLayoutParams) {
        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(0, 0, 0, VUtil.dpToPx(margin))
        this.layoutParams = menuLayoutParams
    } else {
        this.setPadding(0, 0, 0, VUtil.dpToPx(margin))
    }
}

fun View.gapAll(margin: Int = 0) {
    val gap = VUtil.dpToPx(margin)
    if (this.layoutParams is ViewGroup.MarginLayoutParams) {
        val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(
            gap,
            gap,
            gap,
            gap
        )
        this.layoutParams = menuLayoutParams
    } else {
        this.setPadding(
            gap,
            gap,
            gap,
            gap
        )
    }
}

fun View.setClickScaleEffect(
) {
    ClickUtils.applyPressedViewScale(this)
}

fun TextView.applyLotoFont() {
    this.typeface = ResourcesCompat.getFont(context, R.font.raleway_medium)
}

fun TextView.setInterFont() {
    this.typeface = ResourcesCompat.getFont(context, R.font.poppins)
}

fun TextView.setInterBoldFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.raleway_medium)
    this.setTypeface(typeface, Typeface.BOLD)
}
fun TextView.setRalewaySemiBoldFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
    this.setTypeface(typeface, Typeface.NORMAL)
}

fun TextView.setRalewayRegularFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.poppins)
    this.setTypeface(typeface, Typeface.NORMAL)
}
fun TextView.setRalewayBoldFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
    this.setTypeface(typeface, Typeface.BOLD)
}

fun TextView.setRalewayMediumFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.poppins_medium)
    this.setTypeface(typeface, Typeface.NORMAL)
}

fun TextView.setInterThineFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.poppins)
    this.setTypeface(typeface, Typeface.ITALIC)
}

fun TextView.setInterRegularFont() {
    val typeface = ResourcesCompat.getFont(context, R.font.raleway_medium)
    this.setTypeface(typeface, Typeface.NORMAL)
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.fadeIn() {
    val animationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)
    apply {
        visible()
        alpha = 0f
        animate()
            .alpha(1f)
            .setDuration(animationDuration.toLong())
            .setListener(null)
    }
}
/*
fun View.fadeOut(todoCallback: TodoCallback? = null){
    val animationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)
    apply {
        animate()
            .alpha(0f)
            .setDuration(animationDuration.toLong())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    invisible()
                    todoCallback?.execute()
                }
            })
    }
}*/

fun View.onSelectChangeColor(
    lifeCycleScope: CoroutineScope,
    clickColor: Int
) = CoroutineScope(lifeCycleScope.coroutineContext).launch {
    val intialColor = (background as ColorDrawable).color
    setBackgroundColor(
        ContextCompat.getColor(
            context,
            clickColor
        )
    )
    delay(CLICK_COLOR_CHANGE_TIME)
    setBackgroundColor(intialColor)
}

fun View.changeColor(newColor: Int) {
    setBackgroundColor(
        ContextCompat.getColor(
            context,
            newColor
        )
    )
}

fun EditText.disableContentInteraction() {
    keyListener = null
    isFocusable = false
    isFocusableInTouchMode = false
    isCursorVisible = false
    setBackgroundResource(android.R.color.transparent)
    clearFocus()
}

fun EditText.enableContentInteraction() {
    keyListener = EditText(context).keyListener
    isFocusable = true
    isFocusableInTouchMode = true
    isCursorVisible = true
    setBackgroundResource(android.R.color.white)
    requestFocus()
    if (text != null) {
        setSelection(text.length)
    }
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}


fun ViewGroup.deepForEach(function: View.() -> Unit) {
    this.forEach { child ->
        child.function()
        if (child is ViewGroup) {
            child.deepForEach(function)
        }
    }
}


internal fun View?.findSuitableParent(): ViewGroup? {
    var view = this
    var fallback: ViewGroup? = null
    do {
        if (view is CoordinatorLayout) {
            // We've found a CoordinatorLayout, use it
            return view
        } else if (view is FrameLayout) {
            if (view.id == android.R.id.content) {
                // If we've hit the decor content view, then we didn't find a CoL in the
                // hierarchy, so use it.
                return view
            } else {
                // It's not the content view but we'll use it as our fallback
                fallback = view
            }
        }

        if (view != null) {
            // Else, we will loop and crawl up the view hierarchy and try to find a parent
            val parent = view.parent
            view = if (parent is View) parent else null
        }
    } while (view != null)

    // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
    return fallback
}
//inline fun repeat(times: Int, action: (Int) -> Unit) {}
//fun CharSequence.repeat(n: Int): String{
//}