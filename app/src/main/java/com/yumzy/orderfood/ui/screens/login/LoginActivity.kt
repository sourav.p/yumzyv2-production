package com.yumzy.orderfood.ui.screens.login

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.appAnalytics
import com.yumzy.orderfood.appCrashlytics
import com.yumzy.orderfood.data.models.LoginDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.otpview.PinEditView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.login.data.SmsListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.worker.SMSBroadCastReceiver
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject


class LoginActivity :
    BaseActivity<com.yumzy.orderfood.databinding.ActivityLoginBinding, LoginViewModel>(),
    ILoginView, PinEditView.PinViewEventListener, SmsListener, ActionDialog.OnActionPerformed {
    var otpEnforce = false

    
    override val vm: LoginViewModel by viewModel()
    
    private var smsBroadCastReceiver: SMSBroadCastReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_login)

        vm.attachView(this)


        bd.viewModel = vm
        bd.pinview.setPinViewEventListener(this)
        applyDebouchingClickListener(
            bd.btnContinue,
        )
        bd.edtInputField.requestFocus()
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        /*bd.blobTitleStyleDTO = YumUtil.getBlobStyleObject(
            resources.getString(R.string.enter_mobile),
            ThemeConstant.textBlackColor,
            R.style.TextAppearance_MyTheme_Headline4_Black,
            true,
            5,
            ThemeConstant.alphaBlackColor
        )*/

//        bd.llNext.background = ShapeHelper.deformBubbleRightBottom(
//            color1 = 0xffffd15c.toInt(),
//            color2 = 0xfff8b64c.toInt(),
//            shadowColor = 0x80ffff91.toInt()
//        )
        //  bd.includeBubbleDrawable.binding.blobSubtitle.text=resources.getString(R.string.enter_mobile)
//        bd.title=resources.getString(R.string.enter_mobile)
//        bd.subTitle=resources.getString(R.string.will_verify_info)
//        bd.includeBubbleDrawable.contentLayout.background = ShapeHelper.deformBubbleDrawable(
//            color1 = 0xffffd15c.toInt(),
//            color2 = 0xfff8b64c.toInt(),
//            shadowColor = 0x80ffff91.toInt()
//        )

/*

        val phoneDrawable=IconFontDrawable(this,R.string.country_code)
        phoneDrawable.textSize=20f
        phoneDrawable.setTextColor(Color.BLACK)
        bd.edtInputField.setCompoundDrawablesRelativeWithIntrinsicBounds(phoneDrawable,null,null,null)
        bd.edtInputField.compoundDrawablePadding=UIHelper.i3px

*/

        startSMSListener()

//        setDataBaseObject()
    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true

    override fun onSuccess(actionId: Int, data: Any?) {

        if (actionId == IConstants.ActionModule.OTP_VERIFICATION) {
            if (data != null && data is LoginDTO) {
                data.token.let { token ->
                    sessionManager.saveAuthToken(token)
                }
                showProgressBar()
                appAnalytics.setUserId(data.user.userId)
                sharedPrefsHelper.saveUserId(data.user.userId)
                sharedPrefsHelper.saveCartId(data.user.cartId)
                data.user.name.let { value -> sharedPrefsHelper.saveUserName(value) }
                data.user.email.let { value -> sharedPrefsHelper.saveUserEmail(value) }
                data.user.mobile.let { value -> sharedPrefsHelper.saveUserPhone(value) }
                CleverTapHelper.pushEvent(CleverEvents.user_login)
                CleverTapHelper.setCleaverUser(data)

                if (BuildConfig.DEBUG) {
                    CleverTapHelper.pushEvent("Laalsa Test Device")
                }
                vm.saveUserInfo(data.user).observe(this, {
                    appCrashlytics.setUserId(data.user.userId);
                    navHomeActivity()
                })

            }
        }
    }


    private fun startSMSListener() {
        try {
            smsBroadCastReceiver = SMSBroadCastReceiver()
//            smsReceiver!!.mListener=this
            smsBroadCastReceiver?.bindListener(object : SmsListener {
                @SuppressLint("HardwareIds")
                override fun messageReceived(messageText: String?) {

//                Log.e("----->>", messageText)
//                    showToast(messageText.toString())
                    bd.pinview.value = messageText ?: ""
                }

                override fun onTimeOut(messageText: String?) {
                    showToast(messageText.toString())
                }
            })
            val intentFilter = IntentFilter()
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
            this.registerReceiver(smsBroadCastReceiver, intentFilter)

            val client = SmsRetriever.getClient(this)

            val task = client.startSmsRetriever()
            task.addOnSuccessListener {
                // API successfully started
//                showToast("API successfully started ")

            }

            task.addOnFailureListener {
                showSnackBar(getString(R.string.enter_otp_failed_to_read_otp), 1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun navHomeActivity() {
        val intent = Intent(this, HomePageActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, false)
        startActivity(intent)
        finishAffinity()
    }


    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.tv_resend_code -> {
//                if (otpEnforce) return
                vm.resendOTP(vm.number.value ?: "", "login")
                    .observe(this, {
                        otpEnforce = false
                        startCountDownTimer()
                    })
            }
            R.id.btn_continue -> {

                val userNumber: String = vm.number.value ?: ""
                if (!YumUtil.isValidMobile(userNumber)) {
                    bd.tvErrorValue.visibility = View.VISIBLE
                    bd.tvErrorValue.text = getString(R.string.please_enter_valid_number)
                    YumUtil.animateShake(bd.tvErrorValue, 50, 15)
                    return
                }

                if (!otpEnforce) {
                    setOtpToUser()
                    return
                }
                if (otpEnforce) {
//                    val userOtp = bd.pinview.value

                    if (bd.pinview.value.length == 0) {
                        showSnackBar(getString(R.string.please_enter_otp), 0)
                        YumUtil.animateShake(bd.pinview, 30, 10)
                        return
                    } else if (bd.pinview.value.length < 4) {
                        showSnackBar(getString(R.string.invalid_otp), 0)
                        YumUtil.animateShake(bd.pinview, 30, 10)
                        return
                    } else {
                        tryValidateUser()
                    }

                }


            }


        }
    }

    private fun tryValidateUser() {
        val deviceId = YumUtil.getDeviceUniqueId(this)
        vm.verifyOTP(vm.number.value!!, bd.pinview.value, deviceId)
            .observe(this, { result ->
                RHandler<LoginDTO>(
                    this,
                    result
                ) {
                    result.data?.token?.let { token ->
                        sessionManager.saveAuthToken(token)
                    }
                    result.data?.user?.userId?.let { value ->
                        sharedPrefsHelper.saveUserId(
                            value
                        )
                    }
                    result.data?.user?.cartId?.let { value ->
                        sharedPrefsHelper.saveCartId(
                            value
                        )
                    }
                    result.data?.user?.name?.let { value ->
                        sharedPrefsHelper.saveUserName(
                            value
                        )
                    }
                    result?.data?.user?.lastName?.let { value ->
                        sharedPrefsHelper.saveUserLastName(value)
                    }
                    result.data?.user?.email?.let { value ->
                        sharedPrefsHelper.saveUserEmail(
                            value
                        )
                    }
                    result.data?.user?.mobile?.let { value ->
                        sharedPrefsHelper.saveUserPhone(
                            value
                        )
                    }
                    result.data?.user?.picture?.let {
                        sharedPrefsHelper.saveUserPic(it)
                    }
                    result.data?.user?.referralDeepLink?.let {
                        sharedPrefsHelper.saveUserReferralLink(it)
                    }
                    result.data?.user?.refferalCode?.let {
                        sharedPrefsHelper.saveUserReferralCode(it)
                    }
                    result.data?.isNew?.let {
                        sharedPrefsHelper.saveIsNewUser(it)
                    }

                    registerResponse(result.data)
                }
            })
    }

    private fun setOtpToUser() {
        vm.sendOTP().observe(this, {
            RHandler<Any>(this, it) {
                bd.otpLayout.visibility = View.VISIBLE
                bd.tvErrorValue.visibility = View.INVISIBLE
                startCountDownTimer()
                bd.tvInfo.text = resources.getString(R.string.please_enter_otp_for_vatification)

                //bd.includeBubbleDrawable.binding.blobSubtitle.text=resources.getString(R.string.please_enter_otp_for_vatification)
            }
        })
    }

    private fun registerResponse(data: LoginDTO?) {
        showProgressBar()
        if (data != null) {
            vm.updateFcmToken(YumzyFirebaseMessaging.getToken(this).toString(), data.user.userId)
                .observe(this, {
                    if (it.type != -1) {
                        vm.saveUserInfo(data.user).observe(this, {
                            if (it.type != -1) {
                                navHomeActivity()
                                CleverTapHelper.pushEvent(CleverEvents.user_login)
                                CleverTapHelper.setCleaverUser(data)
                                hideProgressBar()
                            }
                        })
                    }
                })
        } else {
            showSnackBar("Failed To Login , Try Later!", -1)
        }
    }


    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Code.USER_NOT_FOUND) {


            /* ActionModuleHandler.showConfirmation(
                 this,
                 IConstants.ActionModule.CONFIRM_DIALOG,
                 "",
                 "New User",
                 error,
                 this
             )*/

            val title = getString(R.string.new_user)
            val sMessage = message

            val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                null,
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.ok),
                )
            )
            NewUserDialogAction(
                IConstants.ActionModule.USER_NOT_FOUND,
                title,
                sMessage,
                actions
            )

        } else
            super.showCodeError(code, title, message)
    }

    private fun NewUserDialogAction(
        newUserLogin: Int,
        title: String,
        sMessage: String,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            this,
            newUserLogin,
            title,
            sMessage,
            actions,
            this
        )
    }

    override fun onBackPressed() {
        if (otpEnforce) {
            val title = getString(R.string.almost_there)
            val sMessage = getString(R.string.we_almost_done_with_login_process)

            val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                null,
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.ok),
                )
            )
            NewUserDialogAction(
                IConstants.ActionModule.CONFIRM_BACK_PRESS,
                title,
                sMessage,
                actions
            )

//            ActionModuleHandler.showConfirmation(
//                this, IConstants.ActionModule.CONFIRM_BACK_PRESS,
//                "",
//                getString(R.string.almost_there),
//                getString(R.string.we_almost_done_with_login_process),
//                this
//            )
        } else
            super.onBackPressed()

    }

    override fun onResponse(code: Int?, response: Any?) {

        if (code == APIConstant.Status.SUCCESS) {
            bd.otpLayout.visibility = View.VISIBLE
            startCountDownTimer()
        } else
            super.onResponse(code, response)
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            finish()
        } else
            if (actionType == IConstants.ActionModule.CONFIRM_DIALOG) {
                navRegisterActivity()
            }
    }

    private fun navRegisterActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        startActivity(intent)
        finish()
    }


    override fun onDismissed(actionType: Int, actionLabel: String?) {

    }

    private fun startCountDownTimer() {
        if (otpEnforce) return

        this.otpEnforce = true
        bd.tvResendCode.isEnabled = false
        bd.edtInputField.isEnabled = false
        bd.tvErrorValue.visibility = View.INVISIBLE
        bd.edtInputField.setTextColor(ThemeConstant.mediumGray)
        bd.tvResendCode.setTextColor(ThemeConstant.pinkiesLight)
        bd.btnContinue.text = getString(R.string.verify_otp)
        bd.tvInfo.text = resources.getString(R.string.will_verify_info)

        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                bd.tvResendCode.text = "${millisUntilFinished / 1000}s Please Wait"

            }

            override fun onFinish() {
                bd.tvResendCode.isEnabled = true
                applyDebouchingClickListener(bd.tvResendCode)
                bd.edtInputField.isEnabled = true
                bd.edtInputField.setTextColor(ThemeConstant.textDarkGrayColor)
                bd.tvResendCode.text = getString(R.string.resend_code)
                bd.tvResendCode.setTextColor(ThemeConstant.blueJeans)
                otpEnforce = false
            }
        }.start()
    }


    override fun onDestroy() {
        vm.detachView()

        super.onDestroy()
        smsBroadCastReceiver?.unbindListener()
        unregisterReceiver(smsBroadCastReceiver)

    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction("com.google.android.gms.auth.api.phone.SMS_RETRIEVED")
        registerReceiver(smsBroadCastReceiver, filter)
    }

    override fun onDataEntered(pinEditView: PinEditView?, fromUser: Boolean) {
        YumUtil.hideKeyboard(this)
        tryValidateUser()
    }

    override fun messageReceived(messageText: String?) {
//        showToast(messageText ?: "Data")
    }

    override fun onTimeOut(messageText: String?) {
        showToast(messageText ?: "Failed")

    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {

        if (actionType == IConstants.ActionModule.USER_NOT_FOUND) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                navRegisterActivity()
            }
        }
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                finish()
            }
        }
    }
}

