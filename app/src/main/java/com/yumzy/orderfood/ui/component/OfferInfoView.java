package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class OfferInfoView extends LinearLayout {

    private String title;
    private String icon;
    private int mColor;

    public OfferInfoView(Context context) {
        this(context, null);
    }

    public OfferInfoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public OfferInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (icon == null) {
            icon = "";
        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.OfferInfoView, defStyleAttr, 0);
        title = a.getString(R.styleable.OfferInfoView_offer_header);
        icon = a.getString(R.styleable.OfferInfoView_offer_icon);
        mColor = a.getColor(R.styleable.OfferInfoView_offer_color, 0);
        a.recycle();
    }

    private void init() {
        this.setOrientation(HORIZONTAL);
        this.setGravity(Gravity.CENTER | Gravity.START);

        int i6px = VUtil.dpToPx(6);
        int i30px = VUtil.dpToPx(30);

        TextView tvTag = new TextView(getContext());
        tvTag.setPadding(i6px, i6px, i6px, i6px);
        tvTag.setId(R.id.tv_offer);
        tvTag.setTextColor(mColor);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), tvTag);

        tvTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvTag.setText(title);

        FontIconView iconView = new FontIconView(getContext());
        iconView.setPadding(i6px, i6px, i6px, i6px);
        iconView.setId(R.id.iv_offer);
        iconView.setText(icon);
        iconView.setTextColor(mColor);

        this.addView(iconView, new LayoutParams(i30px, i30px));
        this.addView(tvTag);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) findViewById(R.id.tv_offer)).setText(this.title);

    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
        ((TextView) findViewById(R.id.iv_offer)).setText(this.icon);
    }

    public int getmColor() {
        return mColor;
    }

    public void setmColor(int mColor) {
        this.mColor = mColor;
        ((TextView) findViewById(R.id.tv_offer)).setTextColor(this.mColor);
        ((TextView) findViewById(R.id.iv_offer)).setTextColor(this.mColor);
    }
}
