package com.yumzy.orderfood.ui.screens.invite

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericFastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.ContactModelDTO
import com.yumzy.orderfood.databinding.ActivityInviteBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.PermissionRequiredItem
import com.yumzy.orderfood.util.contact.ContactHelper
import com.yumzy.orderfood.util.intentShareText
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.collections.forEachWithIndex
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject


class InviteActivity : BaseActivity<ActivityInviteBinding, InviteViewModel>(), IInvite,
    OutletSearchView.OnQueryTextListener, ItemFilterListener<ContactListItem> {

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastAdapter
    private lateinit var mUserReferralAdapter: ItemAdapter<ContactListItem>
    private lateinit var mPermissionAdapter: ItemAdapter<PermissionRequiredItem>

    //    private lateinit var mSearchAdapter: ItemAdapter<SearchBarItem>
    private lateinit var mContactAdapter: ItemAdapter<ContactListItem>
    
//    val sharedPrefsHelper: SharedPrefsHelper by inject()
    
    override val vm: InviteViewModel by viewModel()
    
    var showApplyReferralDialog: BaseDialogFragment<*>? = null

    private var isSynced = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_invite)
        vm.attachView(this)

        //setup action bar
        setSupportActionBar(bd.mainToolbar)
//        clearLightStatusBar(this, ThemeConstant.darkBlue)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        bd.mainToolbar.setNavigationOnClickListener { onBackPressed() }


        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        val typeface = ResourcesCompat.getFont(this, R.font.poppins_medium)
        bd.mainCollapsing.setCollapsedTitleTypeface(typeface)
        bd.mainCollapsing.title = resources.getString(R.string.invite_earn_title);
        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarBlackText);
//        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarWhiteText);
//        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        //create our adapters
//        mSearchAdapter = ItemAdapter.items()
        mUserReferralAdapter = ItemAdapter.items()
        mContactAdapter = ItemAdapter.items()
        mPermissionAdapter = ItemAdapter.items()

        //create our FastAdapter
        val adapters: Collection<ItemAdapter<out GenericItem>> =
            listOf(/*mSearchAdapter ,*/ mUserReferralAdapter, mContactAdapter, mPermissionAdapter)
        mFastAdapter = FastAdapter.with(adapters)

        bd.rvContacts.layoutManager = LinearLayoutManager(this)
        bd.rvContacts.itemAnimator = DefaultItemAnimator()
        bd.rvContacts.adapter = mFastAdapter

        bd.searchView.showSearch(false)
        bd.searchView.setOnQueryTextListener(this)
        bd.searchView.setHint("Search Name or Number")

        setUserReferralAdapter()
        setContactAdapter()

    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true

    private fun setUserReferralAdapter() {
        var userReferralCode = sharedPrefsHelper.getUserReferralCode()
        if (userReferralCode.isEmpty())
            userReferralCode = "Login & Get Refer Code"

        val contactModel = ContactModelDTO(
            userReferralCode,
            "",
            "Your referral code",
            "referral"
        )
        val item = ContactListItem().withContact(contactModel).withListener(this)
        mUserReferralAdapter.set(listOf(item))
    }

    /*Todo sourav Add to base Class*/
    private fun setContactAdapter() {
        Dexter.withContext(this)
            .withPermission(android.Manifest.permission.READ_CONTACTS)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    mPermissionAdapter.clear()
                    mContactAdapter.clear()
                    getContacts(this@InviteActivity, "")
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    mPermissionAdapter.clear()
                    setPermissionRequiredItem()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                    mPermissionAdapter.clear()
                    setPermissionRequiredItem()
                }
            }).check()


        mContactAdapter.itemFilter.filterPredicate =
            { item: ContactListItem, constraint: CharSequence? ->
                (item.contactModel?.name?.toLowerCase(Locale.getDefault())
                    ?.contains(constraint.toString().toLowerCase(Locale.getDefault())) ?: false ||
                        item.contactModel?.number?.toLowerCase(Locale.getDefault())
                            ?.contains(
                                constraint.toString().toLowerCase(Locale.getDefault())
                            ) ?: false)
            }

        mContactAdapter.itemFilter.itemFilterListener = this

    }

    private fun setPermissionRequiredItem() {
        val items = mutableListOf<PermissionRequiredItem>()
        items.add(
            PermissionRequiredItem()
                .withTitle("Permission Required")
                .withDescription("We need your contact permission to show list of leads")
                .withListener {
                    setContactAdapter()
                }
        )
        mPermissionAdapter.add(items)
    }

    @SuppressLint("CheckResult")
    private fun getContacts(context: Context, query: String) {

        Observable.fromCallable { return@fromCallable ContactHelper.readContacts(context, query) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { contactList ->
                vm.saveUserContacts(sharedPrefsHelper.getUserPhone(), contactList)
                    .observe(this, { })
                mContactAdapter.clear()
                val items = mutableListOf<ContactListItem>()
                contactList.forEachWithIndex { i, contact ->
                    items.add(
                        ContactListItem().withContact(contact).withColor(i).withListener(this)
                    )
                }
                mContactAdapter.add(items)
            }
    }

    override fun onSuccess(actionId: Int, data: Any?) {

        if (actionId == IConstants.ActionModule.APPLY_REFERRAL) {
            //  getUserDetails()
        }
    }


    /**
     * Called when the user click on invite or referr button.
     *@param contactModel is the ContactModel object of clicked item
     */
    override fun onInviteClick(contactModel: ContactModelDTO) {


        val userReferralLink = sharedPrefsHelper.getUserReferralLink() ?: ""

        var inviteText = IConstants.AppConstant.INVITE_TEXT
        if (inviteText != "") {
            if (inviteText.contains("refer_code")) {
                inviteText = inviteText.replace(
                    "refer_code",
                    sharedPrefsHelper.getUserReferralCode(),
                    ignoreCase = false
                )
            }

            if (inviteText.contains("refer_link")) {
                inviteText = inviteText.replace(
                    "refer_link",
                    sharedPrefsHelper.getUserReferralLink() ?: "",
                    ignoreCase = false
                )
            }

            if (inviteText.contains("new_line")) {
                inviteText = inviteText.replace(
                    "new_line",
                    "\n",
                    ignoreCase = false
                )
            }

        } else {
            inviteText =
                resources.getString(R.string.invite_text) + "Referral Code: " + sharedPrefsHelper.getUserReferralCode() + "\n" + userReferralLink
        }
        /*
            CleverTapHelper.referralLink(
                sharedPrefsHelper.getUserName(),
                sharedPrefsHelper.getUserPhone(),
                sharedPrefsHelper.getUserReferralCode()
            )*/
        try {

            Dexter.withContext(this)
                .withPermission(android.Manifest.permission.WRITE_CONTACTS)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                        sendTextMsgOnWhatsApp(contactModel.number, inviteText)
                    }

                    override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                        showToast("Permission Required to access contacts.")
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        p0: PermissionRequest?,
                        p1: PermissionToken?
                    ) {
                        p1?.continuePermissionRequest()
                    }
                }).check()

        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this, "Share Link", Toast.LENGTH_SHORT).show()
            intentShareText(this, inviteText)
        }


    }

    /**
     * @param query the query text
     * @return true to override the default action
     */
    override fun onQueryTextSubmit(query: String?): Boolean {
        mContactAdapter.filter(query)
        return true
    }

    /**
     * @param newText the query text
     * @return true to override the default action
     */
    override fun onQueryTextChange(newText: String?): Boolean {
        mContactAdapter.filter(newText)
        return true
    }

    /**
     * Called when the query text is cleared by the user.
     *
     * @return true to override the default action
     */
    override fun onQueryTextCleared(): Boolean {
        mContactAdapter.filter("")
        return true
    }

    /**
     * Called when the items are filtered as per the query text by mContactAdapter listener.
     * @param constraint the query text
     * @param results the list of filtered contact
     */
    override fun itemsFiltered(constraint: CharSequence?, results: List<ContactListItem>?) {

    }

    /**
     * Called when the query text is cleared by the user.
     *
     */
    override fun onReset() {

    }


    private fun sendTextMsgOnWhatsApp(sContactNo: String, sMessage: String) {
        var toNumber = sContactNo // contains spaces, i.e., example +91 98765 43210
        toNumber = toNumber.replace("+", "").replace(" ", "")

        /*this method contactIdByPhoneNumber() will get unique id for given contact,
        if this return's null then it means that you don't have any contact save with this mobile no.*/
        val sContactId = contactIdByPhoneNumber(toNumber)
        if (sContactId != null && sContactId.length > 0) {

            /*
             * Once We get the contact id, we check whether contact has a registered with WhatsApp or not.
             * this hasWhatsApp(hasWhatsApp) method will return null,
             * if contact doesn't associate with whatsApp services.
             * */
            val sWhatsAppNo = hasWhatsApp(sContactId)
            if (sWhatsAppNo != null && sWhatsAppNo.isNotEmpty()) {
                val sendIntent = Intent("android.intent.action.MAIN")
                sendIntent.putExtra("jid", "$toNumber@s.whatsapp.net")
                sendIntent.putExtra(Intent.EXTRA_TEXT, sMessage)
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.setPackage("com.whatsapp")
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
            } else {
                // this contact does not exist in any WhatsApp application
                Toast.makeText(this, "Contact not found in WhatsApp !!", Toast.LENGTH_SHORT).show()
                intentShareText(this, sMessage)
            }
        } else {
            // this contact does not exist in your contact
//            Toast.makeText(this, "create contact for $toNumber", Toast.LENGTH_SHORT).show()
            intentShareText(this, sMessage)
        }
    }

    private fun contactIdByPhoneNumber(phoneNumber: String?): String? {
        var contactId: String? = null
        if (phoneNumber != null && phoneNumber.length > 0) {
            val contentResolver: ContentResolver = contentResolver
            val uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber)
            )
            val projection = arrayOf<String>(ContactsContract.PhoneLookup._ID)
            val cursor: Cursor? = contentResolver.query(uri, projection, null, null, null)
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    contactId =
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID))
                }
                cursor.close()
            }
        }
        return contactId
    }

    private fun hasWhatsApp(contactID: String): String? {
        var rowContactId: String? = null
        val hasWhatsApp: Boolean
        val projection = arrayOf<String>(ContactsContract.RawContacts._ID)
        val selection: String =
            ContactsContract.RawContacts.CONTACT_ID.toString() + " = ? AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?"
        val selectionArgs = arrayOf(contactID, "com.whatsapp")
        val cursor: Cursor? = contentResolver.query(
            ContactsContract.RawContacts.CONTENT_URI,
            projection,
            selection,
            selectionArgs,
            null
        )
        if (cursor != null) {
            hasWhatsApp = cursor.moveToNext()
            if (hasWhatsApp) {
                rowContactId = cursor.getString(0)
            }
            cursor.close()
        }
        return rowContactId
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
//            R.id.ll_referral -> {
//                val userId = sharedPrefsHelper.getUserId() ?: ""
//
//                showApplyReferralDialog =
//                    ActionModuleHandler.showApplyReferralDialog(this, userId, this)
//            }


        }
    }

//    private fun getUserDetails() {
//        vm.userDetails().observe(this, Observer {
//                result ->
//            if (result.type == -1)
//                return@Observer
//            RHandler<Any>(
//                this,
//                result
//            ) {
//                profileResponse(result.data)
//            }})
//        }
//    private fun profileResponse(data: Any?) {
//        if (data != null && data is ProfileResponseDTO) {
//            sharedPrefsHelper.saveReferredBy(data.referredBy)
//
//            val referredBy: String = sharedPrefsHelper.getReferredBy().toString()
//            if (referredBy.isEmpty()) {
//                bd.isReferredBy = true
//            } else {
//                bd.isReferredBy = false
//                bd.referredBy = referredBy
//            }
//
//            // bd.headerSubtitle.text = sharedPrefsHelper.getUserName()?.capitalize()
//            /*Glide.with(requireContext())
//                .load(sharedPrefsHelper.getUserPic().toString())
//                .placeholder(R.drawable.profile_placeholder)
//                .skipMemoryCache(true)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .apply(
//                    RequestOptions()
//                        .circleCrop()
//                )
//                .into(bd.ivUser)*/
//
//
//        }
//
//    }

}


interface IInvite : IModuleHandler {
    fun onInviteClick(contactModel: ContactModelDTO)
}