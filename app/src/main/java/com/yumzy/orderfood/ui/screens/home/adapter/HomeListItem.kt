package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ItemHomeListBinding
import com.yumzy.orderfood.databinding.ItemTrendingFoodBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.appendPriceValue
import com.yumzy.orderfood.ui.helper.hideEmptyTextView
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

class HomeListItem(homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ItemHomeListBinding>(homeListDTO) {
    override val type: Int = R.id.home_list_item

    override fun bindView(binding: ItemHomeListBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        model.run {
            val imageUrl = if (this.flatLayoutType == IConstants.LayoutType.Dish3000.YUMZY_BEST) {
                this.image?.url ?: ""
            } else {
                this.heroImage?.url ?: this.image?.url ?: ""
            }
            YUtils.setImageInView(binding.itemImage, imageUrl)
            binding.title = this.title.capitalize(Locale.getDefault())
//            binding.textLocation.hideEmptyTextView(this.subTitle)
            val text = model.cuisines?.joinToString(separator = ", ", postfix = "") ?: ""
            binding.textSubtitle.hideEmptyTextView(text)
            val duration = model.meta?.duration?.toInt()
            val sDuration = if (duration ?: 0 > 0) {
                "$duration mins"
            } else {
                null
            }
//            val ratting = model.rating ?: ""
            binding.rating = model.rating ?: ""
            binding.textDuration.hideEmptyTextView(sDuration)
            val offerText = YumUtil.setZoneOfferString(model.meta?.offers)
            binding.textRibbon.hideEmptyTextView(offerText)
            val costForTwo =
                if (model.meta?.costForTwo.isNullOrEmpty()) {
                    null
                } else "₹${model.meta?.costForTwo} for one"

            binding.textPriceRange.hideEmptyTextView(costForTwo)
            binding.tvLocationDistance.hideEmptyTextView(
                YumUtil.getLocationDistance(
                    model.subTitle,
                    ""
                )
            )
        }
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemHomeListBinding {
        return ItemHomeListBinding.inflate(inflater, parent, false)
    }


}

class TrendingFoodsItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ItemTrendingFoodBinding>(item) {
    override val type: Int = R.id.trending_food_item

    override fun bindView(binding: ItemTrendingFoodBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.title = model.title
        val isVeg = model.meta?.isVeg ?: true
        if (isVeg)
            binding.vegNonVeg.setTextColor(ThemeConstant.vegColor)
        else
            binding.vegNonVeg.setTextColor(ThemeConstant.nonVegColor)
        model.run {
            YUtils.setImageInView(binding.itemImage, this.image?.url ?: "")
            binding.title = this.title.capitalize(Locale.getDefault())
            binding.textPriceRange.appendPriceValue(model.meta?.price?.toInt())
//            binding.textLocation.hideEmptyTextView(this.subTitle)
            binding.textRating.hideEmptyTextView(rating)
        }
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemTrendingFoodBinding {
        return ItemTrendingFoodBinding.inflate(inflater, parent, false)
    }


}
