package com.yumzy.orderfood.ui.component.groupview

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.databinding.LayoutEmptyExploreBinding
import com.yumzy.orderfood.ui.helper.hideEmptyTextView

class ExploreEmptyView : ConstraintLayout {
    internal val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutEmptyExploreBinding.inflate(layoutInflater, this, true)
    }


    constructor(context: Context) : super(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        binding.blobTitle.alpha = 0f
        val l = LayoutTransition()
        l.enableTransitionType(LayoutTransition.CHANGE_APPEARING)
        this.layoutTransition = l
    }

    init {

        val exploreImage = ImageView(context)
//        exploreImage.setImageResource(R.drawable.img_explore_food)
        val imageParam = LayoutParams(0, 180.px)
        imageParam.rightToRight = LayoutParams.PARENT_ID
        imageParam.bottomToBottom = LayoutParams.PARENT_ID
        imageParam.dimensionRatio = "1:1"
        exploreImage.scaleType = ImageView.ScaleType.FIT_END
        imageParam.setMargins(10.px, 10.px, 10.px, 55.px)
//        imageParam.marginStart = 150.px
        exploreImage.layoutParams = imageParam
        this.addView(exploreImage)
        binding.blobTitle.animate().alpha(1f)
    }

    fun setTitle(title: String?) {
        binding.blobTitle.hideEmptyTextView(title)
    }

    fun setSubtitle(subtitle: String?) {
        binding.blobSubtitle.hideEmptyTextView(subtitle)
    }

    fun setImage(drawable: Drawable?) {
        val imageView = this.getChildAt(0) as ImageView
        imageView.setImageDrawable(drawable)
    }

    val button = binding.layoutButton

}