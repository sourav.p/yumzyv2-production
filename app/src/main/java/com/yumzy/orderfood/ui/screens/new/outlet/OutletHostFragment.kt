package com.yumzy.orderfood.ui.screens.new.outlet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentOutletHostBinding

class OutletHostFragment : Fragment() {

    private val navController by lazy {
        findNavController()
    }

    private val navArgs by lazy {
        navArgs<OutletHostFragmentArgs>().value
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bd: FragmentOutletHostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_outlet_host, container, false)
        navController.setGraph(R.navigation.nav_outlet, navArgs.toBundle())
        return bd.root
    }

}