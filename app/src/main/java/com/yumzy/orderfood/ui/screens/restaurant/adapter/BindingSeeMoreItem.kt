package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterSeeMoreBinding
import com.yumzy.orderfood.databinding.AdapterTopRestaurantBinding
import com.yumzy.orderfood.databinding.AdapterWinterSpecailBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil

class BindingSeeMoreItem : AbstractBindingItem<AdapterSeeMoreBinding>() {

    internal var item: HomeListDTO? = null

    override val type: Int = R.id.item_see_more_id

    fun withSeeMoreItem(item: HomeListDTO): BindingSeeMoreItem {
        this.item = item
        return this
    }


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterSeeMoreBinding {
        return AdapterSeeMoreBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterSeeMoreBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.tvSeeMore.background = DrawableUtils.getRoundDrawableListState(
            ThemeConstant.thinBlue1,
            UIHelper.i8px.toFloat(),
            ThemeConstant.thinBlue2,
            UIHelper.i8px.toFloat()
        )

    }

    override fun unbindView(binding: AdapterSeeMoreBinding) {}


    class SeeMoreClickEventHook(val callBack: (item: HomeListDTO, view: View) -> Unit) :
        ClickEventHook<BindingSeeMoreItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return viewHolder.itemView
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<BindingSeeMoreItem>,
            item: BindingSeeMoreItem
        ) {
            callBack(item.item!!, v)
        }
    }
}