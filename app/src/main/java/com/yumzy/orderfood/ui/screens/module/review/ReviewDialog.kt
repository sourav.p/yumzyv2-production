package com.yumzy.orderfood.ui.screens.module.review

import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.RatingDTO
import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogReviewBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.component.CuisineView
import com.yumzy.orderfood.ui.component.FeedbackSelectionView
import com.yumzy.orderfood.ui.component.groupview.FlowGroupView
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import kotlinx.android.synthetic.main.dialog_apply_coupon.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class ReviewDialog(
    private var rating: String,
    private val orderNum: String,
    private val orderId: String,
    private val outletName: String,
    private val items: String,
    private val moduleHandler: IModuleHandler?

) : BaseDialogFragment<DialogReviewBinding>(), IRateOrder,
    FlowGroupView.OnSelectedItemChangeListener {
    
    val vm: RateOrderViewModel  by viewModel()

    var selectedTasteList = ""
    var selDeliveryFeedbackList = ""
    var comment = ""
    var delivery_ratingbar: String = ""

    override fun getLayoutId(): Int = R.layout.dialog_review


    override fun onDialogReady(view: View) {
        dialogParent.setOnClickListener { onDebounceClick(bd.dialogParent) }
        applyDebouchingClickListener(bd.tvSkip)
        bd.ratingbar.rating = rating.toFloat()

        //showFeedbackMessage(rating)

        bd.tvSubHeader.text = "Rate your experience with $outletName to improve their service "
        applyDebouchingClickListener(bd.btnContinue, bd.btnSubmitFeedback)
        bd.ratingbar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener()
        { ratingBar: RatingBar, fl: Float, b: Boolean ->

            rating = ratingBar.rating.toString()

            bd.ratingbar.rating = rating.toFloat()

            showFeedbackMessage(rating)


        }

        bd.deliveryRatingbar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener()
        { ratingBar: RatingBar, fl: Float, b: Boolean ->

            delivery_ratingbar = ratingBar.rating.toString()

            bd.deliveryRatingbar.rating = delivery_ratingbar.toFloat()
//            when (delivery_ratingbar) {
//
//                IConstants.RatingFlags.RATING_0_0 -> {
//                    bd.tvDeliveryRatingStatus.text = ""
//                    delivery_ratingbar = ""
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.what_did_you_like_most)
//                }
//                IConstants.RatingFlags.RATING_1_0 -> {
//                    bd.tvDeliveryRatingStatus.text =
//                        resources.getString(R.string.disappointed)
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.delivery_issue)
//
//                }
//
//                IConstants.RatingFlags.RATING_2_0 -> {
//                    bd.tvDeliveryRatingStatus.text =
//                        resources.getString(R.string.mixed_feelings)
//
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.delivery_issue)
//                }
//
//                IConstants.RatingFlags.RATING_3_0 -> {
//                    bd.tvDeliveryRatingStatus.text =
//                        resources.getString(R.string.not_satisfied)
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.what_did_you_like_most)
//
//                }
//                IConstants.RatingFlags.RATING_4_0 -> {
//                    bd.tvDeliveryRatingStatus.text =
//                        resources.getString(R.string.good)
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.what_did_you_like_most)
//
//                }
//                IConstants.RatingFlags.RATING_5_0 -> {
//                    bd.tvDeliveryRatingStatus.text =
//                        resources.getString(R.string.excellent)
//                    bd.tvDeliveryTitle.text = resources.getString(R.string.what_did_you_like_most)
//                }
//            }


        }

        setupFoodReview()

        setupDeliverReview()


    }

    private fun showFeedbackMessage(rating: String) {
        when (rating) {
//            IConstants.RatingFlags.RATING_0_0 -> {
//                this.rating = ""
//                bd.tvRatingStatus.text = ""
//
//                bd.tvTitle.text = resources.getString(R.string.what_did_you_like_most)
//            }
//            IConstants.RatingFlags.RATING_1_0 -> {
//                bd.tvRatingStatus.text = resources.getString(R.string.disappointed)
//                bd.tvTitle.text = resources.getString(R.string.what_went_bad)
//
//            }
//            IConstants.RatingFlags.RATING_2_0 -> {
//                bd.tvRatingStatus.text =
//                    resources.getString(R.string.mixed_feelings)
//                bd.tvTitle.text = resources.getString(R.string.what_went_bad)
//
//            }
//            IConstants.RatingFlags.RATING_3_0 -> {
//                bd.tvRatingStatus.text = resources.getString(R.string.not_satisfied)
//                bd.tvTitle.text = resources.getString(R.string.what_did_you_like_most)
//
//            }
//            IConstants.RatingFlags.RATING_4_0 -> {
//                bd.tvRatingStatus.text =
//                    resources.getString(R.string.enjoyed_it)
//                bd.tvTitle.text = resources.getString(R.string.what_did_you_like_most)
//
//            }
//            IConstants.RatingFlags.RATING_5_0 -> {
//                bd.tvRatingStatus.text =
//                    resources.getString(R.string.new_favoutite)
//                bd.tvTitle.text = resources.getString(R.string.what_did_you_like_most)
//
//            }
        }


    }

    private fun setupFoodReview() {
        val cuisineList = mutableListOf<Triple<String, String, String>>()
        cuisineList.add(
            Triple(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/lunch.svg",
                "Packaging", "Packaging"
            )
        )
        cuisineList.add(
            Triple(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/salad_3.svg",
                "Taste", "Taste"
            )
        )
        cuisineList.add(
            Triple(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/pizza_3.svg",
                "Portion Size", "Portion Size"
            )
        )
        bd.cvFoodReview.setItems(
            cuisineList,
            fun(
                listener: OnGroupItemClickListener<CuisineView>,
                item: Triple<String, String, String>
            ): CuisineView {
                val view = CuisineView(requireContext())
                view.setCuisine(item.first, item.second, item.third)
                view.setOnItemClickListener(listener)
                return view
            }
        )
        bd.cvFoodReview.setOnSelectedItemChangeListener(this)
        bd.cvFoodReview.setItemSelectedAt(0)
    }

    private fun setupDeliverReview() {
        val testList = mutableListOf<String>(
            "  I loved it  ",
            "  I didn't like it  ",
            "  Delivery Executive was good  ",
            "  Delivery was fast!  ",
            "  Delivery Executive was rude  ",
            "  Late Delivery  "

        )

        bd.cvDeliveryReview.setItems(
            testList,
            fun(
                listener: OnGroupItemClickListener<FeedbackSelectionView>,
                item: String
            ): FeedbackSelectionView {
                val view = FeedbackSelectionView(requireContext())
                view.text = item
                view.setOnItemClickListener(listener)
                return view
            }
        )
        bd.cvDeliveryReview.setOnSelectedItemChangeListener(this)
        bd.cvDeliveryReview.setItemSelectedAt(0)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun skip() {
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.tv_skip -> {

                val tasteArray = selectedTasteList.split(",")
                val deliveryArray = selDeliveryFeedbackList.split(",")
                var rate = 0.0f
                if (rating.equals("")) {
                    rate = 0.0f

                } else {
                    rate = rating.toFloat()
                }
                var delrate = 0.0f
                if (delivery_ratingbar.equals("")) {
                    delrate = 0.0f
                } else {
                    delrate = delivery_ratingbar.toFloat()
                }
                val tasteArrayList = ArrayList<RatingDTO>()
                tasteArrayList.add(RatingDTO(tasteArray, rate.toDouble()))

                val deliveryArrayList = ArrayList<RatingDTO>()
                deliveryArrayList.add(RatingDTO(deliveryArray, delrate.toDouble()))
                val selRatingFeedback = RatingFeedbackDTO(
                    tasteArrayList,
                    orderId,
                    orderNum,
                    deliveryArrayList,
                    bd.edtComment.text.toString().trim().replace("\\s+".toRegex(), " ").capitalize()

                )
                //  Log.e("----1-------->>>", selRatingFeedback.toString())
                getRatingFeedback(selRatingFeedback)
            }
            R.id.btn_continue -> {
                if (rating.equals("")) {
                    YumUtil.animateShake(bd.ratingbar, 30, 10)

                } else {
                    bd.llDeliveryReview.visibility = View.VISIBLE
                    bd.llDeliveryRatingview.visibility = View.VISIBLE
                    bd.btnSubmitFeedback.visibility = View.VISIBLE
                    bd.llTasteReview.visibility = View.GONE
                    bd.btnContinue.visibility = View.GONE
                    bd.llFoodRatingview.visibility = View.GONE
                    bd.tvSubHeader.text = resources.getString(R.string.delivery_exp_hint)
                }
            }
            R.id.btn_submit_feedback -> {
                if (delivery_ratingbar.equals("")) {
                    YumUtil.animateShake(bd.deliveryRatingbar, 30, 10)

                } else {

                    val tasteArray = selectedTasteList.split(",")
                    val deliveryArray = selDeliveryFeedbackList.split(",")

                    val rate = rating.toFloat()
                    val delrate = delivery_ratingbar.toFloat()
                    val tasteArrayList = ArrayList<RatingDTO>()
                    tasteArrayList.add(RatingDTO(tasteArray, rate.toDouble()))

                    val deliveryArrayList = ArrayList<RatingDTO>()
                    deliveryArrayList.add(RatingDTO(deliveryArray, delrate.toDouble()))
                    val selRatingFeedback = RatingFeedbackDTO(
                        tasteArrayList,
                        orderId,
                        orderNum,
                        deliveryArrayList,
                        bd.edtComment.text.toString().trim().replace("\\s+".toRegex(), " ")
                            .capitalize()
                    )
                    getRatingFeedback(selRatingFeedback)
                    //   Log.e("----2-------->>>", selRatingFeedback.toString())

                }
            }

        }
    }

    private fun getRatingFeedback(selRatingFeedback: RatingFeedbackDTO) {
        CleverTapHelper.submitRatting(selRatingFeedback)
        vm.getRatingFeedback(selRatingFeedback).observe(
            this,
            { result ->
                RHandler<Any>(this, result) {

                    dismiss()
                    moduleHandler?.onSuccess(
                        moduleId,
                        mapOf("orderId" to orderId, "rating" to rating)
                    )
                    AppUIController.showThankYouRate()


                }
            })

    }

    override fun onSelectedItemChanged(list: MutableList<GroupItemActions<*>>, listType: Int) {
        when (listType) {
            R.id.cv_food_review -> {
                selectedTasteList =
                    list.map { view ->
                        (view as CuisineView).findViewById<TextView>(IConstants.CuisineModule.CUISINE_ID)
                            .text
                    }
                        .joinToString(", ")
                bd.btnContinue.visibility = View.VISIBLE

            }

            R.id.cv_delivery_review -> {
                selDeliveryFeedbackList =
                    list.map { item -> (item as FeedbackSelectionView).text }.joinToString(", ")


            }

        }
    }

    override fun onShowMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}
