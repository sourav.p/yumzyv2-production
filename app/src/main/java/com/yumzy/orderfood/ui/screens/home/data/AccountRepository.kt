/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */
package com.yumzy.orderfood.ui.screens.home.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Repository module for handling data operations.
 */
class AccountRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val remoteDataSource: HomeRemoteDataSource
) {
    fun getUserDTO() = databaseLiveData {
        userInfoDao.getUser()
    }

    fun logoutUser() = networkLiveData { remoteDataSource.logoutUser() }
    fun removeUserDTO() = backgroundLiveData {
        userInfoDao.removeUser()
        return@backgroundLiveData ResponseDTO.success(null, null, null, true, null, null)

    }

    fun profileUser() = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.profileUser()
        }
    ).distinctUntilChanged()

    fun getScratchCardInfo() = networkLiveData(
        networkCall = {
            remoteDataSource.getScratchCardInfo()
        }
    )
}