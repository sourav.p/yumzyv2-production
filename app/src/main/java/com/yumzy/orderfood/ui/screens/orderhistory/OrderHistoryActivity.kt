package com.yumzy.orderfood.ui.screens.orderhistory

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.OrderHistoryDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityOrderHistoryBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter.ListLoadingItem
import com.yumzy.orderfood.ui.screens.reorder.ReorderActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.StickyHeaderAdapter
import com.yumzy.orderfood.util.IConstants
import com.zoho.livechat.android.ZohoLiveChat
import com.zoho.salesiqembed.ZohoSalesIQ
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class OrderHistoryActivity : BaseActivity<ActivityOrderHistoryBinding, OrderHistoryViewModel>(),
    IOrderHistory, IModuleHandler {

    var flag: Int = 0
    private var listReachedEnd: Boolean = false

    private var skip: Int = 0
    var itemLoading = false

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    
    override val vm: OrderHistoryViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_order_history)


        JodaTimeAndroid.init(this)
        vm.attachView(this)

        setSupportActionBar(bd.mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
//        setLightStatusBar(this, Color.WHITE)
        bd.mainToolbar.menu.findItem(R.id.chat_zoho).isVisible = false
        val typeface = ResourcesCompat.getFont(this, R.font.poppins_medium)
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)

//        bd.mainCollapsing.setCollapsedTitleTypeface(typeface)
//        bd.mainCollapsing.title = resources.getString(R.string.history)
//        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
//        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarBlackText)

        flag = intent.getIntExtra(IConstants.ActivitiesFlags.FLAG, 0)

        //setupPagingRecyclerView()
    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun setupPagingRecyclerView() {

        val stickyHeaderAdapter = StickyHeaderAdapter<GenericItem>()
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        footerAdapter = ItemAdapter.items()
        mFastAdapter.addAdapter(1, footerAdapter)

        bd.rvOrderHistory.layoutManager = LinearLayoutManager(this)
        bd.rvOrderHistory.itemAnimator = DefaultItemAnimator()
        bd.rvOrderHistory.adapter = stickyHeaderAdapter.wrap(mFastAdapter)

        val decoration = StickyRecyclerHeadersDecoration(stickyHeaderAdapter)
        bd.rvOrderHistory.addItemDecoration(decoration)

        //so the headers are aware of changes
        mFastAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                decoration.invalidateHeaders()
            }
        })

        mFastAdapter.attachDefaultListeners = true


        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = ListLoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                fetchOrderHistoryList()
            }
        }

        bd.rvOrderHistory.addOnScrollListener(endlessRecyclerOnScrollListener)
        fetchOrderHistoryList()
    }

    override fun fetchOrderHistoryList() {
        if (listReachedEnd) {
            footerAdapter.clear()
            val progressItem = ListLoadingItem()
            progressItem.isEnabled = false
            footerAdapter.add(progressItem)
            return
        }
        vm.getOrderHistoryList(skip).observe(this,
            Observer { result ->
                if (result.type == -1)
                    return@Observer
                itemLoading = true
                RHandler<OrderHistoryDTO>(
                    this,
                    result
                ) { orderHistoryResDto ->

                    footerAdapter.clear()
                    if (orderHistoryResDto.orders.isNullOrEmpty()) {
                        listReachedEnd = true
                        vm.getOrderHistoryList(skip)
                            .removeObservers(this)
                    }

                    val items = ArrayList<OrderHistoryItem>()
                    orderHistoryResDto.orders.forEach { order ->

//                        val orderHistoryItem =
//                            OrderHistoryItem().withContext(this).withOrder(order).withListener(this)
//                        items.add(orderHistoryItem)
                    }


                    mFastAdapter.add(items)
                    skip++
                    if (skip == 1 && result.data?.orders.isNullOrEmpty()) {
                        bd.noDataToDisplay.visibility = View.VISIBLE
                        bd.rvOrderHistory.visibility = View.GONE
                        //  bd.tvInfo.visibility = View.GONE
                        bd.mainToolbar.menu.findItem(R.id.chat_zoho).isVisible = false

                        //bd.noDataLayout.addView(UIHelper.showNoDataToDisplay(requireContext = this))


                    } else {
                        bd.noDataToDisplay.visibility = View.GONE
                        bd.rvOrderHistory.visibility = View.VISIBLE
                        //   bd.tvInfo.visibility = View.GONE
                        bd.mainToolbar.menu.findItem(R.id.chat_zoho).isVisible = true

                    }

                }

            })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_chat_option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.chat_zoho) {
            CleverTapHelper.pushEvent(CleverEvents.support_chat_requested)
            zohoSetupVisitor()
            ZohoLiveChat.Chat.open()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun zohoSetupVisitor() {
//        ZohoSalesIQ.registerVisitor(sharedPrefsHelper.getUserPhone())
        ZohoSalesIQ.Visitor.setName(sharedPrefsHelper.getUserName())
        ZohoSalesIQ.Visitor.setEmail(sharedPrefsHelper.getUserEmail())
        ZohoSalesIQ.Visitor.setContactNumber(sharedPrefsHelper.getUserPhone())
//        ZohoSalesIQ.Visitor.addInfo("User Type","Premium"); //Custom visitor information
    }

    override fun getOrderID(orderID: String, position: Int) {
        val intent = Intent(this, ReorderActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.FLAG, orderID)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                OutletActivity.REStAURANT_REQ_CODE -> {
                    val returnIntent = Intent()
                    returnIntent.putExtra(
                        IConstants.ActivitiesFlags.SHOW_PLATE,
                        true
                    )
                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        if (actionId == IConstants.ActionModule.APPLY_RATING && data != null) {
//            setupPagingRecyclerView()
            (data as Map<String, String>)
            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val item = mFastAdapter.getItem(i)
                when (item) {
                    is OrderHistoryItem -> {
                        if (item.order?.orderId == data["orderId"]) {
                            item.order?.rated = true
                            item.order?.rating = data["rating"]?.toDouble() ?: 0.0
                            val position = mFastAdapter.getPosition(item)
                            mFastAdapter.notifyAdapterItemChanged(position)
                            break@loop
                        }
                    }
                }
            }
            AppUIController.showThankYouRate()
        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.rvOrderHistory.visibility = View.GONE
            //  bd.tvInfo.visibility = View.GONE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet


        }
    }

//    override fun actionReorder(outletId: String) {
//        val intent = Intent(this, OutletActivity::class.java)
//        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
//        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
//    }

//    override fun actionTrack(orderId: String) {
//        ActionModuleHandler.showOrderTracking(this, orderId)
//    }
//
//    override fun actionRate(order: Order) {
//        ActionModuleHandler.rateOrder(
//            this,
//            order.orderId,
//            order.orderNum,
//            order.outletName,
//            order.items,
//            this
//        )
//    }
//
//    override fun actionOrderDetails(orderId: String) {
//
//        val intent = Intent(this, ReorderActivity::class.java)
//        intent.putExtra(IConstants.ActivitiesFlags.FLAG, orderId)
//        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
//    }
}


//interface OrderHistoryItemListener {
//    fun actionReorder(outletId: String)
//    fun actionTrack(orderId: String)
//    fun actionRate(order: Order)
//    fun actionOrderDetails(orderId: String)
//}



