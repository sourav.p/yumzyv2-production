package com.yumzy.orderfood.ui.screens.login.fragment


import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.ui.RegisterUserDTO
import com.yumzy.orderfood.databinding.FragmentLoginBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(), ILoginView,
    ActionDialog.OnActionPerformed {
    
    override val vm: LoginViewModel by viewModel()

    private val navController by lazy {
        findNavController()
    }

    var userNumber = ""

    override fun getLayoutId() = R.layout.fragment_login

    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        sharedPrefsHelper.saveIsTemporary(true)

        bd.backArrow.setOnClickListener {
            backPress()
        }
        bd.btnLogin.setOnClickListener {
            userNumber = bd.edtNumber.text.toString()
            if (!YumUtil.isValidMobile(userNumber)) {
                YumUtil.hideKeyboard(requireActivity())
                showSnackBar(getString(R.string.please_enter_valid_number), -1)
            } else {
                vm.number.value = userNumber
                YumUtil.hideKeyboard(requireActivity())
                setOtpToUser()
            }
        }
    }

    private fun setOtpToUser() {
        vm.sendOTP().observe(this, {
            RHandler<Any>(this, it) {
                navOtpVerify()

            }
        })
    }

    override fun navHomeActivity() {
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        YumUtil.hideKeyboard(requireActivity())
        if (code == APIConstant.Code.USER_NOT_FOUND) {

            val title = getString(R.string.new_user)
            val sMessage = message

            val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.create_new_account),
                ),
                ActionItemDTO(
                    key = ActionConstant.CLOSE,
                    text = getString(R.string.cancel),
                    textColor = ThemeConstant.textBlackColor,
                    buttonColor = ThemeConstant.textBlackColor,
                )
            )
            newUserDialogAction(
                IConstants.ActionModule.USER_NOT_FOUND,
                title,
                sMessage,
                actions
            )

        } else
            super.showCodeError(code, title, message)
    }


    override fun onSuccess(actionId: Int, data: Any?) {

//            if (actionId == IConstants.ActionModule.OTP_VERIFICATION) {
//                if (data != null && data is LoginDTO) {
//                    data.token.let { token ->
//                        sessionManager.saveAuthToken(token)
//                    }
//                    showProgressBar()
//                    appAnalytics.setUserId(data.user.userId)
//                    sharedPrefsHelper.saveUserId(data.user.userId)
//                    sharedPrefsHelper.saveCartId(data.user.cartId)
//                    data.user.name.let { value -> sharedPrefsHelper.saveUserName(value) }
//                    data.user.email.let { value -> sharedPrefsHelper.saveUserEmail(value) }
//                    data.user.mobile.let { value -> sharedPrefsHelper.saveUserPhone(value) }
//                    CleverTapHelper.pushEvent(CleverEvents.user_login)
//                    CleverTapHelper.setCleaverUser(data)
//
//                    if (BuildConfig.DEBUG) {
//                        CleverTapHelper.pushEvent("Laalsa Test Device")
//                    }
//                    vm.saveUserInfo(data.user).observe(this, Observer {
//                        appCrashlytics.setUserId(data.user.userId);
//                        navHomeActivity()
//                    })
//
//                }
//            }
    }

    override fun onResponse(code: Int?, response: Any?) {

        if (code == APIConstant.Status.SUCCESS) {
            navOtpVerify()
        } else
            super.onResponse(code, response)
    }

    private fun navOtpVerify() {
        val userData = RegisterUserDTO(
            "", "", "", vm.number.value.toString(),
            IConstants.FragmentFlag.FROM_LOGIN
        )
        val action = LoginFragmentDirections.fragmentLoginToOtpVerify(userData)
        navController.navigate(action)
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            backPress()
        } else
            if (actionType == IConstants.ActionModule.CONFIRM_DIALOG) {
                navRegisterFragment()
            }
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }

    private fun newUserDialogAction(
        newUserLogin: Int,
        title: String,
        sMessage: String,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            requireContext(),
            newUserLogin,
            title,
            sMessage,
            actions,
            this,
            iconString = resources.getString(R.string.icon_user_1)
        )
    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {

        if (actionType == IConstants.ActionModule.USER_NOT_FOUND) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                navRegisterFragment()
            }
        }
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                backPress()
            }
        }
    }

    private fun backPress() {
        navController.popBackStack()
    }

    private fun navRegisterFragment() {
        val action = LoginFragmentDirections.fragmentLoginToRegister(vm.number.value.toString())
        navController.navigate(action)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: LoginFragmentArgs by navArgs()
        if (args.phoneNumber.isNotEmpty()) {
            userNumber = args.phoneNumber.toString()
            vm.number.value = userNumber
            bd.edtNumber.setText(vm.number.value.toString())
        }
    }
}