package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.component.RatingView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil

class FeaturedBannerAdapter(
    val context: Context,
    private val homeLayoutList: ArrayList<HomeListDTO>?,
    val callerFun: (View, HomeListDTO) -> Unit
) :
    RecyclerView.Adapter<FeaturedBannerAdapter.FeaturedBrandHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FeaturedBrandHolder {
        val itemFeatured =
            LayoutInflater.from(context).inflate(R.layout.adapter_featured, parent, false)
        return FeaturedBrandHolder(itemFeatured)
    }

    override fun getItemCount() = homeLayoutList?.size ?: 0


    inner class FeaturedBrandHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.findViewById(R.id.cardView_featured)
        val roundImageView: RoundedImageView = itemView.findViewById(R.id.feature_image)
        val tvTitle: TextView = itemView.findViewById(R.id.feature_title)
        val locality: TextView = itemView.findViewById(R.id.feature_locality)
        val tvSubTitle: TextView = itemView.findViewById(R.id.feature_subtitle)
        val tvDescription: TextView = itemView.findViewById(R.id.feature_description)
        val offer: TextView = itemView.findViewById(R.id.feature_offer)
        val ratingView: RatingView = itemView.findViewById(R.id.feature_rating)

        fun bindView(homeItem: HomeListDTO?) {
            homeItem?.run {


                if (homeItem.cuisines.isNullOrEmpty()) {
                    tvSubTitle.text = ""
                    tvSubTitle.visibility = View.GONE

                } else {
                    tvSubTitle.text =
                        homeItem.cuisines?.joinToString(separator = ", ", postfix = "") ?: ""
                    tvSubTitle.visibility = View.VISIBLE

                }



                cardView.radius = UIHelper.cornerRadius
                roundImageView.cornerRadius = UIHelper.cornerRadius


                YUtils.setImageInView(roundImageView, this.image?.url ?: "")
                tvTitle.text = this.image?.caption ?: ""
                if (this.subTitle.isNullOrEmpty()) {
                    locality.text = ""
                    locality.visibility = View.GONE

                } else {
                    locality.text = this.subTitle
                    locality.visibility = View.VISIBLE

                }

                tvDescription.text = YumUtil.getDurationAndCostForTwo(homeItem)

                if (!homeItem.meta?.offers.isNullOrEmpty()) {
                    val zoneOffer = YumUtil.setZoneOfferString(homeItem.meta?.offers)
                    offer.text = zoneOffer
                }

                if (!this.rating.isNullOrEmpty()) {
                    ratingView.setRating(this.rating.toString())
                }


                ClickUtils.applyGlobalDebouncing(
                    itemView,
                    IConstants.AppConstant.CLICK_DELAY
                ) {
                    callerFun(itemView, homeItem)

                }

            }
        }
    }

    override fun onBindViewHolder(holder: FeaturedBrandHolder, position: Int) {
        val homeItem = homeLayoutList?.get(position)
        holder.bindView(homeItem)
    }

}

