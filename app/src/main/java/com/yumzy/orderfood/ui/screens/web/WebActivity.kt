package com.yumzy.orderfood.ui.screens.web

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.databinding.ActivityWebBinding
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.util.IConstants
import org.json.JSONObject


class WebActivity : AppCompatActivity() {

    companion object {
        const val REQ_CODE = 8272
    }

    private lateinit var webView: WebView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflate = ActivityWebBinding.inflate(layoutInflater)
        setContentView(inflate.root)

        BarUtils.setStatusBarVisibility(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)

        if (intent.hasExtra(IConstants.ActivitiesFlags.URL)) {
            val stringExtra = intent.getStringExtra(IConstants.ActivitiesFlags.URL)
            if (stringExtra == "") finish()

            webView = inflate.webView


            webView.addJavascriptInterface(object : WebAppInterface() {
                override fun onCallback(jsonString: String) {
                    val obj = JSONObject(jsonString)
                    if (obj.has("destination") && obj.has("destination_url")) {
                        StartedActivity.mDestination = obj.getString("destination")
                        StartedActivity.mDestinationUrl = obj.getString("destination_url")
                        finishIntent(Activity.RESULT_OK)
                    } else {
                        finishIntent(Activity.RESULT_CANCELED)
                    }
                }

                override fun finishIntent(result: Int) {
                    val returnIntent = Intent()
                    setResult(result, returnIntent)
                    finish()
                }

                override fun onResize(height: Float) {
                }

            }, "Android")

            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    // do your handling codes here, which url is the requested url
                    // probably you need to open that url rather than redirect:
                    view.loadUrl(url)
                    return false // then it is not handled by default action
                }
            }

            webView.settings.javaScriptEnabled = true
            webView.settings.loadWithOverviewMode = true
            webView.settings.useWideViewPort = true
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
            webView.webChromeClient = WebChromeClient()

            webView.loadUrl(stringExtra ?: "")

        } else {
            cancelActivity()
        }
    }

    private fun cancelActivity() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_CANCELED, returnIntent)
        finish()
    }

    override fun onBackPressed() {
        if (webView.copyBackForwardList().currentIndex > 0) {
            webView.goBack()
        } else {
            cancelActivity()
        }
    }
}

abstract class WebAppInterface {

    /** Show a toast from the web page  */
    @JavascriptInterface
    fun callbackToAndroid(message: String) {
        onCallback(message)
    }

    /** Show a toast from the web page  */
    @JavascriptInterface
    fun resize(height: Float) {
        onResize(height)
    }

    abstract fun onCallback(jsonString: String)

    abstract fun finishIntent(result: Int)

    abstract fun onResize(height: Float)
}