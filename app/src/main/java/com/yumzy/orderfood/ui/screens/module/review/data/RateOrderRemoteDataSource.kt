package com.yumzy.orderfood.ui.screens.module.review.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import javax.inject.Inject


/**
 * Created by Bhupendra Kumar Sahu.
 */
class RateOrderRemoteDataSource  constructor(private val serviceAPI: OrderServiceAPI) :
    BaseDataSource() {
    suspend fun getRatingFeedback(selRatingFeedback: RatingFeedbackDTO) = getResult {
        serviceAPI.getRatingFeedback(selRatingFeedback)
    }
}