package com.yumzy.orderfood.ui.screens.login.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.ui.RegisterUserDTO
import com.yumzy.orderfood.databinding.FragmentCreateAccountBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class RegisterFragment : BaseFragment<FragmentCreateAccountBinding, LoginViewModel>(), ILoginView,
    ActionDialog.OnActionPerformed {
    
    override val vm: LoginViewModel  by viewModel()
    

    private val navController by lazy {
        findNavController()
    }
    lateinit var number: String


    override fun getLayoutId() = R.layout.fragment_create_account
    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        sharedPrefsHelper.saveIsTemporary(true)

//        val leftArrow = IconFontDrawable(requireContext(), R.string.icon_chevron_left)
//        leftArrow.textSize = 23f
//        bd.tvHeaderRegister.setCompoundDrawablesWithIntrinsicBounds(leftArrow, null, null, null)
//        bd.tvHeaderRegister.compoundDrawablePadding = 5.px
        bd.backArrow.setOnClickListener {
            navLoginFragment()
        }
        bd.btnSignup.setOnClickListener {
            val first_name = bd.edtFirstName.text ?: ""
            val last_name = bd.edtLastName.text ?: ""
            val email = bd.edtEmail.text ?: ""
            val number = bd.edtPhone.text ?: ""

            if (YumUtil.isValidNameIgnoreSpace(first_name.toString())) {
                vm.firstName = first_name.toString()

                if (YumUtil.isValidNameIgnoreSpace(last_name.toString())) {
                    vm.lastName = last_name.toString()

                    if (YumUtil.isValidEmail(email.toString())) {
                        vm.emailAddress = email.toString()

                        if (YumUtil.isValidMobile(number.toString())) {
                            vm.mobileNumber = number.toString()
                            hideKeyboard()
                            registerUser()


                        } else {
                            hideKeyboard()
                            showSnackBar(resources.getString(R.string.invalid_number), -1)
                        }
                    } else {
                        hideKeyboard()
                        showSnackBar(resources.getString(R.string.invalid_email), -1)
                    }
                } else {
                    hideKeyboard()
                    showSnackBar(resources.getString(R.string.invalid_last_name), -1)
                }
            } else {
                hideKeyboard()
                showSnackBar(resources.getString(R.string.invalid_first_name), -1)
            }

        }
//
//        bd.tvTermCondition.setOnClickListener {
//            ChromeTabHelper.launchChromeTab(
//                requireContext() as AppCompatActivity,
//                "https://laalsadev.sgp1.digitaloceanspaces.com/common_documents/Privacyandpolicy.html"
//            )
//        }
//        bd.tvLetsExplore.setOnClickListener {
//            ChromeTabHelper.launchChromeTab(requireContext() as AppCompatActivity, "https://yumzy.in/")
//
//        }
    }

    private fun registerUser() {


        vm.sendRegisterOTP(vm.mobileNumber)
            .observe(this, { result ->
                RHandler<Any>(
                    this,
                    result
                ) {
                    onResponse(APIConstant.Status.SUCCESS, null)


                }

            })

    }

    override fun onResponse(code: Int?, response: Any?) {
        when (code) {
            APIConstant.Status.SUCCESS -> {
                val userData = RegisterUserDTO(
                    vm.firstName,
                    vm.lastName,
                    vm.emailAddress,
                    vm.mobileNumber,
                    IConstants.FragmentFlag.FROM_REGISTER
                )
                val action =
                    RegisterFragmentDirections.fragmentRegisterToOtpVerify(userData)
                navController.navigate(action)
            }
            else -> super.onResponse(code, response)

        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        hideKeyboard()
        if (code == APIConstant.Code.USER_EXISTS) {

            val title = getString(R.string.registerd_user)
            val sMessage = message

            val actions: ArrayList<ActionItemDTO?> = arrayListOf(
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.login),
                ),
                ActionItemDTO(
                    key = ActionConstant.CLOSE,
                    text = getString(R.string.cancel),
                    textColor = ThemeConstant.textBlackColor,
                    buttonColor = ThemeConstant.textBlackColor,
                )

            )
            RegUserDialogAction(
                IConstants.ActionModule.USER_EXISTS,
                title,
                sMessage,
                actions
            )
        } else
            super.showCodeError(code, title, message)
    }

    private fun RegUserDialogAction(
        regUser: Int,
        title: String,
        sMessage: String,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            requireActivity(),
            regUser,
            title,
            sMessage,
            actions,
            this,
            iconString = resources.getString(R.string.icon_user_1)
        )
    }

    override fun navHomeActivity() {
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, iActionId: Int, sTransIds: String?) {
        if (iActionId == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            navLoginFragment()
        } else if (iActionId == IConstants.ActionModule.CONFIRM_DIALOG) {
            navLoginFragment()
        }
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {

        if (actionType == IConstants.ActionModule.USER_EXISTS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                navLoginFragment()
            }
        }
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                navLoginFragment()
            }
        }
    }




    private fun navLoginFragment() {
        val action = RegisterFragmentDirections.fragmentRegisterToLogin(bd.edtPhone.text.toString())
        navController.navigate(action)
       // navController.popBackStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: RegisterFragmentArgs by navArgs()
        number = args.phoneNumber.toString()
        vm.mobileNumber = number
        bd.edtPhone.setText(vm.mobileNumber)
    }

    fun hideKeyboard() {
        YumUtil.hideKeyboard(requireActivity())

    }
}