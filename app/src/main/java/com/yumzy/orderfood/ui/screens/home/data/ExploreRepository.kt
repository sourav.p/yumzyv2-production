/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */
package com.yumzy.orderfood.ui.screens.home.data

import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Repository module for handling data operations.
 */
class ExploreRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val remoteDataSource: HomeRemoteDataSource
) {

    val listData = databaseLiveData { userInfoDao.getUser() }

    fun getSearchSuggestion(text: String) = networkLiveData {
        remoteDataSource.getSearchSuggestion(text)
    }

    fun insertSearchItem(localSearchItem: String?) = backgroundLiveData {
//        localSearchItemDao.insertSearchItem(localSearchItem)
        ResponseDTO.success("", "", 0, true, 0, "")

    }

    fun clearSearchTable() = backgroundLiveData {
//        localSearchItemDao.clearSearch()
        ResponseDTO.success("", "", 0, true, 0, "")
    }

//    fun getExpandSearchResult(itemId: String) = networkLiveData {
//        remoteDataSource.getExpandSearch(itemId)
//    }


}