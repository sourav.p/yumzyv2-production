package com.yumzy.orderfood.ui.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont


abstract class BaseSheetFragment<B : ViewDataBinding, T : BaseViewModel<*>> :
    BottomSheetDialogFragment(),
    IModuleHandler {
    var moduleId: Int = 0
    open var actionHandler: IModuleHandler? = null

    open lateinit var bd: B
    abstract val vm: T

    private val mClickListener =
        View.OnClickListener { v -> onDebounceClick(v) }
    lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var mProgressBar: ProgressBar
    private lateinit var frameLayout: FrameLayout
    lateinit var rootView: ConstraintLayout


    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = View.inflate(context, getLayoutId(), null)
        //binding views to data binding.
        rootView = view as ConstraintLayout
        rootView.addView(initFrame())
        bd = DataBindingUtil.bind(rootView)!!
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onDialogCreated(view)
    }

    override fun getTheme(): Int = R.style.Theme_NoWiredStrapInNavigationBar

    fun applyDebouchingClickListener(vararg views: View?) {
        ClickUtils.applyGlobalDebouncing(views, mClickListener)
        ClickUtils.applyPressedViewScale(*views)
    }

    private fun showProgressBar(visibility: Boolean) {
        mProgressBar.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
        if (visibility) frameLayout.setBackgroundColor(0x3CFFFFFF) else frameLayout.background =
            null
        frameLayout.isClickable = visibility
    }

    override fun hideProgressBar() {
        showProgressBar(false)
    }

    override fun showProgressBar() {
        showProgressBar(true)

    }

    open fun showError(error: String) {
        val snackBar = Snackbar.make(rootView, error, Snackbar.LENGTH_SHORT)
        val viewGroup = snackBar.view as ViewGroup
        viewGroup.minimumHeight = 0
        viewGroup.background = ColorDrawable(Color.BLACK)
        val textView = (viewGroup.getChildAt(0) as ViewGroup).getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.maxLines = 3
        textView.setInterBoldFont()
        textView.textSize = 12f
        textView.gravity = Gravity.CENTER
        snackBar.show()
    }

    open fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    open fun showSnackBar(message: String, statusColor: Int) {
        val snackBar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT)
        val viewGroup = snackBar.view as ViewGroup
        viewGroup.minimumHeight = VUtil.dpToPx(0)
        viewGroup.getChildAt(0).background = null
        viewGroup.background = ColorDrawable(Color.BLACK)
        val textView = (viewGroup.getChildAt(0) as ViewGroup).getChildAt(0) as TextView
        textView.background = null
        textView.setTextColor(ThemeConstant.white)
        snackBar.setActionTextColor(Color.WHITE)
        ViewCompat.setOnApplyWindowInsetsListener(snackBar.view, null)
        snackBar.show()
    }

    open fun onDebounceClick(view: View) {}
    override fun showCodeError(code: Int?, title: String?, message: String) {
        when (code) {
            APIConstant.Code.LOGGED_OUT, APIConstant.Code.INVALID_USER -> {
                val requireActivity = requireActivity()
                if (requireActivity is BaseActivity<*, *>) {
                    requireActivity.showCodeError(code, title, message)
                }


            }

            APIConstant.Status.SESSION_EXPIRED -> {
                val requireActivity = requireActivity()
                if (requireActivity is BaseActivity<*, *>) {
                    requireActivity.showCodeError(code, title, message)
                }
            }
            else -> {
                showError(message)
            }
        }

    }

    override fun onResponse(code: Int?, response: Any?) {}

    abstract fun getLayoutId(): Int
    abstract fun onDialogCreated(view: View)
    private fun initFrame(): FrameLayout {
        frameLayout = FrameLayout(requireContext())
        val frameParam = ConstraintLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        frameParam.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
        frameParam.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
        frameParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        frameParam.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
        frameLayout.background = null
//        frameParam.gravity = Gravity.CENTER
        frameLayout.layoutParams = frameParam
        frameLayout.id = R.id.base_framelayout
        mProgressBar = ProgressBar(requireContext())
        mProgressBar.background = null
        mProgressBar.visibility = View.GONE
//        mProgressBar.elevation = 6f
        val pbParam: FrameLayout.LayoutParams = FrameLayout.LayoutParams(45.px, 45.px)
        pbParam.gravity = Gravity.CENTER
//        pbParam.setMargins(0, UIHelper.i25px, 0, 0)
        mProgressBar.layoutParams = pbParam
        mProgressBar.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        frameLayout.addView(mProgressBar)
        return frameLayout

    }


}