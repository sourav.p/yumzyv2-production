package com.yumzy.orderfood.ui.screens.profile.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

class ProfileRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val remoteSource: ProfileRemoteDataSource
) {
    fun logoutUser() = networkLiveData { remoteSource.logoutUser() }
    fun removeUserDTO() = backgroundLiveData {
        userInfoDao.removeUser()
        return@backgroundLiveData ResponseDTO.success(null, null, null, true, null, null)

    }

    fun removeSelectedAddress() = remoteSource.removeSelectedAddress()

    fun profileUser() = networkLiveData<Any>(
        networkCall = {

            remoteSource.profileUser()
        }
    ).distinctUntilChanged()

    fun updateProfileUser(map: Map<String, String>) = networkLiveData<Any>(
        networkCall = {

            remoteSource.updateProfileUser(map)
        }
    ).distinctUntilChanged()

    fun userUpdateProfileImage(pro: MutableMap<String, Any>) = networkLiveData<Any>(
        networkCall = {

            remoteSource.userUpdateProfileImage(pro)
        }
    ).distinctUntilChanged()

    fun getAddressList() = networkLiveData(
        networkCall = {
            remoteSource.getAddressList()
        }
    ).distinctUntilChanged()

    fun updateAddress(address: AddressDTO) = networkLiveData(
        networkCall = {
            remoteSource.updateAddress(address)
        }
    ).distinctUntilChanged()

    fun addAddress(address: AddressDTO) = networkLiveData(
        networkCall = {
            remoteSource.addAddress(address)
        }
    ).distinctUntilChanged()

    fun deleteAddress(addressId: String) = networkLiveData(
        networkCall = {
            remoteSource.deleteAddress(addressId)
        }
    ).distinctUntilChanged()

    fun getHelpHeadings() = networkLiveData(
        networkCall = {
            remoteSource.getHelpHeadings()
        }
    )

    fun getUserDTO() = databaseLiveData {
        userInfoDao.getUser()
    }

    fun getHelpSubheadings(headingId: Int) = networkLiveData(
        networkCall = {
            remoteSource.getHelpSubheadings(headingId)
        }
    )

    fun getWalletTransactions(skip: Int, limit: Int, mobile: Long ) = networkLiveData (
        networkCall = {
            remoteSource.getWalletTransactions(skip, limit, mobile)
        }
    )

    fun getUserWallet(mobile: Long) = networkLiveData (
        networkCall = {
            remoteSource.getUserWallet(mobile)
        }
    )
    fun verifyVPA(vpa:  String) = networkLiveData (
        networkCall = {
            remoteSource.verifyVPA(vpa)
        }
    )
        .distinctUntilChanged()

    fun withdrawBalance(withDraw: MutableMap<String, Any>) = networkLiveData (
        networkCall = {
            remoteSource.withdrawBalance(withDraw)
        }

    )
        .distinctUntilChanged()
}