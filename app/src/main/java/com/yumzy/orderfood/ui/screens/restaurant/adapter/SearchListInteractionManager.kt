package com.yumzy.orderfood.ui.screens.restaurant.adapter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.models.RestaurantItem

class SearchListInteractionManager {

    private val _selectedNotes: MutableLiveData<ArrayList<RestaurantItem>> = MutableLiveData()

    val selectedNotes: LiveData<ArrayList<RestaurantItem>>
        get() = _selectedNotes


    fun getSelectedNotes(): ArrayList<RestaurantItem> = _selectedNotes.value ?: ArrayList()


    fun addOrRemoveNoteFromSelectedList(note: RestaurantItem) {
        var list = _selectedNotes.value
        if (list == null) {
            list = ArrayList()
        }
        if (list.contains(note)) {
            list.remove(note)
        } else {
            list.add(note)
        }
        _selectedNotes.value = list
    }

    fun isNoteSelected(note: RestaurantItem): Boolean {
        return _selectedNotes.value?.contains(note) ?: false
    }

    fun clearSelectedNotes() {
        _selectedNotes.value = null
    }

}









