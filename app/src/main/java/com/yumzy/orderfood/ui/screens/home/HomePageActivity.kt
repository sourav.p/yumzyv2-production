package com.yumzy.orderfood.ui.screens.home

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.viewpager2.widget.ViewPager2
import com.clevertap.android.sdk.CleverTapAPI
import com.laalsa.laalsalib.utilcode.util.AppUtils
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsalib.utilcode.util.ThreadUtils
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.UserDTO
import com.yumzy.orderfood.data.models.appstart.AppStartDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityHomePageBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.tools.update.RemoteConfigUpdate
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.CustomizeOptionDialog
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.component.chromtab.ChromeTabHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.home.adapter.BottomPageAdapter
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.location.LocationSearchActivity
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.module.address.LocationMapAction
import com.yumzy.orderfood.ui.screens.module.address.OrderTrackingService
import com.yumzy.orderfood.ui.screens.offer.OfferActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardActivity
import com.yumzy.orderfood.ui.screens.setupWithNavController
import com.yumzy.orderfood.ui.screens.wallet.WalletActivity
import com.yumzy.orderfood.ui.screens.web.WebActivity
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.zoho.livechat.android.ZohoLiveChat
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.TimeUnit


class HomePageActivity : BaseActivity<ActivityHomePageBinding, HomePageViewModel>(), IHomePageView,
    ICartHandler {


    //    private val homeFragment: HomeFragment = HomeFragment()
//    private var exploreFragment: ExploreFragment? = null
//    private var plateFragment: PlateFragment? = null
//    private var accountFragment: AccountFragment? = null
    private var bottomPageAdapter: BottomPageAdapter? = null
    //private var isNewUser: Boolean = false
    private var timer: Timer? = null
    private var currentPage = 0
    var DELAY_MS: Long = 0
    var PERIOD_MS: Long = 0
    var userId: String = ""
    var cartId: String? = ""
    var outletId: String = ""

    override val navigationPadding: Boolean = true
    override val enableEdgeToEdge = true
    var mUserSavedAddress: AddressDTO? = null
    var mUserCurrentLocationState: IConstants.LocationStatus? = null
    private var currentNavController: LiveData<NavController>? = null
    
    override val vm: HomePageViewModel by viewModel()

    val cartHelper: CartHelper by inject()

    lateinit var user: UserDTO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_home_page)

        vm.attachView(this)
//        if (savedInstanceState == null) {
//            setupBottomNavigationBar()
//        }

        setupBottomNavigationBar()
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.TRANSPARENT)
        setUserDTO()
        RemoteConfigUpdate.with(this).onUpdateNeeded(this).check()
//        isNewUser = intent.getBooleanExtra(IConstants.ActivitiesFlags.NEW_USER, false)
//        if (intent.hasExtra(IConstants.ActivitiesFlags.OUTLET_ID)) {
//            outletId = intent.getStringExtra(IConstants.ActivitiesFlags.OUTLET_ID) ?: ""
//            Handler(Looper.getMainLooper()).postDelayed({
//                val intent = Intent(this, OutletActivity::class.java)
//                intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
//                startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
//            }, 3000)
//        }
//        userId = sharedPrefsHelper.getUserId() ?: ""
//        cartId = sharedPrefsHelper.getCartId() ?: ""
//        //observeAddress()
//        getUserCart()
//        setPlateBadge()
//        pushFcmToken(YumzyFirebaseMessaging.getToken(this))
        //showToast("Dummy User = " + sharedPrefsHelper.getIsTemporary().toString())
    }

    private fun pushFcmToken(token: String?) {
        ZohoLiveChat.Notification.enablePush(
            token,
            !BuildConfig.DEBUG
        ) //true -> for test devices
        CleverTapAPI.getDefaultInstance(this)?.pushFcmRegistrationId(token, true)

    }

    private fun setupBottomNavigationBar() {

        val navGraphIds = listOf(
            R.navigation.nav_explore,
            R.navigation.nav_explore,
            R.navigation.nav_plate,
            R.navigation.nav_account
        )
        // Setup the bottom navigation view with a list of navigation graphs
        val controller = bd.navigationBottom.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_container,
            intent = intent
        )
        bd.navigationBottom.selectedItemId

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, { navController ->
            val displayName = navController.currentDestination?.label ?: ""
            val navId = when {
                displayName.contains("explore", true) -> R.id.nav_explore
                displayName.contains("plate", true) -> R.id.nav_plate
                displayName.contains("account", true) -> R.id.nav_account
                else -> R.id.nav_home/*displayName.contains("home", true)*/
            }

            /*val navHostFragment: Fragment? = supportFragmentManager.primaryNavigationFragment
            val currentFragment: Fragment? =
                navHostFragment?.childFragmentManager?.fragments?.get(0)

            val navigationitemId: Int
            when (currentFragment) {
                is HomeFragment -> {
                    navigationitemId = R.id.nav_home
                }
                is ExploreFragment -> {
                    navigationitemId = R.id.nav_explore
                }
                is PlateFragment -> {
                    navigationitemId = R.id.nav_plate

                }
                is AccountFragment -> {
                    navigationitemId = R.id.nav_account

                }
                else -> {
                    navigationitemId = R.id.nav_home

                }
            }*/
            navigateToFragment(navId, false)
        })
        currentNavController = controller
//        currentNavController?.value?.addOnDestinationChangedListener { _, destination, _ ->
//            when (destination.id) {
//                R.id.homeFragment -> bd.navigationBottom.isGone=false
//                R.id.plateFragment -> bd.navigationBottom.isGone=true
//                R.id.exploreFragment -> bd.navigationBottom.isGone=true
//                R.id.accountOptionsFragment -> bd.navigationBottom.isGone=true
//                else ->bd.navigationBottom.isGone=true
//            }
//        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(AppGlobalObjects.selectedAddress != null ) handleNotification()
        if (intent?.hasExtra("show_home") == true && intent.getBooleanExtra("show_home", false)) {
            navigateToFragment(R.id.nav_home, true)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    fun handleNotification() {

        if (StartedActivity.mDestination != "" && StartedActivity.mDestinationUrl != "") {

            val url = StartedActivity.mDestinationUrl

            when (StartedActivity.mDestination) {

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_APP_HOME -> {
                    navigateToFragment(R.id.nav_home, true)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_REWARD -> {
                    AppIntentUtil.launchClipReveal(
                        bd.root,
                        this,
                        ScratchCardActivity::class.java
                    )
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_EXPLORE -> {
                    Handler(Looper.getMainLooper()).postDelayed({
                        navigateToFragment(
                            R.id.nav_explore,
                            true
                        )
                    }, 600L)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_PLATE -> {
                    Handler(Looper.getMainLooper()).postDelayed({
                        navigateToFragment(
                            R.id.nav_plate,
                            true
                        )
                    }, 600L)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_WALLET -> {
                    AppIntentUtil.launchClipReveal(
                        bd.root,
                        this,
                        WalletActivity::class.java
                    )
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_INVITE -> {
                    AppIntentUtil.launchClipReveal(
                        bd.root,
                        this,
                        InviteActivity::class.java
                    )
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_FAVOURITES -> {
                    Handler(Looper.getMainLooper()).postDelayed({
                        ActionModuleHandler.showFavouriteRestaurantDialog(
                            this,
                            "",
                            null
                        )
                    }, 600L)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_FOOD_PREF -> {
                    Handler(Looper.getMainLooper()).postDelayed({
                        ActionModuleHandler.showFoodPreference(
                            this, this
                        )
                    }, 600L)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_OFFER -> {
                    val offerIntent = Intent(this, OfferActivity::class.java)
                    offerIntent.putExtra(
                        IConstants.ActivitiesFlags.FLAG,
                        IConstants.ActivitiesFlags.FROM_HOME
                    )
                    AppIntentUtil.launchClipRevealFromIntent(bd.root, this, offerIntent)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_URL -> {
                    ChromeTabHelper.launchChromeTab(this, url)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_ORDER_TRACKING -> {
                    val orderTrackingId = YumUtil.getQueryParam(
                        "orderId",
                        url
                    )
                    if (orderTrackingId.isNotBlank()) {
                        ActionModuleHandler.showOrderTracking(this, orderTrackingId)
                    }
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_PROMO_OUTLET -> {
                    val promoId = YumUtil.getQueryParam("promoId", url)
                    val name = YumUtil.getQueryParam("name", url)
                    ActionModuleHandler.showTopRestaurantDialog(this, promoId, name, null, this)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_PROMO_DISH -> {
                    val promoId = YumUtil.getQueryParam("promoId", url)
                    val name = YumUtil.getQueryParam("name", url)
                    ActionModuleHandler.showTopDishesDialog(this, promoId, name, this)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_DISH -> {
                    val outletId = YumUtil.getQueryParam(
                        "outletId",
                        url
                    )
                    val itemId = YumUtil.getQueryParam(
                        "itemId",
                        url
                    )
                    Handler(Looper.getMainLooper()).postDelayed({
                        val intnt = Intent(this, OutletActivity::class.java)
                        intnt.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
                        intnt.putExtra(IConstants.ActivitiesFlags.ITEM_ID, itemId)
                        intnt.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        startActivityForResult(intnt, OutletActivity.REStAURANT_REQ_CODE)
                    }, 600)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OUTLET -> {
                    val outletId = YumUtil.getQueryParam(
                        "outletId",
                        url
                    )
                    Handler(Looper.getMainLooper()).postDelayed({
                        val intnt = Intent(this, OutletActivity::class.java)
                        intnt.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
                        intnt.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        startActivityForResult(intnt, OutletActivity.REStAURANT_REQ_CODE)
                    }, 600)
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_REFERRAL -> {
                    if (!sharedPrefsHelper.getIsTemporary()) {
                        val referral = YumUtil.getQueryParam("referralCode", url)
                        vm.applyReferral(userId, referral).observe(this, { result ->
                            if (result.type == 200 && result.code == APIConstant.Code.DATA_UPDATED) {
                                AppUIController.showApplyREferralUI()
                                sharedPrefsHelper.saveIsNewUser(false)

                            } else if (result.type == APIConstant.Status.ERROR && result.code == APIConstant.Code.INVALID_REFERRAL) {
                                showSnackBar(
                                    result.message!!,
                                    -1
                                )
                                sharedPrefsHelper.saveIsNewUser(false)

                            }
                        })
                    }
                }

                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_UPDATE -> {
                    redirectStore(StartedActivity.mDestinationUrl)
                }

            }

            StartedActivity.mDestination = ""
            StartedActivity.mDestinationUrl = ""
            navigateToFragment(R.id.nav_home, true)
        }
    }

    private fun getUserCart() {
        try {
            cartHelper.getCart().observeForever { }
        } catch (e: Exception) {
            AppUtils.relaunchApp()
        }
    }


    private fun setBadge(count: Int) {
        if (count == 0) {
            bd.navigationBottom.removeBadge(R.id.nav_plate)
        } else {
            val badge =
                bd.navigationBottom.getOrCreateBadge(R.id.nav_plate) // previously showBadge
            badge.number = count
            badge.backgroundColor = ThemeConstant.greenConfirmColor
            badge.badgeTextColor = ThemeConstant.white
        }
    }

    private fun setPlateBadge() {
        vm.getSelectedItems().observe(this, {
            if (it != null) {
                val size = it.sumBy { item -> item.value }
                setBadge(size)
            } else
                setBadge(0)


        })
    }

    private fun observeAddress() {
        if (bd.fragmeFetchingLocation.visibility == View.GONE) {
            bd.fragmeFetchingLocation.setOnClickListener { }
            bd.fragmeFetchingLocation.visibility = View.VISIBLE
            bd.includeFetchingLocation.tvCloseApp.setOnClickListener { finishAffinity() }
            bd.includeFetchingLocation.btnPickerAddress.setOnClickListener {
                startActivity(Intent(this, LocationSearchActivity::class.java))
            }

        }

        AppGlobalObjects.getAddressLiveData.observeForever {
            if (it != null) {
                sharedPrefsHelper.saveAddressTag(it.name)
                sharedPrefsHelper.savedCityLocation(it.googleLocation)
                sharedPrefsHelper.saveFullAddress(it.fullAddress)
                updateCurrentLocation(it)
                handleNotification()
            }
        }

        if (sharedPrefsHelper.getSelectedAddress() == null) {
            //get location from remote database
            vm.getAddressList().observe(this, { result ->
                RHandler<List<AddressDTO>>(
                    this,
                    result
                ) {
                    if (!it.isNullOrEmpty()) {
                        AppGlobalObjects.selectedAddress = it[0]
                    } else {
                        //start location picker activity
                        startLocationPicker()
                    }
                }
            })
        } else {
            AppGlobalObjects.selectedAddress = sharedPrefsHelper.getSelectedAddress()
        }

    }

    private fun startLocationPicker() {
        if (this::user.isInitialized && !user.address.isNullOrEmpty()) {
            vm.setSelectedAddress(user.address?.get((user.address?.size ?: 1) - 1)!!)
        } else {
            bd.includeFetchingLocation.tvDeliverTo.text = getString(R.string.oops)
            bd.includeFetchingLocation.tvLocationMessage.text =
                getString(R.string.did_not_find_saved_location)
            bd.includeFetchingLocation.tvFullLocationMessage.text =
                getString(R.string.pick_again_location)
            bd.includeFetchingLocation.llFooter.visibility = View.GONE
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, LocationSearchActivity::class.java))
                Handler(Looper.getMainLooper()).postDelayed({
                    bd.includeFetchingLocation.llFooter.visibility = View.VISIBLE
                }, 700)
            }, 700)
        }
    }

    private fun updateCurrentLocation(addressDTO: AddressDTO) {

        bd.includeFetchingLocation.llFooter.visibility = View.GONE
        bd.includeFetchingLocation.tvDeliverTo.text = getString(R.string.delivering_to)
        bd.includeFetchingLocation.tvLocationMessage.text =
            " ${addressDTO.name.capitalize(Locale.ROOT)}"
        bd.includeFetchingLocation.tvFullLocationMessage.text = "${addressDTO.fullAddress}"

        //updating current location
        vm.updateCurrentLocation(
            addressDTO.longLat?.coordinates?.get(0).toString(),
            addressDTO.longLat?.coordinates?.get(1).toString(),
            addressDTO.city,
            addressDTO.googleLocation
        ).observe(this, Observer {

            if (it.code == APIConstant.Code.DATA_UPDATED) {
                CleverTapHelper.updateLatLongUserLocation(
                    addressDTO.googleLocation,
                    addressDTO.city,
                    addressDTO.longLat?.coordinates?.get(0) ?: 0.0,
                    addressDTO.longLat?.coordinates?.get(1) ?: 0.0
                )
            }


            //this prevent calling fetchhome data multiple times, duplicates the data in ui
            if (it.type == -1) return@Observer

//            val navHostFragment: NavHostFragment? =
//                supportFragmentManager.findFragmentById(R.id.nav_home)
//            navHostFragment!!.childFragmentManager.fragments[0]
            /*  val fragment = supportFragmentManager.fragments?.get(0)
              if (fragment is HomeFragment){
                  fragment.onResume()
              }*/


            //fetching home location
            Handler(Looper.getMainLooper()).postDelayed({

                val slideDown: Animation =
                    AnimationUtils.loadAnimation(this, R.anim.slide_to_down)
                if (bd.fragmeFetchingLocation.visibility == View.VISIBLE) {
                    bd.fragmeFetchingLocation.startAnimation(slideDown)
                    bd.fragmeFetchingLocation.visibility = View.GONE


                }

                mUserSavedAddress = addressDTO
//                homeFragment.updateSelectedAddress()
//                homeFragment.fetchHomeData(
//                    mUserSavedAddress!!.longLat?.coordinates?.get(1).toString(),
//                    mUserSavedAddress!!.longLat?.coordinates?.get(0).toString()
//                )
                promotionForNewUser()
//                navigateToFragment(R.id.nav_home, true)
            }, 1200)
        })

    }

    private fun promotionForNewUser() {
        val isNewUser = sharedPrefsHelper.getIsNewUser()
        val userId = sharedPrefsHelper.getUserId() ?: ""
        if (isNewUser) {
            ActionModuleHandler.showApplyReferralDialog(this, userId, this)
            sharedPrefsHelper.saveIsNewUser(false)
        }
    }

/* fun fetchUserCart(address: AddressDTO?) {
//       val selAddress= addressHolder.value
     if (address != null) {
         val currentCity = globalAddress?.city ?: ""
         val cartLongLatDTO = CartLongLatDTO(
             arrayListOf(
                 globalAddress?.longLat?.coordinates?.get(0) ?: 0.0,
                 globalAddress?.longLat?.coordinates?.get(1) ?: 0.0
             )
         )
         callPreCheckoutAPI(cartLongLatDTO, currentCity, globalAddress!!)
     }
 }*/


    fun appStart() {
        //if (!(visibleFragment is HomeFragment)) return
        vm.getAppStart().observe(this,
            Observer { result ->
                if (result.type == APIConstant.Status.LOADING || result.type == APIConstant.Status.ERROR) return@Observer
                RHandler<AppStartDTO>(
                    this,
                    result
                ) {
                    setupBottomInfoPager(it)

                }

            })
    }

    private fun setupBottomInfoPager(appStartDTO: AppStartDTO) {
        if (!appStartDTO.infoBar.isNullOrEmpty()) {
            currentPage = 0
            timer?.cancel()
            timer?.purge()
            DELAY_MS = 0
            PERIOD_MS = 0
            bd.vpInfoBanner.adapter = null

            bottomPageAdapter = BottomPageAdapter(this, appStartDTO.infoBar, this)
            bd.vpInfoBanner.adapter = bottomPageAdapter

            DELAY_MS = 1000
            PERIOD_MS = 6000
            val handler = Handler(mainLooper)
            val update = Runnable {
                if (currentPage == appStartDTO.infoBar.size) {
                    currentPage = 0
                }
                bd.vpInfoBanner.setCurrentItem(currentPage++, true)
            }
            timer = Timer()
            timer?.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(update)
                }
            }, DELAY_MS, PERIOD_MS)
            bd.vpInfoBanner.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    currentPage = position
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                }
            })

        } else {
            if (timer != null) {
                currentPage = 0
                timer?.cancel()
                timer?.purge()
                DELAY_MS = 0 //delay in milliseconds before task is to be executed
                PERIOD_MS = 0
            }
            bd.vpInfoBanner.adapter = null
        }
    }


    private fun startOrderTrackingService() {
        val intent = Intent(this, OrderTrackingService::class.java)
        intent.putExtra(
            IConstants.OrderTracking.ORDER_TRACK_ID,
            sharedPrefsHelper.getCurrentOrderId()
        )
        startService(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        return navigateToFragment(item.itemId, false)
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        when (requestCode) {
            IConstants.ActivitiesFlags.ADDRESS_PICKER_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
//                    plateFragment?.updateCart(data?.getSerializableExtra("address") as AddressDTO)
                }
                /*try {
                    hideLocationDialog()
                    if (data?.getStringExtra("address") != null) {
    //                        val address = data.getStringExtra("address")
                        val currentLatitude = data.getDoubleExtra("lat", 0.0)
                        val currentLongitude = data.getDoubleExtra("long", 0.0)
                        fetchSelectedAddress(
                            currentLatitude, currentLongitude
                        )

                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }*/
            }
            IConstants.ActivityResultCode.SUCCESS -> {
                val MBuddle = data!!.extras
                val MMessage = MBuddle!!.getString(IConstants.ActivityResultCode.FLAG)
                if (MMessage.equals(IConstants.ActivityResultCode.IS_SUCCESS)) {
                    bd.fragmeFetchingLocation.visibility = View.GONE
                    // promotionForNewUser()

                }
            }
            IConstants.ActivityResultCode.FAILURE -> {
                val MBuddle = data!!.extras
                val MMessage = MBuddle!!.getString(IConstants.ActivityResultCode.FLAG)
                if (MMessage.equals(IConstants.ActivityResultCode.IS_CANCEL)) {
                    bd.includeFetchingLocation.llFooter.visibility = View.VISIBLE
                    bd.includeFetchingLocation.tvDeliverTo.text = ""
                    bd.includeFetchingLocation.tvLocationMessage.text = getString(R.string.oops)
                    bd.includeFetchingLocation.tvFullLocationMessage.text =
                        getString(R.string.unable_to_findout_location)

                }

            }
            OutletActivity.REStAURANT_REQ_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    hidePromoDialog()
                    navigateToFragment(R.id.nav_plate, true)
                }
            }

            WebActivity.REQ_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    handleNotification()
                }
            }

            else -> super.onActivityResult(requestCode, resultCode, data)

        }

        if (data?.hasExtra(IConstants.ActivitiesFlags.SHOW_PLATE) == true && data.getIntExtra(
                IConstants.ActivitiesFlags.SHOW_PLATE,
                -1
            ) == OutletActivity.REStAURANT_REQ_CODE
        ) {
            hidePromoDialog()
            navigateToFragment(R.id.nav_plate, true)
        }

    }

    private fun hidePromoDialog() {
        if (ActionModuleHandler.mTopDishesDialog != null) {
            ActionModuleHandler.mTopDishesDialog?.dismiss()
        }
        if (ActionModuleHandler.mTopRestaurantsDialog != null) {
            ActionModuleHandler.mTopRestaurantsDialog?.dismiss()
        }
        if (ActionModuleHandler.mFavouritesDialog != null) {
            ActionModuleHandler.mFavouritesDialog?.dismiss()
        }
    }


    fun navigateToFragment(itemId: Int, changeIcon: Boolean): Boolean {
        bd.navigationBottom.selectedItemId = itemId
        YumUtil.hideKeyboard(this)
        showProgressBar()
        BarUtils.transparentStatusBar(this)
        when (itemId) {
            R.id.nav_home -> {
                bd.navigationBottom.visibility = View.VISIBLE
                bottomNavigationVisible(true)
                setupInfoPagerVisibility(true)
                DELAY_MS = 0
                PERIOD_MS = 0
                appStart()
            }
            R.id.nav_explore -> {
                bottomNavigationVisible(true)
                setupInfoPagerVisibility(false)


            }
            R.id.nav_plate -> {
                /*Handler(Looper.getMainLooper()).postDelayed(
                    { plateFragment?.fetchUserCart(null) },
                    500
                )*/
                bottomNavigationVisible(false)
                setupInfoPagerVisibility(false)

            }

            R.id.nav_account -> {

                bottomNavigationVisible(true)
                setupInfoPagerVisibility(false)
            }
        }
        hideProgressBar()
        return true
    }

    private fun setupInfoPagerVisibility(visiblity: Boolean) {
        val isVisible = if (visiblity) View.VISIBLE else View.GONE
        bd.vpInfoBanner.visibility = isVisible

    }

    private fun bottomNavigationVisible(isVisible: Boolean) {
        val visibility = if (isVisible)
            View.VISIBLE
        else
            View.GONE
        bd.navigationBottom.visibility = visibility
    }

    /*override fun onBackPressed() {

        if (!(visibleFragment is HomeFragment)) {
            navigateToFragment(R.id.navigation_home, true)
            return
        } else
            finish()
    }*/


    private fun setUserDTO() {
        vm.getAppUser().observe(this, { userDTO ->
            userDTO?.also {
                user = it
            }
        })
    }


    override fun displayNewEvent() {

    }

    override fun onSuccess(actionId: Int, data: Any?) {
        when (actionId) {

            IConstants.ActionModule.APPLY_RATING -> {
                AppUIController.showThankYouRate()
                DELAY_MS = 0
                PERIOD_MS = 0
                appStart()

            }
            IConstants.ManagerAddress.CLOSE_DIALOG -> {

            }
            IConstants.ActionModule.APPLY_REFERRAL -> {
            }
            IConstants.ActionModule.ADD_ADDRESS -> {
                if (data == IConstants.ManagerAddress.UPDATE_AUTOMATICALLY) {
                    ActionModuleHandler.showConfirmLocation(
                        this,
                        LocationMapAction.MapActionFetch(),
                        this
                    )
                } else if (data == IConstants.ManagerAddress.UPDATE_MANUALLY) {
                    //                    dismissAddressDialog()
                    //                    addressDialog = ActionModuleHandler.showSelectDeliverLocation(this, this)
                }
            }
            IConstants.ActionModule.SELECT_DELIVER_LOCATION -> {
                vm.setSelectedAddress(data as AddressDTO)
//                plateFragment?.updateCart(data)
            }

            IConstants.ActionModule.OPEN_PLATE -> {
                navigateToFragment(R.id.nav_plate, true)
            }

        }
    }


    override fun onDebounceClick(view: View) {
        when (view.id) {
            else -> super.onDebounceClick(view)
        }

    }

    fun displayGpsDialog(view: View?) {
        if (view != null) {
            AppIntentUtil.launchWithBaseAnimation(view, this, LocationSearchActivity::class.java)
        } else
            startActivity(Intent(this, LocationSearchActivity::class.java))
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {

    }

    override fun onDestroy() {
        stopService(Intent(this, OrderTrackingService::class.java))
        CleverTapHelper.appExited()
        super.onDestroy()
    }

    override fun onUpdateNeeded(updateUrl: String) {

        ActionModuleHandler.showActionDialog(
            this,
            IConstants.ActionModule.CONFIRM_DIALOG,
            getString(R.string.app_update_message),
            getString(R.string.update),
            getString(R.string.cancel),
            object : CustomizeOptionDialog.OnActionPerformed {
                override fun onConfirmDone(
                    dialog: Dialog?,
                    actionType: Int,
                    label: String?
                ) {
                    showError("Please Wait")
                    redirectStore(updateUrl)
                    Handler(Looper.getMainLooper()).postDelayed({ finish() }, 500)
                }

                override fun onDismissed(actionType: Int, actionLabel: String?) {
                    if (actionType != 0) {
                        showError("Closing app.")
                        Handler(Looper.getMainLooper()).postDelayed({ finish() }, 500)
                    }
                }
            }
        )

    }

    private fun redirectStore(updateUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    var repatTask: ThreadUtils.Task<Any>? = null
    private fun timlyUpdateBottomViewPger() {


        repatTask = object : ThreadUtils.Task<Any>() {
            override fun doInBackground(): Any? {
                return ""
            }

            override fun onSuccess(orderStatus: Any) {
                //appStart()
            }

            override fun onFail(p0: Throwable?) {
//                showSnackBar(p0?.message.toString(), -1)
            }

            override fun onCancel() {}
        }

        ThreadUtils.executeByCachedAtFixRate(
            repatTask,
            IConstants.OrderTracking.ORDER_TRACKING_DURATION,
            TimeUnit.MILLISECONDS
        )
    }

    override fun itemAdded(totalItemCount: Int) {
        if (ActionModuleHandler.mTopDishesDialog != null && ActionModuleHandler.mTopDishesDialog?.isVisible == true) {
            ActionModuleHandler.mTopDishesDialog?.dismiss()
        }
        navigateToFragment(R.id.nav_plate, true)
    }


    override fun itemRepeated() {}

    override fun failedToAdd(message: String, gotoPlate: Boolean) {
        if (message != "") showSnackBar(message, -1)
    }
}
