package com.yumzy.orderfood.ui.screens.login.fragment

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.PreCheckoutReqDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.home.data.HomePageRepository
import com.yumzy.orderfood.ui.screens.home.fragment.IFetchLocAnim
import javax.inject.Inject


class FetchLocationAnimViewModel  constructor(val repository: HomePageRepository) :
    BaseViewModel<IFetchLocAnim>() {


    fun getAppUser() = repository.getUserDTO()
    fun updateCurrentLocation(latitude: String, longitude: String, city: String, locality: String) =
        repository.updateLocation(latitude, longitude, city, locality)

    fun postCheckout(orderId: String) = repository.postCheckout(orderId)
    fun preCheckout(preCheckout: PreCheckoutReqDTO) = repository.preCheckout(preCheckout)


    fun getSelectedAddress() = repository.getSelectedAddress()

    fun setSelectedAddress(addressDTO: AddressDTO) = repository.setSelectedAddress(addressDTO)

    fun getAddressList() = repository.getAddressList()
    fun applyReferral(userId: String, referralCode: String) =
        repository.applyReferral(userId, referralCode)
}