/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.home.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.data.models.PreCheckoutReqDTO
import com.yumzy.orderfood.data.models.ScratchSummaryDTO
import com.yumzy.orderfood.data.models.UpdateLocationDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.ui.screens.home.adapter.*
import com.yumzy.orderfood.util.IConstants
import javax.inject.Inject

class HomeRemoteDataSource  constructor(private val serviceAPI: HomeServiceAPI) :
    BaseDataSource() {

    suspend fun getProfile() = getResult {
        serviceAPI.getProfile()
    }

    suspend fun profileUser() = getResult {
        serviceAPI.profileUser()
    }

    var requestingHome = false

    suspend fun getFlatHomeData(
        latitude: String,
        longitude: String,
        skip: Int,
        limit: Int
    ): ResponseDTO<ArrayList<HomeListDTO>> {
        if (requestingHome) {
            return ResponseDTO.success(
                "Loading Home",
                "Not come in production will add loading dont raise issue on this",
                APIConstant.Status.LOADING,
                null,
                APIConstant.Status.IGNORE,
                null
            )
        }
        requestingHome = true
//        runBlocking {
//            return@runBlocking
//        }
        val arrayList = ArrayList<HomeListDTO>()
        val result = getResult {
            serviceAPI.getFlatHomeData(latitude, longitude, skip, limit)
        }
//        runBlocking {


        kotlin.synchronized(arrayList) {
            result.data?.forEach { homeSectionItem ->

                val layoutType = homeSectionItem.layoutType
                when (layoutType) {
                    IConstants.LayoutType.Banner1000.TOP_BANNER,
                    IConstants.LayoutType.Banner1000.ADS_ITEM,
                    IConstants.LayoutType.Banner1000.OFFER_ADS -> {
                        val topBanner = HomeListDTO(layoutType)
                        topBanner.displayOrderSeq = homeSectionItem.displayOrderSeq
                        topBanner.heading = homeSectionItem.heading
                        topBanner.nestedList = homeSectionItem.list
                        topBanner.orientation = homeSectionItem.orientation
                        topBanner.subHeading = homeSectionItem.subHeading
                        if (homeSectionItem.list.size > 0) {
                            val homeListDTO = homeSectionItem.list[0]
                            topBanner.firstPromoId = homeListDTO.promoId ?: ""
                        }
                        arrayList.add(topBanner)
                        return@forEach
                    }

                }
                val homeTitle = HomeListDTO(IConstants.LayoutType.Banner1000.HEADING)
                homeTitle.heading = homeSectionItem.heading
                homeTitle.subHeading = homeSectionItem.subHeading
                homeTitle.orientation = homeSectionItem.orientation
                if (homeSectionItem.list.size > 0) {
                    val homeListDTO = homeSectionItem.list[0]
                    homeTitle.firstPromoId = homeListDTO.promoId ?: ""
                    homeTitle.landingPage = homeListDTO.landingPage
                    homeTitle.layoutType = homeSectionItem.layoutType
                }
                arrayList.add(homeTitle)

                when (layoutType) {
                    IConstants.LayoutType.Banner1000.THE_BRANDS,
                    IConstants.LayoutType.Restaurant2000.NEW_LAUNCHED,
                    IConstants.LayoutType.Banner1000.POPULAR_CUISINES,
                    IConstants.LayoutType.Dish3000.WINTER_SPECIAL,
                    IConstants.LayoutType.Web4000.WEB_4001,
                    IConstants.LayoutType.Dish3000.POPULAR_FOOD,
//                    IConstants.LayoutType.Dish3000.YUMZY_BEST,
//                IConstants.LayoutType.Restaurant2000.FEATURED_BRANDS,
//                IConstants.LayoutType.Restaurant2000.YUMZY_EXCLUSIVE
                    -> {

                        val homeItem = HomeListDTO(layoutType)
                        homeItem.displayOrderSeq = homeSectionItem.displayOrderSeq
                        homeItem.heading = homeSectionItem.heading
                        homeItem.nestedList = homeSectionItem.list
                        homeItem.orientation = homeSectionItem.orientation
                        homeItem.subHeading = homeSectionItem.subHeading
                        arrayList.add(homeItem)
                        return@forEach
                    }


                    /*IConstants.LayoutType.Restaurant2000.TOP_RESTAURANTS, IConstants.LayoutType.Restaurant2000.TRENDING_RESTAURANTS -> {
    //                    fastAdapter.add(TopRestaurantItem(homeListData))
                    }*/
                    else -> {
                        homeSectionItem.list.forEach {
                            val homeItem = it
                            homeItem.flatLayoutType = homeSectionItem.layoutType
                            homeItem.displayOrderSeq = homeSectionItem.displayOrderSeq
                            homeItem.heading = homeSectionItem.heading
                            homeItem.orientation = homeSectionItem.orientation
                            homeItem.subHeading = homeSectionItem.subHeading
                            arrayList.add(homeItem)
                        }

                    }


                }

            }

        }
        requestingHome = false

//        }

        return ResponseDTO.success(
            result.title,
            result.message,
            result.type,
            arrayList,
            result.code,
            result.displayType
        )
    }


    suspend fun getHomeData(
        latitude: String,
        longitude: String,
        skip: Int,
        limit: Int
    ) = getResult {
        serviceAPI.homeData(latitude, longitude, skip, limit)
    }

    suspend fun getSearchSuggestion(exploreString: String) = getResult {
        serviceAPI.getSearchSuggestion(exploreString)
    }

    suspend fun logoutUser() = getResult {
        serviceAPI.logoutUser()
    }

    suspend fun updateLocation(
        latitude: String,
        longitude: String,
        city: String,
        locality: String
    ) = getResult {
        val updateLocation = UpdateLocationDTO(
            city = city,
            locality = locality,
            coordinates = listOf(latitude, longitude)
        )
        serviceAPI.updateLocation(updateLocation)
    }

//    suspend fun getExpandSearch(itemId: String) = getResult {
//        serviceAPI.expandSearch(itemId, 0, 20)
//
//    }

    suspend fun postCheckout(orderId: String) = getResult {
        serviceAPI.postCheckout(orderId)
    }

    suspend fun getAppStart() = getResult {
        serviceAPI.getAppStart()
    }

    suspend fun getAddressList() = getResult {
        serviceAPI.getAddressList()
    }

    suspend fun updateUserFcmToken(token: Map<String, String>) = getResult {
        serviceAPI.updateUserFcmToken(token)
    }

    suspend fun preCheckout(preCheckout: PreCheckoutReqDTO) = getResult {
        serviceAPI.preCheckout(preCheckout)
    }

    suspend fun applyReferral(userId: String, referralMap: Map<String, String>) = getResult {
        serviceAPI.applyReferral(userId, referralMap)
    }

    suspend fun getScratchCardInfo(): ResponseDTO<ScratchSummaryDTO> =
        getResult { serviceAPI.getScratchCardInfo() }

}
