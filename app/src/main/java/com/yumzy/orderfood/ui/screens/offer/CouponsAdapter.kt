package com.yumzy.orderfood.ui.screens.offer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CouponListResDTO
import com.yumzy.orderfood.databinding.AdapterCouponLayoutBinding
import com.yumzy.orderfood.ui.component.CouponCardView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.templetes.DetailsInfoTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil


class CouponsAdapter(
    val context: Context,
    val flag: Int,
    private val couponList: List<CouponListResDTO>,
    private val clickedListener: (couponCode: String,couponId:String) -> Unit
) :
    RecyclerView.Adapter<CouponsAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: com.yumzy.orderfood.databinding.AdapterCouponLayoutBinding =
            AdapterCouponLayoutBinding.inflate(layoutInflater, parent, false)
//        ShadowUtils.apply(
//            itemBinding.root,
//            ShadowUtils.Config().setShadowSize(UIHelper.i1px)
//                .setShadowRadius(UIHelper.i10px.toFloat()).setShadowColor(0XBFCECECE.toInt())
//        )
        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {

        return couponList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(couponList[position])


    }

    inner class ItemViewHolder(itemView: AdapterCouponLayoutBinding) :
        RecyclerView.ViewHolder(itemView.root), CouponCardView.TermsListener {

        var adapterBinding: AdapterCouponLayoutBinding = itemView

        fun bind(couponList: CouponListResDTO) {
            when (flag) {
                IConstants.ActivitiesFlags.FROM_HOME -> {
                    adapterBinding.couponView.applyTextView.visibility = View.GONE
                }
                IConstants.ActivitiesFlags.FROM_PLATE -> {
                    adapterBinding.couponView.applyTextView.visibility = View.VISIBLE


                }
            }


            adapterBinding.couponView.couponCode = couponList.code
            if (couponList.code.isNullOrEmpty()) {
                adapterBinding.couponView.applyTextView.visibility = View.GONE
            }

            when (couponList.discountType) {
                IConstants.DiscountType.NET -> adapterBinding.couponView.discount =
                    "₹" + couponList.value.toString()
                IConstants.DiscountType.PERCENTAGE -> adapterBinding.couponView.discount =
                    couponList.value.toString() + "%"
            }
            adapterBinding.couponView.header = couponList.title.capitalize()
            adapterBinding.couponView.subHeader = couponList.desc.capitalize()
            if(couponList.minOrderAmount<=0)
            {
                adapterBinding.couponView.orderAbove =
                    context.resources.getString(R.string.no_minimum_order_value)
            }
            else
            {
                adapterBinding.couponView.orderAbove =
                    context.resources.getString(R.string.applicable_on_orders_above) + couponList.minOrderAmount
            }

            adapterBinding.couponView.termsandCondition = this
            adapterBinding.couponView.applyTextView.setOnClickListener {
                clickedListener(adapterBinding.couponView.couponCode,couponList.couponId)
            }


        }


        override fun terms() {
            if (couponList.get(adapterPosition).terms.isNotEmpty()) {
                ClickUtils.applyGlobalDebouncing(
                    adapterBinding.couponView.tvTerms,
                    IConstants.AppConstant.CLICK_DELAY
                ) {

                    val detailActionDialog = DetailActionDialog()
                    val template = DetailsInfoTemplate(
                        context,
                        "",
                        YumUtil.setColorText(
                            context.resources.getString(R.string.terms),
                            ThemeConstant.blueJeans
                        ),
                        " ",
                        couponList[adapterPosition].terms
                    )
                    detailActionDialog.detailsTemplate = template
                    detailActionDialog.showNow(
                        (context as AppCompatActivity).supportFragmentManager,
                        ""
                    )


                }

            }
        }

    }
}