package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.IExpandable
import com.mikepenz.fastadapter.IParentItem
import com.mikepenz.fastadapter.ISubItem
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.yumzy.orderfood.databinding.AdapterRestaurentFassiBinding
import com.yumzy.orderfood.util.IConstants

/**
 * Created by mikepenz on 28.12.15.
 */
class BindignRestaurantFassiItem(val context: Context) :
    AbstractBindingItem<AdapterRestaurentFassiBinding>(),
    IExpandable<BindingViewHolder<AdapterRestaurentFassiBinding>> {

    var fassiLicNo: String = ""
    var sectionName: String? = ""
    var sectionItemCount: Int? = 0
    var sectionId: Int = 0
    var resultEmpty: Boolean = false


    override var parent: IParentItem<*>? = null
    override var isExpanded: Boolean = false

    var header: String? = null

    override var subItems: MutableList<ISubItem<*>>
        get() = mutableListOf()
        set(_) {}

    override val isAutoExpanding: Boolean
        get() = true

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override val type: Int
        get() = IConstants.FastAdapter.REST_FASSI_VIEW

    /**
     * setter method for the Icon
     *
     * @param item the icon
     * @return this
     */
    fun witLicNo(fassiLicNo: String, resultEmpty: Boolean = false): BindignRestaurantFassiItem {
        this.fassiLicNo = fassiLicNo
        this.resultEmpty = resultEmpty
        return this
    }

    fun withSection(sectionId: Int, section: String, sectionSize: Int): BindignRestaurantFassiItem {
        this.sectionId = sectionId
        this.sectionName = section
        this.sectionItemCount = sectionSize
        return this
    }

    /**
     * binds the data of this item onto the viewHolder
     */
    override fun bindView(binding: AdapterRestaurentFassiBinding, payloads: List<Any>) {
        //define our data for the view
        if (fassiLicNo != null && fassiLicNo.isNotBlank()) {
            binding.tvFassi.text = "Licence No. ${fassiLicNo}"
        } else {
            binding.tvFassi.text = "Applied for Licence"
        }
        if (resultEmpty) {
            binding.tvFilterTag.visibility = View.VISIBLE
        } else
            binding.tvFilterTag.visibility = View.GONE

    }

    override fun unbindView(binding: AdapterRestaurentFassiBinding) {

    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterRestaurentFassiBinding {
        return AdapterRestaurentFassiBinding.inflate(inflater, parent, false)
    }
}