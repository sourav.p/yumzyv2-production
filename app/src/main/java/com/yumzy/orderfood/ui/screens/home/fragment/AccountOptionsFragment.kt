package com.yumzy.orderfood.ui.screens.home.fragment

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.laalsa.laalsalib.utilcode.util.AppUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ProfileResponseDTO
import com.yumzy.orderfood.data.models.ScratchSummaryDTO
import com.yumzy.orderfood.data.models.UserDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.ui.AccountDTO
import com.yumzy.orderfood.databinding.FragmentAccountOptionsBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpHeadingActivity
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.manageaddress.ManageAddressActivity
import com.yumzy.orderfood.ui.screens.rewards.RewardsActivity
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardActivity
import com.yumzy.orderfood.ui.screens.wallet.WalletActivity
import com.yumzy.orderfood.util.IConstants
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

var updateProfile = true

class AccountOptionsFragment : BaseFragment<FragmentAccountOptionsBinding, AccountViewModel>(),
    IAccountView,
    IModuleHandler {
    private var accountOptionArrayList: ArrayList<AccountDTO> = ArrayList()
    
    override val vm: AccountViewModel by viewModel()


    lateinit var user: UserDTO

    override fun getLayoutId() = R.layout.fragment_account_options

    val cartHelper: CartHelper by inject()

    //    var userName: String = ""
    private val navController by lazy {
        findNavController()
    }


    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav_view).visibility =
            View.VISIBLE
        bd.profileInfoView.onEdit = { it ->
            findNavController().navigate(R.id.fragment_accountopt_to_profile)

        }
        if (sharedPrefsHelper.getIsTemporary()) {
            // navController.navigate(R.id.fragment_account_to_temp_login)

            bd.cardView.visibility = View.GONE
            bd.profileInfoView.visibility = View.GONE
            bd.viewSignIn.visibility = View.VISIBLE
            bd.btnSignIn.setOnClickListener {
//                val intent = Intent(requireActivity(), TempLoginActivity::class.java)
//                intent.putExtra(
//                    IConstants.FragmentFlag.FLAG,
//                    IConstants.FragmentFlag.OPEN_LOGIN
//                )
//                startActivity(intent)
//                requireActivity().finish()
                AppUtils.relaunchApp()

            }

            bd.btnSignUp.setOnClickListener {
                AppUtils.relaunchApp()

//                val intent = Intent(requireActivity(), TempLoginActivity::class.java)
//                intent.putExtra(
//                    IConstants.FragmentFlag.FLAG,
//                    IConstants.FragmentFlag.OPEN_REGISTER
//                )
//                startActivity(intent)
//                requireActivity().finish()
            }
            tempAccountOptionList()


        } else {
            accountOptionList()
            applyDebouchingClickListener(
                bd.llWallet,
                bd.llFavoutits,
                bd.llScratchCard
            )

            getScratchCardSummary()
            bd.viewSignIn.visibility = View.GONE

        }

//        userName = "${sharedPrefsHelper.getUserName().capitalize()} " +
//                "${sharedPrefsHelper.getUserLastName().capitalize()}"


    }

    override fun accountOptionList() {

        accountOptionArrayList = populateActionsList()

        bd.rvAccountOption.adapter =
            AccountAdapter(
                requireContext(),
                accountOptionArrayList
            ) { view: View, actionId ->
                onAccountOptionSelected(view, actionId)
            }

        bd.rvAccountOption.layoutManager =
            LinearLayoutManager(context)

    }

    override fun tempAccountOptionList() {
        accountOptionArrayList = tempAcountActionsList()

        bd.rvAccountOption.adapter =
            AccountAdapter(
                requireContext(),
                accountOptionArrayList
            ) { view: View, actionId ->
                onAccountOptionSelected(view, actionId)
            }

        bd.rvAccountOption.layoutManager =
            LinearLayoutManager(context)
    }

    private fun accountDTO(
        actionId: Int,
        iconString: Int,
        title: String,
        description: String
    ): AccountDTO {
        return AccountDTO(actionId, optionIcon(iconString), title, description)
    }

    private fun optionIcon(stringId: Int) = resources.getString(stringId)

    private fun onAccountOptionSelected(view: View, position: Int) {
        when (position) {
            IConstants.AccountAction.MANAGE_ADDRESS -> AppIntentUtil.launchWithSheetAnimation(
                requireActivity(),
                ManageAddressActivity::class.java
            )

            IConstants.AccountAction.FOOD_PREFERENCE -> context?.let {

                ActionModuleHandler.showFoodPreference(
                    it, this
                )
            }
            IConstants.AccountAction.MY_WALLET -> AppIntentUtil.launchWithSheetAnimation(
                requireActivity(),
                WalletActivity::class.java
            )
            IConstants.AccountAction.FAVORITE ->
                ActionModuleHandler.showFavouriteRestaurantDialog(
                    context as AppCompatActivity,
                    "",
                    null
                )

            IConstants.AccountAction.MY_REWARDS -> AppIntentUtil.launchWithBaseAnimation(
                view,
                requireActivity(),
                RewardsActivity::class.java
            )
            IConstants.AccountAction.ORDER_HISTORY -> {
                navController.navigate(R.id.fragment_accountopt_to_order_history)

            }

            IConstants.AccountAction.INVITE_FRIEND -> AppIntentUtil.launchWithSheetAnimation(
                requireActivity(),
                InviteActivity::class.java
            )

            IConstants.AccountAction.HELP_FAQ -> AppIntentUtil.launchWithSheetAnimation(
                requireActivity(),
                HelpHeadingActivity::class.java
            )

        }
    }


    private fun tempAcountActionsList(): ArrayList<AccountDTO> {
        val list = ArrayList<AccountDTO>()

        list.add(
            accountDTO(
                IConstants.AccountAction.HELP_FAQ,
                R.string.icon_question_1,
                getString(R.string.help_faq),
                "Get help for order and more"
            )
        )
//        list.add(
//            accountDTO(
//                IConstants.AccountAction.SEND_FEEDBACK,
//                R.string.icon_receipt,
//                getString(R.string.send_feedback),
//                "App version " + BuildConfig.VERSION_NAME.toString()
//            )
//        )
        return list
    }

    private fun populateActionsList(): ArrayList<AccountDTO> {
        val list = ArrayList<AccountDTO>()
        list.add(
            accountDTO(
                IConstants.AccountAction.MANAGE_ADDRESS,
                R.string.icon_009_address,
                getString(R.string.manage_address),
                "View yours saved addresses"
            )
        )
        list.add(
            accountDTO(
                IConstants.AccountAction.FOOD_PREFERENCE,
                R.string.icon_crockery,
                getString(R.string.food_preference),
                "Select your food preferences"
            )
        )
        list.add(
            accountDTO(
                IConstants.AccountAction.ORDER_HISTORY,
                R.string.icon_receipt,
                getString(R.string.history),
                "View your order history"
            )
        )
        list.add(
            accountDTO(
                IConstants.AccountAction.INVITE_FRIEND,
                R.string.icon_share_alt,
                "Refer & Earn",
                "Refer your friends and earn rewards"
            )
        )
        list.add(
            accountDTO(
                IConstants.AccountAction.HELP_FAQ,
                R.string.icon_question_1,
                getString(R.string.help_faq),
                "Get help for order and more"
            )
        )
        return list
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.ll_wallet -> AppIntentUtil.launchWithSheetAnimation(
                requireActivity() as AppCompatActivity,
                WalletActivity::class.java
            )
            R.id.ll_favoutits ->

                ActionModuleHandler.showFavouriteRestaurantDialog(
                    context as AppCompatActivity,
                    "",
                    null
                )
            R.id.ll_scratch_card -> {
                AppIntentUtil.launchForResultWithSheetAnimation(
                    context as AppCompatActivity,
                    ScratchCardActivity::class.java
                ) { _, _ ->
                    getScratchCardSummary()
                }
            }

            else -> super.onDebounceClick(view)
        }
    }

    private fun getUserDetails() {
        vm.userDetails().observe(this, androidx.lifecycle.Observer { result ->
            if (result.type == -1)
                return@Observer
            RHandler<Any>(
                this,
                result
            ) {
                profileResponse(result.data)
            }

        })
    }

    private fun profileResponse(data: Any?) {
        if (data != null && data is ProfileResponseDTO) {
            sharedPrefsHelper.saveUserName(data.name.toString())
            sharedPrefsHelper.saveUserLastName(data.lastName.toString())
            data.profilePicLink?.let { sharedPrefsHelper.saveUserPic(it) }
            sharedPrefsHelper.saveUserReferralCode(data.referralCode)
            bd.profileInfoView.email = sharedPrefsHelper.getUserEmail()
            bd.profileInfoView.mobile = sharedPrefsHelper.getUserPhone()
            bd.profileInfoView.pic = sharedPrefsHelper.getUserPic().toString()
            val userName = "${
                sharedPrefsHelper.getUserName().capitalize(Locale.getDefault())
            } ${sharedPrefsHelper.getUserLastName().capitalize(Locale.getDefault())}"
            bd.profileInfoView.name = userName
        }
    }

    override fun onResume() {
        super.onResume()
        getUserDetails()
        updateProfile = false
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        if (actionId == IConstants.ActionModule.FOOD_PREFERENCE) {
            AppUIController.showFoodPrefUI()
        }
    }

    private fun getScratchCardSummary() {
        vm.getScratchCardInfo().observe(this, { result ->
            if (result.type == -1) return@observe
            RHandler<ScratchSummaryDTO>(this, result) {
                if (it.unscratched != 0) {
                    bd.scratchCardCount.visibility = View.VISIBLE
                    bd.scratchCardCount.text = it.unscratched.toString()
//                    YumUtil.animateSizeScale(bd.scratchCardCount, 2000, 6)
                } else
                    bd.scratchCardCount.visibility = View.GONE
            }
        })
    }

}