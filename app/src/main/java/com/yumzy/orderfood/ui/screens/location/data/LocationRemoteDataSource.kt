package com.yumzy.orderfood.ui.screens.location.data

import com.yumzy.orderfood.api.AddressServiceAPI
import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.data.models.AddressDTO
import javax.inject.Inject

class LocationRemoteDataSource  constructor(
    val addressServiceAPI: AddressServiceAPI
) : BaseDataSource() {

    suspend fun getAddressList() = getResult {
        addressServiceAPI.getAddressList()
    }

    suspend fun updateAddress(address: AddressDTO) = getResult {
        addressServiceAPI.updateAddress(address)
    }

    suspend fun addAddress(address: AddressDTO) = getResult {
        addressServiceAPI.addAddress(address)
    }

    suspend fun deleteAddress(addressId: String) = getResult {
        addressServiceAPI.deleteAddress(addressId)
    }

}