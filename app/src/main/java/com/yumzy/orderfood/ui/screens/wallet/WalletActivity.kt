package com.yumzy.orderfood.ui.screens.wallet

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.VPIDTO
import com.yumzy.orderfood.data.models.WalletDTO
import com.yumzy.orderfood.data.models.WalletTransactionDTO
import com.yumzy.orderfood.data.models.WithdrawDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityMyWalletBinding
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter.ListLoadingItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.StickyHeaderAdapter
import com.yumzy.orderfood.ui.screens.templetes.EditUPITemplate
import com.yumzy.orderfood.ui.screens.templetes.WithdrawInitiateTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.intentShareText
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class WalletActivity : BaseActivity<ActivityMyWalletBinding, WalletViewModel>(),
    IWallet, EditUPITemplate.UpdateSingleInfoListener,
    WithdrawInitiateTemplate.UpdateSingleInfoListener {

    private var template: EditUPITemplate? = null
    private var availableBalance: Double = 0.0

    private var listReachedEnd: Boolean = false

    private var skip: Int = 0
    private var limit: Int = 16
    var itemLoading = false

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener

    override val vm : WalletViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_my_wallet)
        JodaTimeAndroid.init(this)

        vm.attachView(this)
        setSupportActionBar(bd.toolbar)
        supportActionBar?.title = resources.getString(R.string.wallet)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        setLightStatusBar(this, Color.WHITE)
        BarUtils.setStatusBarLightMode(this, false)
        BarUtils.setStatusBarColor(this, ThemeConstant.transparent)
//        val typeface = ResourcesCompat.getFont(this, R.font.raleway_medium)
//        bd.mainCollapsing.setCollapsedTitleTypeface(typeface)
//        bd.mainCollapsing.title = resources.getString(R.string.yumzy_wallet);
//        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
//        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarBlackText);

        setupPagingRecyclerView()
        getUserWallet()
        //initComponent()

    }

    private fun initComponent() {
        bd.appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                val min_height = ViewCompat.getMinimumHeight(bd.collapsingToolbar) * 2
                val scale = (min_height + verticalOffset).toFloat() / min_height
                bd.amountTransferCardview.scaleX = if (scale >= 0f) scale else 0f
                bd.amountTransferCardview.scaleY = if (scale >= 0f) scale else 0f
                bd.clayoutTransfer.isClickable = scale >= 0f
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    private fun setupPagingRecyclerView() {

        val stickyHeaderAdapter = StickyHeaderAdapter<GenericItem>()
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        footerAdapter = ItemAdapter.items()
        mFastAdapter.addAdapter(1, footerAdapter)

        bd.rvTransactions.layoutManager = LinearLayoutManager(this)
        bd.rvTransactions.itemAnimator = DefaultItemAnimator()
        bd.rvTransactions.adapter = stickyHeaderAdapter.wrap(mFastAdapter)

        val decoration = StickyRecyclerHeadersDecoration(stickyHeaderAdapter)
        bd.rvTransactions.addItemDecoration(decoration)

        //so the headers are aware of changes
        mFastAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                decoration.invalidateHeaders()
            }
        })

        mFastAdapter.attachDefaultListeners = true

        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = ListLoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                fetchWalletTransactions()
            }
        }

        bd.rvTransactions.addOnScrollListener(endlessRecyclerOnScrollListener)
        getUserWallet()
        fetchWalletTransactions()


        ClickUtils.applyGlobalDebouncing(
            bd.clayoutTransfer,
            IConstants.AppConstant.CLICK_DELAY
        ) {
            if (availableBalance == 0.0) {
                showSnackBar(getString(R.string.you_dont_have_withdraw_money), -1)
            } else {
                val detailActionDialog = DetailActionDialog()
                template = EditUPITemplate(
                    this,
                    getString(R.string.upi_verification),
                    this,
                )
                detailActionDialog.detailsTemplate = template
                detailActionDialog.showNow(supportFragmentManager, "")
            }
        }

    }


    override fun getUserWallet() {
        vm.getUserWallet().observe(this, { result ->
            RHandler<WalletDTO>(
                this,
                result
            ) { walletDTO ->
                bd.wallet = walletDTO
                availableBalance = walletDTO.availableBalance

                //terms= walletDTO.terms

            }
        })
    }

    override fun fetchWalletTransactions() {
        if (listReachedEnd) {
            footerAdapter.clear()
            val progressItem = ListLoadingItem()
            progressItem.isEnabled = false
            footerAdapter.add(progressItem)
            return
        }
        vm.getWalletTransactions(skip).observe(this,
            Observer { result ->
                if (result.type == -1)
                    return@Observer
                itemLoading = true

                RHandler<List<WalletTransactionDTO>>(
                    this,
                    result
                ) { walletTransactions ->

                    if (skip == 0 && walletTransactions.isEmpty()) {
                        noScratchCardLayout(true)
                    }

                    footerAdapter.clear()
                    if (walletTransactions.isNullOrEmpty()) {
                        listReachedEnd = true
                        vm.getWalletTransactions(skip)
                            .removeObservers(this)

                    }

                    val items = ArrayList<WalletTransactionItem>()
                    walletTransactions.forEach { listElement ->
                        val topOutletItem = WalletTransactionItem().apply {
                            transcation = listElement
                            context = this@WalletActivity
                            clickedListener = { transaction ->
                                showToast(transaction.toString())
                            }
                        }

                        items.add(topOutletItem)

                    }
                    mFastAdapter.add(items)

                    skip++

                }

            })

    }

    private fun noScratchCardLayout(emptyList: Boolean) {

        if (emptyList) {
            bd.appBarLayout.setExpanded(false)
            bd.rvTransactions.visibility = View.GONE
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.noDataToDisplay.actionCallBack = shareInviteAction

        } else {
            bd.rvTransactions.visibility = View.VISIBLE
            bd.noDataToDisplay.visibility = View.GONE
        }


    }

    private val shareInviteAction: (view: View) -> Unit = { view ->
        var inviteText = IConstants.AppConstant.INVITE_TEXT
        if (inviteText != "") {
            if (inviteText.contains("refer_code")) {
                inviteText = inviteText.replace(
                    "refer_code",
                    sharedPrefsHelper.getUserReferralCode(),
                    ignoreCase = false
                )
            }

            if (inviteText.contains("refer_link")) {
                inviteText = inviteText.replace(
                    "refer_link",
                    sharedPrefsHelper.getUserReferralLink() ?: "",
                    ignoreCase = false
                )
            }

            if (inviteText.contains("new_line")) {
                inviteText = inviteText.replace(
                    "new_line",
                    "\n",
                    ignoreCase = false
                )
            }

        } else {
            inviteText =
                resources.getString(R.string.invite_text) + "Referral Code: " + sharedPrefsHelper.getUserReferralCode() + "\n" + sharedPrefsHelper.getUserReferralLink()
        }

        /*   CleverTapHelper.referralLink(
                        sharedPrefsHelper.getUserName(),
                        sharedPrefsHelper.getUserPhone(),
                        sharedPrefsHelper.getUserReferralCode()
                    )*/
        intentShareText(this, inviteText)
    }


    override fun sendUPI(upiNo: String, dialog: DialogInterface?) {
        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 2)
        vm.verifyVPA(upiNo).observe(this, { result ->
            template?.let {
                RHandler<VPIDTO>(
                    it,
                    result
                ) {
                    if (it.valid) {
                        dialog?.dismiss()
                        val detailActionDialog = DetailActionDialog()
                        val template = WithdrawInitiateTemplate(
                            this,
                            getString(R.string.withdraw_amount),
                            it.vpa,
                            this,
                        )
                        detailActionDialog.detailsTemplate = template
                        detailActionDialog.showNow(supportFragmentManager, "")
                    } else {
                        showToast(getString(R.string.invalid_upi))
                        dialog?.dismiss()
                    }
                }
            }
        })


    }

    override fun sendwithdrawAmount(amount: String, dialog: DialogInterface?, vpa: String) {
        if (amount.toDouble() > availableBalance) {
            showToast(getString(R.string.you_dont_have_withdraw_money))
            dialog?.dismiss()
        } else {
            dialog?.dismiss()
//            val hashMap: HashMap<String, String> = HashMap<String, Any>()
//            hashMap.put("amount", amount)
//            hashMap.put("mobile", sharedPrefsHelper.getUserPhone().toString())
//            hashMap.put("appName", resources.getString(R.string.app_name))
//            hashMap.put("vpa", vpa)

//            val withDraw = WithdrawReqDTO(
//                amount.toDouble(),
//                sharedPrefsHelper.getUserPhone().toString(),
//                resources.getString(R.string.app_name),
//                vpa
//            )

            val body = mutableMapOf<String, Any>()
            body["amount"] = amount.toDouble()
            body["mobile"] = sharedPrefsHelper.getUserPhone().toLong() as Any
            body["appName"] = "yumzy"
            body["vpa"] = vpa
            vm.withdrawBalance(body).observe(this, { result ->

                RHandler<WithdrawDTO>(
                    this,
                    result
                ) {

                    when (it.status) {
                        IConstants.WithDrawStatus.FAILURE -> {
                            AppUIController.showWithdrawStatus(it.status, it.statusDescription, 0)
                        }
                        IConstants.WithDrawStatus.SUCCESS -> {
                            AppUIController.showWithdrawStatus(it.status, it.statusDescription, 1)

                        }
                        IConstants.WithDrawStatus.ACCEPTED -> {
                            AppUIController.showWithdrawStatus(it.status, it.statusDescription, 2)

                        }

                    }
                    getUserWallet()
                    skip = 0
                    listReachedEnd = false
                    mFastAdapter.clear()
                    endlessRecyclerOnScrollListener.resetPageCount(0)
//                    fetchWalletTransactions()

                }
            })
        }

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.cardWalletAmount.visibility = View.GONE
            bd.appBarLayout.setExpanded(false)
            bd.appBarLayout.setExpanded(false)
        }
    }

}
