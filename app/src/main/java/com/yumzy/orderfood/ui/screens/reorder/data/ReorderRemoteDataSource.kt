package com.yumzy.orderfood.ui.screens.reorder.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.OrderServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ReorderRemoteDataSource  constructor(private val serviceAPI: OrderServiceAPI) :
    BaseDataSource() {

    suspend fun orderDetails(orderId: String) = getResult {
        serviceAPI.orderDetails(orderId)

    }
}