/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.content.Context
import android.content.DialogInterface
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setRalewayBoldFont
import com.yumzy.orderfood.ui.helper.setRalewayMediumFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst

open class PopUpTemplateDialog(
    override val context: Context,
    val imageResource: Int = 0,
    val title: CharSequence = "",
    val subTitle: CharSequence = "",
    val extraInfo: CharSequence = ""
) : IDetailsActionTemplate {

    override var headerHeight: Int = VUtil.dpToPx(150)
        set(value) {
            field = VUtil.dpToPx(value)
        }
    override val showClose = false
    override fun heading(): View? {
        val successImageView = ImageView(context)
        successImageView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, headerHeight - 10.px)
        successImageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
        successImageView.setImageResource(imageResource)
        return successImageView
    }

    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        if (title.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_header, title))
        if (subTitle.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_sub_header, subTitle))
        if (extraInfo.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_sub_sub_header, extraInfo))

        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {
        Handler().postDelayed({
            dialog?.dismiss()
        }, 2000)
    }


    open fun getTextView(tvId: Int, msg: CharSequence): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setRalewayMediumFont()

        if (tvId == R.id.tv_info_header) {
            textView.textSize = 15f
            textView.setPadding(i5px, i5px, i5px, 0)
            textView.setTextColor(ThemeConstant.textBlackColor)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(ThemeConstant.pinkies)
            textView.textSize = 25f
            textView.setRalewayBoldFont()
        } else if (tvId == R.id.tv_info_sub_sub_header) {
            textView.setTextColor(ThemeConstant.textBlackColor)
            textView.setPadding(i5px, 0, i5px, i5px)
            textView.textSize = 15f
        }
        return textView
    }


}
