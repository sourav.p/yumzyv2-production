package com.yumzy.orderfood.ui.screens.manageaddress

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ManageAddressViewModel  constructor ( private val repository: ProfileRepository
) : BaseViewModel<IManageAddress>() {

    fun deleteAddress(addressId: String) = repository.deleteAddress(addressId)

    fun getAddressList() = repository.getAddressList()

}