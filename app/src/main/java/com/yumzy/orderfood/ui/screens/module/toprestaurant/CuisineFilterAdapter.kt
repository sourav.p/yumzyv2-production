package com.yumzy.orderfood.ui.screens.module.toprestaurant

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.AdapterFilterCuisineBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils

class CuisineFilterAdapter(
    val context: Context,
    private val clickListener: (adapterPosition: Int) -> Unit
) : RecyclerView.Adapter<CuisineFilterAdapter.ItemViewHolder>() {

    private var clickedItem = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding : AdapterFilterCuisineBinding =
            AdapterFilterCuisineBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        //TODO change the count based on real data
        return 10
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
//        val restaurant = restaurantList.get(position)
      //  holder.bind(CuisineDTO("Noodels"))
    }

    inner class ItemViewHolder (itemView: AdapterFilterCuisineBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        var adapterBinding: AdapterFilterCuisineBinding = itemView

//        fun bind(cuisine: CuisineDTO) {
//            if (adapterPosition == clickedItem) {
//                adapterBinding.itemCuisine.background = VUtil.getRoundStrokeDrawableListState(
//                    ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
//                    10f,
//                    ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
//                    10f,
//                    ThemeConstant.pinkies, 2
//                )
//            } else {
//                adapterBinding.itemCuisine.background = null
//            }
//            adapterBinding.tvCuisinesName.text = cuisine.title
//        }

        init {
            adapterBinding.itemCuisine.setClickScaleEffect()

            adapterBinding.itemCuisine.setOnClickListener {

                var previous = clickedItem
                clickedItem = adapterPosition
                notifyItemChanged(previous)

                adapterBinding.itemCuisine.background = DrawableUtils.getRoundStrokeDrawableListState(
                    ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
                    10f,
                    ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
                    10f,
                    ThemeConstant.pinkies, 2
                )
                clickListener(adapterPosition)
                notifyItemChanged(adapterPosition)
            }
        }

    }

}