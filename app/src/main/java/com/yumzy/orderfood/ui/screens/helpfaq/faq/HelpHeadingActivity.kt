/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.helpfaq.faq

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HelpHeadingDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityHelpHeadingBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import com.zoho.livechat.android.ZohoLiveChat
import com.zoho.salesiqembed.ZohoSalesIQ
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class HelpHeadingActivity : BaseActivity<ActivityHelpHeadingBinding, HelpHeadingViewModel>(),
    IHelpAndSupport {

    private val i8px = VUtil.dpToPx(8)
    private val i2px = VUtil.dpToPx(2)
    override val navigationPadding: Boolean = true
    
    override val vm: HelpHeadingViewModel by viewModel()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_help_heading)
        vm.attachView(this)


        setSupportActionBar(bd.mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        applyDebouchingClickListener(bd.llPreviousOrder)
        getHeadings()
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        val typeface = ResourcesCompat.getFont(this, R.font.poppins_medium)
        showZohoChatButton(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_chat_option, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.chat_zoho) {
            CleverTapHelper.pushEvent(CleverEvents.support_chat_requested)
            ZohoLiveChat.Chat.open()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getHeadings() {
        vm.getHeading().observe(this, { result ->
            RHandler<List<HelpHeadingDTO>>(
                this,
                result
            ) {
                setList(result.data)
            }
        })

    }

    override fun onResume() {
        super.onResume()
        zohoSetupVisitor()
        showZohoChatButton(true)
    }


    private fun zohoSetupVisitor() {

//        ZohoSalesIQ.unregisterVisitor(this)
//        ZohoSalesIQ.registerVisitor(sharedPrefsHelper.getUserPhone())
        if (sharedPrefsHelper.getIsTemporary()) {
            ZohoSalesIQ.Visitor.setName("")
            ZohoSalesIQ.Visitor.setEmail("")
            ZohoSalesIQ.Visitor.setContactNumber("")
        } else {
            ZohoSalesIQ.Visitor.setName(sharedPrefsHelper.getUserName())
            ZohoSalesIQ.Visitor.setEmail(sharedPrefsHelper.getUserEmail())
            ZohoSalesIQ.Visitor.setContactNumber(sharedPrefsHelper.getUserPhone())
        }
//        ZohoSalesIQ.Visitor.addInfo("User Type","Premium"); //Custom visitor information
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        showZohoChatButton(false)
        super.onBackPressed()
    }


    private fun setList(data: List<HelpHeadingDTO>?) {

        data?.forEach { item ->
            bd.llContainerHeadings.addView(
                LinearLayout(this).apply {
                    id = item.headingId + IConstants.Help.HELP_ITEM_ID_CONSTANT
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    orientation = LinearLayout.HORIZONTAL
                    setPadding(i8px, i8px, i8px, i8px)

                    //titleTextview
                    this.addView(MaterialTextView(context).apply {
                        layoutParams =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
                                .apply {
                                    setMargins(i2px, i2px, i2px, i2px)
                                }
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            this.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
                        } else {
                            this.setTextAppearance(context,R.style.TextAppearance_MyTheme_SemiBold)
                        }
                        this.text = item.title
                    })

                    //arrwobutton
                    this.addView(FontIconView(context).apply {
                        layoutParams =
                            LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                            ).apply {
                                setMargins(i2px, i2px, i2px, i2px)
                            }
                        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                        this.text = context.resources.getString(R.string.icon_down_arrow)
                    })

                    this.setClickScaleEffect()
                    applyDebouchingClickListener(this)

                }
            )
        }

    }

/*    private fun setUserDTO() {
        vm.getAppUser().observe(this, Observer { userDTO ->
            userDTO?.also {
                zohoSetupVisitor(it)

            }
        })
    }

    private fun zohoSetupVisitor(user: UserDTO) {
        ZohoSalesIQ.Visitor.setName(user.name)
        ZohoSalesIQ.registerVisitor(user.userId)
        ZohoSalesIQ.Visitor.setEmail(user.email)
        ZohoSalesIQ.Visitor.setContactNumber(user.mobile)

//        ZohoSalesIQ.Visitor.addInfo("User Type","Premium"); //Custom visitor information
    }*/

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when {
            R.id.btn_back == view.id -> finish()
            R.id.ll_previous_order == view.id -> {
                ZohoLiveChat.Chat.open()
            }
            else -> {
                val subHeadingIntent =
                    getSubHeadingIntent(view.id % IConstants.Help.HELP_ITEM_ID_CONSTANT)
                AppIntentUtil.launchIntentBaseAnimation(view, this, subHeadingIntent)

            }

        }
    }

    private fun getSubHeadingIntent(headingId: Int) =
        Intent(
            this,
            HelpSubheadingActivity::class.java
        ).apply {
            putExtra(IConstants.Help.INTENT_HELP, headingId)
        }


    override fun onDestroy() {
        super.onDestroy()
        vm.detachView()
        showZohoChatButton(false)
    }
}