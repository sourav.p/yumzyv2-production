package com.yumzy.orderfood.ui.screens.scratchcard

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.databinding.AdapterScratchCardBinding
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil

val drawableList = listOf(
    R.drawable.img_scratch_0,
    R.drawable.img_scratch_1,
    R.drawable.img_scratch_2,
    R.drawable.img_scratch_3,
    R.drawable.img_scratch_4,
    R.drawable.img_scratch_5,
    R.drawable.img_scratch_6
)

class ScratchCardItem(scratchCardDTO: ScratchCardDTO) :
    ModelAbstractBindingItem<ScratchCardDTO, AdapterScratchCardBinding>(scratchCardDTO) {

//    var scratchCardDTO: ScratchCardDTO? = null
//    var context: ScratchCardActivity? = null
//    var scratchIndex: Int = 0


    override val type: Int = R.id.scratch_card_details

    override fun bindView(binding: AdapterScratchCardBinding, payloads: List<Any>) {

        if (model.scratched) {
            binding.unScratchLayout.visibility = View.GONE
            binding.scratchLayout.visibility = View.VISIBLE

            binding.tvWonMessage.text = HtmlCompat.fromHtml(
                model.scratchMessage,
                HtmlCompat.FROM_HTML_MODE_COMPACT
            )

            when (model.redeemType) {
                IConstants.ScratchCardType.URL_TYPE.redeemType -> {
                    model.image.let {
                        YumUtil.setImageInView(
                            binding.ivRevealed,
                            it, R.drawable.cookie_coin
                        )
                    }
                }
                else -> {
                    YumUtil.setImageInView(
                        binding.ivRevealed,
                        Uri.parse("android.resource://com.yumzy.orderfood/drawable/cookie_coin")
                            .toString()
                    )
                }

            }
        } else {
            binding.unScratchLayout.visibility = View.VISIBLE
            binding.scratchLayout.visibility = View.GONE
            binding.unScratchImage.setImageResource(drawableList[model.position.rem(drawableList.size)])
//            binding.unScratchImage.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }


    }


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterScratchCardBinding {
        return AdapterScratchCardBinding.inflate(inflater, parent, false)
    }
}