package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class RewardsView extends LinearLayout {
    private String title;
    private String badge;
    private Drawable image;

    public RewardsView(Context context) {
        this(context, null);
    }

    public RewardsView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RewardsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (badge == null) {
            badge = "";
        }
        if (image == null) {
        }

        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RewardslView, defStyleAttr, 0);
        badge = typedArray.getString(R.styleable.RewardslView_tv_badge);
        title = typedArray.getString(R.styleable.RewardslView_tv_title);
        image = typedArray.getDrawable(R.styleable.RewardslView_iv_reward);
        typedArray.recycle();
    }

    private void init() {
        this.setOrientation(VERTICAL);
        int i60px = VUtil.dpToPx(60);
        int i5px = VUtil.dpToPx(5);


        this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ImageView iv = new ImageView(getContext());
        iv.setImageDrawable(iv.getDrawable());
        iv.setId(R.id.iv_reward);
        iv.setLayoutParams(new LayoutParams(i60px, i60px));


        LinearLayout containerLayout = new LinearLayout(getContext());
        containerLayout.setOrientation(HORIZONTAL);
        containerLayout.setGravity(Gravity.CENTER);

        LayoutParams layoutParamsStatus = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsStatus.setMargins(i5px, i5px, 0, i5px);
        TextView tvbadge = new TextView(getContext());
        tvbadge.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), tvbadge);
        tvbadge.setText(badge);
        tvbadge.setId(R.id.tv_badge);
        YumUtil.INSTANCE.setInterFontBold(getContext(), tvbadge);

        tvbadge.setLayoutParams(layoutParamsStatus);
        tvbadge.setGravity(Gravity.START);
        tvbadge.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));

        LayoutParams layoutParamsHouseName = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsHouseName.setMargins(i5px, i5px, i5px, i5px);
        TextView tvtitle = new TextView(getContext());
        YumUtil.INSTANCE.setInterFontRegular(getContext(), tvbadge);

        tvtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvtitle.setText(title);
        tvtitle.setId(R.id.tv_reward_title);
        tvtitle.setTypeface(Typeface.DEFAULT_BOLD);
        tvtitle.setLayoutParams(layoutParamsHouseName);
        tvtitle.setGravity(Gravity.START);
        tvtitle.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        YumUtil.INSTANCE.setInterFontBold(getContext(), tvtitle);

        this.addView(iv);
        containerLayout.addView(tvbadge);
        containerLayout.addView(tvtitle);


        this.addView(containerLayout);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) this.findViewById(R.id.tv_reward_title)).setText(this.title);

    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
        ((TextView) this.findViewById(R.id.tv_badge)).setText(this.badge);

    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
        ((ImageView) findViewById(R.id.iv_reward)).setImageDrawable(image);

    }
}
