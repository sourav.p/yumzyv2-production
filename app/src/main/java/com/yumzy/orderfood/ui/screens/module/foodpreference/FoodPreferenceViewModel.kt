package com.yumzy.orderfood.ui.screens.module.foodpreference

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.foodpreference.data.FoodPreferenceRepository
import javax.inject.Inject

class FoodPreferenceViewModel  constructor(private val repository: FoodPreferenceRepository) :
    BaseViewModel<IView>() {
    fun getlikedPrefTags() = repository.getlikedPrefTags()

}