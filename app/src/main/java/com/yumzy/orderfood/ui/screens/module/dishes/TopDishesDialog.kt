package com.yumzy.orderfood.ui.screens.module.dishes

import android.animation.AnimatorInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.DishesResDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogTopDishesBinding
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.screens.fastitems.BottomSheetListItem
import com.yumzy.orderfood.ui.screens.fastitems.LoadingItem
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.module.dishes.adapter.TopDishReccomendItem
import com.yumzy.orderfood.ui.screens.module.dishes.adapter.TopDishRegularItem
import com.yumzy.orderfood.util.IConstants
import io.cabriole.decorator.LinearMarginDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject

class TopDishesDialog : BaseSheetFragment<DialogTopDishesBinding, TopDishesViewModel>(),
    ITopDishesView, ICartHandler {

    
    lateinit var sharedPrefsHelper: SharedPrefsHelper
//    private var mCart: MutableMap<String, Int> = mutableMapOf()

    lateinit var cartHelper: CartHelper
    
    override val vm: TopDishesViewModel by viewModel()

    lateinit var promoId: String
    var title: String? = ""
    var subLayoutType: Int = -1
    var outletId: String = ""

//    private var mIsCartDifferent = false

    override fun getLayoutId(): Int = R.layout.dialog_top_dishes

    private var listReachedEnd: Boolean = false
    private var skip: Int = 0
    private var limit: Int = 15


    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener

    //    lateinit var title: String
//    lateinit var heroImage: HeroImageDTO
    var itemLoading = false


    private fun setupPagingRecyclerView() {
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        footerAdapter = ItemAdapter.items()
        mFastAdapter.addAdapter(1, footerAdapter)

        val gridLayoutManager = GridLayoutManager(context, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (mFastAdapter.getItemViewType(position)) {
                    R.id.item_recommended_menu -> 2
                    else -> 2
                }
            }
        }

        bd.rvTopDishes.layoutManager = gridLayoutManager
        /* bd.rvTopDishes.itemAnimator = SlideUpAlphaAnimator().apply {
             addDuration = 600
             removeDuration = 600
         }*/
        bd.rvTopDishes.adapter = mFastAdapter
//        bd.rvTopDishes.addItemDecoration(GridItemDecoration(5.px, 5.px))
        bd.rvTopDishes.addItemDecoration(
            LinearMarginDecoration(
                leftMargin = 8.px,
                topMargin = 8.px,
                rightMargin = 8.px,
                bottomMargin = 8.px,
                orientation = RecyclerView.VERTICAL
            )
        )
        mFastAdapter.attachDefaultListeners = true

        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = LoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                getDishesList()
            }
        }
        bd.rvTopDishes.addOnScrollListener(endlessRecyclerOnScrollListener)
        /* mFastAdapter.onClickListener = { view, _, item, _ ->
             (item as TopDishesReccomendtItem)
             onPostSelected(
                 item.listElements?.outletId.toString(),
                 item.listElements?.itemId.toString()
             )
             false
         }*/
        getDishesList()
    }

    private fun getDishesList() {
        if (listReachedEnd) {
            footerAdapter.clear()
            return
        }
        vm.getTopDishes(promoId, skip, limit).observe(
            this,
            Observer { result ->
                itemLoading = true
                if (result.type == APIConstant.Status.LOADING) {
                    footerAdapter.clear()
                    val progressItem = LoadingItem()
                    progressItem.isEnabled = false
                    footerAdapter.add(progressItem)
                    return@Observer
                }
//                CleverTapHelper.seeMoreScrolled(title, promoId, skip)
                RHandler<DishesResDTO>(
                    this,
                    result
                ) { dishesResDto ->
                    footerAdapter.clear()
                    if (dishesResDto.items.isNullOrEmpty()) {
                        listReachedEnd = true
                        footerAdapter.add(BottomSheetListItem())
                        vm.getTopDishes(promoId, skip, limit)
                            .removeObservers(this)
                    }

                    val items = ArrayList<GenericItem>()
                    dishesResDto.items.forEach { listElement ->
                        listElement.subLayoutType = if (subLayoutType != -1) subLayoutType else 0
                        when (listElement.subLayoutType) {
                            1 -> {
                                val item = TopDishReccomendItem(listElement).apply {
                                    callerFun = { outletId ->
                                        outletId?.let {
                                            onPostSelected(it)
                                        }
                                    }
                                    clickedListener = { dishItem ->
                                        dishItem?.let { item ->
                                            context?.let { context ->
                                                (context as HomePageActivity).let {
                                                    it.cartHelper.addOrUpdateItemToCart(
                                                        it,
                                                        1,
                                                        RestaurantItem().apply {
                                                            this.outletId = item.outletId
                                                            this.itemId = item.itemId
                                                            this.price = item.price.toDouble()
                                                            this.name = item.name
                                                        },
                                                        null,
                                                        true
                                                    )
                                                }
                                            }
                                        }
                                    }

                                }
                                items.add(item)
                            }
                            else -> {
                                val item = TopDishRegularItem().apply {
                                    listElements = listElement
                                    callerFun = { outletId ->
                                        outletId?.let {
                                            onPostSelected(it)
                                        }
                                    }
                                    clickedListener = { dishItem ->
                                        dishItem?.let { item ->
                                            context?.let { context ->
                                                (context as HomePageActivity).let {
                                                    it.cartHelper.addOrUpdateItemToCart(
                                                        it,
                                                        1,
                                                        RestaurantItem().apply {
                                                            this.outletId = item.outletId
                                                            this.itemId = item.itemId
                                                            this.price = item.price.toDouble()
                                                            this.name = item.name
                                                        },
                                                        null,
                                                        true
                                                    )
                                                }

                                            }
                                        }
                                    }


                                }
                                items.add(item)
                            }
                        }

                    }
                    mFastAdapter.add(items)
                    skip++
                }

            })
    }

    /*private fun onItemAdd(
        itemId: String,
        count: Int,
        price: String,
        outletId: String
    ) {
        val item = RestaurantItem()
        item.outletId = outletId
        item.itemId = itemId
        cartHelper.addOrUpdateItemToCart(this, 1, item, null, true)
    }*/

    /*  private fun fetchTopRestaurantsList() {
          vm.getRestaurants(promoId, skip, limit).observe(
              this,
              Observer { result ->
                  RHandler<RestaurantsResDTO>(this, result) {
                      setTopDishesList(result.data as RestaurantsResDTO)
                  }
              })
      }

      private fun setTopDishesList(restaurantsResDTO: RestaurantsResDTO) {
          if (restaurantsResDTO.list.isNullOrEmpty()) {
              bd.rvRestaurants.visibility = View.GONE
              bd.lottieToprestraList.visibility = View.VISIBLE

          } else {
              bd.rvRestaurants.visibility = View.VISIBLE
              bd.lottieToprestraList.visibility = View.GONE


              activity?.let {
                  //setup restaurant adapter
                  bd.rvRestaurants.adapter =
                      TopRestaurantAdapter(
                          it,
                          restaurantsResDTO.list,
                          clickListener = { outletId: String ->
                              onPostSelected(
                                  outletId
                              )
                          })
                  bd.rvRestaurants.layoutManager = LinearLayoutManager(it)

              }
          }
      }
  */

    /*override fun showFilterOptions() {
        bd.includeBottomFilter.cvFilterAction.visibility = View.VISIBLE
//        sheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED

    }

    override fun hideFilterOptions() {
        bd.includeBottomFilter.cvFilterAction.visibility = View.GONE
//        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun applyFilter() {
//        hideFilterOptions()
    }

    override fun clearAllFilter() {
//        hideFilterOptions()
    }*/

    private fun onPostSelected(outletId: String, itemId: String = "") {
        /*val intent = Intent(context, OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.ITEM_ID, itemId)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)*/
//        dismiss()
    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }

    override fun itemAdded(totalItemCount: Int) {
        onSuccess(IConstants.ActionModule.OPEN_PLATE, "")
        dismiss()
    }

    override fun itemRepeated() {}

    override fun failedToAdd(message: String, gotoPlate: Boolean) {
        showSnackBar(message, -1)
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            else -> super.onDebounceClick(view)
        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        bd.noDataToDisplay.visibility = View.VISIBLE
        bd.rvTopDishes.visibility = View.GONE
        when (code) {
            APIConstant.Status.NO_NETWORK -> {
                bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
                bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
                bd.noDataToDisplay.imageResource = R.drawable.img_no_internet

            }
            APIConstant.Code.NOT_SERVING -> {
                bd.noDataToDisplay.title = title ?: getString(R.string.noting_to_display)
                bd.noDataToDisplay.subTitle = message
                bd.noDataToDisplay.imageResource = R.drawable.img_unserviceable

            }
            APIConstant.Code.NO_DELIVERY_PARTNER -> {
                bd.noDataToDisplay.title = title ?: IConstants.OrderTracking.DELIVERY_UNAVAILABLE
                bd.noDataToDisplay.subTitle = message
                bd.noDataToDisplay.imageResource = R.drawable.img_deliviry_issue

            }
            else -> super.showCodeError(code, title, message)
        }
    }

    override fun onDialogCreated(view: View) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbTopDishes)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)

        }
        bd.tbTopDishes.title =
            title?.split(" ")?.map { item -> item.capitalize() }?.joinToString(" ")
        bd.tbTopDishes.setNavigationOnClickListener { this.dismiss() }
        setupPagingRecyclerView()
        bd.topDishesAppbar.stateListAnimator = AnimatorInflater.loadStateListAnimator(
            context,
            R.animator.app_bar_elevation
        )

        bd.noDataToDisplay.actionCallBack = {
            bd.noDataToDisplay.visibility = View.GONE
            bd.rvTopDishes.visibility = View.VISIBLE
            getDishesList()
        }
    }


}
