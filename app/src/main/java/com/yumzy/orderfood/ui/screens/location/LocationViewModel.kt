package com.yumzy.orderfood.ui.screens.location

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.location.data.LocationRepository
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import javax.inject.Inject

class LocationViewModel  constructor(private val repository: LocationRepository) :
    BaseViewModel<ILocationView>() {

    fun getAddressList() = repository.getAddressList()

    fun addAddress(address: AddressDTO) = repository.addAddress(address)

    fun updateAddress(address: AddressDTO) = repository.updateAddress(address)

}