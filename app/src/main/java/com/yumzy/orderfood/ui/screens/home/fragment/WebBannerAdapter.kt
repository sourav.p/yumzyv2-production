/*
 * Created By Shriom Tripathi 9 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.home.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.appCrashlytics
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.web.WebAppInterface
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst


abstract class WebBannerAdapter<T>(
    val context: Context,
    var items: ArrayList<T>
) :
    RecyclerView.Adapter<WebBannerAdapter<T>.BannerHolder>() {

//    val heightWebViewJSScript = "(function() {var pageHeight = 0;function findHighestNode(nodesList) { for (var i = nodesList.length - 1; i >= 0; i--) {if (nodesList[i].scrollHeight && nodesList[i].clientHeight) {var elHeight = Math.max(nodesList[i].scrollHeight, nodesList[i].clientHeight);pageHeight = Math.max(elHeight, pageHeight);}if (nodesList[i].childNodes.length) findHighestNode(nodesList[i].childNodes);}}findHighestNode(document.documentElement.childNodes); return pageHeight;})()"
    val heightOfWebViewScrip = """(function(){
        var body = document.body,
        html = document.documentElement;
        var height = Math.max( body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);
        return height;
        })();
        """

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WebBannerAdapter<T>.BannerHolder {

        val webView = WebView(context)
        webView.layoutParams =  LinearLayout.LayoutParams(
            ViewConst.MATCH_PARENT,
            ViewConst.MATCH_PARENT
        )

        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        webView.webChromeClient = WebChromeClient()

        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.isScrollbarFadingEnabled = true

        webView.addJavascriptInterface(object : WebAppInterface() {
            var isPageLoaded = false

            override fun onCallback(jsonString: String) { }

            override fun finishIntent(result: Int) { }

            override fun onResize(height: Float) {
                android.os.Handler(Looper.getMainLooper()).postDelayed({
                    if (!isPageLoaded) {
                        this@WebBannerAdapter.onResize(height)
                        this@WebBannerAdapter.notifyDataSetChanged()
                        isPageLoaded = true
                    }
                }, 400)
            }
        }, "Android")

        webView.webViewClient = object : WebViewClient() {
            var isPageLoaded = false
            override fun onPageFinished(view: WebView?, url: String?) {
                (context as HomePageActivity).let {
                    android.os.Handler(Looper.getMainLooper()).postDelayed({
                        view?.evaluateJavascript(heightOfWebViewScrip) { height ->
                            try {
                                if (!isPageLoaded) {
                                    this@WebBannerAdapter.onResize(height.toFloat())
                                    this@WebBannerAdapter.notifyDataSetChanged()
                                    isPageLoaded = true
                                }
                            }catch (e: Exception) {
                                appCrashlytics.log("Web banner adapter: onPageFinished ${e.message}")
                            }
                        }
                    }, 1800)
                }
            }
        }

        /*Usin this empty transparent view so that to disable click inside Webview*/
        val view = View(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewConst.MATCH_PARENT,
                ViewConst.MATCH_PARENT
            )
        }

        val ll = FrameLayout(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewConst.MATCH_PARENT,
                ViewConst.MATCH_PARENT
            )
        }
        ll.addView(webView)
        ll.addView(view)

        return BannerHolder(ll)
    }

    override fun onBindViewHolder(holder: BannerHolder, position: Int) {
        holder.bindData(items[position])
    }

    override fun getItemCount(): Int = items.size

    abstract fun onItemClick(view: View, item: T, bannerPosition: Int)

    abstract fun onResize(height: Float)

    inner class BannerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(bannerItem: T) {
            (itemView as FrameLayout)
            (bannerItem as HomeListDTO)

            (itemView.getChildAt(0) as WebView).loadUrl(bannerItem.meta?.itemUrl ?: "")

            ClickUtils.applyGlobalDebouncing(
                itemView.getChildAt(1),
                IConstants.AppConstant.CLICK_DELAY
            ) {
                onItemClick(itemView.getChildAt(1), items[adapterPosition], adapterPosition)
            }
        }

    }

}