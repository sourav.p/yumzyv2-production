package com.yumzy.orderfood.ui.screens.home.fragment

import android.view.View
import androidx.navigation.fragment.findNavController
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentAccountBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.IModuleHandler
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class AccountFragment  constructor() :
    BaseFragment<FragmentAccountBinding, AccountViewModel>(), IAccountView, IModuleHandler {
    
    override val vm: AccountViewModel by viewModel()
    
    override fun getLayoutId() = R.layout.fragment_account


    //    var userName: String = ""
    private val navController by lazy {
        findNavController()
    }

    override fun onFragmentReady(view: View) {
        vm.attachView(this)
    }

    override fun accountOptionList() {
    }

    override fun tempAccountOptionList() {
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }


}