package com.yumzy.orderfood.ui.screens.home.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.AppUtils
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.databinding.FragmentPlateBinding
import com.yumzy.orderfood.tools.RemoteConfigHelper
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IOrderCustomizationHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.location.LocationInfoActivity
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.offer.OfferActivity
import com.yumzy.orderfood.ui.screens.payment.PaymentActivity
import com.yumzy.orderfood.ui.screens.payment.PaymentConfirmActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.templetes.SpecialInstructionTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import kotlinx.android.synthetic.main.layout_apply_coupon.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PlateFragment  constructor() : BaseFragment<FragmentPlateBinding, PlateViewModel>(),
    IPlateView, ActionDialog.OnActionPerformed {
    override val vm: PlateViewModel by viewModel()

    val cartHelper: CartHelper by inject()

    private val COUPON_REQUEST_CODE = 0
    var locUnserviceable = 0
    var restaurantClosed: Boolean = false
    var callOnResume: Boolean = true

    override fun getLayoutId() = R.layout.fragment_plate

    var outletId: String? = null
    var outletName: String? = null
    var idsSize: Int = 0
    var city: String? = null

    override fun onFragmentReady(view: View) {

        applyDebouchingClickListener(
            bd.restaurantDetailsView,
            bd.btnAddNewAddress,
            bd.btnPayNow,
            bd.specialInstructionLayout,
            bd.tvDelete,
            bd.tvClearCart,
            bd.backArrow,
            bd.walletView


        )
        bd.couponLayoutView.addCoupon = { it ->
            val intent = Intent(context, OfferActivity::class.java)
            intent.putExtra(
                IConstants.ActivitiesFlags.FLAG,
                IConstants.ActivitiesFlags.FROM_PLATE
            )
            AppIntentUtil.launchForResultWithSheetAnimation(
                requireActivity() as AppCompatActivity,
                intent
            ) { resultOk, actionResult ->
                if (resultOk) {
                    callOnResume = false
                    applyCoupon(actionResult.data)

                }
            }
        }

        bd.couponLayoutView.couponRemove = { it ->


            val body = mutableMapOf<String, Any>()
            body["cartId"] = sharedPrefsHelper.getCartId().toString()
            body["couponCode"] = bd.couponLayoutView.tv_coupon_code.text.toString()
            CleverTapHelper.couponRemoved(cartHelper.mSavedCart.value)

            vm.removeCoupon(body).observe(this, { result ->
                RHandler<CartResDTO>(
                    this,
                    result

                ) {
                    bd.couponLayoutView.appliedCode = ""
                    setBillingView(it)
                }
            })


        }
        bd.icvEmptyPlate.actionCallBack = {
            (context as AppCompatActivity).onBackPressed()
        }

        val navHeight: Int = YumUtil.getNavBarHeight(context as AppCompatActivity)
        val statusBarHeight: Int = YumUtil.getStatusBarHeight(resources)
        val navDp = YumUtil.pxToDp(navHeight)
        val StatusBarDp = YumUtil.pxToDp(statusBarHeight)
        bd.paddingBottom = navDp.px
        bd.paddingTop = StatusBarDp.px


        /* bd.walletView.setWalledClickListener{
             onWalletClick()
         }*/
    }

    override fun onResume() {
        super.onResume()
        bd.icvEmptyPlate.actionText = context?.resources?.getString(R.string.browse_restaurants)
        bd.icvEmptyPlate.title = context?.resources?.getString(R.string.empty_plate_header)
        bd.icvEmptyPlate.subTitle = context?.resources?.getString(R.string.add_smthing_from_menu)
        bd.icvEmptyPlate.imageResource = R.drawable.ic_empty_plate
        if (callOnResume) {
            fetchUserCart(AppGlobalObjects.selectedAddress)

        }

    }

    override fun fetchUserCart(address: AddressDTO?) {
        if (address == null) {
            //bd.tvInstruction.text = ""
            val currentCity = AppGlobalObjects.selectedAddress?.city ?: ""
            val cartLongLatDTO = CartLongLatDTO(
                arrayListOf(
                    AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(0) ?: 0.0,
                    AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(1) ?: 0.0
                )
            )
            callPreCheckoutAPI(cartLongLatDTO, currentCity, AppGlobalObjects.selectedAddress)
        } else {
            // bd.tvInstruction.text = ""
            val currentCity = address.city
            val cartLongLatDTO = CartLongLatDTO(
                arrayListOf(
                    address.longLat?.coordinates?.get(0) ?: 0.0,
                    address.longLat?.coordinates?.get(1) ?: 0.0
                )
            )
            callPreCheckoutAPI(cartLongLatDTO, currentCity, address)
        }
    }

    fun getUserWallet() {
        vm.getUserWallet(sharedPrefsHelper.getUserPhone().toLong())
            .observe(this, Observer { result ->
                if (result.type == -1)
                    return@Observer
                RHandler<WalletDTO>(
                    this,
                    result
                ) { walletDTO ->
                    bd.walletView.setWalletDetails(walletDTO)
                }
            })
    }

    @SuppressLint("SetTextI18n")
    private fun callPreCheckoutAPI(
        coordinate: CartLongLatDTO,
        city: String,
        selectedAddress: AddressDTO?
    ) {

        val address: PreCheckoutAddress? = PreCheckoutAddress(coordinate, city)
        val preCheckout = PreCheckoutReqDTO(address, sharedPrefsHelper.getCartId().toString())
        vm.preCheckout(preCheckout).observe(this, Observer
        { result ->

            if (result.data is PreCheckoutResDTO) {

                if (result.type == APIConstant.Status.ERROR) {
                    hidePlateView()
                    return@Observer
                }


                val locationUnservicable = getString(R.string.change_delivery_address)
                when (result.data.code) {
                    APIConstant.Code.INVALID_CART_PAYLOAD -> {
                        hidePlateView()
                        return@Observer
                    }
                    APIConstant.Code.ITEM_NOT_SERVING -> {

                        showPlateView()
                        restaurantClosed = false
                        bd.llRvCartMenu.visibility = View.VISIBLE
                        bd.llRvCartMenu.setOnClickListener {
                            AlerterHelper.showToast(
                                requireContext(),
                                result.data.ids?.size.toString() + " " + getString(R.string.item_unavailable),
                                result.data.error.capitalize()
                            )
                        }

                        setCartData(result.data)
                        bd.tvErrorMsg.text =
                            result.data.ids?.size.toString() + " " + getString(R.string.item_unavailable)
                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
                        bd.tvErrorDec.text = result.data.error.capitalize()
                        bd.btnAddNewAddress.text = getString(R.string.remove_item)
                        AlerterHelper.showToast(
                            requireContext(),
                            result.data.ids?.size.toString() + " " + getString(R.string.item_unavailable),
                            result.data.error.capitalize()
                        )

                        bd.couponLayoutView.visibility = View.GONE
                        bd.walletView.visibility = View.GONE

//                        bd.tvChangeAddress.visibility = View.GONE
                        setDisablePayNow()
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )

                        bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_plate_2,
                            context?.theme
                        )
                        bd.btnAddNewAddress.setOnClickListener {
                            val cartMenuAdapter = bd.rvCartMenu.adapter as CartMenuAdapter
//                            val availableItemList = arrayListOf<CartItem>()
                            cartMenuAdapter.cartMenuListDTO.reversed().forEach { cartItem ->

                                if (cartItem.isUnavailable) {
                                    removeItem(
                                        cartItem.itemID,
                                        0,
                                        "",
                                        cartItem.itemPosition,
                                        cartItem.outletID
                                    )
                                }

                            }
                        }
                    }
                    APIConstant.Code.OUTLET_NOT_SERVING -> {
                        showPlateView()
//                        restaurantClosed = true
                        restaurantClosed = false
                        setCartData(result.data)
                        setDisablePayNow()

                        bd.tvErrorMsg.text = getString(R.string.restaurant_closed)
                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
//                        bd.llRvCartMenu.visibility = View.VISIBLE
//                        bd.llRvCartMenu.setOnClickListener {
//                            AlerterHelper.showToast(
//                                requireContext(),
//                                getString(R.string.restaurant_closed),
//                                result.data.error.capitalize()
//                            )
//
//                        }
                        AlerterHelper.showToast(
                            requireContext(),
                            getString(R.string.restaurant_closed),
                            result.data.error.capitalize()
                        )
                        bd.tvErrorDec.text = result.data.error.capitalize()
                        bd.btnAddNewAddress.text = getString(R.string.check_other_restaurant)
                        bd.couponLayoutView.visibility = View.GONE
                        bd.walletView.visibility = View.GONE


                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_closed_outlet,
                            context?.theme
                        )
                        ClickUtils.applyGlobalDebouncing(
                            bd.btnAddNewAddress,
                            IConstants.AppConstant.CLICK_DELAY
                        ) {
                            val title = "Restaurant not serving"
                            val sMessage =
                                getString(R.string.discard_selection) + " " + outletName + "?"

                            val actions: ArrayList<ActionItemDTO?> = arrayListOf(
                                ActionItemDTO(
                                    key = ActionConstant.CLOSE,
                                    textColor = ThemeConstant.textBlackColor,
                                    buttonColor = null,
                                    text = getString(R.string.cancel),
                                ),
                                ActionItemDTO(
                                    key = ActionConstant.POSITIVE,
                                    text = getString(R.string.clear_plate),
                                )
                            )
                            showClearCartActionDialog(
                                IConstants.ActionModule.CLEAR_PLATE,
                                title,
                                sMessage,
                                actions
                            )

                        }


                    }
                    APIConstant.Code.OFFER_INVALID -> {
                        showPlateView()


                        result.data.cartDoc?.offer?.let { promoteOffer(it) }
                        restaurantClosed = false
                        setCartData(result.data)
                        bd.llRvCartMenu.visibility = View.GONE
//                       bd.tvErrorMsg.text = getString(R.string.invalid_offer)
//                        bd.tvErrorDec.text = result.data.error
                        bd.tvErrorDec.text = selectedAddress?.fullAddress?.trim()
                        bd.tvErrorMsg.text =
                            getString(R.string.deliver_to) + selectedAddress?.name?.trim()
                        setIconTag(selectedAddress?.name ?: "Address")
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
                        bd.btnAddNewAddress.text = locationUnservicable
                        bd.couponLayoutView.visibility = View.VISIBLE
                        bd.walletView.visibility = View.VISIBLE

                        setEnablePayNow()
                        /*  bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                              resources,
                              R.drawable.btn_gray_background,
                              context?.theme
                          )*/
                        /*bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_coupon_circular,
                            context?.theme
                        )*/
                        callOnResume = true

                        ClickUtils.applyGlobalDebouncing(
                            bd.btnAddNewAddress,
                            IConstants.AppConstant.CLICK_DELAY
                        ) {
                            (context as HomePageActivity).displayGpsDialog(view)
                        }
                    }
                    APIConstant.Code.OFFER_MIN_ORDER -> {
                        showPlateView()
                        result.data.cartDoc?.offer?.let { promoteOffer(it) }
                        restaurantClosed = false
                        setCartData(result.data)
//                        bd.tvErrorMsg.text = getString(R.string.offer_unapplicable)
//                        bd.tvErrorDec.text = result.data.error

                        bd.tvErrorDec.text = selectedAddress?.fullAddress?.trim()
                        bd.tvErrorMsg.text =
                            getString(R.string.deliver_to) + selectedAddress?.name?.trim()
                        setIconTag(selectedAddress?.name ?: "Address")

                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
                        bd.llRvCartMenu.visibility = View.GONE

                        setEnablePayNow()
                        bd.btnAddNewAddress.text = locationUnservicable
                        bd.couponLayoutView.visibility = View.VISIBLE
                        bd.walletView.visibility = View.VISIBLE
//                        bd.tvChangeAddress.visibility = View.VISIBLE
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        callOnResume = true
                        ClickUtils.applyGlobalDebouncing(
                            bd.btnAddNewAddress,
                            IConstants.AppConstant.CLICK_DELAY
                        ) {
                            (context as HomePageActivity).displayGpsDialog(view)

                        }
                    }
                    APIConstant.Code.DELIVERY_PERSON_UNAVAILABLE -> {
//                        CleverTapHelper.pushEvent(CleverEvents.unable_to_checkout)
                        restaurantClosed = false
                        showPlateView()
                        setCartData(result.data)
                        bd.tvErrorMsg.text = getString(R.string.person_unavailable)
                        bd.tvErrorDec.text = result.data.error.capitalize()
                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
                        setDisablePayNow()
                        bd.btnAddNewAddress.text = getString(R.string.try_later)
                        bd.couponLayoutView.visibility = View.GONE
                        bd.walletView.visibility = View.GONE
//                        bd.llRvCartMenu.visibility = View.VISIBLE
//                        bd.llRvCartMenu.setOnClickListener {
//                            AlerterHelper.showToast(
//                                requireContext(),
//                                getString(R.string.person_unavailable),
//                                result.data.error.capitalize()
//                            )
//
//                        }
                        AlerterHelper.showToast(
                            requireContext(),
                            getString(R.string.person_unavailable),
                            result.data.error.capitalize()
                        )
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_rider,
                            context?.theme
                        )
                        bd.btnAddNewAddress.setOnClickListener {
                            (context as AppCompatActivity).onBackPressed()
                        }

                    }
                    APIConstant.Code.LOCATION_UNSERVICEABLE -> {

                        setDisablePayNow()
//                        CleverTapHelper.pushEvent(CleverEvents.unable_to_checkout)
                        restaurantClosed = false
                        showPlateView()
                        setCartData(result.data)
                        val title = getString(R.string.loc_unserviceable)
                        bd.tvErrorMsg.text = title
                        bd.tvErrorDec.text = result.data.error.capitalize(Locale.getDefault())
                        bd.tvErrorMsg.setTextColor(ThemeConstant.pinkies)
                        bd.btnAddNewAddress.text = locationUnservicable
                        bd.couponLayoutView.visibility = View.GONE
                        bd.walletView.visibility = View.GONE
//                        bd.llRvCartMenu.visibility = View.VISIBLE
//                        bd.llRvCartMenu.setOnClickListener {
//                            AlerterHelper.showToast(
//                                requireContext(),
//                                title, locationUnservicable
//                            )
//                            // YumUtil.animateShake(bd.btnAddNewAddress, 20, 5)
//
//                        }
                        AlerterHelper.showToast(
                            requireContext(),
                            title, locationUnservicable
                        )
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_placeholder,
                            context?.theme
                        )
                        callOnResume = true
                        ClickUtils.applyGlobalDebouncing(
                            bd.btnAddNewAddress,
                            IConstants.AppConstant.CLICK_DELAY
                        ) {

                            locUnserviceable = APIConstant.Code.LOCATION_UNSERVICEABLE

                            (context as HomePageActivity).displayGpsDialog(view)
                        }

                    }
                    APIConstant.Code.EMPTY_CART -> {
                        hidePlateView()
                        cartHelper.getCart().observeForever {}
                        return@Observer
                    }
                    else -> {
                        showPlateView()

                        bd.llRvCartMenu.visibility = View.GONE
                        restaurantClosed = false
                        if (!sharedPrefsHelper.getIsTemporary()) {
                            getUserWallet()
                            bd.walletView.visibility = View.VISIBLE

                        } else {
                            bd.walletView.visibility = View.GONE
                        }
                        setCartData(result.data)
                        setEnablePayNow()
                        bd.couponLayoutView.visibility = View.VISIBLE
                        bd.btnAddNewAddress.text = locationUnservicable
                        bd.imageViewError.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.ic_placeholder,
                            context?.theme
                        )
                        bd.tvErrorDec.text = selectedAddress?.fullAddress
                        bd.tvErrorMsg.text =
                            getString(R.string.deliver_to) + selectedAddress?.name
                        setIconTag(selectedAddress?.name ?: "Address")
                        callOnResume = true
                        bd.btnAddNewAddress.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.btn_gray_background,
                            context?.theme
                        )
                        ClickUtils.applyGlobalDebouncing(
                            bd.btnAddNewAddress,
                            IConstants.AppConstant.CLICK_DELAY
                        ) {
                            (context as HomePageActivity).displayGpsDialog(view)
                        }
                    }
                }
            }
        })
    }

    private fun showSaveLocationDialog(selectedAddress: AddressDTO?) {
        val title = this.getString(R.string.address_not_saved)
        val sMessage = this.getString(R.string.address_not_saved_detail)

        val actions: ArrayList<ActionItemDTO?> = arrayListOf(
            ActionItemDTO(
                key = ActionConstant.POSITIVE,
                text = this.getString(R.string.ok),
                textColor = ThemeConstant.pinkies,
                buttonColor = null
            )
        )

        ActionModuleHandler.showChoseActionDialog(
            requireContext(),
            IConstants.ActionModule.SAVED_ADDRESS,
            title,
            sMessage,
            actions,
            object : ActionDialog.OnActionPerformed {
                override fun onSelectedAction(
                    dialog: Dialog?,
                    actionType: Int,
                    action: ActionItemDTO?
                ) {
                    when {
                        actionType == IConstants.ActionModule.SAVED_ADDRESS && action?.key == ActionConstant.POSITIVE -> {
                            val intent = Intent(requireContext(), LocationInfoActivity::class.java)
                            intent.putExtra(IConstants.ActivitiesFlags.ADDRESS, selectedAddress)
                            startActivity(intent)
                        }
                    }
                }
            }, resources.getString(R.string.icon_location_point)
        )
    }

    private fun promoteOffer(offer: MissingOfferDTO?) {
        if (offer == null) {
            bd.tvPromoteOffer.visibility = View.GONE
            return
        }
        bd.tvPromoteOffer.visibility = View.VISIBLE
        bd.tvPromoteOffer.text =
            offer.description + " " + getString(R.string.add_few_item_to_avail) + " " + offer.promoName

    }

    private fun setCartData(cartResDTO: PreCheckoutResDTO) {
        if (cartResDTO.cartDoc?.outlets.isNullOrEmpty()) {
            hidePlateView()
            return
        } else {

            showPlateView()

            outletId = cartResDTO.cartDoc?.outlets?.get(0)?.outletId
            if (!sharedPrefsHelper.getOutletID().equals("")) {
                if (!sharedPrefsHelper.getOutletID().equals(outletId)) {
                    bd.tvInstruction.text = ""
                }
            }
            sharedPrefsHelper.setOutletID(outletId.toString())
            outletName = cartResDTO.cartDoc?.outlets?.get(0)?.outletName
            bd.restaurantDetailsView.mTitle = cartResDTO.cartDoc?.outlets?.get(0)?.outletName!!
            bd.restaurantDetailsView.mSubtitle = cartResDTO.cartDoc.outlets.get(0)?.locality!!
            bd.restaurantDetailsView.mImage =
                cartResDTO.cartDoc.outlets.get(0)?.outletLogo.toString()
            bd.billingDetailsView.removeAllViews()

            var amount: Double = 0.0
            val paymentWallet = cartResDTO.cartDoc.payments?.find { paymentDTO ->
                paymentDTO.paidVia.equals(IConstants.PaymentMethod.WALLET)
            }
            val paymentGateWay = cartResDTO.cartDoc.payments?.find { paymentDTO ->
                paymentDTO.paidVia.equals(IConstants.PaymentMethod.PAYMENTGATEWAY)
            }

            if (paymentWallet != null) {
                bd.walletView.setWalletSelected(true, paymentWallet.amount!!)
            } else {
                bd.walletView.setWalletSelected(false)
            }
            if (paymentGateWay != null) {
                amount = paymentGateWay.amount!!
            }
            val billing: Billing =
                Billing(
                    amount,
                    cartResDTO.cartDoc.billing.discount,
                    cartResDTO.cartDoc.billing.subTotal,
                    cartResDTO.cartDoc.billing.charges,
                    cartResDTO.cartDoc.billing.taxes,
                    cartResDTO.cartDoc.billing.originalDelivery
                )

            bd.billingDetailsView.setOutletInfoHeader()
            bd.billingDetailsView.billing = billing
            bd.btnPayNow.text = YumUtil.spanSizeString(
                "Pay",
                "\n₹ ${amount.withPrecision(2)}",
                0.8f
            )
            if (sharedPrefsHelper.getIsTemporary()) {
                bd.btnPayNow.text = "Login"

            }
            if (!cartResDTO.cartDoc.coupons.isNullOrEmpty()) {
                bd.couponLayoutView.appliedCode = cartResDTO.cartDoc.coupons.get(0)?.code
            } else {
                bd.couponLayoutView.appliedCode = ""

            }


            cartResDTO.cartDoc.outlets.get(0)!!.itemDTOS.let {
                if (restaurantClosed) {
                    for (position in it.indices) {
                        it[position].isUnavailable = true
                    }
                } else if (!cartResDTO.ids.isNullOrEmpty()) {
                    for (position in it.indices) {
                        it[position].isUnavailable = cartResDTO.ids.contains(it[position].itemID)
                        idsSize = cartResDTO.ids.size
                    }
                }

                fetchCartMenu(it)
            }
        }
    }

    fun fetchCartMenu(itemDTOS: List<CartItemDTO>) {
        this.let {
            bd.rvCartMenu.adapter =
                CartMenuAdapter(
                    it,
                    itemDTOS,
                    clickedListener = { position, itemId, count, price, outletid ->
                        CleverTapHelper.plateItemChange(position, itemId, count, price, outletid)
                        itemId?.let { it1 ->
                            onItemAdded(
                                it1,
                                count,
                                price,
                                position,
                                outletid,
                                bd.rvCartMenu.adapter as CartMenuAdapter
                            )
                        }
                    }, addAddonsclickedListener = { position, cartItem ->

                        val addons = ArrayList<RestaurantAddOnDTO>()
                        val mapAndKey =
                            sharedPrefsHelper.getCartOutletAddon(cartItem.itemID.toString())
                        if (mapAndKey != null && mapAndKey.first.isNotEmpty() && mapAndKey.second.isNotEmpty()) {
                            mapAndKey.second.forEach { addonId ->

                                val customization = mapAndKey.first.get(addonId)!!.clone()

                                if (customization.isVariant) {
                                    val selectedVariant = cartItem.variant
                                    if (selectedVariant != null) {
                                        customization.options?.forEach { option: RestaurantAddOnOption ->
                                            if (selectedVariant.optionsDTO.optionName.toLowerCase(
                                                    Locale.getDefault()
                                                ) == option.optionName.toLowerCase(Locale.getDefault())
                                            ) {
                                                option.isSelected = true
                                            }
                                        }
                                    }

                                } else {
                                    val selectedAddon =
                                        cartItem.addons.find { item -> item.addonID == customization.addonId }

                                    customization.options?.forEach { option: RestaurantAddOnOption ->
                                        if (selectedAddon?.options?.find { selectedOption ->
                                                selectedOption.optionName.toLowerCase(
                                                    Locale.getDefault()
                                                ) == option.optionName.toLowerCase(Locale.getDefault())
                                            } != null) {
                                            option.isSelected = true
                                        }
                                    }
                                }

                                customization.doInitialSelection = false
                                addons.add(customization)
                            }

                            showCustomizationDialog(
                                cartItem.itemID.toString(),
                                cartItem.outletID,
                                addons,
                                cartItem.quantity,
                                position,
                                cartItem
                            ) { }

                        }

                    })
            bd.rvCartMenu.layoutManager = LinearLayoutManager(context)
        }
    }

    private fun showCustomizationDialog(
        itemId: String,
        outletid: String,
        addons: List<RestaurantAddOnDTO>,
        count: Int,
        position: Int,
        cartItemDTO: CartItemDTO,
        callerFun: (isSuccess: Boolean) -> Unit
    ) {
/*
        addons.forEach{ addons ->
            for((i, value) in cartItem. )
        }
        */
        outletName?.let {

            ActionModuleHandler.showOrderCustomization(
                requireContext(),
                RestaurantItem().let {
                    it.name = cartItemDTO.name
                    it.price = cartItemDTO.price
                    return@let it
                },
                addons,
                object : IOrderCustomizationHandler {
                    override fun onSuccess(
                        variant: Variant?,
                        addOns: List<Variant>
                    ) {

                        val items = outletId?.let {
                            NewItemDTO(
                                it,
                                itemId,
                                cartItemDTO.price,
                                count,
                                variant,
                                addOns
                            )
                        }

                        val cartt = items?.let {
                            NewAddItemDTO(
                                it,
                                sharedPrefsHelper.getCartId().toString()
                            )
                        }

                        cartt?.let { addToCart(it, position) { callerFun(it) } }

                    }

                    override fun onDismiss(dissmissType: Int) {
                        /* if (mSavedCart.value != null && mSavedCart.value?.outlets?.isNullOrEmpty() == false) {
                                 mSavedCart.value?.outlets!![0]?.items?.forEach { item ->
                                     if (item.itemID == restaurantItem.itemId) {
                                         view.quantityCount = item.quantity
                                         return@forEach
                                     }
                                 }
                             } else {
                                 view.quantityCount = 0
                             }*//*
                        if (dissmissType == 2) {
                            callerFun(false)
                        }*/
                    }
                })
        }
    }

    //Update addon variant///
    private fun addToCart(
        cartt: NewAddItemDTO,
        position: Int,
        callerFun: (isSuccess: Boolean) -> Unit
    ) {
        removeAndModifyAddons(
            "",
            0,
            "",
            position,
            "",
            bd.rvCartMenu.adapter as CartMenuAdapter,
            cartt
        )
    }

    private fun removeAndModifyAddons(
        s: String,
        i: Int,
        s1: String,
        position: Int,
        s2: String,
        cartMenuAdapter: CartMenuAdapter,
        cartt: NewAddItemDTO
    ) {

        val cartId = ItemCountDTO(outletId, sharedPrefsHelper.getCartId().toString())

        vm.updateItem(position, cartt).observe(this, { result ->
            RHandler<CartResDTO>(
                this, result
            ) {
                cartHelper.getCart().observeForever { }
                if (it.outletDTOS.isNullOrEmpty()) {
                    hidePlateView()

                } else {
                    showPlateView()
                    fetchUserCart(null)

                }
            }
        })
        /*
        vm.itemCount(position, 0, cartId).observe(this, Observer { result ->
//            if (result.type == -1)
//                return@Observer
            RHandler<CartResDTO>(
                this,
                result
            ) {

                showPlateView()
                vm.addToCart(cartt).observe(this, Observer { result ->
                    *//*if (result.type == -1)
                        return@Observer*//*
                    RHandler<CartResDTO>(
                        this,
                        result
                    ) {
                        cartHelper.getCart().observeForever { }
                        if (it.outletDTOS.isNullOrEmpty()) {
                            hidePlateView()

                        } else {
                            showPlateView()
                            fetchUserCart(null)

                        }
                    }

                })
            }
        })*/
    }

    private fun removeItem(
        itemId: String,
        count: Int,
        price: String,
        position: Int,
        outlet: String

    ) {
        val cartId = ItemCountDTO(outletId, sharedPrefsHelper.getCartId().toString())
        vm.itemCount(position, count, cartId).removeObservers(this)
        vm.removeSelectionByItemId(itemId).observe(this, { })
        vm.itemCount(position, count, cartId)
            .observe(this, getRemoveObserver(position, count, cartId.cartId))
    }

    fun getRemoveObserver(position: Int, count: Int, cartId: String) =
        Observer<ResponseDTO<Any>> { result ->
            RHandler<CartResDTO>(
                this,
                result
            ) {
                cartHelper.getCart().observeForever { }
                if (it.outletDTOS.isNullOrEmpty()) {
                    hidePlateView()

                } else {
                    fetchUserCart(null)

//                    showProgressBar()
//                    Handler().postDelayed({
//                        fetchUserCart(null)
//                        hideProgressBar()
//
//                    }, 500)
                }
            }

        }

    //  increase/decrease item count
    private fun onItemAdded(
        itemId: String,
        count: Int,
        price: String,
        position: Int,
        outlet: String,
        adapter: CartMenuAdapter
    ) {

        val cartId = ItemCountDTO(outletId, sharedPrefsHelper.getCartId().toString())
        vm.itemCount(position, count, cartId).observe(this, { result ->
            /* if (result.type == -1)
                 return@Observer*/
            RHandler<CartResDTO>(
                this,
                result
            ) {

                cartHelper.getCart().observeForever {}

                if (it.outletDTOS.isNullOrEmpty()) {
                    hidePlateView()
                } else {
                    showPlateView()
//                    adapter.setItemQuantity(position, count)
                    adapter.setItemList(it.outletDTOS[0]?.itemDTOS)
                    setBillingView(it)
                    if (!it.coupons.isNullOrEmpty()) {
                        bd.couponLayoutView.appliedCode = it.coupons.get(0)?.code
                    } else {
                        bd.couponLayoutView.appliedCode = ""
                    }
                }
            }

        })
    }

    private fun setBillingView(it: CartResDTO) {
        var amount: Double = 0.0
        bd.billingDetailsView.removeAllViews()
        val paymentWallet =
            it.payments?.find { paymentDTO -> paymentDTO.paidVia.equals(IConstants.PaymentMethod.WALLET) }
        val paymentGateWay =
            it.payments?.find { paymentDTO -> paymentDTO.paidVia.equals(IConstants.PaymentMethod.PAYMENTGATEWAY) }

        if (paymentWallet != null) {
            bd.walletView.setWalletSelected(true, paymentWallet.amount!!)
        } else {
            bd.walletView.setWalletSelected(false)
        }
        if (paymentGateWay != null) {
            amount = paymentGateWay.amount!!
        }
        val billing: Billing =
            Billing(
                amount,
                it.billing?.discount!!,
                it.billing.subTotal,
                it.billing.charges,
                it.billing.taxes,
                it.billing.originalDelivery
            )
        bd.billingDetailsView.setOutletInfoHeader()
        bd.billingDetailsView.billing = billing
        bd.btnPayNow.text =
            YumUtil.spanSizeString("Pay", "\n₹ ${amount.withPrecision(2)}", 0.8f)
        if (sharedPrefsHelper.getIsTemporary()) {
            bd.btnPayNow.text = resources.getString(R.string.login)
        }
    }

    override fun onSuccess(actionId: Int, data: Any?) {

        if (locUnserviceable == APIConstant.Code.LOCATION_UNSERVICEABLE) {
            fetchUserCart(null)
        }
    }

    private fun setIconTag(addressTag: String) {
        when {
            addressTag == "home" -> {
                bd.imageViewError.background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_placeholder_home,
                    context?.theme
                )
            }
            addressTag.equals("office") -> {
                bd.imageViewError.background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_placeholder_office,
                    context?.theme
                )
            }
            else -> {
                bd.imageViewError.background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_placeholder_empty,
                    context?.theme
                )
            }
        }
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.back_arrow -> {
                (context as AppCompatActivity).onBackPressed()
            }

            R.id.btn_pay_now -> {

                if (sharedPrefsHelper.getIsTemporary()) {
//                    val intent = Intent(requireActivity(), TempLoginActivity::class.java)
//                    intent.putExtra(
//                        IConstants.FragmentFlag.FLAG,
//                        IConstants.FragmentFlag.OPEN_LOGIN
//                    )
//                    startActivity(intent)
//                    requireActivity().finish()
                    AppUtils.relaunchApp()


                } else {
                    if (AppGlobalObjects.selectedAddress?.addressId.isNullOrEmpty()) {
                        showSaveLocationDialog(AppGlobalObjects.selectedAddress)
                        return
                    }

                    val longitude = AppGlobalObjects.selectedAddress?.longLat?.coordinates!![0]
                    val latitude = AppGlobalObjects.selectedAddress?.longLat?.coordinates!![1]
                    val longLat = LongLat(coordinates = arrayListOf(longitude, latitude))
                    val deliveryAddress = AddressCheckOut(
                        AppGlobalObjects.selectedAddress!!.name,
                        AppGlobalObjects.selectedAddress!!.landmark,
                        AppGlobalObjects.selectedAddress!!.addressId,
                        AppGlobalObjects.selectedAddress!!.addressTag,
                        AppGlobalObjects.selectedAddress!!.googleLocation,
                        AppGlobalObjects.selectedAddress!!.houseNum,
                        AppGlobalObjects.selectedAddress!!.fullAddress,
                        AppGlobalObjects.selectedAddress!!.city, "", "", "", longLat
                    )
                    val newOrder: NewOrderReqDTO? =
                        deliveryAddress.let { it1 ->
                            NewOrderReqDTO(
                                it1,
                                bd.tvInstruction.text.toString(),
                                sharedPrefsHelper.getCartId().toString()
                            )
                        }


                    try {
                        initiatePayment(newOrder)
                    } catch (e: Exception) {
                        showSnackBar(
                            "Unable to Start Payment Sever Broken \nTry after some time",
                            Color.RED
                        )
                    }

                }

            }
            R.id.restaurant_details_view -> {
                val intent = Intent(context, OutletActivity::class.java)
                intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
//                startActivity(intent)
                AppIntentUtil.launchWithSheetAnimation(requireActivity(), intent)
            }


            R.id.tv_clear_cart, R.id.tv_delete -> {
                val sMessage = getString(R.string.discard_selection) + " " + outletName + "?"
                val title = getString(R.string.clear_cart)
                val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                    ActionItemDTO(
                        key = ActionConstant.POSITIVE,
                        text = getString(R.string.clear_plate),
                    ),
                    ActionItemDTO(
                        key = ActionConstant.CLOSE,
                        textColor = ThemeConstant.textBlackColor,
                        buttonColor = null,
                        text = getString(R.string.cancel),
                    )
                )
                showClearCartActionDialog(
                    IConstants.ActionModule.CLEAR_PLATE,
                    title,
                    sMessage,
                    actions
                )

            }

//            bd.couponLayoutView.id -> {
//                val intent = Intent(context, OfferActivity::class.java)
//                intent.putExtra(
//                    IConstants.ActivitiesFlags.FLAG,
//                    IConstants.ActivitiesFlags.FROM_PLATE
//                )
//                startActivityForResult(intent, COUPON_REQUEST_CODE)
//            }

//            bd.couponLayoutView.icon_action.id -> {
//
//                val body = mutableMapOf<String, Any>()
//                body["cartId"] = sharedPrefsHelper.getCartId().toString()
//                body["couponCode"] = bd.couponLayoutView.tv_coupon_code.text.toString()
//                CleverTapHelper.couponRemoved(cartHelper.mSavedCart.value)
//
//                vm.removeCoupon(body).observe(this, Observer { result ->
//                    RHandler<CartResDTO>(
//                        this,
//                        result
//
//                    ) {
//                        bd.showCouponView = true
//                        bd.couponLayoutView.appliedCode = ""
//
//                        setBillingView(it)
//                    }
//                })
//
//
//            }

            R.id.special_instruction_layout -> {
                val detailActionDialog = DetailActionDialog()
                val template = SpecialInstructionTemplate(
                    context as AppCompatActivity,
                    bd.tvInstruction.text.toString().trim().replace("\\s+".toRegex(), " ")
                        .capitalize(Locale.getDefault())
                ) {
                    bd.tvInstruction.text =
                        it.trim().replace("\\s+".toRegex(), " ").capitalize(Locale.getDefault())

                }
                detailActionDialog.detailsTemplate = template
                detailActionDialog.showNow(
                    (context as AppCompatActivity).supportFragmentManager,
                    ""
                )
            }
            R.id.wallet_view -> {
                onWalletClick()
            }
        }
    }

    private fun initiatePayment(newOrder: NewOrderReqDTO?) {

        if (newOrder != null) {
            val i = Intent(requireContext(), PaymentConfirmActivity::class.java)
            i.putExtra(PaymentActivity.INTENT_ORDER, appGson.toJson(newOrder))

            AppIntentUtil.launchForResultWithSheetAnimation(
                requireActivity() as AppCompatActivity,
                i
            ) { _, result ->

                try {
                    val data = result.data

                    val title = data?.getStringExtra(
                        IConstants.ActivitiesFlags.PAYMENT_TITLE

                    ) ?: ""

                    val body = data?.getStringExtra(
                        IConstants.ActivitiesFlags.PAYMENT_BODY

                    ) ?: "Payment Failed"

                    val intExtra = data?.getIntExtra(
                        IConstants.ActivitiesFlags.PAYMENT_RESPONSE,
                        IConstants.PaymentConst.PAYMENT_FAILED
                    )

                    val earnedScratch = data?.getBooleanExtra(
                        IConstants.ActivitiesFlags.EARNED_SCRATCH,
                        false
                    )


                    if (intExtra == IConstants.PaymentConst.PAYMENT_NOT_REQUIRED) {
                        ActionModuleHandler.showOrderTracking(
                            requireContext(),
                            body
                        )
                    }

                    when (intExtra) {
                        IConstants.PaymentConst.PAYMENT_FAILED -> {
                            //  YumUtil.animateShake(bd.llErrorMsgView, 50, 15)
                            CleverTapHelper.paymentFailed(cartHelper.mSavedCart.value)
//                            AppUIController.showOrderFailedUI(title, body)
                            AlerterHelper.showToast(requireContext(), title, body)


                        }
                        IConstants.PaymentConst.PAYMENT_SUCCESS -> {
                            animPaymentSuccess(body, earnedScratch ?: false)
                            hidePlateView()
                            val earnedScratch = data.getBooleanExtra(
                                IConstants.ActivitiesFlags.EARNED_SCRATCH, false
                            )
                            CleverTapHelper.paymentSuccess(
                                cartHelper.cartRepository.userCart?.value,
                                sharedPrefsHelper,
                                earnedScratch
                            )
                            CleverTapHelper.pushChargeEvent(
                                cartHelper.cartRepository.userCart?.value,
                                sharedPrefsHelper,
                                earnedScratch
                            )
                        }
                    }

                } catch (e: Exception) {
                    showError(e.message.toString())
                }
            }

        }
        CleverTapHelper.paymentInitiated(cartHelper.mSavedCart.value)
    }

    private fun showClearCartActionDialog(
        actionType: Int,
        title: CharSequence,
        sMessage: CharSequence,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            requireContext(),
            actionType,
            title,
            sMessage,
            actions,
            this,
            iconString = resources.getString(R.string.icon_dish)
        )
    }

    private fun onWalletClick() {
        if (bd.walletView.isWalletSelected) {
            val hashMap: HashMap<String, String> = HashMap<String, String>()
            hashMap.put("cartId", sharedPrefsHelper.getCartId().toString())
            vm.unUseWallet(hashMap).observe(this, { result ->
//                if (result.type == -1)
//                    return@Observer
                RHandler<PreCheckoutResDTO>(
                    this,
                    result
                ) {
                    fetchUserCart(null)
                }
            })

        } else {
            val hashMap: HashMap<String, String> = HashMap<String, String>()
            hashMap.put("cartId", sharedPrefsHelper.getCartId().toString())

            vm.useWallet(hashMap).observe(this, { result ->
//                if (result.type == -1)
//                    return@Observer
                RHandler<PreCheckoutResDTO>(
                    this,
                    result
                ) {
                    fetchUserCart(null)
                }
            })
        }
    }

    override fun showProgressBar() {
        setDisablePayNow()
        super.showProgressBar()
    }

    override fun hideProgressBar() {
        setEnablePayNow()
        super.hideProgressBar()
    }

    private fun applyCoupon(data: Intent?) {
        val coupon = data?.getStringExtra(IConstants.ActivitiesFlags.RESULT_KEY) ?: ""
        val couponId = data?.getStringExtra(IConstants.ActivitiesFlags.COUPON_ID) ?: ""

        val body = mutableMapOf<String, Any>()
        body["couponCode"] = coupon
        body["outletId"] = outletId.toString()
        body["cartId"] = sharedPrefsHelper.getCartId().toString()
        body["couponId"] = couponId
        vm.applyCoupon(body).observe(this, { result ->
            RHandler<CartResDTO>(
                this,
                result
            ) {
                CleverTapHelper.couponApplied(it)

                setBillingView(it)
                bd.couponLayoutView.appliedCode = coupon

                callOnResume = true
                showPlateView()
                AppUIController.showCouponAppliedUI(
                    it.billing?.couponDiscount?.withPrecision(2) ?: "SUCCESS"
                )
            }

        })
    }

    override fun onResponse(code: Int?, response: Any?) {
        callOnResume = true
        if (code == APIConstant.Code.COUPON_FAILD) {
            // AppUIController.showCouponFailedUI(resources.getString(R.string.invalid_coupon))
            showPlateView()
        } else if (code == APIConstant.Code.COUPON_NOT_APPLICABLE) {
            //   AppUIController.showCouponFailedUI(resources.getString(R.string.failed))
            showPlateView()

        } else
            showPlateView()
        super.onResponse(code, response)
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        when (code) {
            APIConstant.Status.NO_NETWORK -> {
                callOnResume = true
                hidePlateView()
                bd.cartNestedView.visibility = View.GONE
                bd.llPlateHeader.visibility = View.GONE
                bd.clFooter.visibility = View.GONE
                bd.icvEmptyPlate.visibility = View.VISIBLE
                bd.icvEmptyPlate.title = IConstants.OrderTracking.NO_INTERNET
                bd.icvEmptyPlate.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
                bd.icvEmptyPlate.imageResource = R.drawable.ic_internet
                bd.icvEmptyPlate.actionText = ""

            }
            APIConstant.Code.COUPON_FAILD -> {
                callOnResume = true
                showPlateView()
//                AppUIController.showCouponFailedUI(message)
                AlerterHelper.showToast(requireContext(), title ?: "Failed to apply", message)

            }
            APIConstant.Code.COUPON_NOT_APPLICABLE -> {
                callOnResume = true
                showPlateView()
                //  AppUIController.showCouponFailedUI(message)
                AlerterHelper.showToast(requireContext(), title ?: "Coupon not applicable", message)
            }
            else -> {
                showPlateView()
                callOnResume = true
                AlerterHelper.showToast(
                    requireContext(),
                    title ?: "Unable to process request",
                    message
                )
//                super.showCodeError(code, title, message)
            }
        }
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {

        if (actionType == IConstants.ActionModule.CONFIRM_DIALOG) {
            val cartId = sharedPrefsHelper.getCartId()
            val clearCart = cartId?.let { ClearCartReqDTO(it) }
            clearCart?.let {
                vm.clearCart(it).observe(this, { result ->
                    if (result.code == APIConstant.Status.SUCCESS) {
                        hidePlateView()
                        cartHelper.clearCart(cartId).observeForever {}
                    } else {
                        showPlateView()
                    }

                })
            }
        }
    }

    private fun showPlateView() {
        bd.icvEmptyPlate.visibility = View.GONE
        bd.cartNestedView.visibility = View.VISIBLE
        bd.llPlateHeader.visibility = View.VISIBLE
        bd.clFooter.visibility = View.VISIBLE
        bd.cartInfoLayout.cardTitle = RemoteConfigHelper.non_empty_cart_title
        bd.cartInfoLayout.cardSubtitle = RemoteConfigHelper.non_empty_cart_subtitle
    }

    private fun hidePlateAllView() {
        bd.icvEmptyPlate.visibility = View.GONE
        bd.cartNestedView.visibility = View.GONE
        bd.llPlateHeader.visibility = View.GONE
        bd.clFooter.visibility = View.GONE
    }

    private fun hidePlateView() {
        bd.icvEmptyPlate.visibility = View.VISIBLE
        bd.cartNestedView.visibility = View.GONE
        bd.llPlateHeader.visibility = View.GONE
        bd.clFooter.visibility = View.GONE
        bd.cartInfoLayout.cardTitle = "" //RemoteConfigHelper.empty_cart_title
        bd.cartInfoLayout.cardSubtitle = "" // RemoteConfigHelper.empty_cart_subtitle
    }

    private fun setDisablePayNow() {
        bd.btnPayNow.isEnabled = false
        bd.btnPayNow.background = ResourcesCompat.getDrawable(
            resources,
            R.drawable.button_pink_disable,
            context?.theme
        )
    }

    private fun setEnablePayNow() {
        bd.btnPayNow.isEnabled = true
        bd.btnPayNow.background = ResourcesCompat.getDrawable(
            resources,
            R.drawable.btn_active_red_drawable,
            context?.theme
        )
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }


    private fun animPaymentSuccess(message: String, earnedScratch: Boolean) {
        //  AppUIController.showOrderReceived(earnedScratch) {}
        cartHelper.clearAllSelected()
        if (context is HomePageActivity) {
            (context as HomePageActivity).navigateToFragment(R.id.nav_home, true)
        }
        ActionModuleHandler.showOrderTracking(
            requireContext(),
            message
        )


    }

    fun updateCart(address: AddressDTO?) {
        callOnResume = false
        fetchUserCart(address)
//        bd.tvErrorDec.text = address!!.fullAddress
//        bd.tvErrorMsg.text = "Deliver to " + address.addressTag.trim()
//        setIconTag(address.addressTag)


    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {
        if (actionType == IConstants.ActionModule.CLEAR_PLATE) {
            if (action != null && action.key == ActionConstant.POSITIVE) {

                cartHelper.mSavedCart.value?.let {
                    CleverTapHelper.clearCart(it)
                }
                cartHelper.cartRepository.clearCart(
                    sharedPrefsHelper.getCartId().toString()
                )
                    .observe(this@PlateFragment, { result ->
                        when (result.type) {
                            APIConstant.Status.SUCCESS -> {
                                hidePlateView()
                                sharedPrefsHelper.clearCartItemAddon()
                                cartHelper.clearCart()
                                cartHelper.mSavedCart.postValue(null)
                                bd.icvEmptyPlate.visibility = View.VISIBLE
                                bd.icvEmptyPlate.actionText =
                                    context?.resources?.getString(R.string.browse_restaurants)
                                bd.icvEmptyPlate.title =
                                    context?.resources?.getString(
                                        R.string.empty_plate_header
                                    )

                                bd.icvEmptyPlate.subTitle =
                                    context?.resources?.getString(R.string.add_smthing_from_menu)

                                bd.icvEmptyPlate.imageResource = R.drawable.ic_empty_plate

                            }
                            APIConstant.Status.ERROR -> {
                                showPlateView()
                                showSnackBar(
                                    result.message
                                        ?: IConstants.ResponseError.OOPS_SOMTHING_HAPPENED,
                                    1
                                )
                            }
                        }
                    })
            }

        }

    }

}