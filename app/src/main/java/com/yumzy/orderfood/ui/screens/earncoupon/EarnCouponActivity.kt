package com.yumzy.orderfood.ui.screens.earncoupon

import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CouponListResDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityEarnAndCouponsBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.clearLightStatusBar
import com.yumzy.orderfood.ui.screens.offer.CouponsAdapter
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class EarnCouponActivity :
    BaseActivity<ActivityEarnAndCouponsBinding, EarnCouponViewModel>(), IEarnCoupon {
    
    override val vm: EarnCouponViewModel by viewModel()

//    
//    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_earn_and_coupons)
        vm.attachView(this)


        setSupportActionBar(bd.tbEarncoupon)
        clearLightStatusBar(this, ThemeConstant.darkBlue)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val typeface = ResourcesCompat.getFont(this, R.font.poppins_bold)
        bd.collapsingToolbar.setCollapsedTitleTypeface(typeface)

        val flag: Int = intent.getIntExtra(IConstants.ActivitiesFlags.FLAG, 2)

        var isShow = true
        var scrollRange = -1
        bd.appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = barLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0) {
                if (flag == 0) {
                    bd.collapsingToolbar.title = "Earn Points"

                } else {
                    bd.collapsingToolbar.title = "Earn Coupons"

                }
                isShow = true
            } else if (isShow) {
                bd.collapsingToolbar.title =
                    " " //careful there should a space between double quote otherwise it wont work
                isShow = false
            }
        })



        if (flag == 0) {
            bd.ivSelEarncoupon.background = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_reward,
                theme
            )
            bd.tvHeader.text = "Total Point Earned"
            bd.tvSubHeader.text = "235"
            bd.tvSubSubHeader.text =
                "1 point is equivalent to ₹1, Which you can use during order checkouts."
            fetchEarnList()
        } else if (flag == 1) {

            bd.ivSelEarncoupon.background = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_commerce_and_shopping,
                theme
            )
            bd.tvHeader.text = "Total Coupons Earned"
            bd.tvSubHeader.text = "400"
            bd.tvSubSubHeader.text = "Use these coupons during your order checkout on my plate  "
            fetchCouponList()

        }

    }

    override fun fetchCouponList() {
        vm.getFetchCouponList(
            sharedPrefsHelper.getUserId().toString(),
            sharedPrefsHelper.getCartId(),
            sharedPrefsHelper.getOutletID()
        ).observe(
            this,
            { result ->
                RHandler<Any>(
                    this,
                    result
                ) {
                    result.data?.let { it1 ->
                        couponListResponse(it1)
                    }
                }

            })

    }

    private fun couponListResponse(data: List<CouponListResDTO>) {
        this.let {
            bd.rvCommonEarncoupon.adapter =
                CouponsAdapter(
                    it,
                    IConstants.ActivitiesFlags.FROM_HOME,
                    data,
                    clickedListener ={ couponCode: String, couponId: String ->  getSelCoupon(couponCode,couponId) })
            bd.rvCommonEarncoupon.layoutManager = LinearLayoutManager(it)

        }
    }

    override fun fetchEarnList() {
        this.let {
            bd.rvCommonEarncoupon.adapter =
                EarnListAdapter(it, clickedListener = { onPostSelected(it) })
            bd.rvCommonEarncoupon.layoutManager = LinearLayoutManager(it)

        }
    }

    private fun getSelCoupon(it: String,it1: String) {
//        val intent = Intent()
//        intent.putExtra(IConstants.ActivitiesFlags.RESULT_KEY, it)
//        setResult(Activity.RESULT_OK, intent)
//        finish()
    }

    private fun onPostSelected(position: Int) {
        showToast("clicked $position")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
