package com.yumzy.orderfood.ui.screens.login.fragment

import android.app.Dialog
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.birjuvachhani.locus.Locus
import com.birjuvachhani.locus.isFatal
import com.clevertap.android.geofence.CTGeofenceAPI
import com.clevertap.android.geofence.CTGeofenceSettings
import com.clevertap.android.geofence.Logger
import com.clevertap.android.geofence.interfaces.CTGeofenceEventsListener
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.appCleverTap
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.LongLat
import com.yumzy.orderfood.databinding.FragmentSetLocationBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil.ensureResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class SetLocationFragment : BaseFragment<FragmentSetLocationBinding, LoginViewModel>(), ILoginView {
    
    override val vm: LoginViewModel by viewModel()

    private var mOutletId = ""

    private lateinit var fetchingLocationDialog: ActionDialog

    private var mSelectedGoogleAddress: Address? = null

    var comeFrom = ""

    override fun getLayoutId() = R.layout.fragment_set_location

    private val navController by lazy {
        findNavController()
    }

    override fun onFragmentReady(view: View) {
        vm.attachView(this)


        bd.lottiSetLocation.setAnimation(R.raw.on_location)

        bd.btnAllowLocation.setOnClickListener {
            getCurrentLocation()
        }

        bd.btnSelectManually.setOnClickListener {
            navHomeActivity()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: SetLocationFragmentArgs by navArgs()
        comeFrom = args.from
        when (args.from) {
            IConstants.FragmentFlag.FROM_OTP_VERIFY -> {
                sharedPrefsHelper.saveIsTemporary(false)
            }
            IConstants.FragmentFlag.FROM_EXPLORE -> {
                sharedPrefsHelper.saveIsTemporary(true)

            }
        }

    }

    private fun getCurrentLocation() {
        showProgressBar()
        CTGeofenceAPI.getInstance(requireContext()).triggerLocation()
        Locus.getCurrentLocation(requireActivity()) { result ->
            result.location?.let {
                hideProgressBar()
                val parentJob = Job()
                CoroutineScope(Dispatchers.Main + parentJob).launch {
                    getAddress(it.latitude, it.longitude)
                }
            } ?: run {
                hideProgressBar()
                if(result.error?.isFatal == true) {
                    ensureResult(kotlinx.coroutines.Runnable {
                        getCurrentLocation()
                    }, 1500)
                }else if (result.error != null) {
                    CTGeofenceAPI.getInstance(requireContext())
                        .setCtLocationUpdatesListener {
                            getAddress(
                                it?.latitude ?: 17.4311184,
                                it?.longitude ?: 78.3722143
                            )
                        }
                } else {
                    AlerterHelper.showInfo(requireContext(), "Failed to fetch your location, Try Again.")
                }
            }
        }
    }

    private fun getAddress(lat: Double, lang: Double) {

        val gcd = Geocoder(requireContext(), Locale("en", "IN"))
        val addressList: List<Address>?

        try {
            addressList = gcd.getFromLocation(lat, lang, 2)
            if (addressList != null && addressList.isNotEmpty()) {

                mSelectedGoogleAddress =
                    if (addressList[0].locality != null && addressList[0].locality != "") {
                        addressList[0]
                    } else if (addressList[1].locality != null && addressList[1].locality != "") {
                        addressList[1]
                    } else {
                        ensureResult(kotlinx.coroutines.Runnable {
                            getAddress(lat, lang)
                        })
                        return
                    }

                mSelectedGoogleAddress?.latitude = lat
                mSelectedGoogleAddress?.longitude = lang

                val intentAddress = AddressDTO()
                mSelectedGoogleAddress?.let { gooAdd ->
                    intentAddress.run {
                        name = gooAdd.subLocality ?: (gooAdd.subAdminArea ?: gooAdd.locality)
                        googleLocation =
                            gooAdd.subLocality ?: (gooAdd.subAdminArea ?: gooAdd.locality)
                        city = gooAdd.locality
                        val lonLat = LongLat()
                        lonLat.coordinates = listOf(gooAdd.longitude, gooAdd.latitude)
                        longLat = lonLat
                        this.fullAddress =
                            gooAdd.getAddressLine(0) ?: getString(R.string.trying_to_get_location)
                        this.shortAddress = "${gooAdd.subAdminArea ?: gooAdd.locality}, ${gooAdd.locality ?: gooAdd.adminArea}"
                    }
                }

                AppGlobalObjects.selectedAddress = intentAddress
                sharedPrefsHelper.setSelectedAddress(intentAddress)

                if (this::fetchingLocationDialog.isInitialized) fetchingLocationDialog.dismiss()
                navHomeActivity()

            }
        } catch (e: Exception) {
            e.printStackTrace()
            ensureResult(kotlinx.coroutines.Runnable {
                getAddress(lat, lang)
            })
        }
    }


    private fun initCTGeofenceApi() {
        CTGeofenceAPI.getInstance(requireActivity())
            .init(
                CTGeofenceSettings.Builder()
                    .enableBackgroundLocationUpdates(true)
                    .setLogLevel(Logger.DEBUG)
                    .setLocationAccuracy(CTGeofenceSettings.ACCURACY_HIGH)
                    .setLocationFetchMode(CTGeofenceSettings.FETCH_CURRENT_LOCATION_PERIODIC)
                    .setGeofenceMonitoringCount(99)
                    .setInterval(3600000) // 1 hour
                    .setFastestInterval(1800000) // 30 minutes
                    .setSmallestDisplacement(1000f) // 1 km
                    .build(), appCleverTap
            )


        try {
            CTGeofenceAPI.getInstance(requireActivity()).triggerLocation()
        } catch (e: IllegalStateException) {
            // thrown when this method is called before geofence SDK initialization
        }
        CTGeofenceAPI.getInstance(requireActivity())
            .setOnGeofenceApiInitializedListener {
                /*showToast(
                    "Geofence API initialized",
                )*/
            }
        CTGeofenceAPI.getInstance(requireActivity())
            .setCtGeofenceEventsListener(object : CTGeofenceEventsListener {
                override fun onGeofenceEnteredEvent(jsonObject: JSONObject) {
//                    showToast("Geofence Entered")
                }

                override fun onGeofenceExitedEvent(jsonObject: JSONObject) {
//                    showToast("Geofence Exited")
                }
            })

        CTGeofenceAPI.getInstance(requireActivity())
            .setCtLocationUpdatesListener {
//                CleverTapHelper.pushStartedLocation(this, it.latitude, it.longitude)
//                showToast("Location updated")
//                CleverTapHelper.pushStartedLocation(this, it.latitude, it.longitude)
//                showToast("Location updated")
            }

    }

    override fun navHomeActivity() {
        navController.navigate(R.id.fragment_set_location_to_fetch_location)
        


//        val intent = Intent(requireActivity(), HomePageActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
//        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, false)
//        intent.putExtra(
//            YumzyFirebaseMessaging.NOTIFICATION_DESTINATION,
//            StartedActivity.mDestination
//        )
//        intent.putExtra(YumzyFirebaseMessaging.NOTIFICATION_URL, StartedActivity.mDestinationUrl)
//        if (mOutletId.isNotBlank()) {
//            intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, mOutletId)
//        }
//        startActivity(intent)
//        Handler(requireActivity().mainLooper).postDelayed({
//            requireActivity().finish()
//        }, 1500)


    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }

}