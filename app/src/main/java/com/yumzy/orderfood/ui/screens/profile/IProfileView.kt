package com.yumzy.orderfood.ui.screens.profile

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IProfileView : IView {
    fun signOutUser()
}