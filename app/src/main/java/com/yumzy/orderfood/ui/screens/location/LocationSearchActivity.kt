package com.yumzy.orderfood.ui.screens.location

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.birjuvachhani.locus.Locus
import com.birjuvachhani.locus.isFatal
import com.clevertap.android.geofence.CTGeofenceAPI
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.*
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericFastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.LongLat
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityLocationSearchBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindignRestaurantSectionItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingAddressListItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingSelectFromGPSItem
import com.yumzy.orderfood.util.YUtils.toBounds
import com.yumzy.orderfood.util.viewutils.YumUtil
import io.cabriole.decorator.DecorationLookup
import io.cabriole.decorator.LinearDividerDecoration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject


class LocationSearchActivity : BaseActivity<ActivityLocationSearchBinding, LocationViewModel>(),
    ILocationView, ItemFilterListener<GenericItem> {


    lateinit var mPlacesClient: PlacesClient

    private lateinit var mFastAdapter: GenericFastAdapter
    private lateinit var mItemLocationGPSAdapter: GenericItemAdapter
    private lateinit var mItemAddressAdapter: GenericItemAdapter
    private lateinit var mItemEStablishmentAdapter: GenericItemAdapter
    private lateinit var mItemGeocodeAdapter: GenericItemAdapter
    private lateinit var mItemRegionsAdapter: GenericItemAdapter
    private lateinit var mItemSavedAddressAdapter: GenericItemAdapter

    private lateinit var token: AutocompleteSessionToken

    // Create a RectangularBounds object.
    //It covers Hyderabad, Visakhapatnam, Bangalore and Chennai.
    private val bounds = RectangularBounds.newInstance(
        LatLng(12.490628, 76.883107),
        LatLng(19.076439, 84.403240)
    )

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true

    override val vm: LocationViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_location_search)
        vm.attachView(this)


        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)

        setUpGooglePlacesClient()
        setRecyclerView()
        setUpSearchView()
        setUpAddressFilter()

        if (!sharedPrefsHelper.getIsTemporary()) getSavedAddresses()
    }

    private fun setUpGooglePlacesClient() {

        // Initialize the SDK
        Places.initialize(applicationContext, BuildConfig.MAPS_API_KEY)

        // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
        // and once again when the user makes a selection (for example when calling fetchPlace()).
        token = AutocompleteSessionToken.newInstance()

        // Create a new PlacesClient instance
        mPlacesClient = Places.createClient(this)

    }

    private fun setRecyclerView() {

        mItemAddressAdapter = ItemAdapter.items()
        mItemLocationGPSAdapter = ItemAdapter.items()
        mItemEStablishmentAdapter = ItemAdapter.items()
        mItemGeocodeAdapter = ItemAdapter.items()
        mItemRegionsAdapter = ItemAdapter.items()
        mItemSavedAddressAdapter = ItemAdapter.items()

        val adapters: Collection<ItemAdapter<out GenericItem>> =
            listOf(
                mItemLocationGPSAdapter,
                mItemSavedAddressAdapter,
                mItemAddressAdapter,
                mItemEStablishmentAdapter,
                mItemGeocodeAdapter,
                mItemRegionsAdapter,
            )

        mFastAdapter = FastAdapter.with(adapters)

        mFastAdapter.onClickListener =
            { v: View?, adapter: IAdapter<GenericItem>, item: GenericItem, position: Int ->
                when (item) {
                    is BindingAddressListItem -> {
                        if (item.addessDto == null) {
                            item.predictionAddress?.placeId?.let { getLocationDetailFromPlaceId(it) }
                        } else {
                            sharedPrefsHelper.setSelectedAddress(item.addessDto)
                            AppGlobalObjects.selectedAddress = item.addessDto
                            this.finish()
                        }
                        YumUtil.hideKeyboard(this)
                        true
                    }
                    is BindingSelectFromGPSItem -> {
                        getCurrentLocation()
                        YumUtil.hideKeyboard(this)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }

        mItemLocationGPSAdapter.add(arrayListOf(BindingSelectFromGPSItem()))

        bd.rv.layoutManager = LinearLayoutManager(this)
        bd.rv.adapter = mFastAdapter
        recyclerAddItemDecoration()

    }

    private fun recyclerAddItemDecoration() {
        val decoration = LinearDividerDecoration.create(
            color = ThemeConstant.divider,
            size = 1.px,
            leftMargin = 55.px,
            topMargin = 15.px,
            rightMargin = 5.px,
            bottomMargin = 15.px,
            orientation = RecyclerView.VERTICAL
        )
        decoration.setDecorationLookup(object : DecorationLookup {
            override fun shouldApplyDecoration(position: Int, itemCount: Int): Boolean {
                when (mFastAdapter.getItem(position)) {
                    is BindignRestaurantSectionItem -> {
                        return false
                    }
                    is BindingSelectFromGPSItem -> {
                        return false
                    }

                }
                return true
            }

        })
        bd.rv.addItemDecoration(decoration)

    }

    private fun setUpSearchView() {
        val revealCenter: Point = bd.searchView.revealAnimationCenter
        revealCenter.x -= VUtil.dpToPx(40)
        bd.searchView.showSearch(false)
        bd.searchView.hideCressIcon(false)
        bd.searchView.setOnQueryTextListener(object : OutletSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mItemSavedAddressAdapter.filter(newText)
                getPredictionLocations(newText ?: "Hyderabad")
                return true
            }

            override fun onQueryTextCleared(): Boolean {
                return true
            }
        })
    }

    private fun setUpAddressFilter() {

        mItemSavedAddressAdapter.itemFilter.itemFilterListener = this

        mItemSavedAddressAdapter.itemFilter.filterPredicate = { item, constraint ->
            when (item) {
                is BindingAddressListItem -> {
                    if (item.addessDto != null) {
                        item.addessDto?.name?.toLowerCase(Locale.getDefault())?.contains(
                            constraint.toString().toLowerCase(Locale.getDefault())
                        ) == true ||
                                item.addessDto?.fullAddress?.toLowerCase(Locale.getDefault())
                                    ?.contains(
                                        constraint.toString().toLowerCase(Locale.getDefault())
                                    ) == true
                    } else {
                        false
                    }
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun getSavedAddresses() {
        val items = arrayListOf<GenericItem>()

        vm.getAddressList().observe(this, { response ->
            RHandler<List<AddressDTO>>(this, response) {
                if (it.isEmpty()) {

                    AppGlobalObjects.selectedAddress?.let {

                        val sectionHeaderBinding = BindignRestaurantSectionItem().withSection(
                            "Recent Address",
                            0
                        )

                        items.add(sectionHeaderBinding)
                        items.add(BindingAddressListItem().withAddressDTO(it))

                    }

                } else {

                    val sectionHeaderBinding = BindignRestaurantSectionItem().withSection(
                        "Select from saved Addresses",
                        0
                    )
                    items.add(sectionHeaderBinding)

                    it.forEach {
                        items.add(BindingAddressListItem().withAddressDTO(it))
                    }

                }

                mItemSavedAddressAdapter.clear()
                mItemSavedAddressAdapter.add(items)
            }
        })
    }

    fun getPredictionLocations(query: String) {

        // Use the builder to create a FindAutocompletePredictionsRequest.
        val addressRequest = createAutoCompleteRequest(
            bounds,
            query,
            TypeFilter.ADDRESS,
            getUserOrigin()
        )
        val establishmentRequest =
            createAutoCompleteRequest(bounds, query, TypeFilter.ESTABLISHMENT, getUserOrigin())
        val geocodeRequest =
            createAutoCompleteRequest(bounds, query, TypeFilter.GEOCODE, getUserOrigin())
        val regionsRequest =
            createAutoCompleteRequest(bounds, query, TypeFilter.REGIONS, getUserOrigin())

        //reqeuset autocomplete based on request
        requestAcutoCompletePrediction(addressRequest, mItemAddressAdapter)
        requestAcutoCompletePrediction(establishmentRequest, mItemEStablishmentAdapter)
        requestAcutoCompletePrediction(geocodeRequest, mItemGeocodeAdapter)
        requestAcutoCompletePrediction(regionsRequest, mItemRegionsAdapter)

    }

    private fun getUserOrigin(): LatLng {
        return sharedPrefsHelper.getSelectedAddress()?.let {
            return@let LatLng(
                it.longLat?.coordinates?.get(1) ?: 17.420317,
                it.longLat?.coordinates?.get(0) ?: 78.449036
            )
        } ?: LatLng(17.420317, 78.449036)
    }

    private fun createAutoCompleteRequest(
        bounds: RectangularBounds,
        query: String,
        typeFilter: TypeFilter,
        origin: LatLng
    ): FindAutocompletePredictionsRequest {
        return FindAutocompletePredictionsRequest.builder()
            .setLocationBias(
                if (sharedPrefsHelper.getSelectedAddress() == null) bounds else origin.toBounds(
                    50000.toDouble()
                )
            )
            .setOrigin(origin)
            .setCountries("IN")
            .setTypeFilter(typeFilter)
            .setSessionToken(token)
            .setQuery(query)
            .build()
    }


    private fun requestAcutoCompletePrediction(
        addressRequest: FindAutocompletePredictionsRequest,
        adapter: GenericItemAdapter
    ) {
        mPlacesClient.findAutocompletePredictions(addressRequest)
            .addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                val items = arrayListOf<GenericItem>()
                for (prediction in response.autocompletePredictions) {
                    items.add(BindingAddressListItem().withAddress(prediction))
                }
                adapter.clear()
                adapter.add(items)
                bd.rv.post { bd.rv.scrollToPosition(0) }
            }.addOnFailureListener { exception: Exception? ->
                if (exception is ApiException) {
                }
            }
    }


    private fun getLocationDetailFromPlaceId(placeId: String) {

        // Specify the fields to return.
        val placeFields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)

        // Construct a request object, passing the place ID and fields array.
        val request = FetchPlaceRequest.newInstance(placeId, placeFields)

        mPlacesClient.fetchPlace(request)
            .addOnSuccessListener { response: FetchPlaceResponse ->
                val place = response.place
                openMap(Location("User selected location").run {
                    latitude = place.latLng?.latitude ?: 17.420317
                    longitude = place.latLng?.longitude ?: 78.449036
                    this
                })
            }.addOnFailureListener { exception: Exception ->
                if (exception is ApiException) {
                    val statusCode = exception.statusCode
                }
            }

    }

    private fun getCurrentLocation() {
        showProgressBar()
        CTGeofenceAPI.getInstance(this).triggerLocation()
        Locus.getCurrentLocation(this) { result ->
            result.location?.let {
                hideProgressBar()
                val parentJob = Job()
                CoroutineScope(Dispatchers.Main + parentJob).launch {
                    openLocationPickerActivity(it.latitude, it.longitude)
                }
            } ?: run {
                hideProgressBar()
                if (result.error?.isFatal == true) {
                    YumUtil.ensureResult(kotlinx.coroutines.Runnable {
                        getCurrentLocation()
                    }, 1500)
                } else if (result.error != null) {
                    CTGeofenceAPI.getInstance(applicationContext)
                        .setCtLocationUpdatesListener {
                            openLocationPickerActivity(
                                it?.latitude ?: 17.4311184,
                                it?.longitude ?: 78.3722143
                            )
                        }
                } else {
                    AlerterHelper.showInfo(this, "Failed to fetch your location, Try Again.")
                }


            }
        }
    }

    /*fun enablegps() {

        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(2000)
            .setFastestInterval(1000)

        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)

        val result =
            LocationServices.getSettingsClient(this).checkLocationSettings(settingsBuilder.build())
        result.addOnCompleteListener { task ->

            //getting the status code from exception
            try {
                task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {

                when (ex.statusCode) {

                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {

                        Toast.makeText(this, "GPS IS OFF", Toast.LENGTH_SHORT).show()

                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        val resolvableApiException = ex as ResolvableApiException
                        resolvableApiException.startResolutionForResult(
                            this, REQUEST_CHECK_SETTINGS
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Toast.makeText(
                            this,
                            "PendingIntent unable to execute request.",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {

                        Toast.makeText(
                            this,
                            "Something is wrong in your GPS",
                            Toast.LENGTH_SHORT
                        ).show()

                    }


                }
            }


        }


    }*/

    private fun openMap(location: Location) {
        sharedPrefsHelper.saveLastLocation(location)
        openLocationPickerActivity(location.latitude, location.longitude)
    }

    private fun openLocationPickerActivity(lat: Double, lng: Double) {

        YumUtil.hideKeyboard(this)
        val address = AddressDTO().apply {
            val lonLat = LongLat()
            lonLat.coordinates = listOf(lng, lat)
            longLat = lonLat
        }

        val intent = Intent(this, LocationPickrActivity::class.java)
        intent.putExtra("lat", lat)
        intent.putExtra("lng", lng)
        intent.putExtra("address", address)
//        AppIntentUtil.launchClipRevealFromIntent(bd.searchView, this, intent)
        AppIntentUtil.launchForResultWithSheetAnimation(this, intent) { resultOk, result ->
//            val addressData = result.data?.extras?.get("pickedAddress")
            if (resultOk) {
//                val resultData = Intent()
//                resultData.putExtra("pickedAddress", addressData)
//                resultData.putExtra("show_home", true)
                setResult(Activity.RESULT_OK, result.data)
                finish()
            }

        }
//        startActivity(intent)


    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun itemsFiltered(constraint: CharSequence?, results: List<GenericItem>?) {}

    override fun onReset() {}

}