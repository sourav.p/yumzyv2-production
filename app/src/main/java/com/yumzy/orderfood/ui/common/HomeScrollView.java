package com.yumzy.orderfood.ui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class HomeScrollView extends ScrollView {

    float touchX = 0;
    float touchY = 0;


    public HomeScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeScrollView(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                touchX = ev.getX();
                touchY = ev.getY();
                return super.onTouchEvent(ev);
            case MotionEvent.ACTION_MOVE:


                if (Math.abs(touchX - ev.getX()) < 40) {
                    return super.onTouchEvent(ev);
                } else {

                }
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                touchX = 0;
                touchY = 0;
                break;
        }
        return super.onTouchEvent(ev);
    }
}
