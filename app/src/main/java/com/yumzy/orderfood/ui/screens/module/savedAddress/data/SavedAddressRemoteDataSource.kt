package com.yumzy.orderfood.ui.screens.module.savedAddress.data

import com.yumzy.orderfood.api.BaseDataSource
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class SavedAddressRemoteDataSource  constructor() : BaseDataSource()