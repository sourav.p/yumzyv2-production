package com.yumzy.orderfood.ui.screens.scratchcard.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ScratchCardAPI
import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.data.models.ScratchCardDetailDTO
import com.yumzy.orderfood.data.models.base.ResponseDTO
import javax.inject.Inject

class ScratchDataSource  constructor(private val cardAPI: ScratchCardAPI) :
    BaseDataSource() {


    suspend fun fetchScratchCardList(skip: Int, limit: Int): ResponseDTO<List<ScratchCardDTO>> =
        getResult { cardAPI.fetchScratchCardList(skip, limit) }

    suspend fun getScratchDetails(): ResponseDTO<ScratchCardDetailDTO> =
        getResult { cardAPI.getScratchCardDetails() }

    suspend fun redeemScratchCard(scratchCard: ScratchCardDTO, phone: String?) =
        getResult { cardAPI.redeemScratchCard(scratchCard.scratchID, phone) }

}