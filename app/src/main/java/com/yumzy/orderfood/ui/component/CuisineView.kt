package com.yumzy.orderfood.ui.component

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.TextViewCompat
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.util.IConstants

/*
 * Created By Shriom Tripathi 1 - 5 - 2020
 */

class CuisineView : ConstraintLayout,
    GroupItemActions<CuisineView>, View.OnClickListener {

    //initial sate must be initalized
    override var mIsSelected: Boolean = false
        set(value) {
            if (value) {
                setSelectedBackground()
            } else {
                setDeselectedBackground()
            }
            field = value
        }

    private lateinit var mOnGroupItemClickListener: OnGroupItemClickListener<CuisineView>

    private var mCuisineType = IConstants.CuisineModule.CUISINE_TYPE__ANY

    private val i100px = VUtil.dpToPx(100)
    private val i90px = VUtil.dpToPx(90)
    private val i60px = VUtil.dpToPx(60)
    private val i50px = VUtil.dpToPx(50)
    private val i24px = VUtil.dpToPx(24)
    private val i12px = VUtil.dpToPx(12)
    private val i8px = VUtil.dpToPx(8)
    private val i4px = VUtil.dpToPx(4)

    private var svgImage: String = ""
        set(value) {
            val imageView = this.findViewById<ImageView>(IConstants.CuisineModule.CUISINE_IMAGE)
//            val drawable = ColorDrawable(value)
            field = value
            GlideToVectorYou.justLoadImage(context as Activity?, Uri.parse(value), imageView)

        }

    private var mPrefId: String = ""
        set(value) {
            val textView = this.findViewById<MaterialTextView>(IConstants.CuisineModule.CUISINE_ID)
            textView.text = value.split(" ").joinToString("\n")
            field = value
        }
    private var mTitle: String = ""
        set(value) {
            val textView =
                this.findViewById<MaterialTextView>(IConstants.CuisineModule.CUISINE_TITLE)
            textView.text = value.split(" ").joinToString("\n")
            field = value
        }


    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)

    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        this.apply {
            id = IConstants.CuisineModule.CUISINE_VIEW
            layoutParams = LinearLayout.LayoutParams(i90px, i60px.times(2))
            setPadding(i8px, i8px, i8px, i8px)
//            setMargin(i12px)
            setOnClickListener(this)

        }
        val drawable = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.cuisine_selected_drawable,
            context.theme
        )
        this.background = drawable
        initViews()
    }


    private fun initViews() {
        this.addView(getImage())
        this.addView(getTitleTextView())
        addView(getIdTextView())
        mTitle = mTitle
        svgImage = svgImage
        svgImage = svgImage
        mPrefId = mPrefId
        mIsSelected = mIsSelected
    }

    private fun getImage(): ImageView? {

        val imageView = ImageView(context)
        imageView.id = IConstants.CuisineModule.CUISINE_IMAGE

        val layoutParams = LayoutParams(i50px, i50px)
        layoutParams.setMargins(0, i8px, 0, 0)
        layoutParams.startToStart = LayoutParams.PARENT_ID
        layoutParams.topToTop = LayoutParams.PARENT_ID
        layoutParams.endToEnd = LayoutParams.PARENT_ID
//        layoutParams.dimensionRatio = "1:1"

        imageView.layoutParams = layoutParams


        imageView.scaleType = ImageView.ScaleType.FIT_CENTER

        return imageView

    }


    private fun getTitleTextView(): MaterialTextView? {

        val titleTextView = MaterialTextView(context)
        titleTextView.id = IConstants.CuisineModule.CUISINE_TITLE


        val layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(i8px, i8px, i8px, i8px)
        layoutParams.topToBottom = IConstants.CuisineModule.CUISINE_IMAGE
        layoutParams.startToStart = IConstants.CuisineModule.CUISINE_IMAGE
        layoutParams.endToEnd = IConstants.CuisineModule.CUISINE_IMAGE

        titleTextView.layoutParams = layoutParams
        titleTextView.ellipsize = TextUtils.TruncateAt.END
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            titleTextView.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
        } else {
            titleTextView.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        }
        titleTextView.gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            titleTextView.setAutoSizeTextTypeUniformWithConfiguration(
//                6, 18, 1, TypedValue.COMPLEX_UNIT_DIP
//            )
//        } else
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            titleTextView, 8, 12, 1,
            TypedValue.COMPLEX_UNIT_SP
        )
        titleTextView.maxLines = 2
        titleTextView.minLines = 2

        return titleTextView

    }

    private fun getIdTextView(): MaterialTextView? {

        val titleTextView = MaterialTextView(context)
        titleTextView.id = IConstants.CuisineModule.CUISINE_ID
        titleTextView.visibility = View.GONE
        return titleTextView

    }


    fun setCuisine(svgImg: String, title: String, prefId: String) {
        svgImage = svgImg
        mTitle = title
        mPrefId = prefId
    }

    override fun onClick(v: View?) {
        mOnGroupItemClickListener.onGroupItemClicked(this)
    }

    override fun setOnItemClickListener(value: OnGroupItemClickListener<CuisineView>) {
        mOnGroupItemClickListener = value
    }

    override fun setSelectedBackground() {
        // ShadowUtils.apply(this, ShadowUtils.Config().setShadowRadius(15f).setShadowColor(R.color.cool_gray))
        this.isSelected = true

        /*
              this.background = DrawableUtils.getRoundStrokeDrawableListState(
                  ThemeConstant.lightGray,
                  15.pxf,
                  ThemeConstant.lightGray,
                  15.pxf,
                  ThemeConstant.pinkies, 2
              )*/
    }

    override fun setDeselectedBackground() {
        this.isSelected = false

        //ShadowUtils.apply(this, ShadowUtils.Config().setShadowRadius(15f).setShadowColor(R.color.cool_gray))
        /* this.background = DrawableUtils.getRoundStrokeDrawableListState(
             ThemeConstant.lightGray,
             15.pxf,
             ThemeConstant.lightGray,
             15.pxf,
             ThemeConstant.lightGray, 0
         )*/

    }

    override fun compareTo(other: GroupItemActions<*>): Int {
        val condition =
            this.findViewById<MaterialTextView>(IConstants.CuisineModule.CUISINE_TITLE).text == (other as CuisineView).findViewById<MaterialTextView>(
                IConstants.CuisineModule.CUISINE_TITLE
            ).text

        return if (condition) {
            1
        } else {
            0
        }
    }


}