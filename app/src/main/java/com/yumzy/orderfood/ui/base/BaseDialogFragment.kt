package com.yumzy.orderfood.ui.base

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.ui.CodeColor
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.templetes.InfoTemplate
import com.yumzy.orderfood.ui.screens.templetes.NoInternetTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import timber.log.Timber

abstract class BaseDialogFragment<B : ViewDataBinding> : DialogFragment(), IModuleHandler {

    var moduleId: Int = 0
    open var actionHandler: IModuleHandler? = null

    open lateinit var bd: B
    private lateinit var dialogView: ViewGroup

    var handler: Handler? = null
    var update: Runnable? = null

    private lateinit var mProgressBar: ProgressBar
    private lateinit var frameLayout: FrameLayout

    private val mClickListener =
        View.OnClickListener { v -> onDebounceClick(v) }

    override fun onCreate(savedInstanceState: Bundle?) {
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialogStyle)
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bd = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        dialogView = bd.root as ViewGroup
        dialogView.addView(initFrame())
        return bd.root
    }

    private fun initFrame(): FrameLayout {
        frameLayout = FrameLayout(requireContext())
        val frameParam = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        frameLayout.layoutParams = frameParam
        frameLayout.background = null
        frameLayout.id = R.id.base_framelayout
        mProgressBar = ProgressBar(requireContext())
        mProgressBar.background = null
        mProgressBar.visibility = View.GONE
        val pbParam: FrameLayout.LayoutParams = FrameLayout.LayoutParams(45.px, 45.px)
        pbParam.gravity = Gravity.CENTER
        mProgressBar.layoutParams = pbParam
        mProgressBar.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        frameLayout.addView(mProgressBar)
        return frameLayout

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onDialogReady(view)
        YumUtil.hideDialogKeyboard(dialog!!)

    }

    fun applyDebouchingClickListener(vararg views: View?) {
        ClickUtils.applySingleDebouncing(views, mClickListener)
        ClickUtils.applyPressedViewScale(*views)
    }

    protected fun hideSoftKeyboard(activity: Activity) {
        activity.window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
    }

    private fun showProgressBar(visibility: Boolean) {
        mProgressBar.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
        if (visibility) frameLayout.setBackgroundColor(0x3CFFFFFF) else frameLayout.background =
            null
        frameLayout.isClickable = visibility
    }

    override fun hideProgressBar() {
        showProgressBar(false)
    }

    override fun showProgressBar() {
        showProgressBar(true)

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        val codeColor = CodeColor.COLOR_CODE.getCodeColor(code)
        if (codeColor == null) {
            showToast(message)
            return
        }
        when (code) {
            APIConstant.Status.NO_NETWORK -> {
                val dialog = DetailActionDialog()
                dialog.detailsTemplate = NoInternetTemplate(
                    requireContext(),
                    resources.getString(R.string.icon_010_broccoli),
                    IConstants.ResponseError.NO_INTERNET,
                    message
                )
                dialog.show((context as AppCompatActivity).supportFragmentManager, "$code")
            }
            APIConstant.Status.SESSION_EXPIRED -> {
                val dialog = DetailActionDialog()
                dialog.detailsTemplate = InfoTemplate(
                    requireContext(),
                    R.string.icon_chili_pepper,
                    getString(R.string.please_login_again),
                    getString(R.string.your_session_expired)
                )

                dialog.show((context as AppCompatActivity).supportFragmentManager, "$code")
            }
            else -> {
                showSnackBar(message, Color.RED)
            }
        }


    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.commitAllowingStateLoss()
        } catch (e: IllegalStateException) {
            Timber.d("Exception: ${e}")
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    }

    override fun onResponse(code: Int?, response: Any?) {
    }

    open fun showSnackBar(message: String, statusColor: Int = Color.BLACK) {
        val snackBar = Snackbar.make(dialogView, message, Snackbar.LENGTH_SHORT)
        val viewGroup = snackBar.view as ViewGroup
        viewGroup.minimumHeight = VUtil.dpToPx(0)
        viewGroup.getChildAt(0).background = null
        viewGroup.background = ColorDrawable(Color.BLACK)
        val textView = (viewGroup.getChildAt(0) as ViewGroup).getChildAt(0) as TextView
        textView.background = null
        textView.setTextColor(ThemeConstant.white)
        snackBar.setActionTextColor(Color.WHITE)
//        ViewCompat.setOnApplyWindowInsetsListener(snackBar.view, null)
        snackBar.show()
    }

    open fun onDebounceClick(view: View) {
    }


    abstract fun getLayoutId(): Int

    abstract fun onDialogReady(view: View)

}