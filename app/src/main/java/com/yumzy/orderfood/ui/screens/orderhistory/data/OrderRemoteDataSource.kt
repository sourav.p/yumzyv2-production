package com.yumzy.orderfood.ui.screens.orderhistory.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.OrderServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class OrderRemoteDataSource  constructor(private val serviceAPI: OrderServiceAPI) :
    BaseDataSource() {
    suspend fun getOrderHistoryList(skip: Int) = getResult {
        serviceAPI.getOrderHistoryList(skip)
    }

    suspend fun getOrderStatus(orderId: String) = getResult {
        serviceAPI.getOrderStatus(orderId)
    }

    suspend fun orderDetails(orderId: String) = getResult {
        serviceAPI.orderDetails(orderId)

    }

    suspend fun orderTrackDetails(orderId: String) = getResult {
        serviceAPI.orderDetails(orderId)

    }


}