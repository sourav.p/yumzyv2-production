package com.yumzy.orderfood.ui.screens.information

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.YumLottieView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.infodialog.ILottieTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


class LottieSizedDialog : DialogFragment(),
    YumLottieView.OnLottieAnimationListener, DialogInterface.OnShowListener {
    //    lateinit var imageUrl: String
//    var width: Int = ViewConst.MATCH_PARENT
//    var height: Int = ViewConst.WRAP_CONTENT
    var lottieTemplate: ILottieTemplate? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (lottieTemplate == null) {
            lottieTemplate = BaseAnimationTemplate(
                requireContext(),
                "https://assets8.lottiefiles.com/temp/lf20_nXwOJj.json"
            )
        }

        val clDialog = Dialog(requireContext(), R.style.DialogTheme)
        clDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        clDialog.setContentView(getLayout())


        if (clDialog.window != null) {
            clDialog.window?.setWindowAnimations(R.style.DialogZoomInZoomOut)
            clDialog.window?.setBackgroundDrawableResource(R.drawable.white_rounded_corner)
            clDialog.setOnShowListener(this)
        }


        return clDialog
    }


    private fun getLayout(): View {

        val container = ConstraintLayout(requireContext())
        container.layoutParams =
            ViewGroup.LayoutParams(
                UIHelper.i200px,
                UIHelper.i200px
            )

        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lottieTemplate?.body()?.let {
            if (!lottieTemplate?.showClose!!) {
                it.onLottieAnimationListener = this

            }
            layout.addView(
                it,
                LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 0, 1f)
            )
        }
        if (lottieTemplate?.titleText != null)
            lottieTemplate?.heading()?.let { layout.addView(it) }

        if (lottieTemplate?.footerText != null)
            lottieTemplate?.footer()?.let { layout.addView(it) }

//        layout.setPadding(padLR, padTB, padLR, padTB)
//        layout.background = VUtil.getRoundDrawable(dialogColor, roundRadius)
        container.addView(layout)
        lottieTemplate?.showClose?.let {
            if (it)
                addCloseButton(container)
        }
        layout.setPadding(UIHelper.i10px, UIHelper.i45px, UIHelper.i10px, UIHelper.i10px)

        return container
    }

    private fun addCloseButton(container: ConstraintLayout) {
        val closeIcon = FontIconView(requireContext())
        val closeParam = ConstraintLayout.LayoutParams(VUtil.dpToPx(30), VUtil.dpToPx(30))
        closeIcon.setTextColor(Color.GRAY)
        closeParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        closeParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        closeParam.marginEnd = VUtil.dpToPx(8)
        closeParam.topMargin = VUtil.dpToPx(8)
        closeIcon.layoutParams = closeParam
        closeIcon.setText(R.string.icon_close)
        closeIcon.setOnClickListener {
            dialog?.dismiss()
        }
        container.addView(closeIcon)
    }

//    var padTB = VUtil.dpToPx(15)
//        set(value) {
//            field = VUtil.dpToPx(value)
//        }
//    var padLR = VUtil.dpToPx(15)
//        set(value) {
//            field = VUtil.dpToPx(value)
//        }

    //    var roundRadius: Float = VUtil.dpToPxFloat(15f)
//        set(value) {
//            field = VUtil.dpToPxFloat(value)
//        }
    override fun onShow(dialog: DialogInterface?) {
        lottieTemplate?.onDialogVisible(this.view, this.isVisible, dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        lottieTemplate?.onDialogVisible(this.view, this.isVisible, dialog)
    }

    override fun onAnimationEnd() {
        this.dismiss()
    }

}
