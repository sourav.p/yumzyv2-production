package com.yumzy.orderfood.ui.common

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterRegularFont
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

class ConfirmActionDialog : DialogFragment, DialogInterface.OnShowListener {
    lateinit var clContext: Context
    private var actionType = -1
    private var actionLabel: String = ""
    private var sTitle: CharSequence? = ""
    private var sMessage: CharSequence = ""
    var onActionPerformed: OnActionPerformed? = null

    constructor(
        clContext: Context,
        actionType: Int,
        actionLabel: String,
        sTitle: CharSequence,
        sMessage: CharSequence,
        onActionPerformed: OnActionPerformed?
    ) {
        this.onActionPerformed = onActionPerformed
        this.sTitle = sTitle
        this.sMessage = sMessage
        this.clContext = clContext
        this.actionType = actionType
        this.actionLabel = actionLabel
    }

    constructor()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        super.onCreateDialog(savedInstanceState)

        val clDialog = Dialog(requireContext(), R.style.DialogTheme)
        clDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        clDialog.setContentView(getDialogLayout())


        if (clDialog.window != null) {
            clDialog.window?.setWindowAnimations(R.style.DialogZoomInZoomOut)
            clDialog.window?.setBackgroundDrawableResource(R.drawable.white_rounded_corner)
            clDialog.setOnShowListener(this)
        }

        return clDialog
    }

    private fun getDialogLayout(): View {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setPadding(UIHelper.i20px, UIHelper.i20px, UIHelper.i20px, UIHelper.i20px)
        linearLayout.addView(getIconTitle())
        linearLayout.addView(getSubTitle())
        linearLayout.addView(getActionLayout())

        return linearLayout


    }

    private fun getActionLayout(): LinearLayout {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL or Gravity.END
        val btnOk = UIHelper.filledButton(clContext, "OK", null)
        btnOk.minWidth = VUtil.dpToPx(75)
        btnOk.setInterBoldFont()
        btnOk.setOnClickListener {
            onActionPerformed?.onConfirmDone(this.dialog, actionType, actionLabel)
            dialog?.dismiss()
        }

        val btnClose = UIHelper.outlineButton(clContext, "CLOSE", null)
        btnClose.minWidth = VUtil.dpToPx(75)
        btnClose.setInterBoldFont()
        btnClose.setOnClickListener {
            onActionPerformed?.onDismissed(actionType, actionLabel)
            dialog?.dismiss()
        }

        val spaceView = View(clContext)
        spaceView.layoutParams = LinearLayout.LayoutParams(UIHelper.i20px, 0)

        linearLayout.addView(btnClose)
        linearLayout.addView(spaceView)
        linearLayout.addView(btnOk)
        return linearLayout

    }

    private fun getIconTitle(): LinearLayout {
        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        linearLayout.addView(
            getFontIconView(),
            LinearLayout.LayoutParams(VUtil.dpToPx(28), VUtil.dpToPx(28))
        )
        linearLayout.addView(
            getTitle(),
            LinearLayout.LayoutParams(0, ViewConst.WRAP_CONTENT, 1f)
        )
        return linearLayout

    }

    private fun getSubTitle(): TextView {
        val textView = TextView(clContext)
        textView.text = sMessage
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        textView.setTextColor(ThemeConstant.textGrayColor)
        textView.setInterRegularFont()

        textView.setPadding(UIHelper.i10px, UIHelper.i10px, UIHelper.i10px, UIHelper.i30px)
        return textView
    }

    private fun getTitle(): TextView {
        val textView = TextView(clContext)
        textView.text = sTitle
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        textView.setPadding(UIHelper.i5px, 0, 0, 0)
        textView.setTextColor(ThemeConstant.pinkies)
        textView.setInterBoldFont()
        return textView
    }

    private fun getFontIconView(): FontIconView {
        val fontIconView = FontIconView(clContext)
        fontIconView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        fontIconView.text = clContext.resources.getString(R.string.icon_info)
        fontIconView.setTextColor(ThemeConstant.pinkies)
        return fontIconView
    }

    override fun onResume() {
        super.onResume()
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    interface OnActionPerformed {
        fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?)
        fun onDismissed(actionType: Int, actionLabel: String?)
    }

    override fun onShow(dialog: DialogInterface?) {

    }
}