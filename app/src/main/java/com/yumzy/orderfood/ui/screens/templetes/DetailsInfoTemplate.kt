/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.ViewConst

open class DetailsInfoTemplate(
    override val context: Context,
    imageUrl: String,
    heading: CharSequence,
    subheading: String,
    private val points: List<String?>
) : InfoTemplate(context, 0, heading, subheading) {
    override var headerHeight: Int = VUtil.dpToPx(80)
        set(value) {
            field = VUtil.dpToPx(value)
        }
    override val showClose = false

    override fun getTextView(tvId: Int, msg: CharSequence): View? {
        if (tvId == R.id.tv_info_sub_header) {
            return getTextList(points)
        }
        val i5px = VUtil.dpToPx(5)

        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.LEFT
        textView.setPadding(0, i5px, 0, 0)
        textView.setTextColor(Color.GRAY)
        textView.setInterFont()
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        return textView
    }

    private fun getTextList(points: List<String?>): View? {
        val layout = LinearLayout(context)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        layout.orientation = LinearLayout.VERTICAL
        for (item in points) {
            layout.addView(getTextView(-1, "• $item"))
        }

        return layout
    }
}
