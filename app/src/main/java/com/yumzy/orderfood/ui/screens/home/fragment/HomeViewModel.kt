package com.yumzy.orderfood.ui.screens.home.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.home.data.HomeRepository
import javax.inject.Inject

/**
 * The ViewModel for [HomeFragment].
 */
class HomeViewModel  constructor(
    val homeRepository: HomeRepository
) :
    BaseViewModel<IHomeView>() {

    private val _isSelectedAddress: MutableLiveData<ArrayList<HomeListDTO>> by lazy {
        MutableLiveData(ArrayList<HomeListDTO>())
    }

    val homeItemList: LiveData<ArrayList<HomeListDTO>>
        get() = _isSelectedAddress


    val homeList: (latitude: String, longitude: String, skipIndex: Int, lastIndex: Int) -> Unit =
        { latitude, longitude, skipIndex, lastIndex ->
            launch {
                val flatHomeData =
                    homeRepository.getFlatHomeData(
                        view!!,
                        latitude,
                        longitude,
                        skipIndex,
                        lastIndex
                    )
                val data = flatHomeData.data
                if (data != null) {
                    val previousHomeData = _isSelectedAddress.value
                    previousHomeData?.addAll(data)
                    _isSelectedAddress.postValue(previousHomeData)
                }
            }
        }

    fun flatHomeList(latitude: String, longitude: String, skipIndex: Int, lastIndex: Int) =
        homeRepository.flatHomeList(latitude, longitude, skipIndex, lastIndex)

    fun getHomeResponse(latitude: String, longitude: String, skipIndex: Int, lastIndex: Int) =
        homeRepository.getHomeData(latitude, longitude, skipIndex, lastIndex)

    fun clearHomeData() {
        _isSelectedAddress.value?.clear()
    }


    val orderList = homeRepository.getOrders()

}
