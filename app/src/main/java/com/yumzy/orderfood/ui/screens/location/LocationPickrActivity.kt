package com.yumzy.orderfood.ui.screens.location

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import com.birjuvachhani.locus.Locus
import com.birjuvachhani.locus.isFatal
import com.clevertap.android.geofence.CTGeofenceAPI
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.LongLat
import com.yumzy.orderfood.databinding.ActivityLocationPickrBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import kotlinx.android.synthetic.main.dialog_rate_order.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class LocationPickrActivity : BaseActivity<ActivityLocationPickrBinding, LocationViewModel>(),
    ILocationView, OnMapReadyCallback {

    private var latLng = arrayListOf<Double>()
    private var mGoogleMap: GoogleMap? = null
    private var mSelectedGoogleAddress: android.location.Address? = null

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true

    private lateinit var pickedAddress: AddressDTO

    
    override val vm: LocationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.attachView(this)


        this.bindView(R.layout.activity_location_pickr)

        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)

        getLatLngFromIntent()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        bd.tvSkipAndContinue.isGone = !pickedAddress.addressId.isNullOrEmpty()

        if(sharedPrefsHelper.getIsTemporary()) {
            bd.btnSelectLocation.visibility = View.GONE
            bd.tvSkipAndContinue.text = resources.getString(R.string.explore)
        }

        applyDebouchingClickListener(bd.btnSelectLocation, bd.tvSkipAndContinue, bd.cvMyLocation)
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mGoogleMap = googleMap

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = mGoogleMap?.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.google_maps_style
                )
            )
            if (success == true) {
                Timber.e("Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Timber.e("Can't find style. Error: ${e}")
        }

        val point = CameraUpdateFactory.newLatLng(
            LatLng(
                latLng[0],
                latLng[1]
            )
        )


        // moves camera to coordinates
        mGoogleMap?.moveCamera(point)

        mGoogleMap?.let {
            it.uiSettings.isRotateGesturesEnabled = false
            it.uiSettings.isTiltGesturesEnabled = false
            it.uiSettings.isMyLocationButtonEnabled = true
            it.mapType = GoogleMap.MAP_TYPE_NORMAL
            it.setOnCameraMoveStartedListener {
                bd.expandingMarker.progress = 0f
                bd.expandingMarker.setMinAndMaxProgress(0.0f, 0.65f)
                bd.expandingMarker.playAnimation()
            }
            it.setOnCameraIdleListener {
                bd.expandingMarker.progress = 0.65f
                bd.expandingMarker.setMinAndMaxProgress(0.65f, 1f)
                bd.expandingMarker.playAnimation()
                getAddress(it.cameraPosition.target.latitude, it.cameraPosition.target.longitude)
            }

        }

        setMapPosition(latLng[0], latLng[1])
    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }

    private fun getLatLngFromIntent() {
        val i = intent
        pickedAddress = i.getSerializableExtra("address") as AddressDTO
        latLng.add(pickedAddress.longLat?.coordinates?.get(1) ?: 17.88282)
        latLng.add(pickedAddress.longLat?.coordinates?.get(0) ?: 78.32343)
    }

    private fun getAddress(lat: Double, lang: Double) {

        val gcd = Geocoder(this, Locale("en", "IN"))
        val addressList: List<android.location.Address>?

        val latitude = lat
        val longitude = lang

        try {
            addressList = gcd.getFromLocation(latitude, longitude, 2)
            if (addressList != null && addressList.isNotEmpty()) {

                if (addressList[0].locality != null && addressList[0].locality != "") {
                    mSelectedGoogleAddress = addressList[0]
                } else if (addressList[1].locality != null && addressList[1].locality != "") {
                    mSelectedGoogleAddress = addressList[1]
                } else {
                    YumUtil.ensureResult(kotlinx.coroutines.Runnable{
                        getAddress(lat, lang)
                    })
                    return
                }

                mSelectedGoogleAddress?.latitude = lat
                mSelectedGoogleAddress?.longitude = lang

                bd.addressText = mSelectedGoogleAddress?.getAddressLine(0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            YumUtil.ensureResult(kotlinx.coroutines.Runnable{
                getAddress(lat, lang)
            })
        }
    }

    private fun setMapPosition(lat: Double, lng: Double) {
        val cameraPosition = CameraPosition.Builder()
            .target(LatLng(lat, lng)).zoom(17f).tilt(1f).build()
        mGoogleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun startLocationInfoActivity(address: AddressDTO) {
        val intent = Intent(this, LocationInfoActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.ADDRESS, address)
        startActivity(intent)
    }

    private fun getCurrentLocation() {
        showProgressBar()
        CTGeofenceAPI.getInstance(this).triggerLocation()
        Locus.getCurrentLocation(this) { result ->
            hideProgressBar()
            result.location?.let {
                val parentJob = Job()
                CoroutineScope(Dispatchers.Main + parentJob).launch {
                    setMapPosition(it.latitude, it.longitude)
                }
            } ?: run {
                hideProgressBar()
                if(result.error?.isFatal == true) {
                    YumUtil.ensureResult(kotlinx.coroutines.Runnable {
                        getCurrentLocation()
                    }, 1500)
                } else if (result.error != null) {
                    CTGeofenceAPI.getInstance(applicationContext)
                        .setCtLocationUpdatesListener {
                            setMapPosition(
                                it?.latitude ?: 17.4311184,
                                it?.longitude ?: 78.3722143
                            )
                        }
                } else {
                    AlerterHelper.showInfo(this, "Failed to fetch your location, Try Again.")
                }
            }
        }
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            bd.btnSelectLocation.id -> {

                mSelectedGoogleAddress?.let { gooAdd ->
                    pickedAddress.run {
                        googleLocation =
                            gooAdd.subLocality ?: (gooAdd.subAdminArea ?: gooAdd.locality)
                        city = gooAdd.locality
                        val lonLat = LongLat()
                        lonLat.coordinates = listOf(gooAdd.longitude, gooAdd.latitude)
                        longLat = lonLat
                        this.fullAddress =
                            gooAdd.getAddressLine(0) ?: getString(R.string.trying_to_get_location)
                    }
                }

                startLocationInfoActivity(pickedAddress)
            }
            bd.tvSkipAndContinue.id -> {

                mSelectedGoogleAddress?.let { gooAdd ->
                    pickedAddress.run {
                        name = gooAdd.subLocality ?: (gooAdd.subAdminArea ?: gooAdd.locality)
                        googleLocation =
                            gooAdd.subLocality ?: (gooAdd.subAdminArea ?: gooAdd.locality)
                        city = gooAdd.locality
                        val lonLat = LongLat()
                        lonLat.coordinates = listOf(gooAdd.longitude, gooAdd.latitude)
                        longLat = lonLat
                        this.fullAddress =
                            gooAdd.getAddressLine(0) ?: getString(R.string.trying_to_get_location)
                        this.shortAddress =
                            "${gooAdd.subAdminArea ?: gooAdd.locality}, ${gooAdd.locality ?: gooAdd.adminArea}"


                    }


                }

                val empty = pickedAddress.name.isEmpty()

                if (empty) {
                    showToast("Please wait while we are getting your location")

                    /*MotionToast.createToast(
                        this,
                        "Fetching location",
                        "Please wait while we getting you location",
                        MotionToast.TOAST_INFO,
                        MotionToast.GRAVITY_BOTTOM,
                        MotionToast.SHORT_DURATION,
                        ResourcesCompat.getFont(this, R.font.raleway)
                    )*/
                    /*showProgressBar()
                    Handler(Looper.getMainLooper()).postDelayed({

                        hideProgressBar()
                    },1000)
                    */

                    return
                }
                AppGlobalObjects.selectedAddress = pickedAddress
                sharedPrefsHelper.setSelectedAddress(pickedAddress)

                val resultData = Intent()
                resultData.putExtra("pickedAddress", pickedAddress)
                resultData.putExtra("show_home", true)
                setResult(Activity.RESULT_OK, resultData)
                finish()

//                val intent = Intent(this, HomePageActivity::class.java)
//                AppIntentUtil.launchClipRevealFromIntent(view, this, intent)
//                finish()

            }

            bd.cvMyLocation.id -> {
                getCurrentLocation()
            }

            else -> {
                super.onDebounceClick(view)
            }
        }
    }

}