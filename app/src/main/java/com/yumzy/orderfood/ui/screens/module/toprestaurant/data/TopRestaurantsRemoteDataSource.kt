package com.yumzy.orderfood.ui.screens.module.toprestaurant.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.HomeServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu on 30-May-20.
 */
class TopRestaurantsRemoteDataSource  constructor(private val serviceAPI: HomeServiceAPI) :
    BaseDataSource() {
    suspend fun getRestaurants(
        promoID: String, lat: String,
        lng: String, skip: Int, limit: Int
    ) = getResult {
        serviceAPI.getRestaurants(
            promoID, lat,
            lng, skip, limit
        )
    }

}
