/*
 *   Created by Bhupendra Kumar Sahu
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.InputType
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.gapTop
import com.yumzy.orderfood.ui.helper.setRalewayMediumFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


open class WithdrawInitiateTemplate(
    override val context: Context,
    val title: String = "",
    val vpa: String = "",
    cEvent: UpdateSingleInfoListener
) : IDetailsActionTemplate {
    var mUpdateSingleInfoListener: UpdateSingleInfoListener

    init {
        mUpdateSingleInfoListener = cEvent
    }

    interface UpdateSingleInfoListener {
        fun sendwithdrawAmount(text: String, dialog: DialogInterface?, vpa: String)
//        fun showTerms()

    }


    override var headerHeight: Int = VUtil.dpToPx(40)
        set(value) {
            field = VUtil.dpToPx(value)
        }

    override val showClose = true

    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.setPadding(0, 8.px, 0, 8.px)
        val fontIconView = FontIconView(context)
        val layoutParams = LinearLayout.LayoutParams(65.px, 65.px)
        layoutParams.setMargins(0, 0, 0, 5.px)
        fontIconView.layoutParams = layoutParams
        fontIconView.gravity = Gravity.CENTER
        fontIconView.setPadding(10.px, 10.px, 10.px, 10.px)
        fontIconView.text = context.getString(R.string.icon_035_payment)
        fontIconView.setTextColor(ThemeConstant.white)
        fontIconView.setBackgroundResource(R.drawable.rounded_pink_50)
        layout.gravity = Gravity.CENTER
        layout.orientation = LinearLayout.VERTICAL
        layout.addView(fontIconView)
        layout.setPadding(0, 0, 0, VUtil.dpToPx(20))
        layout.addView(getTextView(R.id.tv_info_header, title))
        return layout
    }


    @SuppressLint("SetTextI18n")
    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        layout.minimumHeight = headerHeight


        val edtUpi = EditText(context)
        edtUpi.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        edtUpi.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        edtUpi.id = R.id.edt_upi
        edtUpi.isEnabled = false
        edtUpi.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )
        edtUpi.setText(vpa)
        edtUpi.setRalewayMediumFont()
        edtUpi.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.cool_gray,
                context.theme
            )
        )
        val verified = MaterialTextView(context)
        verified.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(20))
        verified.text = context.resources.getString(R.string.verified)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            verified.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
        }
        else
        {
            verified.setTextAppearance(context,R.style.TextAppearance_MyTheme_Caption)

        }


        verified.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.colorGreen,
                context.theme
            )
        )
        verified.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        verified.gravity = Gravity.END
        verified.id = R.id.tv_verified

        val edittext = EditText(context)
        edittext.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        edittext.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        edittext.id = R.id.edt_withdraw_amount


        edittext.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )
        //edittext.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        edittext.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL


        edittext.setRalewayMediumFont()
        edittext.hint = context.getString(R.string.enter_withdraw_amount)
        edittext.setHintTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.cool_gray,
                context.theme
            )
        )

        edittext.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.black,
                context.theme
            )
        )


        val contextThemeWrapper = ContextThemeWrapper(context, R.style.Yumzy_Button)
        val btn = MaterialButton(contextThemeWrapper)

        btn.layoutParams =LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 50.px)
        btn.setTextColor(Color.WHITE)
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkies)
        btn.rippleColor = ColorStateList.valueOf(ThemeConstant.rippleColor)
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
        btn.typeface = typeface
        btn.id = R.id.btn_submit_amount

        btn.gapTop(VUtil.dpToPx(5))

        btn.text = context.getString(R.string.submit)

        val v = View(context)
        v.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(10))

//        val terms = TextView(context)
//        terms.layoutParams =
//            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
//        terms.text = context.resources.getString(R.string.terms)
//        terms.setTextColor(
//            ResourcesCompat.getColor(
//                context.resources,
//                R.color.blueJeans,
//                context.theme
//            )
//        )
//        terms.setRalewayMediumFont()
//
//        terms.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
//        terms.gravity = Gravity.END
//        terms.id = R.id.tv_terms_withdraw


        layout.addView(edtUpi)
        layout.addView(verified)
        layout.addView(edittext)
        layout.addView(btn)


        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {

        val edittext = view?.findViewById<EditText>(R.id.edt_withdraw_amount)
        edittext?.maxLines = 1
        edittext?.minLines = 1
        edittext?.isSingleLine = true
            // edittext?.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        edittext?.filters = arrayOf(YumUtil.CLDecimalDigitsInputFilter(6, 2))
//        view?.findViewById<TextView>(R.id.tv_terms_withdraw)?.setOnClickListener {
//            mUpdateSingleInfoListener.showTerms()
//        }
        view?.findViewById<Button>(R.id.btn_submit_amount)?.setOnClickListener {
            if ((!edittext?.text.toString().equals("")) && (!edittext?.text.toString()
                    .equals("0")) && (!edittext?.text.toString().equals(
                    "."
                )) && (edittext?.text.toString().toDouble() >= 1)
            ) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(
                    edittext?.getApplicationWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
                mUpdateSingleInfoListener.sendwithdrawAmount(
                    edittext?.text.toString().trim(), dialog, vpa
                )
            } else {

                edittext?.let { it1 -> YumUtil.animateShake(it1, 20, 5) }

            }
        }


    }


    open fun getTextView(tvId: Int, msg: String): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = MaterialTextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            textView.setTextAppearance(R.style.TextAppearance_MyTheme_Headline6)
        }
        else
        {
            textView.setTextAppearance(context,R.style.TextAppearance_MyTheme_Headline6)

        }

        if (tvId == R.id.tv_info_header) {
            textView.setRalewayMediumFont()
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.DKGRAY)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        }

        return textView

    }

}

