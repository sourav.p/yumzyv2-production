package com.yumzy.orderfood.ui.screens.login

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.UserDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import javax.inject.Inject


class LoginViewModel  constructor(private val repository: LoginRepository) :
    BaseViewModel<ILoginView>() {

    var firstName = ""
    var lastName = ""
    var emailAddress = ""
    var mobileNumber = ""
    var countryCode = "+91"
    var number = MutableLiveData("")
    var validForm = MutableLiveData(IConstants.RegisterStep.STEP_ONE)

    lateinit var user: UserDTO

    //    ============>Login logic

    val validPhone = MediatorLiveData<Boolean>().apply {
        addSource(number) {
            val valid = YumUtil.isValidMobile(it)
            value = valid
        }
    }

    fun sendOTP() = repository.sendOTP(number.value!!, countryCode, APIConstant.Actions.LOGIN)
    fun sendRegisterOTP(number: String) =
        repository.sendOTP(number, countryCode, APIConstant.Actions.SIGN_UP)

    //    ============>Register logic

    fun registerNewUser(userOtp: String) =
        repository.registerUser(
            firstName.trim().replace("\\s+".toRegex(), " ").capitalize(),
            lastName.trim().replace("\\s+".toRegex(), " ").capitalize(),
            userOtp,
            emailAddress,
            mobileNumber
        )


    fun saveUserInfo(userDTO: UserDTO) = repository.saveUserInfo(userDTO)

    //    val selectedAddress =repository.getSelectedAddress()
    fun getSelectedAddress() = repository.getSelectedAddress()
/*

    fun saveUserCurrentLocation(mCurrentLocation: SelectedAddress): LiveData<ResponseDTO<Boolean>> {
        return repository.saveUserCurrentLocation(mCurrentLocation)
    }
*/

    fun verifyOTP(number: String, otpCode: String, deviceId: String) = repository
        .verifyOTP(number, otpCode, "+91", deviceId)

    fun exploreUser(deviceId: String) = repository
        .exploreUser(deviceId)


    fun addAddress(address: AddressDTO) = repository.addAddress(address)

    fun updateAddress(address: AddressDTO) = repository.updateAddress(address)

    fun resendOTP(number: String, action: String) = repository
        .resendOTP(number, APIConstant.Actions.TEXT, action)

    fun updateFcmToken(token: String, userId: String) = repository.updateUserFcmToken(token, userId)
    fun updateCurrentLocation(latitude: String, longitude: String, city: String, locality: String) =
        repository.updateLocation(latitude, longitude, city, locality)
}