/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import javax.inject.Inject

class OrderTrackingServiceViewModel  constructor(
    private val repository: OrderRepository
) : BaseViewModel<IView>() {

    fun getOrderStatus(orderId: String) = repository.getOrderStatus(orderId)


}