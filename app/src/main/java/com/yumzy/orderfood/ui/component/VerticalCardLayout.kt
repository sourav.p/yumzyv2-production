package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


/**
 *    Created by Sourav Kumar Pandit
 * */
class VerticalCardLayout : LinearLayout {


    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        initAttrs(attrs, context, defStyleAttr)
        initView()


    }

    var imageRatio: String = "5:3"
        set(value) {
            field = value
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)

            val imageParam =
                ConstraintLayout.LayoutParams(ViewConst.MATCH_PARENT, 0)
            imageParam.dimensionRatio = this.imageRatio

            imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
            imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            imageView.layoutParams = imageParam
        }
    lateinit var discountDrawable: Drawable
    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.font_icon_view)?.text = value
            field = value
        }
    var title: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_name)?.text = value
            field = value

        }
    var cuisines: String = ""
        set(value) {

            this.findViewById<TextView>(R.id.tv_item_tag)?.text = value
            field = value
            if (value.isNullOrEmpty()) {
                this.findViewById<TextView>(R.id.tv_item_tag)?.visibility = View.GONE
            } else {
                this.findViewById<TextView>(R.id.tv_item_tag)?.visibility = View.VISIBLE
            }


        }
    var subtitle: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_locality)?.text = value
            field = value
            if (value.isNullOrEmpty()) {
                this.findViewById<TextView>(R.id.tv_item_locality).visibility = View.GONE
            } else {
                this.findViewById<TextView>(R.id.tv_item_locality).visibility = View.VISIBLE
            }
        }

    var colorText: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.tv_coupon_code)
            view.maxLines = 1
//            YumUtil.textUniformTextView(view)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.INVISIBLE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }

    var description: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.description)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.INVISIBLE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }
    var costForTwo: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.tv_cost_for_two)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.INVISIBLE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }
    var imageUrl: String = ""
        set(value) {
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(value).into(imageView)
            YUtils.setImageInView(imageView, value)

            field = value
        }

    var isVeg: Boolean = true
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.setTextColor(ThemeConstant.greenConfirmColor)
            } else {
                view.setTextColor(ThemeConstant.nonVegColor)
            }
        }
    var removeRating: Boolean = false
        set(value) {
            field = value
            val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            if (view != null && removeAddQuantity) {
                this.removeView(view)
            }

        }

    fun removeAddQuantity() {
        val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
        val layout = this.findViewById<LinearLayout>(R.id.add_quantity_layot)
        if (view != null && removeAddQuantity) {
            layout.removeView(view)
        }

    }

    var removeAddQuantity: Boolean = false
        set(value) {
            field = value
            val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            val layout = this.findViewById<LinearLayout>(R.id.add_quantity_layot)
            if (view != null && removeAddQuantity) {
                layout.removeView(view)
            }

        }

    var removeVegIcon: Boolean = false
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

    var ribbonText: String? = null
        set(value) {
            field = value
            val findViewById = this.findViewById<RibbonView>(R.id.ribbon_view)
            findViewById?.ribbonText = ribbonText

        }


    var price: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_price)?.text = value
            field = value
        }

    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.VerticalCardLayout,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.VerticalCardLayout_verticalCardTitle) ?: ""
            subtitle = a.getString(R.styleable.VerticalCardLayout_verticalCardLocality) ?: ""
            cuisines = a.getString(R.styleable.VerticalCardLayout_verticalCardSubtitle) ?: ""
            colorText = a.getString(R.styleable.VerticalCardLayout_verticalCardCouponCode) ?: ""
            description = a.getString(R.styleable.VerticalCardLayout_verticalCardDescription) ?: ""
            price = a.getString(R.styleable.VerticalCardLayout_verticalCardPrice) ?: ""
            costForTwo = a.getString(R.styleable.VerticalCardLayout_verticalCardCostForTwo) ?: ""
            ribbonText = a.getString(R.styleable.VerticalCardLayout_verticalRibbonText) ?: ""
//            discountDrawable =
//                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!

            a.recycle()

        }
//        else {
//        }
    }

    private fun initView() {

        this.layoutParams =

            ViewGroup.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)

//        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        val roundImage = restaurantImageView()
        val ribbonView = getRibbonView()

        val itemNameLayout = LinearLayout(context).apply {
            id = R.id.card_top_layout
            orientation = LinearLayout.VERTICAL
            layoutParams =
                ConstraintLayout.LayoutParams(0, ViewConst.WRAP_CONTENT)
                    .apply {
                        startToStart = R.id.restaurant_image
                        topToBottom = R.id.restaurant_image
                        endToEnd = R.id.restaurant_image
                    }
        }

        itemNameLayout.addView(getLabeledTextView(title, 16f, R.id.tv_item_name))
        itemNameLayout.addView(getTextView(subtitle, 10f, R.id.tv_item_locality))
        itemNameLayout.addView(getTextView(cuisines, 10f, R.id.tv_item_tag))
        itemNameLayout.addView(getTextView(costForTwo, 10f, R.id.tv_cost_for_two))
        itemNameLayout.addView(getTextView(colorText, 11f, R.id.tv_coupon_code))


        val priceQuantityView: LinearLayout = getPriceQuantityView(price, 12f, R.id.tv_item_price)
        val bottomParam =
            ConstraintLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        priceQuantityView.setPadding(0, UIHelper.i3px, 0, 0)
        bottomParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        bottomParam.topToBottom = R.id.card_top_layout
        priceQuantityView.layoutParams = bottomParam

        val constraintLayout = ConstraintLayout(context)
        constraintLayout.layoutParams =
            LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        constraintLayout.addView(roundImage)
        constraintLayout.addView(getRattingView(context))
        constraintLayout.addView(itemNameLayout)
        constraintLayout.addView(priceQuantityView)
        constraintLayout.addView(ribbonView)
        this.addView(constraintLayout)

    }

    private fun getRibbonView(): RibbonView {
        val ribbonView = RibbonView(context)
        val ribbonParam =
            ConstraintLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        ribbonParam.setMargins(-UIHelper.i3px, UIHelper.i5px, 0, 0)
        ribbonView.layoutParams = ribbonParam
        ribbonView.ribbonTextSize(18f)

        ribbonView.id = R.id.ribbon_view
        ribbonView.ribbonText = ribbonText
        return ribbonView
    }

    private fun getLabeledTextView(itemTag: String, fl: Float, tvItemTag: Int): View? {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        val layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.marginEnd = VUtil.dpToPx(5)
        layoutParams.topMargin = UIHelper.i8px
        linearLayout.layoutParams = layoutParams
        val fontIconView = FontIconView(context)
        fontIconView.tag = "icon_veg"
        fontIconView.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        fontIconView.setPadding(0, 0, VUtil.dpToPx(5), 0)
        fontIconView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        fontIconView.setTextColor(ThemeConstant.greenConfirmColor)
        fontIconView.text = context.resources.getText(R.string.icon_veg_non_veg)

        linearLayout.addView(fontIconView)
        linearLayout.addView(getTextView(itemTag, fl, tvItemTag))
        return linearLayout
    }


    private fun getPriceQuantityView(itemTag: String, fl: Float, tvItemTag: Int): LinearLayout {
        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        linearLayout.id = R.id.add_quantity_layot
        val layoutParams =
            LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
        layoutParams.marginEnd = VUtil.dpToPx(5)
        linearLayout.layoutParams = layoutParams
        val addQuantityView = AddQuantityView(context)
        addQuantityView.id = R.id.add_quantity_view
        linearLayout.addView(getTextView(itemTag, fl, tvItemTag), layoutParams)
        linearLayout.addView(addQuantityView)
        return linearLayout
    }

    private fun getRattingView(context: Context): View {

        val rattingView = RatingView(context)
        rattingView.id = R.id.rating_card
        val rattingParam = ConstraintLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        rattingParam.marginStart = VUtil.dpToPx(10)
        rattingParam.bottomMargin = VUtil.dpToPx(10)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam

        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            ConstraintLayout.LayoutParams(ViewConst.MATCH_PARENT, 0)
        imageParam.dimensionRatio = this.imageRatio

        imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.setImageResource(R.drawable.ic_default_img)
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }

    private fun getTextView(text: String, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        if (tvId == R.id.tv_item_name) {
            textView.setInterBoldFont()
            textView.maxLines = 2
            textView.ellipsize = TextUtils.TruncateAt.END
            textView.setTextColor(ThemeConstant.textBlackColor)

        } else if (tvId == R.id.tv_item_tag) {
            textView.setTextColor(ThemeConstant.textDarkGrayColor)
            textView.ellipsize = TextUtils.TruncateAt.END
            textView.maxLines = 1

        } else if (tvId == R.id.tv_item_locality) {
            textView.setTextColor(ThemeConstant.textDarkGrayColor)
            textView.ellipsize = TextUtils.TruncateAt.END
            textView.setPadding(0, VUtil.dpToPx(2), 0, 0)
            textView.maxLines = 1

        } else if (tvId == R.id.tv_coupon_code) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
            textView.setTextColor(ThemeConstant.redRibbonColor)
            textView.ellipsize = TextUtils.TruncateAt.END
            textView.maxLines = 1


        } else if (tvId == R.id.tv_item_price) {
            textView.setTextColor(ThemeConstant.textBlackColor)
            textView.setInterBoldFont()
            textView.gravity = Gravity.START
            textView.maxLines = 1
            textView.setPadding(0, VUtil.dpToPx(2), 0, 0)


        } else if (tvId == R.id.tv_cost_for_two) {
            textView.setPadding(0, VUtil.dpToPx(2), 0, 0)
            textView.setTextColor(ThemeConstant.textGrayColor)
            textView.setInterRegularFont()
            textView.setPadding(0, VUtil.dpToPx(2), 0, 0)
            textView.gravity = Gravity.START
            textView.maxLines = 1


        }
//        textView.setOnClickListener(this)
        return textView

    }

    fun getAddQuantityView(): AddQuantityView? {
        return this.findViewById(R.id.add_quantity_view)
    }

    fun setRating(ratting: String) {
        val rattingView = findViewById<RatingView>(R.id.rating_card)
        rattingView?.setRating(ratting)
    }

    fun setQuantitiyListener(countCaller: (Int) -> Unit) {
        this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            .setOnCounterChange(object : AddQuantityView.OnCounterChange {
                override fun onCounterChange(count: Int) {
                    countCaller(count)
                }
            })
    }

    fun updateImageParams(width: Int, height: Int, ratio: String) {
        this.findViewById<RoundedImageView>(R.id.restaurant_image).apply {
            layoutParams = ConstraintLayout.LayoutParams(width, height).apply {
                this.dimensionRatio = ratio
            }

        }
    }

    /*override fun onClick(view: View) {
        when (view.id) {
            R.id.iv_rounded_image -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_title -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_subtitle -> this.restaurantCardListener?.onRestaurantImageClick()
            else -> this.restaurantCardListener?.onRestaurantClick()
        }
    }
*/

}