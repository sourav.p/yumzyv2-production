/*
 * Created  By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.foodpersonalisation

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FoodPersReqDTO
import com.yumzy.orderfood.data.models.FoodPersResDTO
import com.yumzy.orderfood.data.models.PersonalisationDTO
import com.yumzy.orderfood.data.models.PersonalisationResponseDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogFoodPersonalisationBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.component.SelectionTextView
import com.yumzy.orderfood.ui.component.groupview.FlowLayoutGroupView
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject
import kotlin.String as String1


class FoodPersonalisationDialog(
    private val selectedVegan: String1,
    private val selectedCuisineList: String1

) : BaseDialogFragment<DialogFoodPersonalisationBinding>(),
    FlowLayoutGroupView.OnSelectedItemChangeListener {

    private var mGroupId = 9500
    private var mSelectionList = mutableMapOf<Int, List<String1>>()
    private var mSelectionTagList = listOf<kotlin.String>()
    var tags: kotlin.String = ""

    val vm: FoodPersonalisationViewModel by viewModel()

    override fun getLayoutId(): Int = R.layout.dialog_food_personalisation

    override fun onDialogReady(view: View) {
        view.background = ColorDrawable(Color.WHITE)
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbFoodPres)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        bd.tbFoodPres.title = resources.getString(R.string.food_personalization)
        bd.tbFoodPres.setNavigationOnClickListener { this.dismiss() }
        val split = selectedCuisineList.split(",")
        val selPersonalisation = PersonalisationDTO(split, selectedVegan)
        setupFoodPreferenceTags(selPersonalisation)

        val data = vm.getData()
        applyDebouchingClickListener(bd.btnFoodPersonalisation)
        //setupList(data)
    }

    private fun setupFoodPreferenceTags(dto: PersonalisationDTO) {
        vm.getFoodPreferenceTags(dto).observe(
            this,
            { result ->
                RHandler<ArrayList<PersonalisationResponseDTO>>(this, result) {
                    setupList(it)
                }
            })
    }

    private fun setupList(dataList: ArrayList<PersonalisationResponseDTO>) {
        bd.progressBar.visibility = View.VISIBLE
        bd.expandPersonalisationView.buildWithArrayList(
            dataList, fun(listOfString: Any): View {
                //setup expansion view i.e FlowGroupView
//                val flowLayout =FlowLayout(context)


                return FlowLayoutGroupView(context ?: requireContext()).apply {
                    id = mGroupId++
                    setItems(
                        listOfString as List<String1>,
                        fun(
                            listener: OnGroupItemClickListener<SelectionTextView>,
                            item: String1
                        ): SelectionTextView {
                            val view = SelectionTextView(context)
                            view.setPadding(
                                UIHelper.i10px,
                                UIHelper.i8px,
                                UIHelper.i10px,
                                UIHelper.i8px
                            )
//                            view.background=VUtil.getRoundDrawableListState()
                            view.text = item
                            view.setOnItemClickListener(listener)
                            return view
                        }
                    )
                    setProperties(
                        isMultiSelectEnabled = true,
                        minimumSelection = 0
                    )
                    setOnSelectedItemChangeListener(this@FoodPersonalisationDialog)
                    if (listOfString.size > 3) {
                        setItemSelectedAt(0, 1, 2)
                    } else if (listOfString.size > 0) {
                        setItemSelectedAt(0)
                    }

                }
            }, true
        )
        bd.progressBar.visibility = View.GONE
    }

    //    private fun setupList(dataList: ArrayList<PersonalisationResponseDTO>) {
//        bd.progressBar.visibility = View.VISIBLE
//        bd.expandPersonalisationView.buildWithArrayList(
//            dataList, fun(listOfString: Any): FlowLayout {
//                listOfString as List<String1>
//                val flowLayout= FlowLayout(context)
//
//                for (data in listOfString) {
//                    flowLayout.addView(getTextView(data))
//                }
//                return flowLayout
//            }, true
//        )
//        bd.progressBar.visibility = View.GONE
//    }
//
//    private fun getTextView(data: kotlin.String): View? {
//        return  Chip(context).apply{
//            setPadding(15,15,15,15)
//            setText("data")
//
//        }
//
//    }
    override fun onSuccess(actionId: Int, data: Any?) {
    }


    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.btn_food_personalisation -> {
                val selectedCuisineArray = selectedCuisineList.split(",")
                val foodPers: FoodPersReqDTO =
                    FoodPersReqDTO(mSelectionTagList, selectedVegan, selectedCuisineArray)
//                CleverTapHelper.pushEvent(CTEvents.food_preference)
                CleverTapHelper.userFoodPreference(foodPers)

                vm.foodPres(foodPers).observe(this, { result ->
                    RHandler<FoodPersResDTO>(
                        this,
                        result

                    ) {
                        showMessage(result.data as FoodPersResDTO)
                    }

                })
            }

        }
    }

    private fun showMessage(data: FoodPersResDTO) {
        actionHandler?.onSuccess(moduleId, null)
        this.dismiss()


    }

    override fun onSelectedItemChanged(list: MutableList<GroupItemActions<*>>, listType: Int) {
        mSelectionTagList = list.map { item -> (item as SelectionTextView).text.toString() }
        tags = mSelectionTagList.toString()
    }

    override fun onShowMessage(message: kotlin.String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    }

    override fun showCodeError(code: Int?, title: kotlin.String?, message: kotlin.String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.btnFoodPersonalisation.visibility = View.GONE
            bd.nestedScrollView.visibility = View.GONE
            bd.tvLableDescription.visibility = View.GONE
            bd.progressBar.visibility = View.GONE
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet
        }
    }

}