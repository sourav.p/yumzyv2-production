package com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.models.FoodPersReqDTO
import com.yumzy.orderfood.data.models.PersonalisationDTO
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class FoodPersonalisationRepository  constructor(
    private val remoteSource: FoodPersonalisationRemoteDataSource
) {
    fun getFoodPreferenceTags(dto: PersonalisationDTO) = networkLiveData(
        networkCall = {
            remoteSource.getFoodPreferenceTags(dto)
        }
    ).distinctUntilChanged()

    fun foodPres(dto: FoodPersReqDTO) = networkLiveData(
        networkCall = {
            remoteSource.foodPres(dto)
        }
    ).distinctUntilChanged()

}