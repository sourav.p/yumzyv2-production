
package com.yumzy.orderfood.ui.screens.restaurant
/*
import android.content.Context
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.component.VerticalCardLayout
import com.yumzy.orderfood.ui.screens.module.ICartHandler

class ReccomendedAdapter(
    val context: Context,
    override val vm: RestaurantViewModel,
    var menuList: ArrayList<RestaurantItem>,
    var vegMenuList: ArrayList<RestaurantItem> = menuList.filter { item -> item.isVeg } as ArrayList<RestaurantItem>
) : RecyclerView.Adapter<ReccomendedAdapter.GridViewHolder>() {

    private var showVegEnable: Boolean = false
    private var items: MutableList<RestaurantItem> =
        menuList.map { item -> item } as MutableList<RestaurantItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val verticalCardLayout =
            VerticalCardLayout(context = context).apply {
                imageRatio = "1:0.8"
            }
        verticalCardLayout.removeAddQuantity()
        return GridViewHolder(verticalCardLayout)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) =
        holder.bindData(items[position])

    override fun getItemViewType(position: Int): Int = items.size

    internal fun showVegMenu(showVeg: Boolean) {
        showVegEnable = showVeg

        val diffCallback = if (showVeg) {
            MenuItemDiff(items, vegMenuList)
        } else {
            MenuItemDiff(items, menuList)
        }
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()

        if (showVeg) {
            items.addAll(vegMenuList)
        } else {
            items.addAll(menuList)
        }

        diffResult.dispatchUpdatesTo(this)

    }

    internal fun updateList(list: ArrayList<RestaurantItem>) {
        menuList = list
        items = list
        if (showVegEnable) {
            vegMenuList = menuList.filter { item -> item.isVeg } as ArrayList<RestaurantItem>
            items = vegMenuList
        }
        notifyDataSetChanged()
    }

    inner class GridViewHolder(val layout: VerticalCardLayout) : RecyclerView.ViewHolder(layout) {

        fun bindData(menuItem: RestaurantItem) {
            layout.itemTitle = menuItem.name
            layout.itemSubtitle = menuItem.description
            menuItem?.offers?.let {
                if (!it.isNullOrEmpty()) {
                    layout.description = it[0]?.description ?: ""
                }
            }
            layout.isVeg = menuItem.isVeg
            layout.imageUrl = menuItem.imageUrl
            layout.price = "₹" + menuItem.price.toString()
            if (menuItem.prevQuantity > 0) layout.getAddQuantityView()?.quantityCount =
                menuItem.prevQuantity
            layout.setQuantitiyListener { count ->
                val vieww = layout.getAddQuantityView()

                val restaurantItem =
                    if (showVegEnable) {
                        vegMenuList[adapterPosition]

                    } else {
                        menuList[adapterPosition]
                    }

                vm.addItemToCart(
                    itemView.context as LifecycleOwner,
                    count,
                    restaurantItem,
                    vieww,
                    object : ICartHandler {
                        override fun itemAdded(count: Int) {
                            items[adapterPosition].prevQuantity = count
                        }

                        override fun itemRepeated() {

                        }

                        override fun failedToAdd(count: Int, view: AddQuantityView) {
                            view.quantityCount = (items[adapterPosition].prevQuantity)
                        }
                    }
                )

            }
        }

    }
}

*/
