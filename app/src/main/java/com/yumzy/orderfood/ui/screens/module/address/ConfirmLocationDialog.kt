/*
 * Created By Shriom Tripathi 29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import android.annotation.SuppressLint
import android.content.Context
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.LongLat
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogConfirmLocationBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.component.ConfirmLocationCardView
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*
import javax.inject.Inject

class ConfirmLocationDialog : BaseDialogFragment<DialogConfirmLocationBinding>(),
    OnMapReadyCallback, ConfirmLocationCardView.OnConfirmLocationClickListener {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
    }

    internal lateinit var mAction: LocationMapAction
    private lateinit var mMap: GoogleMap
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var mAddressId: String = ""
    private var mPrevious: LatLng = LatLng(0.0, 0.0)
    private var mCameraMoved = false
    protected var mMarker: Marker? = null

    val vm: ConfirmLocationViewModel by viewModel()

    override fun getLayoutId(): Int = R.layout.dialog_confirm_location

    override fun onDialogReady(view: View) {

        applyDebouchingClickListener(bd.btnBack, bd.btnMyLocation)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        mapFragment.getMapAsync(this)
        bd.cnfCardView.setOnConfirmLocationClickListener(this)
    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(context)

        mMap = googleMap ?: return

        //initializing marker
        mMarker = mMap.addMarker(
            MarkerOptions()
                .position(
                    LatLng(
                        mMap.cameraPosition.target.latitude,
                        mMap.cameraPosition.target.longitude
                    )
                )
                .icon(
                    bitmapDescriptorFromVector(
                        context ?: requireContext(),
                        R.drawable.ic_location_pin_my_loc
                    )
                )
                .draggable(false)
        )

        mMap.let {
            it.isMyLocationEnabled = false
            it.uiSettings.isZoomControlsEnabled = true
            it.uiSettings.isCompassEnabled = true
            it.setOnCameraIdleListener {
                //getting center position of map

                if (mPrevious.latitude != mMap.cameraPosition.target.latitude && mPrevious.longitude != mMap.cameraPosition.target.longitude) {
                    setStartLocation(
                        mMap.cameraPosition.target.latitude,
                        mMap.cameraPosition.target.longitude,
                        null
                    )
                    mPrevious = mMap.cameraPosition.target
                }
            }
            it.setOnCameraMoveListener {
                //updating marker when map being moved.
                mMarker?.position = LatLng(
                    mMap.cameraPosition.target.latitude,
                    mMap.cameraPosition.target.longitude
                )
            }

        }

        handleAction()
    }

    override fun onConfirmLocationClicked(address: com.yumzy.orderfood.data.models.AddressDTO) {
        when (mAction) {
            is LocationMapAction.MapActionUpdate -> {
                address.addressId = mAddressId
                vm.updateAddress(address).observe(this, { result ->
                    RHandler<Any>(this, result) {
                        actionHandler?.onSuccess(moduleId, address)
                        dialog?.dismiss()
                    }
                })
            }
            is LocationMapAction.MapActionAdd -> {
                vm.addAddress(address).observe(this, { result ->
                    RHandler<Any>(this, result) {
                        actionHandler?.onSuccess(moduleId, address)
                        dialog?.dismiss()
                    }
                })

            }
            is LocationMapAction.MapActionFetch -> {
                actionHandler?.onSuccess(moduleId, address)
                dialog?.dismiss()
            }
        }
    }

    private fun handleAction() {
//        ((((view as ViewGroup).getChildAt(0)as FrameLayout).getChildAt(0)as FrameLayout).getChildAt(2) as RelativeLayout).getChildAt(0).setMarginTop(100)
        when (mAction) {
            is LocationMapAction.MapActionUpdate -> {
                //if user wants to update or confirm existion location
                val address = (mAction as LocationMapAction.MapActionUpdate).mAddress
                mPrevious = LatLng(
                    address.longLat?.coordinates?.get(0)!!,
                    address.longLat?.coordinates?.get(1)!!
                )
                mAddressId = address.addressId
                setStartLocation(
                    address.longLat?.coordinates?.get(0)!!,
                    address.longLat?.coordinates?.get(1)!!,
                    address
                )
            }
            is LocationMapAction.MapActionAdd -> {
                //if user want to add new address get its furrent location
                getCurrentLocation()
            }
            is LocationMapAction.MapActionFetch -> {
                getCurrentLocation()
            }
        }

    }

    private fun getCurrentLocation() {
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (10 * 1000).toLong()
        locationRequest.fastestInterval = 2000
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        val locationSettingsRequest = builder.build()
        val result = LocationServices.getSettingsClient(context ?: requireContext())
            .checkLocationSettings(locationSettingsRequest)
        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                if (response!!.locationSettingsStates.isLocationPresent) {
                    getLastLocation()
                }
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        try {
                            val resolvable = exception as ResolvableApiException
                            resolvable.startResolutionForResult(
                                requireActivity(),
                                REQUEST_CHECK_SETTINGS
                            )
                        } catch (e: IntentSender.SendIntentException) {
                        } catch (e: ClassCastException) {
                        }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient.lastLocation.addOnSuccessListener { task ->
            if (task != null) {
                setStartLocation(task.latitude, task.longitude, null)
            } else {
                Handler().postDelayed({ getLastLocation() }, 500)
            }
        }
    }

    //Set the user’s starting point and add marker to it.
    private fun setStartLocation(
        lat: Double,
        lng: Double,
        addr: com.yumzy.orderfood.data.models.AddressDTO?
    ) {

        var addressTitle = getString(R.string.marker_title_you)
        var fullAddress = getString(R.string.lable_current_address)
        var googleAddress: Address? = null

        if (addr == null) {
            val gcd = Geocoder(context, Locale.getDefault())
            val addresses: List<Address>
            try {
                addresses = gcd.getFromLocation(lat, lng, 1)
                if (addresses.isNotEmpty()) {
                    addressTitle = addresses[0].featureName
                    fullAddress = addresses[0].getAddressLine(0)
                    googleAddress = addresses[0]
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            addressTitle = addr.name
            fullAddress = addr.fullAddress
        }

        //update marker info and position
        mMarker?.let {
            it.position = LatLng(lat, lng)
            it.title = addressTitle
            it.snippet = "Near $fullAddress"
        }

        val cameraPosition = CameraPosition.Builder()
            .target(LatLng(lat, lng))
            .zoom(17f)
            .build()

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        val address = com.yumzy.orderfood.data.models.AddressDTO()

        //if google address is not null this will get called
        googleAddress?.let { googleAddress ->
            address.apply {
                googleLocation = googleAddress.locality ?: ""
                val lonLat = LongLat()
                lonLat.coordinates = listOf(googleAddress.longitude, googleAddress.latitude)
                longLat = lonLat
                name = addressTitle
                this.fullAddress = fullAddress
                addressTag = "Home"
                landmark = ""
                houseNum = ""
            }
        }
        //if addr address is not null this will get called
        addr?.let { add ->
            address.apply {
                googleLocation = add.googleLocation
                longLat = add.longLat
                name = addressTitle
                this.fullAddress = fullAddress
                addressTag = add.addressTag
                landmark = add.landmark
                houseNum = add.houseNum
            }
        }

        bd.cnfCardView.setAddress(address)
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_back -> dismiss()
            R.id.btn_my_location -> getCurrentLocation()
        }
    }
}