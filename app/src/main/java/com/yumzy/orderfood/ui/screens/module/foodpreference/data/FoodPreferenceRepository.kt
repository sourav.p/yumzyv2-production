package com.yumzy.orderfood.ui.screens.module.foodpreference.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class FoodPreferenceRepository  constructor(
    private val remoteSource: FoodPreferenceRemoteDataSource
) {
    fun getlikedPrefTags() = networkLiveData(
        networkCall = {
            remoteSource.getlikedPrefTags()
        }
    ).distinctUntilChanged()

}