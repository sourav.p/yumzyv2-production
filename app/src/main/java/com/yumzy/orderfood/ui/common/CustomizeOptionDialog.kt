package com.yumzy.orderfood.ui.common

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterRegularFont

class   CustomizeOptionDialog : DialogFragment, DialogInterface.OnShowListener {
    lateinit var clContext: Context
    private var actionType = 2 // back presss or closed by user using dissmis rest are 0, 1
    private var positiveText: CharSequence? = "Ok"
    private var negativeText: CharSequence? = "Close"
    private var sMessage: CharSequence = ""
    var onActionPerformed: OnActionPerformed? = null

    constructor(
        clContext: Context,
        actionType: Int,
        sMessage: CharSequence,
        positiveText: CharSequence?,
        negativeText: CharSequence?,
        onActionPerformed: OnActionPerformed?
    ) {
        this.onActionPerformed = onActionPerformed
        this.sMessage = sMessage
        this.clContext = clContext
//        this.actionType = actionType
        this.positiveText = positiveText ?: "Ok"
        this.negativeText = negativeText ?: "Close"
    }

    constructor()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val clDialog = Dialog(requireContext(), R.style.DialogTheme)
        clDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        clDialog.setContentView(getDialogLayout())


        if (clDialog.window != null) {
            clDialog.window?.setWindowAnimations(R.style.DialogZoomInZoomOut)
            clDialog.window?.setBackgroundDrawableResource(R.drawable.white_rounded_corner)
            clDialog.setOnShowListener(this)
        }


        return clDialog
    }

    private fun getDialogLayout(): View {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setPadding(UIHelper.i10px, UIHelper.i10px, UIHelper.i10px, UIHelper.i10px)
        linearLayout.addView(getSubTitle())
        linearLayout.addView(getActionLayout())

        return linearLayout


    }

    private fun getActionLayout(): LinearLayout {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL or Gravity.END
//        val btnOk = UIHelper.filledButton(clContext, SpannableStringBuilder(positiveText).toString(), null)
        val btnOk = TextView(context)
        btnOk.text = positiveText
//        btnOk.minWidth = VUtil.dpToPx(75)
        btnOk.setTextColor(ThemeConstant.pinkies)
        btnOk.setInterBoldFont()
        btnOk.textSize = 14f
        btnOk.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
        btnOk.setOnClickListener {
            actionType = 0
            onActionPerformed?.onConfirmDone(this.dialog, actionType, positiveText.toString())
            dialog?.dismiss()
        }

        val btnClose = TextView(context)
        btnClose.text = negativeText
        btnClose.setTextColor(ThemeConstant.pinkies)
        btnClose.setInterBoldFont()
        btnOk.textSize = 14f
        btnClose.setOnClickListener {
            actionType = 1
            dialog?.dismiss()
        }

        val spaceView = View(clContext)
        spaceView.layoutParams = LinearLayout.LayoutParams(UIHelper.i20px, 0)

        linearLayout.addView(btnClose)
        linearLayout.addView(spaceView)
        linearLayout.addView(btnOk)
        return linearLayout

    }


    private fun getSubTitle(): TextView {
        val textView = TextView(clContext)
        textView.text = sMessage
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        textView.setTextColor(ThemeConstant.textGrayColor)
        textView.setInterRegularFont()
        textView.setPadding(UIHelper.i10px, UIHelper.i10px, UIHelper.i10px, UIHelper.i20px)
        return textView
    }


    override fun onResume() {
        super.onResume()
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    interface OnActionPerformed {
        fun onConfirmDone(dialog: Dialog?, actionType: Int, label: String?)
        fun onDismissed(actionType: Int, actionLabel: String?)
    }

    override fun onDismiss(dialog: DialogInterface) {
        onActionPerformed?.onDismissed(actionType, negativeText.toString())
        super.onDismiss(dialog)
    }

    override fun onShow(dialog: DialogInterface?) {

    }
}