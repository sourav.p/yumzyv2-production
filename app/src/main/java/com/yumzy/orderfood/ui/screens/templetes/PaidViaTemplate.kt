/*
 *   Created by Bhupendra Kumar Sahu
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.ui.helper.setInterRegularFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst


open class PaidViaTemplate(
    override val context: Context,
    val title: String = "Payment Summary",
    val vpa: String = "",
) : IDetailsActionTemplate {

    override var headerHeight: Int = VUtil.dpToPx(40)
    override val showClose = false
    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.gravity = Gravity.START
        layout.setPadding(12.px, 0, 0, VUtil.dpToPx(20))
        layout.addView(getTextView(R.id.tv_info_header, title))
        return layout
    }

    @SuppressLint("SetTextI18n")
    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        layout.minimumHeight = headerHeight


        val paidFrom = TextView(context)
        paidFrom.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        paidFrom.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        paidFrom.id = R.id.tv_paid_via
        paidFrom.text = vpa
        paidFrom.setInterRegularFont()
        paidFrom.setTextColor(Color.DKGRAY)
        layout.addView(paidFrom)


        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {


    }

    open fun getTextView(tvId: Int, msg: String): View? {
        val i5px = VUtil.dpToPx(5)
        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)
        textView.setInterFont()

        if (tvId == R.id.tv_info_header) {
            textView.setInterBoldFont()
            textView.gravity = Gravity.START or Gravity.CENTER_VERTICAL
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.BLACK)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        }

        return textView

    }

}

