package com.yumzy.orderfood.ui.screens.home.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.CartItemDTO
import com.yumzy.orderfood.databinding.AdapterRestaurentMenuBinding
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

/**
 * Created by Bhupendra Kumar Sahu.
 */
class CartMenuAdapter(
    val context: PlateFragment,
    var cartMenuListDTO: List<CartItemDTO>,
    private val clickedListener: (adapterPosition: Int, itemId: String?, count: Int, price: String, outlet: String) -> Unit,
    private val addAddonsclickedListener: (adapterPosition: Int, cartItemDTO: CartItemDTO) -> Unit
) :
    RecyclerView.Adapter<CartMenuAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {

        return cartMenuListDTO.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(cartMenuListDTO[position])
    }

    fun setItemQuantity(position: Int, value: Int) {
        if (cartMenuListDTO[position].quantity != 0) {
            cartMenuListDTO[position].quantity = value
            notifyItemChanged(position)
        }
    }

    fun setItemList(cartMenuListDTO: List<CartItemDTO>?) {
        if (cartMenuListDTO != null) {
            this.cartMenuListDTO = cartMenuListDTO
        }
        notifyDataSetChanged()
    }

    /*fun setData(availableItemList: ArrayList<CartItem>) {
        cartMenuList = availableItemList

        notifyDataSetChanged()

    }

    fun getData() = cartMenuList
    fun clearUnAvailableItem() {

        notifyDataSetChanged()

    }*/


    inner class ItemViewHolder(itemView: AdapterRestaurentMenuBinding) :
        RecyclerView.ViewHolder(itemView.root), AddItemView.AddItemListener {

        var adapterBinding: AdapterRestaurentMenuBinding = itemView


        fun bind(cartItemDTO: CartItemDTO) {
            if (cartItemDTO.isUnavailable) {
                cartItemDTO.itemPosition = adapterPosition
//                adapterBinding.addItemView.itemAvailable = false
                adapterBinding.addItemView.itemEnable = false
                adapterBinding.addItemView.showCustomizeClick = false

            } else {
                cartItemDTO.itemPosition = adapterPosition
                adapterBinding.addItemView.itemEnable = true
                adapterBinding.addItemView.showCustomizeClick = true

            }
            adapterBinding.addItemView.setTitles(cartItemDTO.name.capitalize(Locale.ROOT))

            if (cartItemDTO.addons.size != 0) {
                adapterBinding.addItemView.showCustomize = false
                adapterBinding.addItemView.showCustomizeClick = true
                if (!cartItemDTO.addons.isNullOrEmpty()) {
                    val selectedAddons = getAddonOptions(cartItemDTO)
                    adapterBinding.addItemView.setSubtitle(selectedAddons)
                }
            } else {
                adapterBinding.addItemView.showCustomize = false
                adapterBinding.addItemView.showCustomizeClick = false
                adapterBinding.addItemView.setSubtitle("")

            }
            if (cartItemDTO.variant != null) {
                if (cartItemDTO.variant.isVariant!!) {
                    adapterBinding.addItemView.showCustomize = false
                    adapterBinding.addItemView.showCustomizeClick = true
                    adapterBinding.addItemView.setTitles(cartItemDTO.name.capitalize(Locale.ROOT) + " (" + cartItemDTO.variant.optionsDTO.optionName + ")")

                } else {
                    adapterBinding.addItemView.showCustomize = false
                    adapterBinding.addItemView.showCustomizeClick = false
                    adapterBinding.addItemView.setTitles(cartItemDTO.name.capitalize(Locale.ROOT))

                }
            }


            if (cartItemDTO.billing.subTotal > cartItemDTO.billing.totalAmount) {

                val priced = YumUtil.getOfferItemPrice(
                    cartItemDTO.billing.subTotal,
                    cartItemDTO.billing.totalAmount
                )
                adapterBinding.addItemView.setItemSpannedPrice(priced)

            } else {
                adapterBinding.addItemView.setItemPrice(cartItemDTO.billing.totalAmount.withPrecision(2))
            }

            adapterBinding.addItemView.setItemPrice(cartItemDTO.billing.subTotal.withPrecision(2))

            adapterBinding.addItemView.itemId = cartItemDTO.itemID
            adapterBinding.addItemView.outletId = cartItemDTO.outletID
            adapterBinding.addItemView.setImageShow(false)
            adapterBinding.addItemView.setQuantity(cartItemDTO.quantity)
            adapterBinding.addItemView.isVeg = cartItemDTO.isVeg
            adapterBinding.addItemView.addItenListener = this
            adapterBinding.addItemView.getCustomizableText()!!.setOnClickListener {
                addAddonsclickedListener(
                    adapterPosition,
                    cartItemDTO
                )
            }


        }

        private fun getAddonOptions(cartItemDTO: CartItemDTO): String {
            return cartItemDTO.addons.joinToString(", ") { addon ->
                addon.options.joinToString(", ") { option -> option.optionName.capitalize(Locale.ROOT) }
            }
        }

        override fun onItemAdded(itemId: String, count: Int, price: String, outlet: String) {
            clickedListener(adapterPosition, itemId, count, price, outlet)
        }


    }


}
