package com.yumzy.orderfood.ui.screens.location

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.material.chip.Chip
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityLocationInfoBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import kotlinx.android.synthetic.main.dialog_rate_order.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import javax.inject.Inject

class LocationInfoActivity : BaseActivity<ActivityLocationInfoBinding, LocationViewModel>() {
    
    override val vm: LocationViewModel by viewModel()

    private var mAddress: AddressDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_location_info)


        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        mAddress = intent.getSerializableExtra(IConstants.ActivitiesFlags.ADDRESS) as AddressDTO
        setUpDetails()
        applyDebouchingClickListener(bd.btnSave, bd.tvChangeAddress)
    }

    private fun setUpDetails() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync { map ->

            map.uiSettings.isScrollGesturesEnabled = false
            map.uiSettings.isZoomGesturesEnabled = false
            map.uiSettings.isZoomControlsEnabled = false
            map.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom = false

            map.setOnCameraMoveStartedListener {
                bd.expandingMarker.progress = 0f
                bd.expandingMarker.setMinAndMaxProgress(0.0f, 0.65f)
                bd.expandingMarker.playAnimation()
            }

            map.setOnCameraIdleListener {
                bd.expandingMarker.progress = 0.65f
                bd.expandingMarker.setMinAndMaxProgress(0.65f, 1f)
                bd.expandingMarker.playAnimation()
            }

            try {
                map?.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        this@LocationInfoActivity,
                        R.raw.google_maps_style
                    )
                )
            } catch (e: Exception) {
                Timber.e(e)
            }


            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(
                        mAddress?.longLat?.coordinates?.get(1) ?: 17.3434,
                        mAddress?.longLat?.coordinates?.get(0) ?: 78.3434,
                    )
                ).zoom(17f).tilt(1f).build()

            map!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }

        bd.addressTag = mAddress?.addressTag
        bd.isShowAddressTagInput = false
        bd.addressType.isSingleSelection = true
        bd.addressType.check(R.id.chip_home)
        bd.addressType.setOnCheckedChangeListener { group, checkedId ->
            bd.isShowAddressTagInput = checkedId == R.id.chip_others
        }

        //set initial data
        bd.sublocality = mAddress?.googleLocation
        bd.fulladdress = mAddress?.fullAddress
        bd.houseNum = mAddress?.houseNum
        bd.landmark = mAddress?.landmark
        bd.addressType.check(
            if (mAddress?.addressTag?.toLowerCase() == "home") {
                R.id.chip_home
            } else if (mAddress?.addressTag?.toLowerCase() == "office") {
                R.id.chip_office
            } else {
                R.id.chip_others
            }
        )
        bd.addressTag = mAddress?.name

    }
    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    override fun onDebounceClick(view: View) {
        when (view.id) {
            bd.btnSave.id -> {
                val homeNo = bd.tvHouseno.text.toString()
                if (homeNo.trim().isEmpty()) {
                    bd.tvHouseno.setError("House No required")
                    return
                }

                var landmark = bd.tvLandmark.text.toString()
                if (landmark.isEmpty() == true) {
                    landmark = ""

                }

                var addressType =
                    findViewById<Chip>(bd.addressType.checkedChipId).text.toString().toLowerCase()

                if (bd.addressType.checkedChipId == R.id.chip_others && bd.tvAddressType.text.toString()
                        .trim()
                        .isEmpty()
                ) {
                    bd.tvAddressType.setError("Address tag Required")
                    return
                } else if (bd.addressType.checkedChipId == R.id.chip_others) {
                    addressType = bd.tvAddressType.text.toString()
                }

                mAddress?.apply {
                    this.houseNum = homeNo
                    this.landmark = landmark.trim()
                    this.addressTag =
                        if (addressType != "home" && addressType != "office") "others" else addressType
                    this.name = addressType
                }

                mAddress?.let { addOrUpdateAddress(view, it) }

            }
            bd.tvChangeAddress.id -> {
                val intent = Intent(this, LocationPickrActivity::class.java)
                intent.putExtra("address", mAddress)
                AppIntentUtil.launchClipRevealFromIntent(bd.tvChangeAddress, this, intent)
                finish()
            }
            else -> {
                super.onDebounceClick(view)
            }
        }
    }

    private fun addOrUpdateAddress(view: View, mAddress: AddressDTO) {
        if (mAddress.addressId.isEmpty()) {
            //add the address
            YumUtil.hideKeyboard(this)
            vm.addAddress(mAddress).observe(this, {
                RHandler<Map<String, String>>(this, it) {
//
                    mAddress.addressId = it.get("addressId").toString()
                    AppGlobalObjects.selectedAddress = mAddress
                    sharedPrefsHelper.setSelectedAddress(mAddress)
//                    startActivity(Intent(this, HomePageActivity::class.java))
                    AppIntentUtil.launchWithBaseAnimation(view, this, HomePageActivity::class.java)
                    finish()

                }
            })
        } else {
            //update the address
            vm.updateAddress(mAddress).observe(this, {
                RHandler<Any>(this, it) {
                    AppIntentUtil.launchWithBaseAnimation(view, this, HomePageActivity::class.java)
                    finish()
                }
            })
        }
    }

}