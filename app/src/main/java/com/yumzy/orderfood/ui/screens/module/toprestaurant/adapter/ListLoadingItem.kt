package com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter

import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.R
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.ui.utils.FastAdapterUIUtils

class ListLoadingItem : AbstractItem<ListLoadingItem.ViewHolder>() {

    override val type: Int
        get() = R.id.progress_item_id

    override val layoutRes: Int
        get() = R.layout.layout_progress_item

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        if (isEnabled) {
            holder.itemView.setBackgroundResource(FastAdapterUIUtils.getSelectableBackground(holder.itemView.context))
        }
    }

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var progressBar: ProgressBar =
            view.findViewById(com.mikepenz.fastadapter.ui.R.id.progress_bar)
    }
}
