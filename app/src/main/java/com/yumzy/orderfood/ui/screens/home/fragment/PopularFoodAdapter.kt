/*
 * Created By Shriom Tripathi 9 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.home.fragment

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.ui.component.PopularFoodItemView
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.OutletInteraction
import java.util.*


abstract class PopularFoodAdapter<T>(
    val context: Context,
    var items: ArrayList<T>,
    val outletInteraction: OutletInteraction
) :
    RecyclerView.Adapter<PopularFoodAdapter<T>.PopularFoodHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PopularFoodAdapter<T>.PopularFoodHolder {
        val popularView = PopularFoodItemView(context)
        return PopularFoodHolder(popularView)
    }

    override fun onBindViewHolder(holder: PopularFoodHolder, position: Int) {

        val itemAtPos = items.get(position) as HomeListDTO

        (holder.itemView as PopularFoodItemView).apply {
            setValue(
                itemAtPos.title.capitalize(Locale.getDefault()),
                itemAtPos.subTitle.capitalize(Locale.getDefault()),
                itemAtPos.meta?.isVeg ?: true,
                itemAtPos.meta?.price ?: 0.0,
                itemAtPos.image?.url ?: "",
                itemAtPos.outletId,
                itemAtPos.dishId,
                context.resources.getString(R.string.view_dish)
            )

            outletId = itemAtPos.outletId
            itemId = itemAtPos.dishId
        }

        holder.itemView.getAddButton()?.setOnClickListener {
            (context as HomePageActivity).let {
                it.cartHelper.addOrUpdateItemToCart(
                    it,
                    1,
                    RestaurantItem().apply {
                        outletId = itemAtPos.outletId
                        itemId = itemAtPos.dishId
                        price = itemAtPos.meta?.price ?: 0.0
                        name = itemAtPos.title
                    },
                    null,
                    true
                )
            }
        }

    }

    override fun getItemCount(): Int = items.size

    abstract fun onItemClick(view: View, item: T, bannerPosition: Int)

    inner class PopularFoodHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}