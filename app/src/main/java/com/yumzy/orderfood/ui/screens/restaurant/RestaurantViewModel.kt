package com.yumzy.orderfood.ui.screens.restaurant

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
//val restaurantAdons:
class RestaurantViewModel  constructor(
    private val repository: RestaurantRepository,
    sharedPrefsHelper: SharedPrefsHelper,
    private val cartHelper: CartHelper
) : BaseViewModel<IRestaurant>() {

    var restaurantDto: RestaurantDTO? = null

    val sectionList = MutableLiveData<List<RestaurantMenuSectionDTO>>()

    val menuItemPosition :HashMap<String, ArrayList<Int>> = hashMapOf()

    val vegCategoryList: HashMap<String, Int> = hashMapOf()
    val commonCategoryList: HashMap<String, Int> = hashMapOf()

    val menuItemMap :HashMap<String, Int> = hashMapOf()

    var cart = repository.getCart(sharedPrefsHelper.getCartId() ?: "") as MutableLiveData

    val restaurantDetail = MutableLiveData<RestaurantDetailDTO>()

    val categoryLiveData = MutableLiveData<List<RestaurantMenuCategoryDTO>>()

    val categoryDTOAll : MutableList<RestaurantMenuCategoryDTO> = mutableListOf()
    val categoryDTOVeg : MutableList<RestaurantMenuCategoryDTO> = mutableListOf()
    val categoryDTOEgg : MutableList<RestaurantMenuCategoryDTO> = mutableListOf()
    val categoryDTOBoth : MutableList<RestaurantMenuCategoryDTO> = mutableListOf()

    var addonsMap: Map<String, RestaurantAddOnDTO>? = mapOf()


    val isVeg = MutableLiveData<Boolean>(false)

    fun foodMenuDetails(outletId: String, userId: String) =
        repository.foodMenuDetails(outletId, userId)

    var outletId: String = ""
    var userId: String = ""

    fun addToCart(addCart: NewAddItemDTO) =
        repository.addToCart(addCart)

    fun itemCount(
        position: Int,
        count: Int,
        itemcount: ItemCountDTO
    ) = repository.itemCount(position, count, itemcount)

    fun addFav(addFav:  MutableMap<String, Any>) =
        repository.addFav(addFav)

    fun clearCart(cartId: String) = repository.clearCart(cartId)

    fun saveRestaurantToDatabase(restaurantDTO: RestaurantDTO) =
        repository.saveMenuToDataBase(restaurantDTO)

    fun getRestaurantDatabase() = repository.getRestaurant()


    fun getSelectedItem() = repository.getSelectedItem()
    fun clearPreviousSearchItemList() = repository.clearRestaurantSearchItemList()

}