package com.yumzy.orderfood.ui.component.chromtab

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.trusted.TrustedWebActivityIntentBuilder
import androidx.browser.trusted.splashscreens.SplashScreenParamKey
import androidx.browser.trusted.splashscreens.SplashScreenVersion
import androidx.core.content.ContextCompat
import com.google.androidbrowserhelper.trusted.TwaLauncher
import com.yumzy.orderfood.R

object ChromeTabHelper {
    //    private val topActivity: AppCompatActivity?
//        get() = ActivityUtils.getTopActivity() as AppCompatActivity
//    private var mSplashScreenStrategy: PwaWrapperSplashScreenStrategy? = null
//    private var mMetadata: LauncherActivityMetadata? = null
    fun launchChromeTab(context: AppCompatActivity, fullUrl: String) {
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(context, R.color.white))
        builder.addDefaultShareMenuItem()
        builder.enableUrlBarHiding()
        builder.setShowTitle(false)
        builder.build().launchUrl(context, Uri.parse(fullUrl))
    }

    @Deprecated("no more in use")
    fun launchUrl(context: AppCompatActivity, fullUrl: String) {
        val parseUrl = Uri.parse(fullUrl)
        val builder =
            TrustedWebActivityIntentBuilder(parseUrl)
//                .setNavigationBarColor(Color.RED)
                .setToolbarColor(Color.WHITE)
                .setSplashScreenParams(makeSplashScreenParamsBundle(context))

        val launcher = TwaLauncher(context)
        launcher.launch(builder, null, null, null)
    }

    private fun makeSplashScreenParamsBundle(context: AppCompatActivity): Bundle {
        /*   mMetadata = LauncherActivityMetadata.parse(context)

           if (mMetadata != null)
               mSplashScreenStrategy = PwaWrapperSplashScreenStrategy(
                   context,
                   mMetadata.splashImageDrawableId,
                   getColorCompat(mMetadata.splashScreenBackgroundColorId),
                   getSplashImageScaleType(),
                   getSplashImageTransformationMatrix(),
                   mMetadata.splashScreenFadeOutDurationMillis,
                   mMetadata.fileProviderAuthority
               )*/
        val bundle = Bundle()
        bundle.putString(SplashScreenParamKey.KEY_VERSION, SplashScreenVersion.V1)
        bundle.putInt(SplashScreenParamKey.KEY_FADE_OUT_DURATION_MS, 700)
        bundle.putInt(SplashScreenParamKey.KEY_BACKGROUND_COLOR, Color.WHITE)
//        bundle.putInt(SplashScreenParamKey.KEY_SCALE_TYPE, mScaleType.ordinal)
        /*     if (mTransformationMatrix != null) {
                 val values = FloatArray(9)
                 mTransformationMatrix.getValues(values)
                 bundle.putFloatArray(
                     SplashScreenParamKey.KEY_IMAGE_TRANSFORMATION_MATRIX,
                     values
                 )
             }*/
        return bundle
    }
    /*private fun getColorCompat(splashScreenBackgroundColorId: Int): Int {
        return ContextCompat.getColor(topActivity.baseContext, splashScreenBackgroundColorId)
    }*/
}