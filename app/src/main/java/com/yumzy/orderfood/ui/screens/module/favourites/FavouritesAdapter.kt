package com.yumzy.orderfood.ui.screens.module.favourites

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.FavOutlet
import com.yumzy.orderfood.data.models.FavRestaurantResDTO
import com.yumzy.orderfood.databinding.AdapterTopRestaurantBinding
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.JsonHelper
import com.yumzy.orderfood.util.viewutils.YumUtil

class FavouritesAdapter(
    context: Context,
    private val favRestaurantResDTO: FavRestaurantResDTO,
    private val clickListener: (adapterPosition: Int, itemId: String) -> Unit
) : RecyclerView.Adapter<FavouritesAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterTopRestaurantBinding =
            AdapterTopRestaurantBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return favRestaurantResDTO.outlets.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(favRestaurantResDTO.outlets[position])
    }

    inner class ItemViewHolder(itemView: AdapterTopRestaurantBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        var adapterBinding: AdapterTopRestaurantBinding = itemView
        fun bind(favOutlet: FavOutlet) {
            adapterBinding.imageUrl = favOutlet.imageUrl
            adapterBinding.title = favOutlet.itemName
            adapterBinding.subtitle = favOutlet.locality
            adapterBinding.cuisines = JsonHelper.arrayToString(favOutlet.cuisines)
            if (!favOutlet.offers.isNullOrEmpty()) {
                val zoneOffer = YumUtil.setZoneOfferString(favOutlet.offers)
                adapterBinding.offer = zoneOffer
            } else {
                adapterBinding.offer = ""
            }
            if (!favOutlet.costForTwo.isNullOrEmpty()) {
                adapterBinding.duration = "${favOutlet.costForTwo} for one"
            } else {
                adapterBinding.duration = ""

            }
            /*TODO Hanlde rating here*/
            /*if (favOutlet.rating.isNullOrEmpty()) {
                adapterBinding.rating = ""


            } else {
                adapterBinding.horizontalView.rating = favOutlet.rating.toString()

            }*/

//            if (favOutlet?.rating.isNullOrEmpty()) {
//                adapterBinding.horizontalView.rating = ""
//
//
//            } else {
//                adapterBinding.horizontalView.rating = favOutlet?.rating.toString()
//
//            }
//
//
//            if (favOutlet?.costForTwo.isNullOrEmpty()) {
//                adapterBinding.horizontalView.bottomText = ""
//
//
//            } else {
//                adapterBinding.horizontalView.bottomText =
//                    "COST FOR TWO - ₹" + favOutlet?.costForTwo.toString()
//
//            }
            adapterBinding.root.setClickScaleEffect()
            adapterBinding.root.setOnClickListener {
                clickListener(adapterPosition, favOutlet.itemId)
            }
        }

    }

}