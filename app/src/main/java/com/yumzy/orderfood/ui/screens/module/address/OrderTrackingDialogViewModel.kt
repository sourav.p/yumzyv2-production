/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.models.OrderStatusDTO
import com.yumzy.orderfood.data.models.RiderInfo
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import javax.inject.Inject

class OrderTrackingDialogViewModel  constructor(
    private val repository: OrderRepository
) : BaseViewModel<IView>() {

    var orderStatus = MutableLiveData<OrderStatusDTO>()

    var orderStatusDesctription = MutableLiveData<String>()
    var orderRiderInfo = MutableLiveData<RiderInfo>()

    fun orderTrackDetails(orderId: String) = repository.orderTrackDetails(orderId)

    fun getOrderStatus(orderId: String) = repository.getOrderStatus(orderId)



}