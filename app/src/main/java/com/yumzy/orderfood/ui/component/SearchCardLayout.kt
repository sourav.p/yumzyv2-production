package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil


/**
 *    Created by Sourav Kumar Pandit
 * */
class SearchCardLayout : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()
    }


    private lateinit var discountDrawable: Drawable
    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.text = field
        }
    var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_fotter)?.text = field

        }
    var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }

    var colorText: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_coupon_code)?.text = field
        }

    var bottomText: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_coupon_code)?.text = field
        }

    var imageUrl: String = ""
        set(value) {
            field = value
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(imageUrl).into(imageView)

            YUtils.setImageInView(imageView, imageUrl)

        }

    var imageSize: Int = 70
        set(value) {
            field = value
            val imageParam =
                LayoutParams(VUtil.dpToPx(field), 0)
            imageParam.topToTop = LayoutParams.PARENT_ID
            imageParam.startToStart = LayoutParams.PARENT_ID
            imageParam.dimensionRatio = "1:1"

            val roundImage = this.findViewById<RoundedImageView>(R.id.restaurant_image)
            roundImage.layoutParams = imageParam

        }
    var rating: String = ""
        set(value) {
            field = value
            val ratingView = this.findViewById<RattingCardView>(R.id.outlet_item_rating)
            ratingView.setRating(context.resources.getString(R.string.icon_star), value)
        }


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.HorizontalCardLayout,
                    defStyleAttr,
                    0
                )
            this.layoutParams =
                ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

            title = a.getString(R.styleable.HorizontalCardLayout_horizontalItemTitle) ?: ""
            subTitle = a.getString(R.styleable.HorizontalCardLayout_horizontalSubtitle) ?: ""
            colorText = a.getString(R.styleable.HorizontalCardLayout_horizontalColorText) ?: ""
            bottomText = a.getString(R.styleable.HorizontalCardLayout_horizontalBottomText) ?: ""
            discountDrawable =
                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!
            a.recycle()

        }
//        else {
//        }
        this.setClickScaleEffect()
        this.setOnClickListener {}
    }

    private fun initView() {

        this.layoutParams =
            ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
        val roundImage = restaurantImageView()

        val layout = LinearLayout(context)
        val linearParam = LayoutParams(0, 0)
        linearParam.endToEnd = LayoutParams.PARENT_ID
        linearParam.topToTop = LayoutParams.PARENT_ID
        linearParam.bottomToBottom = R.id.restaurant_image
        linearParam.startToEnd = R.id.restaurant_image
        layout.layoutParams = linearParam
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(VUtil.dpToPx(15), 0, 0, 0)


        layout.addView(getTextView(title, 14f, R.id.tv_title))
        layout.addView(getTextView(subTitle, 12f, R.id.tv_fotter))
        layout.addView(getTextView(bottomText, 12f, R.id.tv_coupon_code))

        val rattingView = getRattingView(context)

        this.addView(roundImage)
        this.addView(layout)
        this.addView(rattingView)

    }

    val rattingCard: RattingCardView? = this.findViewById(R.id.outlet_item_rating)

    private fun getRattingView(context: Context): View {

        val rattingView = RattingCardView(context)
        rattingView.id = R.id.outlet_item_rating
        val rattingParam = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        rattingParam.marginStart = VUtil.dpToPx(5)
        rattingParam.bottomMargin = VUtil.dpToPx(5)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam

        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            LayoutParams(VUtil.dpToPx(imageSize), 0)
        imageParam.topToTop = LayoutParams.PARENT_ID
        imageParam.startToStart = LayoutParams.PARENT_ID
        imageParam.dimensionRatio = "1:1"

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(8).toFloat()
        roundImage.setImageResource(YUtils.getPlaceHolder())
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    private fun getTextView(text: String, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {
            R.id.tv_title -> {
                textView.minLines = 1
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.setInterBoldFont()
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            R.id.tv_fotter -> {
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
                textView.minLines = 1
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f)
            }
            R.id.tv_coupon_code -> {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                textView.setTextColor(ThemeConstant.pinkies)
            }
            R.id.tv_time_price -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
            }
        }
        return textView

    }

    private fun spanDrawable(textSize: Float): Pair<SpannableString, Drawable> {
        val spannableString = SpannableString("@ $colorText")

        val iconWidth = VUtil.dpToPx(textSize.toInt() - 5)
        val iconHeight = VUtil.dpToPx(textSize.toInt())
        discountDrawable.setBounds(0, 0, iconWidth, iconHeight)
        return Pair(spannableString, discountDrawable)
    }

}