package com.yumzy.orderfood.ui.screens.module.appRating

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.RatingBar
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.DialogApplyRatingBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import kotlinx.android.synthetic.main.dialog_apply_coupon.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class RatingDialog(
    private val orderNum: String,
    private val orderID: String,
    private val outletName: String,
    private val items: String,
    private val moduleHandler: IModuleHandler?
) :
    BaseDialogFragment<DialogApplyRatingBinding>() {
    
    val ratingViewModel: RatingViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ratingViewModel.attachView(this)

        dialogParent.setOnClickListener {
            onDebounceClick(bd.dialogParent)
        }
        applyDebouchingClickListener(bd.ratingbar, bd.tvSkipView)
        bd.tvOrderNo.text = "Order #$orderNum"
        bd.tvRestaurantName.text = outletName
        bd.ratingbar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener()
        { ratingBar: RatingBar, fl: Float, b: Boolean ->
            dialog?.dismiss()
            context?.let {
                ActionModuleHandler.showReviewDialogView(
                    it,
                    "Rate Us",
                    ratingBar.rating.toString(),
                    orderNum,
                    orderID,
                    outletName,
                    items,
                    moduleHandler
                )
            }

        }
    }


    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.tv_skip_view -> dialog?.dismiss()
        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_apply_rating

    override fun onDismiss(dialog: DialogInterface) {
        ratingViewModel.detachView()
        super.onDismiss(dialog)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        moduleHandler?.onSuccess(actionId, data)
    }

    override fun onDialogReady(view: View) {
    }
}