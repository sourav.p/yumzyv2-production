package com.yumzy.orderfood.ui.common

import android.animation.Animator
import android.content.Context
import com.airbnb.lottie.LottieAnimationView

class YumLottieView : LottieAnimationView, Animator.AnimatorListener {
    private var imageUrl: String = ""
    var onLottieAnimationListener: OnLottieAnimationListener? = null

    private constructor(context: Context?) : super(context)
    constructor(context: Context?, imageUrl: String) : super(context) {
        this.imageUrl = imageUrl
        this.onLottieAnimationListener = null
        this.imageUrl
        initAnimation()

    }

    constructor(
        context: Context?,
        imageUrl: String,
        onLottieAnimationListener: OnLottieAnimationListener?
    ) : super(context) {
        this.imageUrl = imageUrl
        this.onLottieAnimationListener = onLottieAnimationListener
        initAnimation()

    }

    fun setRepeatMode() {
        repeatCount = 15

    }

    private fun initAnimation() {
        setAnimationFromUrl(imageUrl)
        addAnimatorListener(this)
        playAnimation()
    }

    fun close() {
        onLottieAnimationListener?.onAnimationEnd()
    }

    override fun onAnimationStart(animation: Animator) {}
    override fun onAnimationCancel(animation: Animator) {}
    override fun onAnimationRepeat(animation: Animator) {}
    override fun onAnimationEnd(animation: Animator) {
        onLottieAnimationListener?.onAnimationEnd()
    }

    interface OnLottieAnimationListener {
        fun onAnimationEnd()
    }
}