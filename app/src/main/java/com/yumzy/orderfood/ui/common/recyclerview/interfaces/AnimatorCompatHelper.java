package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

import android.view.View;

public final class AnimatorCompatHelper {
    private static final AnimatorProvider IMPL;

    static {

        IMPL = new HoneycombMr1AnimatorCompatProvider();


    }

    private AnimatorCompatHelper() {
    }

    public static ValueAnimatorCompat emptyValueAnimator() {
        return IMPL.emptyValueAnimator();
    }

    public static void clearInterpolator(View view) {
        IMPL.clearInterpolator(view);
    }
}
