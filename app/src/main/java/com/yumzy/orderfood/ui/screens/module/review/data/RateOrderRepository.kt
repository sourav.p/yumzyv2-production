package com.yumzy.orderfood.ui.screens.module.review.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.worker.singleNetoworkEmit
import javax.inject.Inject


/**
 * Created by Bhupendra Kumar Sahu.
 */
class RateOrderRepository  constructor(
    private val remoteSource: RateOrderRemoteDataSource
) {
    fun getRatingFeedback(selRatingFeedback: RatingFeedbackDTO) = singleNetoworkEmit(
        networkCall = {
            remoteSource.getRatingFeedback(selRatingFeedback)
        },
        handleResponse = { data, status ->
            when (status) {
                APIConstant.Status.SUCCESS -> {
                }
                APIConstant.Status.ERROR -> {

                }
            }
        }).distinctUntilChanged()


}