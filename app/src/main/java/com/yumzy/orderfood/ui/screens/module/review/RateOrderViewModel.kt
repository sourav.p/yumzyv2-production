package com.yumzy.orderfood.ui.screens.module.review

import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.review.data.RateOrderRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class RateOrderViewModel  constructor(private val repository: RateOrderRepository) :
    BaseViewModel<IView>() {

    fun getRatingFeedback(selRatingFeedback: RatingFeedbackDTO) =
        repository.getRatingFeedback(selRatingFeedback)
}