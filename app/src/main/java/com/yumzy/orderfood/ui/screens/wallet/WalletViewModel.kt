package com.yumzy.orderfood.ui.screens.wallet

import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class WalletViewModel  constructor(
    private val repository: ProfileRepository,
    private val sharedPrefHelper: SharedPrefsHelper
) : BaseViewModel<IWallet>() {

    fun getWalletTransactions(skip: Int) =
        repository.getWalletTransactions(skip, 10, sharedPrefHelper.getUserPhone().toLong())

    fun getUserWallet( ) =
        repository.getUserWallet(sharedPrefHelper.getUserPhone().toLong())

    fun verifyVPA(vpa: String) =
        repository.verifyVPA(vpa)

//    fun withdrawBalance(withdrae: HashMap<String, String>) =
//        repository.withdrawBalance(withdrae)
//
    fun withdrawBalance(withDraw: MutableMap<String, Any>) =
        repository.withdrawBalance(withDraw)

}