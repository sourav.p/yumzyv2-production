package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.ui.base.IView


interface ITempLoginView : IView {
    fun navStartedActivity()

}