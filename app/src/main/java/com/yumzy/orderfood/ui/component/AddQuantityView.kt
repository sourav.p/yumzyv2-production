package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.ui.pxf
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterRegularFont
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

/**
 *    Created by Sourav Kumar Pandit
 * */
//@Styleable("AddQuantityView")
class AddQuantityView : LinearLayout {
    private var onCounterChange: OnCounterChange? = null
    var buttonColor = 0xFFFE0076.toInt()
        set(value) {
            field = value
            val childAt0 = this.getChildAt(0)
            if (childAt0 != null)
                (childAt0 as FontIconView).setTextColor(buttonColor)
            val childAt2 = this.getChildAt(2)
            if (childAt2 != null)
                (this.getChildAt(2) as FontIconView).setTextColor(buttonColor)
        }

    private var addDrawable = DrawableUtils.getRoundDrawable(this.buttonColor, 8f)
        set(drawable) {
            field = drawable
            this.getChildAt(3)?.background = drawable
        }


    var quantityCount: Int = 0
        set(value) {
            /* if (value < 0) {
                 field = 0
                 return
             }*/
            if (field == value) return
            field = value
            toggleVisibility()
            val textView = getChildAt(1) as TextView
            textView.text = quantityCount.toString()
        }

    var ignoreClickCount = false
    var enableButton: Boolean = true
        set(value) {
            field = value
            ignoreClickCount = !value
            if (value) {
                changeButtonColor(ThemeConstant.greenAddItem)
            } else {
                changeButtonColor(ThemeConstant.mediumGray)
            }

        }

    private fun changeButtonColor(color: Int) {
        addDrawable = DrawableUtils.getRoundDrawable(color, 8f)
        buttonColor = color
        this.getChildAt(0).background = UIHelper.getStrokeDrawable(color, 3.pxf, 2.px)
        this.getChildAt(2).background = DrawableUtils.getRoundDrawable(color, 3.pxf)
        (this.getChildAt(2) as FontIconView).setTextColor(Color.WHITE)
    }

    //    private  var maxQuantity=0;
    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        if (atr != null) {
            val a =
                context.obtainStyledAttributes(
                    atr,
                    R.styleable.AddQuantityView,
                    style,
                    0
                )
            quantityCount = a.getInt(R.styleable.AddQuantityView_quantity, 0)
            buttonColor = a.getColor(R.styleable.AddQuantityView_buttonColor, 0xFFFE0076.toInt())
            addDrawable = DrawableUtils.getRoundDrawable(this.buttonColor, 8f)
            a.recycle()

        }

//        val l = LayoutTransition()
//        l.enableTransitionType(LayoutTransition.APPEARING)
//        this.layoutTransition = l
        initiate()
    }

    private fun initiate() {
        this.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
//        val minWidth =
        if (this.minimumWidth <= 50) {
            this.minimumWidth = VUtil.dpToPx(75)
        }
        if (this.minimumWidth <= 0) {
            this.minimumHeight = VUtil.dpToPx(28)
        }
        this.gravity = Gravity.CENTER
        this.addView(getMinusButton())
        this.addView(getTextView())
        this.addView(getPlusButton())
        this.addView(addQuantityButton())


        toggleVisibility()


    }

    private fun addQuantityButton(): View? {
        val addButton = TextView(context)
        addButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        addButton.setPadding(0, 5.px, 0, 5.px)
        addButton.text = context.resources.getString(R.string.text_add)
        addButton.layoutParams =
            LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)
        addButton.gravity = Gravity.CENTER
//        addButton.setPadding(15, 8, 15, 8)
//        addButton.setInterRegularFont()
//        addButton.gravity=Gravity.CENTER
        addButton.setTextColor(Color.WHITE)
        addButton.background = addDrawable
        addButton.setOnClickListener {
            if (ignoreClickCount) {
                this.onCounterChange?.onCounterChange(1)
                return@setOnClickListener
            }
            quantityCount++
            val textView = getChildAt(1) as TextView
            textView.text = quantityCount.toString()
            toggleVisibility()
            addButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            this.onCounterChange?.onCounterChange(quantityCount)
        }
        return addButton
    }

    private fun getTextView(): View? {

        val textView = TextView(context)
        textView.text = quantityCount.toString()
        textView.gravity = Gravity.CENTER
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
//        textView.typeface = Typeface.DEFAULT_BOLD
        textView.setInterRegularFont()

        textView.setTextColor(ThemeConstant.textDarkGrayColor)
        textView.layoutParams = LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)
        return textView
    }

    private fun getPlusButton(): View? {
        val button: FontIconView =
            viewButton(context.resources.getString(R.string.icon_more))
        button.background =
            DrawableUtils.getRoundDrawable(ThemeConstant.greenAddItem, 3.pxf)
//        val i25px = VUtil.dpToPx(35)
//        button.layoutParams = LayoutParams(i25px, i25px)
        button.setTextColor(ThemeConstant.white)

        button.setOnClickListener {
            quantityCount++
            val textView = getChildAt(1) as TextView
            textView.text = quantityCount.toString()
            this.onCounterChange?.onCounterChange(quantityCount)
        }
        return button
    }

    private fun getMinusButton(): View? {
        val button: FontIconView =
            viewButton(context.resources.getString(R.string.icon_minus_4))
        button.background = UIHelper.getStrokeDrawable(
            ThemeConstant.greenAddItem,
            3.pxf,
            UIHelper.i1px
        )
        button.setOnClickListener {
            quantityCount--
            if (quantityCount <= 0) {
                quantityCount = 0
                toggleVisibility()
                this.onCounterChange?.onCounterChange(quantityCount)
                return@setOnClickListener

            }
            this.onCounterChange?.onCounterChange(quantityCount)
            val textView = getChildAt(1) as TextView
            textView.text = quantityCount.toString()
        }
        return button
    }

    private fun viewButton(sLabel: String): FontIconView {
        val button = FontIconView(context)
        val i25px = VUtil.dpToPx(25)
        button.layoutParams = LayoutParams(i25px, i25px)
//        button.setShadowLayer(3f, 1f, 2f, Color.LTGRAY)
        button.setPadding(UIHelper.i3px, UIHelper.i5px, UIHelper.i3px, UIHelper.i3px)
        button.setTextColor(buttonColor)
        button.gravity = Gravity.CENTER
        button.minimumWidth = 0
        button.minimumHeight = 0
        button.minWidth = 0
        button.minHeight = 0
        button.text = sLabel

        button.textSize = 25f
//        button.background = buttonDrawable
        return button
    }

    private fun toggleVisibility() {
        if (quantityCount <= 0) {
            this.getChildAt(0).visibility = View.GONE
            this.getChildAt(1).visibility = View.GONE
            this.getChildAt(2).visibility = View.GONE
            this.getChildAt(3).visibility = View.VISIBLE
        } else {
            this.getChildAt(0).visibility = View.VISIBLE
            this.getChildAt(1).visibility = View.VISIBLE
            this.getChildAt(2).visibility = View.VISIBLE
            this.getChildAt(3).visibility = View.GONE
        }
    }

    fun setButtonText(buttonText: String) {
        val textView = this.getChildAt(3) as TextView
//        textView.setPadding(0,3.px,0,0)
        textView.text = buttonText

        /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
              textView.setAutoSizeTextTypeUniformWithConfiguration(
                  12, 14, 2, TypedValue.COMPLEX_UNIT_PX
              )
          } else
              TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
                  textView, 12, 14, 2,
                  TypedValue.COMPLEX_UNIT_PX
              )*/
    }

    fun setOnCounterChange(onCounterChange: OnCounterChange?) {
        this.onCounterChange = onCounterChange
        if (onCounterChange == null) {
            this.getChildAt(0).isEnabled = false
            this.getChildAt(1).isEnabled = false
            this.getChildAt(2).isEnabled = false
        } else {
            this.getChildAt(0).isEnabled = true
            this.getChildAt(1).isEnabled = true
            this.getChildAt(2).isEnabled = true

        }
    }

    interface OnCounterChange {
        fun onCounterChange(count: Int)

    }


}