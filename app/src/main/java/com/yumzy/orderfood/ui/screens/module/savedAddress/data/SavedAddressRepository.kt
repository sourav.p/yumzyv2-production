package com.yumzy.orderfood.ui.screens.module.savedAddress.data

import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class SavedAddressRepository  constructor(
    private val remoteSource: SavedAddressRemoteDataSource
)