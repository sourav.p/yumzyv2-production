package com.yumzy.orderfood.ui.screens.restaurant.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.RestaurantDao
import com.yumzy.orderfood.data.dao.RestaurantItemDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */

class RestaurantRepository  constructor(
    private val remoteSource: RestaurantRemoteDataSource,
    private val restaurantDao: RestaurantDao?,
    private val restItemDao: RestaurantItemDao?,
    private val selectedItemDao: SelectedItemDao?
) {
    fun foodMenuDetails(outletId: String, userId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.foodMenuDetails(outletId, userId)
        }
    ).distinctUntilChanged()

    fun outletMenuDetails(outletId: String, userId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.foodMenuDetails(outletId, userId)
        }
    ).distinctUntilChanged()

    fun addToCart(addCart: NewAddItemDTO) = networkLiveData<Any>(
        networkCall = {

            remoteSource.addToCart(addCart)
        }
    ).distinctUntilChanged()

    fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = networkLiveData<Any>(
        networkCall = {
            remoteSource.itemCount(position, count, itemCount)
        }
    ).distinctUntilChanged()

    fun addFav(addFav:  MutableMap<String, Any>) = networkLiveData<Any>(
        networkCall = {

            remoteSource.addFav(addFav)
        }
    ).distinctUntilChanged()

    fun getCart(
        cartId: String
    ) = networkLiveData<Any>(
        networkCall = {
            remoteSource.getCart(cartId)
        }
    )


    fun clearCart(cartId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.clearCart(cartId)
        }
    ).distinctUntilChanged()

    fun saveMenuToDataBase(restaurant: RestaurantDTO?) = backgroundLiveData<Boolean>(

        backgroundCallback = {
            if (restaurant != null) {
                restaurantDao?.updateOrInsert(restaurant)
                ResponseDTO.executed(data = true)
            } else
                ResponseDTO.executed(data = false)

        }
    )

    fun getRestaurant() = restaurantDao?.getRestaurant()
//    fun getUsersAndLibraries() = restaurantDao?.getUsersAndLibraries()
    fun searchForItems(query: String) = restItemDao?.getSearchDishItems(query)
    fun saveRestaurantItems(restaurantItem: ArrayList<RestaurantItem>) =
        backgroundLiveData<Boolean>(
            backgroundCallback = {
                if (restaurantItem != null) {
                    restItemDao?.insertAll(restaurantItem)
                    ResponseDTO.executed(data = true)
                } else
                    ResponseDTO.executed(data = false)

            }
        )

    fun addSelectedItem(valuePairSI: ValuePairSI) = backgroundLiveData<Boolean>(
        backgroundCallback = {
            run {
                selectedItemDao?.insertItem(valuePairSI)
                ResponseDTO.executed(data = true)
            }

        }
    )

    fun getSelectedItem() = selectedItemDao?.getSelectedItem()

    fun clearRestaurantSearchItemList() = backgroundLiveData(
        backgroundCallback = {
            restItemDao?.deleteAll()
            ResponseDTO.executed(data = true)
        }
    )

    fun addAllSelectedItem(items: List<ValuePairSI>) = backgroundLiveData (
        backgroundCallback = {
            run {
                selectedItemDao?.insertAll(items)
                ResponseDTO.executed(data = true)
            }
        }
    )

    fun clearSelectedItem() = backgroundLiveData(
        backgroundCallback = {
            selectedItemDao?.deleteAll()
            ResponseDTO.executed(data = true)
        }
    )

    fun clearRestaurantDTO() = backgroundLiveData(
        backgroundCallback = {
            restaurantDao?.removeRestaurantDTO()
            ResponseDTO.executed(data = true)
        }
    )


    /*
    backgroundLiveData(
        backgroundCallback = {
            if (itemId != null) {
                val itemQuantityById = selectedItemDao?.getItemQuantityById(itemId)
                ResponseDTO.executed(data = true)
            } else
                ResponseDTO.executed(data = false)

        }
    )*/


}

