/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.profile.IProfileView
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

class ConfirmLocationViewModel  constructor(private val repository: ProfileRepository) :
    BaseViewModel<IProfileView>() {


    fun addAddress(address: AddressDTO) = repository.addAddress(address)

    fun updateAddress(address: AddressDTO) = repository.updateAddress(address)


}