package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.laalsa.laalsalib.ui.VUtil.dpToPx
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.WalletDTO
import com.yumzy.orderfood.databinding.ViewWalletBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.ViewConst

/**
 * Created by Bhupendra Kumar Sahu on 08-Sep-20.
 */
class WalletView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var bd: ViewWalletBinding

    var isWalletSelected = false

    init {
        this.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        this.orientation = HORIZONTAL
        this.setClickScaleEffect()

        /*android:background='@{vm.mDiscountMode.equals(vm.Companion.WALLET_MODE) ? @drawable/discount_mode_active : @drawable/discount_mode_inactive}'*/
        bd = ViewWalletBinding.inflate(LayoutInflater.from(context), this, true)
        bd.isWalletSelected = isWalletSelected

        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.WalletView,
            defStyleAttr,
            0
        )
        a?.recycle()
    }

    fun setWalletDetails(wallet: WalletDTO?) {
        bd.wallet = wallet
        if (wallet?.availableBalance == 0.0) {
            this.background =
                DrawableUtils.getRoundDrawable(ThemeConstant.lightGray, dpToPx(2).toFloat())
        }
//        else
//        {
//            ShadowUtils.apply(
//                this,
//                ShadowUtils.Config().setShadowSize(UIHelper.i1px)
//                    .setShadowRadius(UIHelper.i10px.toFloat())
//                    .setShadowColor(ThemeConstant.alphaMediumGray)
//            )
//        }
    }

    fun setWalletSelected(isSelect: Boolean, deductAmount: Double = 0.0) {
        bd.isWalletSelected = isSelect
        bd.deductAmount = deductAmount
        isWalletSelected = isSelect

    }

}