/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */
package com.yumzy.orderfood.ui.screens.module.infodialog

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.dpf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.screens.templetes.InfoTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


class DetailActionDialog : DialogFragment(), DialogInterface.OnShowListener {
    var detailsTemplate: IDetailsActionTemplate? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (detailsTemplate == null) {
            detailsTemplate = InfoTemplate(requireContext(), 0, "Done", "", "")
        }
        val clDialog = Dialog(requireContext(), R.style.DialogTheme)
        clDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        clDialog.setContentView(getLayout())

        if (clDialog.window != null) {
            clDialog.window?.setWindowAnimations(R.style.DialogZoomInZoomOut)
            clDialog.window?.setBackgroundDrawableResource(R.drawable.white_rounded_corner)
            clDialog.setOnShowListener(this)
        }
        detailsTemplate?.showClose?.let {
            if (it) {
                this.isCancelable = false
                clDialog.setCancelable(false)
            }
        }
        return clDialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return getLayout()
    }


    private fun getLayout(): View {

        val container = ConstraintLayout(requireContext())
        container.layoutParams =
            ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        val layout = LinearLayout(requireContext())
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        detailsTemplate?.heading()?.let { layout.addView(it) }
        detailsTemplate?.body()?.let { layout.addView(it) }
        detailsTemplate?.actions()?.let { layout.addView(it) }

        layout.setPadding(padLR, padTB, padLR, padTB)
        layout.background = DrawableUtils.getRoundDrawable(dialogColor, roundRadius)
        container.addView(layout)
        detailsTemplate?.showClose?.let {
            if (it) {
                addCloseButton(container)
            }
        }


        return container
    }

    private fun addCloseButton(container: ConstraintLayout) {
        val closeIcon = FontIconView(requireContext())
        val closeParam = ConstraintLayout.LayoutParams(VUtil.dpToPx(30), VUtil.dpToPx(30))
        closeIcon.setTextColor(Color.GRAY)
        closeParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        closeParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        closeParam.marginEnd = VUtil.dpToPx(8)
        closeParam.topMargin = VUtil.dpToPx(8)
        closeIcon.layoutParams = closeParam
        closeIcon.setText(R.string.icon_close)
        closeIcon.setOnClickListener {
            dialog?.let { YumUtil.hideDialogKeyboard(it) }
            dialog?.dismiss()
        }
        container.addView(closeIcon)
    }

    var padTB = VUtil.dpToPx(15)
        set(value) {
            field = VUtil.dpToPx(value)
        }
    var padLR = VUtil.dpToPx(15)
        set(value) {
            field = VUtil.dpToPx(value)
        }
    var roundRadius: Float = 15.dpf
    var dialogColor = Color.WHITE
    override fun onShow(dialog: DialogInterface?) {

        detailsTemplate?.onDialogVisible(this.view, this.isVisible, dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        getDialog()?.let { YumUtil.hideDialogKeyboard(it) }
        super.onDismiss(dialog)

        detailsTemplate?.onDialogVisible(this.view, this.isVisible, dialog)

    }

}