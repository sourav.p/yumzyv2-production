package com.yumzy.orderfood.ui.screens.orderhistory.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.updateDatabase
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class OrderRepository  constructor(
    private val remoteSource: OrderRemoteDataSource,
    private val localDataSource: OrdersDao
) {

    val orderList = databaseLiveData { localDataSource.getOrders() }

    fun insertToDatabase(order: NewOrderDetailsDTO) = updateDatabase(
        databaseQuery = {
            localDataSource.insert(order)
        }
    )

    fun deleteFromDatabase(order: NewOrderDetailsDTO) = updateDatabase(
        databaseQuery = {
            localDataSource.delete(order)
        }
    )

    fun getOrderHistoryList(skip: Int) = networkLiveData(
        networkCall = {
            remoteSource.getOrderHistoryList(skip)
        }
    ).distinctUntilChanged()


    fun getOrderStatus(orderId: String) = networkLiveData(
        networkCall = {
            remoteSource.getOrderStatus(orderId)
        }
    ).distinctUntilChanged()

    fun getOrderDetails(orderId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.orderDetails(orderId)
        }
    ).distinctUntilChanged()

    fun orderTrackDetails(orderId: String) = networkLiveData<Any>(
        networkCall = {

            remoteSource.orderTrackDetails(orderId)
        }
    ).distinctUntilChanged()


}