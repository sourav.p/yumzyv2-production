package com.yumzy.orderfood.ui.screens.restaurant

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.yumzy.orderfood.data.models.RestaurantItem

class MenuItemDiff(private val oldList: List<RestaurantItem>, private val newList: List<RestaurantItem>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList[newItemPosition]
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition].prevQuantity == newList[newPosition].prevQuantity
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}