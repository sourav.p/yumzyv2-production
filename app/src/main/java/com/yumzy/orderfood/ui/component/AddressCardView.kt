package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

/**
 *    Created by Sourav Kumar Pandit
 * */
class AddressCardView : LinearLayout, View.OnClickListener {


    private var iconSize: Float = VUtil.dpToPx(45).toFloat()
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.textSize = field
        }
    private var iconColor: Int = Color.GRAY
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.setTextColor(field)
        }
    private var icon: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.text = field
        }
    private var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_fotter)?.text = field

        }
    private var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }
    private lateinit var addressListener: AddressListener


    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()


    }

    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.AddressCardView,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.AddressCardView_addressTitle) ?: ""
            icon = a.getString(R.styleable.AddressCardView_addressIcon) ?: ""
            subTitle = a.getString(R.styleable.AddressCardView_addressSubtitle) ?: ""
            iconColor = a.getColor(R.styleable.AddressCardView_addressIconColor, Color.GRAY)
            iconSize = a.getDimension(
                R.styleable.AddressCardView_addressIconSize,
                VUtil.dpToPx(45).toFloat()
            )
            a.recycle()

        }
    }


    private fun initView() {
        this.gravity = Gravity.CENTER_VERTICAL
        val iconView = FontIconView(context)
        val iconParam = LayoutParams(VUtil.dpToPx(30), VUtil.dpToPx(30))
        val i8px = VUtil.dpToPx(8)
        val i10px = VUtil.dpToPx(10)
        iconParam.setMargins(i8px, 0, i8px, i10px)
        iconView.layoutParams = iconParam
        iconView.id = R.id.font_icon_view
        iconView.setTextColor(iconColor)
        iconView.gravity = Gravity.TOP
        iconView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50f)
        iconView.text = icon
//        iconView.setOnClickListener(this)

        val layout = LinearLayout(context)
        layout.layoutParams =
            LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layout.orientation = VERTICAL
        layout.addView(getTextView(title, 16f, 1, R.id.tv_title))
        layout.addView(getTextView(subTitle, 12f, 2, R.id.tv_fotter))

        this.addView(iconView)
        this.addView(layout)
    }

    private fun getTextView(text: String, textSize: Float, minLine: Int, tvId: Int): View? {
        val textView = TextView(context)
        textView.setInterFont()
        val i8px = VUtil.dpToPx(8)
        val i5px = VUtil.dpToPx(5)
        if (tvId == R.id.tv_title) {
            textView.setPadding(i8px, i5px, i8px, i5px)
            textView.setInterBoldFont()
            textView.setTextColor(ThemeConstant.textBlackColor)
        } else if (tvId == R.id.tv_fotter) {
            textView.setPadding(i8px, 0, i8px, i5px)
            textView.setTextColor(ThemeConstant.textDarkGrayColor)
            textView.ellipsize = TextUtils.TruncateAt.END
        }
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.minLines = minLine
        textView.maxLines = minLine
        textView.id = tvId
//        textView.setOnClickListener(this)
        return textView

    }

    fun getFontIconView(): FontIconView {
        return this.findViewById<FontIconView>(R.id.font_icon_view)
    }

    fun getTitleView(): TextView {
        return this.findViewById<TextView>(R.id.tv_title)
    }

    fun getSubTitleView(): TextView {
        return this.findViewById<TextView>(R.id.tv_fotter)
    }

    override fun onClick(view: View) {
//        when (view.id) {
//            R.id.iv_rounded_image -> this.addressListener.onAddressImageClick()
//            R.id.tv_title -> this.addressListener.onAddressImageClick()
//            R.id.tv_subtitle -> this.addressListener.onAddressTitleClick(title)
//            else -> this.addressListener.onAddressClick()
//        }
    }

    //    fun setAddressClickListener(addressListener: AddressListener) {
//        this.addressListener = addressListener
//    }
    interface AddressListener {
//        fun onAddressImageClick()
//        fun onAddressTitleClick(value: String?)
//        fun onAddressClick()
    }
}