package com.yumzy.orderfood.ui.screens.restaurant.adapter

/*

class OutletMenuAdapter(
    private val context: Context? = null,
    private val interaction: OutletInteraction? = null
) : RecyclerView.Adapter<OutletMenuAdapter.RestaurantItemHolder>() {

    private val menuItems: ArrayList<RestaurantItem> = ArrayList()
    private var anyItem: List<RestaurantItem> = ArrayList()

    //    private var vegItem: ArrayList<RestaurantItem> = ArrayList()
    private val selectedItem: ArrayList<ValuePairSI> = ArrayList()

    fun setListData(itemList: List<RestaurantItem>) {
        menuItems.clear()
        menuItems.addAll(itemList)
//        if (itemList)
        anyItem = ArrayList(itemList)
        interaction?.adapterSizeChange(menuItems.size)
        notifyDataSetChanged()
    }

    fun setVegOnly() {
//        this.vegEnable = vegEnable
        (context as OutletActivity).let {

            it.vm.viewModelScope.launch(Dispatchers.Main) {
//                it.vm.view?.showProgressBar()

                it.vm.isVeg.observe(it, Observer { value ->
                    val listItem = ArrayList<RestaurantItem>()
                    if (value)
                        listItem.addAll(getVegList(menuItems))
                    else
                        listItem.addAll(menuItems)
                    updateList(listItem)
                })
//                it.vm.view?.hideProgressBar()


            }
        }
    }

    fun getVegList(
        itemList: List<RestaurantItem>
    ): List<RestaurantItem> {
        val vegList = ArrayList<RestaurantItem>()

        itemList.forEach { menuItem ->
            if (menuItem.isVeg) {
                vegList.add(menuItem)
            }
        }

        return vegList
    }

    fun updateList(newList: List<RestaurantItem>?) {
        val diffResult = DiffUtil.calculateDiff(MyDiffCallback(newList, this.anyItem), true)
        diffResult.dispatchUpdatesTo(this)
        anyItem = newList ?: listOf()
//        notifyDataSetChanged()

    }


    fun setSelectedItemData(itemList: List<ValuePairSI>) {
        selectedItem.clear()
        selectedItem.addAll(itemList)
        for((index, item) in menuItems.withIndex()) {
            selectedItem.forEach { selectedItem ->
                if (selectedItem.id == item.itemId) {
                    if(selectedItem.value != item.prevQuantity ) {
                        menuItems[index].prevQuantity = selectedItem.value
                        notifyItemChanged(index)
                        return@forEach
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantItemHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)
        return RestaurantItemHolder(itemBinding, interaction)
    }

    override fun getItemCount() = anyItem.size

    override fun onBindViewHolder(holder: RestaurantItemHolder, position: Int) =
        holder.bind(anyItem[position])


    inner class RestaurantItemHolder constructor(
        val adapterBinding: AdapterRestaurentMenuBinding,
        private val interaction: OutletInteraction?
    ) :
        RecyclerView.ViewHolder(adapterBinding.root) {

        private lateinit var restaurantItem: RestaurantItem

        fun bind(item: RestaurantItem?) = with(itemView) {

            setOnClickListener {
                interaction?.onItemSelected(adapterPosition, restaurantItem)
            }

            restaurantItem = item!!
            adapterBinding.addItemView.setItemOfferPrice(item.price.toString())
            adapterBinding.addItemView.setTitles(item.name.capitalize())
            adapterBinding.addItemView.setSubtitle(item.description)
            adapterBinding.addItemView.itemId = item.itemId.toString()
            adapterBinding.addItemView.setImageShow(false)
            adapterBinding.addItemView.ribbonText = item.ribbon
            adapterBinding.addItemView.isVeg = item.isVeg
            adapterBinding.addItemView.showCustomize = !item.addons.isEmpty()
            adapterBinding.addItemView.showItemAdding(item.isItemAdding)
*/
/*
            var itemQuantity = 0
            selectedItem.run {
                this.forEach {
                    if (it.id == item.itemId) {
                        itemQuantity = it.value
                        return@forEach
                    }
                }
            }*//*

            adapterBinding.addItemView.getQuantityView()?.quantityCount = item.prevQuantity

            adapterBinding.addItemView.addItenListener =
                object : AddItemView.AddItemListener, ICartHandler {

                    override fun onItemAdded(
                        itemId: String,
                        count: Int,
                        priced: String,
                        outletId: String
                    ) {

                        if (adapterPosition >= 0) {
                            menuItems[adapterPosition].isItemAdding = true
                            adapterBinding.addItemView.showItemAdding(true)
                            interaction?.onItemCountChange(
                                this,
                                count,
                                restaurantItem,
                                adapterBinding.addItemView.getQuantityView()!!
                            )
                        }

                    }

                    override fun itemAdded(count: Int) {
                        if (adapterPosition >= 0) {
                            menuItems[adapterPosition].isItemAdding = false
                            adapterBinding.addItemView.showItemAdding(false)

                        }
                    }

                    override fun itemRepeated() {
                    }

                    override fun failedToAdd(count: Int, view: AddQuantityView) {
                        if (adapterPosition >= 0) {
                            menuItems[adapterPosition].isItemAdding = false
                            adapterBinding.addItemView.showItemAdding(false)
                            adapterBinding.addItemView.setQuantity(menuItems[adapterPosition].prevQuantity)
                        }
                    }
                }
        }
    }

}
*/
