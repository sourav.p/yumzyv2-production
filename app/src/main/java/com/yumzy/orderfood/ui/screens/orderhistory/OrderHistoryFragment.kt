package com.yumzy.orderfood.ui.screens.orderhistory

import android.content.Intent
import android.view.*
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Order
import com.yumzy.orderfood.data.models.OrderHistoryDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentOrderHistoryBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter.ListLoadingItem
import com.yumzy.orderfood.ui.screens.reorder.ReorderActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.StickyHeaderAdapter
import com.yumzy.orderfood.util.IConstants
import com.zoho.livechat.android.ZohoLiveChat
import com.zoho.salesiqembed.ZohoSalesIQ
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class OrderHistoryFragment : BaseFragment<FragmentOrderHistoryBinding, OrderHistoryViewModel>(),
    IOrderHistory, IModuleHandler, OrderHistoryItemListener {

    private var listReachedEnd: Boolean = false

    private var skip: Int = 0
    var itemLoading = false

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener

    override val vm: OrderHistoryViewModel by viewModel()

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_order_history
    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        setupPagingRecyclerView()
        requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav_view).visibility =
            View.GONE

        NavigationUI.setupWithNavController(bd.mainToolbar, findNavController())


    }

    private fun addToolbarListener(toolbar: Toolbar) {
        toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.chat_zoho -> {
                    //   findNavController().navigate(R.id.nav_notification)
                    true
                }
                else -> false
            }
        }
    }

    private fun setupPagingRecyclerView() {

        val stickyHeaderAdapter = StickyHeaderAdapter<GenericItem>()
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        footerAdapter = ItemAdapter.items()
        mFastAdapter.addAdapter(1, footerAdapter)

        bd.rvOrderHistory.layoutManager = LinearLayoutManager(requireContext())
        bd.rvOrderHistory.itemAnimator = DefaultItemAnimator()
        bd.rvOrderHistory.adapter = stickyHeaderAdapter.wrap(mFastAdapter)

        val decoration = StickyRecyclerHeadersDecoration(stickyHeaderAdapter)
        bd.rvOrderHistory.addItemDecoration(decoration)

        //so the headers are aware of changes
        mFastAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                decoration.invalidateHeaders()
            }
        })

        mFastAdapter.attachDefaultListeners = true


        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = ListLoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                fetchOrderHistoryList()
            }
        }

        bd.rvOrderHistory.addOnScrollListener(endlessRecyclerOnScrollListener)
        fetchOrderHistoryList()
    }

    override fun fetchOrderHistoryList() {
        if (listReachedEnd) {
            footerAdapter.clear()
            val progressItem = ListLoadingItem()
            progressItem.isEnabled = false
            footerAdapter.add(progressItem)
            return
        }
        vm.getOrderHistoryList(skip).observe(this,
            Observer { result ->
                if (result.type == -1)
                    return@Observer
                itemLoading = true
                RHandler<OrderHistoryDTO>(
                    this,
                    result
                ) { orderHistoryResDto ->

                    footerAdapter.clear()
                    if (orderHistoryResDto.orders.isNullOrEmpty()) {
                        listReachedEnd = true
                        vm.getOrderHistoryList(skip)
                            .removeObservers(this)
                    }

                    val items = ArrayList<OrderHistoryItem>()
                    orderHistoryResDto.orders.forEach { order ->

                        val orderHistoryItem =
                            OrderHistoryItem().withContext(context = OrderHistoryFragment())
                                .withOrder(order).withListener(this)
                        items.add(orderHistoryItem)
                    }


                    mFastAdapter.add(items)
                    skip++
                    if (skip == 1 && result.data?.orders.isNullOrEmpty()) {
                        bd.noDataToDisplay.visibility = View.VISIBLE
                        bd.rvOrderHistory.visibility = View.GONE
                        //  bd.tvInfo.visibility = View.GONE
                        bd.mainToolbar.menu.findItem(R.id.chat_zoho).isVisible = false

                        //bd.noDataLayout.addView(UIHelper.showNoDataToDisplay(requireContext = this))


                    } else {
                        bd.noDataToDisplay.visibility = View.GONE
                        bd.rvOrderHistory.visibility = View.VISIBLE
                        //   bd.tvInfo.visibility = View.GONE
                        bd.mainToolbar.menu.findItem(R.id.chat_zoho).isVisible = true

                    }

                }

            })

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.chat_zoho) {
            CleverTapHelper.pushEvent(CleverEvents.support_chat_requested)
            zohoSetupVisitor()
            ZohoLiveChat.Chat.open()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun zohoSetupVisitor() {
//        ZohoSalesIQ.registerVisitor(sharedPrefsHelper.getUserPhone())
        ZohoSalesIQ.Visitor.setName(sharedPrefsHelper.getUserName())
        ZohoSalesIQ.Visitor.setEmail(sharedPrefsHelper.getUserEmail())
        ZohoSalesIQ.Visitor.setContactNumber(sharedPrefsHelper.getUserPhone())
//        ZohoSalesIQ.Visitor.addInfo("User Type","Premium"); //Custom visitor information
    }

    override fun getOrderID(orderID: String, position: Int) {
        val intent = Intent(requireActivity(), ReorderActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.FLAG, orderID)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)

    }

    override fun onSuccess(actionId: Int, data: Any?) {
        if (actionId == IConstants.ActionModule.APPLY_RATING && data != null) {
//            setupPagingRecyclerView()
            (data as Map<String, String>)
            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val item = mFastAdapter.getItem(i)
                when (item) {
                    is OrderHistoryItem -> {
                        if (item.order?.orderId == data["orderId"]) {
                            item.order?.rated = true
                            item.order?.rating = data["rating"]?.toDouble() ?: 0.0
                            val position = mFastAdapter.getPosition(item)
                            mFastAdapter.notifyAdapterItemChanged(position)
                            break@loop
                        }
                    }
                }
            }
            AppUIController.showThankYouRate()
        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.rvOrderHistory.visibility = View.GONE
            //  bd.tvInfo.visibility = View.GONE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet


        }
    }

    override fun actionReorder(outletId: String) {
        val intent = Intent(requireActivity(), OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
    }

    override fun actionTrack(orderId: String) {
        ActionModuleHandler.showOrderTracking(requireContext(), orderId)
    }

    override fun actionRate(order: Order) {
        ActionModuleHandler.rateOrder(
            requireContext(),
            order.orderId,
            order.orderNum,
            order.outletName,
            order.items,
            this
        )
    }

    override fun actionOrderDetails(orderId: String) {

        val intent = Intent(requireContext(), ReorderActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.FLAG, orderId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
    }
}


interface OrderHistoryItemListener {
    fun actionReorder(outletId: String)
    fun actionTrack(orderId: String)
    fun actionRate(order: Order)
    fun actionOrderDetails(orderId: String)
}


