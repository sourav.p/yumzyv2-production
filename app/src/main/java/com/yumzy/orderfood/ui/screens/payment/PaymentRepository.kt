package com.yumzy.orderfood.ui.screens.payment

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import com.yumzy.orderfood.data.models.PreCheckoutReqDTO
import com.yumzy.orderfood.ui.screens.home.fragment.PlateRemoteDataSource
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.resultLiveData
import javax.inject.Inject

class PaymentRepository  constructor(
    private val remoteDataSource: PaymentRemoteDataSource,
    private val localDataSource: OrdersDao
    ) {

    fun newOrder(newOrder: NewOrderReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.newOrder(newOrder)
        }
    ).distinctUntilChanged()

    fun postCheckout(orderId: String) = networkLiveData<Any>(
        networkCall = {
            remoteDataSource.postCheckout(orderId)
        }
    )

    fun getOrderDetails(orderId: String) = resultLiveData (
        databaseQuery = { localDataSource.getOrder(orderId) },
        networkCall = { remoteDataSource.orderTrackDetails(orderId) },
        saveCallResult = { localDataSource.insert(it) }
    ).distinctUntilChanged()

}