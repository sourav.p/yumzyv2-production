package com.yumzy.orderfood.ui.component

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ListPopupWindow
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.pxf


class CustomSearchView : LinearLayout, TextWatcher {

    private lateinit var mPopupWindow: ListPopupWindow
    private lateinit var mSearchAdapter: SearchAdapter<Any>
    private var mOnSearchListener: OnSearchListener? = null

    //    private val i80px = VUtil.dpToPx(80)
    private val i25px = VUtil.dpToPx(25)
    private val i20px = VUtil.dpToPx(20)

    //    private val i30px = VUtil.dpToPx(30)
    private val i15px = VUtil.dpToPx(15)

    //    private val i12px = VUtil.spToPx(12f)
    private val i10px = VUtil.dpToPx(10)
    private val i8px = VUtil.dpToPx(8)

    private var mShowCloseIcon = false
        set(value) {
            if (value) {
                this.findViewById<FontIconView>(IConstants.SearchModule.CLOSE_ICON).visibility =
                    View.VISIBLE
            } else {
                this.findViewById<FontIconView>(IConstants.SearchModule.CLOSE_ICON).visibility =
                    View.GONE
            }
            field = value
        }

    private var mQuery: String = ""
        set(value) {
            if (value.trim().isNotEmpty()) {
                mShowCloseIcon = true
                mOnSearchListener?.onQueryChange(value)
            } else {
                mShowCloseIcon = false
            }
            field = value
        }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

//        params.setMargins(i10px, i10px, i10px, i10px)

        this.layoutParams = params
        this.orientation = HORIZONTAL
        this.gravity = Gravity.CENTER_VERTICAL
        this.background =
            DrawableUtils.getRoundDrawable(ThemeConstant.lightAlphaGray,10.pxf)

        init()
    }

    private fun init() {
        //add search icon
        this.addView(getSearchIcon())
        this.addView(getTextInputView())
        this.addView(getCloseIcon())
    }

    private fun getSearchIcon(): FontIconView? {

        val fontIconView = FontIconView(context)
        fontIconView.id = IConstants.SearchModule.SEARCH_ICON
        fontIconView.gravity = Gravity.CENTER_VERTICAL
        fontIconView.text = context.resources.getString(R.string.icon_search_1)
        fontIconView.setTextColor(ThemeConstant.textGrayColor)

        val params = LayoutParams(i25px, i25px)
        params.setMargins(i20px, i10px, i10px, i10px)
        fontIconView.layoutParams = params

        return fontIconView
    }

    private fun getTextInputView(): EditText? {

        val editText = EditText(context)
        editText.id = IConstants.SearchModule.SEARCH_INPUT_FIELD
        editText.hint = context.resources.getString(R.string.search)
        editText.setBackgroundColor(ThemeConstant.transparent)
        editText.ellipsize = TextUtils.TruncateAt.END
        editText.isFocusableInTouchMode = true
        editText.setInterFont()
        editText.imeOptions = EditorInfo.IME_ACTION_SEARCH
        editText.isSingleLine = true
        editText.setTextColor(ThemeConstant.textGrayColor)

        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)

        val params = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)
        params.setMargins(i8px, i8px, i8px, i8px)

        editText.layoutParams = params
        editText.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (this@CustomSearchView::mPopupWindow.isInitialized)
                    mPopupWindow.show()
            } else {
                if (this@CustomSearchView::mPopupWindow.isInitialized)
                    mPopupWindow.dismiss()
            }
        }
        editText.addTextChangedListener(this)
        return editText
    }

    private fun getCloseIcon(): FontIconView? {

        val fontIconView = FontIconView(context)
        fontIconView.id = IConstants.SearchModule.CLOSE_ICON
        fontIconView.gravity = Gravity.CENTER_VERTICAL
        fontIconView.text = context.resources.getString(R.string.icon_close)
        fontIconView.setTextColor(ThemeConstant.transparent)

        val params = LayoutParams(i25px, i25px)
        params.setMargins(i10px, i10px, i20px, i10px)

        fontIconView.layoutParams = params
        fontIconView.visibility = View.GONE
        fontIconView.setOnClickListener {
            this.findViewById<EditText>(IConstants.SearchModule.SEARCH_INPUT_FIELD).setText("")
            if (this@CustomSearchView::mPopupWindow.isInitialized) mPopupWindow.dismiss()

        }

        return fontIconView
    }

    fun <T, V : View> setupPop(
        onBind: (value: T) -> V
    ) {
        mSearchAdapter = object : SearchAdapter<Any>(context, emptyList()) {
            override fun getItemView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val value = getItem(position) as T
                val view = onBind(value)

                val i10px = VUtil.dpToPx(10)
                view.setPadding(i10px, i10px, i10px, i10px)

                view.setClickScaleEffect()
                view.setOnClickListener {
                    mOnSearchListener?.onItemSelected(value)
                    mPopupWindow.dismiss()
                }
                return view
            }
        }

        //init popup
        mPopupWindow = ListPopupWindow(context)
        mPopupWindow.anchorView = this

        //set width and height
        mPopupWindow.width = (VUtil.screenWidth * 0.85).toInt()
        mPopupWindow.height = LayoutParams.WRAP_CONTENT
        mPopupWindow.verticalOffset = i15px

        //set popup background
        mPopupWindow.setBackgroundDrawable(
            DrawableUtils.getRoundDrawable(
                ThemeConstant.textGrayColor,
                10.pxf)

        )

        //set adapter to popup list
        mPopupWindow.setAdapter(mSearchAdapter)

        //set dissmiss listener
        mPopupWindow.setOnDismissListener {
            mSearchAdapter.updateList(emptyList())
            this@CustomSearchView.findViewById<EditText>(IConstants.SearchModule.SEARCH_INPUT_FIELD)
                .clearFocus()
        }

    }

    fun setOnSearchListener(onSearchListener: OnSearchListener) {
        mOnSearchListener = onSearchListener
    }


    fun updateSearchList(list: List<Any>) {
        mSearchAdapter.updateList(list)
    }


    override fun afterTextChanged(s: Editable?) {
        mQuery = s.toString()
        if (s.toString().trim().isNotEmpty()) {
            mQuery = s.toString()
            mOnSearchListener?.onQueryChange(mQuery)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}


    abstract inner class SearchAdapter<T>(
        context: Context,
        var items: List<T>
    ) : ArrayAdapter<T>(context, 0, items) {

        override fun getCount(): Int {
            return items.size
        }

        override fun getItem(position: Int): T? {
            return items[position]
        }

        abstract fun getItemView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View

        override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            return getItemView(position, convertView, parent)
        }

        fun updateList(list: List<T>) {
            items = list
            notifyDataSetChanged()
        }

    }

    interface OnSearchListener {
        fun onQueryChange(query: String)
        fun onItemSelected(value: Any?)
    }

}