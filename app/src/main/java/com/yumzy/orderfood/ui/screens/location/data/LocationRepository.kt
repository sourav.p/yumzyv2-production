package com.yumzy.orderfood.ui.screens.location.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.AddressDao
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.resultLiveData
import com.yumzy.orderfood.worker.singleNetoworkEmit
import javax.inject.Inject

class LocationRepository  constructor(
    private val selectedAddressDao: SelectedAddressDao?,
    private val addressDao: AddressDao,
    private val remoteSource: LocationRemoteDataSource,
) {

    fun getAddressList() = singleNetoworkEmit(
        networkCall = {
            remoteSource.getAddressList()
        },
        handleResponse = { data, status ->
            when (status) {
                APIConstant.Status.SUCCESS -> {
                    val savedAddresses = addressDao.getAllSync().toMutableList()
                    val deletedAddressIds =
                        savedAddresses.filter { saveAddress -> data.find { item -> item.addressId == saveAddress.addressId } == null }
                            .map { deltedAddress -> deltedAddress.addressId }
                    deletedAddressIds.forEach { addressDao.deleteById(it) }
                    data.forEach { addressDao.updateOrInsert(it) }
                }
                APIConstant.Status.ERROR -> {

                }
            }
        }
    )

    fun updateAddress(address: AddressDTO) = networkLiveData(
        networkCall = {
            remoteSource.updateAddress(address)
        }
    ).distinctUntilChanged()

    fun addAddress(address: AddressDTO) = networkLiveData(
        networkCall = {
            remoteSource.addAddress(address)
        }
    ).distinctUntilChanged()

    fun deleteAddress(addressId: String) = networkLiveData(
        networkCall = {
            remoteSource.deleteAddress(addressId)
        }
    ).distinctUntilChanged()


}