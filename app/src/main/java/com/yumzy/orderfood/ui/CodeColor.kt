package com.yumzy.orderfood.ui

import android.graphics.Color
import com.yumzy.orderfood.data.models.base.APIConstant

/*
object ThemeConstant {


}*/


enum class CodeColor {
    COLOR_CODE {
        override fun getCodeColor(code: Int?): Int? {
            return when (code) {
                APIConstant.Status.ERROR -> Color.RED
                APIConstant.Status.SUCCESS -> Color.GREEN
                else -> null
            }

        }
    };

    abstract fun getCodeColor(code: Int?): Int?
}
