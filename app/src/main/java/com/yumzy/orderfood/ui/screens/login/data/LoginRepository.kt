package com.yumzy.orderfood.ui.screens.login.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.distinctUntilChanged
import com.laalsa.laalsalib.subutil.CountryUtils
import com.laalsa.laalsalib.utilcode.util.DeviceUtils
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.singleNetoworkEmit
import javax.inject.Inject

/**
 * Repository module for handling data operations.
 */
class LoginRepository  constructor(
    private val userDao: UserInfoDao?,
    private val selectedAddressDao: SelectedAddressDao?,
    private val remoteSource: LoginRemoteDataSource,
    private val profileSource: ProfileRemoteDataSource
) {

    fun registerUser(
        firstName: String,
        lastName: String,
        userOtp: String,
        emailAddress: String,
        phoneNumber: String
    ): LiveData<ResponseDTO<LoginDTO>> {
        return networkLiveData(
            networkCall = {
                val uniqueDeviceId = DeviceUtils.getUniqueDeviceId()

//                val countryCode = CountryUtils.getCountryCodeBySim("Default")
                val registerDTO =
                    RegisterDTO(
                        firstName,
                        lastName,
                        emailAddress,
                        userOtp,
                        phoneNumber,
                        "+91",
                        uniqueDeviceId,
                        ""
                    )
                remoteSource.registerUser(registerDTO)
            }).distinctUntilChanged()
    }

    fun sendOTP(number: String, countryCode: String, action: String) = networkLiveData(
        networkCall = {
            CountryUtils()
//            val simCountryCode = CountryUtils.getCountryCodeBySim("Default")
            val sendOTP = SendOtpDTO(mobile = number, countryCode = "+91", action = action)
            remoteSource.setOtpToDevice(sendOTP)
        }

    ).distinctUntilChanged()

    fun verifyOTP(
        number: String,
        otpCode: String,
        countryCode: String,
        deviceId: String
    ) =
        networkLiveData(
            networkCall = {
//                val simCountryCode = CountryUtils.getCountryCodeBySim("Default")

                val verifyOtpDTO = VerifyOtpDTO(number, otpCode, countryCode, deviceId)
                remoteSource.verifyOtp(verifyOtpDTO)
            }).distinctUntilChanged()


    fun exploreUser(
        deviceId: String
    ) =
        networkLiveData(
            networkCall = {
                val explorUser = ExploreUser(deviceId)
                remoteSource.exploreUser(explorUser)
            }).distinctUntilChanged()

    fun resendOTP(
        number: String,
        type: String,
        action: String
    ) =
        networkLiveData(
            networkCall = {
                /*
                "countryCode" :"+91",
"mobile" :"9075655335",
"action" : "login / signup",
"method" : "resendOtp"

                 */


//                val fullNumber = "+91$number"
                remoteSource.resendOtp(
                    mapOf(
                        "mobile" to number,
                        "countryCode" to "+91",
                        "type" to type,
                        "action" to action,
                        "method" to "resendOtp"
                    )
                )
            }).distinctUntilChanged()

    fun saveUserInfo(userDTO: UserDTO?) = backgroundLiveData(
        backgroundCallback = {
            if (userDTO != null) {
                userDao?.updateOrInsert(userDTO)
                ResponseDTO.executed(data = true)
            } else
                ResponseDTO.executed(data = false)

        }
    ).distinctUntilChanged()

    fun signOut() =
        networkLiveData(
            networkCall = {
                remoteSource.signOut()
            }).distinctUntilChanged()
/*
    fun saveUserCurrentLocation(mCurrentLocation: AddressDTO) = backgroundLiveData {
        run {
            selectedAddressDao?.updateOrInsert(mCurrentLocation)
            ResponseDTO.executed(data = true)
        }
    }
*/

    fun updateAddress(address: AddressDTO) = networkLiveData(
        networkCall = {
            profileSource.updateAddress(address)
        }
    )

    fun addAddress(address: AddressDTO) = singleNetoworkEmit(
        networkCall = {
            profileSource.addAddress(address)
        },
        handleResponse = { data, status ->
            when (status) {
                APIConstant.Status.SUCCESS -> {
                    selectedAddressDao?.updateOrInsert(address)
                }
                APIConstant.Status.ERROR -> {

                }
            }

        }
    )

    fun getSelectedAddress() = databaseLiveData {
        selectedAddressDao?.getPickedAddress()!!
    }

    fun updateLocation(latitude: String, longitude: String, city: String, locality: String) =
        networkLiveData {
            remoteSource.updateLocation(latitude, longitude, city, locality)
        }

    fun updateUserFcmToken(token: String, userId: String) = networkLiveData(
        networkCall = {
            remoteSource.updateUserFcmToken(
                mapOf(
                    "newToken" to token,
                    "userId" to userId
                )
            )
        }
    )
}
