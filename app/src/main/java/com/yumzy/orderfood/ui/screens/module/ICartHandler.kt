package com.yumzy.orderfood.ui.screens.module

import com.yumzy.orderfood.ui.base.IView

interface ICartHandler : IView {
    fun itemAdded(totalItemCount: Int)
    fun itemRepeated()
    fun failedToAdd(message: String = "", gotoPlate: Boolean = false)
}
