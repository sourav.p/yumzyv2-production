package com.yumzy.orderfood.ui.screens.login.fragment

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.navigation.fragment.findNavController
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentSplashBinding
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.login.ISplashView
import com.yumzy.orderfood.ui.screens.login.SplashViewModel
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel


class SplashFragment : BaseFragment<FragmentSplashBinding, SplashViewModel>(), ISplashView {

    
    override val vm: SplashViewModel by viewModel()

    private var mOutletId = ""
    var isFreshUser: Boolean = false

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_splash

    override fun onFragmentReady(view: View) {
        vm.attachView(this)
        isFreshUser = sharedPrefsHelper.getFreshUser()
    }

    override fun onResume() {
        super.onResume()

        Handler(Looper.getMainLooper()).postDelayed({

            if (!sharedPrefsHelper.getIsTemporary()) {
                //    navHomeActivity()
                navController.navigate(R.id.fragment_splash_screen_to_fetch_location)

            } else if (navController.currentDestination?.id == R.id.fragment_splash) {
                if (isFreshUser) {
                    navController.navigate(R.id.fragment_splash_to_fragment_onboard)
                } else {
                    navController.navigate(R.id.fragment_splash_screen_to_started)
                }
            }

        }, 3000)
    }

    override fun navHomeActivity() {
//        val address = sharedPrefsHelper.getSelectedAddress()
//        if (address != null) {
//            AppGlobalObjects.selectedAddress = address
//        }

        activity?.let {
            val intent = Intent(it, HomePageActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, false)
            intent.putExtra(
                YumzyFirebaseMessaging.NOTIFICATION_DESTINATION,
                StartedActivity.mDestination
            )
            intent.putExtra(
                YumzyFirebaseMessaging.NOTIFICATION_URL,
                StartedActivity.mDestinationUrl
            )
            if (mOutletId.isNotBlank()) {
                intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, mOutletId)
            }
            startActivity(intent)
            Handler(Looper.getMainLooper()).postDelayed({
                it.finish()
            }, 1000)

        }


    }


}