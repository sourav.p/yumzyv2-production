package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IPlateView : IModuleHandler, ConfirmActionDialog.OnActionPerformed {
    fun fetchUserCart(address: AddressDTO?)


    // fun checkUserHasAddress()
}
