/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.helpfaq.faq

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

class HelpSubHeadingViewModel   constructor(private val repository: ProfileRepository) :
    BaseViewModel<IView>()  {

    fun getHelpSubheadings(headingId: Int) = repository.getHelpSubheadings(headingId)

}