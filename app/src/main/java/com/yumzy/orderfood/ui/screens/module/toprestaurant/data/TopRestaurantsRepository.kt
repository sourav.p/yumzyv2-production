package com.yumzy.orderfood.ui.screens.module.toprestaurant.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu on 30-May-20.
 */
class TopRestaurantsRepository  constructor(
    private val remoteSource: TopRestaurantsRemoteDataSource
) {
    fun getRestaurants(promoID: String, skip: Int, limit: Int) = networkLiveData(
        networkCall = {
            val lng = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(0).toString()
            val lat = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(1).toString()
            remoteSource.getRestaurants(promoID, lat, lng, skip, limit)
        }
    ).distinctUntilChanged()
}