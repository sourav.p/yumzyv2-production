package com.yumzy.orderfood.ui.screens.module.favourites.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class FavouriteRestaurantRepository  constructor(
    private val remoteSource: FavourriteRestaurantRemoteDataSource
) {
    fun getFavouriteRestaurant() = networkLiveData(
        networkCall = {
            remoteSource.getFavouriteRestaurant()
        }
    ).distinctUntilChanged()
}