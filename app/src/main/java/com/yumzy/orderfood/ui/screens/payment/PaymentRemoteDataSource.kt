package com.yumzy.orderfood.ui.screens.payment

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import javax.inject.Inject

class PaymentRemoteDataSource  constructor(private val serviceAPI: CartServiceAPI)  :
    BaseDataSource() {

    suspend fun newOrder(newOrder: NewOrderReqDTO) = getResult {
        serviceAPI.newOrder(newOrder)
    }

    suspend fun postCheckout(orderId: String) = getResult {
        serviceAPI.postCheckout(orderId)
    }

    suspend fun orderTrackDetails(orderId: String) = getResult {
        serviceAPI.orderDetails(orderId)

    }

}