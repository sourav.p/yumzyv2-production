package com.yumzy.orderfood.ui.screens.login.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.LoginServiceAPI
import com.yumzy.orderfood.data.models.*
import javax.inject.Inject

class LoginRemoteDataSource  constructor(private val serviceAPI: LoginServiceAPI) :
    BaseDataSource() {

    suspend fun registerUser(registerDTO: RegisterDTO) = getResult {
        return@getResult serviceAPI.registerUser(registerDTO)
    }

    suspend fun setOtpToDevice(sendOtpDTO: SendOtpDTO) = getResult {
        serviceAPI.setOtpTo(sendOtpDTO)
    }

    suspend fun verifyOtp(verifyOtpDTO: VerifyOtpDTO) = getResult {
        serviceAPI.verifyOtp(verifyOtpDTO)
    }

    suspend fun exploreUser(exploreUser: ExploreUser) = getResult {
        serviceAPI.exploreUser(exploreUser)
    }

    suspend fun resendOtp(mapOf: Map<String, String>) = getResult {
        serviceAPI.resendOtp(mapOf)
    }

    suspend fun signOut() = getResult {
        serviceAPI.signOut()
    }


    suspend fun updateUserFcmToken(token: Map<String, String>) = getResult {
        serviceAPI.updateUserFcmToken(token)
    }

    suspend fun updateLocation(
        latitude: String,
        longitude: String,
        city: String,
        locality: String
    ) = getResult {
        val updateLocation = UpdateLocationDTO(
            city = city,
            locality = locality,
            coordinates = listOf(latitude, longitude)
        )
        serviceAPI.updateLocation(updateLocation)
    }
}
