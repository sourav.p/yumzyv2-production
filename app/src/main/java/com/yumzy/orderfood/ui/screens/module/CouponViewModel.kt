package com.yumzy.orderfood.ui.screens.module

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRepository
import javax.inject.Inject

class CouponViewModel  constructor(private val repository: RewardsRepository) :
    BaseViewModel<IView>() {

    fun applyReferral(userId: String, referral: String) = repository.applyReferral(userId, referral)

}