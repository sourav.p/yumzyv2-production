package com.yumzy.orderfood.ui.screens.login

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import com.clevertap.android.sdk.CleverTapAPI
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.ktx.Firebase
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.databinding.ActivityStartedBinding
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.util.IConstants
import com.zoho.livechat.android.ZohoLiveChat
import eu.dkaratzas.android.inapp.update.Constants.UpdateMode
import eu.dkaratzas.android.inapp.update.InAppUpdateManager
import eu.dkaratzas.android.inapp.update.InAppUpdateStatus
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import javax.inject.Inject


class StartedActivity : BaseActivity<ActivityStartedBinding, LoginViewModel>(),
    ILoginView, InAppUpdateManager.InAppUpdateHandler {
    //    private val REQ_CODE_GPS = 1101
    override val navigationPadding: Boolean = true
    override val enableEdgeToEdge: Boolean = true

    companion object {
        var mDestination: String = ""
        var mDestinationUrl: String = ""
    }

    private lateinit var inAppUpdateManager: InAppUpdateManager

    
    override val vm: LoginViewModel by viewModel()

    private var mOutletId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_started)
        handleNotifAndLink(intent)
        /* handleNotifAndLink(intent)
         checkNewVersion()
         setContentView(YumzyView(this))
         BarUtils.setStatusBarLightMode(this, true)
         BarUtils.setStatusBarColor(this, Color.WHITE)
         if (sessionManager.fetchAuthToken() != null) {
             Handler(Looper.getMainLooper()).postDelayed({ navHomeActivity() }, 2000)
         } else {
             this.bindView(R.layout.activity_started)
             applyDebouchingClickListener(
                 bd.btnLogin,
                 bd.btnStarted,
                 bd.tvTermCondition,
                 bd.tvLetsExplore
             )
         }

         setSelectedAddress(sharedPrefsHelper.getSelectedAddress())*/
        if (!BuildConfig.DEBUG)
            checkNewVersion()
        pushFcmToken(YumzyFirebaseMessaging.getToken(this))
    }


    private fun pushFcmToken(token: String?) {
        ZohoLiveChat.Notification.enablePush(
            token,
            !BuildConfig.DEBUG
        ) //true -> for test devices
        CleverTapAPI.getDefaultInstance(this)?.pushFcmRegistrationId(token, true)

    }


    private fun setSelectedAddress(selectedAddress: AddressDTO?) {
        if (selectedAddress != null) {
            AppGlobalObjects.selectedAddress = selectedAddress
        }
    }

    private fun checkNewVersion() {
        inAppUpdateManager = InAppUpdateManager.Builder(
            this,
            IConstants.REQ_CODE_VERSION_UPDATE
        )
            .resumeUpdates(true)
            .handler(this)
            .mode(UpdateMode.FLEXIBLE)
            .useCustomNotification(true)

        inAppUpdateManager.checkForAppUpdate()
    }

    private fun getDeepLinkUrl(intent: Intent) {
        Firebase.dynamicLinks
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData ->
                // Get deep link from result (may be null if no link is found)
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                } else {
                    return@addOnSuccessListener
                }

                val url = deepLink.toString()
                val paramsNames = deepLink?.queryParameterNames

                mDestinationUrl = url

                when {
                    paramsNames?.contains("promoId") == true && paramsNames.contains("landingPage") && paramsNames.contains(
                        "name"
                    ) -> {
                        when (deepLink?.getQueryParameter("landingPage")) {
                            "restaurants" -> {
                                mDestination =
                                    YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_PROMO_OUTLET
                            }
                            "dishes" -> {
                                mDestination =
                                    YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_PROMO_DISH
                            }
                        }

                    }

                    paramsNames?.contains("menuId") == true && paramsNames.contains("outletId") -> {
                        mDestination = YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_DISH
                    }

                    paramsNames?.contains("outletId") == true -> {
                        mDestination = YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OUTLET
                    }

                    paramsNames?.contains("referralCode") == true -> {
                        mDestination = YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_REFERRAL
                    }

                    else -> {
                        mDestination = YumzyFirebaseMessaging.NOTIFICATION_DESTINATION_OPEN_APP_HOME
                    }
                }

            }
            .addOnFailureListener(this) { e ->
                Timber.e(
                    StartedActivity::class.java.canonicalName,
                    "getDynamicLink:onFailure",
                    e
                )
            }
    }


    override fun onDebounceClick(view: View) {
        when (view.id) {
            /*R.id.btnLogin -> {
                AppIntentUtil.launchWithSheetAnimation(
                    this@StartedActivity,
                    LoginActivity::class.java
                )
            }
            R.id.btnStarted -> {
                AppIntentUtil.launchWithSheetAnimation(
                    this@StartedActivity,
                    RegisterActivity::class.java
                )
            }

            R.id.tvTermCondition -> {

                ChromeTabHelper.launchChromeTab(
                    this,
                    "https://laalsadev.sgp1.digitaloceanspaces.com/common_documents/Privacyandpolicy.html"
                )
                *//*ChromeTabHelper.launchUrl(
                    this,
                    "https://dev.openlayer.yumzy.ai?outletId=001d2890-6f4c-11ea-9637-5f684b131ca1&userId=bd88dda0-f172-11e9-833e-490bfb4574d4"
                )*//*

            }
            R.id.tv_lets_explore -> {
                ChromeTabHelper.launchChromeTab(this, "https://yumzy.in/")
                //intentOpenWebsite(this@StartedActivity, "https://yumzy.in/")
            }
*/
        }
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        when (requestCode) {
            IConstants.REQ_CODE_VERSION_UPDATE -> {
                when (resultCode) {
                    Activity.RESULT_CANCELED -> {
                        // If the update is cancelled by the user,
                        // you can request to start the update again.
                        inAppUpdateManager.checkForAppUpdate()
                        Log.d(
                            IConstants.APP_TAG,
                            "Update flow failed! Result code: $resultCode"
                        )
                    }
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }


    override fun onInAppUpdateError(code: Int, error: Throwable?) {
/*
         * Called when some error occurred. See Constants class for more details
*/
    }

    override fun onInAppUpdateStatus(status: InAppUpdateStatus?) {

        /*
     * Called when the update status change occurred.
     */
        /*     progressBar.setVisibility(if (status.isDownloading) View.VISIBLE else View.GONE)
             tvVersionCode.setText(
                 String.format(
                     "Available version code: %d",
                     status.availableVersionCode()
                 )
             )
             tvUpdateAvailable.setText(
                 String.format(
                     "Update available: %s",
                     status.isUpdateAvailable.toString()
                 )
             )
             if (status.isDownloaded) {
                 updateButton.setText("Complete Update")
                 updateButton.setOnClickListener(View.OnClickListener { view: View? -> inAppUpdateManager.completeUpdate() })
             }*/
    }


    private fun handleNotifAndLink(intent: Intent) {
        if (intent.hasExtra(YumzyFirebaseMessaging.NOTIFICATION_DESTINATION)) {
            mDestination =
                intent.getStringExtra(YumzyFirebaseMessaging.NOTIFICATION_DESTINATION).toString()
            mDestinationUrl =
                intent.getStringExtra(YumzyFirebaseMessaging.NOTIFICATION_URL).toString()
        } else {
            getDeepLinkUrl(intent)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            handleNotifAndLink(intent)
        }
    }

    override fun navHomeActivity() {

        val address = sharedPrefsHelper.getSelectedAddress()
        if (address != null) {
            AppGlobalObjects.selectedAddress = address
        }

        val intent = Intent(this, HomePageActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, false)
        intent.putExtra(YumzyFirebaseMessaging.NOTIFICATION_DESTINATION, mDestination)
        intent.putExtra(YumzyFirebaseMessaging.NOTIFICATION_URL, mDestinationUrl)
        if (mOutletId.isNotBlank()) {
            intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, mOutletId)
        }
        startActivity(intent)
        finish()
    }
}