package com.yumzy.orderfood.ui.screens.login

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import com.clevertap.android.geofence.CTGeofenceAPI
import com.clevertap.android.geofence.CTGeofenceSettings
import com.clevertap.android.geofence.Logger
import com.clevertap.android.geofence.interfaces.CTGeofenceEventsListener
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.appCleverTap
import com.yumzy.orderfood.appCrashlytics
import com.yumzy.orderfood.data.models.LoginDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityRegisterBinding
import com.yumzy.orderfood.tools.address.CurrentLocationPickerActivity
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.common.otpview.PinEditView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.login.data.SmsListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.worker.SMSBroadCastReceiver
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject


class RegisterActivity : BaseActivity<ActivityRegisterBinding, LoginViewModel>(), ILoginView,
    IModuleHandler, ConfirmActionDialog.OnActionPerformed, PinEditView.PinViewEventListener,
    ActionDialog.OnActionPerformed {

    override val vm: LoginViewModel by viewModel()

    private var smsBroadCastReceiver: SMSBroadCastReceiver? = null
    var otpEnforce = false
    var hasLocationPermission = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_register)
        vm.attachView(this)

        bd.loginVM = vm
        bd.pinview.setPinViewEventListener(this)
        applyDebouchingClickListener(
            bd.btnContinue,
            bd.tvResendCode
        )
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        setInputObserver()
        startSMSListener()
        askLocationPermission()
    }

    private fun startSMSListener() {
        try {
            smsBroadCastReceiver = SMSBroadCastReceiver()

            smsBroadCastReceiver?.bindListener(object : SmsListener {
                @SuppressLint("HardwareIds")
                override fun messageReceived(messageText: String?) {

//                Log.e("----->>", messageText)
//                    showToast(messageText.toString())
                    bd.pinview.value = messageText ?: ""
                }

                override fun onTimeOut(messageText: String?) {
                    showToast(messageText.toString())
                }
            })
            val intentFilter = IntentFilter()
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
            this.registerReceiver(smsBroadCastReceiver, intentFilter)

            val client = SmsRetriever.getClient(this)

            val task = client.startSmsRetriever()
            task.addOnSuccessListener {
                // API successfully started
//                showToast("API successfully started ")

            }

            task.addOnFailureListener {
//                showToast("API fail ")
                showSnackBar(getString(R.string.enter_otp_failed_to_read_otp), 1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    private fun setInputObserver() {

        vm.validForm.observe(this, {
            when (it!!) {
                IConstants.RegisterStep.STEP_ONE -> {
                    layoutStep(IConstants.RegisterStep.STEP_ONE)

                }
                IConstants.RegisterStep.STEP_TWO -> {
                    layoutStep(IConstants.RegisterStep.STEP_TWO)


                }
                IConstants.RegisterStep.STEP_THREE -> {
                    layoutStep(IConstants.RegisterStep.STEP_THREE)


                }
            }
        })
    }


    override fun onSuccess(actionId: Int, data: Any?) {

    }

    override fun navHomeActivity() {
        val intent = Intent(this, HomePageActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, true)
        startActivity(intent)
        finishAffinity()
    }


    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.btn_continue -> {

                if (otpEnforce) {
                    val userOtp = bd.pinview.value

                    if (bd.pinview.value.length == 0) {
                        showSnackBar(getString(R.string.please_enter_otp), 0)
                        YumUtil.animateShake(bd.pinview, 30, 10)
                        return
                    } else if (bd.pinview.value.length < 4) {
                        showSnackBar(getString(R.string.invalid_otp_resend_if_did_not_received), 0)
                        YumUtil.animateShake(bd.pinview, 30, 10)
                        return
                    } else {
                        registerNewUser(userOtp)

                    }

                }

                bd.tvErrorValue.visibility = View.INVISIBLE
                val firstName = bd.edtInput.text ?: ""
                val lastName = bd.edtInputLastName.text ?: ""
                if (vm.validForm.value == IConstants.RegisterStep.STEP_ONE) {
                    if (YumUtil.isValidNameIgnoreSpace(firstName.toString())) {
                        vm.firstName = firstName.toString()
                        if (YumUtil.isValidNameIgnoreSpace(lastName.toString())) {
                            vm.lastName = lastName.toString()
                            vm.validForm.postValue(IConstants.RegisterStep.STEP_TWO)
                            return
                        } else {
                            inputLastNameErrorText(resources.getString(R.string.invalid_last_name))
                        }

                    } else {
                        inputErrorText(resources.getString(R.string.invalid_first_name))
                    }
                }
                if (vm.validForm.value == IConstants.RegisterStep.STEP_TWO) {

                    if (YumUtil.isValidEmail(firstName.toString())) {
                        vm.emailAddress = firstName.toString()
                        vm.validForm.postValue(IConstants.RegisterStep.STEP_THREE)
                        return
                    } else {
                        inputErrorText(resources.getString(R.string.invalid_email))
                    }

                }
                if (vm.validForm.value == IConstants.RegisterStep.STEP_THREE) {
                    vm.mobileNumber = firstName.toString()
                    if (YumUtil.isValidMobile(firstName.toString())) {
                        /* if (selectedAddress == null) {
                             YumUtil.animateShake(bd.addressFrame, 50, 10)
                             showToast("Please Select Location")
                             return
                         }*/
                        registerUser()
                        return
                    } else {
                        inputErrorText(resources.getString(R.string.invalid_number))
                    }


                }
            }
            R.id.tv_resend_code -> {
                vm.resendOTP(vm.mobileNumber, "signup")
                    .observe(this, {
                        otpEnforce = false
                        startCountDownTimer()
                    })
            }


        }
    }

    private fun registerNewUser(userOtp: String) {
        vm.registerNewUser(userOtp).observe(this, { result ->
            RHandler<LoginDTO>(
                this,
                result
            ) {

                it.token.let { token ->
                    sessionManager.saveAuthToken(token)
                }
                it.user.userId.let { value ->
                    sharedPrefsHelper.saveUserId(
                        value
                    )
                }
                it.user.cartId.let { value ->
                    sharedPrefsHelper.saveCartId(
                        value
                    )
                }
                it.user.name.let { value ->
                    sharedPrefsHelper.saveUserName(
                        value
                    )
                }
                it.user.lastName.let { value ->
                    sharedPrefsHelper.saveUserLastName(value)
                }
                it.user.email.let { value ->
                    sharedPrefsHelper.saveUserEmail(
                        value
                    )
                }
                it.user.mobile.let { value ->
                    sharedPrefsHelper.saveUserPhone(
                        value
                    )
                }

                it.user.picture?.let {
                    sharedPrefsHelper.saveUserPic(it)
                }

                it.user.referralDeepLink.let {
                    sharedPrefsHelper.saveUserReferralLink(it)
                }

                it.user.refferalCode.let {
                    sharedPrefsHelper.saveUserReferralCode(it)
                }
                it.isNew.let {
                    sharedPrefsHelper.saveIsNewUser(true)
                }

                registerResponse(it)
                CleverTapHelper.setCleaverUser(it)
                CleverTapHelper.pushEvent(CleverEvents.user_signup)
                try {
                    CTGeofenceAPI.getInstance(applicationContext).triggerLocation()

                } catch (e: IllegalStateException) {
                    // thrown when this method is called before geofence SDK initialization
                    CleverTapHelper.pushEvent(CleverEvents.signup_location)
                }

                if (it.user.refferalCode.isNotEmpty())
                    CleverTapHelper.userReferral(
                        it.user.name,
                        it.user.mobile,
                        it.user.refferalCode
                    )

            }
        })
    }

    private fun inputErrorText(sErrorMsg: String) {
        bd.tvErrorValue.visibility = View.VISIBLE
        bd.tvErrorValue.text = sErrorMsg
        YumUtil.animateShake(bd.tvErrorValue, 50, 15)
    }

    private fun inputLastNameErrorText(sErrorMsg: String) {
        bd.tvErrorLastName.visibility = View.VISIBLE
        bd.tvErrorLastName.text = sErrorMsg
        YumUtil.animateShake(bd.tvErrorLastName, 50, 15)
    }

    private fun registerUser() {


        vm.sendRegisterOTP(vm.mobileNumber)
            .observe(this, { result ->
                RHandler<Any>(
                    this,
                    result
                ) {
                    onResponse(APIConstant.Status.SUCCESS, null)

                }

            })

    }

    private fun registerResponse(data: LoginDTO?) {
        if (data != null) {
            vm.updateFcmToken(YumzyFirebaseMessaging.getToken(this).toString(), data.user.userId)
                .observe(this, {
                    if (it.type != -1) {
                        vm.saveUserInfo(data.user).observe(this, {
                            if (it.type != -1) {
                                appCrashlytics.setUserId(data.user.userId);
                                navHomeActivity()
                            }
                        })
                    }
                })
        } else {
            showSnackBar("Failed To Register , Try Later!", -1)
        }
    }


    private fun layoutStep(stepOne: IConstants.RegisterStep) {
        when (stepOne) {
            IConstants.RegisterStep.STEP_ONE -> {
                bd.otpLayout.visibility = View.GONE
                bd.edtInputLastName.visibility = View.VISIBLE
                bd.tvErrorLastName.visibility = View.VISIBLE
                bd.includeBubbleDrawable.heading = resources.getString(R.string.register_step1)
                inputErrorText("")
                inputLastNameErrorText("")

                changeRegisterStatus(
                    resources.getString(R.string.step_one),
                    resources.getString(R.string.whats_name),
                    resources.getString(R.string.what_to_call),
                    30
                )
                bd.edtInput.inputType = InputType.TYPE_CLASS_TEXT
                bd.edtInput.setText(vm.firstName)
                bd.edtInputNameContainer.hint = getString(R.string.enter_first_name)
                bd.btnContinue.text = getString(R.string.next)
                bd.edtInput.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                bd.edtInput.filters = arrayOf<InputFilter>(LengthFilter(40))

                bd.edtInputLastName.inputType = InputType.TYPE_CLASS_TEXT
                bd.edtInputLastName.setText(vm.lastName)
                bd.edtInputLastNameContainer.hint = getString(R.string.enter_last_name)
                bd.btnContinue.text = getString(R.string.next)
                bd.edtInputLastName.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                bd.edtInputLastName.filters = arrayOf<InputFilter>(LengthFilter(40))
                bd.edtInput.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {


                        val first_name = bd.edtInput.text ?: ""
                        if (vm.validForm.value == IConstants.RegisterStep.STEP_ONE) {
                            if (YumUtil.isValidNameIgnoreSpace(first_name.toString())) {
                                vm.firstName = first_name.toString()
                                inputErrorText("")
                                return@setOnEditorActionListener true

                            } else {
                                inputErrorText(resources.getString(R.string.invalid_first_name))
                                return@setOnEditorActionListener false
                            }
                        }
                    }
                    false
                }

                bd.edtInputLastName.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        val last_name = bd.edtInputLastName.text ?: ""
                        if (vm.validForm.value == IConstants.RegisterStep.STEP_ONE) {
                            val first_name = bd.edtInput.text ?: ""
                            if (YumUtil.isValidNameIgnoreSpace(first_name.toString())) {
                                vm.firstName = first_name.toString()
                                inputErrorText("")
                                if (YumUtil.isValidNameIgnoreSpace(last_name.toString())) {
                                    vm.lastName = last_name.toString()
                                    inputLastNameErrorText("")
                                    vm.validForm.postValue(IConstants.RegisterStep.STEP_TWO)

                                    return@setOnEditorActionListener true

                                } else {
                                    inputLastNameErrorText(resources.getString(R.string.invalid_last_name))
                                    return@setOnEditorActionListener false
                                }
                            } else {
                                inputErrorText(resources.getString(R.string.invalid_first_name))
                                bd.edtInput.requestFocus()
                                return@setOnEditorActionListener false
                            }
                        }
                    }
                    false


                }
            }
            IConstants.RegisterStep.STEP_TWO -> {
                bd.edtInput.requestFocus()
                bd.otpLayout.visibility = View.GONE
                bd.edtInputLastNameContainer.visibility = View.GONE
                bd.tvErrorLastName.visibility = View.GONE
                bd.includeBubbleDrawable.heading = resources.getString(R.string.register_step2)

                inputErrorText("")


                changeRegisterStatus(
                    resources.getString(R.string.step_two),
                    resources.getString(R.string.whats_email),
                    resources.getString(R.string.register_email),
                    60
                )
                bd.edtInput.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                bd.edtInput.setText(vm.emailAddress)
                bd.edtInputNameContainer.hint = getString(R.string.enter_personal_email)
                bd.btnContinue.text = getString(R.string.next)
                bd.edtInput.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                bd.edtInput.filters = arrayOf<InputFilter>(LengthFilter(50))
                bd.edtInput.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        val text = bd.edtInput.text ?: ""
                        if (vm.validForm.value == IConstants.RegisterStep.STEP_TWO) {

                            if (YumUtil.isValidEmail(text.toString())) {
                                vm.emailAddress = text.toString()
                                vm.validForm.postValue(IConstants.RegisterStep.STEP_THREE)
                                inputErrorText("")

                                return@setOnEditorActionListener true
                            } else {
                                inputErrorText(resources.getString(R.string.invalid_email))
                                return@setOnEditorActionListener false

                            }

                        }
                    }
                    false
                }
            }
            IConstants.RegisterStep.STEP_THREE -> {
                inputErrorText("")
                bd.edtInput.requestFocus()
                bd.includeBubbleDrawable.heading = resources.getString(R.string.register_step3)
                bd.edtInputLastNameContainer.visibility = View.GONE
                bd.tvErrorLastName.visibility = View.GONE
                changeRegisterStatus(
                    resources.getString(R.string.step_three),
                    resources.getString(R.string.enter_mobile),
                    resources.getString(R.string.will_verify_info),
                    90
                )
                bd.edtInput.inputType = InputType.TYPE_CLASS_PHONE
                bd.edtInput.setText(vm.mobileNumber)
                bd.edtInputNameContainer.hint = getString(R.string.enter_mobile_no)
                bd.edtInput.inputType = InputType.TYPE_CLASS_NUMBER
                bd.edtInput.filters = arrayOf<InputFilter>(LengthFilter(10))
                bd.btnContinue.text = getString(R.string.send_otp)
                bd.edtInput.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (vm.validForm.value == IConstants.RegisterStep.STEP_THREE) {
                            val text = bd.edtInput.text ?: ""

                            vm.mobileNumber = text.toString()
                            if (YumUtil.isValidMobile(text.toString())) {
                                registerUser()
                                inputErrorText("")

                                return@setOnEditorActionListener true
                            } else {
                                inputErrorText(resources.getString(R.string.invalid_number))
                                return@setOnEditorActionListener false

                            }


                        }

                    }
                    false
                }

            }
        }

    }

    private fun changeRegisterStatus(
        steps: String,
        heading: String,
        subHeading: String,
        progress: Int
    ) {
        /*bd.blobTitleStyleDTO = YumUtil.getBlobStyleObject(
            heading,
            ThemeConstant.textBlackColor,
            R.style.TextAppearance_MyTheme_Headline4_Black,
            true,
            5,
            ThemeConstant.alphaBlackColor
        )*/
        bd.includeBubbleDrawable.title = heading
        bd.tvSubLabel.text = subHeading
        bd.tvStepCount.text = steps
        bd.edtInput.setText("")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            bd.pbStatus.setProgress(progress, true)
        } else
            bd.pbStatus.progress = progress
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction("com.google.android.gms.auth.api.phone.SMS_RETRIEVED")
        registerReceiver(smsBroadCastReceiver, filter)
    }

    override fun onDestroy() {
        vm.detachView()

        super.onDestroy()
        smsBroadCastReceiver?.unbindListener()
        unregisterReceiver(smsBroadCastReceiver)

    }

    override fun onResponse(code: Int?, response: Any?) {
        when (code) {
            APIConstant.Status.SUCCESS -> {
                bd.includeBubbleDrawable.heading = resources.getString(R.string.register_step4)
                bd.includeBubbleDrawable.title = resources.getString(R.string.signup_last)
                bd.otpLayout.visibility = View.VISIBLE
                startCountDownTimer()
            }
            else -> super.onResponse(code, response)

        }
    }

    override fun onBackPressed() {
        if (otpEnforce) {
            val title = getString(R.string.almost_there)
            val sMessage = getString(R.string.almost_done_with_registration)


            val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                null,
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.ok),
                )
            )
            RegUserDialogAction(
                IConstants.ActionModule.CONFIRM_BACK_PRESS,
                title,
                sMessage,
                actions
            )
            return
        }
        if (vm.validForm.value == IConstants.RegisterStep.STEP_THREE) {
            vm.validForm.postValue(IConstants.RegisterStep.STEP_TWO)
            bd.edtInput.filters = arrayOf<InputFilter>(LengthFilter(50))
            return
        } else
            if (vm.validForm.value == IConstants.RegisterStep.STEP_TWO) {
                vm.validForm.postValue(IConstants.RegisterStep.STEP_ONE)
                bd.edtInputLastNameContainer.visibility = View.VISIBLE

                return

            } else
                if (vm.validForm.value == IConstants.RegisterStep.STEP_ONE && vm.firstName.isNotEmpty()) {

                    val title = getString(R.string.almost_there)
                    val sMessage = getString(R.string.almost_done_with_registration)


                    val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                        null,
                        ActionItemDTO(
                            key = ActionConstant.POSITIVE,
                            text = getString(R.string.ok),
                        )
                    )
                    RegUserDialogAction(
                        IConstants.ActionModule.CONFIRM_BACK_PRESS,
                        title,
                        sMessage,
                        actions
                    )
                } else
                    super.onBackPressed()
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Code.USER_EXISTS) {

            val title = getString(R.string.registerd_user)
            val sMessage = message

            val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                null,
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.ok),
                )
            )
            RegUserDialogAction(
                IConstants.ActionModule.USER_EXISTS,
                title,
                sMessage,
                actions
            )
        } else
            super.showCodeError(code, title, message)
    }

    private fun startCountDownTimer() {
        bd.tvSubLabel.text = getString(R.string.please_enter_otp_for_vatification)

        if (otpEnforce) return

        this.otpEnforce = true
        bd.tvResendCode.isEnabled = false
        bd.edtInput.isEnabled = false
        bd.edtInput.setTextColor(ThemeConstant.mediumGray)
        bd.tvResendCode.setTextColor(ThemeConstant.pinkiesLight)
        bd.btnContinue.text = getString(R.string.verify_otp)
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                bd.tvResendCode.text = "${millisUntilFinished / 1000}s Please Wait"

            }

            override fun onFinish() {
                bd.tvResendCode.isEnabled = true
                bd.edtInput.isEnabled = true
                applyDebouchingClickListener(bd.tvResendCode)
                bd.edtInput.setTextColor(ThemeConstant.textDarkGrayColor)
                bd.tvResendCode.text = getString(R.string.resend_code)
                bd.tvResendCode.setTextColor(ThemeConstant.blueJeans)
                otpEnforce = false
            }
        }.start()
    }

    override fun onConfirmDone(dialog: Dialog?, iActionId: Int, sTransIds: String?) {
        if (iActionId == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            finish()
        } else if (iActionId == IConstants.ActionModule.CONFIRM_DIALOG) {
            navLoginActivity()
        }
    }

    private fun navPickActivity(isNew: Boolean) {
        val intent = Intent(this, CurrentLocationPickerActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, true)
        startActivity(intent)
        finishAffinity()
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {

    }

    private fun navLoginActivity() {
        val intent = Intent(this, StartedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    override fun onDataEntered(pinEditView: PinEditView?, fromUser: Boolean) {
        YumUtil.hideKeyboard(this)
        val userOtp = pinEditView?.value
        if (userOtp != null)
            registerNewUser(userOtp)
        else
            showSnackBar("Please Verify OTP", ThemeConstant.redRibbonColor)
    }

    private fun askLocationPermission() {
        Dexter.withContext(this)
            .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    initCTGeofenceApi()
                    hasLocationPermission = true
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    showSnackBar(
                        "Need Location permission for better delivery experience",
                        Color.RED
                    )
                    CleverTapHelper.deniedPermission(p0?.permissionName)
                    hasLocationPermission = false
//                    AppUtils.relaunchApp()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }
            }).check()

    }

    private fun initCTGeofenceApi() {
        CTGeofenceAPI.getInstance(applicationContext)
            .init(
                CTGeofenceSettings.Builder()
                    .enableBackgroundLocationUpdates(true)
                    .setLogLevel(Logger.DEBUG)
                    .setLocationAccuracy(CTGeofenceSettings.ACCURACY_HIGH)
                    .setLocationFetchMode(CTGeofenceSettings.FETCH_CURRENT_LOCATION_PERIODIC)
                    .setGeofenceMonitoringCount(99)
                    .setInterval(3600000) // 1 hour
                    .setFastestInterval(1800000) // 30 minutes
                    .setSmallestDisplacement(1000f) // 1 km
                    .build(), appCleverTap
            )


        try {
            CTGeofenceAPI.getInstance(applicationContext).triggerLocation()
        } catch (e: IllegalStateException) {
            // thrown when this method is called before geofence SDK initialization
        }
        CTGeofenceAPI.getInstance(applicationContext)
            .setOnGeofenceApiInitializedListener {
                /*showToast(
                    "Geofence API initialized",
                )*/
            }
        CTGeofenceAPI.getInstance(applicationContext)
            .setCtGeofenceEventsListener(object : CTGeofenceEventsListener {
                override fun onGeofenceEnteredEvent(jsonObject: JSONObject) {
//                    showToast("Geofence Entered")
                }

                override fun onGeofenceExitedEvent(jsonObject: JSONObject) {
//                    showToast("Geofence Exited")
                }
            })

        CTGeofenceAPI.getInstance(applicationContext)
            .setCtLocationUpdatesListener {
//                CleverTapHelper.pushStartedLocation(this, it.latitude, it.longitude)
//                showToast("Location updated")
//                CleverTapHelper.pushStartedLocation(this, it.latitude, it.longitude)
//                showToast("Location updated")
            }

    }

    private fun RegUserDialogAction(
        regUser: Int,
        title: String,
        sMessage: String,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            this,
            regUser,
            title,
            sMessage,
            actions,
            this
        )
    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {

        if (actionType == IConstants.ActionModule.USER_EXISTS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                navLoginActivity()
            }
        }
        if (actionType == IConstants.ActionModule.CONFIRM_BACK_PRESS) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                finish()
            }
        }
    }
}

