package com.yumzy.orderfood.ui.screens.new.home

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentHomeBottomNavBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.screens.home.HomePageViewModel
import com.yumzy.orderfood.ui.screens.home.IHomePageView
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeBottomNavFragment : BaseFragment<FragmentHomeBottomNavBinding, HomePageViewModel>(),
    IHomePageView {

    override val vm: HomePageViewModel by viewModel()

    private val bottomNavSelectedItemIdKey = "BOTTOM_NAV_SELECTED_ITEM_ID_KEY"
    private var bottomNavSelectedItemId = R.id.nav_home

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_home_bottom_nav

    override fun onFragmentReady(view: View) {
        vm.attachView(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.let {
            bottomNavSelectedItemId = it.getInt(bottomNavSelectedItemIdKey, bottomNavSelectedItemId)

        }
        setupBottomNavBar(view)

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(bottomNavSelectedItemIdKey, bottomNavSelectedItemId)
        super.onSaveInstanceState(outState)
    }

    private fun setupBottomNavBar(view: View) {
        val navGraphIds = listOf(
            R.navigation.nav_home,
            R.navigation.nav_explore,
            R.navigation.nav_plate,
            R.navigation.nav_account
        )

        bd.bottomNavView.selectedItemId =
            bottomNavSelectedItemId // Needed to maintain correct state on return

        val controller = bd.bottomNavView.setupWithNavController(
            fragmentManager = childFragmentManager,
            navGraphIds = navGraphIds,
            backButtonBehaviour = BackButtonBehaviour.POP_HOST_FRAGMENT,
            containerId = R.id.bottom_nav_container,
            firstItemId = R.id.nav_home, // Must be the same as bottomNavSelectedItemId
            intent = requireActivity().intent
        )

        controller.observe(viewLifecycleOwner, { navController ->
            NavigationUI.setupWithNavController(bd.bottomNavToolbar, navController)
            bottomNavSelectedItemId =
                navController.graph.id // Needed to maintain correct state on return
        })
    }

    override fun displayNewEvent() {
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return true
    }


    override fun onSuccess(actionId: Int, data: Any?) { }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) { }

    override fun onDismissed(actionType: Int, actionLabel: String?) { }

    override fun onUpdateNeeded(updateUrl: String?) { }

}