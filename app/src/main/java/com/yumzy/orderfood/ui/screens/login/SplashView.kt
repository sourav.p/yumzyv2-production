package com.yumzy.orderfood.ui.screens.login

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ActivitySplashBinding
import com.yumzy.orderfood.util.ViewConst

class SplashView : FrameLayout {
    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        layoutParams = ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)

        ActivitySplashBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        /*  this.background = ResourcesCompat.getDrawable(
              context.resources,
              R.drawable.img_yumzy_bacground,
              context.theme
          )*/
        binding.yumzyLottiAnimation.setAnimation(R.raw.splash_logo)
      //  binding.root.translationX = (300.pxf)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        binding.yumzyLottiAnimation.playAnimation()
        //  binding.root.animate().translationX(0f)

    }
}