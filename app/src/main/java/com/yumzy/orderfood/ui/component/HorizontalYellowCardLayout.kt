/*
 * Created By Shriom Tripathi 6 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils

class HorizontalYellowCardLayout : ConstraintLayout {

    var imageUrl: String = ""
        set(value) {
            val view = this.getChildAt(0) as RoundedImageView
//            Glide.with(context).load(value).into(view)
            YUtils.setImageInView(view, value)

            field = value
        }

    private var imageSrc: Drawable? = null

    var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_TITLE).text = title
        }

    var locality: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_LOCALITY).text =
                locality

            if (value.isEmpty()) {
                this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_LOCALITY).visibility =
                    View.GONE
            } else {
                this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_LOCALITY).visibility =
                    View.VISIBLE
            }
        }

    var range: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_RANGE).text = range
        }
    var offer: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_OFFER).text = offer
        }

    var subTitle: String = ""
        set(value) {


            this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_SUBTITLE)?.text = value
            field = value
            if (value.isEmpty()) {
                this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_SUBTITLE)?.visibility =
                    View.GONE
            } else {
                this.findViewById<TextView>(IConstants.HorzontalYellowCard.VIEW_SUBTITLE)?.visibility =
                    View.VISIBLE
            }


        }

    var rating: String = ""
        set(value) {
            this.findViewById<RatingView>(IConstants.HorzontalYellowCard.VIEW_RATING).apply {
                setRating(
                    value
                )
            }
            field = value

        }
    var ratingColor: Int = ThemeConstant.yellow
        set(value) {
            field = value

            this.findViewById<RatingView>(IConstants.HorzontalYellowCard.VIEW_RATING).apply {
                cardBackground(ratingColor)
            }

        }
    var ratingRadius: Float = UIHelper.i5px.toFloat()
        set(value) {
            field = value

            this.findViewById<RatingView>(IConstants.HorzontalYellowCard.VIEW_RATING).apply {
                rattingRadius(ratingRadius)
            }

        }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView()
        initAttrs(attrs, context, defStyleAttr)
    }

    private var imageAspectRation: String = "1:1.2"

    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.HorizontalYellowCardLayout,
                    defStyleAttr,
                    0
                )

            imageSrc = a.getDrawable(R.styleable.HorizontalYellowCardLayout_horizontal_image_src)
            title = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_title) ?: ""
            locality = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_locality) ?: ""
            subTitle = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_subtitle) ?: ""
            imageAspectRation =
                a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_image_aspect_ratio)
                    ?: "1:1"
            rating = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_rating) ?: ""
            range = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_range) ?: ""
            offer = a.getString(R.styleable.HorizontalYellowCardLayout_horizontal_offer) ?: ""
            a.recycle()

        }
        this.setClickScaleEffect()
        this.setOnClickListener {}
    }

    private fun initView() {
        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)

        this.layoutParams =
            ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        this.addView(restaurantImageView())

        val layout = LinearLayout(context).apply {
            this.layoutParams = LayoutParams(0, ViewConst.MATCH_PARENT).apply {
                topToTop = LayoutParams.PARENT_ID
//                bottomToBottom = LayoutParams.PARENT_ID
                startToEnd = this@HorizontalYellowCardLayout.getChildAt(0).id
                endToEnd = LayoutParams.PARENT_ID
            }
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.TOP
            setPadding(VUtil.dpToPx(15), 0, 0, 0)
            this.addView(getTextView(title, 14f, IConstants.HorzontalYellowCard.VIEW_TITLE))
            this.addView(
                getTextView(
                    locality,
                    10f,
                    IConstants.HorzontalYellowCard.VIEW_LOCALITY
                )
            )
            this.addView(
                getTextView(
                    subTitle,
                    11f,
                    IConstants.HorzontalYellowCard.VIEW_SUBTITLE
                )
            )
            this.addView(getTextView(range, 10f, IConstants.HorzontalYellowCard.VIEW_RANGE))
            this.addView(getTextView(offer, 12f, IConstants.HorzontalYellowCard.VIEW_OFFER))

            this.addView(getRatingView(rating))
        }

        this.addView(layout)

    }

    private fun getRatingView(ratiing: String): View? {
        return RatingView(context).apply {
            layoutParams =
                LinearLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT).apply {
                    setMargins(0, UIHelper.i8px, 0, 0)
                }
            id = IConstants.HorzontalYellowCard.VIEW_RATING
            setRating(ratiing)
        }
    }

    private fun restaurantImageView(): ImageView {
        return RoundedImageView(context).apply {
            id = IConstants.HorzontalYellowCard.VIEW_IMAGE
            layoutParams = LayoutParams(UIHelper.i80px, 0).apply {
                topToTop = LayoutParams.PARENT_ID
                startToStart = LayoutParams.PARENT_ID
                bottomToBottom = LayoutParams.PARENT_ID
                dimensionRatio = imageAspectRation
            }

            id = R.id.restaurant_image
            cornerRadius = UIHelper.cornerRadius
            setImageDrawable(imageSrc ?: context.getDrawable(YUtils.getPlaceHolder()))
            scaleType = ImageView.ScaleType.CENTER_CROP
        }
    }

    private fun getTextView(text: String, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.textSize = textSize
        textView.id = tvId
        when (tvId) {
            IConstants.HorzontalYellowCard.VIEW_TITLE -> {
                textView.setInterBoldFont()
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            IConstants.HorzontalYellowCard.VIEW_SUBTITLE -> {
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
//                textView.gravity = Gravity.TOP or Gravity.START
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.layoutParams =
                    LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT, 1f)
            }

            IConstants.HorzontalYellowCard.VIEW_LOCALITY -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
//                textView.gravity = Gravity.TOP or Gravity.START
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.layoutParams =
                    LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT, 1f)
            }
            IConstants.HorzontalYellowCard.VIEW_OFFER -> {
                textView.setTextColor(ThemeConstant.pinkies)
                // textView.setPadding(0, VUtil.dpToPx(5), 0, VUtil.dpToPx(1))
                textView.layoutParams =
                    LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT, 1f)
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END
            }
            IConstants.HorzontalYellowCard.VIEW_RANGE -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
                //   textView.setPadding(0, VUtil.dpToPx(3), 0, VUtil.dpToPx(1))
                textView.maxLines = 1
                textView.ellipsize = TextUtils.TruncateAt.END

//                textView.gravity = Gravity.TOP or Gravity.START
                textView.layoutParams =
                    LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT, 1f)
            }
        }
        return textView

    }

}