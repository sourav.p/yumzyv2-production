package com.yumzy.orderfood.ui.screens.module.review

import android.content.DialogInterface
import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FoodPrefSectionDTO
import com.yumzy.orderfood.data.models.FoodPreferenceDTO
import com.yumzy.orderfood.data.models.RatingDTO
import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogRateOrderBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.ui.component.CuisineView
import com.yumzy.orderfood.ui.component.FeedbackSelectionView
import com.yumzy.orderfood.ui.component.groupview.FlowGroupView
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceDialog
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class RateOrderDialog(
    private val orderNum: String,
    private val orderID: String,
    private val outletName: String,
    private val items: String
) :
    BaseSheetFragment<DialogRateOrderBinding, RateOrderViewModel>(), IRateOrder,
    FlowGroupView.OnSelectedItemChangeListener {
    var selectedTasteList = ""
    var selDeliveryFeedbackList = ""
    var outLetRating = 1.0f
    var deliveryRating = 1.0f

    val FOOD_REVIEW_SELECTION = "food_review_selection"

    
    override val vm: RateOrderViewModel by viewModel()

    override fun getLayoutId() = R.layout.dialog_rate_order

    override fun onDialogCreated(view: View) {
        setUIData()
    }

    private fun setUIData() {
        bd.tvOrderNo.text = "Order No #" + orderNum
        bd.tvRestaurantName.text = "Rate your food - $outletName"
        bd.outletRatingBar.rating = outLetRating
        bd.deliveryboyRatingBar.rating = deliveryRating
        bd.tvDeliveryStatus.text =
            resources.getString(R.string.rate_delivery_boy) + " " + resources.getString(R.string.disappointed)
        bd.tvDeliveryIssue.text = resources.getString(R.string.what_went_bad)
        setupDeliverReview(IConstants.RatingFlags.RATING_1_0)



        setupFoodReview()
        bd.outletRatingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener()
        { ratingBar: RatingBar, fl: Float, b: Boolean ->

            if (ratingBar.rating >= 1.0f) {
                outLetRating = ratingBar.rating
            } else if (ratingBar.rating == 0.0f) {
                outLetRating = 1.0f
                bd.outletRatingBar.rating = outLetRating
            }

        }


        bd.deliveryboyRatingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener()
        { ratingBar: RatingBar, fl: Float, b: Boolean ->
            if (ratingBar.rating >= 1.0f) {
                deliveryRating = ratingBar.rating
            } else if (ratingBar.rating == 0.0f) {
                deliveryRating = 1.0f
                bd.deliveryboyRatingBar.rating = deliveryRating
            }

            when (ratingBar.rating) {
                IConstants.RatingFlags.RATING_1_0 -> {
                    bd.tvDeliveryStatus.text =
                        resources.getString(R.string.rate_delivery_boy) + " " + resources.getString(
                            R.string.disappointed
                        )
                    bd.tvDeliveryIssue.text = resources.getString(R.string.what_went_bad)
                    setupDeliverReview(IConstants.RatingFlags.RATING_1_0)

                }

                IConstants.RatingFlags.RATING_2_0 -> {
                    bd.tvDeliveryStatus.text =
                        resources.getString(R.string.rate_delivery_boy) + " " + resources.getString(
                            R.string.not_so_great
                        )
                    bd.tvDeliveryIssue.text = resources.getString(R.string.what_was_issue)
                    setupDeliverReview(IConstants.RatingFlags.RATING_2_0)

                }

                IConstants.RatingFlags.RATING_3_0 -> {
                    bd.tvDeliveryStatus.text =
                        resources.getString(R.string.rate_delivery_boy) + " " + resources.getString(
                            R.string.not_satisfied
                        )
                    bd.tvDeliveryIssue.text = resources.getString(R.string.what_to_improve)
                    setupDeliverReview(IConstants.RatingFlags.RATING_3_0)

                }
                IConstants.RatingFlags.RATING_4_0 -> {

                    bd.tvDeliveryStatus.text =
                        resources.getString(R.string.rate_delivery_boy) + " " + resources.getString(
                            R.string.good
                        )
                    bd.tvDeliveryIssue.text = resources.getString(R.string.what_for_five_star)
                    setupDeliverReview(IConstants.RatingFlags.RATING_4_0)

                }
                IConstants.RatingFlags.RATING_5_0 -> {
                    bd.tvDeliveryStatus.text =
                        resources.getString(R.string.rate_delivery_boy) + "" + resources.getString(R.string.excellent)
                    bd.tvDeliveryIssue.text = resources.getString(R.string.what_did_you_like_most)
                    setupDeliverReview(IConstants.RatingFlags.RATING_5_0)

                }
            }
        }

        bd.btnSubmitRating.setOnClickListener {

            val tasteArray = selectedTasteList.split(",")
            val deliveryArray = selDeliveryFeedbackList.split(",")
            val tasteArrayList = ArrayList<RatingDTO>()
            tasteArrayList.add(RatingDTO(tasteArray, outLetRating.toDouble()))

            val deliveryArrayList = ArrayList<RatingDTO>()
            deliveryArrayList.add(RatingDTO(deliveryArray, deliveryRating.toDouble()))
            val selRatingFeedback = RatingFeedbackDTO(
                tasteArrayList,
                orderID,
                orderNum,
                deliveryArrayList,
                bd.edtSuggestions.text.toString().trim().replace("\\s+".toRegex(), " ")
                    .capitalize()
            )
           // Log.e("----1-------->>>", selRatingFeedback.toString())

            sendRating(selRatingFeedback)
        }
    }

    private fun sendRating(selRatingFeedback: RatingFeedbackDTO) {
        CleverTapHelper.submitRatting(selRatingFeedback)
        vm.getRatingFeedback(selRatingFeedback).observe(
            this,
            { result ->
                RHandler<Any>(this, result) {

                    actionHandler?.onSuccess(
                        moduleId,
                        mapOf("orderId" to orderID, "rating" to outLetRating.toString())
                    )
                    dialog?.dismiss()
                }
            })

    }

    private fun setupFoodReview() {

        val reviewItems = arrayListOf(
            FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/lunch.svg",
                "Packaging",
                "packaging",
                false
            ), FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/salad_3.svg",
                "Taste",
                "taste",
                false
            ), FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/ratings/ratings/pizza_3.svg",
                "Portion Size",
                "portion-size",
                false
            )
        )

        val section =
            FoodPrefSectionDTO(getString(R.string.diet_selection), FOOD_REVIEW_SELECTION, reviewItems, isMultiSelect = true)

        bd.cvFoodReview.withAddonSection(object :
            FoodPreferenceDialog.FoodPrefChangeListener {
            override fun onCustomisationChange(
                foodPrefSectionId: String,
                selectedList: List<FoodPreferenceDTO>
            ) {
                if (selectedList.isNotEmpty()) {
                    selectedTasteList =
                        selectedList.map { selectedItem ->
                            selectedItem.prefId
                        }.joinToString(", ")
                }
            }
        }, section)

    }

    private fun setupDeliverReview(rating: Float) {
        var testList = mutableListOf<String>()
        // bd.cvDeliveryReview.removeAllViews()

        when (rating) {

            IConstants.RatingFlags.RATING_1_0, IConstants.RatingFlags.RATING_2_0 -> {

                testList = mutableListOf<String>(
                    "Late delivery",
                    "Instructions not followed",
                    "Bad attitude",
                    "Careless handling",
                    "Others (Please mention)"
                )
            }
            IConstants.RatingFlags.RATING_3_0, IConstants.RatingFlags.RATING_4_0, IConstants.RatingFlags.RATING_5_0 -> {

                testList = mutableListOf<String>(
                    "Delivery duration",
                    "Package handling",
                    "Professionalism",
                    "Others (Please mention)"
                )
            }

        }
        bd.cvDeliveryReview.setItems(
            testList,
            fun(
                listener: OnGroupItemClickListener<FeedbackSelectionView>,
                item: String
            ): FeedbackSelectionView {
                val view = FeedbackSelectionView(requireContext())
                view.text = item
                view.setOnItemClickListener(listener)
                return view
            }
        )
        bd.cvDeliveryReview.setOnSelectedItemChangeListener(this)
        bd.cvDeliveryReview.setItemSelectedAt(0)
    }

    override fun skip() {}

    override fun showCodeError(code: Int?, title: String?, message: String) {}

    override fun onSuccess(actionId: Int, data: Any?) {
//        when (actionId) {
//            IConstants.ActionModule.APPLY_RATING -> {
//                actionHandler?.onSuccess(
//                    moduleId,
//                    mapOf("orderId" to orderID, "rating" to outLetRating.toString())
//                )
//                dialog?.dismiss()
//            }
//        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
//        actionHandler?.onSuccess(
//            actionId = IConstants.ManagerAddress.CLOSE_DIALOG,
//            data = null
//        )
    }

    override fun onSelectedItemChanged(list: MutableList<GroupItemActions<*>>, listType: Int) {
        when (listType) {
            R.id.cv_food_review -> {
                selectedTasteList =
                    list.map { view ->
                        (view as CuisineView).findViewById<TextView>(IConstants.CuisineModule.CUISINE_ID)
                            .text
                    }
                        .joinToString(", ")

            }

            R.id.cv_delivery_review -> {
                selDeliveryFeedbackList =
                    list.map { item -> (item as FeedbackSelectionView).text }.joinToString(", ")
            }
        }
    }

    override fun onShowMessage(message: String) {
    }


}