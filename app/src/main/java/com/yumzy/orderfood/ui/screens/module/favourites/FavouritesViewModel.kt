package com.yumzy.orderfood.ui.screens.module.favourites

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.favourites.data.FavouriteRestaurantRepository
import javax.inject.Inject

class FavouritesViewModel  constructor(private val repository: FavouriteRestaurantRepository) :
    BaseViewModel<IView>() {

    fun getFavouriteRestaurant() = repository.getFavouriteRestaurant()

}