package com.yumzy.orderfood.ui.screens.offer.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class OfferRepository  constructor(
    private val remoteSource: OfferRemoteDataSource
) {
    fun getFetchCouponList(userID: String, cartId: String?, outletID: String) = networkLiveData(
        networkCall = {
            remoteSource.getFetchCouponList(userID, cartId, outletID)
        }
    ).distinctUntilChanged()

    fun applyCoupon(applyCoupon: MutableMap<String, Any>) = networkLiveData<Any>(
        networkCall = {

            remoteSource.applyCoupon(applyCoupon)
        }
    ).distinctUntilChanged()



}