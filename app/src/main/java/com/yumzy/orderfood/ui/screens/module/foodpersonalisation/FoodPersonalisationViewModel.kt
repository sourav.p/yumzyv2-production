/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.foodpersonalisation

import com.yumzy.orderfood.data.models.FoodPersReqDTO
import com.yumzy.orderfood.data.models.PersonalisationDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data.FoodPersonalisationRepository
import javax.inject.Inject

class FoodPersonalisationViewModel  constructor(private val repository: FoodPersonalisationRepository) :
    BaseViewModel<IView>() {


    fun getFoodPreferenceTags(dto: PersonalisationDTO) = repository.getFoodPreferenceTags(dto)
    fun foodPres(dto: FoodPersReqDTO) = repository.foodPres(dto)


    fun getData(): MutableMap<String, List<String>> {
        val data = mutableMapOf<String, List<String>>()
        data.set(
            "South Indian",
            listOf(
                "Sago Kichdi",
                "Grilled Chichken",
                "Poori",
                "Vegan Korma",
                "Kodi Kura",
                "Pulusu",
                "Banana Curry",
                "Puran Poli",
                "Lemon Rice",
                "Squid Fry",
                "Samber",
                "Pungullu"
            )
        )

        data.set(
            "North Indian",
            listOf(
                "Chole Bhature",
                "Rogan Josh",
                "Stuffed Bati",
                "Malai ki Kheer",
                "Chichken Dum Biryani",
                "Aloo Samosa",
                "Nihari Gosht",
                "Dahi Bhalla"

            )
        )

        data.set(
            "West Indian",
            listOf(
                "Vada Pav",
                "Zunka Bhakri",
                "Bombay Duck",
                "Goan Fish Curry",
                "Bebinca",
                "Methi ka thepla",
                "Shrikhand",
                "Dhokla"
            )
        )

        data.set(
            "East Indain",
            listOf(
                "Khar from Assam",
                "Pitha From Assam",
                "Smoked pork from Nagaland",
                "Galho",
                "Thukpa",
                "Kelli Chana",
                "Tenga Fish",
                "Jadoh from Meghalaya"
            )
        )

        return data
    }

}