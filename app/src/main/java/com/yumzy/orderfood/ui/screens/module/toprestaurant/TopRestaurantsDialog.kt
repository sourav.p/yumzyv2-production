package com.yumzy.orderfood.ui.screens.module.toprestaurant

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ListElement
import com.yumzy.orderfood.data.models.RestaurantsResDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogTopRestaurantBinding
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.ui.screens.fastitems.BottomSheetListItem
import com.yumzy.orderfood.ui.screens.fastitems.LoadingItem
import com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter.TopOutletItem
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.util.IConstants
import io.cabriole.decorator.LinearMarginDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class TopRestaurantsDialog : BaseSheetFragment<DialogTopRestaurantBinding, TopRestaurantViewModel>(),
    ITopRestaurantView {

    private var listReachedEnd: Boolean = false
    private var skip: Int = 0
    private var limit: Int = 15

    override val vm: TopRestaurantViewModel by viewModel()

    //save our FastAdapter
    private lateinit var fastItemAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    var promoId: String = ""
    var title: String? = ""
    lateinit var heroImage: String
    var itemLoading = false
    override fun getLayoutId(): Int = R.layout.dialog_top_restaurant



    private fun setupPagingRecyclerView() {
        fastItemAdapter = FastItemAdapter()
        val selectExtension = fastItemAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        footerAdapter = ItemAdapter.items()
        fastItemAdapter.addAdapter(1, footerAdapter)


//        bd.rvRestaurants.addItemDecoration(VerticalItemDecoration(5.px, false))
        bd.rvRestaurants.addItemDecoration(
            LinearMarginDecoration(
                leftMargin = 8.px,
                topMargin = 8.px,
                rightMargin = 8.px,
                bottomMargin = 8.px,
                orientation = RecyclerView.VERTICAL
            )
        )
        bd.rvRestaurants.layoutManager = LinearLayoutManager(context)
        /* bd.rvRestaurants.itemAnimator = SlideUpAlphaAnimator().apply {
             addDuration = 600
             removeDuration = 200
         }*/
        bd.rvRestaurants.adapter = fastItemAdapter
        fastItemAdapter.attachDefaultListeners = true

        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = LoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                getRestaurantListItems()
            }
        }
        bd.rvRestaurants.addOnScrollListener(endlessRecyclerOnScrollListener)

        getRestaurantListItems()
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<GenericItem>, item: GenericItem, _: Int ->
                when (item) {
                    is TopOutletItem -> {
                        onRestaurantSelected(item.model)
                    }
                }
                true
            }
//        OverScrollDecoratorHelper.setUpOverScroll(bd.rvRestaurants,OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
    }

    private fun getRestaurantListItems() {
        if (listReachedEnd) {
            footerAdapter.clear()

            return
        }
        vm.getRestaurants(promoId, skip, limit).observe(
            this@TopRestaurantsDialog,
            Observer { result ->
                itemLoading = true
//                CleverTapHelper.seeMoreScrolled(title, promoId, skip)
                if (result.type == APIConstant.Status.LOADING) {
                    footerAdapter.clear()
                    val progressItem = LoadingItem()
                    progressItem.isEnabled = false
                    footerAdapter.add(progressItem)
                    return@Observer
                }
                RHandler<RestaurantsResDTO>(
                    this@TopRestaurantsDialog,
                    result
                ) { restaurantResDto ->
                    footerAdapter.clear()
                    if (restaurantResDto.list.isNullOrEmpty()) {
                        listReachedEnd = true
                        footerAdapter.add(BottomSheetListItem())
                        vm.getRestaurants(promoId, skip, limit)
                            .removeObservers(this@TopRestaurantsDialog)
                    }

                    val items = ArrayList<TopOutletItem>()
                    restaurantResDto.list.forEach { listElement ->
                        val topOutletItem = TopOutletItem(listElement)
                        items.add(topOutletItem)
                    }
                    fastItemAdapter.add(items)
                    skip++
                }

            })
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        bd.noDataToDisplay.visibility = View.VISIBLE
        bd.rvRestaurants.visibility = View.GONE
        when (code) {
            APIConstant.Status.NO_NETWORK -> {
                bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
                bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
                bd.noDataToDisplay.imageResource = R.drawable.img_no_internet

            }
            APIConstant.Code.NOT_SERVING -> {
                bd.noDataToDisplay.title = title ?: getString(R.string.noting_to_display)
                bd.noDataToDisplay.subTitle = message
                bd.noDataToDisplay.imageResource = R.drawable.img_unserviceable
            }
            APIConstant.Code.NO_DELIVERY_PARTNER -> {
                bd.noDataToDisplay.title = title ?: IConstants.OrderTracking.DELIVERY_UNAVAILABLE
                bd.noDataToDisplay.subTitle = message
                bd.noDataToDisplay.imageResource = R.drawable.img_deliviry_issue

            }
            else -> super.showCodeError(code, title, message)
        }
    }

    override fun showFilterOptions() {
//        bd.includeBottomFilter.cvFilterAction.visibility = View.VISIBLE

    }

    override fun hideFilterOptions() {
//        bd.includeBottomFilter.cvFilterAction.visibility = View.GONE
    }

    override fun applyFilter() {
    }

    override fun clearAllFilter() {
    }

    private fun onRestaurantSelected(listElement: ListElement) {
        val intent = Intent(context, OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, listElement.outletId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
//        dismiss()
    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }

    override fun onDebounceClick(view: View) {
        when (view.id) {

//            R.id.btnFilter -> showFilterOptions()
            R.id.btnClose -> hideFilterOptions()
            R.id.btnClearAll -> clearAllFilter()
            R.id.btnApply -> applyFilter()
        }
    }

    override fun onDialogCreated(view: View) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbProfileToolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        bd.tbProfileToolbar.title = title
        bd.tbProfileToolbar.title =
            title?.split(" ")?.map { item -> item.capitalize() }?.joinToString(" ")
        bd.tbProfileToolbar.setNavigationOnClickListener { this.dismiss() }

        setupPagingRecyclerView()
        bd.noDataToDisplay.actionCallBack = {
            bd.noDataToDisplay.visibility = View.GONE
            bd.rvRestaurants.visibility = View.VISIBLE
            getRestaurantListItems()
        }
//        context?.let {
//            YUtils.setImageInView(bd.ivTopRestaurantHeader, heroImage)
//        }
    }


}
