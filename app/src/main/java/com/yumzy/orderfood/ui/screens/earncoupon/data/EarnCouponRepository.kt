package com.yumzy.orderfood.ui.screens.earncoupon.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class EarnCouponRepository  constructor(
    private val remoteSource: EarnCouponRemoteDataSource
) {
    fun getFetchCouponList(userID: String, cartId: String?, outletId: String?) = networkLiveData(
        networkCall = {
            remoteSource.getFetchCouponList(userID, cartId, outletId)
        }
    ).distinctUntilChanged()

}