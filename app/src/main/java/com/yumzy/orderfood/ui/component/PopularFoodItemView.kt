package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

class PopularFoodItemView : LinearLayout {
    var title: String = ""
        set(value) {
            val view = findViewById<TextView>(IConstants.PopularFoodItemView.VIEW_TITLE)
            view.text = value
            field = value
        }

    var subtitle: String = ""
        set(value) {
            val view = findViewById<TextView>(IConstants.PopularFoodItemView.VIEW_SUBTITLE)
            view.text = value
            field = value
        }

    var isVeg: Boolean = false
        set(value) {
            val view =
                findViewById<LinearLayout>(IConstants.PopularFoodItemView.VIEW_PRICE_CONTAINER)
            (view.getChildAt(0) as FontIconView).setTextColor(if (value) ThemeConstant.greenConfirmColor else ThemeConstant.redCancelColor)
            field = value
        }

    var price: Double = 0.0
        set(value) {
            val view =
                findViewById<LinearLayout>(IConstants.PopularFoodItemView.VIEW_PRICE_CONTAINER)
            (view.getChildAt(1) as TextView).appendPriceValue(price)
            field = value
        }

    var imageUrl: String = ""
        set(value) {
            val view = findViewById<RoundedImageView>(IConstants.PopularFoodItemView.VIEW_IMAGE)
            YUtils.setImageInView(view, imageUrl)
            field = value
        }

    var outletId: String = ""

    var itemId: String = ""

    var isAddButtonVisible: Boolean = true
        set(value) {
            val view =
                findViewById<LinearLayout>(IConstants.PopularFoodItemView.VIEW_RIGHT_CONTAINER)
            if (value) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
            field = value
        }

    var buttonColor = ThemeConstant.greenAddItem

    var addDrawable = DrawableUtils.getRoundDrawable(this.buttonColor, 8f)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        this.apply {
            orientation = HORIZONTAL
            layoutParams = LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        }

        initViews()

    }

    private fun initViews() {

        this.addView(getRoundImageView(imageUrl, !isAddButtonVisible))

        this.addView(LinearLayout(context).apply {
            id = IConstants.PopularFoodItemView.VIEW_RIGHT_CONTAINER
            this.orientation = VERTICAL
//            this.setPadding(UIHelper.i10px, UIHelper.i3px, UIHelper.i10px, UIHelper.i10px)
            layoutParams = LayoutParams(120.px, ViewConst.MATCH_PARENT).apply {
                setMargins(VUtil.dpToPx(4), VUtil.dpToPx(4), VUtil.dpToPx(4), VUtil.dpToPx(4))
            }

            this.addView(getTextView(title, "title", 16f))
            this.addView(getTextView(subtitle, "subtitle", 10f))
            this.addView(getVegView(isVeg, price))
            if (isAddButtonVisible) {
                val layout = LinearLayout(context)
                layout.id = IConstants.PopularFoodItemView.VIEW_BUTTON_CONTAINER
                layout.layoutParams =
                    LayoutParams(UIHelper.i100px, UIHelper.i40px)
                layout.orientation = VERTICAL
                layout.gravity = Gravity.BOTTOM
                layout.addView(addQuantityButton())
                layout.setClickScaleEffect()
                this.addView(layout)
            }
        })
    }

    fun setValue(
        title: String,
        subtitle: String,
        isVeg: Boolean,
        price: Double,
        imageUrl: String,
        outletId: String,
        itemId: String,
        buttonText: String = "ADD",
        isAddButtonVisible: Boolean = true
    ) {

        this.title = title
        this.subtitle = subtitle
        this.isVeg = isVeg
        this.price = price
        this.imageUrl = imageUrl
        this.outletId = outletId
        this.itemId = itemId


    }

    private fun getRoundImageView(imageUrl: String, isSquare: Boolean): View {

        return RoundedImageView(context).apply {
            id = IConstants.PopularFoodItemView.VIEW_IMAGE
            if (isSquare) {
                this.layoutParams = LayoutParams(120.px, 120.px)
            } else {
                this.layoutParams = LayoutParams(120.px, 150.px)
            }
            this.cornerRadius = UIHelper.cornerRadius
            this.scaleType = ImageView.ScaleType.CENTER_CROP
//            Glide.with(this).load(imageUrl).into(this)
            YUtils.setImageInView(this, imageUrl)

        }

    }

    private fun getTextView(title: String, type: String, textSize: Float): TextView {
        return TextView(context).apply {
            text = title
            setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
            maxLines = 2
            ellipsize = TextUtils.TruncateAt.END
            when (type) {
                "title" -> {
                    id = IConstants.PopularFoodItemView.VIEW_TITLE
                    setInterBoldFont()
                    setTextColor(ThemeConstant.logoText)

//                    setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
                }
                "subtitle" -> {
                    id = IConstants.PopularFoodItemView.VIEW_SUBTITLE
                    setInterThineFont()
                    setTextColor(ThemeConstant.textGrayColor)
                    this.layoutParams =
                        LayoutParams(ViewConst.MATCH_PARENT, 0, 1f); gravity = Gravity.TOP
//                    setPadding(UIHelper.i1px, UIHelper.i1px, UIHelper.i1px, UIHelper.i1px)
                }
            }
        }
    }

    private fun getVegView(isVeg: Boolean, price: Double): View {
        return LinearLayout(context).apply {
            id = IConstants.PopularFoodItemView.VIEW_PRICE_CONTAINER
            this.orientation = HORIZONTAL

            this.addView(FontIconView(context).apply {
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f)
                if (isVeg)
                    setTextColor(ThemeConstant.greenConfirmColor)
                else
                    setTextColor(ThemeConstant.redCancelColor)

                text = context.getString(R.string.icon_veg_non_veg)
                val i5px = VUtil.dpToPx(5)
                setPadding(i5px, i5px, 0, 0)
                layoutParams = LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT
                )

            })

            this.addView(TextView(context).apply {
//                text = "₹${price.withPrecision(0)}"
                appendPriceValue(price)
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                setTextColor(ThemeConstant.textDarkGrayColor)
                setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
                setInterBoldFont()
            })
//            gravity=Gravity.END or Gravity.START

        }
    }

/*
    private fun getAddButton(): LinearLayout {

        val group = LinearLayout(context)
        group.layoutParams = LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        group.orientation = VERTICAL
        group.addView(addQuantityButton())
        return group
    }
*/

    private fun addQuantityButton(): View {
        val addButton = TextView(context)
        addButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        addButton.setPadding(
            8.px,
            5.px,
            8.px,
            3.px,
        )
        addButton.text = context.resources.getString(R.string.add_dish)
        addButton.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        addButton.gravity = Gravity.CENTER
//        addButton.setPadding(15, 8, 15, 8)
        addButton.setInterRegularFont()

        addButton.setTextColor(Color.WHITE)
        addButton.background = addDrawable
        return addButton
    }

    fun getRoundImageView(): RoundedImageView? {
        return this.findViewById(IConstants.PopularFoodItemView.VIEW_IMAGE)
    }

    fun getProgressBar(): ProgressBar? {
        return findViewById(R.id.pb_item_processeing)
    }

    fun getAddButton(): View? {
        return this.findViewById(IConstants.PopularFoodItemView.VIEW_BUTTON_CONTAINER)
    }

}
