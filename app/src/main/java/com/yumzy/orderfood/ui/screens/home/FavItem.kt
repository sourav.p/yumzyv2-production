package com.yumzy.orderfood.ui.screens.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FavOutlet
import com.yumzy.orderfood.databinding.AdapterTopRestaurantBinding
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.screens.module.favourites.FavOutletListener
import com.yumzy.orderfood.util.JsonHelper
import com.yumzy.orderfood.util.viewutils.YumUtil

class FavItem : AbstractBindingItem<AdapterTopRestaurantBinding>() {

    internal var data: FavOutlet? = null
    var itemListener: FavOutletListener? = null
    override val type: Int
        get() = R.id.fav_res_list

    override fun bindView(binding: AdapterTopRestaurantBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)


        binding.imageUrl = data?.imageUrl
        binding.title = data?.itemName
        binding.subtitle = data?.locality
        binding.cuisines = JsonHelper.arrayToString(data?.cuisines)
        if (!data?.offers.isNullOrEmpty()) {
            val zoneOffer = YumUtil.setZoneOfferString(data?.offers)
            binding.offer = zoneOffer
        } else {
            binding.offer = ""
        }
        if (!data?.costForTwo.isNullOrEmpty()) {
            binding.duration = "${data?.costForTwo} for one"
        } else {
            binding.duration = ""

        }
        binding.root.setClickScaleEffect()
        binding.root.setOnClickListener {
            itemListener?.onItemClick(data?.itemId)
        }
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterTopRestaurantBinding {
        return AdapterTopRestaurantBinding.inflate(inflater, parent, false)
    }

    fun withListener(listener: FavOutletListener): FavItem {
        this.itemListener = listener
        return this
    }

    fun withOutlet(outlet: FavOutlet): FavItem {
        this.data = outlet
        return this
    }


}