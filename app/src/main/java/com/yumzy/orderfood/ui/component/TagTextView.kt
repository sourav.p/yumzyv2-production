/*
 * Created By Shriom Tripathi 4 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.IConstants
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils

class TagTextView : LinearLayout, GroupItemActions<TagTextView>, View.OnClickListener {

    private lateinit var mOnGroupItemClickListener: OnGroupItemClickListener<TagTextView>

    private val i8px = VUtil.dpToPx(8)
    private val i4px = VUtil.dpToPx(4)
//    private val i12px = VUtil.dpToPxFloat(12f)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        val lp = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        lp.setMargins(i8px, i4px, i8px, i4px)

        val textView = TextView(context)
        textView.id = IConstants.TagTextView.VIEW_TEXT_TAG
        textView.setPadding(i4px, i4px, i4px, i4px)
        textView.setInterFont()
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)

        this.addView(textView)
        this.setOnClickListener(this)
    }

    fun setText(text: String) {
        if (text.isEmpty())
            return
        else
            this.findViewById<TextView>(IConstants.TagTextView.VIEW_TEXT_TAG).text = text
    }


    override fun onClick(v: View?) {
        mOnGroupItemClickListener.onGroupItemClicked(this)
    }

    override var mIsSelected: Boolean = false
        set(value) {
            if (value) {
                setSelectedBackground()
            } else {
                setDeselectedBackground()
            }
            field = value
        }

    override fun setSelectedBackground() {
        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.lightAlphaGray,
            VUtil.dpToPx(15).toFloat(),
            ThemeConstant.lightAlphaGray,
            VUtil.dpToPx(15).toFloat(),
            ThemeConstant.transparent, 2
        )
    }

    override fun setDeselectedBackground() {
        this.background = null
    }

    override fun setOnItemClickListener(value: OnGroupItemClickListener<TagTextView>) {
        mOnGroupItemClickListener = value
    }

    override fun compareTo(other: GroupItemActions<*>): Int {
        val condition =
            this.findViewById<TextView>(IConstants.TagTextView.VIEW_TEXT_TAG).text == (other as TagTextView).findViewById<TextView>(
                IConstants.TagTextView.VIEW_TEXT_TAG
            ).text

        return if (condition) {
            1
        } else {
            0
        }
    }
}