package com.yumzy.orderfood.ui.screens.rewards

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class RewardsViewModel  constructor(
    private val repository: RewardsRepository
) : BaseViewModel<IMyRewards>() {
//    fun getAppUser() = repository.getUserDTO()
fun getScratchCardInfo() =
    repository.getScratchCardInfo()

}