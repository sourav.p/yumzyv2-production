/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils

class OnPlateRestaurantHeader : LinearLayout {

    var mTitle = ""
        set(value) {
            this.findViewById<TextView>(IConstants.OnPlaceRestaurantHeader.VIEW_TEXT_TITLE).text =
                value
            field = value
        }

    var mSubtitle = ""
        set(value) {

            val tvSubTitle =
                findViewById<TextView>(IConstants.OnPlaceRestaurantHeader.VIEW_TEXT_SUB_TITLE)
            tvSubTitle?.hideEmptyTextView(value)
            tvSubTitle?.leftDrawableIcon(context.getString(R.string.icon_location_1))
            tvSubTitle?.setInterRegularFont()

            field = value
        }
    var mImage = ""
        set(value) {
            val imageView =
                this.findViewById<ImageView>(IConstants.OnPlaceRestaurantHeader.OUTLET_IMAGE)
            field = value
//            Glide.with(context).load(value).into(imageView)
            YUtils.setImageInView(imageView, value)

        }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {


        intiViews()

        if (atr != null) {

            val a = context.obtainStyledAttributes(
                atr,
                R.styleable.OnPlateRestaurantHeaderView,
                style,
                0
            )

            mTitle = a.getString(R.styleable.OnPlateRestaurantHeaderView_addRestaurantTitle) ?: ""
            mImage = a.getString(R.styleable.OnPlateRestaurantHeaderView_addRestaurantTitle) ?: ""
            mSubtitle =
                a.getString(R.styleable.OnPlateRestaurantHeaderView_addRestaurantImage) ?: ""
            a.recycle()
        }

    }

    val i4px = VUtil.dpToPx(4)
    val i62px = VUtil.dpToPx(62)
    val i10px = VUtil.dpToPx(10)
    val i5px = VUtil.dpToPx(5)
    val i1px = VUtil.dpToPx(1)

    private fun intiViews() {

        this.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        this.orientation = HORIZONTAL

        val imageView = restaurantImageView()

        val lp = LinearLayout.LayoutParams(i62px, i62px)
        lp.setMargins(i4px, i4px, i4px, i4px)
        imageView.layoutParams = lp

        this.addView(imageView)

        val linear = LinearLayout(context)
        linear.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        linear.orientation = VERTICAL

        linear.addView(
            getTextView(
                mTitle,
                14f,
                "header",
                IConstants.OnPlaceRestaurantHeader.VIEW_TEXT_TITLE
            )
        )
        linear.addView(
            getTextView(
                mSubtitle,
                12f,
                "subhreader",
                IConstants.OnPlaceRestaurantHeader.VIEW_TEXT_SUB_TITLE
            )
        )
        linear.addView(
            getTextView(
                "View Menu",
                12f,
                "viewmore",
                IConstants.OnPlaceRestaurantHeader.VIEW_TEXT_VIEW_MORE
            )
        )

        this.addView(linear)

    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, 0)
        imageParam.dimensionRatio = "1:1"

        imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = IConstants.OnPlaceRestaurantHeader.OUTLET_IMAGE
        roundImage.cornerRadius = 4.pxf
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }

    private fun getTextView(text: String, textSize: Float, type: String, id: Int): TextView? {

        val textView = TextView(context)
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = id
        textView.setPadding(i10px, i4px, i4px, i1px)

        when (type) {
            "header" -> {
                textView.setInterBoldFont()
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            "subheader" -> {
                textView.setTextColor(
                    ResourcesCompat.getColor(
                        resources,
                        R.color.cool_gray,
                        context.theme
                    )
                )

            }
            "viewmore" -> {
                textView.setInterBoldFont()
                textView.setTextColor(
                    ResourcesCompat.getColor(
                        resources,
                        R.color.blueJeansDark,
                        context.theme
                    )
                )
                // textView.setClickListenerBackground()
                //textView.setOnClickListener(this)
            }

        }

        val lp = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        textView.layoutParams = lp
        return textView
    }

}