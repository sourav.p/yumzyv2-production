package com.yumzy.orderfood.ui.screens.profile

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentEditProfileBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.home.fragment.updateProfile
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.templetes.EditProfileInfoTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.zoho.livechat.android.ZohoLiveChat
import com.zoho.salesiqembed.ZohoSalesIQ
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.*
import java.util.*


class ProfileFragment : BaseFragment<FragmentEditProfileBinding, ProfileViewModel>(),
    IProfileView, EditProfileInfoTemplate.UpdateSingleInfoListener,
    ActionDialog.OnActionPerformed {
    override val vm: ProfileViewModel by viewModel()

    val cartHelper: CartHelper by inject()


    private val GALLERY = 4
    private var CAMERA: Int = 3

    private val RESULT_LOAD_IMAGE = 1

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_edit_profile
    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav_view).visibility =
            View.GONE

        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        bd.toolbar.title = resources.getString(R.string.profile)
        bd.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

        initComponent()
        applyDebouchingClickListener(bd.imvProfile, bd.btnSave, bd.btnInvite)
        setHasOptionsMenu(true);
        setUserDetail()


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_signout -> {
                signOutUser()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initComponent() {
        bd.appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                val min_height = ViewCompat.getMinimumHeight(bd.collapsingToolbar) * 2
                val scale = (min_height + verticalOffset).toFloat() / min_height
                bd.profileImgLayout.setScaleX(if (scale >= 0f) scale else 0f)
                bd.profileImgLayout.setScaleY(if (scale >= 0f) scale else 0f)
                bd.imvProfile.isClickable = scale >= 0f
            })
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_profile, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        if (item.itemId == R.id.menu_signout) {
//            signOutUser()
//        }
//        return super.onOptionsItemSelected(item)
//    }


    private fun setUserDetail() {
        bd.firstName = sharedPrefsHelper.getUserName()
        bd.lastName = sharedPrefsHelper.getUserLastName()
        bd.email = sharedPrefsHelper.getUserEmail()
        bd.mobile = sharedPrefsHelper.getUserPhone()
        bd.city = sharedPrefsHelper.getCityLocation()
        bd.tvReferralCode.text = sharedPrefsHelper.getUserReferralCode()
        if (sharedPrefsHelper.getUserReferralCode().equals("")) {
            bd.clInviteEarn.visibility = View.GONE
        } else {
            bd.clInviteEarn.visibility = View.VISIBLE

        }
//        val iconFontDrawable = IconFontDrawable(this, R.string.icon_user_2)
//        iconFontDrawable.setTextColor(Color.GRAY)
//        iconFontDrawable.textSize = 50f
        Glide.with(this)
            .load(sharedPrefsHelper.getUserPic().toString())
            .placeholder(R.drawable.user_placeholder)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .apply(
                RequestOptions()
                    .circleCrop()
            )
            .into(bd.imvProfile)

    }


    private fun openGallery() {
        val intent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(intent, GALLERY)

    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) {
            return
        }
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            val selectedImage = data.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                requireActivity().contentResolver.query(
                    selectedImage!!,
                    filePathColumn,
                    null,
                    null,
                    null
                )
            cursor!!.moveToFirst()
            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(columnIndex)
            cursor.close()
            // bd.ivUserProfile.setImageBitmap(BitmapFactory.decodeFile(picturePath))
        } else if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    var bitmap =
                        MediaStore.Images.Media.getBitmap(
                            requireActivity().contentResolver,
                            contentURI
                        )
                    bitmap = getResizedBitmap(bitmap, 300)
                    val path: String? = saveImage(bitmap)
                    // bd.ivUserProfile.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                    showToast("Failed!")
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!["data"] as Bitmap?
            //    bd.ivUserProfile.setImageBitmap(thumbnail)
            thumbnail?.let { saveImage(it) }
        }
    }

    private fun saveImage(myBitmap: Bitmap): String? {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes)
        val destination = File(
            Environment.getExternalStorageDirectory(),
            System.currentTimeMillis().toString() + ".jpg"
        )

        val fo: FileOutputStream
        try {
            destination.createNewFile()
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
            sendProfileImage(destination)
            return destination.absolutePath
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ""
    }


    fun sendProfileImage(f: File) {

        var bm = BitmapFactory.decodeFile(f.absolutePath)
        bm = getResizedBitmap(bm, 300)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 70, baos) //bm is the bitmap object
        val b = baos.toByteArray()
        val encodedString = Base64.encodeToString(b, 0)
        val request: MutableMap<String, Any> =
            HashMap()
        request["originalName"] = "image.jpeg"
        request["fileData"] = "data:image/jpeg;base64,$encodedString"
        val job: MutableMap<String, Any> =
            HashMap()
        job["profilePicLink"] = request

        updateUserProfile(job)

    }

    private fun updateUserProfile(pro: MutableMap<String, Any>) {

        vm.userUpdateProfileImage(pro).observe(this, { result ->
            RHandler<Map<String, String>>(
                this,
                result
            ) {
                try {
                    if (it.containsKey("profilePicLink")) {
                        sharedPrefsHelper.saveUserPic(it["profilePicLink"].toString())
                        setUserDetail()
                    }
                } catch (e: Exception) {

                } finally {
                    showSnackBar(getString(R.string.profile_pic_updated_successfully), -1)
                    setUserDetail()
                }
            }
        })
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    override fun sendData(text: String, userKey: String) {

        val map: Map<String, String> = mapOf(userKey to text)

        vm.userUpdateDetails(map).observe(this, { result ->
            RHandler<Any>(
                this,
                result
            ) {
                setUserDetail()

                sharedPrefsHelper.saveUserName(text.capitalize())
                showSnackBar(result.message.toString(), -1)
            }

        })
    }

    //    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return super.onSupportNavigateUp()
//    }
    fun hideKeyboard() {
        YumUtil.hideKeyboard(requireActivity())

    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            bd.imvProfile.id -> {
                callAskPermission()
            }

            bd.btnInvite.id -> {
                AppIntentUtil.launchWithSheetAnimation(
                    requireActivity(),
                    InviteActivity::class.java
                )


            }
            bd.btnSave.id -> {

                if (bd.firstName.toString().trim().isEmpty()) {
                    hideKeyboard()
                    showSnackBar(resources.getString(R.string.invalid_first_name), -1)
                    return
                }

                if (bd.lastName.toString().trim().isEmpty()) {
                    hideKeyboard()
                    showSnackBar(resources.getString(R.string.invalid_last_name), -1)
                    return
                }

                val updatedMap = mapOf(
                    "name" to bd.firstName.toString(),
                    "lastName" to bd.lastName.toString(),
                    "email" to bd.email.toString()
                )

                vm.userUpdateDetails(updatedMap).observe(this, { result ->
                    RHandler<Any>(
                        this,
                        result
                    ) {

                        updateProfile = true
                        sharedPrefsHelper.saveUserName(bd.firstName.toString())
                        sharedPrefsHelper.saveUserLastName(bd.lastName.toString())
                        sharedPrefsHelper.saveUserEmail(bd.email.toString())
                        showSnackBar(result.message.toString(), -1)
                        setUserDetail()
                        zohoSetupVisitor()
                    }

                })

            }
        }
    }

    private fun callAskPermission() {

        Dexter.withContext(requireActivity())
            .withPermissions(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    report.let {
                        if (report.areAllPermissionsGranted()) {

                            uploadPeofileDialog()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            displayNeverAskAgainDialog()
                        }

                    }


                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()

                }
            }).check()

    }

    private fun displayNeverAskAgainDialog() {
        val title = getString(R.string.enable_permission)
        val sMessage = getString(R.string.permit_permission)
        val actions: ArrayList<ActionItemDTO?> = arrayListOf(
            ActionItemDTO(
                key = ActionConstant.POSITIVE,
                textColor = ThemeConstant.blueJeans,
                buttonColor = ThemeConstant.pinkies,
                text = getString(R.string.permit_manually)
            ),
            ActionItemDTO(
                key = ActionConstant.CLOSE,
                text = getString(R.string.cancel)
            )
        )
        profileActionDialog(
            IConstants.ActionModule.ENABLE_PERMISSION,
            title,
            sMessage,
            actions
        )


    }

    private fun uploadPeofileDialog() {
        val title = getString(R.string.upload_profile)
        val sMessage = getString(R.string.from_camera_or_gallery)
        val actions: ArrayList<ActionItemDTO?> = arrayListOf(
            ActionItemDTO(
                key = ActionConstant.POSITIVE,
                text = getString(R.string.gallery),
            ),
            ActionItemDTO(
                key = ActionConstant.NUTURAL,
                text = getString(R.string.camera),
            )
        )
        profileActionDialog(
            IConstants.ActionModule.UPLOAD_PROFILE,
            title,
            sMessage,
            actions,
            resources.getString(R.string.icon_picture_o)
        )

    }

    override fun signOutUser() {
        val title = getString(R.string.logout)
        val sMessage = getString(R.string.are_you_want_to_logout)
        updateProfile = true
        val actions: ArrayList<ActionItemDTO?> = arrayListOf(
            ActionItemDTO(
                key = ActionConstant.POSITIVE,
                text = "Logout",
                textColor = ThemeConstant.textGrayColor,
                buttonColor = null
            ), ActionItemDTO(
                key = ActionConstant.CLOSE,
                text = getString(R.string.close),
                textColor = ThemeConstant.pinkies,
                buttonColor = null
            )
        )
        profileActionDialog(
            IConstants.ActionModule.SIGN_OUT,
            title,
            sMessage,
            actions,
            resources.getString(R.string.icon_logout)
        )

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        showSnackBar(message, -1)
    }

    fun profileActionDialog(
        actionType: Int,
        title: CharSequence,
        sMessage: CharSequence,
        actions: ArrayList<ActionItemDTO?>,
        iconString: String = "\uE0F6"
    ) {
        ActionModuleHandler.showChoseActionDialog(
            requireActivity(),
            actionType,
            title,
            sMessage,
            actions,
            this,
            iconString
        )
    }


    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {
        if (actionType == IConstants.ActionModule.ENABLE_PERMISSION) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri: Uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivity(intent)
            }

        }
        if (actionType == IConstants.ActionModule.UPLOAD_PROFILE) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                openGallery()
            }
            if (action != null && action.key == ActionConstant.NUTURAL) {
                openCamera()
            }

        }
        if (actionType == IConstants.ActionModule.SIGN_OUT) {
            if (action != null && action.key == ActionConstant.POSITIVE) {

                CleverTapHelper.userLogout()
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        vm.logoutUser().observe(requireActivity(), Observer { result ->
                            if (result.type == APIConstant.Status.LOADING) {
                                showProgressBar()
                                return@Observer
                            }

                            hideProgressBar()
                            vm.clearUser()
                            cartHelper.clearAllSelected()
                            vm.removeSelectedAddress()

                            requireActivity().applicationContext.databaseList().forEach {
                                if (!it.contains("yumzy_app"))
                                    requireActivity().applicationContext.deleteDatabase(it)
                            }
                            sharedPrefsHelper.saveIsTemporary(true)
                            sessionManager.removeAuthToken()
                            ZohoLiveChat.unregisterVisitor(requireActivity().applicationContext)
                            sharedPrefsHelper.setSelectedAddress(null)
                            AppGlobalObjects.selectedAddress = null


                        })

                    }, 1000
                )

            }
        }

    }

    private fun zohoSetupVisitor() {

//        ZohoSalesIQ.registerVisitor(sharedPrefsHelper.getUserPhone())
        if (sharedPrefsHelper.getIsTemporary()) {
            ZohoSalesIQ.Visitor.setName("")
            ZohoSalesIQ.Visitor.setEmail("")
            ZohoSalesIQ.Visitor.setContactNumber("")
        } else {
            ZohoSalesIQ.Visitor.setName(sharedPrefsHelper.getUserName())
            ZohoSalesIQ.Visitor.setEmail(sharedPrefsHelper.getUserEmail())
            ZohoSalesIQ.Visitor.setContactNumber(sharedPrefsHelper.getUserPhone())
        }
//        ZohoSalesIQ.Visitor.addInfo("User Type","Premium"); //Custom visitor information
    }

}

