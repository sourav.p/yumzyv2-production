/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.helpfaq.faq

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

class HelpHeadingViewModel  constructor(private val repository: ProfileRepository) :
    BaseViewModel<IView>() {

    fun getHeading() = repository.getHelpHeadings()

    fun getAppUser() = repository.getUserDTO()
}