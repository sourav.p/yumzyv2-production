package com.yumzy.orderfood.ui.screens.new.outlet

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.location.Location
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.invalidateOptionsMenu
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.*
import androidx.viewbinding.ViewBinding
import com.birjuvachhani.locus.Locus
import com.birjuvachhani.locus.isFatal
import com.clevertap.android.geofence.CTGeofenceAPI
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.*
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericFastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter.Companion.items
import com.mikepenz.fastadapter.binding.listeners.addClickListener
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.*
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.common.recyclermargin.LayoutMarginDecoration
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.ui.screens.location.ILocationView
import com.yumzy.orderfood.ui.screens.location.LocationPickrActivity
import com.yumzy.orderfood.ui.screens.location.LocationViewModel
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.restaurant.*
import com.yumzy.orderfood.ui.screens.restaurant.adapter.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils.toBounds
import com.yumzy.orderfood.util.intentShareText
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable
import io.cabriole.decorator.DecorationLookup
import io.cabriole.decorator.LinearDividerDecoration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

class OutletSearchFragment : BaseFragment<FragmentOutletSearchBinding, RestaurantViewModel>(),
    IRestaurantSearch, OutletInteraction, OutletSearchView.OnQueryTextListener,
    AddItemView.AddItemListener, ItemFilterListener<BindingRegularMenuItem>  {

    private var selectedItems: ArrayList<ValuePairSI> = ArrayList()

    var isFirstLoad = true

    var totalMenuSize = -1

    private val itemPosition = AtomicInteger(0)

    override val vm: RestaurantViewModel by viewModel()

    val cartHelper: CartHelper  by inject()

    private val itemAdapter: ItemAdapter<BindingRegularMenuItem> by lazy {
        items()
    }

    private val mFastAdapter: FastAdapter<BindingRegularMenuItem> by lazy {
        FastAdapter.with(itemAdapter)
    }

    private val activity by lazy {
        requireActivity() as AppCompatActivity
    }

    private val navController by lazy {
        findNavController()
    }

    private val navArgs by lazy {
        navArgs<OutletSearchFragmentArgs>().value
    }

    private val isFilterEnabled by lazy {
        navArgs.isFilterEnabled
    }

    override fun getLayoutId() = R.layout.fragment_outlet_search

    override fun onFragmentReady(view: View) {
//        vm.attachView(this)


        BarUtils.setStatusBarLightMode(activity, true)
        BarUtils.setStatusBarColor(activity, Color.WHITE)

        itemAdapter.itemFilter.filterPredicate =
            { item: BindingRegularMenuItem, constraint: CharSequence? ->
                (item.restaurantItem?.name?.toLowerCase(Locale.getDefault())
                    ?.contains(constraint.toString().toLowerCase(Locale.getDefault())) ?: false ||
                        item.restaurantItem?.description?.toLowerCase(Locale.getDefault())
                            ?.contains(
                                constraint.toString().toLowerCase(Locale.getDefault())
                            ) ?: false)
            }

        itemAdapter.itemFilter.itemFilterListener = this

        setupSearchView()
        setupRecyclerView()
        subScribeSelectedItems()
        bd.rvRestaurantItems.addItemDecoration(LayoutMarginDecoration(1, UIHelper.i5px))
        bd.rvRestaurantItems.layoutManager = LinearLayoutManagerWrapper(activity)
        bd.rvRestaurantItems.itemAnimator = DefaultItemAnimator()
        bd.rvRestaurantItems.adapter = mFastAdapter

    }

    private fun setupSearchView() {
        bd.searchView.showSearch(false)
        bd.searchView.setOnQueryTextListener(this)
    }

    private fun setupRecyclerView() {
        val decoration = LinearDividerDecoration.create(
            color = ThemeConstant.whiteSmoke,
            size = 1.px,
            leftMargin = 0.px,
            topMargin = 20.px,
            rightMargin = 0.px,
            bottomMargin = 20.px,
            orientation = RecyclerView.VERTICAL
        )
        bd.rvRestaurantItems.addItemDecoration(decoration)
        vm.getRestaurantDatabase()?.observe(this, { restaurant ->
            if (restaurant != null) {

                val menuSections = restaurant.menuSectionDTOS!!.toMutableList()

                var commonMenuListDTO: RestaurantMenuSectionDTO? = null
                for ((index, section) in menuSections.withIndex()) {
                    if (section.type == "menu") {
                        commonMenuListDTO = section
                        menuSections.removeAt(index)
                        break
                    }
                }

                menuSections.clear()
                menuSections.addAll(getMenuSections(commonMenuListDTO))

                vm.addonsMap = restaurant.menuAddons

                setItems(menuSections)

            }
        })
    }

    private fun setItems(menuSectionDTOS: MutableList<RestaurantMenuSectionDTO>) {
        val items = ArrayList<BindingRegularMenuItem>()
        for ((index, section) in menuSectionDTOS.withIndex()) {

            for ((itemIndex, item) in section.list?.withIndex() ?: emptyList()) {

                if (isFilterEnabled && !item.isVeg) continue
                if (selectedItems.isNotEmpty()) {
                    val pair =
                        selectedItems.find { selectedItem -> selectedItem.id == item.itemId }
                    if (pair != null) {
                        item.prevQuantity = pair.value
                    }
                }

                val simpleItem = BindingRegularMenuItem(this).withRestaurantAddItem(item)
                items.add(simpleItem)

                var positionList = vm.menuItemPosition[item.itemId]
                if (positionList.isNullOrEmpty()) {
                    positionList = arrayListOf(itemPosition.getAndIncrement())
                } else {
                    positionList.add(itemPosition.getAndIncrement())
                }
                vm.menuItemPosition[item.itemId] = positionList
            }
        }

        bd.itemCount = items.size
        totalMenuSize = items.size
        itemAdapter.add(items)
    }

    private fun subScribeSelectedItems() {

        vm.getSelectedItem()?.observeForever { it ->
            val deSelectedItems = arrayListOf<ValuePairSI>()

            if (selectedItems.isNotEmpty()) {

                selectedItems.forEach { previousItem ->
                    val item = it.find { item -> item.id == previousItem.id }
                    if (item == null) {
                        deSelectedItems.add(ValuePairSI(previousItem.id, 0))
                    }
                }

                if (deSelectedItems.isNotEmpty()) {
                    updateMenuItemSelection(deSelectedItems)
                    deSelectedItems.clear()
                }

            }


            selectedItems.clear()
            selectedItems.addAll(it)
            updateMenuItemSelection(selectedItems)
        }

    }

    private fun updateMenuItemSelection(it: List<ValuePairSI>) {

        it.forEach { selectedItem ->

            //get item position from the list as per id

            val positionList = arrayListOf<Int>()

            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val holder = mFastAdapter.getItem(i)
                when (holder) {
                    is BindingRegularMenuItem -> {
                        if (holder.restaurantItem?.itemId == selectedItem.id) {
                            positionList.add(mFastAdapter.getPosition(holder))
                            break@loop
                        }
                    }
                }
            }

            if (!positionList.isNullOrEmpty()) {

                //get item from fast adapter as per each position
                positionList.forEach { pos ->

                    val item = mFastAdapter.getItem(pos)

                    //find item object type and update it's value
                    when (item) {
                        is BindingRegularMenuItem -> {
                            item.restaurantItem?.prevQuantity = selectedItem.value
                            //                                    Handler(Looper.getMainLooper()).postDelayed({mFastAdapter.notifyAdapterItemChanged(pos)}, 300)
                            mFastAdapter.notifyAdapterItemChanged(pos)
                        }
                    }
                }
            }

        }
    }


    private fun onSearchItem(searchText: String) {
        itemAdapter.filter(searchText)
    }

    private fun getMenuSections(menuSectionDTO: RestaurantMenuSectionDTO?): Collection<RestaurantMenuSectionDTO> {
        val sections = mutableListOf<RestaurantMenuSectionDTO>()
        val list = menuSectionDTO?.list
        var i = 0
        if (!list.isNullOrEmpty())
            while (i < list.size) {
                if (list[i].type == "category") {
                    val section = RestaurantMenuSectionDTO()
                    section.type = "menu"
                    section.name = list[i].name.capitalize()
                    section.layoutType = 12
                    section.list = ArrayList()
                    val sectionList = ArrayList(section.list ?: listOf())
                    for (j in i + 1 until list.size) {
                        if (list[j].type == "item") {
                            sectionList.add(list[j])
                        } else if (list[j].type == "subCategory") {
                            continue
                        } else if (list[j].type == "category") {
                            i = j - 2
                            break
                        }
                    }
                    if (sectionList != null) {
                        section.list = sectionList
                        sections.add(section)
                    }
                }
                i++
            }

        return sections
    }

    private fun getItemListFromSection(menuSectionDTOS: MutableList<RestaurantMenuSectionDTO>): Collection<RestaurantItem> {
        val list = ArrayList<RestaurantItem>()
        menuSectionDTOS.forEach { section ->
            section.list?.forEach { item -> list.add(item) }
        }
        return list
    }

    override fun onQueryTextSubmit(query: String): Boolean {
//        getItemsFromDb(query)
        itemAdapter.filter(query)
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
//        getItemsFromDb(newText)
        itemAdapter.filter(newText)
        return true
    }

    override fun onQueryTextCleared(): Boolean {
//        getItemsFromDb("")
        itemAdapter.filter("")
        return true

    }

    override fun onItemSelected(position: Int, item: RestaurantItem) {}

    override fun onItemCountChange(
        cartHandler: ICartHandler,
        count: Int,
        item: RestaurantItem,
        quantityView: AddQuantityView,
        isIncremented: Boolean?
    ) {
        cartHelper.addOrUpdateItemToCart(
            cartHandler,
            count,
            item,
            vm.addonsMap,
            isIncremented
        )
    }

    override fun onItemFailed(message: String) {}

    override fun adapterSizeChange(itemSize: Int) {

        bd.tvItemCount.text = "Dishes ($itemSize)"
    }

    override fun onNoteClicked(item: RestaurantItem) {}

    override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {
        showToast("$count")
    }

    override fun itemsFiltered(
        constraint: CharSequence?,
        results: List<BindingRegularMenuItem>?
    ) {
        bd.itemCount = results?.size
//        CleverTapHelper.menuSearch(results)
    }

    private fun gotoPlateFragment() {

    /*  TODO handle this
    val returnIntent = Intent()
        returnIntent.putExtra(
            IConstants.ActivitiesFlags.SHOW_PLATE,
            true
        )
        setResult(Activity.RESULT_OK, returnIntent)
        finish()*/
    }

    override fun gotoPlate() {
        gotoPlateFragment()
    }

    override fun onReset() {
        //gets called when search feild is cleared
        bd.itemCount = totalMenuSize
    }

}