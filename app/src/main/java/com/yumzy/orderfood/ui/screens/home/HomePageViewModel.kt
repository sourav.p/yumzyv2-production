package com.yumzy.orderfood.ui.screens.home

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.PreCheckoutReqDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.home.data.HomePageRepository
import com.yumzy.orderfood.util.IConstants
import javax.inject.Inject

class HomePageViewModel  constructor(val repository: HomePageRepository) :
    BaseViewModel<IHomePageView>() {
    init {
//        globalAddressHolder = SingleLiveEvent()
//        selectedAddress= addressHolder.value
    }

    fun getAppUser() = repository.getUserDTO()
    fun updateCurrentLocation(latitude: String, longitude: String, city: String, locality: String) =
        repository.updateLocation(latitude, longitude, city, locality)

    fun postCheckout(orderId: String) = repository.postCheckout(orderId)
    fun preCheckout(preCheckout: PreCheckoutReqDTO) = repository.preCheckout(preCheckout)

    fun getAppStart() = repository.getAppStart()

    val locationDialogState: MutableLiveData<IConstants.LocationStatus?> = MutableLiveData()

    sealed class UserLocationState {
        object Serving {
            val name = "serving"
        }

        object NotServing {
            val name = "notserving"
        }

        object LocationDifferent {
            val name = "locationdifferent"
        }

        object LocationNotFound {
            val name = "locationnotfound"
        }
    }

    val vSelectedAddress = repository.getSelectedAddress()

    fun setSelectedAddress(addressDTO: AddressDTO) = repository.setSelectedAddress(addressDTO)

    fun removeSelectedAddress() = repository.removeSelectedAddress()

    /*un saveSelectedAddress(
        knownName: String,
        address: String,
        latitude: Double,
        longitude: Double,
        state: String,
        city: String,
        country: String
    ) = repository.saveSelectedAddress(
        knownName,
        address,
        latitude,
        longitude,
        state,
        city,
        country
    )*/
    fun getAddressList() = repository.getAddressList()
    fun getSelectedItems() = repository.getSelectedItemList()
    fun updateFcmToken(token: String, userId: String) = repository.updateUserFcmToken(token, userId)
    fun applyReferral(userId: String, referralCode: String) = repository.applyReferral(userId, referralCode)
}

