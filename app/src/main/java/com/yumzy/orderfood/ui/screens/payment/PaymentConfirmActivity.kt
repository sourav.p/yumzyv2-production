package com.yumzy.orderfood.ui.screens.payment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.laalsa.laalsalib.utilcode.util.VibrateUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import com.yumzy.orderfood.data.models.PostCheckoutResDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.databinding.ActivityPaymentConfirmBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.getQueryParam
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class PaymentConfirmActivity : BaseActivity<ActivityPaymentConfirmBinding, PaymentViewModel>(),
    IWebPayment {

    override val vm: PaymentViewModel by viewModel()

    val cartHelper: CartHelper by inject()

    var isFirst = true

    var orderId = ""

    var earnedScratchCard = false

    private lateinit var mainHandler: Handler

    private val paymentInitiatedTask = Runnable {
        if (!isFirst) postCheckout(orderId)
    }

    private val paymentSuccessTask = Runnable {
        closeActivity(
            IConstants.PaymentConst.PAYMENT_SUCCESS,
            "",
            orderId,
            earnedScratchCard
        )
    }


    override fun networkAvailable() {
    }

    override fun networkUnavailable() {
//        bd.lottiView.setAnimation(R.raw.payment_failed)
//        bd.lottiView.playAnimation()
//        bd.statusText = IConstants.ResponseError.NOT_CONNECTED
//finish()
        closeActivity(
            IConstants.PaymentConst.PAYMENT_FAILED,
            resources.getString(R.string.oops),
            IConstants.ResponseError.NOT_CONNECTED
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_payment_confirm)
        vm.attachView(this)


        mainHandler = Handler(Looper.getMainLooper())
        getIntentData(intent)
        listenOfPaymentStateChange()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        getIntentData(intent)
    }

    override fun showZohoChatButton(isVisible: Boolean) {
        super.showZohoChatButton(false)
    }

    override fun getIntentData(intent: Intent?) {
        if (intent?.hasExtra(PaymentActivity.INTENT_ORDER) == true) {
            //start PaymentWebActivity with this data and wait for result.
            val cartString = intent.getStringExtra(PaymentActivity.INTENT_ORDER)
            val newOrderObj = Gson().fromJson(cartString, NewOrderReqDTO::class.java)
            newOrder(newOrderObj)
        }

    }

    override fun newOrder(mOrder: NewOrderReqDTO) {
        vm.newOrder(mOrder).observe(this, { result ->
            when {
                result.type == APIConstant.Status.SUCCESS && result.data != null -> {
                    val jsonData =
                        JSONObject((com.yumzy.orderfood.appGson.toJson((result as ResponseDTO).data)))

                    if (jsonData.get("paymentLink").toString() != "") {
                        startPaymentForResult(jsonData)
                    } else {
                        postCheckout(jsonData.get("orderId").toString())
                    }

                }
                result.type == APIConstant.Status.ERROR -> {
                    if (result.data != null) {
                        val errorMsg = ((result.data as Map<*, *>).toMap())["error"]
                        closeActivity(
                            IConstants.PaymentConst.PAYMENT_FAILED,
                            resources.getString(R.string.oops),
                            errorMsg.toString()
                        )
                    }
                }
                result.type == APIConstant.Status.LOADING -> {
                    return@observe
                }
            }
        })
    }

    override fun postCheckout(orderId: String) {
        if(orderId == "" ) return
        vm.postCheckout(orderId).observe(this, { response ->
            isFirst = false
            RHandler<PostCheckoutResDTO>(this, response) {

                cartHelper.getCart().observeForever { }

                if (it.status == PaymentState.PAYMENT_COMPLETE.value) {
                    earnedScratchCard = it.earnedScratch
                    this.orderId = orderId
                }

                vm.paymentState.value = when (it.status) {
                    PaymentState.PAYMENT_FAILED.value -> PaymentState.PAYMENT_FAILED
                    PaymentState.PAYMENT_COMPLETE.value -> PaymentState.PAYMENT_COMPLETE
                    else -> PaymentState.PAYMENT_INITIATED
                }
            }
        })
    }

    override fun onSuccess(actionId: Int, data: Any?) {}

    override fun showCodeError(code: Int?, title: String?, message: String) {
        super.showCodeError(code, title, message)
        closeActivity(
            IConstants.PaymentConst.PAYMENT_FAILED,
            title ?: resources.getString(R.string.oops),
            message
        )
    }

    override fun showError(message: String) {
        super.showError(message)
        closeActivity(
            IConstants.PaymentConst.PAYMENT_FAILED,
            resources.getString(R.string.oops),
            message
        )
    }

    override fun onBackPressed() {
//        CleverTapHelper.pushEvent(CleverEvents.payment_canceled)
//        closeActivity(
//            IConstants.PaymentConst.PAYMENT_FAILED,
//            resources.getString(R.string.oops),
//            "Payment Cancelled"
//        )
        ActionModuleHandler.showChoseActionDialog(
            this,
            IConstants.ActionModule.EXIT_PAYMENT,
            "",
            "Are you sure you want to cancel the payment?",
            arrayListOf(
                ActionItemDTO(
                    key = ActionConstant.CLOSE,
                    textColor = ThemeConstant.blueJeans,
                    buttonColor = ThemeConstant.pinkies,
                    text = getString(R.string.no),
                ),
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.yes),
                )
            ),
            object : ActionDialog.OnActionPerformed {
                override fun onSelectedAction(
                    dialog: Dialog?,
                    actionType: Int,
                    action: ActionItemDTO?
                ) {
                    if (action != null && action.key == ActionConstant.POSITIVE) {
                        CleverTapHelper.pushEvent(CleverEvents.payment_canceled)
                        closeActivity(
                            IConstants.PaymentConst.PAYMENT_FAILED,
                            resources.getString(R.string.oops),
                            "Payment Cancelled"
                        )
                    }
                }
            }
        )
    }

    @SuppressLint("MissingPermission")
    private fun listenOfPaymentStateChange() {
        vm.paymentState.observe(this, Observer {
            if (it == null) return@Observer
            when (it) {
                PaymentState.PAYMENT_INITIATED -> {
                    //update ui
                    bd.lottiView.setAnimation(R.raw.payment_loader)
                    bd.lottiView.playAnimation()
                    bd.statusText = "Please Wait..."
                    mainHandler.postDelayed(paymentInitiatedTask, 4000)
                }
                PaymentState.PAYMENT_COMPLETE -> {
                    if (earnedScratchCard) {
                        bd.layoutEarnScretchCard.visibility = View.VISIBLE
                        bd.lottiAnimationView.setAnimation(R.raw.success_animation)
                        VibrateUtils.vibrate(
                            longArrayOf(
                                75,
                                38,
                                75,
                                488,
                                75,
                                38,
                                75,
                                200,
                                75,
                                38,
                                75,
                                400
                            ), -1
                        )
                        mainHandler.postDelayed(paymentSuccessTask, 4000)
                    } else {
                        bd.lottiView.setAnimation(R.raw.payment_success)
                        bd.lottiView.playAnimation()
                        bd.statusText = "Payment Successfull"
                        mainHandler.postDelayed(paymentSuccessTask, 4000)
                    }

                }
                PaymentState.PAYMENT_FAILED -> {
                    closeActivity(
                        IConstants.PaymentConst.PAYMENT_FAILED,
                        resources.getString(R.string.oops),
                        "Payment Failed"
                    )
//                    bd.lottiView.setAnimation(R.raw.payment_failed)
//                    bd.lottiView.pauseAnimation()
//                    bd.statusText = "Payment Failed"
//                    Handler(Looper.getMainLooper()).postDelayed({
//                        closeActivity(
//                            IConstants.PaymentConst.PAYMENT_FAILED,
//                            resources.getString(R.string.oops),
//                            "Payment Failed"
//                        )
//                    }, 4000)

                }

            }
        })
    }

    private fun startPaymentForResult(jsonData: JSONObject) {
        val paymentLink = try {
            jsonData.getString("paymentLink")
        } catch (e: Exception) {
            closeActivity(
                IConstants.PaymentConst.PAYMENT_CANCELED,
                "Server broken",
                "Unable to initiate payment "
            )
            return
        }

        val intent = Intent(this, PaymentWebActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_LINK, paymentLink)
        mainHandler.removeCallbacks(paymentInitiatedTask)
        AppIntentUtil.launchForResultWithSheetAnimation(this, intent) { resultOk, intentResult ->
            if (resultOk) {
                val resultIntent = intentResult.data
                if (resultIntent?.hasExtra(IConstants.ActivitiesFlags.PAYMENT_REDIRECT) == true) {
                    val redirectUrl =
                        resultIntent.getStringExtra(IConstants.ActivitiesFlags.PAYMENT_REDIRECT)
                    val orderId = redirectUrl?.let { getQueryParam("orderId", it) } ?: ""
                    if (orderId != "") {
                        this.orderId = orderId
                        postCheckout(orderId)
                    }
                }
            } else {
                closeActivity(IConstants.PaymentConst.PAYMENT_CANCELED, "", "Payment Canceled")
            }
        }
    }

    private fun closeActivity(
        resultCode: Int,
        title: String,
        message: String,
        earnedScratch: Boolean = false
    ) {
        removeObservers()
        val intent = Intent()
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_RESPONSE, resultCode)
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_BODY, message)
        intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_TITLE, title)
        intent.putExtra(IConstants.ActivitiesFlags.EARNED_SCRATCH, earnedScratch)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun removeObservers() {
        mainHandler.removeCallbacks(paymentInitiatedTask)
        mainHandler.removeCallbacks(paymentSuccessTask)
    }

    enum class PaymentState(val value: String) {
        PAYMENT_INITIATED("initiated"),
        PAYMENT_COMPLETE("complete"),
        PAYMENT_FAILED("failed"),
    }
}