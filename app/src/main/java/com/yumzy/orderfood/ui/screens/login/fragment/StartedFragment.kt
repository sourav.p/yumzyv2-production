package com.yumzy.orderfood.ui.screens.login.fragment

import android.app.Dialog
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.LoginDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentStartedBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.component.chromtab.ChromeTabHelper
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel

class StartedFragment : BaseFragment<FragmentStartedBinding, LoginViewModel>(), ILoginView {
    
    override val vm: LoginViewModel by viewModel()
    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_started
    override fun onFragmentReady(view: View) {
        vm.attachView(this)
        sharedPrefsHelper.saveFreshUser(false)
        sharedPrefsHelper.saveIsTemporary(true)

        bd.btnExplore.setOnClickListener {

            saveDummyUser()

        }

        bd.btnLogin.setOnClickListener {
            navController.navigate(R.id.fragment_started_screen_to_login)
            sharedPrefsHelper.saveIsTemporary(false)
        }
        bd.tvTermCondition.setOnClickListener {
            ChromeTabHelper.launchChromeTab(
                requireActivity() as AppCompatActivity,
                "https://laalsadev.sgp1.digitaloceanspaces.com/common_documents/Privacyandpolicy.html"
            )
        }
//        bd.tvLetsExplore.setOnClickListener {
//            ChromeTabHelper.launchChromeTab(
//                requireActivity() as AppCompatActivity,
//                "https://yumzy.in/"
//            )
//
//        }

    }

    private fun saveDummyUser() {
        val deviceId = YumUtil.getDeviceUniqueId(requireContext())
        vm.exploreUser(deviceId)
            .observe(this, { result ->
                RHandler<LoginDTO>(
                    this,
                    result
                ) {
                    result.data?.token?.let { token ->
                        sessionManager.saveTempAuthToken(token)
                    }
                    result.data?.user?.userId?.let { value ->
                        sharedPrefsHelper.saveUserId(
                            value
                        )
                    }
                    result.data?.user?.isTemporary?.let { value ->
                        sharedPrefsHelper.saveIsTemporary(
                            value
                        )
                    }
                    result.data?.user?.cartId?.let { value ->
                        sharedPrefsHelper.saveCartId(
                            value
                        )
                    }
                    result.data?.user?.name?.let { value ->
                        sharedPrefsHelper.saveUserName(
                            value
                        )
                    }
                    result?.data?.user?.lastName?.let { value ->
                        sharedPrefsHelper.saveUserLastName(value)
                    }
                    result.data?.user?.email?.let { value ->
                        sharedPrefsHelper.saveUserEmail(
                            value
                        )
                    }
                    result.data?.user?.mobile?.let { value ->
                        sharedPrefsHelper.saveUserPhone(
                            value
                        )
                    }
                    result.data?.user?.picture?.let {
                        sharedPrefsHelper.saveUserPic(it)
                    }
                    result.data?.user?.referralDeepLink?.let {
                        sharedPrefsHelper.saveUserReferralLink(it)
                    }
                    result.data?.user?.refferalCode?.let {
                        sharedPrefsHelper.saveUserReferralCode(it)
                    }

                    sharedPrefsHelper.saveIsNewUser(false)
                    saveExploreUser(result.data)
                }
            })
    }

    private fun saveExploreUser(data: LoginDTO?) {
        showProgressBar()
        if (data != null) {
            vm.updateFcmToken(
                YumzyFirebaseMessaging.getToken(requireActivity()).toString(),
                data.user.userId
            )
                .observe(this, {
                    if (it.type != -1) {
                        vm.saveUserInfo(data.user).observe(this, {
                            if (it.type != -1) {
                                CleverTapHelper.pushEvent(CleverEvents.user_explore_first)
                                //TODO: Sourav
                                // CleverTapHelper.setCleaverUser(data)
                                hideProgressBar()
                                val action =
                                    StartedFragmentDirections.fragmentStartedScreenToSetLocation(
                                        IConstants.FragmentFlag.FROM_EXPLORE
                                    )
                                navController.navigate(action)
//                                val action =
//                                    OTPVerifyFragmentDirections.fragmentOtpToSetLocation(IConstants.FragmentFlag.FROM_OTP_VERIFY)
//                                navController.navigate(action)
                            }
                        })
                    }
                })
        } else {
            showToast("Failed To Login , Try Later!")
        }
    }

    override fun navHomeActivity() {
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }
}