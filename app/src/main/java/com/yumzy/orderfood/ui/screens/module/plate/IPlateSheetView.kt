package com.yumzy.orderfood.ui.screens.module.plate

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IPlateSheetView : IModuleHandler, ConfirmActionDialog.OnActionPerformed {
    fun fetchUserCart(address: AddressDTO?)


    // fun checkUserHasAddress()
}
