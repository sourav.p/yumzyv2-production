package com.yumzy.orderfood.ui.screens.module.plate.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.models.*
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class PlateSheetRemoteDataSource  constructor(private val serviceAPI: CartServiceAPI) :
    BaseDataSource() {
    suspend fun preCheckout(preCheckout: PreCheckoutReqDTO) = getResult {
        serviceAPI.preCheckout(preCheckout)
    }

    suspend fun applyCoupon(applyCoupon: MutableMap<String, Any>) = getResult {
        serviceAPI.applyCoupon(applyCoupon)
    }

    suspend fun getCart(cartId: String) = getResult {
        serviceAPI.getCart(cartId)
    }

    suspend fun addToCart(addCart: NewAddItemDTO) = getResult {
        serviceAPI.addToCart(addCart)
    }

    suspend fun newOrder(newOrder: NewOrderReqDTO) = getResult {
        serviceAPI.newOrder(newOrder)
    }

    suspend fun postCheckout(orderId: String) = getResult {
        serviceAPI.postCheckout(orderId)
    }

    suspend fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = getResult {
        serviceAPI.itemCount(position, count, itemCount)
    }

    suspend fun updateItem(
        position: Int,
        addCart: NewAddItemDTO
    ) = getResult {
        serviceAPI.updateItem(position, addCart)
    }

    suspend fun clearCart(cart: ClearCartReqDTO) = getResult {
        val cartMap = mapOf(
            "cartId" to cart.cartId
        )
        serviceAPI.clearCart(cartMap)
    }

    suspend fun removeCoupon(removeCouponReqDTO: MutableMap<String, Any>) = getResult {
        serviceAPI.removeCoupon(removeCouponReqDTO)
    }

    suspend fun getAddressList() = getResult {
        serviceAPI.getAddressList()
    }

    suspend fun getUserWallet(mobile: Long) = getResult {
        serviceAPI.getUserWallet(mobile)
    }

    suspend fun useWallet(cartId: HashMap<String, String>) = getResult {
        serviceAPI.useWallet(cartId)
    }

    suspend fun unUseWallet(cartId: HashMap<String, String>) = getResult {
        serviceAPI.unUseWallet(cartId)
    }

}