package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.YumUtil.getDurationAndCostForTwo

class YumzyExclusiveAdapter2(
    val context: Context,
    private val homeLayoutList: ArrayList<HomeListDTO>?,
    val callerFun: (View, HomeListDTO) -> Unit
) :
    RecyclerView.Adapter<YumzyExclusiveAdapter2.YumzyExclusiveHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): YumzyExclusiveHolder {
        val itemFeatured =
            LayoutInflater.from(context).inflate(R.layout.adapter_exclusive, parent, false)
        return YumzyExclusiveHolder(itemFeatured)
    }

    override fun getItemCount() = homeLayoutList?.size ?: 0


    inner class YumzyExclusiveHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val roundImageView: RoundedImageView = itemView.findViewById(R.id.exclusive_image)
        val tvTitle: TextView = itemView.findViewById(R.id.exclusive_title)
        val tvSubTitle: TextView = itemView.findViewById(R.id.exclusive_subtitle)
        val tvOffer: TextView = itemView.findViewById(R.id.exclusive_offer)
        val range: TextView = itemView.findViewById(R.id.exclusive_range)
        val locality: TextView = itemView.findViewById(R.id.exclusive_locality)
        val cardView: CardView = itemView.findViewById(R.id.shadowRectLayout)


        fun bindView(homeItem: HomeListDTO?) {
            cardView.radius = UIHelper.cornerRadius
            roundImageView.cornerRadius = UIHelper.cornerRadius

            YumUtil.textUniformTextView(tvOffer)
//            tvOffer.setAutoSizeTextTypeUniformWithConfiguration(
//                1, 17, 1, TypedValue.COMPLEX_UNIT_DIP);
            homeItem?.run {
//                Glide.with(roundImageView).load(this.image.url).into(roundImageView)
                YUtils.setImageInView(roundImageView, this.image?.url ?: "")
                tvTitle.text = this.title
                if (this.subTitle.isNullOrEmpty()) {
                    locality.text = ""
                    locality.visibility = View.GONE

                } else {
                    locality.text = this.subTitle
                    locality.visibility = View.VISIBLE

                }

                if (homeItem.cuisines.isNullOrEmpty()) {
                    tvSubTitle.text = ""
                    tvSubTitle.visibility = View.GONE

                } else {
                    tvSubTitle.text =
                        homeItem.cuisines?.joinToString(separator = ", ", postfix = "") ?: ""
                    tvSubTitle.visibility = View.VISIBLE

                }


                range.text = getDurationAndCostForTwo(homeItem)
                tvOffer.text = YumUtil.setZoneOfferString(homeItem.meta?.offers)

                ClickUtils.applyGlobalDebouncing(
                    itemView,
                    IConstants.AppConstant.CLICK_DELAY
                ) {

                    callerFun(itemView, homeItem)
                }

            }

        }
    }

    override fun onBindViewHolder(holder: YumzyExclusiveHolder, position: Int) {
        val homeItem = homeLayoutList?.get(position)
        holder.bindView(homeItem)
    }

}

