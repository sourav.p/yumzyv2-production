package com.yumzy.orderfood.ui.screens.reorder

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import com.yumzy.orderfood.databinding.ActivityReorderBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpHeadingActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.dateformater.UTCtoNormalDate
import kotlinx.android.synthetic.main.activity_reorder.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class ReorderActivity : BaseActivity<ActivityReorderBinding, ReorderViewModel>(), IReorder {
    var orderId: String = ""

    
    override val vm: ReorderViewModel  by viewModel()
    
    var outletId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_reorder)

        vm.attachView(this)

        setSupportActionBar(bd.toolbarReorder)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        orderId = intent.getStringExtra(IConstants.ActivitiesFlags.FLAG) ?: ""
        applyDebouchingClickListener(bd.orderPickView, bd.btnReorder)
        getOrderDetails()


    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    private fun getOrderDetails() {
        vm.orderDetails(orderId).observe(this, { result ->
            RHandler<NewOrderDetailsDTO>(
                this,
                result
            ) {
                orderDetailsResponse(it)
            }

        })
    }

//    fun onResponse(code: Int?, response: OrderDetailsDTO?) {
//        super.onResponse(code, response)
//        getOrderDetails()
//    }



    private fun orderDetailsResponse(data: NewOrderDetailsDTO) {
        //ToastUtils.showLong(data.deliveryAddress.address.city)
        supportActionBar?.title = resources.getString(R.string.orders) + " #" + data.orderNum
        bd.orderDetails = data
        bd.orderPickView.title = data.outlets.get(0).outletName
        bd.orderPickView.subTitle = data.outlets.get(0).outletLocation

        bd.tvCreationDate.text =
            "Date : " + UTCtoNormalDate.getDateTime(data.creationDate.toString())
        bd.orderDeliveryTo.title = data.deliveryAddress?.address?.addressTag?.capitalize()
        bd.orderDeliveryTo.subTitle = data.deliveryAddress?.address?.fullAddress
        //  bd.billingDetailsView.setOutletInfoHeader()
        bd.billingDetailsView.setOutletInfoList(data.outlets.get(0).items)

        bd.offerInfoView.title = data.statusDesc

        bd.billingDetailsView.setBillingInfoList(data.billing!!) { view ->
/*
*   var data: String? = paidVia
            .filter { paymentDTO -> paymentDTO.paid ?: 0.0 > 0.0 && paymentDTO.method !== "unknown" }
            .map { paymentDTO -> paymentDTO.method.toString().toUpperCase() }
            .joinToString(" and ")

        return if(data?.isEmpty()==true) " UNKNOWN " else data?.trim().toString()
* */
            var text: String =
                data.payments
                    .filter { paymentDTO -> paymentDTO.paid ?: 0.0 > 0.0 && paymentDTO.method !== "unknown" }
                    .map { paymentDTO ->
                        "• ${paymentDTO.method?.toUpperCase()} : ₹${
                            paymentDTO.paid?.withPrecision(
                                2
                            )+"\n"
                        }"
                    }
                    .joinToString("\n")

            if (text.trim().isEmpty()) {
                text = "• UNKNOWN : ₹0.00\n"
            }
            /*data.payments.forEach { paymentDTO ->
                if (paymentDTO.paid ?: 0.0 > 0.0 && paymentDTO.method !== "unknown" )
                {  text =text + "\n• " + paymentDTO.method?.toUpperCase() + " : " + "₹" + paymentDTO.paid?.withPrecision(2
                            ).toString() + "\n"
                }
                else
                {
                    if(text.isEmpty()==true) "\n• UNKNOWN : ₹0.00\n" else text
                }
            }*/
            showListMenu(text, view)

//
//
//            val bottomSheet = SheetActionDialog()
//            val template = PaidViaTemplate(
//                context = this,
//                vpa = text,
//            )
//            bottomSheet.detailsTemplate = template
//            bottomSheet.showNow(supportFragmentManager, "")


            /*  var mFilterPopup: PopupWindow? = null
              val layout = LinearLayout(this).apply {
                  this.background =
                      VUtil.getRoundDrawable(ThemeConstant.white, VUtil.dpToPx(10).toFloat())
                  this.orientation = LinearLayout.VERTICAL

                  data.payments.forEach { paymentDTO ->
                      val horzontalItem = LinearLayout(this@ReorderActivity)

                      if (paymentDTO.paid != 0.0) {

                          horzontalItem.addView(
                              TextView(this@ReorderActivity).apply {
                                  if (!paymentDTO.method.isNullOrEmpty()) {
                                      text = paymentDTO.method?.capitalize() + " : "
                                  } else {
                                      text = "Online : "
                                  }
                              }
                          )
                          horzontalItem.addView(
                              TextView(this@ReorderActivity).apply {
                                  text = "₹" + paymentDTO.paid?.withPrecision(2).toString()
                              }
                          )
                          horzontalItem.setPadding(
                              UIHelper.i10px,
                              UIHelper.i10px,
                              UIHelper.i10px,
                              UIHelper.i10px
                          )
                          this.addView(horzontalItem)
                      }
                      else
                      {
                      }
                  }
                  setPadding(UIHelper.i10px, UIHelper.i10px, UIHelper.i10px, UIHelper.i10px)
              }

              if(layout.childCount < 1)
                  return@setBillingInfoList



              mFilterPopup =
                  PopupWindow(
                      layout,
                      ViewConst.WRAP_CONTENT,
                      ViewConst.WRAP_CONTENT
                  )

              mFilterPopup.isOutsideTouchable = true
              mFilterPopup.isFocusable = true
              mFilterPopup.setBackgroundDrawable(null)
              mFilterPopup.showAtLocation(
                  view,
                  Gravity.START,
                  view.pivotX.toInt() - 200,
                  view.pivotY.toInt() - 200
              )

              val container = mFilterPopup.contentView?.parent as View
              val manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
              val windowParams = container.layoutParams as WindowManager.LayoutParams

              windowParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
              windowParams.dimAmount = 0.03f
              windowManager.updateViewLayout(container, windowParams)*/

        }

        //   bd.billingDetailsView.setPaymentType(YumUtil.setPaidVia(data.payments))

        if (!data.specialInstructions.isNullOrBlank()) {
            bd.specialInstructionLayout.visibility = View.VISIBLE
            bd.specialInstructionLayout.tv_instruction.text =
                data.specialInstructions.toString().capitalize()
        } else
            bd.specialInstructionLayout.visibility = View.GONE


        outletId = data.outlets[0].outletId

        when (data.orderStatus) {
            IConstants.OrderTracking.ORDER_STATUS_ACCEPTANCE_WAIT,
            IConstants.OrderTracking.ORDER_STATUS_VALET_ON_WAY_TO_CONFIRM,
            IConstants.OrderTracking.ORDER_STATUS_FOOD_PREPARATION,
            IConstants.OrderTracking.ORDER_STATUS_WAITIN_DELIVERY_PICKUP,
            IConstants.OrderTracking.ORDER_STATUS_FOOD_READY -> {

                bd.btnReorder.visibility = View.GONE

                bd.rateViewComponent.setRatingColor(
                    resources.getString(R.string.icon_clock_o),
                    " " + data.statusDesc.toString(), ThemeConstant.blueJeans
                )

                updateOrderinfo(data.statusText.toString(), ThemeConstant.blueJeans)

            }
            IConstants.OrderTracking.ORDER_STATUS_OUT_FOR_DELIVERY -> {

                bd.btnReorder.visibility = View.GONE

                val statusText =
                    " ${data.statusDesc}, ${data.deliveryEmployee} partner with ${data.deliveryPartner?.capitalize()}"

                bd.rateViewComponent.setRatingColor(
                    resources.getString(R.string.icon_clock_o),
                    " $statusText", ThemeConstant.blueJeans
                )

                updateOrderinfo(data.statusText.toString(), ThemeConstant.blueJeans)

            }
            IConstants.OrderTracking.ORDER_STATUS_DELIVERED,
            IConstants.OrderTracking.ORDER_STATUS_COMPLETE -> {

                bd.btnReorder.visibility = View.VISIBLE

                bd.rateViewComponent.setRatingColor(
                    resources.getString(R.string.icon_clock_o),
                    " " + data.statusDesc.toString(), ThemeConstant.greenConfirmColor
                )

                updateOrderinfo(data.statusText.toString(), ThemeConstant.greenConfirmColor)

            }
            IConstants.OrderTracking.ORDER_STATUS_CANCELLED,
            IConstants.OrderTracking.ORDER_STATUS_REJECTED,
            IConstants.OrderTracking.ORDER_STATUS_IN_DISPUTE,
            IConstants.OrderTracking.ORDER_STATUS_ENFORCED_CANCEL -> {

                bd.btnReorder.visibility = View.VISIBLE

                updateOrderinfo(data.statusText.toString(), ThemeConstant.pinkiesLight)
            }
        }
    }



    private fun updateOrderinfo(
        data: String,
        color: Int
    ) {
        bd.rateViewComponent.setRatingColor(
            resources.getString(R.string.icon_clock_o),
            " $data", color
        )

        bd.offerInfoView.background = UIHelper.roundDotedGreenStrokedDrawable(
            VUtil.dpToPx(1),
            color,
            ThemeConstant.white,
            VUtil.dpToPx(5).toFloat(),
            VUtil.dpToPx(5).toFloat()
        )
        bd.offerInfoView.setmColor(color)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_chat_option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.chat_zoho) {
            CleverTapHelper.pushEvent(CleverEvents.support_chat_requested)
            startActivity(Intent(this, HelpHeadingActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                OutletActivity.REStAURANT_REQ_CODE -> {
                    val returnIntent = Intent()
                    returnIntent.putExtra(
                        IConstants.ActivitiesFlags.SHOW_PLATE,
                        true
                    )
                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.order_pick_view -> {
                val intent = Intent(this, OutletActivity::class.java)
                intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
                startActivity(intent)
            }
            R.id.btn_reorder -> {
                val intent = Intent(this, OutletActivity::class.java)
                intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
                startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
//                finishAffinity()
            }

        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {


        if (code == APIConstant.Status.NO_NETWORK) {


            bd.btnReorder.visibility = View.GONE
            bd.llHeader.visibility = View.GONE
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet
        }
    }

    private fun showListMenu(text: String, anchorView: View) {
        val inflater: LayoutInflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.layout_paid_via_view, null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
            view, // Custom view to show in popup window
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }

        // If API level 23 or higher then execute the code
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            // Create a new slide animation for popup window enter transition
//            val slideIn = Slide()
//            slideIn.slideEdge = Gravity.TOP
//            popupWindow.enterTransition = slideIn
//
//            // Slide animation for popup window exit transition
//            val slideOut = Slide()
//            slideOut.slideEdge = Gravity.BOTTOM
//            popupWindow.exitTransition = slideOut
//
//        }
        val tv = view.findViewById<TextView>(R.id.tv_paid_via)
        tv.text = text
        val point = IntArray(2)
        view.measure(0,0)
        val height:Int=view.measuredHeight
        val width:Int=VUtil.screenWidth-view.measuredWidth-20.px
        anchorView.getLocationOnScreen(point)
        popupWindow.isOutsideTouchable = true
        //popupWindow.isFocusable = true
        popupWindow.setOnDismissListener {}
        popupWindow.showAtLocation(
            anchorView, // Location to display popup window
            Gravity.NO_GRAVITY, // Exact position of layout to display popup
           // width, // view right end point
            point[0], // view left start point
            point[1]-height-10.px // Y offset
        )
    }

}
