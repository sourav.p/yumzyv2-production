package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

public interface AnimatorUpdateListenerCompat {
    void onAnimationUpdate(ValueAnimatorCompat var1);
}
