package com.yumzy.orderfood.ui.screens.earncoupon.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class EarnCouponRemoteDataSource  constructor(private val serviceAPI: CartServiceAPI) :
    BaseDataSource() {
    suspend fun getFetchCouponList(userID: String, cartId: String?, outletId: String?) = getResult {
        serviceAPI.getFetchCouponList(userID, cartId,outletId)
    }
}