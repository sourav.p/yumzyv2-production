package com.yumzy.orderfood.ui.screens.module.savedAddress

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogSavedAddressBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.component.shadowcard.DetailsCardView
import com.yumzy.orderfood.ui.screens.manageaddress.IManageAddress
import com.yumzy.orderfood.ui.screens.manageaddress.ManageAddressAdapter
import com.yumzy.orderfood.ui.screens.manageaddress.ManageAddressViewModel
import com.yumzy.orderfood.ui.screens.module.address.LocationMapAction
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class SavedAddressDialog :
    BaseDialogFragment<DialogSavedAddressBinding>(), IManageAddress,
    DetailsCardView.DetailsCardListener {

    val vm: ManageAddressViewModel by viewModel()

    var actionId: Int = 0

    private lateinit var mAdapter: ManageAddressAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.attachView(this)

        setupAdapter()
        applyDebouchingClickListener(bd.ivClose, bd.layoutAddNewAddress)

        fetchAddressList()


    }

    override fun fetchAddressList() {
        vm.getAddressList().observe(
            this,
            { result ->
                RHandler<List<AddressDTO>>(this, result) {
                    mAdapter.updateList(it)
                }
            })
    }

    override fun editAddress(address: AddressDTO) {

    }

    override fun deleteRemoteAddress(addressId: String) {
    }


    override fun setupAdapter() {
        mAdapter =
            ManageAddressAdapter(
                emptyList(),
                this
                /* clickedListener = { address, position, action -> },
                 selAddressListener = { address, position -> passSelectedAddress(address) }*/
            )
        bd.rvSavedAddress.adapter = mAdapter
        bd.rvSavedAddress.layoutManager = LinearLayoutManager(context)
    }

    private fun passSelectedAddress(address: AddressDTO) {
        actionHandler?.onSuccess(actionId, address)
        this.dismiss()


    }


    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.iv_close -> dialog?.dismiss()

            R.id.layout_add_new_address -> {

                dialog?.dismiss()
                ActionModuleHandler.showConfirmLocation(
                    requireContext(),
                    LocationMapAction.MapActionAdd(),
                    this
                )

            }

        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_saved_address

    override fun onDismiss(dialog: DialogInterface) {
        vm.detachView()
        super.onDismiss(dialog)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        actionHandler?.onSuccess(actionId, data)
    }

    override fun onDialogReady(view: View) {
    }

    override fun onDetailsCard(tag: Any) {
        if (tag is AddressDTO)
            passSelectedAddress(tag)
    }

    override fun onAction(tag: Any) {
    }

    override fun onRightIcon(tag: Any) {
    }
}