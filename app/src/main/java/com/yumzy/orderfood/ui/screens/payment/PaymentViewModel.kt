package com.yumzy.orderfood.ui.screens.payment

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import com.yumzy.orderfood.data.models.OrderStatusDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.resultLiveData
import javax.inject.Inject

class PaymentViewModel  constructor(
    private val repository: PaymentRepository,
    private val localDataSource: OrdersDao
) : BaseViewModel<IWebPayment>() {

    val paymentState = MutableLiveData(PaymentConfirmActivity.PaymentState.PAYMENT_INITIATED)

    fun newOrder(newOrder: NewOrderReqDTO) = repository.newOrder(newOrder)

    fun postCheckout(orderId: String) = repository.postCheckout(orderId)

    fun getOrderDetail(orderId: String) = repository.getOrderDetails(orderId)

}