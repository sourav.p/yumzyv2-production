package com.yumzy.orderfood.ui.screens.module.foodpreference.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ProfileServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class FoodPreferenceRemoteDataSource  constructor(private val serviceAPI: ProfileServiceAPI) :
    BaseDataSource() {
    suspend fun getlikedPrefTags() = getResult {
        serviceAPI.getlikedPrefTags()
    }
}