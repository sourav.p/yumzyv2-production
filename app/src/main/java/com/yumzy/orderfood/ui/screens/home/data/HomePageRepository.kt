/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */
package com.yumzy.orderfood.ui.screens.home.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.PreCheckoutReqDTO
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.updateDatabase
import javax.inject.Inject

/**
 * Repository module for handling data operations.
 */
class HomePageRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val selectedItemDao: SelectedItemDao,
    private val selectedAddressDao: SelectedAddressDao,
    private val remoteDataSource: HomeRemoteDataSource
) {

    fun getUserDTO() = databaseLiveData {
        userInfoDao.getUser()
    }

    fun updateLocation(latitude: String, longitude: String, city: String, locality: String) =
        networkLiveData {
            remoteDataSource.updateLocation(latitude, longitude, city, locality)
        }

    fun postCheckout(orderId: String) = networkLiveData<Any>(
        networkCall = {
            remoteDataSource.postCheckout(orderId)
        }
    )

    fun getAppStart() = networkLiveData<Any>(
        networkCall = {
            remoteDataSource.getAppStart()
        }
    )

    fun getSelectedAddress() = databaseLiveData {
        selectedAddressDao.getPickedAddress()
    }

    fun setSelectedAddress(address: AddressDTO) = updateDatabase {
        selectedAddressDao.updateOrInsert(address)
    }

    fun removeSelectedAddress() = updateDatabase {
        selectedAddressDao.removePickedAddress()
    }

    fun getAddressList() = networkLiveData(
        networkCall = {
            remoteDataSource.getAddressList()
        }
    ).distinctUntilChanged()

    fun getSelectedItemList() = databaseLiveData {
        selectedItemDao.getSelectedItem()
    }

    fun preCheckout(preCheckout: PreCheckoutReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.preCheckout(preCheckout)
        }
    ).distinctUntilChanged()
/*

    fun saveSelectedAddress(
        knownName: String,
        address: String,
        latitude: Double,
        longitude: Double,
        state: String,
        city: String,
        country: String
    ) = backgroundLiveData {
        run {
            val selectedAddress = SelectedAddress(
                knownName = knownName,
                fullAddress = address,
                city = city,
                state = state,
                country = country,
                latitude = latitude,
                longitude = longitude
            )
            selectedAddressDao.updateOrInsert(selectedAddress)
            ResponseDTO.executed(data = true)
        }
    }

 */

    fun updateUserFcmToken(token: String, userId: String) = networkLiveData(
        networkCall = {
            remoteDataSource.updateUserFcmToken(
                mapOf(
                    "newToken" to token,
                    "userId" to userId
                )
            )
        }
    )

    fun applyReferral(userId: String, referralCode: String) = networkLiveData(
        networkCall = {
            remoteDataSource.applyReferral(
                userId,
                mapOf(
                    "referralCode" to referralCode
                )
            )
        }
    )

}
