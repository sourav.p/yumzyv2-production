package com.yumzy.orderfood.ui.screens.module.address

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

class SelectDeliveryLocationViewModel  constructor( private val repository: ProfileRepository
) : BaseViewModel<IView>() {

    fun getAddressList() = repository.getAddressList()

}