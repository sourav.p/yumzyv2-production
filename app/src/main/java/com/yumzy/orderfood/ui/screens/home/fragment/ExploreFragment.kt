package com.yumzy.orderfood.ui.screens.home.fragment

import android.content.Intent
import android.graphics.Point
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.VUtil
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.select.getSelectExtension
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.ExploreSearchDTO
import com.yumzy.orderfood.data.SearchItemDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentExploreBinding
import com.yumzy.orderfood.tools.RemoteConfigHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.ui.screens.home.ExploreItem
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.StickyHeaderAdapter
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject


class ExploreFragment  constructor() :
    BaseFragment<FragmentExploreBinding, ExploreViewModel>(), IExploreView,
    Toolbar.OnMenuItemClickListener, OutletSearchView.OnQueryTextListener,
    ExploreItemListener {

    private var searchResult: ArrayList<SearchItemDTO> = arrayListOf<SearchItemDTO>()

    var searchedString = ""

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter

    override val vm: ExploreViewModel by viewModel()
    
    override fun getLayoutId() = R.layout.fragment_explore
    
    override fun onFragmentReady(view: View) {
        setHasOptionsMenu(true)
        bd.viewModel = vm


        setupSearchView(bd.toolbar.menu)
        bd.icvEmptyPlate.button.setOnClickListener {
            val requireActivity = requireActivity()
            if (requireActivity is HomePageActivity) {
                requireActivity.navigateToFragment(R.id.nav_home, true)
            }
        }
        setupPagingRecyclerView()


    }

    private fun setupPagingRecyclerView() {

        val stickyHeaderAdapter = StickyHeaderAdapter<GenericItem>()
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        bd.rvExplore.layoutManager = LinearLayoutManager(context)
        bd.rvExplore.itemAnimator = DefaultItemAnimator()
        bd.rvExplore.adapter = stickyHeaderAdapter.wrap(mFastAdapter)

        val decoration = StickyRecyclerHeadersDecoration(stickyHeaderAdapter)
        bd.rvExplore.addItemDecoration(decoration)

        //so the headers are aware of changes
        mFastAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                decoration.invalidateHeaders()
            }
        })

        mFastAdapter.attachDefaultListeners = true

    }


    private fun setupSearchView(menu: Menu) {
        val item: MenuItem = menu.findItem(R.id.action_search)
        bd.searchView
            .setMenuItem(item)
        val revealCenter: Point = bd.searchView.revealAnimationCenter
        revealCenter.x -= VUtil.dpToPx(40)
        bd.searchView.showSearch(false)
        bd.searchView.hideCressIcon(false)
        bd.searchView.setOnQueryTextListener(this)
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        searchedString = query
        fetchSearchResult(searchedString)
        return true
    }


    override fun onQueryTextChange(query: String): Boolean {
        searchedString = query
        if (searchedString.isEmpty()) {
            onQueryTextCleared()
            return true
        } else {
            fetchSearchResult(searchedString)
            return true
        }
    }

    override fun onQueryTextCleared(): Boolean {
        bd.searchView.hideCressIcon(false)
        bd.icvEmptyPlate.setTitle(RemoteConfigHelper.explore_initial_title)
        bd.icvEmptyPlate.setSubtitle(RemoteConfigHelper.explore_initial_subtitle)
        bd.icvEmptyPlate.setImage(null)
        clearData()
        searchedString = ""
        showErrorView(true)
        showRecyclerView(false)
        fetchSearchResult(searchedString)

        return true
    }

    private fun fetchSearchResult(query: String) {

        if (query.length > RemoteConfigHelper.search_threshold) {
            showErrorView(false)
            bd.icvEmptyPlate.visibility = View.GONE
            vm.getSearchResult(query).observe(this, Observer { result ->
                if (result.type == APIConstant.Status.LOADING) {
                    showSearchBarProgress(true)
                    return@Observer
                }
                showSearchBarProgress(false)
                RHandler<ExploreSearchDTO>(this, result) {

                    if (it.items.isNullOrEmpty() || it.outlets.isNullOrEmpty()) {
                        vm.getSearchResult("")
                            .removeObservers(this)
                    }
                    if (searchedString.isEmpty()) {
                        clearData()
                        return@RHandler
                    }

                    val outlets = it.outlets
                    val items = it.items

                    when {
                        it.items?.size != 0 && it.outlets?.size != 0 -> {
                            clearData()
                            searchResult.addAll(items!!)
                            searchResult.addAll(outlets!!)
                            showRecyclerView(true)
                            showErrorView(false)
                            val expItems = ArrayList<ExploreItem>()
                            searchResult.forEach { order ->
                                val exploreView =
                                    ExploreItem().withOrder(order).withListener(this)
                                expItems.add(exploreView)
                            }
                            mFastAdapter.add(expItems)
                        }

                        it.outlets?.size != 0 -> {
                            clearData()
                            searchResult.addAll(outlets!!)
                            showRecyclerView(true)
                            showErrorView(false)
                            val expItems = ArrayList<ExploreItem>()
                            searchResult.forEach { order ->
                                val exploreView =
                                    ExploreItem().withOrder(order).withListener(this)
                                expItems.add(exploreView)
                            }
                            mFastAdapter.add(expItems)
                        }

                        it.items?.size != 0 -> {
                            clearData()
                            searchResult.addAll(items!!)
                            showRecyclerView(true)
                            showErrorView(false)
                            val expItems = ArrayList<ExploreItem>()
                            searchResult.forEach { order ->
                                val exploreView =
                                    ExploreItem().withOrder(order).withListener(this)
                                expItems.add(exploreView)
                            }
                            mFastAdapter.add(expItems)
                        }

                        it.items?.size == 0 && it.outlets?.size == 0 -> {
                            clearData()
                            showRecyclerView(false)
                            showErrorView(true)
                            bd.icvEmptyPlate.setTitle(result.title ?: "404 Food not found")
                            bd.icvEmptyPlate.setSubtitle(
                                result.message ?: RemoteConfigHelper.explore_result_empty
                            )
                        }
                    }
                }
            })

        } else {

            bd.icvEmptyPlate.setTitle(RemoteConfigHelper.explore_threshold_title)
            bd.icvEmptyPlate.setSubtitle(RemoteConfigHelper.explore_threshold_subtitle)
            clearData()
            showErrorView(true)
            showRecyclerView(false)
            bd.searchView.hideCressIcon(false)

        }
    }

    private fun showErrorView(show: Boolean) {
        if (show) {
            bd.icvEmptyPlate.visibility = View.VISIBLE
        } else {
            bd.icvEmptyPlate.visibility = View.GONE
        }
    }

    private fun clearData() {
        searchResult.clear()
        mFastAdapter.clear()
    }

    private fun showRecyclerView(show: Boolean) {
        if (show) {
            bd.rvExplore.visibility = View.VISIBLE
        } else {
            bd.rvExplore.visibility = View.GONE
        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {
            showRecyclerView(true)
            showErrorView(false)
            bd.icvEmptyPlate.setTitle(IConstants.OrderTracking.NO_INTERNET)
            bd.icvEmptyPlate.setSubtitle(IConstants.OrderTracking.NO_INTERNET_MSG)
            bd.icvEmptyPlate.setImage(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.img_no_internet,
                    context?.theme
                )
            )
        } else if (code == APIConstant.Code.NOT_SERVING || code == APIConstant.Code.NO_RESTAURANT_AT_LOCATION) {
            showSearchBarProgress(false)
            showErrorView(true)
            showRecyclerView(false)
            bd.icvEmptyPlate.setTitle(title ?: getString(R.string.all_not_serving))
            bd.icvEmptyPlate.setSubtitle(message)
            bd.icvEmptyPlate.setImage(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.img_unserviceable,
                    context?.theme
                )
            )
        } else if (code == APIConstant.Code.NO_DELIVERY_PARTNER) {
            showErrorView(true)
            showRecyclerView(false)
            bd.icvEmptyPlate.setTitle(title ?: IConstants.OrderTracking.DELIVERY_UNAVAILABLE)
            bd.icvEmptyPlate.setSubtitle(message)
            bd.icvEmptyPlate.setImage(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.img_deliviry_issue,
                    context?.theme
                )
            )

        } else
            super.showCodeError(code, title, message)
    }

    private fun showSearchBarProgress(show: Boolean) {
        bd.searchView.showProgress(show)
    }

    override fun onItemClick(outletId: String?, itemId: String?) {
        val intent = Intent(context, OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
        intent.putExtra(IConstants.ActivitiesFlags.ITEM_ID, itemId)
        val homeActivity = context as AppCompatActivity
        if (homeActivity is HomePageActivity) {
            AppIntentUtil.launchForResultWithSheetAnimation(
                homeActivity,
                intent
            ) { resultOk, result ->
                if (resultOk) {
                    homeActivity.navigateToFragment(R.id.nav_plate, false)
                }
            }
        }
    }

}

interface ExploreItemListener {
    fun onItemClick(outletId: String?, itemId: String?)

}


