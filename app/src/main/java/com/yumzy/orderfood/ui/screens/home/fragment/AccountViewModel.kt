package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.home.data.AccountRepository
import javax.inject.Inject

/**
 * The ViewModel for [AccountFragment].
 */
class AccountViewModel  constructor(private val homeRepository: AccountRepository) :
    BaseViewModel<IAccountView>() {
    fun getAppUser() = homeRepository.getUserDTO()
    fun userDetails() = homeRepository.profileUser()
    fun logoutUser() = homeRepository.logoutUser()
    fun clearUser() = homeRepository.removeUserDTO()
    fun getScratchCardInfo() = homeRepository.getScratchCardInfo()

//    val accountDataObserve: AccountDataObserve = AccountDataObserve()

}
/*

class AccountDataObserve : BaseObservable() {
    // val data = ...
    @Bindable
    fun getRememberMe(): String {
        return "data.rememberMe"
    }

    fun setRememberMe(value: String) {
        // Avoids infinite loops.
//        if (data.rememberMe != value) {

//            data.rememberMe = value

        // React to the change.
//            saveData()

        // Notify observers of a new value.
        notifyPropertyChanged(BR.rememberMe)
        notifyChange()
//        }
    }
}*/
