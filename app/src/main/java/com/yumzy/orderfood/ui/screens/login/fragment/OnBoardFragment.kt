package com.yumzy.orderfood.ui.screens.login.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.button.MaterialButton
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentStepperOnboardBinding
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardFragment : BaseFragment<FragmentStepperOnboardBinding, LoginViewModel>() {

    override val vm: LoginViewModel  by viewModel()

    private val navController by lazy {
        findNavController()
    }
    val MAX_STEP = 3

    var myViewPagerAdapter: MyViewPagerAdapter? = null
    var about_title_array = arrayOf(
        R.string.discount_n_offer,
        R.string.free_delivery,
        R.string.offer_cashback
    )
    val about_description_array = arrayOf(
        R.string.screen_first_dec,
        R.string.screen_second_dec,
        R.string.screen_third_dec
    )
    val about_images_array = intArrayOf(
        R.raw.search,
        R.raw.on_bike,
        R.raw.money
    )
    val back_images_array = intArrayOf(
        R.drawable.ic_screen_1,
        R.drawable.ic_screen_3,
        R.drawable.ic_screen_2
    )

    override fun getLayoutId() = R.layout.fragment_stepper_onboard
    override fun onFragmentReady(view: View) {
        initComponent()

    }

    private fun initComponent() {


        // adding bottom dots
        bottomProgressDots(0)
        myViewPagerAdapter = MyViewPagerAdapter()
        bd.viewPager.setAdapter(myViewPagerAdapter)
        bd.viewPager.addOnPageChangeListener(viewPagerPageChangeListener)
//        bd.tvSkip.setOnClickListener {
//            navController.navigate(R.id.action_fragment_onboard_to_fragment_started)
//        }
        bd.btnNext.setOnClickListener({
            val current = bd.viewPager.getCurrentItem() + 1
            if (current < MAX_STEP) {
                // move to next screen
                bd.viewPager.setCurrentItem(current, true)
            } else {
                navController.navigate(R.id.fragment_onboard_screen_to_started)
            }
        })
    }

    inner class MyViewPagerAdapter : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = getLayoutInflater()
            val view: View =
                layoutInflater!!.inflate(R.layout.fragment_onboard_layout, container, false)
            (view.findViewById<View>(R.id.tv_title) as TextView).setText(
                about_title_array.get(
                    position
                )
            )
            (view.findViewById<View>(R.id.tv_sub_title) as TextView).setText(
                about_description_array.get(
                    position
                )
            )
            (view.findViewById<View>(R.id.img_on_board) as LottieAnimationView).setAnimation(
                about_images_array.get(
                    position
                )
            )
            (view.findViewById<View>(R.id.cl_background) as ConstraintLayout).setBackgroundResource(
                back_images_array.get(
                    position
                )
            )
            (view.findViewById<View>(R.id.button_skip) as MaterialButton).setOnClickListener {
                navController.navigate(R.id.fragment_onboard_screen_to_started)
            }
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return about_title_array.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }


    private fun bottomProgressDots(current_index: Int) {

        val dots =
            arrayOfNulls<ImageView>(MAX_STEP)
        bd.layoutDots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = ImageView(context)
            val width = 20.px
            val height = 4.px
            val params =
                LinearLayout.LayoutParams(ViewGroup.LayoutParams(width, height))
            params.setMargins(10, 10, 10, 10)
            dots[i]!!.layoutParams = params
            dots[i]!!.setImageResource(R.drawable.shape_indicater_inactive)
            bd.layoutDots.addView(dots[i])
        }
        if (dots.size > 0) {
            dots[current_index]!!.setImageResource(R.drawable.shape_indicater_active)

        }
    }

    fun getColor(colorId: Int): Int {
        return ResourcesCompat.getColor(resources, colorId, requireActivity().theme)
    }

    //  viewpager change listener
    var viewPagerPageChangeListener: ViewPager.OnPageChangeListener =
        object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                bottomProgressDots(position)
//                if (position == about_title_array.size - 1) {
//                    btnNext!!.text = getString(R.string.GOT_IT)
//                    btnNext!!.setBackgroundColor(resources.getColor(R.color.orange_400))
//                    btnNext!!.setTextColor(Color.WHITE)
//                } else {
//                    btnNext!!.text = getString(R.string.NEXT)
//                    btnNext!!.setBackgroundColor(resources.getColor(R.color.grey_10))
//                    btnNext!!.setTextColor(resources.getColor(R.color.grey_90))
//                }
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
            override fun onPageScrollStateChanged(arg0: Int) {}
        }


}
