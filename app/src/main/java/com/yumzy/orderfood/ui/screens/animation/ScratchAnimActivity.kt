package com.yumzy.orderfood.ui.screens.animation

import android.animation.Animator
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.utilcode.util.VibrateUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ActivityAnimationorBinding


class ScratchAnimActivity : AppCompatActivity(), Animator.AnimatorListener {
    private var animatedValue = 0

    private val bd: ActivityAnimationorBinding by lazy {
        ActivityAnimationorBinding.inflate(layoutInflater)
    }

    private val animationArray =
        arrayOf(R.raw.success_animation, R.raw.gift_rewards, R.raw.success_animation)
    private val animationCount = animationArray.size
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bd.root)
        bd.lottiAnimationView.addAnimatorListener(this)
        bd.lottiAnimationView.playAnimation()
        VibrateUtils.vibrate(longArrayOf(75, 38, 75, 488, 75, 38, 75, 200, 75, 38, 75, 400), -1)

        /*if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
            intent.hasExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_X) &&
            intent.hasExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_Y)
        ) {
            bd.root.visibility = View.INVISIBLE
            revealX = intent.getIntExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_X, 0)
            revealY = intent.getIntExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_Y, 0)
            val viewTreeObserver: ViewTreeObserver = bd.root.viewTreeObserver
            if (viewTreeObserver.isAlive) {
                viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        revealActivity(revealX, revealY)
                        bd.root.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                    }
                })
            }
        } else {
            bd.root.setVisibility(View.VISIBLE)
        }*/

    }

    /*protected open fun *//*@@lgxeyg@@*//*revealActivity(x:*//*@@zfmmcy@@*//*Int, y:*//*@@zfmmcy@@*//*Int) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            var finalRadius: *//*@@ssbean@@*//*kotlin.Float = (java.lang.Math.max(
                bd.root.getWidth(),
                bd.root.getHeight()
            ) * 1.1) as *//*@@ssbean@@*//*kotlin.Float

            // create the animator for this view (the start radius is zero)
            var circularReveal: *//*@@jlcncv@@*//*android.animation.Animator? =
                ViewAnimationUtils.createCircularReveal(
                    bd.root,
                    x,
                    y,
                    0f,
                    finalRadius
                )
            circularReveal?.setDuration(400)
            circularReveal?.setInterpolator(AccelerateInterpolator())

            // make the view visible and start the animation
            bd.root.setVisibility(android.view.View.VISIBLE)
            circularReveal?.start()
        } else {
            finish()
        }
    }

    protected fun unRevealActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish()
        } else {
            val finalRadius =
                (Math.max(bd.root.getWidth(), bd.root.getHeight()) * 1.1) as Float
            val circularReveal = ViewAnimationUtils.createCircularReveal(
                bd.root, revealX, revealY, finalRadius, 0f
            )
            circularReveal.duration = 400
            circularReveal.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    bd.root.setVisibility(View.INVISIBLE)
                    finish()
                }
            })
            circularReveal.start()
        }
    }
*/
    override fun onAnimationStart(animation: Animator?) {

    }

    override fun onAnimationEnd(animation: Animator?) {
        if (animatedValue == animationCount - 1) {
            val handler = Handler(this.mainLooper)
            val runnable = Runnable {
                finish()
            }

            handler.postDelayed(runnable, 500)
        } else {
            bd.lottiAnimationView.setAnimation(animationArray[animatedValue])
            bd.lottiAnimationView.playAnimation()
        }
        animatedValue++


    }

    override fun onAnimationCancel(animation: Animator?) {
    }

    override fun onAnimationRepeat(animation: Animator?) {

    }
}