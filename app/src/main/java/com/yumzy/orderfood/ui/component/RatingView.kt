package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


/**
 *    Created by Sourav Kumar Pandit
 * */
class RatingView : CardView {

    val i3px = VUtil.dpToPx(3)

    private var starIcon: String = ""

    private var textRating: String? = ""

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        radius = 5.pxf
        starIcon = context.resources.getString(R.string.icon_star)
//        textRating = "NEW"
        this.addView(
            LinearLayout(context).apply {
                layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
//                setPadding(UIHelper.i5px, i3px, UIHelper.i5px, i3px)
                setPadding(i3px, UIHelper.i2px, i3px, UIHelper.i2px)
                gravity = Gravity.CENTER
//                orientation = HORIZONTAL
                this.addView(getFondIconView(Color.BLACK))
                this.addView(getRatingTextView(Color.BLACK))
                /* this.background = GradientDrawable().apply { setColor(Color.WHITE)
                 cornerRadius=VUtil.dpToPxFloat(8f)
                 }*/

            })

        cardElevation = 4f
        setCardBackgroundColor(Color.WHITE)
        this.visibility = View.GONE

    }

    var textColor: Int = Color.WHITE
        set(value) {
            field = value
            val textView = findViewById<TextView>(R.id.tv_ratingtitle)
            textView.setTextColor(textColor)
        }
    var starColor: Int = Color.WHITE
        set(value) {
            field = value
            val textView = findViewById<TextView>(R.id.icon_rating)
            textView.setTextColor(starColor)
        }


    private fun getFondIconView(iconColorInt: Int): FontIconView? {
        return FontIconView(context).apply {
            id = R.id.icon_rating
            text = starIcon
            setTextColor(iconColorInt)
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        }
    }

    private fun getRatingTextView(textColor: Int): TextView {
        return TextView(context).apply {
            id = R.id.tv_ratingtitle
            text = textRating
            setInterFont()
            setTextColor(textColor)
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
            setPadding(i3px, 0, UIHelper.i5px, 0)
        }
    }

    fun setRating(
        rating: String?
    ) {
        this.textRating = rating

        val ratingView = this.findViewById<TextView>(R.id.tv_ratingtitle)
//        ratingView.text = textRating
        if (rating.isNullOrEmpty()) {
            ratingView.visibility = View.GONE
            this.visibility = View.GONE


        } else {
            ratingView.text = textRating.toString()
            ratingView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
            ratingView.visibility = View.VISIBLE
            this.visibility = View.VISIBLE


        }

    }

    fun cardBackground(color: Int) {
        this.setCardBackgroundColor(color)

    }

    fun rattingRadius(ratingRadius: Float) {
        this.radius = ratingRadius

    }
}

