/*
 *   Created by Bhupendra Kumar Sahu
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


open class SpecialInstructionTemplate(
    override val context: Context,
    var instruction: String,
    private val callerFun: (String) -> Unit

) : IDetailsActionTemplate {

    override var headerHeight: Int = VUtil.dpToPx(40)
        set(value) {
            field = VUtil.dpToPx(value)
        }

    override val showClose = false
    /*
     <com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
        android:id="@+id/fontIconView3"
        android:layout_width="@dimen/_65sdp"
        android:layout_height="@dimen/_65sdp"
        android:layout_marginTop="@dimen/_8sdp"
        android:background="@drawable/rounded_pink_50"
        android:gravity="center"
        android:padding="@dimen/_10sdp"
        android:text="@string/icon_042_food_and_drink"
        android:textColor="@color/white"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
     */

    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.setPadding(0, 8.px, 0, 8.px)
        val fontIconView = FontIconView(context)
        val layoutParams = LinearLayout.LayoutParams(65.px, 65.px)
        layoutParams.setMargins(0, 0, 0, 5.px)
        fontIconView.layoutParams = layoutParams
        fontIconView.gravity = Gravity.CENTER
        fontIconView.setPadding(10.px, 10.px, 10.px, 10.px)
        fontIconView.text = context.getString(R.string.icon_commenting_o)
        fontIconView.setTextColor(ThemeConstant.white)
        fontIconView.setBackgroundResource(R.drawable.rounded_pink_50)
        layout.gravity = Gravity.CENTER
        layout.orientation = LinearLayout.VERTICAL
        layout.addView(fontIconView)
        layout.addView(getTextView(R.id.tv_info_header, "Special Instruction"))
        return layout
    }


    @SuppressLint("SetTextI18n")
    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        layout.minimumHeight = headerHeight
        val edittext = EditText(context)
        edittext.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(90))
        edittext.gravity = Gravity.TOP
        edittext.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )
        edittext.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        //edittext.inputType = InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE
        // edittext.maxLines = 4
        edittext.id = R.id.edt_instruction
        edittext.isSingleLine = false
        edittext.setRalewayMediumFont()
        edittext.filters = arrayOf<InputFilter>(LengthFilter(240))
//        edittext.imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
        edittext.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        edittext.hint = context.getString(R.string.enter_special_instruction)
        if (instruction.equals("")) {
            edittext.hint = context.getString(R.string.enter_special_instruction)

        } else {
            edittext.setText(instruction.replace("\n", " ").trim())
        }
        edittext.setHintTextColor(ThemeConstant.textLightGrayColor)
        edittext.setTextColor(ThemeConstant.textBlackColor)

        val contextThemeWrapper = ContextThemeWrapper(context, R.style.Yumzy_Button)
        val btn = MaterialButton(contextThemeWrapper)

        btn.layoutParams = LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 50.px)
        btn.setTextColor(Color.WHITE)
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkies)
        btn.rippleColor = ColorStateList.valueOf(ThemeConstant.rippleColor)
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
        btn.typeface = typeface
        btn.gapTop(VUtil.dpToPx(5))
        btn.text = "Done"
        btn.id = R.id.btn_instruction
        layout.addView(edittext)
        layout.addView(btn)


        return layout
    }

    override fun actions(): View? = null

    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {
        val edittext = view?.findViewById<EditText>(R.id.edt_instruction)

        view?.findViewById<Button>(R.id.btn_instruction)?.setOnClickListener {
            (dialog as Dialog).let { YumUtil.hideDialogKeyboard(it) }
            // or // callerFun(edittext.text.toString())
            edittext?.text?.toString()?.trim()?.let { it1 -> callerFun(it1) }
            dialog.dismiss()


        }


    }

    open fun getTextView(tvId: Int, msg: String): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)
        textView.setInterFont()

        if (tvId == R.id.tv_info_header) {
            textView.setRalewayBoldFont()
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.DKGRAY)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        }

        return textView

    }


}

