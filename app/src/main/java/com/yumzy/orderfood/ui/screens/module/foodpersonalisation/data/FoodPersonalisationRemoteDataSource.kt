package com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.models.FoodPersReqDTO
import com.yumzy.orderfood.data.models.PersonalisationDTO
import javax.inject.Inject

class FoodPersonalisationRemoteDataSource  constructor(private val serviceAPI: ProfileServiceAPI) :
    BaseDataSource() {
    suspend fun getFoodPreferenceTags(dto: PersonalisationDTO) = getResult {
        serviceAPI.getFoodPreferenceTags(dto)
    }

    suspend fun foodPres(dto: FoodPersReqDTO) = getResult {
        serviceAPI.foodPres("update", dto)
    }
}