package com.yumzy.orderfood.ui.component

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.ViewConst

class RateUsActionDialog : DialogFragment, DialogInterface.OnShowListener {
    lateinit var clContext: Context
    private var actionType = -1
    private var actionLabel: String = ""
    private var sTitle: String? = ""
    private var sMessage: String = ""
    var onActionPerformed: OnActionPerformed? = null

    constructor(
        clContext: Context,
        actionType: Int,
        actionLabel: String,
        sTitle: String,
        sMessage: String,
        onActionPerformed: OnActionPerformed?
    ) {
        this.clContext = clContext
        this.onActionPerformed = onActionPerformed
        this.sTitle = sTitle
        this.sMessage = sMessage
        this.actionType = actionType
        this.actionLabel = actionLabel
    }

    constructor()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        super.onCreateDialog(savedInstanceState)

        val clDialog = Dialog(requireContext(), R.style.DialogTheme)
        clDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        clDialog.setContentView(getDialogLayout())


        if (clDialog.window != null) {
            clDialog.window?.setWindowAnimations(R.style.DialogZoomInZoomOut)
            clDialog.window?.setBackgroundDrawableResource(R.drawable.white_rounded_corner)
            clDialog.setOnShowListener(this)
        }



        return clDialog
    }

    private fun getDialogLayout(): View {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setPadding(UIHelper.i20px, UIHelper.i20px, UIHelper.i20px, UIHelper.i20px)
        linearLayout.addView(getIconTitle())
        linearLayout.addView(getTitle())
        linearLayout.addView(getSubTitle())
        linearLayout.addView(getActionLayout())

        return linearLayout


    }

    private fun getActionLayout(): LinearLayout {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_HORIZONTAL


        val btnClose = UIHelper.outlineButton(clContext, "Not Now", null)
        btnClose.minWidth = VUtil.dpToPx(100)
        btnClose.setInterBoldFont()
        btnClose.setOnClickListener {
            onActionPerformed?.onDismissed(actionType, actionLabel)
            dialog?.dismiss()
        }

        val btnOk = UIHelper.filledButton(clContext, "Rate Now", null)
        btnOk.minWidth = VUtil.dpToPx(100)
        btnOk.setInterBoldFont()
        btnOk.setOnClickListener {
            onActionPerformed?.onConfirmDone(this.dialog, actionType, actionLabel)
            dialog?.dismiss()
        }
        linearLayout.addView(getSpacingView())
        linearLayout.addView(btnClose)
        linearLayout.addView(getSpacingView())
        linearLayout.addView(btnOk)
        linearLayout.addView(getSpacingView())
        return linearLayout

    }

    private fun getSpacingView(): View {
        val spaceView = View(clContext)
        spaceView.layoutParams = LinearLayout.LayoutParams(0, ViewConst.WRAP_CONTENT, 1f)
        return spaceView
    }

    private fun getIconTitle(): LinearLayout {
        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        linearLayout.addView(
            getFontIconView(),
            LinearLayout.LayoutParams(VUtil.dpToPx(100), VUtil.dpToPx(100))
        )
        return linearLayout

    }

    private fun getSubTitle(): TextView {
        val textView = TextView(clContext)
        textView.text = sMessage
        textView.gravity = Gravity.CENTER
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        textView.setTextColor(ThemeConstant.textGrayColor)
        textView.setPadding(UIHelper.i10px, UIHelper.i10px, UIHelper.i10px, UIHelper.i30px)
        return textView
    }

    private fun getTitle(): TextView {
        val textView = TextView(clContext)
        textView.text = sTitle
        textView.gravity = Gravity.CENTER_HORIZONTAL
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        textView.setPadding(UIHelper.i5px, 0, 0, 0)
        textView.setTextColor(ThemeConstant.pinkies)
        textView.setInterBoldFont()
        return textView
    }

    private fun getFontIconView(): ImageView {
        val fontIconView = ImageView(clContext)

        fontIconView.background =
            ResourcesCompat.getDrawable(resources, R.drawable.img_success, context?.theme)

        return fontIconView
    }

    override fun onResume() {
        super.onResume()
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        dialog?.window
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    interface OnActionPerformed {
        fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?)
        fun onDismissed(actionType: Int, actionLabel: String?)
    }

    override fun onShow(dialog: DialogInterface?) {

    }
}