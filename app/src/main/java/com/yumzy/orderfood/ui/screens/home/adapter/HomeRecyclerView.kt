package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.listeners.addClickListener
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.common.VerticalItemDecoration
import com.yumzy.orderfood.ui.screens.fastitems.EndListItem
import com.yumzy.orderfood.ui.screens.fastitems.LoadingItem
import com.yumzy.orderfood.ui.screens.fastitems.SeeAllEndListItem
import com.yumzy.orderfood.ui.screens.home.fragment.HomeViewModel
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeTitleListener
import com.yumzy.orderfood.ui.widget.staterecycler.ContentItemLoadingStateFactory
import com.yumzy.orderfood.ui.widget.staterecycler.EmptyStateRecyclerView
import com.yumzy.orderfood.ui.widget.staterecycler.TextStateDisplay
import com.yumzy.orderfood.util.IConstants
import kotlinx.android.synthetic.main.view_section_title.view.*
import java.util.*


class HomeRecyclerView : ConstraintLayout, EmptyStateRecyclerView.OnStateChangedListener {
    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var longitude: String = ""
    private var latitude: String = ""
    private val recyclerView: EmptyStateRecyclerView
    private var itemFinished = false

    private var listReachedEnd: Boolean = false
    private var skip: Int = 0
    private var limit: Int = 7
    var itemLoading = false
    private val listModuleListener: ListModuleListener

    private val refreshLayout: SwipeRefreshLayout

    //save our FastAdapter
    private val fastAdapter: GenericFastItemAdapter

    private val footerAdapter: GenericItemAdapter

    private var endlessScrollListener: EndlessRecyclerOnScrollListener? = null

    lateinit var vm: HomeViewModel
    lateinit var homeActivity: AppCompatActivity
    lateinit var iview: IView


    init {
        listModuleListener = ListModuleListener()
        recyclerView = EmptyStateRecyclerView(context)
        recyclerView.onStateChangedListener = this
        refreshLayout = SwipeRefreshLayout(context)
        fastAdapter = FastItemAdapter()
        footerAdapter = ItemAdapter.items()
//        footerAdapter.add(HomeEndItem())
        setupViewData()
        recyclerView.addItemDecoration(VerticalItemDecoration(3.px, false))
        recyclerView.background = ColorDrawable(Color.WHITE)
    }

    var viewSetup: Boolean = false
    fun setViewModel(appCompatActivity: AppCompatActivity, viewModel: HomeViewModel, iview: IView) {
        this.homeActivity = appCompatActivity
        this.vm = viewModel
        this.iview = iview
        viewSetup = true


    }

    private fun setupViewData() {
        setRecyclerView()
        initAdapter()
        setRecyclerScrollListener()
    }


    private fun setRecyclerScrollListener() {
        recyclerView.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val canScrollVertically = recyclerView.canScrollVertically(-1)
                    recyclerScrollListener?.invoke(canScrollVertically, recyclerView, dx, dy)
                }
            }

        )
    }

    var recyclerScrollListener: ((Boolean, RecyclerView, Int, Int) -> Unit)? = null
//    var homeRecyclerStateChange: ((Boolean, RecyclerView, FastAdapter<GenericItem>) -> Unit)? = null

    private fun initAdapter() {
        fastAdapter.addAdapter(1, footerAdapter)
        recyclerView.adapter = fastAdapter
        recyclerView.overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        setLayoutManager()
        setRecyclerAnimator()
        setEndlessScroll()
        setSpanHelper()
        clickEventListener()

    }

    private fun setSpanHelper() {
//        val snapHelper: SnapHelper = LinearSnapHelper()
//        snapHelper.attachToRecyclerView(recyclerView)
    }

    private fun setRecyclerView() {
        val recycleParam = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val refreshParam = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        refreshLayout.setOnRefreshListener(listModuleListener)
        // setup any states here...
        recyclerView.setStateDisplay(
            EmptyStateRecyclerView.STATE_LOADING,
            ContentItemLoadingStateFactory.newListLoadingState(context)
        )
        recyclerView.setStateDisplay(
            EmptyStateRecyclerView.STATE_EMPTY,
            TextStateDisplay(context, "No content yet", "Please join or create some content.")
        )
        recyclerView.setStateDisplay(
            EmptyStateRecyclerView.STATE_ERROR,
            TextStateDisplay(context, "SORRY...!", "Something went wrong :(")
        )
        recyclerView.layoutParams =
            ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
//        recyclerView.layoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        refreshLayout.addView(recyclerView, recycleParam)
        this.addView(refreshLayout, refreshParam)
    }

    fun fetchHomeList(lat: String, lng: String, fromRefresh: Boolean = false) {
        if (fromRefresh || lat == "null" || lng == "null") return
        if (this.latitude != lat || this.longitude != lng) {
            this.latitude = lat
            this.longitude = lng
            itemFinished = false
            itemLoading = false
            skip = 0
            limit = 7
            footerAdapter.clear()
            fastAdapter.clear()
            recyclerView.visibility = View.VISIBLE
            endlessScrollListener?.resetPageCount()

//            setEndlessScroll()
//            return
        }
        if (!viewSetup) return
        if (itemLoading) return
        val itemCount = fastAdapter.itemCount <= 1
        if (itemCount) listReachedEnd = false
        if (listReachedEnd) {
            footerAdapter.clear()
            footerAdapter.add(SeeAllEndListItem())
            footerAdapter.add(EndListItem())
            return
        }
//        vm.flatHomeList("17.4311184", "78.3722143", skip, limit).observe(
        vm.flatHomeList(this.latitude, this.longitude, skip, limit).observe(
            homeActivity,
            { result ->
                itemLoading = true
                if (result.type == -1) {
                    itemLoading = true
                    footerAdapter.clear()
                    val progressItem = LoadingItem()
                    progressItem.isEnabled = false
                    footerAdapter.add(progressItem)
                    return@observe
                } else {
                    itemLoading = false
                    footerAdapter.clear()
                }
                RHandler<ArrayList<HomeListDTO>>(
                    iview,
                    result
                ) { homeItemList ->
                    footerAdapter.clear()
                    if (result.code == APIConstant.Status.NO_NETWORK) {
                        recyclerView.visibility = View.INVISIBLE
                        iview.showCodeError(result.code, result.title, result.message ?: "")
                        return@RHandler
                    } else if (result.code == APIConstant.Code.NO_DELIVERY_PARTNER) {
                        recyclerView.visibility = View.INVISIBLE
                        iview.showCodeError(result.code, result.title, result.message ?: "")
                        return@RHandler
                    } else if (result.code == APIConstant.Code.NOT_SERVING || result.code == APIConstant.Code.NO_RESTAURANT_AT_LOCATION) {
                        iview.showCodeError(result.code, result.title, result.message ?: "")
                        recyclerView.visibility = View.INVISIBLE
                        return@RHandler
                    }
                    if (homeItemList.isNullOrEmpty()) {
                        listReachedEnd = true
//                        recyclerView.visibility = View.VISIBLE
                        footerAdapter.add(SeeAllEndListItem())
                        footerAdapter.add(EndListItem())
                        return@RHandler
                    }

                    setRecyclerItemData(homeItemList)
                    skip++
                }

            })
    }


    private fun setEndlessScroll() {
        endlessScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                if(latitude == "null" || longitude == "null") return;
                fetchHomeList(latitude, longitude)
            }
        }
        recyclerView.addOnScrollListener(endlessScrollListener!!)
    }

    private fun setRecyclerItemData(homeListDTO: List<HomeListDTO>?) {
        homeListDTO?.forEach { homeListData ->
            recyclerView.visibility = View.VISIBLE
//            recyclerView.invokeState(EmptyStateRecyclerView.STATE_OK)
//            homeRecyclerStateChange?.invoke(false, recyclerView, fastAdapter)
            when (homeListData.flatLayoutType) {
                IConstants.LayoutType.Banner1000.HEADING -> {
                    fastAdapter.add(SectionTitleItem(homeListData))//click handled
                }
                IConstants.LayoutType.Banner1000.TOP_BANNER -> {
                    fastAdapter.add(TopBannerItem(homeListData))//click handled
                }
                IConstants.LayoutType.Banner1000.OFFER_ADS -> {
                    fastAdapter.add(OfferAdsListItem(homeListData))//click handled
                }
                IConstants.LayoutType.Banner1000.ADS_ITEM -> {
                    fastAdapter.add(AdsListItem(homeListData))//click handled
                }

                IConstants.LayoutType.Banner1000.THE_BRANDS -> {
                    fastAdapter.add(BrandListItem(homeListData))//click handled
                }
                IConstants.LayoutType.Banner1000.POPULAR_CUISINES -> {
                    fastAdapter.add(PopularCuisinesItem(homeListData))//click handled
                }
                IConstants.LayoutType.Dish3000.WINTER_SPECIAL -> {
                    fastAdapter.add(WinterSpecialItem(homeListData))//click handled
                }
                IConstants.LayoutType.Restaurant2000.NEW_LAUNCHED -> {
                    fastAdapter.add(NewLaunchItem(homeListData))//click handled
                }
                IConstants.LayoutType.Dish3000.POPULAR_FOOD -> {
                    fastAdapter.add(PopularFoodListItem(homeListData))//click handled
                }
                IConstants.LayoutType.Dish3000.TRENDING_FOODS -> {
                    fastAdapter.add(TrendingFoodsItem(homeListData))//click handled
                }
                IConstants.LayoutType.Web4000.WEB_4001 -> {
                    fastAdapter.add(WebListItem(homeListData))//click handled
                }
                //IConstants.LayoutType.Restaurant2000.FEATURED_BRANDS -> {}
                //IConstants.LayoutType.Dish3000.YUMZY_BEST -> {}
                else -> {
                    fastAdapter.add(HomeListItem(homeListData))
                }

            }
        }
    }

    private fun setLayoutManager(
        linearLayoutManager: LinearLayoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
    ) {
        recyclerView.layoutManager = linearLayoutManager

    }

    private fun setRecyclerAnimator(itemAnimator: RecyclerView.ItemAnimator? = DefaultItemAnimator()) {
        recyclerView.itemAnimator = itemAnimator
    }

    private fun clickEventListener() {
        fastAdapter.addClickListener<ViewBinding, GenericItem>({ binding -> binding.root })
        { v, position, fastAdapter, item ->
//            AlerterHelper.showToast(homeActivity, "asdf", "5sdf54")

            when (item) {
                is HomeListItem -> {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                is SeeAllEndListItem -> {
                    globalHomeItemListener?.invoke(
                        v,
                        HomeListDTO(IConstants.LayoutType.Restaurant2000.TOP_RESTAURANTS).apply {
                            this.landingPage = IConstants.LandingPageType.RESTAURANTS
                            title = "Restaurants Near You"
                        })
                }
                is TrendingFoodsItem -> {
                    globalHomeItemListener?.invoke(v, item.model)
                }
            }
        }
        fastAdapter.addEventHook(object : ClickEventHook<IItem<*>>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                val itemView = viewHolder.itemView
                val seeAll = itemView.findViewById<TextView>(R.id.tv_see_all)
                return seeAll
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<IItem<*>>,
                item: IItem<*>
            ) {
                when (item) {
                    is SectionTitleItem -> {
                        val model = item.model
                        globalHomeTitleListener?.invoke(v, model)
                    }
                }
            }
        })


    }

    open inner class ListModuleListener : SwipeRefreshLayout.OnRefreshListener {
        override fun onRefresh() {
            skip = 0
            limit = 7
            itemFinished = false
            itemLoading = false
            android.os.Handler(homeActivity.mainLooper).postDelayed({
                refreshLayout.isRefreshing = false
                footerAdapter.clear()
                fastAdapter.clear()
                endlessScrollListener?.resetPageCount()
                fetchHomeList(latitude, longitude, true)
            }, 700)


        }


    }

    override fun onStateChanged(state: Byte) {
        if (EmptyStateRecyclerView.STATE_ERROR == state) {

        }
    }

    fun scrollToPosition(position: Int) {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        val scrollPosition: Int = linearLayoutManager.findFirstVisibleItemPosition()
        if (scrollPosition > 10) {
            linearLayoutManager.scrollToPosition(9)
        }
        linearLayoutManager.smoothScrollToPosition(recyclerView, null, position)

    }

    fun clearData() {
        fastAdapter.clear()
        recyclerView.visibility = View.GONE
    }
}