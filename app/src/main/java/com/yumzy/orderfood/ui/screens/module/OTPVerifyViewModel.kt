package com.yumzy.orderfood.ui.screens.module

import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import javax.inject.Inject

class OTPVerifyViewModel  constructor(private val repository: LoginRepository) :
    BaseViewModel<IView>() {

    fun verifyOTP(number: String, otpCode: String, deviceId: String) = repository
        .verifyOTP(number, otpCode, "+91", deviceId)

    fun resendOTP(number: String) = repository
        .resendOTP(number, APIConstant.Actions.TEXT, "")

}