package com.yumzy.orderfood.ui.screens.profile

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ProfileViewModel  constructor(
    private val repository: ProfileRepository
) : BaseViewModel<IProfileView>() {

    fun userDetails() = repository.profileUser()
    fun userUpdateDetails(map: Map<String, String>) = repository.updateProfileUser(map)
    fun userUpdateProfileImage(pro: MutableMap<String, Any>) =
        repository.userUpdateProfileImage(pro)

    fun logoutUser() = repository.logoutUser()
    fun clearUser() = repository.removeUserDTO()
    fun removeSelectedAddress() = repository.removeSelectedAddress()
}