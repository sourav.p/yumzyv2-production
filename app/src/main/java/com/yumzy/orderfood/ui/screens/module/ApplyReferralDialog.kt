package com.yumzy.orderfood.ui.screens.module

import android.content.DialogInterface
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.databinding.DialogApplyCouponBinding
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.base.BaseSheetFragment
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.android.ext.android.inject
import javax.inject.Inject

class ApplyReferralDialog : BaseSheetFragment<DialogApplyCouponBinding, CouponViewModel>(),
    TextWatcher {

    
    override val vm: CouponViewModel by inject()
    lateinit var userId: String

    
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun getLayoutId(): Int = R.layout.dialog_apply_coupon
    override fun onDialogCreated(view: View) {
        setUIData()
    }

    private fun setUIData() {
        applyDebouchingClickListener(bd.btnClaim, bd.fiCloseView)
    }


    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(
        s: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {

    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {

            R.id.fiCloseView -> {
                YumUtil.hideDialogKeyboard(requireDialog())
                dialog?.dismiss()
                sharedPrefsHelper.saveIsNewUser(false)
            }
            R.id.btn_claim -> {
                val referral = bd.edtInputField.text.toString()
                if (referral.isEmpty()) {
                    YumUtil.animateShake(bd.edtInputField, 50, 15)
                    return
                }
                vm.applyReferral(userId, referral).observe(this, { result ->
                    if (result.type == APIConstant.Status.SUCCESS && result.code == APIConstant.Code.DATA_UPDATED) {
                        dialog?.dismiss()
                        YumUtil.hideDialogKeyboard(requireDialog())
                        AppUIController.showApplyREferralUI()
                        Handler(requireContext().mainLooper).postDelayed(
                            {

                                //  showToast("Referral Applied")
                                bd.lottiAnimation.visibility = View.VISIBLE
                                bd.lottiAnimation.playAnimation()
                            }, 1500

                        )
                        sharedPrefsHelper.saveIsNewUser(false)

                    } else if (result.type == APIConstant.Status.ERROR && result.code == APIConstant.Code.INVALID_REFERRAL) {
                        showToast(result.title.toString())

                    }
                })
            }

        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        super.showCodeError(
            code,
            title,
            message
        )
    }

    override fun onResponse(code: Int?, response: Any?) {
    }


    override fun onDismiss(dialog: DialogInterface) {
        vm.detachView()
        sharedPrefsHelper.saveIsNewUser(false)
        YumUtil.hideDialogKeyboard(requireDialog())
        super.onDismiss(dialog)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }


}
