package com.yumzy.orderfood.ui.helper

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import com.tapadoo.alerter.Alerter
import com.yumzy.orderfood.R
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable

object AlerterHelper {
    @JvmStatic
    fun showError(context: Context?, message: String?) {
        Alerter.create((context as AppCompatActivity?)!!)
            .setTitle(R.string.error)
            .setText(message!!)
            .setIcon(R.drawable.ic_error)
            .setBackgroundColorRes(R.color.red)
            .show()
    }

    @JvmStatic
    fun showWarning(context: Context?, message: String?) {
        Alerter.create((context as AppCompatActivity?)!!)
            .setTitle(R.string.warning)
            .setText(message!!)
            .setIcon(R.drawable.ic_warning)
            .setBackgroundColorRes(R.color.quantum_orange)
            .show()
    }

    @JvmStatic
    fun showInfo(context: Context?, message: String?) {
        Alerter.create((context as AppCompatActivity?)!!)
            .setTitle(R.string.info)
            .setText(message!!)
            .setIcon(R.drawable.ic_info)
            .setBackgroundColorRes(R.color.pinkies)
            .show()
    }

    @JvmStatic
    fun showToast(context: Context?, title: CharSequence, message: CharSequence) {
        val iconFontDrawable = IconFontDrawable(context, R.string.icon_info)
        iconFontDrawable.setTextColor(Color.WHITE)
        Alerter.create((context as AppCompatActivity?)!!)
            .setTitle(title)
            .setText(message)
            .setIcon(iconFontDrawable)
            .setBackgroundColorRes(R.color.pinkies)
            .show()
    }
}