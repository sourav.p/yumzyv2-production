package com.yumzy.orderfood.ui.screens.offer

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.AllCaps
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CouponListResDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityOfferBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class OfferActivity : BaseActivity<ActivityOfferBinding, OfferViewModel>(), IOffer {
    var flag: Int = 0

    override val vm: OfferViewModel by viewModel()
    
    var header:String=""

//    
//    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_offer)
        vm.attachView(this)


        setSupportActionBar(bd.mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        applyDebouchingClickListener(bd.btnApplyCoupon)

        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
//        val typeface = ResourcesCompat.getFont(this, R.font.raleway_medium)
//        bd.mainCollapsing.setCollapsedTitleTypeface(typeface)
//        bd.mainCollapsing.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
//        bd.mainCollapsing.setExpandedTitleTextAppearance(R.style.ExpandAppBarBlackText);


        bd.edtCoupon.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.length > 0) {

                    bd.btnApplyCoupon.isEnabled = true
                    bd.edtCoupon.filters = arrayOf<InputFilter>(AllCaps())

                    //   bd.btnApplyCoupon.setBackgroundResource(R.drawable.coupon_button_dark_background)
                } else {
                    //   bd.btnApplyCoupon.setBackgroundResource(R.drawable.coupon_button_background)
                    bd.btnApplyCoupon.isEnabled = false


                }
            }
        })
        YumUtil.hideKeyboard(this)

        flag = intent.getIntExtra(IConstants.ActivitiesFlags.FLAG, 0)

        when (flag) {
            IConstants.ActivitiesFlags.FROM_HOME -> {
                //  supportActionBar?.title = resources.getString(R.string.offers)
                bd.llEnterCoupon.visibility = View.GONE
                header = resources.getString(R.string.offers)
                /*bd.blobTitleStyleDTO = YumUtil.getBlobStyleObject(
                    "\n"+resources.getString(R.string.n_offers),
                    ThemeConstant.alphaBlackColor,
                    R.style.TextAppearance_MyTheme_Headline4_White,
                    true,
                    3,
                    ThemeConstant.alphaBlackColor
                )*/

            }
            IConstants.ActivitiesFlags.FROM_PLATE -> {
                //   supportActionBar?.title = resources.getString(R.string.applycoupon)
                bd.llEnterCoupon.visibility = View.VISIBLE
                header = resources.getString(R.string.applycoupon)

                /* bd.blobTitleStyleDTO = YumUtil.getBlobStyleObject(
                     "\n"+resources.getString(R.string.your_coupon),
                     ThemeConstant.alphaBlackColor,
                     R.style.TextAppearance_MyTheme_Headline4_White,
                     true,
                     3,
                     ThemeConstant.alphaBlackColor
                 )
 */

            }
        }
        bd.mainToolbar.title = header
        fetchCouponList()
    }
    override val enableEdgeToEdge: Boolean=true
    override val navigationPadding: Boolean=true

    private fun getSelCoupon(couponCode: String, couponId: String) {
        YumUtil.hideKeyboard(this)
        val intent = Intent()
        intent.putExtra(IConstants.ActivitiesFlags.RESULT_KEY, couponCode)
        intent.putExtra(IConstants.ActivitiesFlags.COUPON_ID, couponId)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun fetchCouponList() {
        vm.getFetchCouponList(
            sharedPrefsHelper.getUserId().toString(),
            sharedPrefsHelper.getCartId(),
            sharedPrefsHelper.getOutletID()
        ).observe(this,
            { result ->
                RHandler<List<CouponListResDTO>>(
                    this,
                    result
                ) {
                    couponListResponse(it)
                }

            })

    }

    private fun couponListResponse(data: List<CouponListResDTO>) {
        if (data.isNullOrEmpty()) {
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.rvCoupons.visibility = View.GONE


        } else {
            bd.noDataToDisplay.visibility = View.GONE
            bd.rvCoupons.visibility = View.VISIBLE

        this.let {
            bd.rvCoupons.adapter =
                CouponsAdapter(it, flag, data, clickedListener = { couponCode: String, couponId: String ->  getSelCoupon(couponCode,couponId) })
            bd.rvCoupons.layoutManager = LinearLayoutManager(it)

        }}
    }

    private fun onPostSelected(position: Int) {
        showToast("clicked $position")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.btn_apply_coupon -> {
                if (bd.edtCoupon.text.isEmpty()) {
                    YumUtil.animateShake(bd.edtCoupon, 20, 5)

                } else {
                    getSelCoupon(bd.edtCoupon.text.toString().trim(), "")
                }
            }
        }
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {
            bd.llEnterCoupon.visibility = View.GONE
            bd.rvCoupons.visibility = View.GONE

            bd.noDataToDisplay.visibility = View.VISIBLE
            //  bd.tvInfo.visibility = View.GONE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_coupon
        }
    }
}

