package com.yumzy.orderfood.ui.screens.restaurant

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.FoodMenuDTO
import com.yumzy.orderfood.databinding.AdapterRestaurentMenuBinding
import com.yumzy.orderfood.databinding.AdapterTypeHeaderTextBinding
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.util.IConstants

/**
 * Created by Bhupendra Kumar Sahu.
 */
class RestaurantMenuAdapter(
    val context: Context,
    private val menuList: List<FoodMenuDTO.MenuItem>,
    private val clickedListener: (adapterPosition: Int, itemId: String?, count: Int, price: String, outletId: String) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            IConstants.LayoutType.RESTAURANT_ITEM -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val itemBinding: AdapterRestaurentMenuBinding =
                    AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)
                ViewHolderItem(itemBinding)
            }
            else -> {

                val layoutInflater = LayoutInflater.from(parent.context)
                val itemBinding: AdapterTypeHeaderTextBinding =
                    AdapterTypeHeaderTextBinding.inflate(layoutInflater, parent, false)
                ViewHolderCategory(itemBinding)
            }
        }

    }

    override fun getItemCount(): Int {

        return (menuList.size)
    }

    override fun getItemViewType(position: Int): Int {

        return when (menuList[position].type) {
            IConstants.LayoutType.RestaurantsType.RESTAURANT_ITEM -> IConstants.LayoutType.RESTAURANT_ITEM
            else -> IConstants.LayoutType.RESTAURANT_LABEL
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolderCategory -> {
                holder.bind(menuList[position])
            }
            is ViewHolderItem -> {
                holder.bind(menuList[position])

            }
        }
    }

    inner class ViewHolderItem(itemBinding: AdapterRestaurentMenuBinding) :
        RecyclerView.ViewHolder(itemBinding.root), AddItemView.AddItemListener {

        var adapterBinding: AdapterRestaurentMenuBinding = itemBinding

        fun bind(foodMenuDTO: FoodMenuDTO.MenuItem) {

            adapterBinding.addItemView.setItemOfferPrice(foodMenuDTO.price.toString())
            adapterBinding.addItemView.setTitles(foodMenuDTO.name)
            adapterBinding.addItemView.itemId = foodMenuDTO.itemId
            adapterBinding.addItemView.setImageShow(false)


            adapterBinding.addItemView.isVeg = foodMenuDTO.isVeg
            adapterBinding.addItemView.addItenListener = this


        }

        override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {
            clickedListener(adapterPosition, itemId, count, priced, outletId)
        }
    }
}

class ViewHolderCategory(itemView: AdapterTypeHeaderTextBinding) :
    RecyclerView.ViewHolder(itemView.root) {

    var adapterBinding: AdapterTypeHeaderTextBinding = itemView

    fun bind(foodMenuDTO: FoodMenuDTO.MenuItem) {

        adapterBinding.tvHeaderType.text = foodMenuDTO.name

    }
}
