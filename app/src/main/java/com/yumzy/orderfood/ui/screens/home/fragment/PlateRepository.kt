package com.yumzy.orderfood.ui.screens.home.fragment

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.backgroundLiveData
import com.yumzy.orderfood.worker.networkLiveData
import com.yumzy.orderfood.worker.singleNetoworkEmit
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class PlateRepository  constructor(
    private val selectedItemDao: SelectedItemDao,
    private val cartDao: CartResDao,
    private val remoteDataSource: PlateRemoteDataSource
) {

    /*  fun getUserDTO() = databaseLiveData {
          userInfoDao.getUser()
      }*/

    fun preCheckout(preCheckout: PreCheckoutReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.preCheckout(preCheckout)
        }
    ).distinctUntilChanged()

    fun applyCoupon(applyCoupon: MutableMap<String, Any>) = singleNetoworkEmit(
        networkCall = {
            remoteDataSource.applyCoupon(applyCoupon)
        },
        handleResponse = { data, status ->
            when (status) {
                APIConstant.Status.SUCCESS -> {
                    cartDao.updateOrInsert(data)
                }
                APIConstant.Status.ERROR -> {

                }
            }
        }
    ).distinctUntilChanged()

    fun getCart(cartId: String) = networkLiveData(

        networkCall = { remoteDataSource.getCart(cartId) }


    ).distinctUntilChanged()

    fun addToCart(addCart: NewAddItemDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.addToCart(addCart)
        }
    ).distinctUntilChanged()

    fun newOrder(newOrder: NewOrderReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.newOrder(newOrder)
        }
    ).distinctUntilChanged()

    fun postCheckout(orderId: String) = networkLiveData<Any>(
        networkCall = {
            remoteDataSource.postCheckout(orderId)
        }
    )

    fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.itemCount(position, count, itemCount)
        }
    ).distinctUntilChanged()


    fun updateItem(
        position: Int,
        addCart: NewAddItemDTO
    ) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.updateItem(position, addCart)
        }
    ).distinctUntilChanged()

    fun clearCart(cartId: ClearCartReqDTO) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.clearCart(cartId)
        }
    ).distinctUntilChanged()

    fun removeCoupon(removeCouponReqDTO: MutableMap<String, Any>) = networkLiveData<Any>(
        networkCall = {

            remoteDataSource.removeCoupon(removeCouponReqDTO)
        }
    ).distinctUntilChanged()

    fun getAddressList() = networkLiveData(
        networkCall = {
            remoteDataSource.getAddressList()
        }
    ).distinctUntilChanged()

    fun getUserWallet(mobile: Long) = networkLiveData(
        networkCall = {
            remoteDataSource.getUserWallet(mobile)
        }
    )

    fun useWallet(cartId: HashMap<String, String>) = networkLiveData(
        networkCall = {
            remoteDataSource.useWallet(cartId)
        }

    )
        .distinctUntilChanged()

    fun unUseWallet(cartId: HashMap<String, String>) = networkLiveData(
        networkCall = {
            remoteDataSource.unUseWallet(cartId)
        }
    )
        .distinctUntilChanged()

    fun removeSelectionByItemId(itemId: String?) = backgroundLiveData<Boolean>(

        backgroundCallback = {
            if (itemId != null) {
                selectedItemDao.removeItemById(itemId)
                ResponseDTO.executed(data = true)
            } else
                ResponseDTO.executed(data = false)

        }
    )

}