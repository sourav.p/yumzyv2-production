package com.yumzy.orderfood.ui.screens.offer.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import javax.inject.Inject


class OfferRemoteDataSource  constructor(private val serviceAPI: CartServiceAPI) :
    BaseDataSource() {
    suspend fun getFetchCouponList(userID: String, cartId: String?, outletID: String) = getResult {
        serviceAPI.getFetchCouponList(userID,cartId, outletID)
    }

    suspend fun applyCoupon(applyCoupon: MutableMap<String, Any>) = getResult {
        serviceAPI.applyCoupon(applyCoupon)
    }

}