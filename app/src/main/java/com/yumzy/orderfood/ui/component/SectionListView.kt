/*
 * Created By Shriom Tripathi 29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterFont
import com.laalsa.laalsalib.ui.VUtil

class SectionListView : LinearLayout {

    private val i10px = VUtil.dpToPx(10)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        this.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.orientation = VERTICAL
    }

    fun <T, V : View> setUpList(
        sectionTitle: String,
        items: List<T>,
        onBind: (item: T) -> V
    ) {

        this.addView(getTextView(sectionTitle))
        for (item in items) {
            this.addView(onBind(item))
        }

    }

    private fun getTextView(text: String): TextView? {
        val textView = TextView(context)
        textView.setInterFont()
        val i8px = VUtil.dpToPx(8)
        val i5px = VUtil.dpToPx(5)

        textView.setPadding(i8px, i5px, i8px, i5px)
        textView.setInterFont()
        textView.setTextColor(ThemeConstant.textBlackColor)

        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        textView.maxLines = 1
        return textView

    }

}