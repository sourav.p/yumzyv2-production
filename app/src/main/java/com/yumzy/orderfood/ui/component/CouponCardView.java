package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.laalsa.laalsalib.ui.VUtil;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.UIHelper;
import com.yumzy.orderfood.util.ViewConst;
import com.yumzy.orderfood.util.viewutils.YumUtil;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class CouponCardView extends LinearLayout implements View.OnClickListener {

    private String discount;
    private String couponCode;
    private String header;
    private String subHeader;
    private String orderAbove;
    private TextView apply;
    private TextView tvTerms;
    private TermsListener termsandCondition;


    public CouponCardView(Context context) {
        this(context, null);
    }

    public CouponCardView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CouponCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (header == null) {
            header = "";
        }
        if (subHeader == null) {
            subHeader = "";
        }
        if (discount == null) {
            discount = "";
        }
        if (couponCode == null) {
            couponCode = "";
        }
        if (orderAbove == null) {
            orderAbove = "";
        }
        if (apply == null) {

        }
        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CouponCardView, defStyleAttr, 0);
        couponCode = a.getString(R.styleable.CouponCardView_couponCode);
        header = a.getString(R.styleable.CouponCardView_header);
        subHeader = a.getString(R.styleable.CouponCardView_subHeader);
        discount = a.getString(R.styleable.CouponCardView_discount);
        orderAbove = a.getString(R.styleable.CouponCardView_order_above);
        a.recycle();
    }


    public void init() {
        this.setOrientation(HORIZONTAL);
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int i105px = VUtil.dpToPx(115);
        int i150px = VUtil.dpToPx(150);
        int i8px = VUtil.dpToPx(8);
        int i12px = VUtil.dpToPx(12);
        int i5px = VUtil.dpToPx(5);

        LinearLayout leftContainer = setLeftContainer(i105px, i150px, i5px);
        ConstraintLayout rightContainer = setRightContainer(i8px, i12px);
        this.addView(leftContainer);
        this.addView(rightContainer);
    }

    @NotNull
    private ConstraintLayout setRightContainer(int i8px, int i12px) {
        ConstraintLayout rightContainer = new ConstraintLayout(getContext());
        rightContainer.setBackgroundResource(R.drawable.coupon_bg_right);
        LayoutParams rightLayoutParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        rightContainer.setLayoutParams(rightLayoutParams);
        rightContainer.setPadding(UIHelper.INSTANCE.getI15px(), UIHelper.INSTANCE.getI10px(), UIHelper.INSTANCE.getI10px(), UIHelper.INSTANCE.getI10px());

        LinearLayout layout = new LinearLayout(getContext());
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ViewConst.MATCH_PARENT, 0);
        params.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        params.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;

        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);


        LinearLayout applyCouponLayout = new LinearLayout(getContext());
        LayoutParams rightLayoutParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        applyCouponLayout.setLayoutParams(rightLayoutParam);
        applyCouponLayout.setOrientation(HORIZONTAL);
        applyCouponLayout.setWeightSum(10);
        applyCouponLayout.setPadding(0, UIHelper.INSTANCE.getI5px(), UIHelper.INSTANCE.getI10px(), UIHelper.INSTANCE.getI5px());
        applyCouponLayout.setGravity(Gravity.CENTER_VERTICAL);

        TextView headers = new TextView(getContext());
        LayoutParams headParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 2f);

        headers.setText(header);
        headers.setId(R.id.tv_coupon_header);
        YumUtil.INSTANCE.setInterFontBold(getContext(), headers);
        headers.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        headers.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
        headers.setLayoutParams(headParam);


        TextView applyText = new TextView(getContext());
        LayoutParams applyParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 8f);

        applyText.setText(R.string.apply);
        applyText.setId(R.id.tv_apply);
        applyText.setOnClickListener(this);
        applyText.setGravity(Gravity.END | Gravity.TOP);
        YumUtil.INSTANCE.setInterFontBold(getContext(), applyText);
        applyText.setTextColor(ContextCompat.getColor(getContext(), R.color.blueJeans));
        applyText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        applyText.setLayoutParams(applyParam);


        applyCouponLayout.addView(headers);
        applyCouponLayout.addView(applyText);


        TextView subHeaders = new TextView(getContext());
        subHeaders.setText(subHeader);
        subHeaders.setLayoutParams(new LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 0, 1f));
        subHeaders.setMaxLines(2);
        subHeaders.setMinLines(2);
        subHeaders.setEllipsize(TextUtils.TruncateAt.END);
        subHeaders.setId(R.id.tv_coupon_subheaders);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), subHeaders);
        subHeaders.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        subHeaders.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);


        TextView terms = new TextView(getContext());
        terms.setText(R.string.terms);
        terms.setId(R.id.tv_trems);
        terms.setOnClickListener(this);
        terms.setPadding(0, UIHelper.INSTANCE.getI5px(), 0, 0);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), terms);
        terms.setTextColor(ContextCompat.getColor(getContext(), R.color.blueJeans));
        terms.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);

        TextView minAmount = new TextView(getContext());
        minAmount.setText(orderAbove);
        minAmount.setId(R.id.tv_order_above);
        minAmount.setMaxLines(1);

        minAmount.setPadding(0, UIHelper.INSTANCE.getI2px(), 0, UIHelper.INSTANCE.getI10px());
        minAmount.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkies));
        minAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), minAmount);
        layout.addView(applyCouponLayout);
        layout.addView(subHeaders);
        layout.addView(terms);
        layout.addView(minAmount);
        rightContainer.addView(layout);
        return rightContainer;
    }

    @NotNull
    private LinearLayout setLeftContainer(int i105px, int i150px, int i5px) {
        LinearLayout leftContainer = new LinearLayout(getContext());
        leftContainer.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        leftContainer.setBackgroundResource(R.drawable.coupon_bg_left);
        LayoutParams containerLayoutParams = new LayoutParams(i105px, i150px);
        leftContainer.setLayoutParams(containerLayoutParams);
        leftContainer.setOrientation(VERTICAL);


        TextView couponHeader = new TextView(getContext());
        couponHeader.setText(R.string.couponcode);
        couponHeader.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), couponHeader);
        couponHeader.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        couponHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9f);

        TextView couponCodes = new TextView(getContext());
        couponCodes.setText(couponCode);
        couponCodes.setPadding(VUtil.dpToPx(6), 0, 0, VUtil.dpToPx(6));
        couponCodes.setMinLines(1);
        couponCodes.setMaxLines(2);

        couponCodes.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        couponCodes.setId(R.id.tv_couponcode);
        YumUtil.INSTANCE.setInterFontBold(getContext(), couponCodes);
        couponCodes.setTextColor(ContextCompat.getColor(getContext(), R.color.quantum_yellow));
        couponCodes.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);

        TextView discounts = new TextView(getContext());
        discounts.setPadding(0, i5px, 0, 0);
        discounts.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        discounts.setText(discount);
        discounts.setId(R.id.tv_discount);
        YumUtil.INSTANCE.setInterFontBold(getContext(), discounts);
        discounts.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        discounts.setTextSize(TypedValue.COMPLEX_UNIT_SP, 27f);


        TextView discountHeader = new TextView(getContext());
        discountHeader.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        discountHeader.setText(R.string.discount);
        //discountHeader.setId(R.id.tv_address_tag);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), discountHeader);
        discountHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        discountHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f);

        leftContainer.addView(couponHeader);
        leftContainer.addView(couponCodes);
        leftContainer.addView(discounts);
        leftContainer.addView(discountHeader);
        return leftContainer;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
        ((TextView) findViewById(R.id.tv_discount)).setText(this.discount);

    }

    public TextView getApplyTextView() {
        return this.findViewById(R.id.tv_apply);
    }


    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
        ((TextView) findViewById(R.id.tv_couponcode)).setText(this.couponCode);
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
        ((TextView) findViewById(R.id.tv_coupon_header)).setText(this.header);

    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
        ((TextView) findViewById(R.id.tv_coupon_subheaders)).setText(this.subHeader);

    }

    public String getOrderAbove() {
        return orderAbove;
    }

    public void setOrderAbove(String orderAbove) {
        this.orderAbove = orderAbove;
        ((TextView) findViewById(R.id.tv_order_above)).setText(this.orderAbove);
    }

    public TextView getTvTerms() {
        return this.findViewById(R.id.tv_trems);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_trems:
                if (termsandCondition != null)
                    termsandCondition.terms();
                break;


        }
    }

    public TermsListener getTermsandCondition() {
        return termsandCondition;
    }

    public void setTermsandCondition(TermsListener termsandCondition) {
        this.termsandCondition = termsandCondition;
    }

    public interface TermsListener {
        void terms();

    }
}
