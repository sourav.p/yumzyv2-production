package com.yumzy.orderfood.ui.screens.restaurant.adapter

/*

class OutletTopAdapter(
    val context: Context? = null,
    val interaction: OutletInteraction? = null
) : RecyclerView.Adapter<OutletTopAdapter.GridViewHolder>() {


    private var vegEnable: Boolean = false
    private val menuItems: ArrayList<RestaurantItem> = ArrayList()
    private val selectedItem: ArrayList<ValuePairSI> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val verticalCardLayout =
            RecommendedCardLayout(context = context).apply {
                imageRatio = "1:0.8"
            }
        verticalCardLayout.removeAddQuantity()
        return GridViewHolder(verticalCardLayout, interaction)
    }

    override fun getItemCount() = menuItems.size


    fun setListData(itemList: List<RestaurantItem>) {
        menuItems.clear()
//        menuItems.addAll(getVegList(this.vegEnable, itemList))
        menuItems.addAll(itemList)
        interaction?.adapterSizeChange(menuItems.size)
        notifyDataSetChanged()
    }

    fun getVegList(
        showVeg: Boolean,
        itemList: List<RestaurantItem>
    ): List<RestaurantItem> {
        if (showVeg) {
            val vegList = ArrayList<RestaurantItem>()

            itemList.forEach { menuItem ->
                if (menuItem.isVeg) {
                    vegList.add(menuItem)
                }
            }

            return vegList
        } else {
            return itemList
        }
    }

    fun updateList(newList: List<RestaurantItem>?) {
        val diffResult = DiffUtil.calculateDiff(MyDiffCallback(newList, this.menuItems))
        diffResult.dispatchUpdatesTo(this)
    }

    fun setSelectedItemData(itemList: List<ValuePairSI>) {
        selectedItem.clear()
        selectedItem.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) =
        holder.bind(menuItems[position])

    fun setVegOnly() {
        (context as OutletActivity).let {
            it.vm.viewModelScope.launch(Dispatchers.Main) {
                it.vm.isVeg.observe(it, Observer { value ->
                    updateList(getVegList(value, menuItems))
                })

            }
        }
    }

    inner class GridViewHolder(
        val layout: RecommendedCardLayout,
        val interaction: OutletInteraction?
    ) : RecyclerView.ViewHolder(layout) {

        private lateinit var restaurantItem: RestaurantItem

        fun bind(item: RestaurantItem) = with(itemView) {
            setOnClickListener {
                interaction?.onItemSelected(adapterPosition, restaurantItem)
            }

            restaurantItem = item
            layout.itemTitle = item.name.capitalize()
            layout.ribbonText = item.ribbon.capitalize()
            layout.displayType = item.displayType.capitalize()
            layout.itemSubtitle = item.description.capitalize()
            layout.isVeg = item.isVeg
            layout.imageUrl = item.imageUrl
            layout.price = "₹" + item.price.toString()
            val zoneOffer = YumUtil.setRecZoneOfferString(item.offers)
            layout.colorText = zoneOffer
            var itemQuantity = 0
            selectedItem.run {
                this.forEach {
                    if (it.id == item.itemId)
                        itemQuantity = it.value
                    return@forEach
                }
            }

            layout.getAddQuantityView()?.quantityCount = itemQuantity

            layout.setQuantitiyListener { count ->
                interaction?.onItemCountChange(
                    object : ICartHandler {
                        override fun itemAdded(count: Int) {}

                        override fun itemRepeated() {}

                        override fun failedToAdd(count: Int, view: AddQuantityView) {
                            view.quantityCount = menuItems[adapterPosition].prevQuantity
                        }
                    },
                    count,
                    restaurantItem,
                    layout.getAddQuantityView()!!
                )
            }
        }

    }
}

*/
