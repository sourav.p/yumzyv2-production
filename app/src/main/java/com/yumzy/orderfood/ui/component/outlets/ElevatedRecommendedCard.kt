package com.yumzy.orderfood.ui.component.outlets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutElevatedRecomendedBinding

class ElevatedRecommendedCard : FrameLayout {

    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutElevatedRecomendedBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.ApplyCouponView,
                    defStyleAttr,
                    0
                )

//            couponAppliedMsg =
//                a.getString(R.styleable.ApplyCouponView_couponApplyMessage) ?: "Apply Coupon"
//            appliedCode = a.getString(R.styleable.ApplyCouponView_couponAppliedCode) ?: ""


            a.recycle()

        }

    }
}