package com.yumzy.orderfood.ui.screens.offer

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.offer.data.OfferRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class OfferViewModel  constructor(
    private val repository: OfferRepository
) : BaseViewModel<IOffer>() {
   fun applyCoupon(applyCoupon: MutableMap<String, Any>) = repository.applyCoupon(applyCoupon)
    fun getFetchCouponList(userID: String, cartId: String?, outletID: String) =
        repository.getFetchCouponList(userID, cartId, outletID)
}