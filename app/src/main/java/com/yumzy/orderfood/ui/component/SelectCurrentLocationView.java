package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;
import com.laalsa.laalsalib.ui.VUtil;

/*
 * Created by Bhupendra Sahu
 * */
public class SelectCurrentLocationView extends LinearLayout {
    private String header;
    private String subHeader;
    private String icon;


    public SelectCurrentLocationView(Context context) {
        this(context, null);
    }

    public SelectCurrentLocationView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectCurrentLocationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (header == null) {
            header = "";
        }
        if (subHeader == null) {
            subHeader = "";
        }
        if (icon == null) {
            icon = "";
        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SelectCurrentLocationView, defStyleAttr, 0);
        header = a.getString(R.styleable.SelectCurrentLocationView_location_icon);
        icon = a.getString(R.styleable.SelectCurrentLocationView_location_header);
        subHeader = a.getString(R.styleable.SelectCurrentLocationView_location_subheader);
        a.recycle();
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
        ((TextView) this.findViewById(R.id.iv_location)).setText(this.icon);

    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
        ((TextView) this.findViewById(R.id.tv_header)).setText(this.header);

    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
        ((TextView) this.findViewById(R.id.tv_sub_header)).setText(this.subHeader);

    }

    private void init() {
        this.setOrientation(LinearLayout.HORIZONTAL);

        this.setId(R.id.ll_current_location);
        int i10px = VUtil.dpToPx(10);
        int i50px = VUtil.dpToPx(50);
        FontIconView iconView = new FontIconView(getContext());
        iconView.setLayoutParams(new LinearLayout.LayoutParams(i50px, i50px));
        iconView.setPadding(i10px, i10px, i10px, i10px);
        iconView.setId(R.id.iv_location);
        iconView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        iconView.setTextColor(getResources().getColor(R.color.pinkRoseDark));


        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER_VERTICAL);
        layout.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.addView(getTextView(true, header));
        layout.addView(getTextView(false, subHeader));

        this.addView(iconView);
        this.addView(layout);


    }

    private View getTextView(boolean isHeading, String text) {
        TextView textView = new TextView(getContext());

        textView.setText(text);
        textView.setTextColor(getResources().getColor(R.color.pinkRoseDark));
        if (isHeading) {
            textView.setText(header);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setId(R.id.tv_header);
        } else {
            textView.setText(subHeader);
            textView.setId(R.id.tv_sub_header);
        }


        return textView;

    }


}
