package com.yumzy.orderfood.ui.common.glidemodule

/*

@GlideModule
class YumGlideModule  constructor() : AppGlideModule() {
    
    lateinit var okHttpClient: OkHttpClient
    override fun applyOptions(context: Context, builder: GlideBuilder) {

        val bitmapPoolSizeBytes = 1024 * 1024 * 10 // 10mb
        val memoryCacheSizeBytes = 1024 * 1024 * 10 // 10mb
        builder.setMemoryCache(LruResourceCache(memoryCacheSizeBytes.toLong()))
        builder.setBitmapPool(LruBitmapPool(bitmapPoolSizeBytes.toLong()))
        builder.setDiskCache(InternalCacheDiskCacheFactory(context, memoryCacheSizeBytes.toLong()))
        builder.setDefaultRequestOptions(requestOptions())
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        val client = OkHttpClient.Builder()
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)
            .build()
        val factory: OkHttpUrlLoader.Factory =
            OkHttpUrlLoader.Factory(client)
        glide.registry.replace(GlideUrl::class.java, InputStream::class.java, factory)
    }

    companion object {
        private fun requestOptions(): RequestOptions {


            return RequestOptions()
                .signature(
                    ObjectKey(
                        System.currentTimeMillis() / (24 * 60 * 60 * 1000)
                    )
                )
                .override(600, 200)
                .centerCrop()
                .encodeFormat(Bitmap.CompressFormat.PNG)
                .encodeQuality(100)
                .placeholder(R.drawable.ic_default_img)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .format(DecodeFormat.PREFER_RGB_565)
                .disallowHardwareConfig()


        }
    }

}
*/


//
//@GlideModule
//class YumGlideModule : AppGlideModule() {
//    //    
////    var client: OkHttpClient? = null
//
//    /*
// override fun applyOptions(
//        context: Context,
//        builder: GlideBuilder
//     ) {
//         super.applyOptions(context, builder)
//         builder.setDefaultRequestOptions(
//             RequestOptions()
//                 .diskCacheStrategy(DiskCacheStrategy.ALL) // cache both original & resized image
//         )
//     }
//
//
//*/
//    override fun applyOptions(context: Context, builder: GlideBuilder) {
////
////        val calculator = MemorySizeCalculator.Builder(context)
////            .setBitmapPoolScreens(2f)
////            .build()
//
//        val bitmapPoolSizeBytes = 1024 * 1024 * 10 // 10mb
//        val memoryCacheSizeBytes = 1024 * 1024 * 10 // 10mb
//
//        builder.setMemoryCache(LruResourceCache(memoryCacheSizeBytes.toLong()))
//        builder.setBitmapPool(LruBitmapPool(bitmapPoolSizeBytes.toLong()))
//
//        builder.setDefaultRequestOptions(
//            RequestOptions()
//                .placeholder(R.drawable.ic_default_img)
//                .skipMemoryCache(true)
//                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                .format(DecodeFormat.PREFER_RGB_565)
//                .disallowHardwareConfig()
//
//        )
//
//
//    }
//
//}
