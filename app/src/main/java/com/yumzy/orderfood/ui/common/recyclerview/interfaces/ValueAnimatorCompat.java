package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

import android.view.View;

public interface ValueAnimatorCompat {
    void setTarget(View var1);

    void addListener(AnimatorListenerCompat var1);

    void setDuration(long var1);

    void start();

    void cancel();

    void addUpdateListener(AnimatorUpdateListenerCompat var1);

    float getAnimatedFraction();
}