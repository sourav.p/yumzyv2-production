package com.yumzy.orderfood.ui.helper

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager


//-------------------------------------------------------------------------------------------------------------
//------------------------------------------ACTIVITY-------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
            /*
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                   val controller = window.insetsController
                   controller?.hide(WindowInsets.Type.statusBars())
               } else {
                   window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                           or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                           or View.SYSTEM_UI_FLAG_IMMERSIVE
                           or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                           or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                           or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)
               }*/
        }
    }
}


fun Activity.makeStatusBarDarkTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}


fun Activity.setLightStatusBar(activity: Activity, color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags =
            activity.window.decorView.systemUiVisibility // get current flag
        flags =
            flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR // add LIGHT_STATUS_BAR to flag
        activity.window.decorView.systemUiVisibility = flags
        activity.window.statusBarColor = color // optional
    }
}

fun Activity.clearLightStatusBar(activity: Activity, color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags =
            activity.window.decorView.systemUiVisibility // get current flag
        flags =
            flags xor View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR // use XOR here for remove LIGHT_STATUS_BAR from flags
        activity.window.decorView.systemUiVisibility = flags
        activity.window.statusBarColor = color// optional
    }
}
