package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.common.VerticalItemDecoration
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setHorizontalListDecoration
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingHomeNewLaunchItem
import com.yumzy.orderfood.util.SnapHelperOneByOne
import io.cabriole.decorator.LinearBoundsMarginDecoration
import io.cabriole.decorator.LinearDividerDecoration

//import me.everything.android.ui.overscroll.OverScrollDecoratorHelper

class NewLaunchedListView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<BindingHomeNewLaunchItem> = FastItemAdapter()

    init {
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<BindingHomeNewLaunchItem>, item: BindingHomeNewLaunchItem, _: Int ->
                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        this.addItemDecoration(VerticalItemDecoration(UIHelper.i10px, false))
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        val linearSpanHelperOneByOne = SnapHelperOneByOne()
        linearSpanHelperOneByOne.attachToRecyclerView(this)
        this.setHorizontalListDecoration()
        /*OverScrollDecoratorHelper.setUpOverScroll(
            this,
            OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL
        )*/

    }
    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setNewLaunchData(
        section: List<HomeListDTO>?
    ) {

        if (section.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE

        val items = ArrayList<BindingHomeNewLaunchItem>()
        section.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingHomeNewLaunchItem(item)
                )
            }
        }

        fastItemAdapter.add(items)


    }

}