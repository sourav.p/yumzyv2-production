package com.yumzy.orderfood.ui.common

import com.yumzy.orderfood.data.models.Variant
import com.yumzy.orderfood.ui.base.IView

interface IModuleHandler : IView {
    fun onSuccess(actionId: Int, data: Any?)

}

interface IOrderCustomizationHandler {
    fun onSuccess(variant: Variant?, addOns: List<Variant>)
    fun onDismiss(dissmissType: Int)
}