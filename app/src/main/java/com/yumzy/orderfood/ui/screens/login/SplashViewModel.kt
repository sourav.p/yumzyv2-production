package com.yumzy.orderfood.ui.screens.login

import com.yumzy.orderfood.ui.base.BaseViewModel
import javax.inject.Inject


class SplashViewModel  constructor() :
    BaseViewModel<ISplashView>() {}