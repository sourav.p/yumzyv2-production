package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * The ViewModel for [PlateFragment].
 */
class PlateViewModel  constructor(val repository: PlateRepository) :
    BaseViewModel<IPlateView>() {
//    var cartDto: MutableLiveData<CartResDTO> = MutableLiveData()
    //    fun getAppUser() = repository.getUserDTO()
    fun preCheckout(preCheckout: PreCheckoutReqDTO) = repository.preCheckout(preCheckout)
    fun applyCoupon(applyCoupon: MutableMap<String, Any>) = repository.applyCoupon(applyCoupon)
    fun getCart(cartId: String) = repository.getCart(cartId)
    fun addToCart(addCart: NewAddItemDTO) = repository.addToCart(addCart)
    fun newOrder(newOrder: NewOrderReqDTO) = repository.newOrder(newOrder)
    fun postCheckout(orderId: String) = repository.postCheckout(orderId)

    fun itemCount(
        position: Int,
        count: Int,
        itemcount: ItemCountDTO
    ) = repository.itemCount(position, count, itemcount)

    fun updateItem(
        position: Int,
        newItem: NewAddItemDTO
    ) = repository.updateItem(position, newItem)

    fun clearCart(cartId: ClearCartReqDTO) = repository.clearCart(cartId)
    fun removeCoupon(removeCouponReqDTO: MutableMap<String, Any>) =
        repository.removeCoupon(removeCouponReqDTO)

    fun getAddressList() = repository.getAddressList()

    fun removeSelectionByItemId(itemId: String) = repository.removeSelectionByItemId(itemId)
    fun getUserWallet(number: Long) =
        repository.getUserWallet(number)

    fun useWallet(cartId: HashMap<String, String>) =
        repository.useWallet(cartId)

    fun unUseWallet(cartId: HashMap<String, String>) =
        repository.unUseWallet(cartId)


}
