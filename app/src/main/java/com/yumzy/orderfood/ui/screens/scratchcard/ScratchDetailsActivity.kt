package com.yumzy.orderfood.ui.screens.scratchcard

import android.R.attr.label
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.ui.pxf
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.databinding.ActivityScratchDetailsBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.widget.scratchcard.ScratchCardLayout
import com.yumzy.orderfood.ui.widget.scratchcard.ScratchCardUtils
import com.yumzy.orderfood.ui.widget.scratchcard.ScratchListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.intentOpenWebsite
import com.yumzy.orderfood.util.intentShareImage
import com.yumzy.orderfood.util.intentShareText
import com.yumzy.orderfood.util.viewutils.YumUtil
import top.defaults.drawabletoolbox.DrawableBuilder


class ScratchDetailsActivity : AppCompatActivity(), View.OnClickListener, ScratchListener {
    private val bd: ActivityScratchDetailsBinding by lazy {
        ActivityScratchDetailsBinding.inflate(
            layoutInflater
        )
    }
    private val referLink: String? by lazy {
        intent.getStringExtra("referLink")
    }
    private val referCode: String? by lazy {
        intent.getStringExtra("refer_code")
    }

    private var startedReveling: Boolean = false
    private var scratchCompleted: Boolean = false

    private val scratchCardDTO: ScratchCardDTO? by lazy {
        intent.getParcelableExtra("scratchCard")
    }

    private val scratchDrawableId: Int? by lazy {
        intent.getIntExtra("scratchDrawableId", R.drawable.img_scratch_0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BarUtils.setStatusBarLightMode(this, false)
        setContentView(bd.root)
        setControlPanelListeners()
        bd.constrainContainer.setOnClickListener {
            closeOrReveal()
        }

        bd.closeIcon.setOnClickListener(this)

        scratchCardDTO?.let {
            scratchCompleted = it.scratched
            bd.tvWonMessage.text = HtmlCompat.fromHtml(
                it.scratchMessage,
                HtmlCompat.FROM_HTML_MODE_COMPACT
            )
            bd.actionButton.text = it.action.text

            when (it.redeemType) {
                IConstants.ScratchCardType.URL_TYPE.redeemType -> {
                    YumUtil.setImageInView(bd.ivIconType, it.image, R.drawable.cookie_coin)
                    bd.actionButton.setOnClickListener {
                        intentOpenWebsite(this, scratchCardDTO?.action?.url ?: "")
                    }
                    if (!it.action.couponCode.isNullOrEmpty()) {

                        bd.tvLabelText.visibility = View.VISIBLE
                        bd.tvLabelText.background = DrawableBuilder()
                            .rectangle()
                            .hairlineBordered()
                            .dashed()
                            .cornerRadius(UIHelper.i5px)
                            .strokeColor(ThemeConstant.alphaMediumGray)
                            .strokeColorPressed(ThemeConstant.textDarkGrayColor)
                            .strokeWidth(5.px)
                            .dashGap(5.px)
                            .ripple().rippleColor(ThemeConstant.lightAlphaGray)
                            .build()
                        bd.tvLabelText.text = it.action.couponCode
                        bd.tvLabelText.setClickScaleEffect()
//                        bd.tvWonMessage.setClickScaleEffect()
                        bd.tvLabelText.setOnClickListener { _ -> copyCoupon(it) }
//                        bd.tvWonMessage.setOnClickListener { _ -> copyCoupon(it) }
                    }
                }
                else -> {
                    bd.actionButton.setOnClickListener { _ ->


                        var shareText = IConstants.AppConstant.SCRATCH_CARD_INVITE_TEXT

                        if (shareText == "") {
                            shareText =
                                resources.getString(R.string.share_with_friend_from_scratch_card)
                        }

                        if (shareText.contains("won_amount")) {
                            shareText = shareText.replace(
                                "won_amount",
                                if (it.amount > 0) it.amount.toString() else "scratch card"
                            )
                        }

                        if (shareText.contains("refer_link")) {
                            shareText = shareText.replace(
                                "refer_link",
                                referLink ?: "app"
                            )
                        }

                        if (shareText.contains("refer_code")) {
                            shareText = shareText.replace(
                                "refer_code",
                                referCode ?: ""
                            )
                        }

                        if (shareText.contains("new_line")) {
                            shareText = shareText.replace("new_line", "\n")
                        }

                        try {
                            val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_whatsapp_illustration)

                            Glide.with(this).asBitmap().load(bitmap)
                                .apply(RequestOptions().override(400, 400))
                                .listener(object : RequestListener<Bitmap> {
                                    override fun onLoadFailed(
                                        e: GlideException?,
                                        model: Any?,
                                        target: Target<Bitmap>?,
                                        isFirstResource: Boolean
                                    ): Boolean {
                                        intentShareText(this@ScratchDetailsActivity, shareText)
                                        return false
                                    }

                                    override fun onResourceReady(
                                        resource: Bitmap?,
                                        model: Any?,
                                        target: Target<Bitmap>?,
                                        dataSource: DataSource?,
                                        isFirstResource: Boolean
                                    ): Boolean {
                                        intentShareImage(
                                            this@ScratchDetailsActivity,
                                            resource,
                                            shareText
                                        )
                                        return true
                                    }
                                }).submit()

                        } catch (e: Exception) {
                            intentShareText(this, shareText)
                        }
                    }
                }
            }

            if (it.scratched) {
                bd.scratchCard.revealScratch()
                bd.actionButton.visibility = View.VISIBLE
                bd.tvEarnBy.text = HtmlCompat.fromHtml(
                    it.afterEarnBy ?: "",
                    HtmlCompat.FROM_HTML_MODE_COMPACT
                )
                bd.tvEarnBy.movementMethod = LinkMovementMethod.getInstance()
            } else {
                bd.tvEarnBy.text = HtmlCompat.fromHtml(
                    it.beforeEarnBy ?: "",
                    HtmlCompat.FROM_HTML_MODE_COMPACT
                )
                bd.scratchCard.setScratchDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        scratchDrawableId ?: R.drawable.img_scratch_0,
                        theme
                    )

                )
                bd.actionButton.visibility = View.GONE
                bd.tvEarnBy.movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun closeOrReveal() {
        if (!startedReveling)
            onBackPressed()
        else {
            bd.scratchCard.revealScratch()
            startedReveling = false
            scratchCompleted = true
        }
    }


    private fun copyCoupon(it: ScratchCardDTO) {
        val clipboard: ClipboardManager =
            getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(label.toString(), it.action.couponCode)
        clipboard.setPrimaryClip(clip)
        showToast("Coupon Copied")
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun setControlPanelListeners() {
        //Scratch Brush size config
        bd.scratchCard.setScratchWidthDip(ScratchCardUtils.dipToPx(this, 25.pxf))
        bd.scratchCard.setRevealFullAtPercent(30)
        bd.scratchCard.setScratchListener(this)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.close_icon -> {
                closeOrReveal()
            }
        }
    }

    override fun onBackPressed() {
        if (scratchCompleted) {
            val resultData = Intent()
            resultData.putExtra("scratchCard", scratchCardDTO)
            setResult(Activity.RESULT_OK, resultData)

        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        if (startedReveling) {
            bd.scratchCard.revealScratch()
        } else
            super.onBackPressed()

    }

    override fun onScratchStarted() {
        if (!scratchCompleted) startedReveling = true
    }

    override fun onScratchProgress(
        scratchCardLayout: ScratchCardLayout,
        atLeastScratchedPercent: Int
    ) {
//        startedReveling = true
    }

    override fun onScratchComplete() {
        startedReveling = false
        scratchCompleted = true


        if (scratchCardDTO?.scratched == false) {
            setResult(Activity.RESULT_OK)
        }
        if (scratchCardDTO?.amount?.toInt() ?: 0 > 0 || scratchCardDTO?.action?.url?.isNotEmpty() == true) {
            bd.actionButton.visibility = View.VISIBLE
            bd.lottiExplode.playAnimation()
            bd.tvEarnBy.text = HtmlCompat.fromHtml(
                scratchCardDTO?.afterEarnBy ?: "",
                HtmlCompat.FROM_HTML_MODE_COMPACT
            )

        }


    }


}