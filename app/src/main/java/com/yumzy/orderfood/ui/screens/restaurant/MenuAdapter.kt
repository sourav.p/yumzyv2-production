package com.yumzy.orderfood.ui.screens.restaurant

/*
class MenuAdapter(
    val context: Context,
    override val vm: RestaurantViewModel,
    var menuList: ArrayList<RestaurantItem>,
    var vegMenuList: ArrayList<RestaurantItem> = menuList.filter { item -> item.isVeg } as ArrayList<RestaurantItem>
) : RecyclerView.Adapter<MenuAdapter.RestaurantItemHolder>() {

    private var showVegEnable: Boolean = false
    private var items: MutableList<RestaurantItem> = menuList.map { item -> item } as MutableList<RestaurantItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantItemHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)
        return RestaurantItemHolder(itemBinding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RestaurantItemHolder, position: Int) =
        holder.bindData(items[position])

    internal fun showVegMenu(showVeg: Boolean) {
        showVegEnable = showVeg

        val diffCallback = if (showVeg) {
            MenuItemDiff(items, vegMenuList)
        } else {
            MenuItemDiff(items, menuList)
        }
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()

        if (showVeg) {
            items.addAll(vegMenuList)
        } else {
            items.addAll(menuList)
        }

        diffResult.dispatchUpdatesTo(this)

    }

    fun updateList(list: ArrayList<RestaurantItem>) {
        menuList = list
        items = list
        if (showVegEnable) {
            vegMenuList = menuList.filter { item -> item.isVeg } as ArrayList<RestaurantItem>
            items = vegMenuList
        }
        notifyDataSetChanged()
    }

    inner class RestaurantItemHolder(itemBinding: AdapterRestaurentMenuBinding) :
        YItemViewHolder(itemBinding.root) {

        var adapterBinding: AdapterRestaurentMenuBinding = itemBinding

        fun bindData(item: RestaurantItem?) {

            adapterBinding.addItemView.setItemPrice(item?.price.toString())
            item?.name?.let { adapterBinding.addItemView.setTitles(it.capitalize()) }
            item?.description?.let { adapterBinding.addItemView.setSubtitle(it) }
            adapterBinding.addItemView.itemId = item?.itemId.toString()
            adapterBinding.addItemView.setImageShow(false)
            adapterBinding.addItemView.ribbonText = item?.ribbon
            adapterBinding.addItemView.isVeg = item?.isVeg!!
            adapterBinding.addItemView.showCustomize = !item.addons.isEmpty()
            if (item.prevQuantity > 0) adapterBinding.addItemView.getQuantityView()?.quantityCount =
                item.prevQuantity
            adapterBinding.addItemView.addItenListener = object : AddItemView.AddItemListener {
                override fun onItemAdded(
                    itemId: String,
                    count: Int,
                    priced: String,
                    outletId: String
                ) {
                    val restaurantItem =
                        if (showVegEnable) {
                            vegMenuList[adapterPosition]

                        } else {
                            menuList[adapterPosition]
                        }

                    vm.addItemToCart(
                        itemView.context as LifecycleOwner,
                        count,
                        restaurantItem,
                        adapterBinding.addItemView.getQuantityView(),
                        object : ICartHandler {
                            override fun itemAdded(count: Int) {
                                items[adapterPosition].prevQuantity = count
                            }

                            override fun itemRepeated() {

                            }

                            override fun failedToAdd(count: Int, view: AddQuantityView) {
                                adapterBinding.addItemView.setQuantity(items[adapterPosition].prevQuantity)
                            }
                        }
                    )
                }

            }
        }
    }

}
*/
