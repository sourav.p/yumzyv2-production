package com.yumzy.orderfood.ui.screens.restaurant

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter.Companion.items
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.data.models.RestaurantMenuSectionDTO
import com.yumzy.orderfood.databinding.ActivityOutletSearchBinding
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.common.recyclermargin.LayoutMarginDecoration
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingRegularMenuItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.OutletInteraction
import com.yumzy.orderfood.util.IConstants
import io.cabriole.decorator.LinearDividerDecoration
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject
import kotlin.collections.ArrayList

class OutletSearchActivity : BaseActivity<ActivityOutletSearchBinding, RestaurantViewModel>(),
    IRestaurantSearch, OutletInteraction, OutletSearchView.OnQueryTextListener,
    AddItemView.AddItemListener, ItemFilterListener<BindingRegularMenuItem> {
    
    val cartHelper: CartHelper  by inject()

//    private var restaurantItem: ArrayList<RestaurantItem> = ArrayList()

    //save our FastAdapter
    private lateinit var mFastAdapter: FastAdapter<BindingRegularMenuItem>
    private lateinit var itemAdapter: ItemAdapter<BindingRegularMenuItem>

    private var selectedItems: ArrayList<ValuePairSI> = ArrayList()

    var isFirstLoad = true

    var totalMenuSize = -1

    var isFilterEnabled = false

    private val itemPosition = AtomicInteger(0)

    override val enableEdgeToEdge: Boolean = true

    override val navigationPadding: Boolean = true
    
    override val vm: RestaurantViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (intent.hasExtra("filter_enabled")) {
            isFilterEnabled = intent.getBooleanExtra("filter_enabled", false)
        }

        this.bindView(R.layout.activity_outlet_search)
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        itemAdapter = items()
        mFastAdapter = FastAdapter.with(itemAdapter)

        itemAdapter.itemFilter.filterPredicate =
            { item: BindingRegularMenuItem, constraint: CharSequence? ->
                (item.restaurantItem?.name?.toLowerCase(Locale.getDefault())
                    ?.contains(constraint.toString().toLowerCase(Locale.getDefault())) ?: false ||
                        item.restaurantItem?.description?.toLowerCase(Locale.getDefault())
                            ?.contains(
                                constraint.toString().toLowerCase(Locale.getDefault())
                            ) ?: false)
            }

        itemAdapter.itemFilter.itemFilterListener = this

        setupSearchView()
        setupRecyclerView()
        subScribeSelectedItems()
        bd.rvRestaurantItems.addItemDecoration(LayoutMarginDecoration(1, UIHelper.i5px))
        bd.rvRestaurantItems.layoutManager = LinearLayoutManagerWrapper(this)
        bd.rvRestaurantItems.itemAnimator = DefaultItemAnimator()
        bd.rvRestaurantItems.adapter = mFastAdapter
    }

    private fun setupSearchView() {
        bd.searchView.showSearch(false)
        bd.searchView.setOnQueryTextListener(this)
    }

    private fun setupRecyclerView() {
        val decoration = LinearDividerDecoration.create(
            color = ThemeConstant.whiteSmoke,
            size = 1.px,
            leftMargin = 0.px,
            topMargin = 20.px,
            rightMargin = 0.px,
            bottomMargin = 20.px,
            orientation = RecyclerView.VERTICAL
        )
        bd.rvRestaurantItems.addItemDecoration(decoration)
        vm.getRestaurantDatabase()?.observe(this, { restaurant ->
            if (restaurant != null) {

                val menuSections = restaurant.menuSectionDTOS!!.toMutableList()

                var commonMenuListDTO: RestaurantMenuSectionDTO? = null
                for ((index, section) in menuSections.withIndex()) {
                    if (section.type == "menu") {
                        commonMenuListDTO = section
                        menuSections.removeAt(index)
                        break
                    }
                }

                menuSections.clear()
                menuSections.addAll(getMenuSections(commonMenuListDTO))

                vm.addonsMap = restaurant.menuAddons

                setItems(menuSections)

            }
        })
    }

    private fun setItems(menuSectionDTOS: MutableList<RestaurantMenuSectionDTO>) {
        val items = ArrayList<BindingRegularMenuItem>()
        for ((index, section) in menuSectionDTOS.withIndex()) {

            for ((itemIndex, item) in section.list?.withIndex() ?: emptyList()) {

                if (isFilterEnabled && !item.isVeg) continue
                if (selectedItems.isNotEmpty()) {
                    val pair =
                        selectedItems.find { selectedItem -> selectedItem.id == item.itemId }
                    if (pair != null) {
                        item.prevQuantity = pair.value
                    }
                }

                val simpleItem = BindingRegularMenuItem(this).withRestaurantAddItem(item)
                items.add(simpleItem)

                var positionList = vm.menuItemPosition[item.itemId]
                if (positionList.isNullOrEmpty()) {
                    positionList = arrayListOf(itemPosition.getAndIncrement())
                } else {
                    positionList.add(itemPosition.getAndIncrement())
                }
                vm.menuItemPosition[item.itemId] = positionList
            }
        }

        bd.itemCount = items.size
        totalMenuSize = items.size
        itemAdapter.add(items)
    }

    private fun subScribeSelectedItems() {

        vm.getSelectedItem()?.observeForever { it ->
            val deSelectedItems = arrayListOf<ValuePairSI>()

            if (selectedItems.isNotEmpty()) {

                selectedItems.forEach { previousItem ->
                    val item = it.find { item -> item.id == previousItem.id }
                    if (item == null) {
                        deSelectedItems.add(ValuePairSI(previousItem.id, 0))
                    }
                }

                if (deSelectedItems.isNotEmpty()) {
                    updateMenuItemSelection(deSelectedItems)
                    deSelectedItems.clear()
                }

            }


            selectedItems.clear()
            selectedItems.addAll(it)
            updateMenuItemSelection(selectedItems)
        }

    }

    private fun updateMenuItemSelection(it: List<ValuePairSI>) {

        it.forEach { selectedItem ->

            //get item position from the list as per id

            val positionList = arrayListOf<Int>()

            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val holder = mFastAdapter.getItem(i)
                when (holder) {
                    is BindingRegularMenuItem -> {
                        if (holder.restaurantItem?.itemId == selectedItem.id) {
                            positionList.add(mFastAdapter.getPosition(holder))
                            break@loop
                        }
                    }
                }
            }

            if (!positionList.isNullOrEmpty()) {

                //get item from fast adapter as per each position
                positionList.forEach { pos ->

                    val item = mFastAdapter.getItem(pos)

                    //find item object type and update it's value
                    when (item) {
                        is BindingRegularMenuItem -> {
                            item.restaurantItem?.prevQuantity = selectedItem.value
                            //                                    Handler(Looper.getMainLooper()).postDelayed({mFastAdapter.notifyAdapterItemChanged(pos)}, 300)
                            mFastAdapter.notifyAdapterItemChanged(pos)
                        }
                    }
                }
            }

        }
    }


    private fun onSearchItem(searchText: String) {
        itemAdapter.filter(searchText)
    }

    private fun getMenuSections(menuSectionDTO: RestaurantMenuSectionDTO?): Collection<RestaurantMenuSectionDTO> {
        val sections = mutableListOf<RestaurantMenuSectionDTO>()
        val list = menuSectionDTO?.list
        var i = 0
        if (!list.isNullOrEmpty())
            while (i < list.size) {
                if (list[i].type == "category") {
                    val section = RestaurantMenuSectionDTO()
                    section.type = "menu"
                    section.name = list[i].name.capitalize()
                    section.layoutType = 12
                    section.list = ArrayList()
                    val sectionList = ArrayList(section.list ?: listOf())
                    for (j in i + 1 until list.size) {
                        if (list[j].type == "item") {
                            sectionList.add(list[j])
                        } else if (list[j].type == "subCategory") {
                            continue
                        } else if (list[j].type == "category") {
                            i = j - 2
                            break
                        }
                    }
                    if (sectionList != null) {
                        section.list = sectionList
                        sections.add(section)
                    }
                }
                i++
            }

        return sections
    }

    private fun getItemListFromSection(menuSectionDTOS: MutableList<RestaurantMenuSectionDTO>): Collection<RestaurantItem> {
        val list = ArrayList<RestaurantItem>()
        menuSectionDTOS.forEach { section ->
            section.list?.forEach { item -> list.add(item) }
        }
        return list
    }

    override fun onQueryTextSubmit(query: String): Boolean {
//        getItemsFromDb(query)
        itemAdapter.filter(query)
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
//        getItemsFromDb(newText)
        itemAdapter.filter(newText)
        return true
    }

    override fun onQueryTextCleared(): Boolean {
//        getItemsFromDb("")
        itemAdapter.filter("")
        return true

    }

    override fun onItemSelected(position: Int, item: RestaurantItem) {}

    override fun onItemCountChange(
        cartHandler: ICartHandler,
        count: Int,
        item: RestaurantItem,
        quantityView: AddQuantityView,
        isIncremented: Boolean?
    ) {
        cartHelper.addOrUpdateItemToCart(
            cartHandler,
            count,
            item,
            vm.addonsMap,
            isIncremented
        )
    }

    override fun onItemFailed(message: String) {}

    override fun adapterSizeChange(itemSize: Int) {

        bd.tvItemCount.text = "Dishes ($itemSize)"
    }

    override fun onNoteClicked(item: RestaurantItem) {}

    override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {
        showToast("$count")
    }

    override fun itemsFiltered(
        constraint: CharSequence?,
        results: List<BindingRegularMenuItem>?
    ) {
        bd.itemCount = results?.size
//        CleverTapHelper.menuSearch(results)
    }

    private fun gotoPlateFragment() {
        val returnIntent = Intent()
        returnIntent.putExtra(
            IConstants.ActivitiesFlags.SHOW_PLATE,
            true
        )
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun gotoPlate() {
        gotoPlateFragment()
    }

    override fun onReset() {
        //gets called when search feild is cleared
        bd.itemCount = totalMenuSize
    }


}
