/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.android.SphericalUtil
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.ThreadUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Coordinate
import com.yumzy.orderfood.data.models.OrderStatusDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import com.yumzy.orderfood.data.models.orderdetails.Outlet
import com.yumzy.orderfood.databinding.DialogOrderTrackingBinding
import com.yumzy.orderfood.databinding.ItemOrderItemBinding
import com.yumzy.orderfood.databinding.ItemOrderOutletBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.component.TimelineHeaderView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardActivity
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.maputils.LatLngInterpolator
import com.yumzy.orderfood.util.maputils.MarkerAnimation
import com.yumzy.orderfood.util.maputils.PathParser
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.zoho.livechat.android.ZohoLiveChat
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import top.defaults.drawabletoolbox.DrawableBuilder
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class OrderTrackingDialog : BaseDialogFragment<DialogOrderTrackingBinding>(),
    OnMapReadyCallback, IOrderTracking {

    private var mAddedMarkerSnippet: Boolean = false

    private lateinit var mMap: GoogleMap

    private var mSourceMarker: Marker? = null

    private var mDestinationMarker: Marker? = null

    private var mRiderMarker: Marker? = null

    private var mRiderToUserPath: Polyline? = null

    private var mOutletToUserArch: Polyline? = null

    private var mOutletToUserStraightLine: Polyline? = null

    private var mIsPathDrawn = false

    private lateinit var mainHandler: Handler

    private val task = Runnable { getOrderStatus() }

    val vm: OrderTrackingDialogViewModel by viewModel()

    internal var mOrderId: String = ""

    override fun getLayoutId(): Int = R.layout.dialog_order_tracking

    private var isInitialLoad = true

    private var mOutletName = "Outlet"

    private var mUserName = "Your Location"

    override fun onDialogReady(view: View) {

//        CleverTapHelper.pushEvent(CTEvents.order_tracking)

        bd.includeOrderTracking.tvSpcInc.background = DrawableBuilder()
            .rectangle()
            .hairlineBordered()
            .longDashed()
            .cornerRadius(UIHelper.i5px)
            .strokeColor(ThemeConstant.mediumGray)
            .strokeColorPressed(ThemeConstant.mediumGray)
            .strokeWidth(UIHelper.i1px)
            .dashGap(UIHelper.i5px)
            .ripple()
            .build()

        /* (activity as AppCompatActivity).run {
             setSupportActionBar(bd.tbTrackOrder)
             supportActionBar?.setDisplayHomeAsUpEnabled(true)
             supportActionBar?.setHomeButtonEnabled(true)
         }
 */

        bd.lifecycleOwner = this
        mainHandler = Handler(Looper.getMainLooper())
        applyDebouchingClickListener(
            bd.includeOrderTracking.btnCallRider,
            bd.includeOrderTracking.btnCallRider1,
            bd.tvBack,
            bd.tvHelp,
            bd.includeOrderTracking.scratchCardLayout
        )
        setupMap()
        setupBottomSheet()
        subscribe()
    }

    override fun onSuccess(actionId: Int, data: Any?) {}

    override fun onMapReady(googleMap: GoogleMap?) {

        MapsInitializer.initialize(context)

        mMap = googleMap ?: return

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    context, R.raw.google_maps_style
                )
            )
            if (!success) {
                Log.e("order tracking", "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e("order tracking", "Can't find style. Error: ", e)
        }
/*

        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker?): View {

                val info = LinearLayout(context)
                info.setOrientation(LinearLayout.VERTICAL)
                val title = TextView(context)
                title.setTextColor(ThemeConstant.textBlackColor)
                title.gravity = Gravity.CENTER
                title.setInterRegularFont()
                title.text = marker?.title
                info.addView(title)

                return info
            }

            override fun getInfoWindow(p0: Marker?): View? {
                return null
            }
        })
*/

        hideProgressBar()
    }

    private fun setupMap() {
        //setup map
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        showProgressBar()
    }

    private fun updateMap(orderStatus: OrderStatusDTO) {

        orderStatus.outletLocation?.let { setSourceMarker(it) }

        orderStatus.userLocation?.let { setDestinationMarker(it) }

        orderStatus.delivery?.riderInfo?.location?.let { setRiderMarker(it) }

        updateTimelineAndMap(orderStatus)

        boundMapView(orderStatus)

        /*      if (!mAddedMarkerSnippet) {
                  mSourceMarker?.showInfoWindow()
      //            mDestinationMarker?.showInfoWindow()
                  mAddedMarkerSnippet = true
              }*/

    }

    private fun setSourceMarker(outletLocation: Coordinate) {
        //set marker only once
        if (mSourceMarker == null) {
            mSourceMarker = mMap.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            outletLocation.lat,
                            outletLocation.lng
                        )
                    )
                    .icon(
                        bitmapDescriptorFromVector(
                            context ?: requireContext(),
                            R.drawable.ic_outlet_pin
                        )
                    )
                    .title(mOutletName)
                    .snippet("Restaurant")
                    .draggable(false)
            )
        }
    }

    private fun setDestinationMarker(userLocation: Coordinate) {
        //set marker only once
        if (mDestinationMarker == null) {
            mDestinationMarker = mMap.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            userLocation.lat,
                            userLocation.lng
                        )
                    )
                    .icon(
                        bitmapDescriptorFromVector(
                            context ?: requireContext(),
                            R.drawable.ic_my_location_pin
                        )
                    )
                    .title(mUserName)
                    .snippet("You")
                    .draggable(false)
            )
        }
    }

    private fun setRiderMarker(riderLocation: Coordinate) {
        //set marker only once
        //change the rider icon
        if (mRiderMarker == null) {
            mRiderMarker = mMap.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            riderLocation.lat,
                            riderLocation.lng
                        )
                    )
                    .icon(
                        bitmapDescriptorFromVector(
                            context ?: requireContext(),
                            R.drawable.ic_rider
                        )
                    )
                    .snippet("Rider")
                    .draggable(false)
            )
        }

    }

    private fun boundMapView(
        orderStatus: OrderStatusDTO
    ) {

        //this helps to show all marker on screen at once.
        val boundsBuilder = LatLngBounds.Builder()

        when (orderStatus.orderStatus) {
            IConstants.OrderTracking.ORDER_STATUS_ACCEPTANCE_WAIT,
            IConstants.OrderTracking.ORDER_STATUS_VALET_ON_WAY_TO_CONFIRM -> {
                boundsBuilder.include(mSourceMarker?.position)
                boundsBuilder.include(mDestinationMarker?.position)
            }

            IConstants.OrderTracking.ORDER_STATUS_FOOD_PREPARATION,
            IConstants.OrderTracking.ORDER_STATUS_FOOD_READY,
            IConstants.OrderTracking.ORDER_STATUS_WAITIN_DELIVERY_PICKUP -> {
                //draw arc and update riderlocation with it facing to the destination

                val location = orderStatus.delivery?.riderInfo?.location
                if (location != null) {
                    boundsBuilder.include(mSourceMarker?.position)
                    boundsBuilder.include(mRiderMarker?.position)
                } else {
                    boundsBuilder.include(mSourceMarker?.position)
                    boundsBuilder.include(mDestinationMarker?.position)
                }
            }

            IConstants.OrderTracking.ORDER_STATUS_OUT_FOR_DELIVERY -> {

                boundsBuilder.include(mRiderMarker?.position)
                boundsBuilder.include(mDestinationMarker?.position)

            }

            IConstants.OrderTracking.ORDER_STATUS_DELIVERED,
            IConstants.OrderTracking.ORDER_STATUS_COMPLETE -> {
                boundsBuilder.include(mSourceMarker?.position)
                boundsBuilder.include(mDestinationMarker?.position)
            }

            else -> {
                boundsBuilder.include(mSourceMarker?.position)
                boundsBuilder.include(mDestinationMarker?.position)
            }
        }
/*
        boundsBuilder.include(mDestinationMarker?.position)
        mRiderMarker?.let { boundsBuilder.include(it.position) }*/
        val cu = CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), VUtil.dpToPx(60))
        val point = CameraUpdateFactory.newLatLng(mDestinationMarker?.position)

// moves camera to coordinates
        if (isInitialLoad) {
            mMap.moveCamera(point)
            isInitialLoad = false
        }
        mMap.animateCamera(cu)
    }

    private fun updateTimelineAndMap(orderStatus: OrderStatusDTO) {
        when (orderStatus.orderStatus) {
            IConstants.OrderTracking.ORDER_STATUS_ACCEPTANCE_WAIT,
            IConstants.OrderTracking.ORDER_STATUS_VALET_ON_WAY_TO_CONFIRM -> {
                //draw arc
                drawArchFromOutletToUser()
                //update UI
                setUpTimeLine("first", orderStatus.text)
                setArrivalTime(orderStatus)
            }

            IConstants.OrderTracking.ORDER_STATUS_FOOD_PREPARATION,
            IConstants.OrderTracking.ORDER_STATUS_FOOD_READY,
            IConstants.OrderTracking.ORDER_STATUS_WAITIN_DELIVERY_PICKUP -> {
                //draw arc and update riderlocation with it facing to the destination

                val location = orderStatus.delivery?.riderInfo?.location
                if (location != null) {
                    mOutletToUserArch?.remove()
                    mOutletToUserStraightLine?.remove()
                } else {
                    drawArchFromOutletToUser()
                }
                updateRiderLocation(
                    location ?: Coordinate(),
                    orderStatus.outletLocation ?: Coordinate()
                )

                //update ui
                setUpTimeLine("second", orderStatus.text)
                setArrivalTime(orderStatus)
            }

            IConstants.OrderTracking.ORDER_STATUS_OUT_FOR_DELIVERY -> {
                mOutletToUserArch?.remove()
                mOutletToUserStraightLine?.remove()
                updateRiderLocation(
                    orderStatus.delivery?.riderInfo?.location ?: Coordinate(),
                    orderStatus.userLocation ?: Coordinate()
                )
//                drawRiderToUserPath(orderStatus)
                //update ui
//                mSourceMarker?.remove()
                setUpTimeLine("third", orderStatus.text)
                setArrivalTime(orderStatus)
            }

            IConstants.OrderTracking.ORDER_STATUS_DELIVERED,
            IConstants.OrderTracking.ORDER_STATUS_COMPLETE -> {
//                mSourceMarker?.remove()
//                CleverTapHelper.pushEvent(CleverEvents.order_completed)
                mRiderMarker?.remove()
                setUpTimeLine("fourth")
                setArrivalTime(orderStatus)
            }
            IConstants.OrderTracking.ORDER_STATUS_IN_DISPUTE -> {
//                CleverTapHelper.pushEvent(CleverEvents.order_deliveryfailed)
                setOrderIssueState(orderStatus.text)
            }
            IConstants.OrderTracking.ORDER_STATUS_IN_PAYMENT,
            IConstants.OrderTracking.ORDER_STATUS_CANCELLED,
            IConstants.OrderTracking.ORDER_STATUS_REJECTED -> {
//                CleverTapHelper.pushEvent(CleverEvents.order_restorejected)
                setOrderIssueState(orderStatus.text)

            }
            IConstants.OrderTracking.ORDER_STATUS_ENFORCED_CANCEL -> {
                //remove path from map
//                CleverTapHelper.pushEvent(CleverEvents.order_cancelled)
                setOrderIssueState(orderStatus.text)

            }


        }

    }

    private fun setArrivalTime(orderStatus: OrderStatusDTO) {
        val withPrecision = orderStatus.delivery?.eta?.dropOff?.toInt()
        bd.eta = if (withPrecision == 0) "" else withPrecision.toString()
    }

    private fun drawArchFromOutletToUser() {
        if (mOutletToUserArch == null) {
            mOutletToUserArch = mMap.addPolyline(
                getCurveLine(
                    mSourceMarker?.position ?: LatLng(0.0, 0.0),
                    mDestinationMarker?.position ?: LatLng(0.0, 0.0),
                    0.2,
                    ThemeConstant.pinkies
                )
            )
            mOutletToUserStraightLine = mMap.addPolyline(
                getStraightLine(
                    mSourceMarker?.position ?: LatLng(0.0, 0.0),
                    mDestinationMarker?.position ?: LatLng(0.0, 0.0),
                    ThemeConstant.alphaMediumGray
                )
            )
        } else {

        }
    }

    private fun getStraightLine(
        p1: LatLng,
        p2: LatLng,
        color: Int
    ): PolylineOptions? {

        val option = PolylineOptions()
        option.add(p1, p2)
        option.width(8F)
        option.color(color)

        val pattern: List<PatternItem> = listOf(Dot(), Gap(10F))

        option.pattern(pattern)

        return option
    }

    private fun getCurveLine(
        latLag1: LatLng,
        latLng2: LatLng,
        k: Double = 0.5,
        color: Int
    ): PolylineOptions? {

        var h = SphericalUtil.computeHeading(latLag1, latLng2)
        var d: Double
        val p: LatLng?

        //The if..else block is for swapping the heading, offset and distance 
        //to draw curve always in the upward direction
        if (h < 0) {
            d = SphericalUtil.computeDistanceBetween(latLng2, latLag1)
            h = SphericalUtil.computeHeading(latLng2, latLag1)
            //Midpoint position
            p = SphericalUtil.computeOffset(latLng2, d * 0.5, h)
        } else {
            d = SphericalUtil.computeDistanceBetween(latLag1, latLng2)

            //Midpoint position
            p = SphericalUtil.computeOffset(latLag1, d * 0.5, h)
        }

        //Apply some mathematics to calculate position of the circle center
        val x: Double = (1 - k * k) * d * 0.5 / (2 * k)
        val r: Double = (1 + k * k) * d * 0.5 / (2 * k)

        val c = SphericalUtil.computeOffset(p, x, h + 90.0)


        //Polyline options
        val options = PolylineOptions()
        val pattern: List<PatternItem> = listOf(Dot(), Gap(10F))


        //Calculate heading between circle center and two points
        val h1 = SphericalUtil.computeHeading(c, latLag1)
        val h2 = SphericalUtil.computeHeading(c, latLng2)


        //Calculate positions of points on circle border and add them to polyline options
        val numpoints = 100
        val step = (h2 - h1) / numpoints

        for (i in 0 until numpoints) {
            val pi = SphericalUtil.computeOffset(c, r, h1 + i * step)
            options.add(pi)
        }


        return options.width(9F).color(color).geodesic(false).pattern(pattern).zIndex(100f)
    }

    private fun updateRiderLocation(
        riderLocation: Coordinate,
        userLocation: Coordinate
    ) {
        if (riderLocation.lat != 0.0 && riderLocation.lng != 0.0) {

            mRiderMarker?.rotation = PathParser.bearingBetweenLocations(
                LatLng(riderLocation.lat, riderLocation.lng),
                LatLng(
                    mRiderMarker?.position?.latitude ?: userLocation.lat,
                    mRiderMarker?.position?.longitude ?: userLocation.lng
                )
            )

            mRiderMarker?.let {
                MarkerAnimation.animateMarkerToGB(
                    it,
                    LatLng(riderLocation.lat, riderLocation.lng),
                    LatLngInterpolator.Linear()
                )
            }
//            mRiderMarker?.position = LatLng(riderLocation.lat, riderLocation.lng)

        }
    }

    private fun drawRiderToUserPath(orderStatus: OrderStatusDTO) {

        //get the path by rider and user location
        doAsync {
            val pathResult = URL(
                """https://maps.googleapis.com/maps/api/directions/json?
                |origin=${orderStatus.delivery?.riderInfo?.location?.lat},${orderStatus.delivery?.riderInfo?.location?.lng}
                |&destination=${orderStatus.userLocation?.lat},${orderStatus.userLocation?.lng}
                |&sensor=false
                |&key=AIzaSyAhSi820V_loyJU6Y6cQeOz3CSduJ7-xzs""".trimMargin()
            ).readText()
            val paths = PathParser.parse(JSONObject(pathResult))

            var points: ArrayList<LatLng?>
            var lineOptions: PolylineOptions? = null

            // Traversing through all the paths
            for (i in 0 until paths?.size!!) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                // Fetching i-th route
                val path: List<HashMap<String, String>> = paths[i]

                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    val position = LatLng(lat, lng)
                    points.add(position)
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(8f)
                lineOptions.color(ThemeConstant.pinkies)
            }

            uiThread {
                // Drawing polyline in the Google Map for the i-th route
                if (mRiderToUserPath != null) {
                    mRiderToUserPath?.remove()
                }
                if (lineOptions != null) {
                    mRiderToUserPath = mMap.addPolyline(lineOptions)
                }
            }
        }

    }

    private fun setUpTimeLine(section: String, sectionTitle: String = "") {
        when (section) {
            "first" -> {
                bd.section = "first"
                bd.includeOrderTracking.timelineOrderReceived.setState(
                    TimelineHeaderView.TimelineState.Ongoing,
                    sectionTitle
                )
                bd.includeOrderTracking.timelineOrderPreparing.setState(
                    TimelineHeaderView.TimelineState.Upcoming,
                    "Food is being prepared"
                )
                bd.includeOrderTracking.timelinePickedUp.setState(
                    TimelineHeaderView.TimelineState.Upcoming,
                    "Order Picked Up"
                )
            }
            "second" -> {
                bd.section = "second"

                bd.includeOrderTracking.timelineOrderReceived.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Order Received"
                )
                bd.includeOrderTracking.timelineOrderPreparing.setState(
                    TimelineHeaderView.TimelineState.Ongoing,
                    sectionTitle
                )
                bd.includeOrderTracking.timelinePickedUp.setState(
                    TimelineHeaderView.TimelineState.Upcoming,
                    "Order Picked Up"
                )
            }
            "third" -> {
                bd.section = "third"

                bd.includeOrderTracking.timelineOrderReceived.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Order Received"
                )
                bd.includeOrderTracking.timelineOrderPreparing.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Food Ready"
                )
                bd.includeOrderTracking.timelinePickedUp.setState(
                    TimelineHeaderView.TimelineState.Ongoing,
                    sectionTitle
                )
            }
            else -> {
                bd.eta = ""
                bd.section = "fourth"
                bd.includeOrderTracking.timelineOrderReceived.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Order Received"
                )
                bd.includeOrderTracking.timelineOrderPreparing.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Food Ready"
                )
                bd.includeOrderTracking.timelinePickedUp.setState(
                    TimelineHeaderView.TimelineState.Completed,
                    "Order Delivered"
                )
            }
        }
    }

    private fun setupBottomSheet() {
        val behaviour = BottomSheetBehavior.from(bd.nestedOrderTracking)
        behaviour.setPeekHeight(VUtil.dpToPx(250), true)
        behaviour.isHideable = false

        bd.viewmodel = vm
        bd.lifecycleOwner = this
        bd.section = ""
        val layoutParams =
            CoordinatorLayout.LayoutParams(
                ViewConst.MATCH_PARENT,
                VUtil.screenHeight - VUtil.dpToPx(150)
            )

        bd.map.layoutParams = layoutParams
    }

    var repatTask: ThreadUtils.Task<Any>? = null

    private fun subscribe() {

        repatTask = object : ThreadUtils.Task<Any>() {
            override fun doInBackground(): Any? {
                return OrderStatusDTO()
            }

            override fun onSuccess(orderStatus: Any) {
                getOrderStatus()
            }

            override fun onFail(p0: Throwable?) {
                showSnackBar(p0?.message.toString(), -1)
            }

            override fun onCancel() {}
        }

        ThreadUtils.executeByCachedAtFixRate(
            repatTask,
            IConstants.OrderTracking.ORDER_TRACKING_DURATION,
            TimeUnit.MILLISECONDS
        )

        vm.orderTrackDetails(mOrderId)
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                if (it.type == -1) return@Observer
                RHandler<NewOrderDetailsDTO>(
                    this,
                    it
                ) { orderDetails ->
                    bd.includeOrderTracking.timelineContainer.visibility = View.VISIBLE

                    bd.includeOrderTracking.fulladdress =
                        orderDetails.deliveryAddress?.address?.fullAddress
                    bd.includeOrderTracking.ordernum = orderDetails.orderNum
                    orderDetails.outlets.forEach { outlet ->
                        setUpPriceAndOutlet(outlet, orderDetails)
                    }
                    if (orderDetails.earnedScratch) {
                        bd.includeOrderTracking.scratchCardLayout.visibility = View.VISIBLE
                    } else {
                        bd.includeOrderTracking.scratchCardLayout.visibility = View.GONE

                    }

                    vm.orderStatusDesctription.value = orderDetails.statusDesc

                    orderDetails.deliveryAddress?.address?.longLat?.let {
                        setDestinationMarker(Coordinate().apply {
                            lat = it.coordinates[1]
                            lng = it.coordinates[0]
                        })
                    }

//                    val cu =
//                        CameraUpdateFactory.newLatLngZoom(mDestinationMarker?.position, 15F)
//
//                    if (mMap != null) {
//                        mMap.animateCamera(cu)
//                    }

                    when (orderDetails.orderStatus) {
                        IConstants.OrderTracking.ORDER_STATUS_ACCEPTANCE_WAIT,
                        IConstants.OrderTracking.ORDER_STATUS_VALET_ON_WAY_TO_CONFIRM -> {
                            //update UI
                            setUpTimeLine("first", orderDetails.statusText ?: "Order Received")
                            pendingOrderToggel(orderDetails.statusText.toString(), false)
                        }

                        IConstants.OrderTracking.ORDER_STATUS_FOOD_PREPARATION,
                        IConstants.OrderTracking.ORDER_STATUS_FOOD_READY,
                        IConstants.OrderTracking.ORDER_STATUS_WAITIN_DELIVERY_PICKUP -> {
                            //update ui
                            setUpTimeLine(
                                "second",
                                orderDetails.statusText ?: "Food Being Prepared"
                            )
                        }

                        IConstants.OrderTracking.ORDER_STATUS_OUT_FOR_DELIVERY -> {
                            //update ui
                            setUpTimeLine("third", orderDetails.statusText ?: "Out For Delivery")
                        }

                        IConstants.OrderTracking.ORDER_STATUS_DELIVERED,
                        IConstants.OrderTracking.ORDER_STATUS_COMPLETE -> {
                            mMap.clear()
                            setUpTimeLine("fourth")
                            bd.eta = ""

                            removeHandlers()
                        }

                        IConstants.OrderTracking.ORDER_STATUS_IN_PAYMENT,
                        IConstants.OrderTracking.ORDER_STATUS_CANCELLED,
                        IConstants.OrderTracking.ORDER_STATUS_REJECTED,
                        IConstants.OrderTracking.ORDER_STATUS_IN_DISPUTE,
                        IConstants.OrderTracking.ORDER_STATUS_ENFORCED_CANCEL -> {
                            setOrderIssueState(orderDetails.statusText)
                        }

                    }

                    orderDetails.outlets.let { outlets ->
                        if (!outlets.isNullOrEmpty()) {
                            mOutletName = outlets[0].outletName
                        }
                    }

                    getOrderStatus()

                }
            })

    }

    private fun setOrderIssueState(statusText: String?) {
        bd.eta = ""
        mMap.clear()
        pendingOrderToggel(statusText ?: "Something Happened")

        removeHandlers()
    }

    private fun getOrderStatus() {
        if (view != null)
            vm.getOrderStatus(mOrderId).observe(viewLifecycleOwner, {
                if (it.type == -1) return@observe
                RHandler<OrderStatusDTO>(
                    this@OrderTrackingDialog,
                    it
                ) { orderStatus ->

                    vm.orderStatus.value = orderStatus
                    vm.orderStatusDesctription.value = orderStatus.description
                    vm.orderRiderInfo.value = orderStatus.delivery?.riderInfo
                    updateMap(orderStatus)
                }
            })
    }

    private fun setUpPriceAndOutlet(
        outlet: Outlet,
        orderDetails: NewOrderDetailsDTO
    ) {
//        bd.tbTrackOrder.title = "Order #${outlet.orderNum}"
        if (!orderDetails.specialInstructions.isNullOrEmpty()) {
            bd.includeOrderTracking.tvSpcInc.visibility = View.VISIBLE
            bd.includeOrderTracking.tvSpcInc.text =
                orderDetails.specialInstructions.toString().capitalize()

        } else {
            bd.includeOrderTracking.tvSpcInc.visibility = View.GONE
        }
        bd.includeOrderTracking.containerOrderOutlet.apply {
            //add outlet
            this.addView(
                ItemOrderOutletBinding.inflate(
                    LayoutInflater.from(
                        requireContext()
                    )
                ).apply {
                    //add outlet item
                    this.outlet = outlet
                    this.billing = orderDetails.billing
                    if (!YumUtil.setPaidVia(orderDetails.payments).equals("")) {
                        this.paymentType = "Paid via " + YumUtil.setPaidVia(orderDetails.payments)
                    } else {
                        this.paymentType = ""

                    }

                    outlet.items.forEach { item ->
                        this.tvOutletItemContainer.addView(
                            ItemOrderItemBinding.inflate(
                                LayoutInflater.from(requireContext())
                            ).apply {
                                this.item = item.let {
                                    it.name.trim().capitalize()
                                    return@let it
                                }
                            }.root
                        )
                    }
                }.root
            )
        }
    }

    private fun pendingOrderToggel(statusText: String, isError: Boolean = true) {

        bd.includeOrderTracking.clTimelineContainer.visibility =
            if (isError) View.GONE else View.VISIBLE
        bd.includeOrderTracking.tvError.text = statusText
        bd.includeOrderTracking.tvError.visibility = if (!isError) View.GONE else View.VISIBLE
        bd.includeOrderTracking.containerOrderDetails.visibility =
            if (isError) View.GONE else View.VISIBLE
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.scratch_card_layout -> {
                val intent = Intent(requireContext(), ScratchCardActivity::class.java)
                startActivity(intent)

            }
            R.id.btn_call_rider, R.id.btn_call_rider1 -> {
                val callIntent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + vm.orderRiderInfo.value?.phone))
                (requireContext() as AppCompatActivity).startActivity(callIntent)
            }
            R.id.btn_back -> {
                vm.detachView()
                dismiss()
            }
            R.id.tv_help -> {
                ZohoLiveChat.Chat.open()
            }
            R.id.tv_back -> {
                dismiss()
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        removeHandlers()
        super.onDismiss(dialog)

    }

    private fun removeHandlers() {
        repatTask?.cancel(true)
        mainHandler.removeCallbacks(task)
    }

    fun updateOrderTraking() {
        getOrderStatus()
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {


        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.nestedOrderTracking.visibility = View.GONE
//            bd.map.visibility = View.GONE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet
        }
    }
}