package com.yumzy.orderfood.ui

object SharedTransition {

    const val TRANSITION_NAME: String = "transition_name"
    const val TRANSITION_MULTI_OBJECT: String = "transition_multi_object"
    const val ACTIVITY_IMAGE_TRANSITION = "activity_image_transition"
    const val ACTIVITY_TEXT_TRANSITION = "activity_text_transition"
    const val ACTIVITY_MIXED_TRANSITION = "activity_mixed_transition"
    const val FRAGMENT_IMAGE_TRANSITION = "fragment_image_transition"
    const val ACTIVITY_FRAME_TRANSITION = "fragment_frame_transition"
    const val FRAGMENT_FRAME_TRANSITION = "fragment_frame_transition"
    const val PARCELABLE_BITMAP = "parcelable_bitmap"


    const val EXTRA_TRANSITION_NAME = "extra_transition_name"
    const val EXTRA_MULTI_TRANSITION_NAME = "extra_multi_transition_name"


}