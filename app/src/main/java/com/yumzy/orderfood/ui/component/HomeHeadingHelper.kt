package com.yumzy.orderfood.ui.component

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewSectionTitleBinding
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst

class HomeHeadingHelper(
    private val context: Context,
    private val layoutType: Int,
    private val homItemForPromo: HomeListDTO,
    private val title: String?,
    private val description: String?,
    private val showSeeAll: Boolean,
    private val callerFun: (String?, HomeListDTO, Int) -> Unit

) {
    fun build(): View? {

        val binding =
            ViewSectionTitleBinding.inflate(LayoutInflater.from(context), null, false)
        binding.title = title
        binding.subtitle = description
        val b = !(layoutType in 2000..2999 ||
                layoutType == IConstants.LayoutType.Banner1000.THE_BRANDS ||
                layoutType == IConstants.LayoutType.Dish3000.YUMZY_BEST ||
                layoutType == IConstants.LayoutType.Dish3000.POPULAR_FOOD ||
                layoutType == IConstants.LayoutType.Dish3000.TRENDING_FOODS
                )

//        binding.hideSeeAll = !showSeeAll

        binding.hideSeeAll = if (b) View.VISIBLE else View.GONE

        if (title?.isEmpty() == true)
            return View(context).apply {
                layoutParams =
                    LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 5.px)
            }
        ClickUtils.applyGlobalDebouncing(
            binding.tvSeeAll,
            IConstants.AppConstant.CLICK_DELAY
        ) {
            callerFun.invoke(title, homItemForPromo, layoutType)
        }

        return binding.root
    }

}