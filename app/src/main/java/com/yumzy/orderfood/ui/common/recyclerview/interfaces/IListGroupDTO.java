package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

import java.util.List;

public interface IListGroupDTO<T> {
    String getMenuName();

    boolean isCollapsible();

    int getParentId();

    boolean isGroup();

    int getIconId();

    int getMenuId();

    byte getLevel();

    List<IListGroupDTO<T>> getListItems();


    /*interface IMenuDTO extends IListGroupDTO{
        int getMenuId();
        int getParentId();
    }*/
}