package com.yumzy.orderfood.ui.screens.module.favourites.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ProfileServiceAPI
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class FavourriteRestaurantRemoteDataSource  constructor(private val serviceAPI: ProfileServiceAPI) :
    BaseDataSource() {
    suspend fun getFavouriteRestaurant() = getResult {
        serviceAPI.getFavouriteRestaurant()
    }


}