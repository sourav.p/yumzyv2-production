package com.yumzy.orderfood.ui.screens.restaurant.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class RestaurantRemoteDataSource  constructor(private val serviceAPI: CartServiceAPI) :
    BaseDataSource() {

    suspend fun foodMenuDetails(outletId: String, userId: String) = getResult {
        serviceAPI.foodMenuDetails(outletId, userId)
    }

    suspend fun addToCart(addCart: NewAddItemDTO) = getResult {
        serviceAPI.addToCart(addCart)
    }

    suspend fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = getResult {
        serviceAPI.itemCount(position, count, itemCount)
    }

    suspend fun addFav(addFav:  MutableMap<String, Any>) = getResult {
        serviceAPI.addFav(addFav)
    }

    suspend fun getCart(cartId: String) = getResult {
        serviceAPI.getCart(cartId)
    }


    suspend fun clearCart(cartId: String) = getResult {

        val cartMap = mapOf<String, String>(
            "cartId" to  cartId
        )
        serviceAPI.clearCart(cartMap)
    }

}