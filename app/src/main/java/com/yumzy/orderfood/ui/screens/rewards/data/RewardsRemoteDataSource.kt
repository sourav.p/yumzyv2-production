package com.yumzy.orderfood.ui.screens.rewards.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.RewardServiceAPI
import com.yumzy.orderfood.data.models.ScratchSummaryDTO
import com.yumzy.orderfood.data.models.base.ResponseDTO
import javax.inject.Inject

class RewardsRemoteDataSource  constructor(private val rewardServiceAPI: RewardServiceAPI) :
    BaseDataSource() {
    suspend fun applyReferral(
        userId: String,
        mapReferral: Map<String, String>
    ) = getResult {
        rewardServiceAPI.applyReferral(userId, mapReferral)
    }

    suspend fun getScratchCardInfo(): ResponseDTO<ScratchSummaryDTO> =
        getResult { rewardServiceAPI.getScratchCardInfo() }
}