package com.yumzy.orderfood.ui.screens.orderhistory

import android.view.LayoutInflater
import android.view.ViewGroup
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Order
import com.yumzy.orderfood.databinding.AdapterOrderHistoryBinding
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.dateformater.UTCtoNormalDate

/**
 * Created by Bhupendra Kumar Sahu on 07-Aug-20.
 */
class OrderHistoryItem : AbstractBindingItem<AdapterOrderHistoryBinding>() {

    internal var order: Order? = null
    var context: OrderHistoryFragment? = null
    var sectionTitle: String? = ""
    var sectionId: Long = 0L
    var historyItemListener: OrderHistoryItemListener? = null
    lateinit var clickedListener: (action: Int, orderHistoryList: Order) -> Unit
    lateinit var viewClickListener: (orderId: String) -> Unit
    override val type: Int
        get() = R.id.top_outlet_list

    override fun bindView(binding: AdapterOrderHistoryBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        order?.creationDate?.let {
            if (UTCtoNormalDate.isToday(it) == true) {
                sectionTitle = context?.getString(R.string.today)
            } else if (UTCtoNormalDate.isYesterday(it) == true) {
                sectionTitle = context?.getString(R.string.yesterday)
            } else {
                sectionTitle =
                    UTCtoNormalDate.getDate(it)
            }
            sectionId = UTCtoNormalDate.getDateMillis(it)
        }

        binding.orderHistoryCard.setOrderHistoryDetails(order)

        ClickUtils.applyGlobalDebouncing(
            binding.orderHistoryCard.getReorderBtn(),
            IConstants.AppConstant.CLICK_DELAY
        ) {
            historyItemListener?.actionReorder(order?.outletId ?: "")
        }

        ClickUtils.applyGlobalDebouncing(
            binding.orderHistoryCard.getRateNowBtn(),
            IConstants.AppConstant.CLICK_DELAY
        ) {
            historyItemListener?.actionRate(order!!)
        }

        ClickUtils.applyGlobalDebouncing(
            binding.orderHistoryCard.getTrackNowBtn(),
            IConstants.AppConstant.CLICK_DELAY
        ) {
            historyItemListener?.actionTrack(order?.orderId ?: "")

        }

        ClickUtils.applyGlobalDebouncing(
            binding.orderHistoryCard,
            IConstants.AppConstant.CLICK_DELAY
        ) {
            historyItemListener?.actionOrderDetails(order?.orderId ?: "")
        }
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOrderHistoryBinding {
        return AdapterOrderHistoryBinding.inflate(inflater, parent, false)
    }

    fun withOrder(order: Order): OrderHistoryItem {
        this.order = order
        return this
    }

    fun withListener(listener: OrderHistoryItemListener): OrderHistoryItem {
        this.historyItemListener = listener
        return this
    }

    fun withContext(context: OrderHistoryFragment): OrderHistoryItem {
        this.context = context
        return this
    }

}