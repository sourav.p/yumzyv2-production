package com.yumzy.orderfood.ui.screens.payment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil.appendUri
import java.net.URI
import java.net.URL


class PaymentWebActivity : AppCompatActivity() {

    companion object {
        const val REQ_CODE = 8272
    }

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        if (intent.hasExtra(IConstants.ActivitiesFlags.PAYMENT_LINK)) {
            val stringExtra = intent.getStringExtra(IConstants.ActivitiesFlags.PAYMENT_LINK)
            if (stringExtra == "") cancelActivity()

            webView = findViewById(R.id.webView)

              webView.webViewClient = object : WebViewClient() {
                  override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

                      val parsedUrl = URL(url)
                      if(parsedUrl.host ==  URL(BuildConfig.CALL_BACK_URL).host) {
                          val intent = Intent()
                          intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_REDIRECT, url)
                          setResult(Activity.RESULT_OK, intent)
                          this@PaymentWebActivity.finish()
                          return false //will not be handled by default action
                      }

                      return true // will be handled by default action
                  }
              }

            webView.settings.javaScriptEnabled = true
            webView.settings.loadWithOverviewMode = true
            webView.settings.useWideViewPort = true
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
            webView.webChromeClient = WebChromeClient()
            webView.addJavascriptInterface(object : WebAppInterface(){
                override fun onNavigationUpClick() {  Handler(Looper.getMainLooper()).post { onBackPressed() } }
            }, "Android")

            stringExtra?.let {webView.loadUrl(appendUri(it, "platform=android").toString()) }

        } else {
            cancelActivity()
        }
    }

    override fun onBackPressed() {
        if (webView.copyBackForwardList().currentIndex > 0) {
            webView.goBack()
        } else {
            cancelActivity()
        }
    }

    private fun cancelActivity() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_CANCELED, returnIntent)
        finish()
    }

    abstract class WebAppInterface {
        @JavascriptInterface
        fun event_back(){
            onNavigationUpClick()
        }

        abstract fun onNavigationUpClick()
    }

}