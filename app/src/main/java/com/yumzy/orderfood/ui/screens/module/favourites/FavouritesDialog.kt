package com.yumzy.orderfood.ui.screens.module.favourites

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.select.getSelectExtension
import com.mikepenz.itemanimators.SlideUpAlphaAnimator
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FavRestaurantResDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogFavouriteRestaurantBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.screens.home.FavItem
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.StickyHeaderAdapter
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import javax.inject.Inject

class FavouritesDialog :
    BaseDialogFragment<DialogFavouriteRestaurantBinding>(),
    IFavouriteRestaurantsView, FavOutletListener {
    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter

    val restaurantViewModel: FavouritesViewModel by viewModel()

    override fun getLayoutId(): Int = R.layout.dialog_favourite_restaurant

    override fun onDialogReady(view: View) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbFavRes)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        showProgressBar()

        bd.tbFavRes.title = resources.getString(R.string.favourite_restaurant)
        bd.tbFavRes.setNavigationOnClickListener { this.dismiss() }

    }

    private fun setupPagingRecyclerView() {

        val stickyHeaderAdapter = StickyHeaderAdapter<GenericItem>()
        mFastAdapter = FastItemAdapter()
//        fastItemAdapter .selec(true);
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        bd.rvFavRestaurant.layoutManager = LinearLayoutManager(context)
        bd.rvFavRestaurant.itemAnimator = DefaultItemAnimator()
        bd.rvFavRestaurant.adapter = stickyHeaderAdapter.wrap(mFastAdapter)

        val decoration = StickyRecyclerHeadersDecoration(stickyHeaderAdapter)
        bd.rvFavRestaurant.addItemDecoration(decoration)

        //so the headers are aware of changes
        mFastAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                decoration.invalidateHeaders()
            }
        })

        mFastAdapter.attachDefaultListeners = true
        fetchFavouriteRestaurantList()
    }

    private fun fetchFavouriteRestaurantList() {

        restaurantViewModel.getFavouriteRestaurant().observe(
            this,
            { result ->
                RHandler<FavRestaurantResDTO>(this, result) {

                    val favItem = ArrayList<FavItem>()
                    it.outlets.forEach { outlets ->
                        val favItemView =
                            FavItem().withOutlet(outlets).withListener(this)
                        favItem.add(favItemView)
                    }
                    mFastAdapter.add(favItem)
                    if (it.outlets.isNullOrEmpty()) {
                        bd.noDataToDisplay.visibility = View.VISIBLE
                        bd.rvFavRestaurant.visibility = View.GONE


                    } else {
                        //  bd.rvFavRestaurant.addItemDecoration(VerticalItemDecoration(5.px, false))
                        bd.rvFavRestaurant.layoutManager = LinearLayoutManager(context)
                        bd.rvFavRestaurant.itemAnimator = SlideUpAlphaAnimator().apply {
                            addDuration = 600
                            removeDuration = 200
                        }
                        bd.noDataToDisplay.visibility = View.GONE
                        bd.rvFavRestaurant.visibility = View.VISIBLE
                    }

                    // setFavouriteRestaurantList(result.data as FavRestaurantResDTO)
                }
            })
    }
//    private fun setFavouriteRestaurantList(favRestaurantResDTO: FavRestaurantResDTO) {
//
//        if (favRestaurantResDTO.outlets.isNullOrEmpty()) {
//            bd.noDataToDisplay.visibility = View.VISIBLE
//            bd.rvFavRestaurant.visibility = View.GONE
//
//
//        } else {
//            //  bd.rvFavRestaurant.addItemDecoration(VerticalItemDecoration(5.px, false))
//            bd.rvFavRestaurant.layoutManager = LinearLayoutManager(context)
//            bd.rvFavRestaurant.itemAnimator = SlideUpAlphaAnimator().apply {
//                addDuration = 600
//                removeDuration = 200
//            }
//            bd.noDataToDisplay.visibility = View.GONE
//            bd.rvFavRestaurant.visibility = View.VISIBLE
//
//            activity?.let {
//                //setup restaurant adapter
//                bd.rvFavRestaurant.adapter =
//                    FavouritesAdapter(
//                        it,
//                        favRestaurantResDTO,
//                        clickListener = { position, itemId -> onPostSelected(position, itemId) })
//                bd.rvFavRestaurant.layoutManager = LinearLayoutManager(it)
//
//            }
//        }
//    }
//    private fun onPostSelected(position: Int, itemId: String) {
//        val intent = Intent(context, OutletActivity::class.java)
//        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, itemId)
//        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
////        dismiss()
//    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet

        }
    }

    override fun onResume() {
        super.onResume()

        setupPagingRecyclerView()

    }

    override fun onItemClick(itemId: String?) {
        val intent = Intent(context, OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, itemId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
    }

}

interface FavOutletListener {
    fun onItemClick(itemId: String?)

}
