package com.yumzy.orderfood.ui.component

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.annotation.IntRange
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.ISelectionListener
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.data.models.CustomizeOrderSectionDTO
import com.yumzy.orderfood.ui.screens.module.restaurant.OrderCustomizationDialog

class OrderCustomizationView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<OrderCustomizationOptionItem>
    private var selectExtension: SelectExtension<OrderCustomizationOptionItem>

    private var maximumSelection: Int = NO_LIMIT
    private var minimumSelection: Int = NO_LIMIT
    private var onGoingInitialSelection: Boolean = true

    internal var section: CustomizeOrderSectionDTO? = null
    private var listener: OrderCustomizationDialog.CustomizationChangeListener? = null

    init {

        //create our FastAdapter which will manage everything
        fastItemAdapter = FastItemAdapter()
        selectExtension = fastItemAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<OrderCustomizationOptionItem>, item: OrderCustomizationOptionItem, _: Int ->
                v?.let {
                    Toast.makeText(v.context, item.name?.getText(v.context), Toast.LENGTH_LONG)
                        .show()
                }
                false
            }
        fastItemAdapter.onPreClickListener =
            { _: View?, _: IAdapter<OrderCustomizationOptionItem>, _: OrderCustomizationOptionItem, _: Int ->
                true // consume otherwise radio/checkbox will be deselected
            }


        fastItemAdapter.addEventHook(OrderCustomizationOptionItem.CheckBoxClickEvent())

        selectExtension.selectionListener =
            object : ISelectionListener<OrderCustomizationOptionItem> {
                override fun onSelectionChanged(
                    item: OrderCustomizationOptionItem,
                    selected: Boolean
                ) {
                    onItemSelectionChanged(item, selected, selectExtension.selectedItems)
                }
            }

    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = LinearLayoutManager(context)
        this.itemAnimator = DefaultItemAnimator()
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter

    }

    private fun setSelectionConstraints(
        @IntRange(from = 1) minimumSelection: Int,
        @IntRange(from = 1) maximumSelection: Int
    ) {
        if (maximumSelection < minimumSelection) {
            throw IllegalArgumentException("maximumsSelction: ${maximumSelection} cannot be less than minimumSelection: ${minimumSelection}")
        }
        this.minimumSelection = minimumSelection
        this.maximumSelection = maximumSelection

        if (selectExtension.selectedItems.size >= minimumSelection) return
        //Select initial items to the min value
        for (i in 0 until minimumSelection) {
            selectExtension.toggleSelection(i)
        }

    }

    private fun onItemSelectionChanged(
        actionItem: OrderCustomizationOptionItem,
        selected: Boolean,
        selectedItems: MutableSet<OrderCustomizationOptionItem>
    ) {

        if (onGoingInitialSelection) return

        //check the conditions for max selection and min selections
        if (selectedItems.size < minimumSelection) {
            //handling for selection less than @minimumSelection
            var addSelectionCount = minimumSelection - selectedItems.size
            for (i in 0 until fastItemAdapter.itemCount) {
                if (fastItemAdapter.getItem(i)?.isSelected == false) {
                    selectExtension.toggleSelection(i)
                    addSelectionCount--
                } else {
                    continue
                }
                if (addSelectionCount <= 0) break
            }

        } else if (selectedItems.size > maximumSelection) {
            //handling for selection greater than @maximumSelection
            var removeSelectionCount = selectedItems.size - maximumSelection
            for (i in 0 until fastItemAdapter.itemCount) {
                val item = fastItemAdapter.getItem(i)
                if (item?.isSelected == false) {
                    continue
                } else if (item?.identifier != actionItem.identifier) {
                    selectExtension.toggleSelection(i)
                    removeSelectionCount--
                }
                if (removeSelectionCount <= 0) break
            }
        }

        listener?.onCustomisationChange(
            section?.addOnId.toString(),
            selectExtension.selectedItems.map { item -> item.data!! }.toList()
        )
    }

    fun withAddonSection(
        listener: OrderCustomizationDialog.CustomizationChangeListener,
        section: CustomizeOrderSectionDTO
    ): OrderCustomizationView {

        this.section = section
        this.listener = listener

        val items = ArrayList<OrderCustomizationOptionItem>()
        for (data in section.items) {
            items.add(
                OrderCustomizationOptionItem(
                    if (section.isMultiSelectEnable)
                        OrderCustomizationOptionItem.Companion.OptionItemType.MULTI_SELECT
                    else
                        OrderCustomizationOptionItem.Companion.OptionItemType.SINGLE_SELECT
                ).withData(data)
            )
        }

        fastItemAdapter.add(items)


        section.items.forEachIndexed { index, customizeOrderItemDTO ->
            if (customizeOrderItemDTO.isSelected) selectExtension.toggleSelection(index)
        }

        setSelectionConstraints(section.min, section.max)

        onGoingInitialSelection = false

        listener.onCustomisationChange(
            section.addOnId,
            selectExtension.selectedItems.map { item -> item.data!! }.toList()
        )

        return this
    }

    fun getSelectedOptions(): Pair<Double, MutableList<String>>? {
        val options = mutableListOf<String>()
        var price = 0.0
        selectExtension.selectedItems.forEach { item ->
            options.add(item.data?.itemName.toString())
            price +=
                if (item.data?.itemOfferPrice != 0.0) item.data?.itemOfferPrice
                    ?: 0.0 else item.data?.itemPrice ?: 0.0
        }
        return if (options.isEmpty()) null else Pair(price, options)
    }

    fun restoreOnConfigChange(savedInstanceState: Bundle?) {
        //restore selections (this has to be done after the items were added
        fastItemAdapter.withSavedInstanceState(savedInstanceState)
    }

    companion object {
        const val NO_LIMIT = -1
    }


}