package com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ListElement
import com.yumzy.orderfood.databinding.AdapterTopRestaurantBinding
import com.yumzy.orderfood.util.viewutils.YumUtil

class TopOutletItem(restaurantItem: ListElement) :
    ModelAbstractBindingItem<ListElement, AdapterTopRestaurantBinding>(restaurantItem) {


    override val type: Int
        get() = R.id.top_outlet_list


    override fun bindView(binding: AdapterTopRestaurantBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.imageUrl = model.imageUrl
        binding.title = model.outletName
        binding.subtitle = if (!model.subTitle.isNullOrEmpty()) model.subTitle else ""
        binding.cuisines =
            model.cuisines.joinToString(separator = ", ", postfix = "")
        binding.duration =
            if (model.costForTwo.isNullOrEmpty()) "" else "${model.costForTwo} for One"
        binding.offer = YumUtil.setZoneOfferString(model.offers)

    }

    override fun unbindView(binding: AdapterTopRestaurantBinding) {
//        binding.imageUrl = null

        binding.title = null
        binding.subtitle = null
        binding.cuisines = null
        binding.offer = null
//        binding.rating = null
        binding.duration = null
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterTopRestaurantBinding {
        return AdapterTopRestaurantBinding.inflate(inflater, parent, false)
    }
}