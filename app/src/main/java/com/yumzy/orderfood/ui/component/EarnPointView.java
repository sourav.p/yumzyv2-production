package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.laalsa.laalsalib.ui.VUtil;
import com.laalsa.laalsalib.utilcode.util.ShadowUtils;
import com.yumzy.orderfood.R;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class EarnPointView extends LinearLayout {

    private String points;
    private String header;
    private String subHeader;


    public EarnPointView(Context context) {
        this(context, null);
    }

    public EarnPointView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EarnPointView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (header == null) {
            header = "";
        }
        if (subHeader == null) {
            subHeader = "";
        }
        if (points == null) {
            points = "";
        }

        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.EarnPointView, defStyleAttr, 0);
        points = a.getString(R.styleable.EarnPointView_points);
        header = a.getString(R.styleable.EarnPointView_pointsHeader);
        subHeader = a.getString(R.styleable.EarnPointView_pointsSubHeader);

        a.recycle();
    }


    public void init() {
        this.setOrientation(HORIZONTAL);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int i105px = VUtil.dpToPx(105);
        int i150px = VUtil.dpToPx(150);
        int i8px = VUtil.dpToPx(8);
        int i12px = VUtil.dpToPx(12);
        int i5px = VUtil.dpToPx(5);
        int i1px = VUtil.dpToPx(1);
        int i10px = VUtil.dpToPx(10);


        LinearLayout leftContainer = new LinearLayout(getContext());
        leftContainer.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);


        LinearLayout.LayoutParams containerLayoutParams = new LinearLayout.LayoutParams(i105px, i105px);
        containerLayoutParams.setMargins(i5px, i5px, i5px, i5px);
        ShadowUtils.apply(
                leftContainer,
                new ShadowUtils.Config().setShadowSize(i1px).setShadowRadius(i10px).setShadowColor(0XBFCECECE)
        );
        leftContainer.setLayoutParams(containerLayoutParams);
        leftContainer.setOrientation(VERTICAL);


        TextView point = new TextView(getContext());
        point.setText("Points");
        point.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        point.setTypeface(Typeface.DEFAULT);
        point.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        point.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);

        TextView tvPoints = new TextView(getContext());
        tvPoints.setText(points);
        tvPoints.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        tvPoints.setId(R.id.tv_points);
        tvPoints.setTypeface(Typeface.DEFAULT_BOLD);
        tvPoints.setTextColor(ContextCompat.getColor(getContext(), R.color.blueJeansDark));
        tvPoints.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f);


        TextView tvWallet = new TextView(getContext());
        tvWallet.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        tvWallet.setPadding(i5px, i5px, i5px, 0);
        tvWallet.setText(R.string.received_in_yumzy_wallet);
        tvWallet.setTypeface(Typeface.DEFAULT);
        tvWallet.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        tvWallet.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);

        leftContainer.addView(point);
        leftContainer.addView(tvPoints);
        leftContainer.addView(tvWallet);


        LinearLayout rightContainer = new LinearLayout(getContext());
        LinearLayout.LayoutParams rightLayoutParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);

        rightContainer.setLayoutParams(rightLayoutParams);
        rightContainer.setOrientation(VERTICAL);
        rightContainer.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        rightContainer.setPadding(i12px, 0, i8px, 0);

        TextView tvHeader = new TextView(getContext());
        tvHeader.setText(R.string.reward_received_on);
        tvHeader.setPadding(0, 0, 0, i8px);
        tvHeader.setTypeface(Typeface.DEFAULT_BOLD);
        tvHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);


        TextView headers = new TextView(getContext());
        headers.setText(header);
        headers.setId(R.id.tv_points_header);
        headers.setTypeface(Typeface.DEFAULT);
        headers.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        headers.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        headers.setPadding(0, 0, 0, i8px);

        TextView subHeaders = new TextView(getContext());
        subHeaders.setText(subHeader);
        subHeaders.setId(R.id.tv_points_subheaders);
        subHeaders.setTypeface(Typeface.DEFAULT);
        subHeaders.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        subHeaders.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);


        rightContainer.addView(tvHeader);
        rightContainer.addView(headers);
        rightContainer.addView(subHeaders);


        this.addView(leftContainer);
        this.addView(rightContainer);
    }


    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
        ((TextView) findViewById(R.id.tv_points)).setText(this.points);
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
        ((TextView) findViewById(R.id.tv_points_header)).setText(this.header);


    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
        ((TextView) findViewById(R.id.tv_points_subheaders)).setText(this.subHeader);
    }


}
