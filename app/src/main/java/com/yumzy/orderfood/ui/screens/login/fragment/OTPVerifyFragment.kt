package com.yumzy.orderfood.ui.screens.login.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.clevertap.android.geofence.CTGeofenceAPI
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.LoginDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentOtpVerifyBinding
import com.yumzy.orderfood.tools.analytics.CleverEvents
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.otpview.PinEditView
import com.yumzy.orderfood.ui.screens.login.ILoginView
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.ui.screens.login.data.SmsListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.worker.SMSBroadCastReceiver
import org.koin.androidx.viewmodel.ext.android.viewModel


class OTPVerifyFragment : BaseFragment<FragmentOtpVerifyBinding, LoginViewModel>(), ILoginView,
    SmsListener,
    PinEditView.PinViewEventListener {
    lateinit var number: String
    lateinit var email: String
    lateinit var firstName: String
    lateinit var lastName: String
    lateinit var mobile: String
    lateinit var navFrom: String
    private var smsBroadCastReceiver: SMSBroadCastReceiver? = null
    private val navController by lazy {
        findNavController()
    }

    override val vm: LoginViewModel by viewModel()


    override fun getLayoutId(): Int = R.layout.fragment_otp_verify

    override fun onFragmentReady(view: View) {
        vm.attachView(this)

        sharedPrefsHelper.saveIsTemporary(true)

//        val leftArrow = IconFontDrawable(requireContext(), R.string.icon_chevron_left)
//        leftArrow.textSize = 23f
//        bd.tvHeaderOtp.setCompoundDrawablesWithIntrinsicBounds(leftArrow, null, null, null)
//        bd.tvHeaderOtp.compoundDrawablePadding = 5.px
        bd.backArrow.setOnClickListener {
            hideKeyboard()
            navController.popBackStack()
        }
        bd.tvChangeNo.setOnClickListener {
            hideKeyboard()
            navController.popBackStack()
        }
        bd.pinview.setPinViewEventListener(this)
        hideKeyboard()
        startCountDownTimer()
        startSMSListener()
        bd.btnSubmitOtp.setOnClickListener {

            if (bd.pinview.value.length == 0) {
                showSnackBar(getString(R.string.please_enter_otp), -1)

            } else if (bd.pinview.value.length < 4) {
                showSnackBar(getString(R.string.invalid_otp), -1)
            } else {
                if (navFrom.equals(IConstants.FragmentFlag.FROM_LOGIN)) {
                    tryValidateUser()
                } else {
                    registerNewUser(bd.pinview.value)
                }
            }


        }
        bd.tvResendCode.setOnClickListener {
            if (navFrom.equals(IConstants.FragmentFlag.FROM_LOGIN)) {
                vm.resendOTP(number, "login")
                    .observe(this, {
                        RHandler<Any>(this, it) {
                            startCountDownTimer()

                        }
                    })
            } else {
                vm.resendOTP(number, "signup")
                    .observe(this, {
                        startCountDownTimer()
                    })
            }
        }
    }

    private fun startCountDownTimer() {
        val counterTimer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                bd.tvCounter.text = "${millisUntilFinished / 1000}s Please Wait"
                bd.tvResendCode.isEnabled = false
                bd.tvCounter.visibility = View.VISIBLE
                bd.tvResendCode.visibility = View.GONE
            }

            override fun onFinish() {
                if (activity != null) {
                    bd.tvResendCode.isEnabled = true
                    bd.tvResendCode.text = getString(R.string.resend_code)
                    bd.tvCounter.visibility = View.GONE
                    bd.tvResendCode.visibility = View.VISIBLE


                }
            }
        }
        counterTimer.start()
    }

    private fun startSMSListener() {
        try {
            smsBroadCastReceiver = SMSBroadCastReceiver()

            smsBroadCastReceiver?.bindListener(object : SmsListener {
                @SuppressLint("HardwareIds")
                override fun messageReceived(messageText: String?) {

//                Log.e("----->>", messageText)
//                    showToast(messageText.toString())
                    bd.pinview.value = messageText ?: ""
                }

                override fun onTimeOut(messageText: String?) {
                    showSnackBar(messageText.toString(), -1)
                }
            })
            val intentFilter = IntentFilter()
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
            requireActivity().registerReceiver(smsBroadCastReceiver, intentFilter)

            val client = SmsRetriever.getClient(requireActivity())

            val task = client.startSmsRetriever()
            task.addOnSuccessListener {
                // API successfully started
//                showToast("API successfully started ")

            }

            task.addOnFailureListener {
//                showToast("API fail ")
                showSnackBar(getString(R.string.enter_otp_failed_to_read_otp), -1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun registerNewUser(userOtp: String) {
        vm.registerNewUser(userOtp).observe(this, { result ->
            RHandler<LoginDTO>(
                this,
                result
            ) {

                it.token.let { token ->
                    sessionManager.saveAuthToken(token)
                }
                it.user.userId.let { value ->
                    sharedPrefsHelper.saveUserId(
                        value
                    )
                }
                it.user.cartId.let { value ->
                    sharedPrefsHelper.saveCartId(
                        value
                    )
                }
                it.user.name.let { value ->
                    sharedPrefsHelper.saveUserName(
                        value
                    )
                }
                it.user.lastName.let { value ->
                    sharedPrefsHelper.saveUserLastName(value)
                }
                it.user.email.let { value ->
                    sharedPrefsHelper.saveUserEmail(
                        value
                    )
                }
                it.user.mobile.let { value ->
                    sharedPrefsHelper.saveUserPhone(
                        value
                    )
                }

                it.user.picture?.let {
                    sharedPrefsHelper.saveUserPic(it)
                }

                it.user.referralDeepLink.let {
                    sharedPrefsHelper.saveUserReferralLink(it)
                }

                it.user.refferalCode.let {
                    sharedPrefsHelper.saveUserReferralCode(it)
                }
                it.isNew.let {
                    sharedPrefsHelper.saveIsNewUser(true)
                }

                registerResponse(it)
                CleverTapHelper.setCleaverUser(it)
                CleverTapHelper.pushEvent(CleverEvents.user_signup)
                try {
                    CTGeofenceAPI.getInstance(requireActivity()).triggerLocation()

                } catch (e: IllegalStateException) {
                    // thrown when this method is called before geofence SDK initialization
                    CleverTapHelper.pushEvent(CleverEvents.signup_location)
                }

                if (it.user.refferalCode.isNotEmpty())
                    CleverTapHelper.userReferral(
                        it.user.name,
                        it.user.mobile,
                        it.user.refferalCode
                    )

            }
        })
    }

    override fun onDataEntered(pinEditView: PinEditView?, fromUser: Boolean) {
        hideKeyboard()
        pinEditView?.value?.let {
            if (navFrom.equals(IConstants.FragmentFlag.FROM_LOGIN)) {
                tryValidateUser()
            } else {
                registerNewUser(it)
            }
        }
    }

    override fun navHomeActivity() {
    }


    override fun onResponse(code: Int?, response: Any?) {
        if (code == APIConstant.Status.SUCCESS) {
            startCountDownTimer()
        } else
            super.onResponse(code, response)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }


    private fun tryValidateUser() {
        val deviceId = YumUtil.getDeviceUniqueId(requireActivity())
        vm.verifyOTP(number, bd.pinview.value, deviceId)
            .observe(this, { result ->
                RHandler<LoginDTO>(
                    this,
                    result
                ) {
                    result.data?.token?.let { token ->
                        sessionManager.saveAuthToken(token)
                    }
                    result.data?.user?.userId?.let { value ->
                        sharedPrefsHelper.saveUserId(
                            value
                        )
                    }
                    result.data?.user?.cartId?.let { value ->
                        sharedPrefsHelper.saveCartId(
                            value
                        )
                    }
                    result.data?.user?.name?.let { value ->
                        sharedPrefsHelper.saveUserName(
                            value
                        )
                    }
                    result?.data?.user?.lastName?.let { value ->
                        sharedPrefsHelper.saveUserLastName(value)
                    }
                    result.data?.user?.email?.let { value ->
                        sharedPrefsHelper.saveUserEmail(
                            value
                        )
                    }
                    result.data?.user?.mobile?.let { value ->
                        sharedPrefsHelper.saveUserPhone(
                            value
                        )
                    }
                    result.data?.user?.picture?.let {
                        sharedPrefsHelper.saveUserPic(it)
                    }
                    result.data?.user?.referralDeepLink?.let {
                        sharedPrefsHelper.saveUserReferralLink(it)
                    }
                    result.data?.user?.refferalCode?.let {
                        sharedPrefsHelper.saveUserReferralCode(it)
                    }
                    result.data?.isNew?.let {
                        sharedPrefsHelper.saveIsNewUser(it)
                    }

                    registerResponse(result.data)
                }
            })
    }

    private fun registerResponse(data: LoginDTO?) {
        showProgressBar()
        if (data != null) {
            vm.updateFcmToken(
                YumzyFirebaseMessaging.getToken(requireActivity()).toString(),
                data.user.userId
            )
                .observe(this, {
                    if (it.type != -1) {
                        vm.saveUserInfo(data.user).observe(this, {
                            if (it.type != -1) {
                                CleverTapHelper.pushEvent(CleverEvents.user_login)
                                CleverTapHelper.setCleaverUser(data)
                                hideProgressBar()
                                val action =
                                    OTPVerifyFragmentDirections.fragmentOtpToSetLocation(IConstants.FragmentFlag.FROM_OTP_VERIFY)
                                navController.navigate(action)
                            }
                        })
                    }
                })
        } else {
            showSnackBar("Failed To Login , Try Later!", -1)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: OTPVerifyFragmentArgs by navArgs()
        number = args.RegisterUser?.mobileNumber.toString()
        email = args.RegisterUser?.email.toString()
        firstName = args.RegisterUser?.firstName.toString()
        lastName = args.RegisterUser?.lastNAme.toString()
        navFrom = args.RegisterUser?.navFrom.toString()
        vm.mobileNumber = number
        vm.emailAddress = email
        vm.firstName = firstName
        vm.lastName = lastName
        bd.phoneNumber = number
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction("com.google.android.gms.auth.api.phone.SMS_RETRIEVED")
        requireActivity().registerReceiver(smsBroadCastReceiver, filter)
    }

    override fun onDestroy() {
        vm.detachView()
        super.onDestroy()
        smsBroadCastReceiver?.unbindListener()
        requireActivity().unregisterReceiver(smsBroadCastReceiver)

    }

    override fun messageReceived(messageText: String?) {
    }

    override fun onTimeOut(messageText: String?) {
        showSnackBar(messageText ?: "Failed", -1)
    }

    fun hideKeyboard() {
        YumUtil.hideKeyboard(requireActivity())

    }
}
