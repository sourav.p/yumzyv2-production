package com.yumzy.orderfood.ui.component.nodata

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.ui.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutNoDataActionBinding
import com.yumzy.orderfood.ui.helper.hideEmptyTextView
import com.yumzy.orderfood.util.viewutils.YumUtil

class EmptyScreenLayout : FrameLayout, View.OnClickListener {
    private var imageSize: Float = 200.pxf
        set(value) {
            field = value
            val imageParam = bind.ivLayoutImage.layoutParams
            imageParam.height = field.toInt()
            bind.ivLayoutImage.layoutParams = imageParam
        }


    var lottiImageRaw: Int? = null
        set(value) {
            field = value
            if (field != null && field != -1) {
                bind.ivLayoutImage.visibility = View.GONE
                bind.screenLotti.setAnimation(field!!)
                bind.screenLotti.playAnimation()
            } else {
                bind.ivLayoutImage.visibility = View.VISIBLE
                bind.screenLotti.visibility = View.GONE
            }
        }
    var title: CharSequence? = ""
        set(value) {
            field = value
            bind.tvTitle.hideEmptyTextView(field)
        }

    var subTitle: CharSequence? = ""
        set(value) {
            field = value
            bind.tvSubTitle.hideEmptyTextView(field)
        }
    var actionText: CharSequence? = ""
        set(value) {
            field = value
            bind.btnAction.hideEmptyTextView(field)
            if (field != null)
                bind.btnAction.setOnClickListener(this)

        }
    private var iconColor: Int = -1
        set(value) {
            field = value
            actionIcon = actionIcon
        }

    //    private var buttonColor: Int = -1
//        set(value) {
//            field = value
//            binding.btnAction.setbti
//            actionIcon = actionIcon
//        }
    private var actionIcon: Int = -1
        set(value) {
            field = value
            if (field != -1) {

                val drawable = ResourcesCompat.getDrawable(context.resources, field, context.theme)
                drawable?.setTint(iconColor)
                bind.btnAction.icon = drawable
                bind.btnAction.setPadding(15.px, 15.px, 25.px, 15.px)

            } else {
                bind.btnAction.icon = null
                bind.btnAction.setPadding(25.px, 15.px, 25.px, 15.px)


            }

        }


    var imageResource: Int? = null
        set(value) {
            field = value
            if (field != null && field != -1) {
                val drawable =
                    ResourcesCompat.getDrawable(context.resources, field!!, context.theme)
                bind.ivLayoutImage.setImageDrawable(drawable)
            } else {
                bind.ivLayoutImage.visibility = View.GONE
                bind.screenLotti.visibility = View.VISIBLE
            }
        }

    fun setImageUrl(url: String?) {
        if (url != null)
            YumUtil.setImageInView(
                bind.ivLayoutImage, url
            )
        if (url == null)
            bind.ivLayoutImage.visibility = View.GONE
        else {
            bind.ivLayoutImage.visibility = View.INVISIBLE
        }
    }

    var actionCallBack: ((view: View) -> Unit)? = null
    private val bind by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutNoDataActionBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.EmptyScreenLayout,
                    defStyleAttr,
                    0
                )
            this.title = a.getString(R.styleable.EmptyScreenLayout_eLayoutTitle)
            this.subTitle = a.getString(R.styleable.EmptyScreenLayout_eLayoutSubtitle)
            this.actionText = a.getString(R.styleable.EmptyScreenLayout_eLayoutAction)
            this.imageSize =
                a.getDimension(R.styleable.EmptyScreenLayout_eLayoutImageSize, imageSize)
            this.imageResource = a.getResourceId(
                R.styleable.EmptyScreenLayout_eLayoutSrc,
                -1
            )
            this.actionIcon = a.getResourceId(
                R.styleable.EmptyScreenLayout_eLayoutActionIcon,
                -1
            )
            this.iconColor = a.getColor(
                R.styleable.EmptyScreenLayout_eLayoutIconColor,
                Color.WHITE
            )
            this.lottiImageRaw = a.getResourceId(
                R.styleable.EmptyScreenLayout_eLayoutLottiImageRaw,
                -1
            )

            a.recycle()

        }


    }

    override fun onClick(v: View) {
        when (v.id) {
            bind.btnAction.id -> actionCallBack?.invoke(v)
        }
    }
}
