package com.yumzy.orderfood.ui.screens.restaurant

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import androidx.viewbinding.ViewBinding
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericFastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter.Companion.items
import com.mikepenz.fastadapter.binding.listeners.addClickListener
import com.mikepenz.fastadapter.listeners.ItemFilterListener
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityOutletBinding
import com.yumzy.orderfood.databinding.AdapterOutletDetailBinding
import com.yumzy.orderfood.databinding.AdapterRestaurentMenuBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.restaurant.adapter.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.intentShareText
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable
import io.cabriole.decorator.DecorationLookup
import io.cabriole.decorator.LinearDividerDecoration
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.set


class OutletActivity : BaseActivity<ActivityOutletBinding, RestaurantViewModel>(),
    IRestaurant, AddItemView.AddItemListener, OutletInteraction, ItemFilterListener<GenericItem> {

    private var isOutleClosed: Boolean = false

    //    private var isSearchVisible: Boolean = false
    private var outletName: String = ""
    private var restaurantDetailDTO: RestaurantDetailDTO? = null
    
    override val vm: RestaurantViewModel by viewModel()

    val cartHelper: CartHelper by inject()

    private var mCart: MutableMap<String, Int> = mutableMapOf()
    private var menuSectionDTOS: MutableList<RestaurantMenuSectionDTO> = mutableListOf()
    private var selectedItems: ArrayList<ValuePairSI> = ArrayList()

    private val id = AtomicLong(1)

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastAdapter
    private lateinit var mHeaderAdapter: GenericItemAdapter
    private lateinit var mItemAdapter: GenericItemAdapter
    private lateinit var mFassiAdapter: GenericItemAdapter

    var isOutletSame = false
    var isRestauranFetched = false
    var outletId: String = ""
    var itemId: String = ""
    var userId: String = ""
    var cartOutletId: String = ""
    var isItemReloadRequired = true
    var isFilterEnabled = false

    private var mCategoryView: NestedScrollView? = null

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.clearPreviousSearchItemList().observe(this, {})
        this.bindView(R.layout.activity_outlet)
        vm.attachView(this)


        setSupportActionBar(bd.tbRestaurant)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""

        BarUtils.setStatusBarLightMode(this, true)

        userId = sharedPrefsHelper.getUserId() ?: ""
        outletId = intent.getStringExtra(IConstants.ActivitiesFlags.OUTLET_ID) ?: ""
        itemId = if (intent.hasExtra(IConstants.ActivitiesFlags.ITEM_ID)) intent.getStringExtra(
            IConstants.ActivitiesFlags.ITEM_ID
        ).toString() else ""

        getCart()
        subscribe()
        applyDebouchingClickListener(bd.btnPayNow, bd.menuFab)
        bd.scrim.setOnClickListener {
            bd.menuFab.isExpanded = false
        }
//        bd.mainAppbar.

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun getCart() {

        cartHelper.mSavedCart.observeForever { cart ->

            if (cart != null) {

                if (cart.outletDTOS != null && cart.outletDTOS.isNotEmpty()) {
                    cart.outletDTOS.get(0)?.itemDTOS?.forEach { item ->
                        mCart[item.itemID] = item.quantity
                    }
                    updateUI(
                        cart.outletDTOS.get(0)?.itemDTOS?.sumBy { item -> item.quantity } ?: 0,
                        cart.outletDTOS.get(0)?.outletName ?: "",
                        cart.billing?.subTotal ?: 0.0
                    )
                    if (cart.outletDTOS[0]?.outletId == outletId) {
                        cartOutletId = outletId

                        val items = ArrayList<ValuePairSI>()
                        cart.outletDTOS.get(0)?.itemDTOS?.forEach { item ->
                            items.add(ValuePairSI(item.itemID, item.quantity))
                        }

                    }
                } else {
                    isOutletSame = true
                    updateUI(
                        0,
                        "",
                        0.0
                    )
                }

            } else {
                isOutletSame = true
                updateUI(
                    0,
                    "",
                    0.0
                )
            }

            fetchMenu()

        }

    }

    override fun fetchMenu() {
        if (isRestauranFetched) return

        vm.foodMenuDetails(outletId, userId).observe(this, { result ->
            isRestauranFetched = true
            RHandler<RestaurantDTO>(
                this,
                result
            ) { restaurant ->

                val restaurantDetail = restaurant.restaurantDetailDTO
                if (restaurantDetail?.available.equals(IConstants.OutletOnOff.ON)) {
                    CleverTapHelper.viewedOutlet(restaurant)
                    vm.restaurantDto = restaurant
                    vm.sectionList.postValue(restaurant.menuSectionDTOS)

                    saveSearchItems(restaurant)
                    val menuSections1 = ArrayList(restaurant.menuSectionDTOS ?: listOf())
                    vm.restaurantDetail.value = restaurantDetail
                    vm.addonsMap = restaurant.menuAddons
                    if (cartOutletId.isNotBlank()) {
                        if (vm.addonsMap != null && vm.addonsMap?.isNotEmpty()!!) {
                            sharedPrefsHelper.saveCartOutletAddon(
                                vm.addonsMap!!,
                                null,
                                arrayListOf()
                            )
                        }
                    }
                    if (menuSections1 == null) return@RHandler

                    populateRecyclerView(restaurant)

                } else {
//                    setContentView(R.layout.layout_not_serving)
//                    val icview = findViewById<InfoCommonView>(R.id.icv_empty_plate)
//                    icview.setInfoHeader(getString(R.string.restaurant_close))
//                    icview.setInfoSubHeader(getString((R.string.not_serving)))

                    isOutleClosed = true;
                    vm.restaurantDetail.value = restaurantDetail
                    showErrorCodeView(
                        getString(R.string.restaurant_close),
                        getString(R.string.not_serving),
                        R.drawable.img_unserviceable
                    )
                    CleverTapHelper.outletClosed(outletId, outletName)
                }

            }
        })
    }


    private fun populateRecyclerView(restaurant: RestaurantDTO) {

        menuSectionDTOS = restaurant.menuSectionDTOS!!.toMutableList()
//        val filterMenuSection = restaurant.menuSections

        var reccomendMenuListDTO: RestaurantMenuSectionDTO? = null
        for ((index, section) in menuSectionDTOS.withIndex()) {
            if (section.type == "top-cards") {
                reccomendMenuListDTO = section
                menuSectionDTOS.removeAt(index)
                break
            }
        }

        var commonMenuListDTO: RestaurantMenuSectionDTO? = null
        for ((index, section) in menuSectionDTOS.withIndex()) {
            if (section.type == "menu") {
                commonMenuListDTO = section
                menuSectionDTOS.removeAt(index)
                break
            }
        }

        if (reccomendMenuListDTO != null) {
            menuSectionDTOS.add(getReccomendMenuSeciton(reccomendMenuListDTO)!!)
        }

        menuSectionDTOS.addAll(getMenuSections(commonMenuListDTO))

        //create our adapters
        mHeaderAdapter = items()
        mItemAdapter = items()
        mFassiAdapter = items()

        //we also want the expandable feature

        //create our FastAdapter
        val adapters: Collection<ItemAdapter<out GenericItem>> =
            listOf(mHeaderAdapter, mItemAdapter, mFassiAdapter)
        mFastAdapter = FastAdapter.with(adapters)
//        val spanHelper=LinearSnapHelper()
//        spanHelper.attachToRecyclerView(bd.rv)


        val gridLayoutManager = GridLayoutManager(this, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (mFastAdapter.getItemViewType(position)) {
                    IConstants.FastAdapter.REST_DETAIL_VIEW -> 2
                    IConstants.FastAdapter.REST_RECCOMEND_ITEM_VIEW -> 1
                    IConstants.FastAdapter.REST_ADD_ITEM_VIEW -> 2
                    IConstants.FastAdapter.REST_FASSI_VIEW -> 2
                    IConstants.FastAdapter.REST_MENU_SECTION -> 2
                    R.id.item_regular_menu -> 2
                    R.id.item_recommended_menu -> 2
                    R.id.item_section_header -> 2
                    else -> -1
                }
            }
        }

        //get our recyclerView and do basic setup
        bd.rv.layoutManager = gridLayoutManager
        bd.rv.adapter = mFastAdapter
        recyclerAddItemDecoration()
        bd.rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset()
                if (computeVerticalScrollOffset > VUtil.dpToPx(40)) {
                    supportActionBar?.title = outletName
                } else {
                    supportActionBar?.title = ""

                }
            }
        })

        mFastAdapter.addClickListener({ vh: ViewBinding ->


            when (vh) {
                is AdapterRestaurentMenuBinding -> {

                }
                is AdapterOutletDetailBinding -> {
                    vh.flSearch.setOnClickListener {
                        val intent = Intent(this, OutletSearchActivity::class.java)
                        intent.putExtra("filter_enabled", isFilterEnabled)
                        AppIntentUtil.launchForResultWithSheetAnimation(
                            this,
                            intent
                        ) { resultOk, _ ->
                            if (resultOk)
                                gotoPlateFragment()

                        }
                        isItemReloadRequired = true
                    }

                    vh.switchVeg.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked) {
                            vh.llEggContainer.visibility = View.VISIBLE
                            mItemAdapter.filter(if (vh.switchEgg.isChecked) "both" else "veg")
                            vm.categoryLiveData.value =
                                if (vh.switchEgg.isChecked) vm.categoryDTOBoth else vm.categoryDTOVeg
                        } else {
                            vh.switchEgg.isChecked = false
                            vh.llEggContainer.visibility = View.GONE
                            mItemAdapter.filter("")
                            vm.categoryLiveData.value = vm.categoryDTOAll
                        }
                    }

                    vh.switchEgg.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked) {
                            mItemAdapter.filter(if (vh.switchVeg.isChecked) "both" else "egg")
                            vm.categoryLiveData.value =
                                if (vh.switchVeg.isChecked) vm.categoryDTOBoth else vm.categoryDTOEgg
                        } else {
                            mItemAdapter.filter(if (vh.switchVeg.isChecked) "veg" else "")
                            vm.categoryLiveData.value =
                                if (vh.switchVeg.isChecked) vm.categoryDTOVeg else vm.categoryDTOAll
                        }
                    }

                }
                else -> {
                }
            }
            return@addClickListener vh.root
        }) { view, position, adapter: FastAdapter<GenericItem>, item: GenericItem -> }


        //header details
        setHeaderDetails(restaurant)

        //set menu items
        setItems(menuSectionDTOS)

        //set bottom fasse detail
        setFassiDetail(restaurant.restaurantDetailDTO)

        vm.getSelectedItem()?.observeForever { it ->
            val deSelectedItems = arrayListOf<ValuePairSI>()

            if (selectedItems.isNotEmpty()) {

                selectedItems.forEach { previousItem ->
                    val item = it.find { item -> item.id == previousItem.id }
                    if (item == null) {
                        deSelectedItems.add(ValuePairSI(previousItem.id, 0))
                    }
                }

                if (deSelectedItems.isNotEmpty()) {
                    updateMenuItemSelection(deSelectedItems)
                    deSelectedItems.clear()
                }

            }


            selectedItems.clear()
            selectedItems.addAll(it)
            updateMenuItemSelection(selectedItems)
        }

        mItemAdapter.itemFilter.filterPredicate = { item, constraint ->
            when (constraint) {
                "veg" -> {
                    when (item) {
                        is BindingRegularMenuItem -> item.restaurantItem?.isVeg == true
                        is BindingRecommendMenuItem -> item.restaurantItem?.isVeg == true
                        is BindingOutletSectionHeader -> item.hasVeg
                        else -> false
                    }
                }
                "egg" -> {
                    when (item) {
                        is BindingRegularMenuItem -> item.restaurantItem?.containsEgg == true
                        is BindingRecommendMenuItem -> item.restaurantItem?.containsEgg == true
                        is BindingOutletSectionHeader -> item.hasEgg
                        else -> false
                    }
                }
                "both" -> {
                    when (item) {
                        is BindingRegularMenuItem -> (item.restaurantItem?.containsEgg == true && item.restaurantItem?.isVeg == true)
                        is BindingRecommendMenuItem -> item.restaurantItem?.containsEgg == true && item.restaurantItem?.isVeg == true
                        is BindingOutletSectionHeader -> item.hasBoth
                        else -> false
                    }
                }
                else -> {
                    true
                }
            }
        }

        mItemAdapter.itemFilter.itemFilterListener = this

    }

    private fun recyclerAddItemDecoration() {
        val decoration = LinearDividerDecoration.create(
            color = ThemeConstant.whiteSmoke,
            size = 1.px,
            leftMargin = 0.px,
            topMargin = 20.px,
            rightMargin = 0.px,
            bottomMargin = 20.px,
            orientation = RecyclerView.VERTICAL
        )
        decoration.setDecorationLookup(object : DecorationLookup {
            override fun shouldApplyDecoration(position: Int, itemCount: Int): Boolean {
                when (mFastAdapter.getItem(position)) {
                    is BindingOutletSectionHeader -> {
                        return false
                    }
                    is BindignRestaurantDetailItem -> {
                        return false
                    }
                    is BindignRestaurantFassiItem -> {
                        return false
                    }
                }
                return true
            }

        })

        bd.rv.addItemDecoration(decoration)

    }

    private fun updateMenuItemSelection(it: List<ValuePairSI>) {
        it.forEach { selectedItem ->
            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val holder = mFastAdapter.getItem(i)
                when (holder) {
                    is BindingRecommendMenuItem -> {
                        val position = mFastAdapter.getPosition(holder)

                        if (holder.restaurantItem?.itemId == selectedItem.id) {
                            if (holder.restaurantItem?.prevQuantity != selectedItem.value) {
                                holder.restaurantItem?.prevQuantity = selectedItem.value
                                mFastAdapter.notifyAdapterItemChanged(position)
                            }
                        }
                    }
                    is BindingRegularMenuItem -> {
                        val position = mFastAdapter.getPosition(holder)
                        if (holder.restaurantItem?.itemId == selectedItem.id) {
                            if (holder.restaurantItem?.prevQuantity != selectedItem.value) {
                                holder.restaurantItem?.prevQuantity = selectedItem.value
                                mFastAdapter.notifyAdapterItemChanged(position)
                            }
                            break@loop
                        }
                    }
                }
            }

        }
    }

    private fun setHeaderDetails(restaurant: RestaurantDTO) {
        val details = restaurant.restaurantDetailDTO
        val restaurantDetailItem = BindignRestaurantDetailItem(this)
            .withRestaurantDetail(
                details
            ).withCategory(vm.categoryLiveData)

        restaurantDetailItem.identifier = id.getAndIncrement()
        mHeaderAdapter.clear()
        mHeaderAdapter.set(listOf(restaurantDetailItem))
    }

    private fun setHorizontalCategory() {

    }

    private fun setItems(menuSectionDTOS: List<RestaurantMenuSectionDTO>?) {

        val items = ArrayList<GenericItem>()
        for ((sectionIndex, section) in menuSectionDTOS?.withIndex()!!) {

            val sectionTitle = section.name.capitalize()
            var vegItemCounter = 0
            var eggItemCounter = 0
            var bothItemCounter = 0

            val sectionHeaderBinding = BindingOutletSectionHeader().withSection(
                sectionTitle,
                section.list?.size ?: 0
            )
            items.add(sectionHeaderBinding)

            for ((_, item) in section.list?.withIndex()!!) {

                if (item.isVeg) {
                    sectionHeaderBinding.hasVeg = true
                    vegItemCounter++
                }

                if (item.containsEgg) {
                    sectionHeaderBinding.hasEgg = true
                    eggItemCounter++
                }

                if (item.containsEgg && item.isVeg) {
                    sectionHeaderBinding.hasBoth = true
                    bothItemCounter++
                }

                if (selectedItems.isNotEmpty()) {
                    val pair = selectedItems.find { selectedItem -> selectedItem.id == item.itemId }
                    if (pair != null) {
                        item.prevQuantity = pair.value
                    } else {
                        item.prevQuantity = 0
                    }
                }

                when (section.layoutType) {
                    13 -> {
                        val reccomendedItem =
                            BindingRecommendMenuItem(this).withRestaurantAddItem(
                                item
                            )
                        reccomendedItem.identifier = id.getAndIncrement()
                        items.add(reccomendedItem)
                    }
                    else -> {
                        val restaurantAddItem =
                            BindingRegularMenuItem(this).withRestaurantAddItem(item)
                        restaurantAddItem.identifier = id.getAndIncrement()
                        items.add(restaurantAddItem)

                    }
                }

            }


            vm.categoryDTOAll.add(RestaurantMenuCategoryDTO().apply {
                key = sectionTitle
                value = sectionHeaderBinding.itemCount
                hasVeg = sectionHeaderBinding.hasVeg
            })

            if (sectionHeaderBinding.hasVeg) {
                vm.categoryDTOVeg.add(RestaurantMenuCategoryDTO().apply {
                    key = sectionTitle
                    value = vegItemCounter
                    hasVeg = sectionHeaderBinding.hasVeg
                })
            }

            if (sectionHeaderBinding.hasEgg) {
                vm.categoryDTOEgg.add(RestaurantMenuCategoryDTO().apply {
                    key = sectionTitle
                    value = eggItemCounter
                    hasEgg = sectionHeaderBinding.hasEgg
                })
            }

            if (sectionHeaderBinding.hasBoth) {
                vm.categoryDTOBoth.add(RestaurantMenuCategoryDTO().apply {
                    key = sectionTitle
                    value = bothItemCounter
                    hasBoth = sectionHeaderBinding.hasBoth
                })
            }

        }


        vm.categoryLiveData.value = vm.categoryDTOAll

        mItemAdapter.set(items)

        Handler(Looper.getMainLooper()).postDelayed({ scrollToItem(this.itemId) }, 1200L)

    }

    private fun scrollToItem(itemId: String) {

        if (itemId.isNotEmpty()) {

            var position = -1
            loop@ for (i in 0 until mFastAdapter.itemCount) {
                val viewHolder = mFastAdapter.getItem(i)
                when {
                    viewHolder is BindingRegularMenuItem && viewHolder.restaurantItem?.itemId == this.itemId ||
                            viewHolder is BindingRecommendMenuItem && viewHolder.restaurantItem?.itemId == this.itemId -> {
                        position = i
                        break@loop

                    }
                }
            }


            //this will collapse action bar layout when scolled
            if (!bd.rv.hasNestedScrollingParent(ViewCompat.TYPE_NON_TOUCH)) {
                bd.rv.startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL, ViewCompat.TYPE_NON_TOUCH)
            }

            if (position != -1) {
                if (mFastAdapter.itemCount > 10 && position > 10) {
                    bd.rv.layoutManager?.scrollToPosition(position - 10)
                }
                val smoothScroller: SmoothScroller =
                    object : LinearSmoothScroller(this) {
                        override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics?): Float {
                            return 20f / displayMetrics?.densityDpi!!
                        }

                        override fun getVerticalSnapPreference(): Int {
                            return SNAP_TO_START
                        }
                    }
                smoothScroller.targetPosition = if (position > 0) position - 1 else position
                bd.rv.layoutManager?.startSmoothScroll(smoothScroller)
                this.itemId = ""

            }

        }
    }

    private fun scrollToSection(sectionTitle: String) {

        //find the position of first item with this sectionIndex
        var position = -1
        loop@ for (i in 0 until mFastAdapter.itemCount) {
            val viewHolder = mFastAdapter.getItem(i)
            when {
                viewHolder is BindingOutletSectionHeader && viewHolder.mTitle?.toLowerCase() == sectionTitle.toLowerCase(
                    Locale.getDefault()
                ) -> {
                    position = mFastAdapter.getPosition(viewHolder)
                    break@loop
                }
            }
        }


        val layoutManager = bd.rv.layoutManager as LinearLayoutManager
        val currentPosition = layoutManager.findFirstCompletelyVisibleItemPosition()

        if (mFastAdapter.itemCount > 10) {

            when {
                position in 11 until currentPosition -> {
                    bd.rv.layoutManager?.scrollToPosition(position + 10)
                }
                position < 10 -> {
                    bd.rv.layoutManager?.scrollToPosition(11)
                }
                position in currentPosition until mFastAdapter.itemCount && position - currentPosition > 10 -> {
                    bd.rv.layoutManager?.scrollToPosition(position - 10)
                }
            }

        }

        //this will collapse action bar layout when scolled
        if (!bd.rv.hasNestedScrollingParent(ViewCompat.TYPE_NON_TOUCH)) {
            bd.rv.startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL, ViewCompat.TYPE_NON_TOUCH)
        }

        //scroll to that position.
        val smoothScroller: SmoothScroller =
            object : LinearSmoothScroller(this) {
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics?): Float {
                    return 50f / displayMetrics?.densityDpi!!
                }

                override fun getVerticalSnapPreference(): Int {
                    return SNAP_TO_START
                }
            }
        smoothScroller.targetPosition = if (position > 0) position - 1 else position
        bd.rv.layoutManager?.startSmoothScroll(smoothScroller)
        this.itemId = ""


        //  mFilterPopup?.dismiss()
        bd.menuFab.isExpanded = false

    }

    private fun setFassiDetail(restaurantDTO: RestaurantDetailDTO?, resultEmpty: Boolean = false) {
        val fassiItem =
            BindignRestaurantFassiItem(this).witLicNo(restaurantDTO?.lic ?: "", resultEmpty)
        fassiItem.identifier = id.getAndIncrement()
        mFassiAdapter.clear()
        mFassiAdapter.set(listOf(fassiItem))
    }

    private fun saveSearchItems(restaurantDTO: RestaurantDTO) {
        vm.saveRestaurantToDatabase(restaurantDTO).observe(this, {})
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//         Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_restaurant, menu)
        return true
    }

    override fun subscribe() {
        vm.restaurantDetail.observe(this, { detail ->

            outletName = detail.outletName.capitalize(Locale.getDefault())
            if (detail.favorite) {
                invalidateOptionsMenu()
            }
            CleverTapHelper.outletOpened(outletId, outletName)

        })

        vm.categoryLiveData.observe(this, { categories ->


            val layout1 = LinearLayout(this)
            layout1.apply {
                this.orientation = LinearLayout.VERTICAL

                this.layoutParams = LinearLayout.LayoutParams(
                    ViewConst.MATCH_PARENT,
                    ViewConst.MATCH_PARENT
                )
            }

            categories?.forEach { category ->

                val layout = LinearLayout(this)
                layout.apply {
                    this.orientation = LinearLayout.HORIZONTAL

                    //category title
                    this.addView(TextView(context).apply {
                        text = category.key.toLowerCase(Locale.getDefault())
                            .capitalize(Locale.getDefault())
                        layoutParams = LinearLayout.LayoutParams(0, ViewConst.WRAP_CONTENT, 1F)
                        setTextColor(
                            DrawableUtils.getColorStateListDrawable(
                                ThemeConstant.textBlackColor,
                                ThemeConstant.textBlackColor
                            )
                        )
                        setInterFont()
                        maxLines = 2
                        ellipsize = TextUtils.TruncateAt.END
                        setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                        setPadding(UIHelper.i5px, UIHelper.i8px, UIHelper.i5px, UIHelper.i8px)
                    })
                    //category count
                    this.addView(TextView(context).apply {
                        text = category.value.toString()
                        setTextColor(
                            DrawableUtils.getColorStateListDrawable(
                                ThemeConstant.textBlackColor,
                                ThemeConstant.textBlackColor
                            )
                        )
                        layoutParams = LinearLayout.LayoutParams(
                            ViewConst.WRAP_CONTENT,
                            ViewConst.WRAP_CONTENT
                        )
                        setInterBoldFont()
                        setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                        setPadding(UIHelper.i5px, UIHelper.i8px, UIHelper.i5px, UIHelper.i8px)
                    })

//                    setClickScaleEffect()
//                    this.setBackgroundColor(
//                        ContextCompat.getColor(
//                            this@OutletActivity,
//                            R.color.white
//                        )
//                    )

                    setOnClickListener {
                        Handler(Looper.getMainLooper()).postDelayed(
                            { scrollToSection(category.key) },
                            350
                        )
                        bd.menuFab.isExpanded = false
                    }
                }

                //   mCategoryView?.background = VUtil.getRoundDrawable(ThemeConstant.lightGray, 0f)

                layout1.addView(layout)

            }


            mCategoryView = NestedScrollView(this).apply {

                this.layoutParams = LinearLayout.LayoutParams(
                    ViewConst.MATCH_PARENT,
                    ViewConst.WRAP_CONTENT
                )
                isScrollbarFadingEnabled = true
                setPadding(UIHelper.i12px, UIHelper.i12px, UIHelper.i12px, UIHelper.i12px)
//                background = VUtil.getRoundDrawable(ThemeConstant.white, UIHelper.i15px.toFloat())
                this.addView(layout1)
                bd.llMenuCategory.removeAllViews()
                bd.llMenuCategory.addView(this)


            }

        })


    }

    private fun getReccomendMenuSeciton(menuSectionDTO: RestaurantMenuSectionDTO?): RestaurantMenuSectionDTO? {
        val itemIndex = AtomicInteger(0)
        menuSectionDTO?.list?.forEach { item -> item.indexPosition = itemIndex.getAndIncrement() }
        menuSectionDTO?.list?.get(menuSectionDTO.list!!.size - 1)?.isLastItem = true
        return menuSectionDTO
    }

    private fun getMenuSections(menuSectionDTO: RestaurantMenuSectionDTO?): Collection<RestaurantMenuSectionDTO> {
        val sections = mutableListOf<RestaurantMenuSectionDTO>()
        val list = menuSectionDTO?.list
        var i = 0
        var itemIndex = AtomicInteger(0)
        if (!list.isNullOrEmpty())
            while (i < list.size) {
                if (list[i].type == "category") {
                    val section = RestaurantMenuSectionDTO()
                    section.type = "menu"
                    section.name = list[i].name.capitalize(Locale.getDefault())
                    section.layoutType = 12
                    section.list = ArrayList()
                    val sectionList = ArrayList(section.list ?: listOf())
                    for (j in i + 1 until list.size) {
                        if (list[j].type == "item") {
                            list[j].indexPosition = itemIndex.getAndIncrement()
                            sectionList.add(list[j])
                        } else if (list[j].type == "subCategory") {
                            continue
                        } else if (list[j].type == "category") {
                            i = j - 2
                            break
                        }
                    }
                    if (sectionList != null) {
                        section.list = sectionList
                        sections.add(section)
                    }
                }
                i++
            }

        return sections
    }

    private fun updateUI(size: Int, outletName: String = "", subTotal: Double = 0.0) {
        if (size > 0) {
            bd.clFooter.visibility = View.VISIBLE
            bd.tvCartPrice.text =
                """$size ${if (size > 1) "Items" else "Item"}  ₹${subTotal.withPrecision(2)}"""
            bd.tvCartOutlet.text = " From $outletName"
        } else {
            bd.clFooter.visibility = View.GONE
        }
    }

    /*
    * Option menu handling (fav and share)
    * */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_like -> {
                val newFavState = !(vm.restaurantDetail.value?.favorite ?: false)
                updateLikeMenuItem(newFavState, item)

                val body = mutableMapOf<String, Any>()
                body["isFav"] = newFavState
                body["ids"] = listOf(outletId)
                body["type"] = "outlet"

                //val addFavrouite = AddFavReqDTO(newFavState, listOf(outletId), "outlet")
                vm.addFav(body).observe(this, { result ->
                    if (result.type == 500) {
                        updateLikeMenuItem(false, item)
                    }
                })
                return true
            }
            R.id.menu_share -> {
                startShare(vm.restaurantDetail.value?.deepLinkUrl)
                return true
            }

            R.id.menu_search -> {
                if(isOutleClosed) return true;
                AppIntentUtil.launchForResultWithBaseAnimation(
                    item.actionView,
                    this,
                    OutletSearchActivity::class.java
                ) { resultOk, _ ->
                    if (resultOk)
                        gotoPlateFragment()
                }

                return true
            }
            else -> false
        }
    }

    private fun startShare(deepLinkUrl: String?) {
        if (!deepLinkUrl.isNullOrBlank()) {
            intentShareText(
                this,
                getString(R.string.Checkout_this_amazing_restaurant) + deepLinkUrl + getString(R.string.found_in_yumzy_app)
            )
            CleverTapHelper.shareOutlet(restaurantDetailDTO)

        } else {
            showToast("Share Link Broken")
        }
    }

    private fun updateLikeMenuItem(newFavState: Boolean, item: MenuItem? = null) {
        if (newFavState) {
            val filledHeart = IconFontDrawable(this, R.string.icon_heart)
            filledHeart.textSize = 24f
            filledHeart.setTextColor(ThemeConstant.pinkies)
            item?.icon = filledHeart

        } else {

            val emptydHeart = IconFontDrawable(this, R.string.icon_heart_empty)
            emptydHeart.textSize = 24f
            emptydHeart.setTextColor(ThemeConstant.pinkies)

            item?.icon = emptydHeart
        }
        vm.restaurantDetail.value?.favorite = newFavState
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        if (vm.restaurantDetail.value?.favorite == true) {
            updateLikeMenuItem(true, menu?.findItem(R.id.menu_like)!!)
        } else {
            updateLikeMenuItem(false, menu?.findItem(R.id.menu_like)!!)
        }

        menu.findItem(R.id.menu_search).isVisible = true

        return super.onPrepareOptionsMenu(menu)

    }


    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.menu_fab -> {
                if (vm.categoryLiveData.value?.isNullOrEmpty() == false) {
                    //    bd.rv.suppressLayout(true)
                    bd.menuFab.isExpanded = true
                    //   bd.rv.suppressLayout(false)

                }

            }
            R.id.btn_pay_now -> {
                gotoPlateFragment()
            }
            R.id.fl_search -> {
                AppIntentUtil.launchWithBaseAnimation(view, this, OutletSearchActivity::class.java)
//                val intent = Intent(this, OutletSearchActivity::class.java)
//                startActivityForResult(intent, SEARCH_REQ_CODE)

            }

        }
    }

    private fun gotoPlateFragment() {
        val returnIntent = Intent()
        returnIntent.putExtra(IConstants.ActivitiesFlags.SHOW_PLATE, REStAURANT_REQ_CODE)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    SEARCH_REQ_CODE -> {
                        gotoPlateFragment()
                    }
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }

    }

    override fun onResponse(code: Int?, response: Any?) {
        if (code == APIConstant.Code.DATA_UPDATED) {
            showSnackBar(response.toString(), -1)

        } else super.onResponse(code, response)

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {


        if (code == APIConstant.Status.NO_NETWORK) {
            showErrorCodeView(
                IConstants.OrderTracking.NO_INTERNET,
                IConstants.OrderTracking.NO_INTERNET_MSG,
                R.drawable.img_no_internet
            )
        } else if (code == APIConstant.Code.NOT_SERVING || code == APIConstant.Code.NO_RESTAURANT_AT_LOCATION) {
            showErrorCodeView(
                title ?: getString(R.string.restaurant_close),
                message,
                R.drawable.img_unserviceable
            )
            CleverTapHelper.outletClosed(outletId, outletName)
        } else if (code == APIConstant.Code.NO_DELIVERY_PARTNER) {
            showErrorCodeView(
                title ?: IConstants.OrderTracking.DELIVERY_UNAVAILABLE,
                message,
                R.drawable.img_deliviry_issue
            )
            CleverTapHelper.outletClosed(outletId, outletName)
        } else
            super.showCodeError(code, title, message)

    }

    private fun showErrorCodeView(title: String, subTitle: String, image: Int) {
        bd.noDataToDisplay.visibility = View.VISIBLE
        bd.clFooter.visibility = View.GONE
        bd.llFloatingMenu.visibility = View.GONE
        bd.frameContainer.visibility = View.GONE
        bd.noDataToDisplay.title = title
        bd.noDataToDisplay.subTitle = subTitle
        bd.noDataToDisplay.imageResource = image
        bd.mainAppbar.setExpanded(false)
//        bd.mainAppbar.layoutParams = CoordinatorLayout.LayoutParams(ViewConst.MATCH_PARENT, 75.px)


    }

    companion object {
        const val REStAURANT_REQ_CODE = 3211
        const val SEARCH_REQ_CODE = 3212
    }

    override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {

    }

    override fun onItemSelected(position: Int, item: RestaurantItem) {

    }

    override fun onItemCountChange(
        cartHandler: ICartHandler,
        count: Int,
        item: RestaurantItem,
        quantityView: AddQuantityView,
        isIncremented: Boolean?

    ) {
        cartHelper.addOrUpdateItemToCart(cartHandler, count, item, vm.addonsMap, isIncremented)
    }

    override fun onItemFailed(message: String) {
        showSnackBar(message, -1)
    }

    override fun adapterSizeChange(itemSize: Int) {
    }

    override fun onNoteClicked(item: RestaurantItem) {
    }

    override fun gotoPlate() {
        gotoPlateFragment()
    }

    override fun itemsFiltered(constraint: CharSequence?, results: List<GenericItem>?) {
        val adapterItem = mFassiAdapter.getAdapterItem(0)
        if (adapterItem is BindignRestaurantFassiItem) {
            val b = constraint?.isNotEmpty() ?: false && results?.isEmpty() == true
            setFassiDetail(restaurantDetailDTO, resultEmpty = b)
        }

    }

    override fun onReset() {}
    override fun onBackPressed() {
        if (!bd.menuFab.isExpanded) {
            super.onBackPressed()

        } else {
            bd.menuFab.isExpanded = false
        }
    }


}

