package com.yumzy.orderfood.ui.screens.scratchcard

import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.scratchcard.data.ScratchRepository
import javax.inject.Inject

class ScratchCardViewModel  constructor(
    private val repository: ScratchRepository,
    private val sharedPrefHelper: SharedPrefsHelper
) : BaseViewModel<IScratchCardView>() {
//    private val _scratchCardInfo: MutableLiveData<ScratchCardDetailDTO> = MutableLiveData()
//    val scratchCardInfo: LiveData<ScratchCardDetailDTO>
//        get() = _scratchCardInfo

    fun getScratchCardsList(skip: Int, limit: Int) =
        repository.fetchScratchCardList(skip, limit)

    fun getScratchCardDetails() =
        repository.getScratchCardDetails()

    fun redeemScratchCard(scratchCard: ScratchCardDTO, phone: String?) =
        repository.redeemScratchCard(scratchCard, phone)

}

