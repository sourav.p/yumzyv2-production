package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.databinding.ListSectionHeadderBinding
import com.yumzy.orderfood.util.IConstants

/**
 * Created by Shriom.
 */
class BindignRestaurantSectionItem :
    AbstractBindingItem<ListSectionHeadderBinding>() {

    var title = ""
    var itemCount = 0
    var hasVeg = false

    var header: String? = null

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override val type: Int
        get() = IConstants.FastAdapter.REST_MENU_SECTION

    /**
     * setter method for the Icon
     *
     * @param item the icon
     * @return this
     */
    fun withSection(sectionTitle: String, sectionItemCount: Int): BindignRestaurantSectionItem {
        this.title = sectionTitle
        this.itemCount = sectionItemCount
        return this
    }
    /**
     * binds the data of this item onto the viewHolder
     */
    override fun bindView(binding: ListSectionHeadderBinding, payloads: List<Any>) {
        //define our data for the view
        binding.title = title.capitalize()
//        binding.count = itemCount
    }

    override fun unbindView(binding: ListSectionHeadderBinding) {
        binding.title = null
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ListSectionHeadderBinding {
        return ListSectionHeadderBinding.inflate(inflater, parent, false)
    }
}