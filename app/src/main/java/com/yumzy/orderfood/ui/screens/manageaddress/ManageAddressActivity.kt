package com.yumzy.orderfood.ui.screens.manageaddress

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityManageAddressBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.component.shadowcard.DetailsCardView
import com.yumzy.orderfood.ui.helper.AlerterHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.location.LocationInfoActivity
import com.yumzy.orderfood.ui.screens.location.LocationSearchActivity
import com.yumzy.orderfood.util.IConstants
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class ManageAddressActivity : BaseActivity<ActivityManageAddressBinding, ManageAddressViewModel>(),
    IManageAddress, IModuleHandler, DetailsCardView.DetailsCardListener {
    
    override val vm: ManageAddressViewModel by viewModel()

    private lateinit var mAdapter: ManageAddressAdapter
    override val navigationPadding: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_manage_address)

        vm.attachView(this)


        setSupportActionBar(bd.tbManageAddress)
        BarUtils.setStatusBarLightMode(this, true)
        BarUtils.setStatusBarColor(this, Color.WHITE)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        setupAdapter()
        applyDebouchingClickListener(bd.btnAddAddress)

        bd.noDataToDisplay.actionCallBack = {
            addNewAddress(it)
        }
    }

    override fun onResume() {
        super.onResume()
        fetchAddressList()
    }

    override fun setupAdapter() {
        mAdapter =
            ManageAddressAdapter(
                emptyList(),
                this
            )
        bd.rvSavedAddress.adapter = mAdapter
        bd.rvSavedAddress.layoutManager = LinearLayoutManager(this)
    }

    override fun fetchAddressList() {
        showProgressBar()
        vm.getAddressList().observe(
            this,
            { result ->
                RHandler<List<AddressDTO>>(this, result) {
                    if (it.isEmpty()) {
                        sharedPrefsHelper.putSelectedAddress("")
                        bd.noDataToDisplay.visibility = View.VISIBLE
                        bd.rvSavedAddress.visibility = View.GONE
                        bd.btnAddAddress.visibility = View.GONE
                    } else {
                        bd.noDataToDisplay.visibility = View.GONE
                        bd.rvSavedAddress.visibility = View.VISIBLE
                        bd.btnAddAddress.visibility = View.VISIBLE
                        mAdapter.updateList(it)

                    }
                }
            })
    }

    override fun editAddress(address: AddressDTO) {
        val intent = Intent(this, LocationInfoActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.ADDRESS, address)
        startActivity(intent)
    }

    override fun deleteRemoteAddress(addressId: String) {
        vm.deleteAddress(addressId).observe(this, Observer { result ->
            if (result.type == -1) return@Observer
            if (result.type == APIConstant.Status.SUCCESS) {
                when (result.code) {
                    1000 -> {
                        val selectedAddress = sharedPrefsHelper.getSelectedAddress()
                        if (selectedAddress != null && selectedAddress.addressId == addressId) sharedPrefsHelper.setSelectedAddress(
                            null
                        )
                        AlerterHelper.showToast(
                            this,
                            result.title ?: "Error",
                            result.message ?: "Unable to delete"
                        )
                        onResume()
                    }
                }
            } else {
                AlerterHelper.showToast(
                    this,
                    result.title ?: "Error",
                    result.message ?: "Unable to Process"
                )
            }
//            RHandler<Any>(this, result) {
//                val selectedAddress = sharedPrefsHelper.getSelectedAddress()
//                if (selectedAddress != null && selectedAddress.addressId == addressId) sharedPrefsHelper.setSelectedAddress(
//                    null
//                )
//            }

        })
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        when (actionId) {
            IConstants.ActionModule.CONFIRM_LOCATION -> fetchAddressList()
        }
    }

    override fun onResponse(code: Int?, response: Any?) {
        super.onResponse(code, response)

//        when (code) {
//            1000 -> {
//                showToast(response as String); fetchAddressList(); }
//
//        }

    }

    override fun onDestroy() {
        super.onDestroy()
        vm.detachView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode == Activity.RESULT_OK && requestCode == IConstants.LocationPickerModule.REQ_CODE -> {
                showToast(getString(R.string.location_updated))
                fetchAddressList()
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_add_address ->
                addNewAddress(view)

        }
    }

    private fun addNewAddress(view: View) {
        AppIntentUtil.launchWithSheetAnimation(
//            view,
            this,
            Intent(this, LocationSearchActivity::class.java)
        )
    }


    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {
            bd.btnAddAddress.visibility = View.GONE
            bd.noDataToDisplay.visibility = View.VISIBLE
            showSnackBar("No Internet, Unable to connect", Color.RED)

            /*bd.icvNoInternet.visibility = View.VISIBLE
            bd.icvNoInternet.setInfoHeader(IConstants.OrderTracking.NO_INTERNET)
            bd.icvNoInternet.setInfoSubHeader(IConstants.OrderTracking.NO_INTERNET_MSG)
            bd.icvNoInternet.setInfoImage(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_internet,
                    this.theme
                )
            )*/
        }
    }

    override fun onDetailsCard(tag: Any) {

    }

    override fun onAction(tag: Any) {
        if (tag is AddressDTO)
            editAddress(tag)
    }

    override fun onRightIcon(tag: Any) {
        if (tag is AddressDTO)
            confirmAction(tag)

    }

    private fun confirmAction(tag: AddressDTO) {
        val sMessage = getString(R.string.delete_address)
//        +
//        "\n${tag.houseNum}, ${if (!tag.landmark.isBlank()) tag.landmark + ", " else ""}${tag.fullAddress}"
//        sMessage += sMessage
        ActionModuleHandler.showChoseActionDialog(
            this,
            IConstants.ActionModule.CONFIRM_DIALOG,
            "Delete Address",
            sMessage,
            arrayListOf(
                ActionItemDTO(
                    key = ActionConstant.POSITIVE,
                    text = getString(R.string.delete),
                    buttonColor = null,
                    textColor = ThemeConstant.pinkies
                ),
                ActionItemDTO(
                    key = ActionConstant.CLOSE,
                    text = getString(R.string.cancel),
                    buttonColor = null,
                    textColor = ThemeConstant.textBlackColor
                ),
            ),

            object : ActionDialog.OnActionPerformed {
                override fun onSelectedAction(
                    dialog: Dialog?,
                    actionType: Int,
                    action: ActionItemDTO?
                ) {
                    if (action != null && action.key == ActionConstant.POSITIVE)
                        deleteRemoteAddress(tag.addressId)
                }


            },
            "\ue0d3"
        )
    }
}
