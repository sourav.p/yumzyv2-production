package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.laalsa.laalsalib.ui.VUtil;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.yumzy.orderfood.util.ViewConst;


/**
 * Created by Bhupendra Kumar Sahu
 */
public class InfoCommonView extends LinearLayout {
//        implements View.OnClickListener {

    public String infoSubHeader;
    public String infoHeader;
    public Boolean isShow;
    public Drawable infoImage;
    public int ivSize = 0, bigsize = 1, smallsize = 2;
    //public InfoViewListener infoViewListener;

    public InfoCommonView(Context context) {
        this(context, null);
    }

    public InfoCommonView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InfoCommonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (infoImage == null) {
        }
        if (infoHeader == null) {
            infoHeader = "";
        }
        if (infoSubHeader == null) {
            infoSubHeader = "";
        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.InfoCommonView, defStyleAttr, 0);
        infoImage = ta.getDrawable(R.styleable.InfoCommonView_info_image);
        infoHeader = ta.getString(R.styleable.InfoCommonView_info_header);
        infoSubHeader = ta.getString(R.styleable.InfoCommonView_info_sub_header);
        isShow = ta.getBoolean(R.styleable.InfoCommonView_isShow, false);
        ivSize = ta.getInt(R.styleable.InfoCommonView_info_image_size, VUtil.dpToPx(180));
        bigsize = ta.getInt(R.styleable.InfoCommonView_info_header_big_size, 0);
        smallsize = ta.getInt(R.styleable.InfoCommonView_info_header_small_size, 0);

        ta.recycle();
    }

    private void init() {
        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER);
        this.setBackgroundColor(ThemeConstant.white);

        ImageView infoImageView = new ImageView(getContext());
        infoImageView.setId(R.id.iv_info);
        //  infoImageView.setOnClickListener(this);
        infoImageView.setImageDrawable(infoImage);

        infoImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        int ivpx = VUtil.dpToPx(ivSize);
        this.addView(infoImageView, new LayoutParams(ViewConst.MATCH_PARENT, ivpx));
        this.addView(getTextView(R.id.tv_info_header, infoHeader));
        this.addView(getTextView(R.id.tv_info_sub_header, infoSubHeader));
//        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.Yumzy_Button);
//        ExtendedFloatingActionButton btn = new ExtendedFloatingActionButton(getContext());
//        IconFontDrawable iconFontDrawable = new IconFontDrawable(getContext(), R.string.icon_002_noodles);
//        iconFontDrawable.setTextColor(Color.WHITE);
//        iconFontDrawable.setTextSize(24);
//        btn.setIcon(iconFontDrawable);
        MaterialButton btn = new MaterialButton(getContext());
        LayoutParams params = new LayoutParams(
                ViewConst.WRAP_CONTENT,
                ViewConst.WRAP_CONTENT
        );
        params.setMargins(0, VUtil.dpToPx(25), 0, 0);
        btn.setLayoutParams(params);
        btn.setId(R.id.btn_browse);
        btn.setText(R.string.browse_restaurants);
        btn.setAllCaps(false);
        if (isShow) {
            btn.setVisibility(VISIBLE);
        } else btn.setVisibility(GONE);
        btn.setGravity(Gravity.CENTER);
        btn.setTextColor(Color.WHITE);
        btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.pinkies)));
        btn.setRippleColor(ColorStateList.valueOf(ThemeConstant.rippleColor));
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.poppins_semibold);
        btn.setTypeface(typeface);


        this.addView(btn);
    }

    private View getTextView(int tvId, String msg) {
        MaterialTextView textView = new MaterialTextView(getContext());
        //textView.setOnClickListener(this);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setText(msg);

        int i5px = VUtil.dpToPx(5);
        int i15px = VUtil.dpToPx(15);
        textView.setId(tvId);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(i5px, i5px, i5px, 0);
        if (tvId == R.id.tv_info_header) {

            textView.setTextColor(getContext().getResources().getColor(R.color.colorLightBlack));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, bigsize);
            textView.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Headline6);
//
//            Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.inter_bold);
//            textView.setTypeface(typeface);
            textView.setTextColor(Color.BLACK);
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, smallsize);
            textView.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Caption);

            //  YumUtil.INSTANCE.setInterFontRegular(getContext(), textView);

            //textView.setPadding(tv40px, i5px, tv40px, tv15px);
            // textView.setId(R.id.tv_info_sub_header);
        }
        return textView;
    }

    public String getInfoSubHeader() {
        return infoSubHeader;
    }

    public void setInfoSubHeader(String infoSubHeader) {
        this.infoSubHeader = infoSubHeader;
        ((TextView) findViewById(R.id.tv_info_sub_header)).setText(this.infoSubHeader);
    }

    public String getInfoHeader() {
        return infoHeader;
    }

    public void setInfoHeader(String infoHeader) {
        this.infoHeader = infoHeader;
        ((TextView) findViewById(R.id.tv_info_header)).setText(this.infoHeader);
    }

    public Drawable getInfoImage() {
        return infoImage;
    }

    public void setInfoImage(Drawable infoImage) {
        this.infoImage = infoImage;
        ((ImageView) findViewById(R.id.iv_info)).setImageDrawable(infoImage);

    }

    public int getIvSize() {
        return ivSize;
    }

    public void setIvSize(int ivSize) {
        this.ivSize = ivSize;
        findViewById(R.id.iv_info).setMinimumHeight(ivSize);

    }

    public int getBigsize() {
        return bigsize;
    }

    public void setBigsize(int bigsize) {
        this.bigsize = bigsize;
        ((TextView) findViewById(R.id.tv_info_header)).setTextSize(bigsize);
    }

    public MaterialButton getButton() {
        return this.findViewById(R.id.btn_browse);
    }

    public Boolean getShow() {
        return isShow;
    }

    public void setShow(Boolean show) {
        isShow = show;
        if (isShow) {
            findViewById(R.id.btn_browse).setVisibility(VISIBLE);
        } else findViewById(R.id.btn_browse).setVisibility(GONE);
    }


    public void setSmallsize(int smallsize) {
        this.smallsize = smallsize;
        ((TextView) findViewById(R.id.tv_info_sub_header)).setTextSize(this.smallsize);

    }
}
