package com.yumzy.orderfood.ui.screens.scratchcard

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.mikepenz.fastadapter.ClickListener
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.data.models.ScratchCardDetailDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.ActivityScratchCardBinding
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseActivity
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.fastitems.BottomSheetListItem
import com.yumzy.orderfood.ui.screens.fastitems.LoadingItem
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.intentShareText
import net.danlew.android.joda.JodaTimeAndroid
import org.jetbrains.anko.collections.forEachWithIndex
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class ScratchCardActivity : BaseActivity<ActivityScratchCardBinding, ScratchCardViewModel>(),
    IScratchCardView, ClickListener<GenericItem> {
    private var listReachedEnd: Boolean = false

    private var skip: Int = 0
    private var limit: Int = 16
    var itemLoading = false

    //save our FastAdapter
    private lateinit var mFastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter

    //endless scroll
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    
    override val vm: ScratchCardViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bindView(R.layout.activity_scratch_card)
        JodaTimeAndroid.init(this)

        vm.attachView(this)
        setSupportActionBar(bd.toolbar)
        supportActionBar?.title = resources.getString(R.string.scratch_card)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        BarUtils.setStatusBarLightMode(this, false)
        BarUtils.setStatusBarColor(this, ThemeConstant.transparent)
        getScratchCardDetails()
        setupPagingRecyclerView()
        //  initComponent()

    }

    private fun initComponent() {
        bd.appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
//                val min_height = ViewCompat.getMinimumHeight(bd.collapsingToolbar) * 2
                val min_height = 150.px * 2
                val scale = (min_height + verticalOffset).toFloat() / min_height
                bd.cardview.scaleX = if (scale >= 0f) scale else 0f
                bd.cardview.scaleY = if (scale >= 0f) scale else 0f
            })
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override val enableEdgeToEdge: Boolean = true
    override val navigationPadding: Boolean = true
    private fun setupPagingRecyclerView() {
        skip = 0
        listReachedEnd = false
        mFastAdapter = FastItemAdapter()
        val selectExtension = mFastAdapter.getSelectExtension()
        selectExtension.isSelectable = true
        footerAdapter = ItemAdapter.items()
        mFastAdapter.addAdapter(1, footerAdapter)
        bd.rvScratchCard.layoutManager = GridLayoutManager(this, 2).apply {
            this.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (mFastAdapter.getItemViewType(position)) {
                        R.id.scratch_card_details -> 1
                        else -> 2
                    }
                }
            }
        }
        bd.rvScratchCard.adapter = mFastAdapter
//        bd.rvScratchCard.itemAnimator = DefaultItemAnimator()
//        bd.rvScratchCard.setAdapter(FastAdapter.wrap(fastItemAdapter));
        mFastAdapter.onClickListener = this
        mFastAdapter.attachDefaultListeners = true
        endlessRecyclerOnScrollListener = object : EndlessRecyclerOnScrollListener(footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                footerAdapter.clear()
                val progressItem = LoadingItem()
                progressItem.isEnabled = false
                footerAdapter.add(progressItem)
                fetchScratchCardList()
            }
        }

        bd.rvScratchCard.addOnScrollListener(endlessRecyclerOnScrollListener)

        fetchScratchCardList()
    }


    override fun getScratchCardDetails() {
        vm.getScratchCardDetails().observe(this, { result ->
            if (result.type == -1) {
//                footerAdapter.clear()
//                val progressItem = ListLoadingItem()
//                progressItem.isEnabled = false
//                footerAdapter.add(progressItem)
                return@observe
            }
            RHandler<ScratchCardDetailDTO>(
                this,
                result
            ) { it ->
                bd.scratchDetails = it.scratchedAmount.toInt().toString()

                it.banner.also {
                    if (it.title.isNotEmpty()) {
                        bd.cardTitle.text =
                            HtmlCompat.fromHtml(it.title, HtmlCompat.FROM_HTML_MODE_COMPACT)
                        bd.cardview.visibility = View.VISIBLE

                    } else {
                        bd.cardSubtitle.visibility = View.GONE
                    }
                    if (it.description.isNotEmpty()) {
                        bd.cardSubtitle.text =
                            HtmlCompat.fromHtml(it.description, HtmlCompat.FROM_HTML_MODE_COMPACT)
                        bd.cardview.visibility = View.VISIBLE

                    } /*else {
                        bd.cardSubtitle.visibility = View.GONE
                    }*/

                }
            }
        })
    }

    private fun noScratchCardLayout(emptyList: Boolean) {

        if (emptyList) {
            bd.appBarLayout.setExpanded(false)
            bd.rvScratchCard.visibility = View.GONE
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.noDataToDisplay.actionCallBack = shareInviteAction

        } else {
            bd.rvScratchCard.visibility = View.VISIBLE
            bd.noDataToDisplay.visibility = View.GONE
        }


    }

    private val shareInviteAction: (view: View) -> Unit = { view ->
        var inviteText = IConstants.AppConstant.INVITE_TEXT
        if (inviteText != "") {
            if (inviteText.contains("refer_code")) {
                inviteText = inviteText.replace(
                    "refer_code",
                    sharedPrefsHelper.getUserReferralCode(),
                    ignoreCase = false
                )
            }

            if (inviteText.contains("refer_link")) {
                inviteText = inviteText.replace(
                    "refer_link",
                    sharedPrefsHelper.getUserReferralLink() ?: "",
                    ignoreCase = false
                )
            }

            if (inviteText.contains("new_line")) {
                inviteText = inviteText.replace(
                    "new_line",
                    "\n",
                    ignoreCase = false
                )
            }

        } else {
            inviteText =
                resources.getString(R.string.invite_text) + "Referral Code: " + sharedPrefsHelper.getUserReferralCode() + "\n" + sharedPrefsHelper.getUserReferralLink()
        }

        /*   CleverTapHelper.referralLink(
                        sharedPrefsHelper.getUserName(),
                        sharedPrefsHelper.getUserPhone(),
                        sharedPrefsHelper.getUserReferralCode()
                    )*/
        intentShareText(this, inviteText)
    }

    override fun redeemScratchCard(scratchCard: ScratchCardDTO) {
        val userPhone = sharedPrefsHelper.getUserPhone()
        vm.redeemScratchCard(scratchCard, userPhone).observe(this, {
            if (it.type == APIConstant.Status.SUCCESS) {
                updateAdapterItem(scratchCard)
                getScratchCardDetails()
            }
        })
    }

    private fun updateAdapterItem(scratchCard: ScratchCardDTO) {
        for (itemIndex in 0 until mFastAdapter.itemCount) {
            val holder = mFastAdapter.getItem(itemIndex)
            when (holder) {
                is ScratchCardItem -> {
                    if (holder.model.scratchID.equals(scratchCard.scratchID)) {
                        if (holder.model.scratched == true) return
                        scratchCard.scratched = true
                        holder.model = scratchCard
                        if (holder.model.amount.toInt() <= 0 && holder.model.action.url.isEmpty() == true) {
                            mFastAdapter.remove(itemIndex)
                        }
                        mFastAdapter.notifyAdapterItemChanged(itemIndex)
//                        bd.rvScratchCard.scrollToPosition(bd.rvScratchCard.)
                        if (mFastAdapter.itemCount == 0) {
                            noScratchCardLayout(true)
                        } else {
                            bd.rvScratchCard.layoutManager?.scrollToPosition(
                                if (itemIndex > -1 && itemIndex < mFastAdapter.itemCount) {
                                    //case: between head and tail of list
                                    itemIndex
                                } else if (itemIndex > mFastAdapter.itemCount - 1) {
                                    //case: end case for list tail
                                    mFastAdapter.itemCount - 1
                                } else {
                                    //case: start case for list head
                                    0
                                }
                            )
                        }
                        break
                    }
                }
            }
        }
    }

    override fun fetchScratchCardList() {
        if (listReachedEnd) {
            footerAdapter.clear()
            footerAdapter.add(BottomSheetListItem())
            return
        }
        /*if (listReachedEnd) {
            footerAdapter.clear()
            val progressItem = ListLoadingItem()
            progressItem.isEnabled = false
            footerAdapter.add(progressItem)
            footerAdapter.add(BottomSheetListItem())
            return
        }*/
//        if (itemLoading) {
//            return
//        }
        vm.getScratchCardsList(skip, limit).observe(
            this,
            Observer { result ->
                itemLoading = true
                if (result.type == APIConstant.Status.LOADING) {
                    footerAdapter.clear()
                    val progressItem = LoadingItem()
                    progressItem.isEnabled = false
                    footerAdapter.add(progressItem)
                    return@Observer
                }

                RHandler<List<ScratchCardDTO>>(
                    this,
                    result
                ) { scratchCard ->
                    footerAdapter.clear()
                    if (skip == 0 && scratchCard.isNullOrEmpty()) {
                        noScratchCardLayout(scratchCard.isNullOrEmpty())
                        listReachedEnd = true
                        footerAdapter.add(BottomSheetListItem())
                        vm.getScratchCardsList(skip, limit)
                            .removeObservers(this)
                    }

                    val items = ArrayList<ScratchCardItem>()
                    scratchCard.forEachWithIndex { i, listElement ->
                        listElement.position = i
                        val topOutletItem = ScratchCardItem(listElement)
                        items.add(topOutletItem)
                    }
                    mFastAdapter.add(items)
                    skip++

                }

            })

    }

    private fun showScratchCard(view: View?, cardDto: ScratchCardDTO?, drawableId: Int) {
        if (cardDto == null) return
        val scratchCard =
            if (cardDto.afterEarnBy.isNullOrEmpty() || cardDto.beforeEarnBy.isNullOrEmpty()) {
                val selectedCard =
                    if (!cardDto.earnBy.isNullOrEmpty()) {
                        cardDto.copy(
                            beforeEarnBy = cardDto.earnBy,
                            afterEarnBy = cardDto.earnBy
                        )
                        cardDto
                    } else {
                        cardDto
                    }
                selectedCard
            } else {

                cardDto
            }
        val intent = Intent(this, ScratchDetailsActivity::class.java)
        intent.putExtra("scratchCard", scratchCard)
        intent.putExtra("scratchDrawableId", drawableId)
        intent.putExtra("referLink", sharedPrefsHelper.getUserReferralLink())
        intent.putExtra("refer_code", sharedPrefsHelper.getUserReferralCode())
//        cardScratchState.launch(intent)
        AppIntentUtil.launchForResultWithSheetAnimation(this, intent) { resultOk, result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val scratchCardDTO = result.data?.getParcelableExtra<ScratchCardDTO>("scratchCard")
                if (scratchCardDTO != null && !scratchCardDTO.scratched) {
                    redeemScratchCard(scratchCardDTO)
                }
                setResult(Activity.RESULT_OK)
            }
            cardOpened = false

        }


    }

    var cardOpened = false
    override fun invoke(
        v: View?,
        adapter: IAdapter<IItem<out RecyclerView.ViewHolder>>,
        item: IItem<out RecyclerView.ViewHolder>,
        position: Int
    ): Boolean {
        if (item is ScratchCardItem) {
            if (cardOpened) return true
            cardOpened = true
            showScratchCard(
                v,
                item.model,
                drawableList[item.model.position.rem(drawableList.size)]
//                item.drawableList[item.scratchIndex.rem(item.drawableList.size)]
            )
            return false
        }
        return false
    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {
            bd.scratchDetails = "0.00"
            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.appBarLayout.setExpanded(false)

        }
    }
}
