package com.yumzy.orderfood.ui.screens.module.address

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.DialogAddAddressBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.util.IConstants


class AddAddressDialog : BaseDialogFragment<DialogAddAddressBinding>() {

//    
//    public lateinit var couponViewModel: CouponViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        couponViewModel.attachView(this)
        applyDebouchingClickListener(bd.btnLocationAccess, bd.btnManually)
    }

    override fun onSuccess(actionId: Int, data: Any?) {
        actionHandler?.onSuccess(actionId, data)
    }


    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_location_access -> {
                /*val intent = Intent(context, LocationPickerActivity::class.java)
                (context as AppCompatActivity).startActivityForResult(
                    intent,
                    IConstants.ActivitiesFlags.ADDRESS_PICKER_REQUEST

                )*/
            }
            R.id.btn_manually -> {
                actionHandler?.onSuccess(
                    actionId = moduleId,
                    data = IConstants.ManagerAddress.UPDATE_MANUALLY
                )
            }


        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        actionHandler?.onSuccess(
            actionId = IConstants.ManagerAddress.CLOSE_DIALOG,
            data = null
        )
    }

    override fun getLayoutId(): Int = R.layout.dialog_add_address

    override fun onDialogReady(view: View) {

    }
}
