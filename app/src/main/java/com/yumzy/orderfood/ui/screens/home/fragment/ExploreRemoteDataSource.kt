package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.ExploreService
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class ExploreRemoteDataSource  constructor(private val service: ExploreService) :
    BaseDataSource() {
    //suspend fun getExplorePosts() = getResult { service.getExplore() }
}
