package com.yumzy.orderfood.ui.screens.payment

import android.content.Intent
import com.yumzy.orderfood.data.models.NewOrderReqDTO
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IWebPayment : IModuleHandler {

//    fun setupActionBar()
    fun getIntentData(intent: Intent?)
    fun newOrder(mOrder: NewOrderReqDTO)
    fun postCheckout(orderId: String)

}