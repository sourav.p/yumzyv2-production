package com.yumzy.orderfood.ui.screens.invite.data

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.models.ContactModelDTO
import javax.inject.Inject

class InviteRemoteDataSource  constructor(private val homeServiceAPI: HomeServiceAPI) :
    BaseDataSource() {
    suspend fun profileUser() = getResult {
        homeServiceAPI.profileUser()
    }

    suspend fun saveUserContacts(userPhoneNumber: String, contacts: List<ContactModelDTO>) = getResult {
        val body = mapOf<String, Any>(
            "mobile" to userPhoneNumber,
            "contacts" to contacts
        )
        homeServiceAPI.saveUserContacts(body)
    }
}