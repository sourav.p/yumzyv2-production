package com.yumzy.orderfood.ui.screens.rewards.data

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.worker.databaseLiveData
import com.yumzy.orderfood.worker.networkLiveData
import javax.inject.Inject

class RewardsRepository  constructor(
    private val userInfoDao: UserInfoDao,
    private val remoteSource: RewardsRemoteDataSource
) {

    fun applyReferral(userId: String, referralCode: String) = networkLiveData(
        networkCall = {
            remoteSource.applyReferral(
                userId,
                mapOf(
                    "referralCode" to referralCode
                )
            )
        }
    ).distinctUntilChanged()

    fun getUserDTO() = databaseLiveData {
        userInfoDao.getUser()
    }

    fun getScratchCardInfo() = networkLiveData(
        networkCall = {
            remoteSource.getScratchCardInfo()
        }
    )

}