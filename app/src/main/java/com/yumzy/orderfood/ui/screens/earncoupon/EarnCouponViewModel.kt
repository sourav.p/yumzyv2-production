package com.yumzy.orderfood.ui.screens.earncoupon

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.earncoupon.data.EarnCouponRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class EarnCouponViewModel  constructor(
    private val repository: EarnCouponRepository
) : BaseViewModel<IEarnCoupon>() {
    fun getFetchCouponList(userID: String, cartId: String?, outletId: String?) =
        repository.getFetchCouponList(userID, cartId, outletId)

}