package com.yumzy.orderfood.ui.screens.module.dishes

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.ClearCartReqDTO
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.dishes.data.TopDishesRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu on 30-May-20.
 */
class TopDishesViewModel  constructor(
    private val repository: TopDishesRepository,
    sharedPrefsHelper: SharedPrefsHelper
) :
    BaseViewModel<IView>() {

    //    fun getTopDishes(promoID: String, skip: Int) = repository.getTopDishes(promoID, skip)
    fun addToCart(addCart: NewAddItemDTO) = repository.addToCart(addCart)
    var cart = repository.getCart(sharedPrefsHelper.getCartId() ?: "") as MutableLiveData
    fun clearCart(cartId: ClearCartReqDTO) = repository.clearCart(cartId)

    fun getTopDishes(promoID: String, skip: Int, limit: Int) =
        repository.getTopDishes(promoID, skip, limit)

    fun itemCount(
        position: Int,
        count: Int,
        itemcount: ItemCountDTO
    ) = repository.itemCount(position, count, itemcount)

}