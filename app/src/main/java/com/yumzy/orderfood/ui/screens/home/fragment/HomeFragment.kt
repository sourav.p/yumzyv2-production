package com.yumzy.orderfood.ui.screens.home.fragment

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.data.models.HomeSectionDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.databinding.FragmentHomeBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.location.LocationSearchActivity
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.new.home.HomeBottomNavFragmentDirections
import com.yumzy.orderfood.ui.screens.new.location.LocationSearchFragmentDirections
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.adapter.OutletInteraction
import com.yumzy.orderfood.ui.screens.wallet.WalletActivity
import com.yumzy.orderfood.ui.screens.web.WebActivity
import com.yumzy.orderfood.util.IConstants
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.net.URL
import java.util.*

var globalHomeTitleListener: ((view: View, HomeListDTO) -> Unit)? = null
var globalHomeItemListener: ((view: View, HomeListDTO) -> Unit)? = null

class HomeFragment  constructor() : BaseFragment<FragmentHomeBinding, HomeViewModel>(),
    IHomeView, OutletInteraction {
    private var longitude: String = ""
    private var latitude: String = ""
    private var homeList: ArrayList<HomeSectionDTO> = ArrayList()
    private var isNewUser: Boolean = false


    private val activity by lazy {
        requireActivity() as AppCompatActivity
    }

    private val navController by lazy {
        findNavController()
    }


    override val vm: HomeViewModel by sharedViewModel()
    val cartHelper: CartHelper by inject()

    var userId: String = ""
    var cartId: String? = ""
    var outletId: String = ""

    override fun onFragmentReady(view: View) {
        vm.attachView(this)
        /*
        view.findViewById<BottomNavigationView>(R.id.bottom_nav_view).visibility =
            View.VISIBLE*/
        ViewCompat.setElevation(bd.constraintLayout2, 6f)
        sharedPrefsHelper.putSelectedAddress("")
        applyDebouchingClickListener(
            bd.ivLocation,
            bd.ivOfferLogo,
            bd.labelLocation,
            bd.tvCurrentLocation,
            bd.tvHomeOffer
        )
        listenDeliveryAddressChange()
        setHomeRecyclerValue()
        showProgressBar()
        if (sharedPrefsHelper.getIsTemporary()) {
            bd.tvHomeOffer.visibility = View.GONE
            bd.ivOfferLogo.visibility = View.GONE
        }
//        bd.moveTop.setOnClickListener { bd.homeRecyclerView.scrollToPosition(0) }
    }

    private fun listenDeliveryAddressChange() {
        AppGlobalObjects.getAddressLiveData.observe(this, {
            if (it != null) {
                bd.labelLocation.text = it.name
                bd.tvCurrentLocation.text =
                    if (it.shortAddress.trim().isEmpty()) it.fullAddress else it.shortAddress

                val latitude = it.longLat?.coordinates?.get(1).toString()
                val longitude = it.longLat?.coordinates?.get(0).toString()
                if (this.latitude != latitude || this.longitude != longitude) {
                    this.latitude = latitude
                    this.longitude = longitude
                    bd.homeRecyclerView.fetchHomeList(latitude, longitude)
                }
            }
        })
    }

    private fun setHomeRecyclerValue() {
        bd.noDataToDisplay.actionCallBack = {
            AppIntentUtil.launchWithSheetAnimation(
//                    bd.labelLocation,
                requireActivity(),
                LocationSearchActivity::class.java
            )
        }
        bd.homeRecyclerView.setViewModel(requireActivity() as AppCompatActivity, vm, this)
        bd.homeRecyclerView.recyclerScrollListener = { scrolledTop, recyclerView, _, _ ->
            bd.constraintLayout2.isSelected = scrolledTop
            /*val scrollPosition: Int =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (scrollPosition > 10) {
                bd.moveTop.visibility = View.VISIBLE
            } else
                bd.moveTop.visibility = View.GONE*/

        }

        globalHomeTitleListener = { _, homeItem ->
            handleSectionTitle(title = homeItem.heading, homeItem = homeItem)
        }
        globalHomeItemListener = { view, homeItem ->
            handleHomeItemClicked(view, homeItem.title, homeItem, homeItem.flatLayoutType)
        }


    }

    override fun getLayoutId() = R.layout.fragment_home

    private fun handleSectionTitle(
        title: String? = "Order Now", /*"Got Hungry, Order on Yumzy"*/
        homeItem: HomeListDTO?,
    ) {
        homeItem?.let {
            val promoId = it.firstPromoId
            val imageUrl = it.heroImage?.url ?: ""  // homeItemForPromo?.image?.url ?:

            when (it.landingPage) {
                IConstants.LandingPageType.DISHES, IConstants.LandingPageType.DISH -> {
                    ActionModuleHandler.showTopDishesDialog(
                        context as AppCompatActivity,
                        promoId ?: "",
                        title ?: "Dishes",
                        null,
                        homeItem.subLayoutType
                    )
                }
                IConstants.LandingPageType.RESTAURANTS, IConstants.LandingPageType.RESTAURANT -> {
                    ActionModuleHandler.showTopRestaurantDialog(
                        context as AppCompatActivity,
                        promoId,
                        title ?: "Order Now",
                        imageUrl,
                        null
                    )
                }
            }
//            CleverTapHelper.seeMoreClicked(title, layoutID, it.promoId ?: "")

        }
    }

    override fun fetchHomeData(latitude: String, longitude: String) {


//        vm.homeList.invoke(latitude, longitude, 0, 50)


        /*bd.scrollView.visibility = View.VISIBLE
        bd.noDataToDisplay.visibility = View.GONE
        homeList.clear()
        hasMoreData = true
        skip = 0
        bd.homeContent.removeAllViews()
        loadingMore = false
        vm.getHomeResponse(
            latitude, longitude
//            "17.4311184","78.3722143"
            , skip, limit
        ).observe(this, Observer {
            bd.swipeRefreshLayout.isRefreshing = false
            RHandler<ArrayList<HomeSectionDTO>>(this, it) { result ->
                homeList = result
                if (homeList.isEmpty()) {
                    //   bd.homeContent.addView(UIHelper.showNoDataToDisplay(requireContext()))
                } else {
                    CleverTapHelper.onAppStarted(
                        it.message ?: "success",
                        globalAddress?.googleLocation ?: "unknown",
                        homeList.size,
                        it.type ?: 200
                    )
                    for (homeSection in homeList) {
                        val buildHeadingData =
                            buildHeadingData(
                                homeSection.layoutType,
                                homeSection.list[0],
                                homeSection.heading,
                                homeSection.subHeading,
                                homeSection.list.size
                            )
                        if (buildHeadingData != null)
                            bd.homeContent.addView(buildHeadingData)
                        bd.homeContent.addView(
                            buildHomeData(
                                homeSection.heading,
                                homeSection.list,
                                homeSection.layoutType,
                                homeSection.orientation
                            )
                        )
                    }
                }
                (activity as HomePageActivity).let {
                    it.mUserCurrentLocationState = IConstants.LocationStatus.SERVING_AT_LOCATION
                    it.vm.locationDialogState.value = it.mUserCurrentLocationState
                }
            }
        })

*/
    }


    override fun onResume() {
        super.onResume()
        val latitude = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(1).toString()
        val longitude = AppGlobalObjects.selectedAddress?.longLat?.coordinates?.get(0).toString()
        if (this.latitude != latitude || this.longitude != longitude) {
            this.latitude = latitude
            this.longitude = longitude
            bd.homeRecyclerView.fetchHomeList(latitude, longitude)
//            bd.homeRecyclerView.fetchHomeList(this.latitude,this.longitude)
        }
    }


    private fun handleHomeItemClicked(
        view: View?,
        sectionHeading: String,
        homeItem: HomeListDTO,
        layoutType: Int,
        count: Int = -1
    ) {
        val requireActivity = requireActivity()
        if (count == -2) {
            handleSectionTitle(sectionHeading, homeItem)
        } else {
            when (homeItem.landingPage) {
                IConstants.LandingPageType.RESTAURANT -> {
//                    val action = HomeBottomNavFragmentDirections.actionFragmentHomeToFragmentOutlet( homeItem.outletId,"")
//                    navController.navigate(action)
                    val bundle = Bundle()
                    bundle.putString("outletId", homeItem.outletId)
                    bundle.putString("itemId", null)
                    navController.setGraph(R.navigation.nav_outlet, bundle)
//                    val intent = Intent(context, OutletActivity::class.java)
//                    intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, homeItem.outletId)
//                    AppIntentUtil.launchForResultWithSheetAnimation(
//                        requireActivity as AppCompatActivity, intent
//                    ) { resultOk, _ ->
//                        if (resultOk) {
//                            if (requireActivity is HomePageActivity) {
//                                requireActivity.navigateToFragment(R.id.nav_plate, true)
//                            }
//                        }v
//
//                    }


                    CleverTapHelper.seePromoClicked(
                        sectionHeading,
                        homeItem,
                        layoutType,
                        null
                    )

                }
                IConstants.LandingPageType.RESTAURANTS -> {
                    val imageUrl = homeItem.heroImage?.url ?: ""
//                    if (homeItem.promoId == null) {
//                        showToast("Sorry unable to display right now")
//                        return
//                    }
                    ActionModuleHandler.showTopRestaurantDialog(
                        context as AppCompatActivity,
                        homeItem.promoId,
                        homeItem.title,
                        imageUrl,
                        null
                    )
                    CleverTapHelper.seePromoClicked(
                        sectionHeading,
                        homeItem,
                        layoutType,
                        null
                    )

                }
                IConstants.LandingPageType.DISH -> {
                    val intent = Intent(context, OutletActivity::class.java)
                    intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, homeItem.outletId)
                    intent.putExtra(IConstants.ActivitiesFlags.ITEM_ID, homeItem.dishId)
//                    startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
                    AppIntentUtil.launchForResultWithSheetAnimation(
                        requireActivity as AppCompatActivity, intent
                    ) { resultOk, _ ->
                        if (resultOk) {
                            if (requireActivity is HomePageActivity) {
                                requireActivity.navigateToFragment(R.id.nav_plate, true)
                            }
                        }

                    }


                }
                IConstants.LandingPageType.DISHES -> {
                    ActionModuleHandler.showTopDishesDialog(
                        context as AppCompatActivity,
                        homeItem.promoId ?: "",
                        homeItem.title,
                        null,
                        homeItem.subLayoutType
                    )
                    CleverTapHelper.seePromoClicked(
                        sectionHeading,
                        homeItem,
                        layoutType,
                        homeItem.meta?.itemName ?: ""
                    )
                }
                IConstants.LandingPageType.URL -> {
                    val url = URL(homeItem.meta?.clickUrl ?: "")
                    val scheme = url.protocol
                    val host = url.host
                    val path = url.path
                    val query = url.query
                    val webUrl =
                        "$scheme://$host$path?$query&userId=${sharedPrefsHelper.getUserId()}&x-access-token=${sessionManager.fetchAuthToken()}&promoId=${homeItem.promoId}&platform=android"
                    val intent = Intent(context, WebActivity::class.java)
                    intent.putExtra(IConstants.ActivitiesFlags.URL, webUrl)
                    AppIntentUtil.launchForResultWithBaseAnimation(
                        view,
                        requireActivity as AppCompatActivity, intent
                    ) { resultOk, _ ->
                        if (resultOk) {
                            if (requireActivity is HomePageActivity) {
                                requireActivity.handleNotification()
                            }
                        }

                    }
                    /* (context as HomePageActivity).startActivityForResult(
                         intent,
                         WebActivity.REQ_CODE
                     )*/
                }
                IConstants.LandingPageType.INVITE -> {
                    if (view == null)
                        (context as HomePageActivity).startActivity(
                            Intent(
                                context,
                                InviteActivity::class.java
                            )
                        )
                    else
                        AppIntentUtil.launchWithBaseAnimation(
                            view,
                            requireActivity,
                            InviteActivity::class.java
                        )
                }
            }
        }
    }


    override fun onSuccess(actionId: Int, data: Any?) {
        if (actionId == IConstants.ActionModule.CONFIRM_LOCATION)
            if (data != null) {
                val address = data as AddressDTO
                val longLat = address.longLat
                sharedPrefsHelper.savedCityLocation(address.googleLocation)
                sharedPrefsHelper.saveFullAddress(address.fullAddress)
                sharedPrefsHelper.updateLatitudeLongitude(
                    longLat?.coordinates?.get(1).toString(),
                    longLat?.coordinates?.get(0).toString()
                )
                fetchHomeData(
                    longLat?.coordinates?.get(1).toString(),
                    longLat?.coordinates?.get(0).toString()
                )
            }
    }

    override fun onDebounceClick(view: View) {
        when (view.id) {
            R.id.iv_location, R.id.label_Location, R.id.tv_current_location -> {
                AppIntentUtil.launchWithSheetAnimation(
//                    bd.labelLocation,
                    requireActivity(),
                    LocationSearchActivity::class.java
                )
            }
            R.id.iv_offer_logo, R.id.tv_home_offer -> {
                val intent = Intent(context, WalletActivity::class.java)
                intent.putExtra(
                    IConstants.ActivitiesFlags.FLAG,
                    IConstants.ActivitiesFlags.FROM_HOME
                )
//                startActivityForResult(intent, 0)
                AppIntentUtil.launchWithSheetAnimation(
                    context as AppCompatActivity, intent
                )

            }
            else -> super.onDebounceClick(view)
        }
    }

    override fun onResponse(code: Int?, response: Any?) {
        if (code == 1223) {

        } else if (code == 1224) {

        }

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        CleverTapHelper.onAppStarted(
            message,
            AppGlobalObjects.selectedAddress?.googleLocation ?: "unknown",
            0,
            code ?: APIConstant.Status.ERROR
        )
        if (code == APIConstant.Status.NO_NETWORK) {
            showNoInternetConnection(title, message)
        } else if (code == APIConstant.Code.NO_DELIVERY_PARTNER) {
            showNotDeliverable(title, message)
        } else if (code == APIConstant.Code.NOT_SERVING || code == APIConstant.Code.NO_RESTAURANT_AT_LOCATION) {
            showNotService(title, message)
        } else
            super.showCodeError(code, title, message)
    }

    private fun showNotDeliverable(title: String?, error: String) {
        showErrorCodeView(
            title ?: IConstants.OrderTracking.DELIVERY_UNAVAILABLE,
            error,
            R.drawable.img_deliviry_issue
        )

        (activity as HomePageActivity).let {
            it.mUserCurrentLocationState = IConstants.LocationStatus.NOT_SERVING_AT_LOCATION
            it.vm.locationDialogState.value = it.mUserCurrentLocationState
        }
    }

    private fun showNoInternetConnection(title: String?, error: String) {
        showErrorCodeView(
            title ?: "No Internet",
            error,
            R.drawable.img_no_internet
        )
    }

    override fun onConfirmDone(dialog: Dialog?, actionType: Int, sTransIds: String?) {
    }

    override fun onDismissed(actionType: Int, actionLabel: String?) {
    }

    private fun showNotService(title: String?, error: String) {
        showErrorCodeView(
            title ?: getString(R.string.all_not_serving),
            error,
            R.drawable.img_unserviceable
        )
        (activity as HomePageActivity).let {
            it.mUserCurrentLocationState = IConstants.LocationStatus.NOT_SERVING_AT_LOCATION
            it.vm.locationDialogState.value = it.mUserCurrentLocationState
        }
    }

    private fun showErrorCodeView(title: String, subTitle: String, image: Int) {
        bd.noDataToDisplay.visibility = View.VISIBLE
        bd.noDataToDisplay.title = title
        bd.noDataToDisplay.subTitle = subTitle
        bd.noDataToDisplay.imageResource = image
        bd.homeRecyclerView.clearData()


    }


    override fun onItemCountChange(
        cartHandler: ICartHandler,
        count: Int,
        item: RestaurantItem,
        quantityView: AddQuantityView,
        isIncremented: Boolean?
    ) {
        cartHelper.addOrUpdateItemToCart(cartHandler, count, item, null, isIncremented)
    }

    override fun onItemSelected(position: Int, item: RestaurantItem) {

    }

    override fun gotoPlate() {
        (context as HomePageActivity).navigateToFragment(R.id.nav_plate, true)
    }

    override fun onItemFailed(message: String) {

    }

    override fun adapterSizeChange(itemSize: Int) {

    }

    override fun onNoteClicked(item: RestaurantItem) {

    }
}