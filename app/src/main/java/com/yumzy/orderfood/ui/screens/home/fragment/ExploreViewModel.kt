package com.yumzy.orderfood.ui.screens.home.fragment

import androidx.lifecycle.MutableLiveData
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.home.data.ExploreRepository
import com.yumzy.orderfood.util.IConstants
import javax.inject.Inject

/**
 * The ViewModel for [ExploreFragment].
 */
class ExploreViewModel  constructor(val repository: ExploreRepository) :
    BaseViewModel<IExploreView>() {
    var searchState = MutableLiveData(IConstants.ExploreSuggestion.NO_RESULT)
    var searchText = MutableLiveData("")

    val noteList = repository.listData
    fun getSearchResult(text: String) = repository.getSearchSuggestion(text)

    fun insertSearchItem(localSearchItem: String?) =
        repository.insertSearchItem(localSearchItem)


    fun clearSearchTable() {
        repository.clearSearchTable()
    }

    fun searchExplore(text: String) = repository.getSearchSuggestion(text)
//    fun getExpandSearch(itemId: String) = repository.getExpandSearchResult(itemId)

}
