package com.yumzy.orderfood.ui.screens.module.toprestaurant

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.module.toprestaurant.data.TopRestaurantsRepository
import javax.inject.Inject

class TopRestaurantViewModel  constructor(private val repository: TopRestaurantsRepository) :
    BaseViewModel<IView>() {

    var promoID: String = ""
    var skip: Int = 0
    var limit: Int = 0
    fun getRestaurants(promoID: String, skip: Int, limit: Int) =
        repository.getRestaurants(promoID, skip, limit)

//    val restaurantList= MutableLiveData<>()

}