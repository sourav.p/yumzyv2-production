package com.yumzy.orderfood.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.utilcode.util.ActivityUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.component.chromtab.ChromeTabHelper
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.animation.OrderSuccessAnimActivity
import com.yumzy.orderfood.ui.screens.animation.ScratchAnimActivity
import com.yumzy.orderfood.ui.screens.module.infodialog.OrderSuccessDialog
import com.yumzy.orderfood.ui.screens.templetes.FailedTemplateDialog
import com.yumzy.orderfood.ui.screens.templetes.PopUpGIFDialog
import com.yumzy.orderfood.ui.screens.templetes.PopUpTemplateDialog
import com.yumzy.orderfood.ui.screens.templetes.SuccessTemplateDialog

object AppUIController {
    private val topActivity: AppCompatActivity?
        get() = ActivityUtils.getTopActivity() as AppCompatActivity

/*
    fun showOrderSuccessUI() {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpTemplateDialog(
            topActivity!!,
            R.drawable.img_order_confirm,
            topActivity!!.getString(R.string.your_order_is_processed),
            topActivity!!.getString(R.string.success),
            topActivity!!.getString(R.string.order_update_shortly)

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showScratchCardUI() {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpTemplateDialog(
            topActivity!!,
            R.drawable.ic_scratch_card_yellow,
            "",
            topActivity!!.getString(R.string.congo),
            topActivity!!.getString(R.string.you_earn_scratch)

        )
//        VibrateUtils.vibrate(longArrayOf(125,75,125,275,200,275,125,75,125,275,200,600,200,600),-1)
        VibrateUtils.vibrate(longArrayOf(75, 38, 75, 488, 75, 38, 75, 200, 75, 38, 75, 400), -1)

        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }*/

    fun showOrderFailedUI(title: String, message: String) {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = FailedTemplateDialog(
            topActivity!!,
            ThemeConstant.redRibbonColor,
            title,
            message,
            ""

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    /*fun shoeCartFailedUI(msg: String, error: String) {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = FailedTemplateDialog(
            topActivity!!,
            ThemeConstant.redRibbonColor,
            "",
            msg.capitalize(),
            error.capitalize()

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }*/

    fun showCouponAppliedUI(amountValue: String) {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = SuccessTemplateDialog(
            topActivity!!,
            ThemeConstant.redRibbonColor,
            topActivity!!.getString(R.string.you_saved),
            "₹$amountValue",
            topActivity!!.getString(R.string.congratulation)

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showCouponFailedUI(error: String) {
        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = FailedTemplateDialog(
            topActivity!!,
            ThemeConstant.redRibbonColor,
            topActivity!!.getString(R.string.oh_no),
            error
            //topActivity!!.getString(R.string.failed),
            //topActivity!!.getString(R.string.invalid_coupon)

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showThankYouRate() {

        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpTemplateDialog(
            topActivity!!,
            R.drawable.ic_thank_you,
            "",
            topActivity!!.getString(R.string.thank_you),
            topActivity!!.getString(R.string.feedback_valuable)
        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showFoodPrefUI() {

        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpTemplateDialog(
            topActivity!!,
            R.drawable.ic_thank_you,
            "",
            topActivity!!.getString(R.string.thank_you),
            topActivity!!.getString(R.string.feedback_food_pref),
//            topActivity!!.getString(R.string.thank_you)

        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showApplyREferralUI() {

        if (topActivity == null) return
        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpTemplateDialog(
            topActivity!!,
            R.drawable.img_success,
            "",
            topActivity!!.getString(R.string.congo),
            topActivity!!.getString(R.string.referral_applied)
        )
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun launchUrl(fullUrl: String) {
        if (topActivity == null) return
        ChromeTabHelper.launchChromeTab(topActivity!!, fullUrl)
    }

    fun showWithdrawStatus(status: String, statusDescription: String, flag: Int) {
        if (topActivity == null) return
        var gif: Int = 0
        if (flag == 0)
            gif = R.raw.fail
        if (flag == 1)
            gif = R.raw.success
        if (flag == 2)
            gif = R.raw.waiting

        val detailActionDialog = OrderSuccessDialog()
        val template = PopUpGIFDialog(topActivity!!, gif, status, "", statusDescription)
        detailActionDialog.detailsTemplate = template
        detailActionDialog.showNow(
            topActivity!!.supportFragmentManager,
            ""
        )
    }

    fun showOrderReceived(earnedScratch: Boolean, actionResponse: () -> Any) {
        if (topActivity == null) return
        if (earnedScratch) {
            val intent = Intent(topActivity, ScratchAnimActivity::class.java)
            topActivity!!.startActivity(intent)
        } else {
            val intent = Intent(topActivity, OrderSuccessAnimActivity::class.java)
            topActivity!!.startActivity(intent)
        }
    }
}