package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ItemWebListBinding
import com.yumzy.orderfood.ui.common.recyclermargin.ViewPageMargin
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.home.fragment.WebBannerAdapter
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst

class WebListItem(homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ItemWebListBinding>(homeListDTO) {
    override val type: Int = R.id.web_list_item

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemWebListBinding {
        val inflate = ItemWebListBinding.inflate(inflater, parent, false)
        inflate.title = model.toString()
        inflate.webBannerPage.adapter =
            object :
                WebBannerAdapter<HomeListDTO>(inflate.webBannerPage.context, model.nestedList!!) {
                override fun onItemClick(
                    view: View,
                    item: HomeListDTO,
                    bannerPosition: Int
                ) {
                    if (model.layoutType == IConstants.LayoutType.Web4000.WEB_4001)
                        globalHomeItemListener?.invoke(view, item)

                }

                override fun onResize(height: Float) {
                    (context as HomePageActivity).runOnUiThread {
                        inflate.webBannerPage.layoutParams = LinearLayout.LayoutParams(
                            ViewConst.MATCH_PARENT,
                            (height * context.getResources().displayMetrics.density).toInt()
                        )
                        inflate.webBannerPage.requestLayout()
                    }
                }

            }
        inflate.webBannerPage.clipToPadding = false

        inflate.webBannerPage.addItemDecoration(ViewPageMargin(10.px))

        return inflate
    }


}
