package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout
import com.laalsa.laalsalib.ui.VUtil.dpToPx
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Order
import com.yumzy.orderfood.databinding.ViewOrderHistoryBinding
import com.yumzy.orderfood.ui.AppUIController
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.ViewConst


class OrderHistoryView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var bd: ViewOrderHistoryBinding


    init {
        this.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        this.orientation = HORIZONTAL
        this.setClickScaleEffect()

        /*android:background='@{vm.mDiscountMode.equals(vm.Companion.WALLET_MODE) ? @drawable/discount_mode_active : @drawable/discount_mode_inactive}'*/
        bd = ViewOrderHistoryBinding.inflate(LayoutInflater.from(context), this, true)

        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.OrderHistoryView,
            defStyleAttr,
            0
        )
        a?.recycle()
    }

    fun setOrderHistoryDetails(orderHistory: Order?) {
        bd.orderHistory = orderHistory
        this.background = DrawableUtils.getRoundDrawable(ThemeConstant.lightGray, dpToPx(10).toFloat())
        bd.btnReorder.setClickScaleEffect()
        bd.btnRateNow.setClickScaleEffect()
        bd.btnTrackorder.setClickScaleEffect()
        //bd.btnReorder.setOnClickListener { AppUIController.showCouponFailedUI(error) }
    }
    fun getReorderBtn():Button {
        return bd.btnReorder
    }
    fun getRateNowBtn():Button {
        return bd.btnRateNow
    }
    fun getTrackNowBtn():Button {
        return bd.btnTrackorder
    }


}