package com.yumzy.orderfood.ui.screens.module.dishes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.data.models.DishesItem
import com.yumzy.orderfood.databinding.AdapterOutletRegularItemBinding
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.util.IConstants

class TopDishRegularItem : AbstractBindingItem<AdapterOutletRegularItemBinding>(),
    AddItemView.AddItemListener {
    var listElements: DishesItem? = null


    lateinit var callerFun: (String?) -> Unit
    lateinit var clickedListener: (listElement: DishesItem?) -> Unit

    override val type: Int
        get() = IConstants.FastAdapter.REST_TOP_DISH_ITEM_VIEW


    //    var onRestaurantClick={outletId}
    override fun bindView(binding: AdapterOutletRegularItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.outletRegularItem.setTopDishRegularItem(listElements)
        binding.outletRegularItem.addItemListener = this
    }


    override fun unbindView(binding: AdapterOutletRegularItemBinding) {
        binding.outletRegularItem.setTopDishRegularItem(null)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOutletRegularItemBinding {
        val bind = AdapterOutletRegularItemBinding.inflate(inflater, parent, false)
        bind.outletRegularItem.changeAddButtonSize(80.px, 30.px)
        bind.outletRegularItem.addTextTopPadding(5.px)
        return bind
    }

    override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {
        clickedListener(listElements)
    }


}