package com.yumzy.orderfood.ui.common.cart

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.laalsa.laalsalib.utilcode.util.ActivityUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.ui.base.actiondialog.ActionConstant
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IOrderCustomizationHandler
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.screens.cart.CartRepository
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.util.IConstants
import javax.inject.Inject

class CartHelper  constructor(
    val sharedPrefsHelper: SharedPrefsHelper,
    val cartRepository: CartRepository,
    val mSavedCart: MutableLiveData<CartResDTO> = MutableLiveData()
) : ActionDialog.OnActionPerformed {

    private val baseContext: AppCompatActivity?
        get() = ActivityUtils.getTopActivity() as AppCompatActivity


    private var mICartHandler: ICartHandler? = null
    private var isIncremented: Boolean? = null
    private var mAddonsMap: Map<String, RestaurantAddOnDTO>? = null

    /*private var mRestaurantItem: RestaurantItem? = null
    private var mAddonsMap: Map<String, RestaurantAddOnDTO>? = null
    private var mCount: Int? = null*/
    //  private var mIndexx: Int? = null


    init {
        cartRepository.getCart(sharedPrefsHelper.getCartId().toString()).observeForever { cart -> }

        cartRepository.userCart?.observeForever { cart ->
            mSavedCart.value = cart
        }
    }

    fun addOrUpdateItemToCart(
        actionHandler: ICartHandler,
        count: Int,
        restaurantItem: RestaurantItem,
        addonsMap: Map<String, RestaurantAddOnDTO>?,
        incremented: Boolean?
    ) {

        mICartHandler = actionHandler
        // mRestaurantItem = restaurantItem
        //  mCount = count*/
        mAddonsMap = addonsMap
        if (this.baseContext == null) {
            mICartHandler?.failedToAdd()
            return
        }
        CleverTapHelper.itemQuantityChange(restaurantItem, count)
        isIncremented = incremented

        if (mICartHandler != null) {
            val cart = mSavedCart.value
            if (cart != null && !cart.outletDTOS.isNullOrEmpty()) {

                // cart is not empty

                //check for, is cart outlet different
                if (cart.outletDTOS[0]?.outletId ?: "" != restaurantItem.outletId) {

                    //show clear cart dialog
                    if (this.baseContext == null) {
                        mICartHandler?.failedToAdd()
                        return
                    }
                    val title = this.baseContext!!.getString(R.string.clear_cart)
                    val sMessage = this.baseContext!!.getString(R.string.clear_cart_to_proceed)

                    val sBody: MutableMap<String, Any> = mutableMapOf()
                    sBody["restaurantItem"] = restaurantItem
                    sBody["count"] = count

                    val actions: ArrayList<ActionItemDTO?> = arrayListOf(
                        ActionItemDTO(
                            key = ActionConstant.POSITIVE,
                            text = this.baseContext!!.getString(R.string.ok),
                            textColor = ThemeConstant.pinkies,
                            buttonColor = null,
                            sBody
                        ),
                        ActionItemDTO(
                            key = ActionConstant.CLOSE,
                            text = this.baseContext!!.getString(R.string.close),
                            textColor = ThemeConstant.textBlackColor,
                            buttonColor = null
                        )
                    )

                    showClearCartActionDialog(
                        IConstants.ActionModule.CLEAR_PLATE,
                        title,
                        sMessage,
                        actions
                    )
                } else {
                    //outlet is same

                    val items = cart.outletDTOS[0]?.itemDTOS ?: emptyList()
                    var indexx = -1
                    var valuee: CartItemDTO? = null
                    var itemCount = 0
                    for ((index, value) in items.withIndex()) {
                        if (value.itemID == restaurantItem.itemId) {
                            indexx = index
                            valuee = value
                            break
                        }
                    }

                    if (indexx == -1 || valuee == null) {
                        //cart dose not contain this item

                        if (restaurantItem.addons.isEmpty()) {
                            //it is a simple item
                            addToCartWithoutAddons(
                                restaurantItem,
                                count,
                                baseContext!!
                            )
                        } else {
                            //it is item with addons
                            addToCartWithVariant(
                                restaurantItem,
                                addonsMap,
                                count,
                                baseContext!!
                            )
                        }

                    } else {
                        //cart contain this item

                        if (this.baseContext == null) {
                            mICartHandler?.failedToAdd()
                            return
                        }


                        if (restaurantItem.addons.isEmpty()) {
                            //item dose not have item
                            updateItemCountInCart(
                                indexx,
                                count,
                                restaurantItem
                            )
                        } else {
                            //car have item have item show repeat item dialog

                            val b =
                                cart.outletDTOS.get(0)?.itemDTOS?.filter { item -> item.itemID == valuee.itemID }
                                    ?.sumBy { _ -> 1 } ?: 0 > 1

                            if (b) {
                                //cart contain this item more than one time with different customization

                                if (isIncremented == false) {

                                    //show go to plate dialog
                                    val title = this.baseContext!!.getString(R.string.view_plate)
                                    val sMessage =
                                        this.baseContext!!.getString(R.string.cart_contain_different_items)
                                    val actions: ArrayList<ActionItemDTO?> = arrayListOf(

                                        ActionItemDTO(
                                            key = ActionConstant.POSITIVE,
                                            text = this.baseContext!!.getString(R.string.view_plate),
                                            textColor = ThemeConstant.pinkies,
                                            buttonColor = null
                                        ),
                                        ActionItemDTO(
                                            key = ActionConstant.CLOSE,
                                            text = this.baseContext!!.getString(R.string.cancel),
                                            textColor = ThemeConstant.textBlackColor,
                                            buttonColor = null
                                        )
                                    )

                                    showClearCartActionDialog(
                                        IConstants.ActionModule.OPEN_PLATE_CUSTOMIZATION,
                                        title,
                                        sMessage,
                                        actions
                                    )


//                                    ActionModuleHandler.showActionDialog(
//                                        this.baseContext!!,
//                                        IConstants.ActionModule.CONFIRM_DIALOG,
//                                        baseContext!!.getString(R.string.cart_contain_different_items),
//                                        baseContext!!.getString(R.string.view_plate),
//                                        baseContext!!.getString(R.string.cancel),
//                                        object : CustomizeOptionDialog.OnActionPerformed {
//                                            override fun onConfirmDone(
//                                                dialog: Dialog?,
//                                                actionType: Int, label: String?
//                                            ) {
//                                                mICartHandler?.failedToAdd(gotoPlate = true)
//                                            }
//
//                                            override fun onDismissed(
//                                                actionType: Int,
//                                                actionLabel: String?
//                                            ) {
//                                                mICartHandler?.failedToAdd(gotoPlate = false)
//                                                return
//                                            }
//                                        }
//                                    )

                                } else {

                                    //else direct add item
                                    addToCartWithVariant(
                                        restaurantItem,
                                        addonsMap,
                                        1,
                                        baseContext!!
                                    )
                                }

                            } else {
                                //cart contain this item more thane time with same addons

                                if (count < valuee.quantity) {
                                    updateItemCountInCart(indexx, count, restaurantItem)
                                    return
                                }
                                val sBody = mutableMapOf<String, Any>()
                                sBody["indexx"] = indexx
                                sBody["count"] = count
                                sBody["restaurantItem"] = restaurantItem

                                val fBody: MutableMap<String, Any> = mutableMapOf()
                                fBody["restaurantItem"] = restaurantItem
                                fBody["count"] = 1


                                val title = this.baseContext!!.getString(R.string.repeat)
                                val sMessage =
                                    this.baseContext!!.getString(R.string.cart_have_item_with_addons)
                                val actions: ArrayList<ActionItemDTO?> = arrayListOf(
                                    ActionItemDTO(
                                        key = ActionConstant.CLOSE,
                                        text = "",
                                        textColor = ThemeConstant.white,
                                        buttonColor = null
                                    ),
                                    ActionItemDTO(
                                        key = ActionConstant.POSITIVE,
                                        text = this.baseContext!!.getString(R.string.repeat),
                                        textColor = ThemeConstant.pinkies,
                                        buttonColor = null,
                                        sBody
                                    ),
                                    ActionItemDTO(
                                        key = ActionConstant.NUTURAL,
                                        text = this.baseContext!!.getString(R.string.select),
                                        textColor = ThemeConstant.textBlackColor,
                                        buttonColor = null,
                                        fBody
                                    )
                                )


                                showClearCartActionDialog(
                                    IConstants.ActionModule.ITEM_REPEAT,
                                    title,
                                    sMessage,
                                    actions
                                )

//                                ActionModuleHandler.showActionDialog(
//                                    this.baseContext!!,
//                                    IConstants.ActionModule.CONFIRM_DIALOG,
//                                    baseContext!!.getString(R.string.cart_have_item_with_addons),
//                                    baseContext!!.getString(R.string.repeat),
//                                    baseContext!!.getString(R.string.select),
//                                    object : CustomizeOptionDialog.OnActionPerformed {
//                                        override fun onConfirmDone(
//                                            dialog: Dialog?,
//                                            actionType: Int, label: String?
//                                        ) {
//                                            //update item count
//                                            updateItemCountInCart(
//                                                indexx,
//                                                count,
//                                                restaurantItem
//                                            )
//                                        }
//
//                                        override fun onDismissed(
//                                            actionType: Int,
//                                            actionLabel: String?
//                                        ) {
//                                            when (actionType) {
//                                                1 -> {
//                                                    //add new item to cart
//                                                    addToCartWithVariant(
//                                                        restaurantItem,
//                                                        addonsMap,
//                                                        1,
//                                                        baseContext!!
//                                                    )
//                                                }
//                                                2 -> {
//                                                    mICartHandler?.failedToAdd()
//                                                    return
//                                                }
//                                                else -> {
//                                                }
//                                            }
//                                        }
//                                    }
//                                )

                            }


                        }

                    }


                }

            } else {
                //cart is empty
                addToCartWithOrWithoutAddon(restaurantItem, addonsMap, count)
            }
        }
    }

    private fun addToCartWithOrWithoutAddon(
        restaurantItem: RestaurantItem,
        addonsMap: Map<String, RestaurantAddOnDTO>?,
        count: Int
    ) {
        mAddonsMap = addonsMap
        if (restaurantItem.addons.isNullOrEmpty() == false) {
            //item contains addon as so add to cart with variant
            addToCartWithVariant(
                restaurantItem,
                addonsMap,
                count,
                baseContext!!
            )


        } else {
            //item does not have addon so add to cart without variant
            addToCartWithoutAddons(
                restaurantItem,
                count,
                baseContext!!
            )

        }
    }

    private fun addToCartWithVariant(
        restaurantItem: RestaurantItem,
        addonsMap: Map<String, RestaurantAddOnDTO>?,
        count: Int,
        lifecycleOwner: LifecycleOwner
    ) {
//        mRestaurantItem = restaurantItem
        mAddonsMap = addonsMap
//        mCount = count
        sharedPrefsHelper.saveCartOutletAddon(
            addonsMap,
            restaurantItem.itemId,
            restaurantItem.addons
        )
        val addons = ArrayList<RestaurantAddOnDTO>()
        restaurantItem.addons.forEach { addonId ->
            val addOnValue = addonsMap?.get(addonId)?.clone()
            if (addOnValue != null)
                addons.add(addOnValue)
        }

        return showCustomizationDialog(
            restaurantItem,
            addons,
            count,
            lifecycleOwner
        )

    }

    private fun addToCartWithoutAddons(
        restaurantItem: RestaurantItem,
        count: Int,
        lifecycleOwner: LifecycleOwner
    ) {

        val items = NewItemDTO(
            restaurantItem.outletId,
            restaurantItem.itemId,
            restaurantItem.price, count,
            null,
            null
        )
        val cartt = NewAddItemDTO(items, sharedPrefsHelper.getCartId().toString())
//        CleverTapHelper.itemQuantityChange(restaurantItem, count)

        addToCart(cartt, lifecycleOwner)
    }

    private fun showCustomizationDialog(
        restaurantItem: RestaurantItem,
        addons: MutableList<RestaurantAddOnDTO>,
        count: Int,
        lifecycleOwner: LifecycleOwner
    ) {
        if (this.baseContext == null) {
            mICartHandler?.failedToAdd()
            return
        }

        ActionModuleHandler.showOrderCustomization(
            this.baseContext!!,
            restaurantItem,
            addons,
            object : IOrderCustomizationHandler {
                override fun onSuccess(
                    variant: Variant?,
                    addOns: List<Variant>
                ) {

                    val items = NewItemDTO(
                        restaurantItem.outletId,
                        restaurantItem.itemId,
                        restaurantItem.price,
                        count,
                        variant,
                        addOns
                    )

                    val cartt = NewAddItemDTO(
                        items,
                        sharedPrefsHelper.getCartId().toString()
                    )

                    addToCart(cartt, lifecycleOwner)
//                    CleverTapHelper.itemQuantityChange(restaurantItem, count)

                }


                override fun onDismiss(dissmissType: Int) {
                    if (dissmissType == 2) {
                        mICartHandler?.failedToAdd()
                    }
                }
            })
    }


    private fun updateItemCountInCart(
        indexx: Int,
        count: Int,
        restaurantItem: RestaurantItem,
        isIncrementRequired: Boolean = true
    ) {
        synchronized(this) {
//            mIndexx = indexx
//            mCount = count
//            mRestaurantItem = restaurantItem
            cartRepository.itemCount(
                indexx,
                count,
                ItemCountDTO(
                    restaurantItem.outletId,
                    sharedPrefsHelper.getCartId().toString()
                )
            ).observeForever { result ->
                when {
                    result.type == APIConstant.Status.LOADING -> {
                        mICartHandler?.showProgressBar()
                        return@observeForever
                    }
                    result.type == APIConstant.Status.SUCCESS -> {

                        if (result?.data?.outletDTOS != null) {

                            if (result.data.outletDTOS.isEmpty()) {
                                clearAllSelected()
                            } else {

                                val totalItemCount =
                                    result.data.outletDTOS.get(0)?.itemDTOS?.filter { item -> item.itemID == restaurantItem.itemId }
                                        ?.sumBy { item -> item.quantity }
                                updateSelectedItemCount(
                                    totalItemCount ?: 0,
                                    restaurantItem.itemId,
                                    restaurantItem.name
                                )
                            }
                            mICartHandler?.itemAdded(count)

                        } else {
                            mICartHandler?.failedToAdd(message = result.message.toString())
                        }

                    }
                    result.type == APIConstant.Status.ERROR -> {
                        mICartHandler?.showCodeError(
                            APIConstant.Status.ERROR,
                            result.message.toString()
                        )
                        mICartHandler?.failedToAdd(message = result.message.toString())
                    }
                }
                mICartHandler?.hideProgressBar()
            }
        }
    }

    private fun addToCart(
        cartItem: NewAddItemDTO,
        lifecycleOwner: LifecycleOwner
    ) {

        synchronized(this) {
            cartRepository.addToCartRemote(cartItem).observeForever { result ->

                when (result.type) {

                    APIConstant.Status.LOADING -> {
                        mICartHandler?.showProgressBar()
                        return@observeForever
                    }

                    APIConstant.Status.ERROR -> {
                        mICartHandler?.failedToAdd()
                        when {
                            result.code != null -> mICartHandler?.showCodeError(
                                result.code,
                                "${result.message}"
                            )
                            else -> mICartHandler?.showCodeError(
                                APIConstant.Status.ERROR,
                                "${result.message}"
                            )
                        }
                    }
                    APIConstant.Status.SUCCESS -> {
//                        if (!cartItem.item.addons.isNullOrEmpty())
//                            CleverTapHelper.itemAddonAdded(cartItem)
                        when {
                            result.data != null -> {

                                //get count of item in cart with this item id and update the selected count in database

                                val totalItemCount =
                                    result?.data.outletDTOS?.get(0)?.itemDTOS?.filter { item -> item.itemID == cartItem.item.itemID }
                                        ?.sumBy { item -> item.quantity }

                                mICartHandler?.itemAdded(totalItemCount ?: 0)
                                updateSelectedItemCount(
                                    totalItemCount ?: 0,
                                    cartItem.item.itemID,
                                    null
                                )

                            }
                            result.type == APIConstant.Status.SUCCESS -> {
                                mICartHandler?.onResponse(
                                    result.code, result.message
                                )
                                mICartHandler?.failedToAdd(message = result.message.toString())
                            }
                            else -> {
                                mICartHandler?.showCodeError(
                                    APIConstant.Status.ERROR,
                                    "${result.message}"
                                )
                                mICartHandler?.failedToAdd(message = result.message.toString())
                            }
                        }

                    }

                }
                mICartHandler?.hideProgressBar()

            }


        }
    }

    fun updateSelectedItemCount(
        count: Int,
        itemId: String,
        name: String?
    ) {

        /*if (count == 0) {
            //if decreased count is 0, remove item from database selected list
            cartRepository.removeSelectedItem(itemId)
                .observeForever{ result -> }
        } else {
            //update the item count in database selected list
            cartRepository.addSelectedItem(
                ValuePairSI(
                    itemId,
                    count
                )
            ).observeForever{ result -> }
        }*/
        getCart().observe(this.baseContext!!, { result -> })
    }


    fun getCart(): LiveData<ResponseDTO<CartResDTO>> {
        return cartRepository.getCart(sharedPrefsHelper.getCartId().toString())
    }

    fun clearCart(cartId: String = ""): LiveData<ResponseDTO<Any>> {
        return cartRepository.clearCart(sharedPrefsHelper.getCartId().toString())
    }

    fun clearAllSelected() {
        return cartRepository.removeAllSelectedItem()
    }

    private fun showClearCartActionDialog(
        actionType: Int,
        title: CharSequence,
        sMessage: CharSequence,
        actions: ArrayList<ActionItemDTO?>
    ) {
        ActionModuleHandler.showChoseActionDialog(
            this.baseContext!!,
            actionType,
            title,
            sMessage,
            actions,
            this,
            iconString = baseContext?.resources?.getString(R.string.icon_dish) ?: "\uE0F6"
        )
    }

    override fun onSelectedAction(dialog: Dialog?, actionType: Int, action: ActionItemDTO?) {
        if (actionType == IConstants.ActionModule.CLEAR_PLATE) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                mSavedCart.value?.let {
                    CleverTapHelper.clearCart(it)
                }
                val cartId = sharedPrefsHelper.getCartId()
                val clearCart = cartId?.let { ClearCartReqDTO(it) }
                clearCart?.let {
                    cartRepository.clearCart(it.cartId)
                        .observe(this@CartHelper.baseContext!!, Observer { result ->

                            when (result.type) {
                                APIConstant.Status.LOADING -> {
                                    return@Observer
                                }

                                APIConstant.Status.SUCCESS -> {
                                    //clear cart and everything and add this item to cart
                                    sharedPrefsHelper.clearCartItemAddon()
                                    mSavedCart.postValue(null)


                                    addToCartWithOrWithoutAddon(
                                        action.data?.get("restaurantItem") as RestaurantItem,
                                        mAddonsMap,
                                        action.data.get("count") as Int,
                                    )
                                }
                                else -> {
                                    mICartHandler?.showCodeError(
                                        APIConstant.Status.ERROR,
                                        result.message
                                            ?: baseContext!!.getString(R.string.somthing_happened)
                                    )
                                    mICartHandler?.failedToAdd()
                                }
                            }
                        })
                }
            } else if (action != null && action.key == ActionConstant.CLOSE) {
//                mICartHandler?.showCodeError(
//                    APIConstant.Status.SUCCESS,
//                    baseContext!!.getString(R.string.can_not_add_without_clearing_cart),
//                )
                mICartHandler?.failedToAdd()
            }

        }
        if (actionType == IConstants.ActionModule.OPEN_PLATE_CUSTOMIZATION) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                mICartHandler?.failedToAdd(gotoPlate = true)
            } else if (action != null && action.key == ActionConstant.CLOSE) {
                mICartHandler?.failedToAdd(gotoPlate = false)
            }
        }

        if (actionType == IConstants.ActionModule.ITEM_REPEAT) {
            if (action != null && action.key == ActionConstant.POSITIVE) {
                updateItemCountInCart(
                    action.data?.get("indexx") as Int,
                    action.data.get("count") as Int,
                    action.data.get("restaurantItem") as RestaurantItem
                )
            } else if (action != null && action.key == ActionConstant.NUTURAL) {
                addToCartWithVariant(
                    action.data?.get("restaurantItem") as RestaurantItem,
                    mAddonsMap,
                    action.data.get("count") as Int,
                    baseContext!!
                )

            } else {
                mICartHandler?.failedToAdd()

            }
        }
    }


}