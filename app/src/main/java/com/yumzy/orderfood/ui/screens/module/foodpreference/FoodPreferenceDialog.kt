package com.yumzy.orderfood.ui.screens.module.foodpreference

import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FoodPrefSectionDTO
import com.yumzy.orderfood.data.models.FoodPreferenceDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.DialogFoodPreferenceBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.component.FoodPrefSectionView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject


class FoodPreferenceDialog : BaseDialogFragment<DialogFoodPreferenceBinding>(), IModuleHandler {

    
    val foodPreferenceDialog: FoodPreferenceViewModel by viewModel()

    var selectedvVegan = ""
    var selectedCuisineList = ""

    override fun getLayoutId(): Int {
        return R.layout.dialog_food_preference
    }

    val VEGAN_SELECTION = "vegan_selection"
    val FOOD_PREF_SELECTION = "food_pref_selection"

    override fun onDialogReady(view: View) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbFoodPref)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        bd.tbFoodPref.title = resources.getString(R.string.food_preference)
        bd.tbFoodPref.setNavigationOnClickListener { this.dismiss() }
        applyDebouchingClickListener(bd.btnFoodPrefNext)
        setupVeganSelectionView()

        setupFoodPreference()

    }

    private fun setupVeganSelectionView() {

        val veganList = arrayListOf(
            FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/food_pref/food_pref/anything.svg",
                "Anything",
                "anything",
                false
            ), FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/food_pref/food_pref/veg.svg",
                "Vegetarian",
                "veg",
                false
            ), FoodPreferenceDTO(
                "https://listing.sgp1.digitaloceanspaces.com/app_images/food_pref/food_pref/nonveg.svg",
                "Non Vegetarian",
                "nonveg",
                false
            )
        )

        val section =
            FoodPrefSectionDTO(getString(R.string.diet_selection), VEGAN_SELECTION, veganList)

        val sectionTitle = getSectionTitleView(section)
        val foodPrefSelectionView =
            FoodPrefSectionView(requireContext()).withAddonSection(object : FoodPrefChangeListener {
                override fun onCustomisationChange(
                    foodPrefSectionId: String,
                    selectedList: List<FoodPreferenceDTO>
                ) {
                    if (selectedList.isNotEmpty()) {
                        when (selectedList[0].prefId) {
                            "anything" -> bd.tvVegan.text = "Anything"
                            "veg" -> bd.tvVegan.text = "Vegetarian"
                            "nonveg" -> bd.tvVegan.text = "Non Vegetarian"

                        }
                        selectedvVegan = selectedList[0].prefId
                    }
                }
            }, section)

        bd.foodPrefContainer.addView(sectionTitle)
        bd.foodPrefContainer.addView(foodPrefSelectionView)

    }

    private fun setupFoodPreference() {

        foodPreferenceDialog.getlikedPrefTags().observe(
            this,
            { result ->
                RHandler<List<FoodPreferenceDTO>>(
                    this,
                    result
                ) {

                    val foodPrefList = it

                    val section =
                        FoodPrefSectionDTO(
                            getString(R.string.cuisine_selection),
                            FOOD_PREF_SELECTION,
                            it,
                            true
                        )
                    val sectionTitle = getSectionTitleView(section)
                    val foodPrefSelectionView =
                        FoodPrefSectionView(requireContext()).withAddonSection(object :
                            FoodPrefChangeListener {
                            override fun onCustomisationChange(
                                foodPrefSectionId: String,
                                selectedList: List<FoodPreferenceDTO>
                            ) {
                                selectedCuisineList =
                                    selectedList.joinToString(",") { item -> item.prefId }
                                bd.tvCuisineCount.text =
                                    "${selectedList.size} ${if (selectedList.size > 1) "Cuisines" else "Cuisine"} Selected"
                            }
                        }, section)

                    bd.foodPrefContainer.addView(sectionTitle)
                    bd.foodPrefContainer.addView(foodPrefSelectionView)

                }
            })
    }

    private fun getSectionTitleView(section: FoodPrefSectionDTO): View {
        val titleContainer = LinearLayout(context).run {
            layoutParams = LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
            orientation = LinearLayout.VERTICAL

            val titleTextView = TextView(context)
            titleTextView.setPadding(8.px, 20.px, 8.px, 16.px)
            titleTextView.setInterBoldFont()
            titleTextView.gravity = Gravity.CENTER
            titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            titleTextView.text = section.menuSectionTitile.capitalize()
            titleTextView.setTextColor(ThemeConstant.textBlackColor)

            this.addView(titleTextView)
            return@run this
        }
        return titleContainer
    }

    override fun onDebounceClick(view: View) {
        super.onDebounceClick(view)
        when (view.id) {
            R.id.btn_back ->
                dialog?.dismiss()

            R.id.btn_food_pref_next -> {
                ActionModuleHandler.showFoodPersonalisation(
                    context as AppCompatActivity,
                    selectedvVegan,
                    selectedCuisineList,
                    this
                )

            }
        }
    }


    override fun onSuccess(actionId: Int, data: Any?) {
        if (actionId == IConstants.ActionModule.FOOD_PERSONALISATION) {
            this.dismiss()
            actionHandler?.onSuccess(moduleId, null)

        }

    }

    override fun showCodeError(code: Int?, title: String?, message: String) {
        if (code == APIConstant.Status.NO_NETWORK) {

            bd.noDataToDisplay.visibility = View.VISIBLE
            bd.layoutFooter.visibility = View.GONE
            bd.scrollingFoodPref.visibility = View.GONE
            bd.noDataToDisplay.title = IConstants.OrderTracking.NO_INTERNET
            bd.noDataToDisplay.subTitle = IConstants.OrderTracking.NO_INTERNET_MSG
            bd.noDataToDisplay.imageResource = R.drawable.img_no_internet

        }
    }


    interface FoodPrefChangeListener {
        fun onCustomisationChange(
            foodPrefSectionId: String,
            selectedList: List<FoodPreferenceDTO>
        )
    }

}