/*
 *   Created by Bhupendra Kumar Sahu
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.InputFilter
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.gapTop
import com.yumzy.orderfood.ui.helper.setRalewayBoldFont
import com.yumzy.orderfood.ui.helper.setRalewayMediumFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


open class EditUPITemplate(
    override val context: Context,
    val title: String = "",
    val mUpdateSingleInfoListener: UpdateSingleInfoListener
) : IDetailsActionTemplate, IView {

    interface UpdateSingleInfoListener {
        fun sendUPI(text: String, dialog: DialogInterface?)

    }


    override var headerHeight: Int = VUtil.dpToPx(40)
        set(value) {
            field = VUtil.dpToPx(value)
        }

    private lateinit var  edittext :EditText
    private lateinit var  btn :MaterialButton
    override val showClose = false

    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.setPadding(0, 8.px, 0, 8.px)
        val fontIconView = FontIconView(context)
        val layoutParams = LinearLayout.LayoutParams(65.px, 65.px)
        layoutParams.setMargins(0, 0, 0, 5.px)
        fontIconView.layoutParams = layoutParams
        fontIconView.gravity = Gravity.CENTER
        fontIconView.setPadding(10.px, 10.px, 10.px, 10.px)
        fontIconView.text = context.getString(R.string.icon_035_payment)
        fontIconView.setTextColor(ThemeConstant.white)
        fontIconView.setBackgroundResource(R.drawable.rounded_pink_50)
        layout.gravity = Gravity.CENTER
        layout.orientation = LinearLayout.VERTICAL
        layout.addView(fontIconView)
        layout.setPadding(0, 0, 0, VUtil.dpToPx(20))
        layout.addView(getTextView(R.id.tv_info_header, title))
        return layout
    }

    @SuppressLint("SetTextI18n")
    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        layout.minimumHeight = headerHeight
        edittext = EditText(context)
        edittext.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        edittext.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        edittext.id = R.id.edt_upi_no
        edittext.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )
//        edittext.background = DrawableBuilder()
//            .rectangle()
//            .hairlineBordered()
//            .strokeColor(ThemeConstant.textLightGrayColor)
//            .strokeColorPressed(ThemeConstant.textDarkGrayColor)
//            .ripple()
//            .build()

        edittext.hint = context.getString(R.string.enter_upi)
        edittext.setHintTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.cool_gray,
                context.theme
            )
        )
        edittext.setRalewayMediumFont()
        edittext.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.black,
                context.theme
            )
        )


        val contextThemeWrapper = ContextThemeWrapper(context, R.style.Yumzy_Button)
        btn = MaterialButton(contextThemeWrapper)

        btn.layoutParams =LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 50.px)
        btn.setTextColor(Color.WHITE)
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkies)
        btn.rippleColor = ColorStateList.valueOf(ThemeConstant.rippleColor)
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
        btn.typeface = typeface
        btn.id = R.id.btn_verify_upi

        btn.gapTop(VUtil.dpToPx(5))
        btn.text = context.getString(R.string.verify_upi)
        layout.addView(edittext)
        layout.addView(btn)
        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {

//        edittext = view?.findViewById<EditText>(R.id.edt_upi_no)

        edittext.maxLines = 1
        edittext.minLines = 1
        edittext.isSingleLine = true
        edittext.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        ClickUtils.applyGlobalDebouncing(
            view?.findViewById<Button>(R.id.btn_verify_upi),
            IConstants.AppConstant.CLICK_DELAY
        ) {
            if ((!edittext.text.toString().equals("")) && (!edittext.text.toString()
                    .equals("0"))
            ) {

                mUpdateSingleInfoListener.sendUPI(
                    edittext.text.toString().trim(), dialog
                )
            } else {

                edittext.let { it1 -> YumUtil.animateShake(it1, 20, 5) }

            }

        }


    }


    open fun getTextView(tvId: Int, msg: String): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = MaterialTextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            textView.setTextAppearance(R.style.TextAppearance_MyTheme_Headline6)
        } else {
            textView.setTextAppearance(context, R.style.TextAppearance_MyTheme_Headline6)
        }

        if (tvId == R.id.tv_info_header) {
            textView.setRalewayBoldFont()
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.DKGRAY)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        }

        return textView

    }

    override fun showProgressBar() {

// Rotate, Sweep & Flip the Ring
//       val drawableBuilder= DrawableBuilder()
//           .size(10)
//           .ring()
//           .useLevelForRing()
//           .solidColor(ThemeConstant.pinkies)
//           .innerRadiusRatio(3f)
//           .thicknessRatio(10f)
//           .rotate(0f, 720f)
//           .build()
//        edittext.setCompoundDrawables(null,null,drawableBuilder,null)
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkiesLight)
        btn.isEnabled=false
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(
            edittext.getApplicationWindowToken(),
            InputMethodManager.HIDE_NOT_ALWAYS
        )

//        edittext.isFocusable=false;
//        edittext.onEditorAction(EditorInfo.IME_ACTION_DONE)
       // hide
        btn.text=context.getString(R.string.loading)


    }

    override fun hideProgressBar() {
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkies)
        btn.isEnabled = true
        btn.text = context.getString(R.string.verify_upi)


    }

    override fun showCodeError(code: Int?, title: String?, message: String) {

    }

    override fun onResponse(code: Int?, response: Any?) {

    }




}

