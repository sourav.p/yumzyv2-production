package com.yumzy.orderfood.ui.common.recyclerview.view;

public class YSwipeItemDTO {
    private String sText;
    private int imageResId;
    private int iColor;
    private int iPos;
    private int iWidth;

    public YSwipeItemDTO(String sText, int imageResId, int iColor, int iPos) {
        this.sText = sText;
        this.imageResId = imageResId;
        this.iColor = iColor;
        this.iPos = iPos;
    }

    public YSwipeItemDTO(String sText, int imageResId, int iColor, int iPos, int iWidth) {
        this.sText = sText;
        this.imageResId = imageResId;
        this.iColor = iColor;
        this.iPos = iPos;
        this.iWidth = iWidth;
    }

    public String getText() {
        return sText;
    }

    public void setText(String sText) {
        this.sText = sText;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public int getColor() {
        return iColor;
    }

    public void setColor(int iColor) {
        this.iColor = iColor;
    }

    public int getPos() {
        return iPos;
    }

    public void setPos(int iPos) {
        this.iPos = iPos;
    }

    public int getWidth() {
        return iWidth;
    }

    public void setWidth(int iWidth) {
        this.iWidth = iWidth;
    }
}
