package com.yumzy.orderfood.util.viewutils

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.*
import android.graphics.drawable.shapes.RoundRectShape
import android.graphics.drawable.shapes.Shape
import android.location.LocationManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.text.*
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StrikethroughSpan
import android.util.Patterns
import android.util.TypedValue
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.TextViewCompat
import coil.load
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.ui.common.search.utils.SimpleAnimationUtils
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.dateformater.UTCtoNormalDate
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable
import org.joda.time.DateTime
import java.net.URI
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.regex.Matcher
import java.util.regex.Pattern


object YumUtil {
    private val sNextGeneratedId = AtomicInteger(1)

/*    fun adjustFontScale(
        context: Context,
        configuration: Configuration
    ) {
        if (configuration.fontScale != 1f) {
            configuration.fontScale = 1f
            val metrics = context.resources.displayMetrics
//            val wm =
//                context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//            wm.defaultDisplay.getMetrics(metrics)
            metrics.scaledDensity = configuration.fontScale * metrics.density
            configuration.orientation = Configuration.ORIENTATION_PORTRAIT
            context.createConfigurationContext(configuration)
        }
    }*/

    fun getAndroidVersion(): Int {
        return Build.VERSION.SDK_INT
    }

    fun getGradientDrawable(color1: Int, color2: Int): Drawable {
        val gradDrawable = GradientDrawable()
        gradDrawable.colors = intArrayOf(color1, color2)

//        gradDrawable.cornerRadius=8f
//        gradDrawable.setPadding(5,5,5,5)
//        gradDrawable.gradientRadius=0.5f
//        gradDrawable.setColor()
//        gradDrawable.setColor(Color.parseColor("#EF718D"))
        return gradDrawable

    }

    fun setImageInView(imageView: ImageView, imageUrl: String) {
        imageView.load(imageUrl) {
            placeholder(YUtils.getPlaceHolder())
            error(YUtils.getPlaceHolder(6))
        }
        /*GlideApp
            .with(imageView)
            .load(imageUrl)
            .placeholder(YUtils.getPlaceHolder())
            .thumbnail(0.4f)
            .apply(
                RequestOptions().override(
                    imageView.width,
                    imageView.height
                )
            ) // can also be a drawable
            .into(imageView)*/
//        Glide.with(imageView.context).


    }

    fun setImageInView(imageView: ImageView, imageUrl: String, placeHolder: Int) {
        imageView.load(imageUrl) {
            placeholder(YUtils.getPlaceHolder())
            error(YUtils.getPlaceHolder(placeHolder))
        }
        /*GlideApp
            .with(imageView)
            .load(imageUrl)
            .placeholder(placeHolder)
            .thumbnail(0.4f)
            .apply(
                RequestOptions().override(
                    imageView.width,
                    imageView.height
                )
            ) // can also be a drawable
            .into(imageView)*/
//        Glide.with(imageView.context).


    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val clView = activity.currentFocus
        if (clView != null)
            imm.hideSoftInputFromWindow(clView.windowToken, 0)
        else
            imm.hideSoftInputFromWindow(activity.window?.decorView?.windowToken, 0)
    }

    fun hideDialogKeyboard(clDialog: Dialog) {
        val imm =
            clDialog.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(clDialog.window?.decorView?.windowToken, 0)

    }


    fun getIcon(context: Context, iResId: Int, iColor: Int): Drawable? {
        var drawable = context.resources.getDrawable(iResId, context.theme)
        drawable = DrawableCompat.wrap(drawable!!)
        DrawableCompat.setTint(
            drawable,
            iColor
        ) // CLThemeUtil.getMenuViewColors(context, R.styleable.CLNavigationView_item_imageColor));
        return drawable
    }

    fun isValidEmail(emailAddress: String?): Boolean {
        return emailAddress != null && Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()
    }

    fun isValidMobile(phone: String?): Boolean {

        if (phone == null) return false
        return if (!Pattern.matches("[a-zA-Z]+", phone)) {
            phone.length in 10..10
        } else false
    }

    fun isValidPhone(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }

    fun isValidName(name: String?): Boolean {
        if (name == null || name.length < 2) return false
        val pattern = Pattern.compile("[A-Za-z_]+")
        return pattern.matcher(name).matches()
    }

    fun isValidNameIgnoreSpace(name: String?): Boolean {
        val noSpaceName = name?.replace(" ", "") ?: ""
        if (name == null || noSpaceName.length < 2) return false
        val pattern = Pattern.compile("[A-Za-z_]+")
        return pattern.matcher(noSpaceName)
            .matches()
    }

    fun animateChangeAlpha(view: View, duration: Int, cont: Int) {
        val anim: Animation = AlphaAnimation(0.2f, 1.0f)
        anim.duration = duration.toLong() //You can manage the blinking time with this parameter
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = cont
        view.startAnimation(anim)
    }

    fun animateSizeScale(view: View, duration: Int, cont: Int) {
        val anim: Animation = ScaleAnimation(0.8f, 1.0f, 0.8f, 1.0f)
        anim.duration = duration.toLong() //You can manage the blinking time with this parameter
        anim.repeatMode = Animation.REVERSE
        anim.duration = duration.toLong()
        anim.repeatCount = cont
        view.startAnimation(anim)
    }

    fun animateShake(view: View, duration: Int, offset: Int) {
        val anim: Animation = TranslateAnimation((-offset).toFloat(), offset.toFloat(), 0f, 0f)
        anim.duration = duration.toLong()
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = 5
        view.startAnimation(anim)
    }

    fun animateVerticalShake(view: View, duration: Int, offset: Int) {
        val anim: Animation = TranslateAnimation(0f, 0f, (-offset).toFloat(), offset.toFloat())
        anim.duration = duration.toLong()
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = 5
        view.startAnimation(anim)
    }

    fun getDeviceUniqueId(context: Context): String {
        return Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        ) ?: UTCtoNormalDate.getDateMillis(DateTime.now().toString()).toString()
    }

    fun getNavigationDrawable(context: Context, @ColorInt naVColor: Int): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(
                    canvas: Canvas,
                    paint: Paint
                ) {
                    paint.shader = LinearGradient(
                        0f, 0f, canvas.width / 2.toFloat(), height,
                        adjustAlpha(naVColor, 150), naVColor, Shader.TileMode.MIRROR
                    )
                    paint.setShadowLayer(10f, 4f, 0f, 0xffdedede.toInt())
//                paint.setColor(naVColor);
                    val weight = canvas.width + 4f
                    val height = canvas.height + 4f
                    mPath.reset()
                    mPath.moveTo(weight / 2.toFloat(), 0f)
                    mPath.lineTo(0f, 0f)
                    mPath.lineTo(0f, height * 0.8f)
                    mPath.cubicTo(
                        0f, height.toFloat(), weight / 2.toFloat(), height * 1.2f,
                        weight * 0.6f, height * 0.55f
                    )
                    mPath.cubicTo(
                        weight * 0.6f,
                        height * 0.6f,
                        weight * 0.6f,
                        height * 0.2f,
                        weight.toFloat(),
                        getActionBarHeight(context).toFloat()
                    )
                    mPath.lineTo(weight.toFloat(), 0f)
                    mPath.close()
                    canvas.drawPath(mPath, paint)
                }
            }
        shape.height
        return ShapeDrawable(shape)
    }


    fun getActionBarHeight(context: Context): Int {
        val tv = TypedValue()
        context.theme.resolveAttribute(R.attr.actionBarSize, tv, true)
        return context.resources.getDimensionPixelSize(tv.resourceId)
    }

    fun adjustAlpha(@ColorInt color: Int, alpha: Int): Int {
        return alpha shl 24 or (color and 0x00ffffff)
    }

    fun getSelectablePressedBackground(
        context: Context,
        @ColorInt selected_color: Int,
        pressed_alpha: Int,
        animate: Boolean
    ): StateListDrawable? {
        val states: StateListDrawable =
            getSelectableBackground(
                context,
                selected_color,
                animate
            )
        val clrPressed = ColorDrawable(
            adjustAlpha(
                selected_color,
                pressed_alpha
            )
        )
        states.addState(intArrayOf(android.R.attr.state_pressed), clrPressed)
        return states
    }

    fun getSelectableBackground(ctx: Context): Int { /* If we're running on Honeycomb or newer, then we can use the Theme's selectableItemBackground to ensure that the View has a pressed state*/
        val outValue =
            TypedValue() /*it is important here to not use the android.R because this wouldn't add the latest drawable*/
        ctx.theme.resolveAttribute(R.attr.selectableItemBackground, outValue, true)
        return outValue.resourceId
    }

    fun getSelectableBackground(
        ctx: Context,
        @ColorInt selected_color: Int,
        animate: Boolean
    ): StateListDrawable {
        val states =
            StateListDrawable()
        val clrActive = ColorDrawable(selected_color)
        states.addState(intArrayOf(android.R.attr.state_selected), clrActive)
        states.addState(
            intArrayOf(),
            ContextCompat.getDrawable(
                ctx,
                getSelectableBackground(ctx)
            )
        )

        //if possible we enable animating across states
        if (animate) {
            val duration =
                ctx.resources.getInteger(android.R.integer.config_shortAnimTime)
            states.setEnterFadeDuration(duration)
            states.setExitFadeDuration(duration)
        }
        return states
    }

    fun getWaveDrawable(): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    paint.setShadowLayer(10f, 4f, 0f, 0xffdedede.toInt())
                    paint.color = ThemeConstant.pinkies
                    val canvasWeight = canvas.width + 4f
                    val canvasHeight = canvas.height + 4f
                    mPath.reset()
                    mPath.moveTo(canvasWeight.toFloat(), canvasHeight * 0.8f)
                    mPath.lineTo(canvasWeight.toFloat(), 0f)
                    mPath.lineTo(0f, 0f)
                    mPath.lineTo(0f, canvasHeight * 0.7f)
                    mPath.cubicTo(
                        0f,
                        canvasHeight * 0.7f,
                        canvasWeight * 0.2f,
                        canvasHeight * 0.85f,
                        canvasWeight * 0.3f,
                        canvasHeight * 0.6f
                    )
                    mPath.cubicTo(
                        canvasWeight * 0.3f,
                        canvasHeight * 0.6f,
                        canvasWeight * 0.5f,
                        canvasHeight * 0.15f,
                        canvasWeight * 0.7f,
                        canvasHeight * 0.8f
                    )
                    mPath.cubicTo(
                        canvasWeight * 0.7f,
                        canvasHeight * 0.8f,
                        canvasWeight * 0.8f,
                        canvasHeight * 1.2f,
                        canvasWeight.toFloat(),
                        canvasHeight * 0.8f
                    )
                    canvas.drawPath(mPath, paint)
                }
            }
        shape.height
        return ShapeDrawable(shape)
    }

    fun getHexaColor(iColorVal: Int): String? {
        var iColorVal = iColorVal
        var shValue = (iColorVal / 0x10000).toShort() // red
        var sHexaColor: String = getFixedLengthString(
            Integer.toHexString(shValue.toInt()), 2
        )
        iColorVal = iColorVal % 0x10000
        shValue = (iColorVal / 0x100).toShort() // green
        sHexaColor = sHexaColor + getFixedLengthString(
            Integer.toHexString(shValue.toInt()), 2
        )
        shValue = (iColorVal % 0x100).toShort() //blue
        sHexaColor = sHexaColor + getFixedLengthString(
            Integer.toHexString(shValue.toInt()), 2
        )
        return sHexaColor
    }

    fun getFixedLengthString(sValue: String?, iLength: Int): String {
        var sValue = sValue
        if (sValue == null) sValue = ""
        if (iLength - sValue.length > 0) {
            val chValue = '0'
            val chRepeatStr = CharArray(iLength - sValue.length)
            for (i in chRepeatStr.indices) chRepeatStr[i] = chValue
            val sValuePrefix = String(chRepeatStr)
            return sValuePrefix + sValue
        }
        return sValue
    }

    fun showKeyboard(context: Context, edit: EditText?) {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
        inputMethodManager.showSoftInput(edit, 0)
        edit!!.requestFocus()
    }

    fun getRoundedDrawable(iLeftColor: Int, iRightColor: Int): Drawable? {
        val iRadius = UIHelper.i8px.toFloat()
        val fArrLeftOuterRadius =
            floatArrayOf(iRadius, iRadius, iRadius, iRadius, iRadius, iRadius, iRadius, iRadius)
        val fArrRightOuterRadius =
            floatArrayOf(0.0f, 0.0f, iRadius, iRadius, iRadius, iRadius, 0.0f, 0.0f)
        val clTopRoundRectangle =
            RoundRectShape(fArrLeftOuterRadius, null as RectF?, null as FloatArray?)
        val clLeftShape = ShapeDrawable(clTopRoundRectangle)
        clLeftShape.paint.color = iLeftColor
        val clLeftroundrectangle =
            RoundRectShape(fArrRightOuterRadius, null as RectF?, null as FloatArray?)
        val clRightShape = ShapeDrawable(clLeftroundrectangle)
        clRightShape.paint.color = iRightColor
        val drawarray = arrayOf<Drawable>(clLeftShape, clRightShape)
        val clLayerDrawable = LayerDrawable(drawarray)
        clLayerDrawable.setLayerInset(0, 0, 0, 0, 0)
        val i4 = (iRadius / 2).toInt()
        clLayerDrawable.setLayerInset(1, i4, 0, 0, 0)
        return clLayerDrawable
    }

    fun spannedTabCountString(
        sLabel: String?,
        iCount: Int
    ): CharSequence? {
        val sCount: String = if (iCount < 0) "" else " ($iCount)"

//        SpannableString span1 = new SpannableString(sLable);
//        span1.setSpan(new ForegroundColorSpan(CLThemeUtil.getThemePrimaryColor(clContext)), 0, span1.length(), 0);
        val span2 = SpannableString(sCount)
        span2.setSpan(
            ForegroundColorSpan(ThemeConstant.pinkies),
            0,
            span2.length,
            0
        )
        return TextUtils.concat(sLabel, span2)
    }

    fun getOfferItemPrice(
        offerPrice: Double?,
        itemPrice: Double?,
        ignoreOfferPriceZero: Boolean = false,
        strikeColor: Int = ThemeConstant.textGrayColor,
        offerColor: Int = -1
    ): CharSequence? {
        if (!ignoreOfferPriceZero && (offerPrice == null || offerPrice == 0.0)) return "₹${
            itemPrice?.withPrecision(
                0
            )
        }"

        val span2 = SpannableString(" ₹${itemPrice?.withPrecision(0)}  ")
        span2.setSpan(
            StrikethroughSpan(),
            0,
            span2.length,
            Paint.STRIKE_THRU_TEXT_FLAG
        )

        span2.setSpan(
            ForegroundColorSpan(strikeColor),
            0,
            span2.length,
            SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE
        )
//        span2.setSpan(StrikethroughSpan(), 0,  span2.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

        val offerPriceText = " ₹${offerPrice?.withPrecision(2)}"
        if (offerColor != -1) {
            val offerSpan = SpannableString(offerPriceText)
            offerSpan.setSpan(
                ForegroundColorSpan(offerColor),
                0,
                offerSpan.length,
                SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return TextUtils.concat(span2, offerSpan)

        } else
            return TextUtils.concat(span2, offerPriceText)
    }

    fun generateViewId(): Int {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            while (true) {
                val result: Int = sNextGeneratedId.get()
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                var newValue = result + 1
                if (newValue > 0x00FFFFFF) newValue = 1 // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(
                        result,
                        newValue
                    )
                ) {
                    return result
                }
            }
        } else {
            return View.generateViewId()
        }
    }

    fun revealOrFadeIn(
        view: View,
        duration: Int,
        listener: SimpleAnimationUtils.AnimationListener?,
        center: Point?
    ): Animator? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SimpleAnimationUtils.reveal(view, duration, listener, center)
        } else {
            SimpleAnimationUtils.fadeIn(view, duration, listener)
        }
    }

    fun convertTitleCase(text: String?): String? {
        if (text == null || text.isEmpty()) {
            return text
        }
        val converted = StringBuilder()
        var convertNext = true
        var ch1: Char
        for (ch in text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true
                converted.append(ch)

            } else if (convertNext) {
                ch1 = Character.toTitleCase(ch)
                convertNext = false
                converted.append(ch1)

            } else {
                ch1 = Character.toLowerCase(ch)
                converted.append(ch1)

            }
        }
        return converted.toString()
    }

    fun dividerView(context: Context?, color: Int, height: Int): View {
        return dividerView(context, color, height, UIHelper.i5px, UIHelper.i8px)
    }

    fun dividerView(
        context: Context?,
        color: Int,
        height: Int,
        marginLeftRight: Int,
        marginTopBottom: Int
    ): View {
        val divider = LinearLayout(context)
        divider.layoutParams =
            ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        divider.background = VDrawableUtils.lineCenterDrawable(color, height.toFloat())
        divider.setPadding(marginLeftRight, marginTopBottom, marginLeftRight, marginTopBottom)
        divider.clipToOutline = false
        return divider
    }

    fun generateBackgroundWithShadow(
        view: View, backgroundColor: Int,
        cornerRadius: Float,
        shadowColor: Int,
        elevation: Int,
        shadowGravity: Int
    ): Drawable? {
        val outerRadius = floatArrayOf(
            cornerRadius, cornerRadius, cornerRadius,
            cornerRadius, cornerRadius, cornerRadius, cornerRadius,
            cornerRadius
        )
        val backgroundPaint = Paint()
        backgroundPaint.style = Paint.Style.FILL
        backgroundPaint.setShadowLayer(cornerRadius, 0f, 0f, 0)
        val shapeDrawablePadding = Rect()
        shapeDrawablePadding.left = elevation
        shapeDrawablePadding.right = elevation
        val DY: Int
        when (shadowGravity) {
            Gravity.CENTER -> {
                shapeDrawablePadding.top = elevation
                shapeDrawablePadding.bottom = elevation
                DY = 0
            }
            Gravity.TOP -> {
                shapeDrawablePadding.top = elevation * 2
                shapeDrawablePadding.bottom = elevation
                DY = -1 * elevation / 3
            }
            Gravity.BOTTOM -> {
                shapeDrawablePadding.top = elevation
                shapeDrawablePadding.bottom = elevation * 2
                DY = elevation / 3
            }
            else -> {
                shapeDrawablePadding.top = elevation
                shapeDrawablePadding.bottom = elevation * 2
                DY = elevation / 3
            }
        }
        val shapeDrawable = ShapeDrawable()
        shapeDrawable.setPadding(shapeDrawablePadding)
        shapeDrawable.paint.color = backgroundColor
        shapeDrawable.paint.setShadowLayer(
            cornerRadius / 3, 0f,
            DY.toFloat(), shadowColor
        )
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, shapeDrawable.paint)
        shapeDrawable.shape = RoundRectShape(outerRadius, null, null)
        val drawable = LayerDrawable(arrayOf<Drawable>(shapeDrawable))
        drawable.setLayerInset(
            0,
            elevation,
            elevation * 2,
            elevation,
            elevation * 2
        )
        return drawable
    }

    fun animateVerticalWithFadeIn(view: View, animDuration: Long) {

        val animattorTanslationY = ObjectAnimator.ofFloat(view, "translationY", 300f).apply {
            duration = animDuration
        }
        val animatorTanslationY2 = ObjectAnimator.ofFloat(view, "translationY", -0f).apply {
            duration = animDuration
        }
        val animatorFadeIn = ObjectAnimator.ofFloat(view, "alpha", 1f).apply {
            duration = animDuration
        }
        val animatorFadeOut = ObjectAnimator.ofFloat(view, "alpha", 0.2f).apply {
            duration = animDuration
        }

        AnimatorSet().apply {
            play(animatorFadeIn).with(animatorTanslationY2).before(
                AnimatorSet().apply {
                    play(animattorTanslationY).with(animatorFadeOut)
                }
            )
            start()
        }
    }

    fun animateVerticalWithFadeOut(view: View, animDuration: Long) {

        val animattorTanslationY = ObjectAnimator.ofFloat(view, "translationY", 300f).apply {
            duration = animDuration
        }
        val animatorTanslationY2 = ObjectAnimator.ofFloat(view, "translationY", -0f).apply {
            duration = animDuration

        }
        val animatorFadeIn = ObjectAnimator.ofFloat(view, "alpha", 1f).apply {
            duration = animDuration
        }
        val animatorFadeOut = ObjectAnimator.ofFloat(view, "alpha", 0.2f).apply {
            duration = animDuration
        }

        AnimatorSet().apply {
            play(animattorTanslationY).with(animatorFadeOut).before(
                AnimatorSet().apply {
                    play(animatorFadeIn).with(animatorTanslationY2)
                }
            )
            start()
        }
    }


    fun getHomeTitle(layoutType: Int): String {
        return when (layoutType) {
            IConstants.LayoutType.Restaurant2000.NEW_LAUNCHED -> "New Launched"
            IConstants.LayoutType.Restaurant2000.TOP_RESTAURANTS -> "Top Restaurants"
            IConstants.LayoutType.Restaurant2000.YUMZY_EXCLUSIVE -> "Yumzy Exclusive"
            IConstants.LayoutType.Restaurant2000.TRENDING_RESTAURANTS -> "Tranding Restaurants"
            IConstants.LayoutType.Restaurant2000.FEATURED_BRANDS -> "Featured Brands"
            IConstants.LayoutType.Dish3000.YUMZY_BEST -> "Yumzy Best"
            IConstants.LayoutType.Dish3000.WINTER_SPECIAL -> "Winter Special"
            IConstants.LayoutType.Dish3000.POPULAR_FOOD -> "Popular Food"
            IConstants.LayoutType.Dish3000.TRENDING_FOODS -> "Trending Food"
            IConstants.LayoutType.Banner1000.TOP_BANNER -> "Banner"
            IConstants.LayoutType.Banner1000.OFFER_ADS -> "Offer Ads"
            IConstants.LayoutType.Banner1000.THE_BRANDS -> "Brands"
            IConstants.LayoutType.Banner1000.ADS_ITEM -> "Ads"
            IConstants.LayoutType.Banner1000.POPULAR_CUISINES -> "Popular Cousions"

            else -> ""
        }
    }

    fun getRatingCount(rating: Double): SpannableStringBuilder? {
//        val value = java.lang.Double.toString(rating)
        val times = "★ ".repeat(rating.toInt())

        val spannable = SpannableStringBuilder(times.toString())
        spannable.setSpan(RelativeSizeSpan(1.5f), 0, 0, 0)

        spannable.setSpan(
            ForegroundColorSpan(ThemeConstant.yellow),  /* start index */
            0,  /* end index */spannable.length,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )
        /*val ratingText = SpannableStringBuilder(" Rated")
        ratingText.setSpan(
            StyleSpan(Typeface.BOLD),
            0, ratingText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ratingText.setSpan(
            ForegroundColorSpan(ThemeConstant.textBlackColor),  *//* start index *//*
            0,  *//* end index *//*ratingText.length,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )
        spannable.insert(times.length, ratingText)*/
        //            ★   🟊
        return spannable
    }

    /* private fun getRatingSpann(): SpannableString? {
         val spannable = SpannableString("“Text is spantastic!”")
         spannable.setSpan(
             ForegroundColorSpan(Color.RED),
             8, 12,
             Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
         )
         spannable.setSpan(
             StyleSpan(Typeface.BOLD),
             8, spannable.length,
             Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
         )
         return spannable
     }*/
    /* fun isOnline(context: Context): Boolean {
         val cm = context.getSystemService(
             Context.CONNECTIVITY_SERVICE
         ) as ConnectivityManager
         return cm.activeNetwork != null
     }*/


    fun setInterFontRegular(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.raleway_medium)
        textView.typeface = typeface

    }

    fun setRalewaySemiBold(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
        textView.typeface = typeface

    }

    fun setRalewayMedium(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_medium)
        textView.typeface = typeface

    }

    fun setRalewayRegular(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.poppins)
        textView.typeface = typeface

    }

    fun setInterFontBold(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.raleway_medium)
        textView.typeface = typeface

    }

    fun setInterFontLight(context: Context, textView: TextView) {
        val typeface = ResourcesCompat.getFont(context, R.font.poppins)
        textView.typeface = typeface

    }

    fun setColorText(text: String, color: Int): CharSequence {

        val sb = SpannableString(text)
        sb.setSpan(
            ForegroundColorSpan(color),
            0,
            text.length,
            SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return sb
    }

    fun setZoneOfferString(offers: List<OfferDTO>?): String {
        if (offers.isNullOrEmpty()) return ""
        var offerMessage = ""

        offers.forEach { offerItem ->
//            val net = IConstants.OfferConstant.NET
//            IConstants.OfferConstant(offerItem.discountType)


            when (offerItem.discountType) {
                IConstants.DiscountType.NET -> {
                    if (findCurrentDateBewteenStartAndEndTime(
                            offerItem.startDate,
                            offerItem.endDate
                        )
                    ) {

                        if (checktimings(offerItem.startTime, offerItem.endTime)) {
/*
30%off on orders above 100
min--100 max-0

flat 100 rs off on order above 100
min-100,max-0, net value

30%off on orders above 100 upto rs 75
min--100 max-75

30%off on orders above 0 upto rs75
min--0 max-75

50% off
min-0 ,max-0
 */
                            if (offerItem.minOrderAmount > 0) {
                                offerMessage =
                                    "Flat ₹" + offerItem.value + " OFF"
                                return@forEach
                            }

                            if (offerItem.minOrderAmount == 0) {
                                offerMessage = """Flat ₹${offerItem.value} OFF """
                                return@forEach
                            }

                        }
                    }
                }
                IConstants.DiscountType.PERCENTAGE -> {
                    if (findCurrentDateBewteenStartAndEndTime(
                            offerItem.startDate,
                            offerItem.endDate
                        )
                    ) {

                        if (checktimings(offerItem.startTime, offerItem.endTime)) {

                            if (offerItem.minOrderAmount == 0 && offerItem.maxDiscount == 0) {
                                offerMessage = "Flat ${offerItem.value}% OFF "
                                return@forEach
                            }
                            if (offerItem.minOrderAmount > 0 && offerItem.maxDiscount > 0) {
//                                30%off on orders above 0 upto rs75
                                offerMessage =
                                    "${offerItem.value}% OFF"
                                return@forEach
                            }
                            if (offerItem.minOrderAmount == 0 && offerItem.maxDiscount > 0) {
//                                30%off on orders above 0 upto rs75
                                offerMessage =
                                    "${offerItem.value}% OFF"
                                return@forEach
                            }
                            if (offerItem.minOrderAmount > 0 && offerItem.maxDiscount == 0) {
//                                30%off on orders above 0 upto rs75
                                offerMessage =
                                    "Flat ${offerItem.value}% OFF"
                                return@forEach
                            }

                        }
                    }
                }
            }


//            IConstants.OfferConstant


        }
        return offerMessage

    }

    fun setRecZoneOfferString(
        offers: List<RestaurantItemOffer>?,
        itemPrice: Double
    ): Pair<String, Double>? {
        if (offers.isNullOrEmpty()) return null

        var offerMessage: String
        var offerPrice: Double

        offers.forEach { offerItem ->
//            val net = IConstants.OfferConstant.NET
//            IConstants.OfferConstant(offerItem.discountType)
            when (offerItem.discountType) {
                IConstants.DiscountType.NET -> {
                    if (findCurrentDateBewteenStartAndEndTime(
                            offerItem.startDate,
                            offerItem.endDate
                        )
                    ) {
                        if (checktimings(offerItem.startTime, offerItem.endTime)) {
                            offerMessage = "₹" + offerItem.value + " OFF"
                            offerPrice = itemPrice - offerItem.value
                            return Pair(offerMessage, offerPrice)
                        }
                    }
                }
                IConstants.DiscountType.PERCENTAGE -> {
                    if (findCurrentDateBewteenStartAndEndTime(
                            offerItem.startDate,
                            offerItem.endDate
                        )
                    ) {
                        if (checktimings(offerItem.startTime, offerItem.endTime)) {
                            offerMessage = "Flat " + offerItem.value + "% OFF"
                            offerPrice =
                                itemPrice - itemPrice * (offerItem.value.toDouble() * (1f / 100f))
                            return Pair(offerMessage, offerPrice)
                        }
                    }
                }
            }

        }
        return null
    }

    fun findCurrentDateBewteenStartAndEndTime(
        startDateStr: String?,
        endDateStr: String?
    ): Boolean {
        return try {
            val sdf =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val startDate = sdf.parse(startDateStr)
            val endDate = sdf.parse(endDateStr)
            val currentDate: Date? = findCurrentDate()
            val currentDateStr = sdf.format(currentDate)
            currentDate?.after(startDate)!! && currentDate.before(endDate) || currentDateStr == sdf.format(
                startDate
            ) || currentDateStr == sdf.format(endDate)
        } catch (e: Exception) {
            false
        }
    }

    fun findCurrentDate(): Date? {
        return try {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            val calobj = Calendar.getInstance()
            df.parse(df.format(calobj.time))
        } catch (e: java.lang.Exception) {
            null
        }
    }

    fun getCurrentTimeUsingCalendar(): String {
        val cal = Calendar.getInstance()
        val date = cal.time
        val dateFormat: DateFormat = SimpleDateFormat("HHmm")
        val formattedDate = dateFormat.format(date)
        return formattedDate
        println("Current time of the day using Calendar - 24 hour format: $formattedDate")
    }

    private fun checktimings(startTime: String?, endTime: String?): Boolean {
        val currentTime = getCurrentTimeUsingCalendar().toInt()
        return startTime?.run { this.toInt() <= currentTime } ?: false && endTime?.run { currentTime <= this.toInt() } ?: false
    }

    fun hasPermission(context: Context, permission: String): Boolean {
        val res: Int = context.checkCallingOrSelfPermission(permission)


        return res == PackageManager.PERMISSION_GRANTED
    }

    fun hasPermissions(
        context: Context?,
        vararg permissions: String?
    ): Boolean {
        var hasAllPermissions = true
        for (permission in permissions) {
            //you can return false instead of assigning, but by assigning you can log all permission values
            if (!hasPermission(context!!, permission!!)) {
                hasAllPermissions = false
            }
        }
        return hasAllPermissions
    }

    fun textUniformTextView(textView: TextView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textView.setAutoSizeTextTypeUniformWithConfiguration(
                10, 16, 1, TypedValue.COMPLEX_UNIT_DIP
            )
        } else
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
                textView, 10, 16, 1,
                TypedValue.COMPLEX_UNIT_DIP
            )
    }

    fun spanSizeString(spanString: String, normalString: String, spanValue: Float): CharSequence? {
        val ss1 = SpannableString(spanString)
        ss1.setSpan(RelativeSizeSpan(spanValue), 0, ss1.length, 0) // set size

        val ss2 = SpannableString(normalString)
//        ss2.setSpan(RoundedBackgroundSpan(Color.WHITE,ThemeConstant.greenAddItem), 0, ss2.length, 0) // set size

//        ss1.setSpan(ForegroundColorSpan(Color.RED), 0, ss1.length, 0) // set color
        return TextUtils.concat(ss1, ss2)
    }

    fun getQueryParam(query: String, url: String): String {
        try {
            url.split("?")[1].split("&").forEach { params ->
                if (params.contains(query)) {
                    return params.split("=")[1]
                }
            }
        } catch (e: java.lang.Exception) {
            FirebaseCrashlytics.getInstance()
                .setCustomKey("functionName", "getQueryParam($query, $url)")
            FirebaseCrashlytics.getInstance().setCustomKey(
                "notification_without_url",
                "Received $url as Destination in Notification"
            )
        }

        return ""
    }

    fun getDurationAndCostForTwo(itemAtPos: HomeListDTO): String {
        val data: String

        if (itemAtPos.meta?.duration ?: 0.0 > 0.0 && (!itemAtPos.meta?.costForTwo.isNullOrEmpty())) {
            if (itemAtPos.meta?.duration == 1.0) {
                data =
                    itemAtPos.meta?.duration?.toInt()
                        .toString() + IConstants.DeliveryTime.MIN + IConstants.DeliveryTime.COSTFOR2 + itemAtPos.meta?.costForTwo

            } else {

                data =
                    itemAtPos.meta?.duration?.toInt()
                        .toString() + IConstants.DeliveryTime.MINS + IConstants.DeliveryTime.COSTFOR2 + itemAtPos.meta?.costForTwo
            }
        } else if (itemAtPos.meta?.duration == 0.0 && (!itemAtPos.meta?.costForTwo.isNullOrEmpty())) {
            data = IConstants.DeliveryTime.COSTFOR2 + itemAtPos.meta?.costForTwo.toString()

        } else if (itemAtPos.meta?.duration ?: 0.0 > 0.0 && (itemAtPos.meta?.costForTwo.isNullOrEmpty())) {
            if (itemAtPos.meta?.duration == 1.0) {
                data =
                    itemAtPos.meta?.duration?.toInt().toString() + IConstants.DeliveryTime.MIN

            } else {

                data =
                    itemAtPos.meta?.duration?.toInt().toString() + IConstants.DeliveryTime.MINS
            }
        } else {
            data = ""
        }
        return data

    }

    fun setPaidVia(paidVia: List<PaymentDTO>): String {

        var data: String? = paidVia
            .filter { paymentDTO -> paymentDTO.paid ?: 0.0 > 0.0 && paymentDTO.method !== "unknown" }
            .map { paymentDTO -> paymentDTO.method.toString().toUpperCase() }
            .joinToString(" and ")

        return if (data?.isEmpty() == true) " UNKNOWN " else data?.trim().toString()

//        var data: String? = ""
//        paidVia.forEach { paymentDTO ->
//
//            if (paymentDTO.paid != 0.0) {
//                if (!paymentDTO.method.equals("unknown")) {
//                    data = data + paymentDTO.method.toString().toUpperCase() + " and "
//                } else {
//                    data = "UNKNOWN    "
//                }
//            } else {
//                data = data
//            }
//
//        }
//        return data?.substring(0, data!!.length - 4)!!

    }

    fun convertDoubleToInt(value: Double): Int? {
        if (value == 0.0) {
            return null
        }
        return value.toInt()

    }

    fun firstCaps(value: String): String? {
        if (value.isNullOrEmpty()) {
            return " "
        }
        return value.capitalize()

    }

    class CLDecimalDigitsInputFilter(iBeforeZero: Int, iDecimalSize: Int) :
        InputFilter {
        var mPattern: Pattern
        override fun filter(
            source: CharSequence,
            start: Int,
            end: Int,
            dest: Spanned,
            dstart: Int,
            dend: Int
        ): CharSequence? {
            /* Matcher matcher=mPattern.matcher(dest);
        if(!matcher.matches())
            return "";*/
            val replacement = source.subSequence(start, end).toString()
            val newVal: String = (dest.subSequence(0, dstart).toString().toString() + replacement
                    + dest.subSequence(dend, dest.length).toString())
            val matcher: Matcher = mPattern.matcher(newVal)
            if (matcher.matches()) return null
            return if (TextUtils.isEmpty(source)) dest.subSequence(dstart, dend) else ""
        }

        init {
//        mPattern=Pattern.compile("[0-9]*+((\\.[0-9]{0," + (iDecimalSize-1) + "})?)||(\\.)?");
//        mPattern=Pattern.compile( "[-+]?^[0-9]+(.[0-9]{0," + (iDecimalSize - 1) + "})?$");
            mPattern =
                Pattern.compile("[-+]?[0-9]*$iBeforeZero}+((\\.[0-9]{0,$iDecimalSize})?)|(\\.)?")
        }
    }

    fun dotLineView(context: Context?, color: Int, height: Int): View {
        return dotLineView(context, color, height, UIHelper.i2px, UIHelper.i2px)
    }

    fun dotLineView(
        context: Context?,
        color: Int,
        height: Int,
        marginLeftRight: Int,
        marginTopBottom: Int
    ): View {
        val divider = LinearLayout(context)
        divider.layoutParams =
            ViewGroup.LayoutParams(UIHelper.i40px, ViewConst.WRAP_CONTENT)
        divider.background = VDrawableUtils.lineCenterDrawable(color, height.toFloat())
        divider.setPadding(marginLeftRight, marginTopBottom, marginLeftRight, marginTopBottom)
        divider.clipToOutline = false

        return divider
    }

    /*todo move to util class*/
    fun getNavBarHeight(c: Context): Int {
        val result = 0
        val hasMenuKey: Boolean = ViewConfiguration.get(c).hasPermanentMenuKey()
        val hasBackKey: Boolean = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)
        if (!hasMenuKey && !hasBackKey) {
            //The device has a navigation bar
            val resources: Resources = c.resources
            val orientation: Int = resources.getConfiguration().orientation
            val resourceId: Int
            resourceId =
                resources.getIdentifier(
                    if (orientation == Configuration.ORIENTATION_PORTRAIT) "navigation_bar_height" else "navigation_bar_width",
                    "dimen",
                    "android"
                )

            if (resourceId > 0) {
                return resources.getDimensionPixelSize(resourceId)
            }
        }
        return result
    }

    fun pxToDp(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }

    fun getStatusBarHeight(resources: Resources): Int {
        var result = 0
        val resourceId: Int = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun getIconFontBoundedDrawable(
        context: Context,
        iconRes: Int,
        iconSize: Float = 24f,
        @ColorInt iconColor: Int = ThemeConstant.textBlackColor
    ): IconFontDrawable {
        val iconFontDrawable =
            IconFontDrawable(context, iconRes)

        iconFontDrawable.textSize = iconSize
        iconFontDrawable.setBounds(0, 0, iconSize.toInt(), iconSize.toInt())
        iconFontDrawable.setTextColor(iconColor)
        return iconFontDrawable
    }

    fun getIconFontDrawable(
        context: Context,
        iconRes: Int,
        iconSize: Float = 24f,
        @ColorInt iconColor: Int = ThemeConstant.textBlackColor
    ): IconFontDrawable {
        val iconFontDrawable =
            IconFontDrawable(context, iconRes)

        iconFontDrawable.textSize = iconSize
//        iconFontDrawable.setBounds(0, 0, iconSize.toInt(), iconSize.toInt())
        iconFontDrawable.setTextColor(iconColor)
        return iconFontDrawable
    }

    fun setDurationAndCostForTwo(restaurantItem: ListElement): String {
        val data: String

        if (restaurantItem.duration > 0.0 && (!restaurantItem.costForTwo.isNullOrEmpty())) {
            if (restaurantItem.duration == 1.0) {
                data =
                    restaurantItem.duration.toInt()
                        .toString() + IConstants.DeliveryTime.MIN + IConstants.DeliveryTime.COSTFOR2 + restaurantItem.costForTwo

            } else {

                data =
                    restaurantItem.duration.toInt()
                        .toString() + IConstants.DeliveryTime.MINS + IConstants.DeliveryTime.COSTFOR2 + restaurantItem.costForTwo
            }
        } else if (restaurantItem.duration == 0.0 && (!restaurantItem.costForTwo.isNullOrEmpty())) {
            data = IConstants.DeliveryTime.COSTFOR2 + restaurantItem.costForTwo.toString()

        } else if (restaurantItem.duration > 0.0 && (restaurantItem.costForTwo.isNullOrEmpty())) {
            if (restaurantItem.duration == 1.0) {
                data =
                    restaurantItem.duration.toInt().toString() + IConstants.DeliveryTime.MIN

            } else {

                data =
                    restaurantItem.duration.toInt().toString() + IConstants.DeliveryTime.MINS
            }
        } else {
            data = ""
        }
        return data

    }

    @JvmStatic
    fun isGPSEnabled(activity: AppCompatActivity): Boolean {
        val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }


    fun ensureResult(runnable: Runnable, duration: Long = 1000) {
        Handler(Looper.getMainLooper()).postDelayed(
            runnable,
            duration
        )
    }

    fun getLocationDistance(subTitle: String, distance: String?): CharSequence? {
        if (distance.isNullOrEmpty()) {
            return subTitle
        } else {
            return "$subTitle | $distance kms"

        }


    }

    fun appendUri(uri: String?, appendQuery: String): URI {
        val oldUri = URI(uri)
        var newQuery: String = oldUri.getQuery()
        newQuery += "&$appendQuery"
        return URI(oldUri.scheme, oldUri.authority,
                oldUri.path, newQuery, oldUri.fragment)
    }


}


