package com.yumzy.orderfood.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.AdapterPermissionRequiredBinding

/**
 * Created by Shriom Tripathi on 11-Sept-20.
 */
class PermissionRequiredItem : AbstractBindingItem<AdapterPermissionRequiredBinding>() {

    private var title: String = ""
    private var subtitle: String = ""
    private var listener: View.OnClickListener? = null


    override val type: Int
        get() = R.id.item_permission

    override fun bindView(binding: AdapterPermissionRequiredBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.title = title
        binding.subtitle = subtitle
        binding.clickListener = listener
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterPermissionRequiredBinding {
        return AdapterPermissionRequiredBinding.inflate(inflater, parent, false)
    }

    fun withTitle(title: String): PermissionRequiredItem {
        this.title = title
        return this
    }


    fun withDescription(description: String): PermissionRequiredItem {
        this.subtitle = description
        return this
    }

    fun withListener(listener: View.OnClickListener): PermissionRequiredItem {
        this.listener = listener
        return this
    }


}