package com.yumzy.orderfood.util.contact

import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import com.yumzy.orderfood.data.models.ContactModelDTO
import java.util.*

object ContactHelper {
    private val contactList: MutableList<ContactModelDTO> = ArrayList()
    fun readContacts(context: Context, query: String?): List<ContactModelDTO> {
        contactList.clear()
        val projection = arrayOf(
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
            ContactsContract.CommonDataKinds.Photo.PHOTO_THUMBNAIL_URI
        )
        val cursor: Cursor?
        cursor = if (query == null || query == "") {
            context.contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC "
            )
        } else {
            val mQuery = arrayOf("%$query%")
            context.contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? OR " + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ? ",
                mQuery,
                ContactsContract.Contacts.DISPLAY_NAME + " ASC "
            )
        }
        if (cursor != null && cursor.count > 0) {
            while (cursor.moveToNext()) {
                val name = cursor.getString(0)
                val phone = cursor.getString(1)
                val photo = cursor.getString(2)
                if (!(phone == null || phone == "")) {
                    contactList.add(ContactModelDTO(name, photo, phone))
                }
            }
            cursor.close()
        }
        return contactList
    }

    fun formatPhone(phone: String): String {
        var phone = phone
        phone = phone.replace("\\s".toRegex(), "")
        if (phone.length == 10) {
            phone = "+91$phone"
        } else if (phone.length == 11) {
            phone = "+91" + phone.substring(1)
        }
        return phone
    }
}