package com.yumzy.orderfood.util

import android.graphics.Color
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.data.models.base.APIConstant

interface IConstants {
    companion object {
        const val APP_ID = 1001
        const val REQ_CODE_VERSION_UPDATE = 530
        const val APP_TAG = "YUMZY_APP_V_2"//for Timber Log and printing log file

        /* Prodiction URL */
//        const val BASE_URL = "https://api.yumzy.ai/suggestionv2/" //production url

        /* Developer sent URL */
        const val BASE_URL = BuildConfig.BASE_URL

        /* Staging */
        //const val BASE_URL = "https://api.staging.yumzy.ai/suggestionv2/" //internal

        //        const val BASE_URL = "http://68.183.89.81:1337" //BASE url
        const val SHARE_PREF_NAME = "yumzy_app"

    }

    /*all your urls below*/

    enum class URLS(val uRL: String) {
        LOGIN(APIConstant.URL.LOGIN),
        SIGN_UP(APIConstant.URL.SIGN_UP),
    }

    enum class OfferConstant(val value: String) {
        NET("%"),
        PERCENTAGE("Rs."),
    }




    enum class ScratchCardType(
        val redeemType: Int,
        val colorCode: Int,
        val action: String
    ) {
        POINTS(80, Color.RED, "points"),
        URL_TYPE(81, Color.GRAY, "url"),
        INVITE(80, Color.BLUE, "invite"),
    }

    enum class OrderStatus(
        val statusId: Int,
        val colorCode: String,
        val action: String
    ) {
        APPROVED(Approval.APPROVED, "#008000", "Approve"),
        PENDING(Approval.PENDING, "#FFA500", "Pending"),
        REJECTED(Approval.REJECTED, "#FF0000", "Reject"),
        STOPPED(Approval.STOPPED, "#A9A9A9", "Stop"),
        MORE_INFO(Approval.WAITING, "#FF0000", "Waiting");
    }

    enum class LocationStatus(
        val actionTitle: String,
        val actionIDescription: String,
        val actionButtonOne: String,
        val actionButtonTwo: String
    ) {
        SERVING_AT_LOCATION(
            "Current location",
            "We are delivering at your current location.",
            "OK",
            "Change location"
        ),
        NOT_SERVING_AT_LOCATION(
            "Not Serving",
            "We are not serving at your current location.",
            "Close",
            "Change location"
        ),
        DIFFERENT_CURRENT_LOCATION(
            "Different Location",
            "It seems your current location is not as same as previous location. Please update for better service.",
            "I'll do it later",
            "Yes, Let's update it"
        ),
        NO_LOCATION_FOUND(
            "No location found",
            "Please add your location for better service.",
            "No, May be later",
            "Yes, Let's pick it"
        )
    }

    interface Approval {
        companion object {
            const val APPROVED = 114
            const val PENDING = 115
            const val STOPPED = 116
            const val REJECTED = 117
            const val WAITING = 142
        }

    }

    interface AccountAction {
        companion object {
            const val MANAGE_ADDRESS = 4401 // Manage Address
            const val FOOD_PREFERENCE = 4402//Food Preferences
            const val MY_WALLET = 4403 //My Wallet
            const val FAVORITE = 4404  //Favorite
            const val MY_REWARDS = 4405 //My Rewards
            const val ORDER_HISTORY = 4406 //Orders History
            const val HELP_FAQ = 4407 //Help& FAQs
            const val SIGN_OUT = 4408 //Sign Out
            const val INVITE_FRIEND = 4409 //Sign Out
            const val PROFILE = 4410 //Sign Out
            const val SEND_FEEDBACK = 4411 //Sign Out
        }

    }

    /*all your module*/
    interface ActionModule {
        companion object {

            const val CONFIRM_DIALOG = 4249
            const val OTP_VERIFICATION = 4250
            const val TOP_RESTAURANT = 4251
            const val APPLY_REFERRAL = 4252
            const val FAVOURITE_RESTAURANT = 4253
            const val ADD_ADDRESS = 4254
            const val CUSTOMIZE_ORDER = 4255
            const val FOOD_PREFERENCE = 4256
            const val REVIEW = 4257
            const val APPLY_RATING = 4258
            const val SELECT_RESTAURANT = 4259
            const val SELECT_DELIVER_LOCATION = 4260
            const val CONFIRM_LOCATION = 4261
            const val CONFIRM_BACK_PRESS = 4262
            const val FOOD_PERSONALISATION = 4263
            const val ORDER_TRACKING = 4264
            const val ASK_GPS = 4265
            const val SAVED_ADDRESS = 4266
            const val UPDATE_LOCATION_HEAD = 4267
            const val TOP_DISHES = 4268
            const val TOP_RESTAURANTS = 4269
            const val RESTAURANT_ITEM_SEARCH = 4270
            const val SEE_ALL = 4271
            const val RECYCLER_SEE_ALL = 4272
            const val RATE_US = 4273
            const val EXIT_PAYMENT = 4273
            const val EXIT_APP = 4274
            const val ENABLE_GPS_REQUEST = 4275
            const val SIGN_OUT = 4276
            const val OPEN_PLATE = 4277
            const val CLEAR_PLATE = 4278
            const val UPLOAD_PROFILE = 4279
            const val USER_NOT_FOUND = 4280
            const val USER_EXISTS = 4281
            const val ENABLE_PERMISSION = 4282
            const val OPEN_PLATE_CUSTOMIZATION = 4283
            const val ITEM_REPEAT = 4284
            const val ENABLE_GPS = 4283
            const val FETCHING_LOCATION = 4284


        }
    }

    interface UserModule {
        companion object {
            const val USER_EMAIL = 4350
            const val USER_NUMBER = 4351
            const val USER_IMAGE = 4352
            const val USER_CLASS = 4353
            const val USER_NAME = 4354
        }
    }


    interface CuisineModule {
        companion object {
            const val CUISINE_VIEW = 4450
            const val CUISINE_IMAGE = 4451
            const val CUISINE_TITLE = 4452
            const val CUISINE_ID = 4453

            const val CUISINE_TYPE_VEG = 4453
            const val CUISINE_TYPE_NON_VEG = 4454
            const val CUISINE_TYPE__ANY = 4455

        }
    }

    interface SearchModule {
        companion object {
            const val SEARCH_ICON = 4550
            const val SEARCH_INPUT_FIELD = 4551
            const val CLOSE_ICON = 4552
        }
    }


    interface ConfirmLocationModule {
        companion object {
            const val VIEW_ICON_IMAGE = 4650
            const val VIEW_TEXT_TITLE = 4651
            const val VIEW_TEXT_FULL_ADDRESS = 4652
            const val VIEW_TEXT_FLAT_DETAIL = 4653
            const val VIEW_TEXT_LANDMARK = 4654
            const val VIEW_TEXT_OTHERS = 4659
            const val VIEW_BTN_CHANGE = 4655
            const val VIEW_BTN_CONFIRM_LOCATION = 4656
            const val VIEW_CONTAINER_DETAIL_ADDRESS = 4657
            const val VIEW_CONTAINER_ADDRESS_TYPE = 4658
        }
    }

    interface LocationPickerModule {
        companion object {
            const val REQ_CODE = 5761
            const val ADDRESS_TYPE_UPDATE = "update"

        }
    }

    interface FastAdapter {
        companion object {
            const val REST_ADD_ITEM_VIEW = 9231
            const val REST_RECCOMEND_ITEM_VIEW = 9232
            const val REST_MENU_SECTION = 9233
            const val REST_DETAIL_VIEW = 9234
            const val REST_FASSI_VIEW = 9235
            const val REST_TOP_DISH_ITEM_VIEW = 9236
            const val REST_TOP_DISH_RECCOMEND_VIEW = 9237
        }
    }

    interface OnPlaceRestaurantHeader {
        companion object {
            const val VIEW_TEXT_TITLE = 4750
            const val VIEW_TEXT_SUB_TITLE = 4751
            const val VIEW_TEXT_VIEW_MORE = 4752
            const val OUTLET_IMAGE = 4753
        }
    }

    interface TagTextView {
        companion object {
            const val VIEW_TEXT_TAG = 4850
        }
    }

    interface ManagerAddress {
        companion object {
            const val ACTION_EDIT = 0
            const val ACTION_DELETE = 1
            const val UPDATE_MANUALLY = 120
            const val UPDATE_AUTOMATICALLY = 121
            const val CLOSE_DIALOG = 122
        }
    }

    interface OrderHistory {
        companion object {
            const val ACTION_REORDER = 0
            const val ACTION_REVIEW = 1
            const val ACTION_REORDER_BUTTON = 2
        }
    }

    interface UploadFrom {
        companion object {
            const val GALLERY = 0
            const val CAMERA = 1
        }
    }

    interface HorzontalYellowCard {
        companion object {
            const val VIEW_TITLE = 5670
            const val VIEW_SUBTITLE = 5671
            const val VIEW_IMAGE = 5672
            const val VIEW_RATING = 5673
            const val VIEW_RANGE = 5674
            const val VIEW_OFFER = 5675
            const val VIEW_LOCALITY = 5676
        }
    }

    interface PopularFoodItemView {
        companion object {
            const val VIEW_IMAGE = 6670
            const val VIEW_RIGHT_CONTAINER = 6671
            const val VIEW_TITLE = 6672
            const val VIEW_SUBTITLE = 6673
            const val VIEW_PRICE_CONTAINER = 6674
            const val VIEW_BUTTON = 6675
            const val VIEW_BUTTON_CONTAINER = 6676
        }
    }

    interface TimelineHeaderView {
        companion object {
            const val VIEW_ICON = 7770
            const val VIEW_TITLE = 7771
        }
    }

    enum class RegisterStep {
        STEP_ONE,
        STEP_TWO,
        STEP_THREE,
    }

    /*Sourav Kumar Pandit*/
    interface SHARED_PREF {
        companion object {
            const val SINGLE_CART = "single_cart"
            const val CART_ID = "user_cart"
            const val FULL_ADDRESS = "full_address"
            const val LOCALITY = "locality"
            const val HAS_LOCATION = "user_has_location"
            const val DEVICE_ID = "device_id"
            const val USER_ID = "user_id"
            const val IS_ADDRESS_SELECTED = "address"
            const val DEVICE_LAT = "device_lat"
            const val DEVICE_LONG = "device_long"
            const val USER_NAME = "user_name"
            const val USER_LAST_NAME = "user_last_name"
            const val USER_EMAIL = "user_email"
            const val USER_PIC = "user_pic"
            const val USER_PHONE = "user_pjone"
            const val CURRENT_ORDER_ID = "current_order_id"
            const val CART_OUTLET_ADDON = "cart_outlet_addon"
            const val OUTLET_ID = "outlet_id"
            const val USER_FCM_TOKEN = "user_fcm_token"
            const val USER_REFERRAL_CODE = "referral_code"
            const val USER_REFERRAL_LINK = "referral_link"
            const val REFERRED_BY = "referredBy"
            const val IS_NEW_USER = "is_new_user"
            const val IS_FRESH_USER = "is_fresh_user"
            const val USER_LAST_LOCATION = "last_location"
            const val USER_SELECTED_ADDRESS = "selected_address"
            const val ADDRESS_TAG = "address_tag"
            const val IS_TEMP = "is_temp"
        }

    }
    /*</Sourav Kumar Pandit*/

    interface UserUploadModule {
        companion object {
            const val USER_EMAIL_KEY = "email"
            const val USER_NUMBER_KEY = "mobile"
            const val USER_PROFILE_PIC = "profilePicLink"
            const val USER_NAME_KEY = "name"
        }
    }

    interface ActivitiesFlags {
        companion object {
            const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X"
            const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"


            const val ADDRESS_PICKER_REQUEST = 1020
            const val AUTOCOMPLETE_REQUEST_CODE = 1450
            const val LOCATION_PERMISSION_REQUEST_CODE = 1451
            const val LOCATION_NOT_FOUND = 1452


            const val SHOW_PLATE = "show_plate"
            const val PAYMENT_RESPONSE = "payment_response"
            const val PAYMENT_BODY = "payment_message"
            const val PAYMENT_TITLE = "payment_title"
            const val EARNED_SCRATCH = "earnedScratch"
            const val OUTLET_ID = "outletId"
            const val PROMO_ID = "promoId"
            const val DEEP_LINK = "deepLink"
            const val ITEM_ID = "itemId"
            const val RECIPE_ID = "recipeId"
            const val RECIPE_NAME = "recipeName"
            const val FLAG = "flag"
            const val NEW_USER = "isNewUser"
            const val RESULT_KEY = "resultFlag"
            const val COUPON_ID = "couponId"
            const val URL = "url"
            const val ADDRESS = "address"
            const val PAYMENT_LINK = "payment_link"
            const val PAYMENT_REDIRECT = "payment_redirect"
            const val IMAGE_BITMAP = "image_bitmap"
            const val FROM_HOME = 4900
            const val FROM_HELP = 4901
            const val FROM_PLATE = 4902
            const val FROM_ACCOUNT = 4903
            const val FROM_SAVED_ADDRESS = 4904
            const val FROM_MANAGE_ADDRESS = 4905
        }
    }

    interface PermissionConst {
        companion object {
            const val LOCATION_ACCESS = 552
            const val REQUEST_PERMISSIONS_REQUEST_CODE = 34
            const val LOCATION_REQUEST_CODE = 4000
            const val PLAY_SERVICES_REQUEST = 1000
            const val REQUEST_CHECK_SETTINGS = 2000
            const val REQUEST_CODE_GPS = 3000

        }
    }

    interface RatingFlags {
        companion object {
            const val RATING_0_0 = "0.0"

            //const val RATING_0_5 = "0.5"
            const val RATING_1_0 = 1.0f

            //    const val RATING_1_5 = 1.5
            const val RATING_2_0 = 2.0f

            //  const val RATING_2_5 = 2.5
            const val RATING_3_0 = 3.0f

            //  const val RATING_3_5 = 3.5
            const val RATING_4_0 = 4.0f

            // const val RATING_4_5 = 4.5
            const val RATING_5_0 = 5.0f

        }
    }

    interface AppStart {
        companion object {
            const val TRACKING = "tracking"
            const val RATE = "rate"
            const val RAIN = "rain"

        }
    }

    interface OrderTracking {
        companion object {
            const val CHANNEL_ID = "com.yumzy.orderfood.ORDER_TRACK_CHANNEL"
            const val CHANNEL_NAME = "Yumzy Order Tracking"

            var ORDER_TRACKING_DURATION = 10000L
            var ORDER_REFRESH_RATE = 15000L
            const val ORDER_TRACK_DEFAULT_NOTIFY_ID = 75000
            const val ORDER_TRACK_REQUEST_CODE = 75001
            const val ORDER_TRACK_ID = "tracking_order_id"
            const val ORDER_TRACK_DATA = "order_track_data"
            const val ORDER_TRACK_BROADCAST = "order_track_broadcast_yumzy"

            const val ORDER_STATUS_ACCEPTANCE_WAIT = "acceptanceWait"
            const val ORDER_STATUS_VALET_ON_WAY_TO_CONFIRM = "valetOnWayToConfirm"

            const val ORDER_STATUS_FOOD_PREPARATION = "foodPreparation"
            const val ORDER_STATUS_FOOD_READY = "foodReady"
            const val ORDER_STATUS_WAITIN_DELIVERY_PICKUP = "waitingDeliveryPickup"

            const val ORDER_STATUS_OUT_FOR_DELIVERY = "outForDelivery"
            const val ORDER_STATUS_DELIVERED = "delivered"

            const val ORDER_STATUS_INITIATED = "initiated"
            const val ORDER_STATUS_IN_PAYMENT = "inPayment"
            const val ORDER_STATUS_COMPLETE = "complete"
            const val ORDER_STATUS_CANCELLED = "cancelled"
            const val ORDER_STATUS_REJECTED = "rejected"
            const val ORDER_STATUS_IN_DISPUTE = "inDispute"
            const val ORDER_STATUS_ENFORCED_CANCEL = "enforcedCancel"
            const val DELIVERY_UNAVAILABLE = "Delivery Executive is currently unavailable"
            const val NO_INTERNET = "\nOops! No internet!"
            const val NO_INTERNET_MSG =
                "Please check your internet connection, without it we can't serve you!\n"

        }
    }

    interface NotificationChannel {
        companion object {
            const val UPDATE_SERVICE = "UpdateLocation"


        }
    }

    interface Help {
        companion object {
            const val INTENT_HELP = "intent_help_id"
            const val HELP_ITEM_ID_CONSTANT = 4264
        }
    }

    interface LayoutType {
        interface RestaurantsType {
            companion object {
                const val RESTAURANT_LABEL = "category"
                const val RESTAURANT_ITEM = "item"
            }

        }

        interface Banner1000 {
            companion object {
                const val HEADING = -1000 //done
                const val TOP_BANNER = 1000//done //heading  wont come
                const val OFFER_ADS = 1001//done //heading  wont come
                const val THE_BRANDS = 1002//done //see all wont come
                const val ADS_ITEM = 1003//done //heading wont come
                const val POPULAR_CUISINES = 1004 //done //see all wont come
            }
        }

        interface Restaurant2000 {
            companion object {
                const val TOP_RESTAURANTS = 2001 //done //see all  come
                const val NEW_LAUNCHED = 2002 //done  //see all  come
                const val YUMZY_EXCLUSIVE = 2003//done  //see all  come
                const val TRENDING_RESTAURANTS = 2004 //done  //see all  come
                const val FEATURED_BRANDS = 2005//done  //see all  come
            }
        }

        interface Dish3000 {
            companion object {
                const val YUMZY_BEST = 3001//done  //see all  come
                const val WINTER_SPECIAL = 3002 //done //see all wont come
                const val POPULAR_FOOD = 3003//done  //see all  come
                const val TRENDING_FOODS = 3004//done  //see all  come
            }
        }

        interface Web4000 {
            companion object {
                const val WEB_4001 = 4001
            }
        }



        companion object {
            const val TEST_LAYOUT = 104
            const val RESTAURANT_LABEL = 301//not from backend
            const val RESTAURANT_ITEM = 302//not from backend


//            const val BANNER = 10
//            const val TOP_RESTAURANT = 15
//            const val TOP_BRANDS = 20
//            const val OFFER_ADS = 25
//            const val NEW_LAUNCH = 30
//            const val YUM_EXCLUSIVE = 35
//            const val SINGLE_OFFER_ADS = 40
//            const val TRENDING_RESTAURANTS = 45
//            const val FEATURED_TINDER = 50
//            const val POPULAR_CUISINES = 55
//            const val YUMZY_BEST = 60
//            const val WINTER_SPECIAL = 65
//            const val POPULAR_FOOD = 70

            const val Banner1000 = 1000
            const val Restaurant2000 = 2000
            const val Dish3000 = 3000
            const val VERTICAL = 1
            const val HORIZONTAL = 0
        }

    }

    enum class ExploreSuggestion {
        NO_RESULT,
        EMPTY_TEXT,
        SHOW_SUGGESTION,
        LIMITED_SUGGESTION,
        EXPANDED_SUGGESTION

    }

    interface LottieImage {
        companion object {
            const val NOT_SERVICE_DOG = "https://assets4.lottiefiles.com/temp/lf20_tsdGdl.json"
            const val NOT_SERVICE_CAT = "https://assets4.lottiefiles.com/temp/lf20_l0ORt3.json"
            const val RATING_LINK =
                "https://assets10.lottiefiles.com/datafiles/6lq0JF4GbfKULT1/data.json"

            const val OTP_ANIM =
                "https://assets10.lottiefiles.com/datafiles/dqQoip6K5bE1jVw/data.json"

            const val UPDATE_LOGO = "https://assets6.lottiefiles.com/packages/lf20_NXqR2d.json"
            const val DELIVERY_BOY = "https://assets6.lottiefiles.com/packages/lf20_A9VAQE.json"
            const val APPLY_COUPONS = "https://assets10.lottiefiles.com/packages/lf20_zYdCqj.json"
//            - https://assets10.lottiefiles.com/temp/lf20_sTumYD.json

            const val REWARDS = "https ://assets10.lottiefiles.com/packages/lf20_u0Fzi2.json"
//            - https://assets6.lottiefiles.com/packages/lf20_ihxIYc.json

            const val GIFT_BOX =
                "https://assets6.lottiefiles.com/datafiles/zc3XRzudyWE36ZBJr7PIkkqq0PFIrIBgp4ojqShI/newAnimation.json"

            const val LIKES = "https://assets9.lottiefiles.com/packages/lf20_wMezg6.json"

            const val PAYMENT_FAILED = "https://assets7.lottiefiles.com/packages/lf20_Dum1s3.json"
            const val DUCK_DOWN = "https://assets8.lottiefiles.com/temp/lf20_B7BO04.json"

            const val PAYMENT_SUCCESS = "https://assets7.lottiefiles.com/packages/lf20_7W0ppe.json"

            const val MONEY_COIN = "https://assets7.lottiefiles.com/temporary_files/EGBZKv.json"
            const val NO_INTERNET = "https://assets5.lottiefiles.com/packages/lf20_nTfkVR.json"
        }
    }

    interface PaymentConst {
        companion object {

            const val PROCESS_PAYMENT = 8555
            const val PAYMENT_CANCELED = 8524
            const val PAYMENT_FAILED = 8525
            const val PAYMENT_SUCCESS = 8526
            const val PAYMENT_NOT_REQUIRED = 8527


        }


    }

    interface DiscountType {
        companion object {
            const val PERCENT = "percent"
            const val PERCENTAGE = "percentage"
            const val NET = "net"


        }


    }

    interface IBorderType {
        companion object {
            const val DEFAULT = 0
            const val LEFT = 1
            const val RIGHT = 2
            const val TOP = 3
            const val BOTTOM = 4
            const val LEFT_RIGHT = 5
            const val TOP_BOTTOM = 6
        }
    }

    interface AppConstant {
        companion object {
//            const val AF_DEV_KEY = "8fgba3qyvssLWYT3u6LvB5"

            const val ZOHO_APP_KEY = "XMz2%2FwuyoHBi%2FisUXANsdz1VLa%2F%2BCv9k_in"
            const val ZOHO_ACCESS_KEY =
                "6Cs6q7aHnPd72ZBZsldQQMacn6HfS4U92sxiAuWG%2FrKSO7BZeVySKbq8w4%2BwkWG22iEswMTH3tuooCO6WTvbAdGsDdGxiJwsvdDigj%2Bt4%2BmKg9kEqERjEQ5Y6f05A68hYgPpRKKE5iFJfVLiR91gPMtMyVqX6c6t"
            const val DIALOG_DIM_AMMOUNT = 0.2f
            const val CLICK_DELAY = 500L
            var INVITE_TEXT = ""
            var SCRATCH_CARD_INVITE_TEXT = ""
        }

    }

    interface ActivityResultCode {
        companion object {
            const val SUCCESS = 101
            const val FAILURE = 102
            const val IS_SUCCESS = "success"
            const val IS_CANCEL = "cancel"
            const val FLAG = "ok"
        }

    }

    interface OutletOnOff {
        companion object {
            const val ON = "on"
            const val OFF = "off"

        }
    }

    interface LandingPageType {
        companion object {

            const val DISH: String = "dish"
            const val RESTAURANT: String = "restaurant"
            const val RESTAURANTS: String = "restaurants"
            const val DISHES: String = "dishes"
            const val URL: String = "url"
            const val INVITE: String = "invite"
        }

    }

    interface DeliveryTime {
        companion object {

            const val MIN: String = " min"
            const val MINS: String = " mins"
            const val COSTFOR2: String = " •  COST FOR TWO  :  ₹"
        }

    }

    interface ResponseError {
        companion object {

            const val UNKNOWN_ERROR: String = "Unknown Error"
            const val SOMTHING_HAPPENED: String = "Sorry, something unexpected happened!"
            const val NO_INTERNET = "No Internet Connection"
            const val NOT_CONNECTED = "You are not connected to the internet"
            const val OOPS_SOMTHING_HAPPENED = "Oops! Something happened!"
        }

    }

    interface WalletFlag {
        companion object {

            const val CREDIT: String = "credit"
            const val DEBIT: String = "debit"

        }

    }

    interface PaymentMethod {
        companion object {

            const val WALLET: String = "wallet"
            const val PAYMENTGATEWAY: String = "paymentGateway"

        }

    }

    interface WithDrawStatus {
        companion object {

            const val ACCEPTED: String = "ACCEPTED"
            const val SUCCESS: String = "SUCCESS"
            const val FAILURE: String = "FAILURE"

        }

    }

    interface FragmentFlag {
        companion object {

            const val FLAG = "flag"
            const val FROM_LOGIN: String = "FROM_LOGIN"
            const val FROM_REGISTER: String = "FROM_REGISTER"
            const val FROM_OTP_VERIFY: String = "FROM_OTP_VERIFY"
            const val FROM_EXPLORE: String = "FROM_EXPLORE"
            const val OPEN_LOGIN: String = "OPEN_LOGIN"
            const val OPEN_REGISTER: String = "OPEN_REGISTER"


        }

    }

}

object ViewConst {
    const val MATCH_PARENT = -1
    const val WRAP_CONTENT = -2
}