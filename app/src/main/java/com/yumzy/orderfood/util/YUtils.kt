package com.yumzy.orderfood.util

import android.content.Context
import android.graphics.Bitmap
import android.location.Location
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.preference.PreferenceManager
import com.bumptech.glide.request.RequestOptions
import com.github.twocoffeesoneteam.glidetovectoryou.GlideApp
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.maps.android.SphericalUtil
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


object YUtils {
    const val KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates"

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The [Context].
     */
    fun requestingLocationUpdates(context: Context?): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false)
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    fun setRequestingLocationUpdates(
        context: Context?,
        requestingLocationUpdates: Boolean
    ) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
            .apply()
    }

    /**
     * Returns the `location` object as a human readable string.
     * @param location  The [Location].
     */
    fun getLocationText(location: Location?): String {
        return if (location == null) "Unknown location" else "(" + location.latitude + ", " + location.longitude + ")"
    }

    fun getLocationTitle(context: Context): String {
        return context.getString(
            R.string.location_updated,
            DateFormat.getDateTimeInstance().format(Date())
        )
    }

    fun setImageInView(imageView: ImageView, imageUrl: String) {
        GlideApp
            .with(imageView)
            .load(imageUrl)
            .placeholder(getPlaceHolder())
            .thumbnail(0.4f)
            .apply(
                RequestOptions().override(
                    imageView.width,
                    imageView.height
                )
            )// can also be a drawable
            .into(imageView)
//        Glide.with(imageView.context).


    }

    fun plateLabel(value: String): String {
        when (value) {
            "packingCharges" -> return "Packaging Charge"
            "restaurantCharges" -> return "Restaurant Charge"
            "deliveryCharge" -> return "Delivery Charge"
            "gst" -> return "GST"

        }

        return ""
    }

    var ones = arrayOf(
        "",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
    )

    var tens = arrayOf(
        "", // 0
        "", // 1
        "twenty", // 2
        "thirty", // 3
        "forty", // 4
        "fifty", // 5
        "sixty", // 6
        "seventy", // 7
        "eighty", // 8
        "ninety"     // 9
    )

    /*Returns the word for input thiseric value*/
    fun Int.getWordCount(): String {
        return when {
            this < 20 -> ones[this]
            this < 100 -> ("""${tens[this / 10]}${if (this % 10 != 0) " " else ""}${ones[this % 10]}""")
            this < 1000 -> """${ones[this / 100]}  hundred ${(this % 100).let { """${tens[it / 10]}${if (it % 10 != 0) " " else ""}${ones[it % 10]}""" }}"""
            else -> ""
        }
    }

    fun getLocalBitmapUri(bitmap: Bitmap?, context: Context): Uri? {
        var bmpUri: Uri? = null
        try {
            val file = File(context.cacheDir, "yumzysharedimages")
            if (!file.exists()) {
                val result: Boolean = file.mkdirs()
                if (!result) {
                    return null
                }
            }
            val imageFile = File(file, "share_image_" + System.currentTimeMillis() + ".png")
            val out = FileOutputStream(imageFile)
            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.close()
            bmpUri = FileProvider.getUriForFile(
                context,
                BuildConfig.APPLICATION_ID.toString() + ".provider",
                imageFile
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }

    //distance in meters
    fun LatLng.toBounds(distance: Double): RectangularBounds {
        val center = this

        val ne = SphericalUtil.computeOffset(center, distance, 45.0)
        val sw = SphericalUtil.computeOffset(center, distance, 225.0)
        return RectangularBounds.newInstance(sw, ne)
    }

    private val id = AtomicInteger(1)
    fun getPlaceHolder(): Int {
        return when (id.getAndIncrement() % 4) {
          /*  0 -> {
                R.drawable.place_holder_1
            }
            1 -> {
                R.drawable.place_holder_2
            }
            2 -> {
                R.drawable.place_holder_3
            }*/
            else -> {
                R.drawable.place_holder_0
            }
        }
    }

    fun getPlaceHolder(position: Int): Int {
        return when (position % 4) {
            /*  0 -> {
                  R.drawable.place_holder_1
              }
              1 -> {
                  R.drawable.place_holder_2
              }
              2 -> {
                  R.drawable.place_holder_3
              }*/
            else -> {
                R.drawable.place_holder_0
            }
        }
    }
}