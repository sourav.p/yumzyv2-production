package com.yumzy.orderfood.util.viewutils.fontutils

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import com.yumzy.orderfood.R

class FontIconView : AppCompatTextView {
    private var isBrandingIcon = false
    private var isSolidIcon = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int = 0
    ) : super(context, attrs, defStyle) {
        initAttr(context, attrs)
        init()
    }


    private fun initAttr(context: Context, attrs: AttributeSet?) {
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            8,
            1000,
            1,
            TypedValue.COMPLEX_UNIT_SP
        )
        val a = context.theme.obtainStyledAttributes(
            attrs, R.styleable.FontIconView,
            0, 0
        )
        this.gravity = Gravity.CENTER
        isSolidIcon = a.getBoolean(R.styleable.FontIconView_solid_icon, false)
        isBrandingIcon = a.getBoolean(R.styleable.FontIconView_brand_icon, false)
    }


    private fun init() {
        typeface =
            when {
                isBrandingIcon -> FontCache[context, "fonts/yumzy_Icon.ttf"]
                isSolidIcon -> FontCache[context, "fonts/yumzy_Icon.ttf"]
                else -> FontCache[context, "fonts/yumzy_Icon.ttf"]
            }
    }
}