package com.yumzy.orderfood.util.viewutils

import android.R
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.drawable.*
import android.graphics.drawable.shapes.Shape
import com.laalsa.laalsalib.ui.VUtil.dpToPx

object VDrawableUtils {
    const val BORDER_TOP: Byte = 0
    const val BORDER_BOTTOM: Byte = 1
    const val BORDER_LEFT: Byte = 2
    const val BORDER_RIGHT: Byte = 3
    const val TOP_TO_BOTTOM: Byte = 0
    const val BOTTOM_TO_TOP: Byte = 1
    const val LEFT_TO_RIGHT: Byte = 2
    const val RIGHT_TO_LEFT: Byte = 3
    fun getLinearDrawable(iBackgroundColor: Int, iLayerColor: Int, iLayerType: Int): Drawable? {
        var iOrientation: GradientDrawable.Orientation? = null
        iOrientation =
            if (iLayerType == TOP_TO_BOTTOM.toInt()) GradientDrawable.Orientation.TOP_BOTTOM else if (iLayerType == BOTTOM_TO_TOP.toInt()) GradientDrawable.Orientation.BOTTOM_TOP else if (iLayerType == LEFT_TO_RIGHT.toInt()) GradientDrawable.Orientation.LEFT_RIGHT else if (iLayerType == RIGHT_TO_LEFT.toInt()) GradientDrawable.Orientation.RIGHT_LEFT else null
        var clBackground: GradientDrawable? = null
        if (iOrientation != null) {
            clBackground = GradientDrawable(
                iOrientation,
                intArrayOf(iBackgroundColor, iBackgroundColor, iLayerColor)
            ) //new int[]{startColor, centerColor, endColor}
            clBackground.setGradientCenter(0.5f, 0.5f)
            clBackground.shape = GradientDrawable.RECTANGLE
        }
        return clBackground
    }

    fun getLayerListDrawableBackground(
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Byte,
        iLayerHeight: Int
    ): Drawable {
        var iLayerHeight = iLayerHeight
        if (iLayerHeight <= 0) iLayerHeight = dpToPx(10)
        val clBackground = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(iBackgroundColor, iLayerColor)
        )
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.setColor(iLayerColor)
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)

        //setLayerInset(layer, leftOffset, topOffset, rightOffset, bottomOffset)
        clLinearDrawable.setLayerInset(0, 0, 0, 0, 0)
        clLinearDrawable.setLayerInset(
            1,
            if (iLayerType == BORDER_LEFT) iLayerHeight else 0,
            if (iLayerType == BORDER_TOP) iLayerHeight else 0,
            if (iLayerType == BORDER_RIGHT) iLayerHeight else 0,
            if (iLayerType == BORDER_BOTTOM) iLayerHeight else 0
        )
        return clLinearDrawable
    }

    fun getLayerListDrawable(
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Int,
        iLayerHeight: Int
    ): Drawable {
        var iLayerHeight = iLayerHeight
        if (iLayerHeight <= 0) iLayerHeight = dpToPx(10)
        val clBackground = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(iBackgroundColor, iLayerColor)
        )
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.setColor(iLayerColor)
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)

        //setLayerInset(layer, leftOffset, topOffset, rightOffset, bottomOffset)
        clLinearDrawable.setLayerInset(0, 0, 0, 0, 0)
        clLinearDrawable.setLayerInset(
            1,
            if (iLayerType == BORDER_LEFT.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_TOP.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_RIGHT.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_BOTTOM.toInt()) iLayerHeight else 0
        )
        return clLinearDrawable
    }

    fun getLayerListDrawable(
        iBackgroundColor: Int,
        clColorStateList: ColorStateList,
        iLayerType: Int,
        iBorderWidth: Int
    ): Drawable {
        var iBorderWidth = iBorderWidth
        if (iBorderWidth < 0) iBorderWidth = dpToPx(5)
        val clBackground = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(iBackgroundColor, clColorStateList.defaultColor)
        )
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.color = clColorStateList
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)

        //setLayerInset(layer, leftOffset, topOffset, rightOffset, bottomOffset)
        clLinearDrawable.setLayerInset(0, 0, 0, 0, 0)
        clLinearDrawable.setLayerInset(
            1,
            if (iLayerType == BORDER_LEFT.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_TOP.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_RIGHT.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_BOTTOM.toInt()) iBorderWidth else 0
        )
        return clLinearDrawable
    }

    fun getSelectorDrawable(color: Int): StateListDrawable {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(color))
        res.addState(intArrayOf(R.attr.state_selected), ColorDrawable(color))
        res.addState(intArrayOf(R.attr.state_activated), ColorDrawable(color))
        res.addState(intArrayOf(), ColorDrawable(Color.TRANSPARENT))
        return res
    }

    fun getRPLEffectPreLOP1(
        iSelectedColor: Int,
        iNormalColor: Int
    ): StateListDrawable {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_checked), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(), ColorDrawable(iNormalColor))
        return res
    }

    fun getRPLEffectPreLOP(
        iSelectedColor: Int,
        iNormalColor: Int
    ): StateListDrawable {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_selected), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_activated), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(), ColorDrawable(iNormalColor))
        return res
    }

    fun topAngledDrawable(color: Int, radius: Float): Drawable {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    paint.setShadowLayer(10f, 0f, -radius / 2, 0xffdedede.toInt())
                    paint.color = color
//                    paint.style=Paint.fi

                    val width = canvas.width.toFloat()
                    val height = canvas.height.toFloat()
                    mPath.reset()


                    mPath.moveTo(0f, height / 3f)
                    mPath.lineTo(width, radius)

                    mPath.lineTo(width, height - radius)


                    mPath.cubicTo(
                        width, height - radius,
                        width, height,

                        width - radius, height
                    )

                    mPath.lineTo(radius, height)
                    mPath.cubicTo(
                        radius, height,
                        0f, height,
                        0f, height - radius

                    )
                    mPath.lineTo(0f, height / 3f)

                    canvas.drawPath(mPath, paint)
                }
            }
        shape.height
        return ShapeDrawable(shape)
    }


    fun lineCenterDrawable(color: Int, height: Float): Drawable {
//        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
//                    paint.setShadowLayer(10f, 0f, -height/ 2, 0xffdedede.toInt())
                    paint.color = color
                    paint.style = Paint.Style.STROKE
                    paint.strokeCap = Paint.Cap.ROUND
                    paint.strokeWidth = height
                    val canvasWeight = canvas.width.toFloat()
                    val canvasHeight = canvas.height.toFloat()
                    canvas.drawLine(
                        height,
                        canvasHeight / 2,
                        canvasWeight - height,
                        canvasHeight / 2,
                        paint
                    )
                }
            }
        shape.height
        return ShapeDrawable(shape)
    }
}