package com.yumzy.orderfood.api

import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.api.session.SessionManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * A {@see RequestInterceptor} that adds an auth token to requests
 */
@Singleton
class AuthInterceptor  constructor(
    private val sessionManager: SessionManager?,
    val token: String? = null
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        // If token has been saved, add it to the request
        sessionManager?.fetchAuthToken()?.let {
            requestBuilder.addHeader("x-access-token", it)
            requestBuilder.addHeader("version", "${BuildConfig.VERSION_CODE}")
            requestBuilder.addHeader("versionName", BuildConfig.VERSION_NAME)
        }

        return chain.proceed(requestBuilder.build())
    }
}