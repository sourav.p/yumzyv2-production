/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */

package com.yumzy.orderfood.api.session

import com.yumzy.orderfood.data.SharedPrefsHelper
import javax.inject.Inject
import javax.inject.Singleton

class SessionManager  constructor(private val prefsHelper: SharedPrefsHelper) {

    var tempToken: String? = null

    companion object {
        const val USER_TOKEN = "user_token"
    }

    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String) {
        prefsHelper.put(USER_TOKEN, token)
    }

    fun saveTempAuthToken(token: String) {
        prefsHelper.put(USER_TOKEN, token)
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? {
        return prefsHelper[USER_TOKEN, tempToken]

    }

    /**
     * Function to remove auth token
     */
    fun removeAuthToken(){
        prefsHelper.deleteSavedData(USER_TOKEN)

    }
}