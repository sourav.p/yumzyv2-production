package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.UpdateLocationDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.*

interface AddressServiceAPI {

    @GET(APIConstant.URL.ADDRESS)
    suspend fun getAddressList(): Response<ResponseDTO<List<AddressDTO>>>

    @PUT(APIConstant.URL.ADDRESS)
    suspend fun updateAddress(@Body address: AddressDTO): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.ADDRESS)
    suspend fun addAddress(@Body address: AddressDTO): Response<ResponseDTO<Any>>

    @DELETE(APIConstant.URL.ADDRESS)
    suspend fun deleteAddress(@Query("id") id: String): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.UPDATE_LOCATION)
    suspend fun updateLocation(@Body updateLocation: UpdateLocationDTO): Response<ResponseDTO<Any>>
}