package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.OrderHistoryDTO
import com.yumzy.orderfood.data.models.OrderStatusDTO
import com.yumzy.orderfood.data.models.RatingFeedbackDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Query

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface OrderServiceAPI {

    @GET(APIConstant.URL.ORDER_HISTORY)
    suspend fun getOrderHistoryList(@Query("skip") skip: Int): Response<ResponseDTO<OrderHistoryDTO>>

    @GET(APIConstant.URL.ORDER_DETAILS)
    suspend fun orderDetails(@Query("orderId") id: String): Response<ResponseDTO<NewOrderDetailsDTO>>

    @GET(APIConstant.URL.ORDER_STATUS)
    suspend fun getOrderStatus(@Query("orderId") id: String): Response<ResponseDTO<OrderStatusDTO>>

    @PATCH(APIConstant.URL.RATE_ORDER)
    suspend fun getRatingFeedback(@Body selRatingFeedback: RatingFeedbackDTO): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.ORDER_DETAILS)
    fun enqueOrderDetails(@Query("orderId") id: String): Call<ResponseDTO<NewOrderDetailsDTO>>

}