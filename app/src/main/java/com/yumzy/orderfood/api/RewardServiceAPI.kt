package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.ScratchSummaryDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RewardServiceAPI {


    @POST(APIConstant.URL.APPLY_REFERRAL)
    suspend fun applyReferral(
        @Query("userId")
        userId: String,
        @Body mapReferral: Map<String, String>
    ): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.SCRATCH_CARD_SUMMARY)
    suspend fun getScratchCardInfo(): Response<ResponseDTO<ScratchSummaryDTO>>
}
