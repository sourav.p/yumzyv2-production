package com.yumzy.orderfood.data.ui


class BlobSubTitleStyleDTO (
    var subTitle: String,
    var style: Int,
    var color: Int,
    var shadow: Int
)