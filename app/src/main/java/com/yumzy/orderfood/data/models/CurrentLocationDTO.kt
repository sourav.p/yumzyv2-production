package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class CurrentLocationDTO(
    @field:SerializedName("coordinates") val coordinates: ArrayList<Double>?,
    @field:SerializedName("type") val type: String
)