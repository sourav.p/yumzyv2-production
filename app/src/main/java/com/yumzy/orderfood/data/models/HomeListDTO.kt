package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HomeListDTO(var flatLayoutType: Int) {
    @field:SerializedName("bannerText")
    var bannerText: String = ""

    @field:SerializedName("cuisines")
    var cuisines: List<String>? = null

    @field:SerializedName("dishId")
    var dishId: String = ""

    @field:SerializedName("displayOrderSeq")
    var displayOrderSeq: Int = 0

    @field:SerializedName("heroImage")
    var heroImage: HeroImageDTO? = null

    @field:SerializedName("image")
    var image: ImageDTO? = null

    @field:SerializedName("landingPage")
    var landingPage: String = ""

    @field:SerializedName("meta")
    var meta: MetaDTO? = null

    @field:SerializedName("outletId")
    var outletId: String = ""

    @field:SerializedName("promoId")
    var promoId: String? = ""

    @field:SerializedName("rating")
    var rating: String? = ""

    @field:SerializedName("subTitle")
    var subTitle: String = ""

    @field:SerializedName("title")
    var title: String = ""

    @field:SerializedName("isItemAdding")
    var isItemAdding: Boolean = false

    @field:SerializedName("prevQuantity")
    var prevQuantity: Int = 0

    @field:SerializedName("subLayoutType")
    var subLayoutType: Int = 1

    @Expose(serialize = false, deserialize = false)
    var nestedList: ArrayList<HomeListDTO>? = null

    @Expose(serialize = false, deserialize = false)
    var heading: String? = null

    @Expose(serialize = false, deserialize = false)
    var layoutType: Int? = null

    @Expose(serialize = false, deserialize = false)
    var orientation: Int? = null

    @Expose(serialize = false, deserialize = false)
    var subHeading: String? = null

    @Expose(serialize = false, deserialize = false)
    var firstPromoId: String? = null

}