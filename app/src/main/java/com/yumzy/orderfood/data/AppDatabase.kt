package com.yumzy.orderfood.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.yumzy.orderfood.data.dao.*
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import com.yumzy.orderfood.worker.SeedDatabaseWorker
import javax.inject.Singleton

/**
 * The Room database for this app
 */
@Database(
    entities = [UserDTO::class, CartResDTO::class, AddressDTO::class, NewOrderDetailsDTO::class,
        RestaurantDTO::class, RestaurantItem::class, ValuePairSI::class, RestaurantMenuSectionDTO::class
    ],
    version = 12, exportSchema = false
)
@TypeConverters(RoomConverters::class)
@Singleton
abstract class AppDatabase : RoomDatabase() {

    abstract fun userInfoDao(): UserInfoDao
    abstract fun cartResDao(): CartResDao
    abstract fun selectedAddressDao(): SelectedAddressDao
    abstract fun ordersDao(): OrdersDao
    abstract fun restaurantDao(): RestaurantDao
    abstract fun restaurantItemDao(): RestaurantItemDao
    abstract fun selectedItemDao(): SelectedItemDao
    abstract fun addressDao(): AddressDao
//    abstract fun selectedItems(): BaseDao<ValuePairSI>
//    abstract fun localSearchItemDao(): LocalSearchItemDao

    companion object {
        val DATABASE_NAME = "yumzy_app.db" //DATA_BASE Name

        // For Singleton instantiation
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
                        WorkManager.getInstance(context).enqueue(request)
                    }
                })
                .allowMainThreadQueries()
                .build()
        }
    }
}
