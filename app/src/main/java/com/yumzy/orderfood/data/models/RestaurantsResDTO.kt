package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

/*
 Created by Bhupendra Kumar Sahu on 30-May-20.
 */
data class RestaurantsResDTO(
    @field:SerializedName("title") val title: String,
    @field:SerializedName("list") val list: List<ListElement>,
    @field:SerializedName("skip") val skip: Int,
    @field:SerializedName("promoDetailsImage") val promoDetailsImage: String
)

data class ListElement(
   @field:SerializedName("offers") val offers: List<OfferDTO>,
   @field:SerializedName("outletName") val outletName: String,
   @field:SerializedName("outletId") val outletId: String,
   @field:SerializedName("imageUrl") val imageUrl: String,
   @field:SerializedName("locality") val locality: String,
   @field:SerializedName("costForTwo") val costForTwo: String?,
   @field:SerializedName("rating") val rating: String?,
   @field:SerializedName("haveAvailableItems") val haveAvailableItems: Boolean,
   @field:SerializedName("cuisines") val cuisines: List<String>,
   @field:SerializedName("duration") val duration: Double,
   @field:SerializedName("subTitle") var subTitle: String = ""
)