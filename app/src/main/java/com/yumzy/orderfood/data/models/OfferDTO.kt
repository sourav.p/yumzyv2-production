package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class OfferDTO(
    @field:SerializedName("description") val description: String,
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("endDate") val endDate: String,
    @field:SerializedName("endTime") val endTime: String,
    @field:SerializedName("itemId") val itemId: String,
    @field:SerializedName("maxDiscount") val maxDiscount: Int,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Int,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("promoBudget") val promoBudget: Int,
    @field:SerializedName("startDate") val startDate: String,
    @field:SerializedName("startTime") val startTime: String,
    @field:SerializedName("usedBudget") val usedBudget: Double,
    @field:SerializedName("value") val value: Int
)