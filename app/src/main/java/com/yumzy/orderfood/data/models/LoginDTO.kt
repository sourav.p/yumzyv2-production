/*
 *   Created by Sourav Kumar Pandit  23 - 4 - 2020
 */

package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class LoginDTO(
    @field:SerializedName("token") val token: String,
    @field:SerializedName("user") val user: UserDTO,
    @field:SerializedName("isNew") val isNew: Boolean? = false
)

