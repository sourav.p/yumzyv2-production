package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScratchCardDTO(
    @field:SerializedName("creationDate") val creationDate: String,
    @field:SerializedName("scratchId") val scratchID: String,
    @field:SerializedName("amount") val amount: Double,
    @field:SerializedName("earnBy") val earnBy: String? = "",
    @field:SerializedName("beforeEarnBy") val beforeEarnBy: String? = "",
    @field:SerializedName("afterEarnBy") val afterEarnBy: String? = "",
    @field:SerializedName("scratchMessage") val scratchMessage: String,
    @field:SerializedName("scratched") var scratched: Boolean,
    @field:SerializedName("image") val image: String,
    @field:SerializedName("action") val action: ScratchActionDTO,
    @field:SerializedName("redeemType") val redeemType: Int,
    var position: Int,
) : Parcelable