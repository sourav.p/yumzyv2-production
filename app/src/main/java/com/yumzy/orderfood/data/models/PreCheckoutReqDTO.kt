package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class PreCheckoutReqDTO(
    @field:SerializedName("address") val address: PreCheckoutAddress?,
    @field:SerializedName("cartId") val cartId: String
)

data class PreCheckoutAddress(
    @field:SerializedName("longLat") val longLat: CartLongLatDTO,
    @field:SerializedName("city") val city: String?
)

data class CartLongLatDTO(
    @field:SerializedName("coordinates") val coordinates: ArrayList<Double>
)

