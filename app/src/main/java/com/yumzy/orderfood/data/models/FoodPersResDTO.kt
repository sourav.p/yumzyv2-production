package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Bhupendra Kumar Sahu.
 */
data class FoodPersResDTO
    (
    @field:SerializedName("likeTags") val likeTags: List<String>,
    @field:SerializedName("preferenceType") val preferenceType: String,
    @field:SerializedName("likeCategories") val likeCategories: List<String>,
    @field:SerializedName("likeTagsSelected") val likeTagsSelected: Boolean
)