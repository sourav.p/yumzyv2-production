package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class VPIDTO
    (
    @field:SerializedName("valid") val valid: Boolean,
    @field:SerializedName("vpa") val vpa: String
) : Parcelable