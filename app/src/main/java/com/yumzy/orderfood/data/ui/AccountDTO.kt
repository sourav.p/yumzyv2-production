package com.yumzy.orderfood.data.ui

data class AccountDTO(

    var actionId: Int,
    var icons: String,
    var title: String,
    var description: String

)