package com.yumzy.orderfood.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ExploreSearchDTO(
    @SerializedName("items")
    @Expose
    var items: ArrayList<SearchItemDTO>? = null,

    @SerializedName("outlets")
    @Expose
    var outlets: ArrayList<SearchItemDTO>? = null

)

data class SearchItemDTO(
    @SerializedName("itemId")
    @Expose
    var itemId: String? = null,

    @SerializedName("outletName")
    @Expose
    var outletName: String? = null,

    @SerializedName("outletId")
    @Expose
    var outletId: String? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @Expose
    val imageUrl: String
) : Comparable<SearchItemDTO> {
    override fun compareTo(other: SearchItemDTO): Int {
        return -1
    }
}
/*

data class SearchOutletDTO(
    @SerializedName("outletName")
    @Expose
    var outletName: String? = null,

    @SerializedName("outletId")
    @Expose
    var outletId: String? = null

)*/
