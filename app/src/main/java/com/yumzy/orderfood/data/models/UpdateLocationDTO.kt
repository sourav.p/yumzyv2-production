package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UpdateLocationDTO(
    @field:SerializedName("city") val city: String,
    @field:SerializedName("coordinates") val coordinates: List<String>,
    @field:SerializedName("locality") val locality: String
) : Parcelable