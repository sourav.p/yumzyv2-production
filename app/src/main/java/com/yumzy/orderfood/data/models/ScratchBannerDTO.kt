package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScratchBannerDTO(
    @field:SerializedName("title")val title: String,
    @field:SerializedName("description") val description: String
): Parcelable