package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WalletDTO(
    @field:SerializedName("availableBalance")var availableBalance: Double = 0.0,
    @field:SerializedName("maximumUsable") var maximumUsable:  Double = 0.0,
    @field:SerializedName("message")var message: String = "",
    @field:SerializedName("showMaxUse")val showMaxUse: Boolean = false,
    @field:SerializedName("showAddToWallet")val showAddToWallet: Boolean = false,
    @field:SerializedName("terms") val terms: List<String>

) : Serializable {

    override fun toString(): String {
        return """
            availableBalance = $availableBalance
        """.trimIndent()
    }

}