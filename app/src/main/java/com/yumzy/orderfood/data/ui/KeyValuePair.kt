/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */

package com.yumzy.orderfood.data.ui

data class KeyValueSI(var key: String, var value: Int)
data class KeyValueSSI(var key: String, var ke2: String, var value: Int)
data class KeyValueSII(var key: String, var value: Int, var value2: String)

//Key Value pair
data class KeyValueIS(var key: Int, var value: String)
data class KeyValueIIS(var key: Int, var key2: Int?, var value: String)
