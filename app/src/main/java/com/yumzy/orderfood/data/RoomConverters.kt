package com.yumzy.orderfood.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.models.*
import java.io.IOException
import java.util.*


/**
 * Type converters to allow Room to reference complex data types.
 */
class RoomConverters {
    val gson: Gson = appGson

    @Throws(IOException::class)
    /*private fun largeDataWrite(
        jsonObjectHolder: List<JsonObject>
    ) {
        val writer = JsonWriter(FileWriter())
        writer.beginArray()
        for (jsonObject in jsonObjectHolder) {
            gson.toJson(jsonObject, writer)
        }
        writer.endArray()
        writer.close()
    }*/
    @TypeConverter
    fun calendarToDateStamp(calendar: Calendar): Long = calendar.timeInMillis

    @TypeConverter
    fun dateStampToCalendar(value: Long): Calendar =
        Calendar.getInstance().apply { timeInMillis = value }

    @TypeConverter
    fun saveAddressList(listOfString: List<AddressDTO?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getAddressList(listOfString: String?): List<AddressDTO?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<AddressDTO?>?>() {}.type
        )
    }

    /*  CurrentLocationDTO List<Double?>? */
    @TypeConverter
    fun toDoubleList(listOfString: ArrayList<Double>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun fromDoubleList(value: String): ArrayList<Double>? {
        return gson.fromJson(
            value,
            object : TypeToken<ArrayList<Double>?>() {}.type
        )
    }

    @TypeConverter
    fun toCurrentLocationDTO(value: String): CurrentLocationDTO? {
//        value.isEmpty()
        return gson.fromJson(
            value,
            object : TypeToken<CurrentLocationDTO?>() {}.type
        )
    }

    @TypeConverter
    fun fromCurrentLocationDTO(categories: CurrentLocationDTO?): String? {
        return gson.toJson(categories)

    }

    @TypeConverter
    fun saveCartOutletDetailsList(listOfString: List<CartOutletDetailsDTO?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getCartOutletDetailsList(listOfString: String?): List<CartOutletDetailsDTO?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<CartOutletDetailsDTO?>?>() {}.type
        )
    }

    @TypeConverter
    fun toCartBillingDTO(value: String?): CartBilling? {
        return gson.fromJson(
            value,
            object : TypeToken<CartBilling?>() {}.type
        )
    }

    @TypeConverter
    fun fromCartBillingDTO(categories: CartBilling?): String? {
        return gson.toJson(categories)

    }


    @TypeConverter
    fun saveCouponList(listOfString: List<Coupon?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getCouponList(listOfString: String?): List<Coupon?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<Coupon?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveCoordinate(coordinate: Coordinate): String? {
        return gson.toJson(coordinate)
    }

    @TypeConverter
    fun getCoordinate(coordinate: String?): Coordinate? {
        return gson.fromJson(
            coordinate,
            Coordinate::class.java
        )
    }

    @TypeConverter
    fun saveDelivery(delivery: Delivery): String? {
        return gson.toJson(delivery)
    }

    @TypeConverter
    fun getDelivery(delivery: String?): Delivery? {
        return gson.fromJson(
            delivery,
            Delivery::class.java
        )
    }

    @TypeConverter
    fun saveOutletList(listOfString: List<com.yumzy.orderfood.data.models.orderdetails.Outlet?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getOutletList(listOfString: String?): List<com.yumzy.orderfood.data.models.orderdetails.Outlet?>? {
        return gson.fromJson(
            listOfString,
            object :
                TypeToken<List<com.yumzy.orderfood.data.models.orderdetails.Outlet?>?>() {}.type
        )
    }
    /*@TypeConverter
    fun saveRestaurant(restaurantDTO: RestaurantDTO): String? {
        return gson.toJson(restaurantDTO)
    }
    @TypeConverter
    fun getRestaurant(response: String) : RestaurantDTO? {
        return gson.fromJson(
            response,
            RestaurantDTO::class.java
        )
    }*/

    @TypeConverter
    fun saveRestaurantDetails(restaurantDTODTO: RestaurantDetailDTO): String? {
        return gson.toJson(restaurantDTODTO)
    }

    @TypeConverter
    fun getRestaurantDetails(response: String): RestaurantDetailDTO? {
        return gson.fromJson(
            response,
            RestaurantDetailDTO::class.java
        )
    }


    @TypeConverter
    fun saveRestaurantMenuSection(listOfString: List<RestaurantMenuSectionDTO>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getRestaurantMenuSection(listOfString: String?): List<RestaurantMenuSectionDTO>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<RestaurantMenuSectionDTO?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveMenuCategory(listOfString: List<RestaurantMenuCategoryDTO>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getMenuCategory(listOfString: String?): List<RestaurantMenuCategoryDTO>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<RestaurantMenuCategoryDTO>?>() {}.type
        )
    }

    @TypeConverter
    fun fromAddOnMap(value: String): Map<String, RestaurantAddOnDTO?> {
        val mapType = object : TypeToken<Map<String, RestaurantAddOnDTO?>>() {}.type
        return gson.fromJson(value, mapType)
    }

    @TypeConverter
    fun fromStringAddOnMap(map: Map<String, RestaurantAddOnDTO?>): String {
        return gson.toJson(map)
    }

    @TypeConverter
    fun saveRestaurantItem(listOfString: List<RestaurantItem?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getRestaurantItem(listOfString: String?): List<RestaurantItem?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<RestaurantItem?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveRestaurantItemOffer(listOfString: List<RestaurantItemOffer?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getRestaurantItemOffer(listOfString: String?): List<RestaurantItemOffer?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<RestaurantItemOffer?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveRestaurantItemAddititonalCharges(listOfString: List<RestaurantItemAddititonalCharges?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getRestaurantItemAddititonalCharges(listOfString: String?): List<RestaurantItemAddititonalCharges?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<RestaurantItemAddititonalCharges?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveListStringItem(listOfString: List<String?>?): String? {
        return gson.toJson(listOfString)
    }

    @TypeConverter
    fun getListStringItem(listOfString: String?): List<String?>? {
        return gson.fromJson(
            listOfString,
            object : TypeToken<List<String?>?>() {}.type
        )
    }

    @TypeConverter
    fun saveLatLong(latLong: LongLat?): String? {
        return gson.toJson(latLong)
    }

    @TypeConverter
    fun getLatLong(latLong: String?): LongLat? {
        return gson.fromJson(latLong, LongLat::class.java)
    }

    @TypeConverter
    fun saveJsonArray(jsonArray: JsonArray?): String? {
        return gson.toJson(jsonArray)
    }

    @TypeConverter
    fun getJsonArray(jsonArray: String?): JsonArray? {
        return gson.fromJson(jsonArray, JsonArray::class.java)
    }

    @TypeConverter
    fun saveBmi(bmi: Bmi?): String? {
        return gson.toJson(bmi)
    }

    @TypeConverter
    fun getBmi(bmi: String?): Bmi? {
        return gson.fromJson(bmi, Bmi::class.java)
    }

    @TypeConverter
    fun savePayments(payments: List<PaymentDTO>?): String? {
        return gson.toJson(payments)
    }

    @TypeConverter
    fun getPayments(payments: String?): List<PaymentDTO>? {
        return gson.fromJson(
            payments,
            object : TypeToken<List<PaymentDTO>?>() {}.type
        )
    }


    /*@TypeConverter
    fun saveItemQuantity(itemData: ItemQuantityDTO): String? {
        return gson.toJson(itemData)
    }

    @TypeConverter
    fun getItemQuantity(valueString: String?) : ItemQuantityDTO? {
        return gson.fromJson(
            valueString,
            ItemQuantityDTO::class.java
        )
    }*/
}
