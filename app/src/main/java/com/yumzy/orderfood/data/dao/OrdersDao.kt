package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.OrderStatusDTO
import com.yumzy.orderfood.data.models.Order
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO

@Dao
interface OrdersDao {

    @Query("SELECT * FROM orders ")
    fun getOrders(): LiveData<List<NewOrderDetailsDTO>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<NewOrderDetailsDTO>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(orderStatus: NewOrderDetailsDTO)


    @Query("SELECT * FROM orders WHERE orderId = :id")
    fun getOrder(id: String): LiveData<NewOrderDetailsDTO>

    @Delete
    suspend  fun  delete(order: NewOrderDetailsDTO)

    @Query("DELETE FROM orders")
    suspend fun deleteAll()

}