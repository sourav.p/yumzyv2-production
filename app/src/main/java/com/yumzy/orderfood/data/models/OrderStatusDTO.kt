/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.data.models

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrderStatusDTO(

    var orderId: String = "",

    @SerializedName("orderStatus")
    @Expose
    var orderStatus: String = "",

    @SerializedName("deliveryStatus")
    @Expose
    var deliveryStatus: String = "",

    @SerializedName("text")
    @Expose
    var text: String = "",

    @SerializedName("description")
    @Expose
    var description: String = "",

    @SerializedName("override")
    @Expose
    var override: Boolean = false,

    @SerializedName("userLocation")
    @Expose
    var userLocation: Coordinate? = null,

    @SerializedName("outletLocation")
    @Expose
    var outletLocation: Coordinate? = null,

    @SerializedName("delivery")
    @Expose
    var delivery: Delivery? = null

) : Serializable


data class Coordinate(

    @SerializedName("lat")
    @Expose
    var lat: Double = 0.0,

    @SerializedName("lng")
    @Expose
    var lng: Double = 0.0

) : Serializable

data class Delivery(

    @SerializedName("state")
    @Expose
    var state: String = "",

    @SerializedName("eta")
    @Expose
    var eta: Eta? = null,

    @SerializedName("runner")
    @Expose
    var riderInfo: RiderInfo? = null

) : Serializable


data class Eta(

    @SerializedName("pickup")
    @Expose
    var pickup: Float,

    @SerializedName("dropoff")
    @Expose
    var dropOff: Double

) : Serializable

data class RiderInfo(

    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("phone_number")
    @Expose
    var phone: String = "",

    @SerializedName("location")
    @Expose
    var location: Coordinate? = null,

    @SerializedName("deliveryPartner")
    @Expose
    var deliveryPartner: String = ""

) : Serializable