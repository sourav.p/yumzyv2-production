package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.BuildConfig
import java.io.Serializable


data class NewOrderReqDTO
    (
    @field:SerializedName("address") val address: AddressCheckOut,
    @field:SerializedName("specialInstructions") val specialInstructions: String,
    @field:SerializedName("cartId") val cartId: String,
    @field:SerializedName("postBackURL") val postBackUrl: String = BuildConfig.CALL_BACK_URL + "payment"
)

data class AddressCheckOut(
   @field:SerializedName("name")  val name: String,
   @field:SerializedName("landmark")  val landmark: String,
   @field:SerializedName("addressId")  val addressId: String,
   @field:SerializedName("addressTag")  val addressTag: String,
   @field:SerializedName("googleLocation")  val googleLocation: String,
   @field:SerializedName("houseNum")  val houseNum: String,
   @field:SerializedName("fullAddress")  val fullAddress: String,
   @field:SerializedName("city")  val city: String,
   @field:SerializedName("state")  val state: String,
   @field:SerializedName("pincode")  val pincode: String,
   @field:SerializedName("country")  val country: String,
   @field:SerializedName("longLat")  val longLat: LongLat
)

data class LongLat(

    @SerializedName("coordinates")
    @Expose
    var coordinates: List<Double>? = null
) : Serializable
