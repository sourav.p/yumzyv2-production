package com.yumzy.orderfood.data.ui

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterUserDTO
    (
    @field:SerializedName("firstName") val firstName: String,
    @field:SerializedName("lastName") val lastNAme: String,
    @field:SerializedName("email") val email: String,
    @field:SerializedName("mobileNumber") val mobileNumber: String,
    @field:SerializedName("navfrom") var navFrom: String
) : Parcelable
