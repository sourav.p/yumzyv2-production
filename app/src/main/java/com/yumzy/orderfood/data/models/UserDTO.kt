package com.yumzy.orderfood.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//typealias addressList = ArrayList<AddressDTO>

@Entity
data class UserDTO(
    @PrimaryKey
    @field:SerializedName("userId") val userId: String,
    @field:SerializedName("mobile") val mobile: String,
    @field:SerializedName("email") val email: String,
    @field:SerializedName("picture") val picture: String?,
    @field:SerializedName("currentLocality") val currentLocality: String,
    @field:SerializedName("city") val city: String,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("lastName") val lastName: String,
    @field:SerializedName("profileCompleted") val profileCompleted: Double,
    @field:SerializedName("referralDeepLink") val referralDeepLink: String,
    @field:SerializedName("referralCode") val refferalCode: String,
    @field:SerializedName("address") val address: List<AddressDTO?>?,
    @field:SerializedName("currentLocation") val currentLocation: CurrentLocationDTO? = null,
    @field:SerializedName("cartId") val cartId: String,
    @field:SerializedName("referredBy") val referredBy: String? = "",
    @field:SerializedName("isTemporary") val isTemporary: Boolean = false

)