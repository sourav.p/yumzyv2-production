package com.yumzy.orderfood.data

import android.content.SharedPreferences
import android.location.Location
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.RestaurantAddOnDTO
import com.yumzy.orderfood.util.IConstants
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton

class SharedPrefsHelper  constructor(private val mSharedPreferences: SharedPreferences)
/*:SharedPreferences.OnSharedPreferenceChangeListener */ {
//    init {
//        mSharedPreferences.registerOnSharedPreferenceChangeListener(this)
//    }

    fun put(key: String?, value: String?) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun put(key: String?, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    fun put(key: String?, value: Float) {
        mSharedPreferences.edit().putFloat(key, value).apply()
    }

    fun put(key: String?, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    operator fun get(key: String?, defaultValue: String?): String? {
        return mSharedPreferences.getString(key, defaultValue)
    }

    operator fun get(key: String?, defaultValue: Int): Int {
        return mSharedPreferences.getInt(key, defaultValue)
    }

    operator fun get(key: String?, defaultValue: Float): Float {
        return mSharedPreferences.getFloat(key, defaultValue)
    }

    operator fun get(key: String?, defaultValue: Boolean): Boolean {
        return mSharedPreferences.getBoolean(key, defaultValue)
    }

    fun deleteSavedData(key: String?) {
        mSharedPreferences.edit().remove(key).apply()
    }

    fun clearAllData() {
        mSharedPreferences.edit().clear().apply()
    }

    fun saveUserId(userId: String) {
        put(IConstants.SHARED_PREF.USER_ID, userId)
    }

    fun putSelectedAddress(check: String) {
        put(IConstants.SHARED_PREF.IS_ADDRESS_SELECTED, check)
    }

    fun geSelectedAddress(): String {
        return get(IConstants.SHARED_PREF.IS_ADDRESS_SELECTED, "") ?: ""
    }

    fun getUserId(): String? {
        return get(IConstants.SHARED_PREF.USER_ID, "")
    }

    fun removeUserId() {
        deleteSavedData(IConstants.SHARED_PREF.USER_ID)
    }

    fun saveDeviceId(deviceId: String) {
        put(IConstants.SHARED_PREF.DEVICE_ID, deviceId)
    }

    fun removeDeviceId() {
        deleteSavedData(IConstants.SHARED_PREF.DEVICE_ID)
    }

    fun updateLatitudeLongitude(latitude: String, longitude: String) {
        put(IConstants.SHARED_PREF.DEVICE_LAT, latitude)
        put(IConstants.SHARED_PREF.DEVICE_LONG, longitude)
    }

    fun getLatitudeLongitude(): Array<String> {
        val latitude = get(IConstants.SHARED_PREF.DEVICE_LAT, "") ?: ""
        val longitude = get(IConstants.SHARED_PREF.DEVICE_LONG, "") ?: ""
        return arrayOf(latitude, longitude)
    }

    fun getLongLatitudeLongitude(): ArrayList<Double> {
        val latitude = get(IConstants.SHARED_PREF.DEVICE_LAT, "0.0") ?: "0.0"
        val longitude = get(IConstants.SHARED_PREF.DEVICE_LONG, "0.0") ?: "0.0"

        return ArrayList<Double>().apply {
            add(latitude.toDouble())
            add(longitude.toDouble())
        }
    }

    fun getLongLat(): ArrayList<Double> {
        val longitude = get(IConstants.SHARED_PREF.DEVICE_LONG, "0.0") ?: "0.0"
        val latitude = get(IConstants.SHARED_PREF.DEVICE_LAT, "0.0") ?: "0.0"

        return ArrayList<Double>().apply {
            add(longitude.toDouble())
            add(latitude.toDouble())

        }
    }

    fun savedCityLocation(locality: String?) {
        put(IConstants.SHARED_PREF.LOCALITY, locality)
    }

    fun getCityLocation(): String {
        return get(IConstants.SHARED_PREF.LOCALITY, "") ?: ""
    }

    fun saveFullAddress(fullAddress: String) {
        put(IConstants.SHARED_PREF.FULL_ADDRESS, fullAddress)

    }

    fun getFullAddress(): String {
        return get(IConstants.SHARED_PREF.FULL_ADDRESS, "") ?: ""
    }

    fun saveCartId(cartId: String?) {
        put(IConstants.SHARED_PREF.CART_ID, cartId)
    }

    fun getCartId(): String? {
        return get(IConstants.SHARED_PREF.CART_ID, "")
    }

    fun getUserName(): String {
        return get(IConstants.SHARED_PREF.USER_NAME, "") ?: ""
    }

    fun saveUserName(name: String) {
        put(IConstants.SHARED_PREF.USER_NAME, name)
    }

    fun getUserLastName(): String {
        return get(IConstants.SHARED_PREF.USER_LAST_NAME, "") ?: ""
    }

    fun saveUserLastName(name: String) {
        put(IConstants.SHARED_PREF.USER_LAST_NAME, name)
    }

    fun getUserPic(): String? {
        return get(IConstants.SHARED_PREF.USER_PIC, "")
    }

    fun saveUserPic(pic: String) {
        put(IConstants.SHARED_PREF.USER_PIC, pic)
    }

    fun getUserEmail(): String? {
        return get(IConstants.SHARED_PREF.USER_EMAIL, "")
    }

    fun saveUserEmail(email: String) {
        put(IConstants.SHARED_PREF.USER_EMAIL, email)
    }

    fun getUserPhone(): String {
        return get(IConstants.SHARED_PREF.USER_PHONE, "") ?: ""
    }

    fun saveUserPhone(phone: String) {
        put(IConstants.SHARED_PREF.USER_PHONE, phone)
    }

    fun getCurrentOrderId(): String? {
        return get(IConstants.SHARED_PREF.CURRENT_ORDER_ID, "")
    }

    fun saveCurrentOrderId(orderId: String) {
        put(IConstants.SHARED_PREF.CURRENT_ORDER_ID, orderId)
    }

    fun saveCartOutletAddon(
        addonsMap: Map<String, RestaurantAddOnDTO>?,
        cartItemId: String?,
        addons: List<String>
    ) {
        if (!addons.isNullOrEmpty())
            put("cartItem_addons_map_$cartItemId", appGson.toJson(addonsMap))
        if (!addons.isNullOrEmpty())
            put("cartItem_addons_list_$cartItemId", appGson.toJson(addons))
    }

    fun getCartOutletAddon(cartItemId: String): Pair<HashMap<String, RestaurantAddOnDTO>, ArrayList<String>>? {
        val addonsMapString = get("cartItem_addons_map_$cartItemId", "")
        val itemAddonsList = get("cartItem_addons_list_$cartItemId", "")
        if (!addonsMapString.isNullOrEmpty() && !itemAddonsList.isNullOrEmpty()) {
            val gson = appGson
            val hashMap = HashMap<String, RestaurantAddOnDTO>()
            val fromJson = gson.fromJson(
                addonsMapString,
                HashMap<String, LinkedTreeMap<String, String>>().javaClass
            )
            fromJson.iterator().forEach { mapItem ->
                hashMap[mapItem.key] =
                    gson.fromJson(gson.toJson(mapItem.value), RestaurantAddOnDTO::class.java)
            }

            object : TypeToken<java.util.HashMap<String?, RestaurantAddOnDTO?>?>() {}.type
//            val addonMap = appGson.fromJson(addonsMapString, type)
            val addonsKeyList = gson.fromJson(itemAddonsList, ArrayList<String>().javaClass)
            return Pair(hashMap, addonsKeyList)
        } else {
            return null
        }
    }

    fun clearCartItemAddon() {
        val keys: Map<String, *> = mSharedPreferences.all

        for ((key, value) in keys) {
            if (key.contains("cartItem_")) {
                remoteValueFromSharePref(key)
            }
        }
    }

    private fun remoteValueFromSharePref(key: String) {
        mSharedPreferences.edit().remove(key).apply()
    }

    fun setOutletID(outletID: String) {
        put(IConstants.SHARED_PREF.OUTLET_ID, outletID)
    }

    fun getOutletID(): String {
        return get(IConstants.SHARED_PREF.OUTLET_ID, "") ?: ""
    }

    fun saveUserFcmToken(token: String) {
        put(IConstants.SHARED_PREF.USER_FCM_TOKEN, token)
    }

    fun getUserFcmToken(): String? {
        return get(IConstants.SHARED_PREF.USER_FCM_TOKEN, "")
    }

    fun getUserReferralCode(): String {
        return get(IConstants.SHARED_PREF.USER_REFERRAL_CODE, "") ?: ""
    }

    fun saveUserReferralCode(referral: String) {
        put(IConstants.SHARED_PREF.USER_REFERRAL_CODE, referral)
    }

    fun getUserReferralLink(): String? {
        return get(IConstants.SHARED_PREF.USER_REFERRAL_LINK, "")
    }

    fun saveUserReferralLink(referral: String) {
        put(IConstants.SHARED_PREF.USER_REFERRAL_LINK, referral)
    }

    fun getReferredBy(): String? {
        return get(IConstants.SHARED_PREF.REFERRED_BY, "")
    }

    fun saveReferredBy(referredBy: String) {
        put(IConstants.SHARED_PREF.REFERRED_BY, referredBy)
    }

    fun getIsNewUser(): Boolean {
        return get(IConstants.SHARED_PREF.IS_NEW_USER, false)
    }

    fun saveIsNewUser(newUser: Boolean) {
        put(IConstants.SHARED_PREF.IS_NEW_USER, newUser)
    }

    fun getFreshUser(): Boolean {
        return get(IConstants.SHARED_PREF.IS_FRESH_USER, true)
    }

    fun saveFreshUser(newUser: Boolean) {
        put(IConstants.SHARED_PREF.IS_FRESH_USER, newUser)
    }

    fun saveLastLocation(location: Location) {
        put(
            IConstants.SHARED_PREF.USER_LAST_LOCATION,
            "{\"lat\":${location.latitude},\"lng\":${location.longitude}}"
        )
    }

    fun getLastLocation(): Location? {
        val jsonString = get(
            IConstants.SHARED_PREF.USER_LAST_LOCATION,
            ""
        )

        val jsonObject = JSONObject(jsonString ?: "")

        if (!jsonObject.has("lat") || !jsonObject.has("lng")) return null

        return Location("User last location").apply {
            latitude = jsonObject.getDouble("lat")
            longitude = jsonObject.getDouble("lng")
        }

    }

    fun setSelectedAddress(addressDTO: AddressDTO?) {
        try {
            put(
                IConstants.SHARED_PREF.USER_SELECTED_ADDRESS,
                appGson.toJson(addressDTO, AddressDTO::class.java)
            )
        } catch (e: Exception) {
            put(
                IConstants.SHARED_PREF.USER_SELECTED_ADDRESS,
                null
            )
        }
    }

    fun getSelectedAddress(): AddressDTO? {
        val jsonData = get(IConstants.SHARED_PREF.USER_SELECTED_ADDRESS, null)
        val address = appGson.fromJson(jsonData, AddressDTO::class.java)
        return address
    }

    fun saveAddressTag(addressTag: String) {
        put(IConstants.SHARED_PREF.ADDRESS_TAG, addressTag)
    }

    fun getAddressTag(): String {
        return get(
            IConstants.SHARED_PREF.ADDRESS_TAG,
            get(IConstants.SHARED_PREF.LOCALITY, "")
        ) ?: ""
    }


    fun getIsTemporary(): Boolean {
        return get(IConstants.SHARED_PREF.IS_TEMP, true)
    }

    fun saveIsTemporary(isTemp: Boolean) {
        put(IConstants.SHARED_PREF.IS_TEMP, isTemp)
    }

//    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
//        TODO("Not yet implemented") SHIFT LOGIC HERE
//    }

}