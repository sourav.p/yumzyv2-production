package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Bhupendra Kumar Sahu.
 */

data class FoodPreferenceDTO(
    @field:SerializedName("imageurl") val imageurl: String,
    @field:SerializedName("displayName") val displayName: String,
    @field:SerializedName("prefId") val prefId: String,
    @field:SerializedName("isSelected") val selected: Boolean
)

data class FoodPrefSectionDTO(
    @field:SerializedName("menuSectionTitile") val menuSectionTitile: String,
    @field:SerializedName("sectionIdentifier") val sectionIdentifier: String,
    @field:SerializedName("items") val items: List<FoodPreferenceDTO>,
    @field:SerializedName("isMultiSelect") val isMultiSelect: Boolean = false,
    @field:SerializedName("isSelected") val selected: Boolean = false

)

