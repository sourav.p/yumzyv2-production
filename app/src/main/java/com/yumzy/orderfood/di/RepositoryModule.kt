package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.ui.screens.cart.CartRepository
import com.yumzy.orderfood.ui.screens.earncoupon.data.EarnCouponRepository
import com.yumzy.orderfood.ui.screens.home.data.AccountRepository
import com.yumzy.orderfood.ui.screens.home.data.ExploreRepository
import com.yumzy.orderfood.ui.screens.home.data.HomePageRepository
import com.yumzy.orderfood.ui.screens.home.data.HomeRepository
import com.yumzy.orderfood.ui.screens.home.fragment.PlateRepository
import com.yumzy.orderfood.ui.screens.invite.data.InviteRepository
import com.yumzy.orderfood.ui.screens.location.data.LocationRepository
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.ui.screens.module.dishes.data.TopDishesRepository
import com.yumzy.orderfood.ui.screens.module.favourites.data.FavouriteRestaurantRepository
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data.FoodPersonalisationRepository
import com.yumzy.orderfood.ui.screens.module.foodpreference.data.FoodPreferenceRepository
import com.yumzy.orderfood.ui.screens.module.review.data.RateOrderRepository
import com.yumzy.orderfood.ui.screens.module.toprestaurant.data.TopRestaurantsRepository
import com.yumzy.orderfood.ui.screens.offer.data.OfferRepository
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import com.yumzy.orderfood.ui.screens.payment.PaymentRepository
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRepository
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRepository
import com.yumzy.orderfood.ui.screens.scratchcard.data.ScratchRepository
import com.yumzy.orderfood.ui.screens.wallet.data.MyWalletRepository
import org.koin.dsl.module

fun repositoryModule() = module {

    factory { LoginRepository(get(), get(), get(), get()) }

    factory { HomePageRepository(get(), get(), get(), get()) }
    factory { HomeRepository(get(), get(), get()) }

    factory { ProfileRepository(get(), get()) }

    factory { RestaurantRepository(get(), get(), get(), get()) }

    factory { RewardsRepository(get(), get()) }

    factory { MyWalletRepository(get()) }

    factory { OrderRepository(get(), get()) }

    factory { OfferRepository(get()) }

    factory { EarnCouponRepository(get()) }

    factory { PaymentRepository(get(), get()) }

    factory { ScratchRepository(get()) }

    factory { LocationRepository(get(), get(), get()) }

    factory { InviteRepository(get()) }

    factory { FavouriteRestaurantRepository(get()) }

    factory { FoodPreferenceRepository(get()) }

    factory { FoodPersonalisationRepository(get()) }

    factory { TopDishesRepository(get()) }

    factory { TopRestaurantsRepository(get()) }

    factory { RateOrderRepository(get()) }
    factory { ExploreRepository(get(), get()) }
    factory { AccountRepository(get(), get()) }
    factory { PlateRepository(get(), get(), get()) }
    factory { CartRepository(get(), get(), get()) }


}