package com.example.di.module

import androidx.room.FtsOptions
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.api.*
import com.yumzy.orderfood.api.session.SessionManager
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.util.IConstants
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

fun remoteModule() = module {

    single { SessionManager(get()) }

    single { File(androidContext().cacheDir, UUID.randomUUID().toString()) }

    single { Cache(get(), 20 * 1024 * 1024) }

    single { appGson }

    single {
        OkHttpClient.Builder()
            .cache(get())
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .followSslRedirects(true)
            .cache(get())
            .addInterceptor(AuthInterceptor(get()))
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE })
            .addNetworkInterceptor(CacheInterceptor())
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(IConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
    }

    factory { get<Retrofit>().create(AddressServiceAPI::class.java) }

    factory { get<Retrofit>().create(CartServiceAPI::class.java) }

    factory { get<Retrofit>().create(ExploreService::class.java) }

    factory { get<Retrofit>().create(HomeServiceAPI::class.java) }

    factory { get<Retrofit>().create(LoginServiceAPI::class.java) }

    factory { get<Retrofit>().create(FtsOptions.Order::class.java) }

    factory { get<Retrofit>().create(ProfileServiceAPI::class.java) }

    factory { get<Retrofit>().create(RewardServiceAPI::class.java) }

    factory { get<Retrofit>().create(ScratchCardAPI::class.java) }

    factory { get<Retrofit>().create(OrderServiceAPI::class.java) }


}