package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.ui.screens.earncoupon.EarnCouponViewModel
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpHeadingViewModel
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpSubHeadingViewModel
import com.yumzy.orderfood.ui.screens.home.HomePageViewModel
import com.yumzy.orderfood.ui.screens.home.fragment.AccountViewModel
import com.yumzy.orderfood.ui.screens.home.fragment.ExploreViewModel
import com.yumzy.orderfood.ui.screens.home.fragment.HomeViewModel
import com.yumzy.orderfood.ui.screens.home.fragment.PlateViewModel
import com.yumzy.orderfood.ui.screens.invite.InviteViewModel
import com.yumzy.orderfood.ui.screens.location.LocationViewModel
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.ui.screens.login.SplashViewModel
import com.yumzy.orderfood.ui.screens.login.fragment.FetchLocationAnimViewModel
import com.yumzy.orderfood.ui.screens.manageaddress.ManageAddressViewModel
import com.yumzy.orderfood.ui.screens.module.CouponViewModel
import com.yumzy.orderfood.ui.screens.module.OTPVerifyViewModel
import com.yumzy.orderfood.ui.screens.module.address.ConfirmLocationViewModel
import com.yumzy.orderfood.ui.screens.module.address.OrderTrackingDialogViewModel
import com.yumzy.orderfood.ui.screens.module.address.SelectDeliveryLocationViewModel
import com.yumzy.orderfood.ui.screens.module.dishes.TopDishesViewModel
import com.yumzy.orderfood.ui.screens.module.favourites.FavouritesViewModel
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.FoodPersonalisationViewModel
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceViewModel
import com.yumzy.orderfood.ui.screens.module.review.RateOrderViewModel
import com.yumzy.orderfood.ui.screens.module.toprestaurant.TopRestaurantViewModel
import com.yumzy.orderfood.ui.screens.offer.OfferViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.OrderHistoryViewModel
import com.yumzy.orderfood.ui.screens.payment.PaymentViewModel
import com.yumzy.orderfood.ui.screens.profile.ProfileViewModel
import com.yumzy.orderfood.ui.screens.reorder.ReorderViewModel
import com.yumzy.orderfood.ui.screens.restaurant.RestaurantViewModel
import com.yumzy.orderfood.ui.screens.rewards.RewardsViewModel
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardViewModel
import com.yumzy.orderfood.ui.screens.wallet.WalletViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun viewModelModule() = module {

    /*
    * Activity ViewModels
    * */

    viewModel { LoginViewModel(get()) }

    viewModel { HomeViewModel(get()) }

    viewModel { HomePageViewModel(get()) }

    viewModel { ManageAddressViewModel(get()) }

    viewModel { RestaurantViewModel(get(), get(), get()) }

    viewModel { RewardsViewModel(get()) }

    viewModel { WalletViewModel(get(), get()) }

    viewModel { OrderHistoryViewModel(get()) }

    viewModel { OfferViewModel(get()) }

    viewModel { EarnCouponViewModel(get()) }

    viewModel { HelpSubHeadingViewModel(get()) }

    viewModel { HelpHeadingViewModel(get()) }

    viewModel { ReorderViewModel(get()) }

    viewModel { PaymentViewModel(get(), get()) }

    viewModel { ScratchCardViewModel(get(), get()) }

    viewModel { LocationViewModel(get()) }

    viewModel { InviteViewModel(get()) }


    /*
    * Fragment ViewModels
    * */

    viewModel { FetchLocationAnimViewModel(get()) }

    viewModel { ProfileViewModel(get()) }

    viewModel { SplashViewModel() }
    viewModel { ExploreViewModel(get()) }
    viewModel { PlateViewModel(get()) }
    viewModel { AccountViewModel(get()) }


    /*
   * Dialog ViewModels
   * */
    viewModel { OTPVerifyViewModel(get()) }

    viewModel { CouponViewModel(get()) }

    viewModel { FavouritesViewModel(get()) }

    viewModel { FoodPreferenceViewModel(get()) }

    viewModel { SelectDeliveryLocationViewModel(get()) }

    viewModel { ConfirmLocationViewModel(get()) }

    viewModel { FoodPersonalisationViewModel(get()) }

    viewModel { OrderTrackingDialogViewModel(get()) }

    viewModel { TopDishesViewModel(get(), get()) }

    viewModel { TopRestaurantViewModel(get()) }

    viewModel { RateOrderViewModel(get()) }





}