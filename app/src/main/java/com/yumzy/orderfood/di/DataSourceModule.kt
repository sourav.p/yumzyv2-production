package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.ui.screens.cart.CartRemoteDataSource
import com.yumzy.orderfood.ui.screens.earncoupon.data.EarnCouponRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.fragment.PlateRemoteDataSource
import com.yumzy.orderfood.ui.screens.invite.data.InviteRemoteDataSource
import com.yumzy.orderfood.ui.screens.location.data.LocationRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.data.LoginRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.dishes.data.TopDishesRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.favourites.data.FavourriteRestaurantRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data.FoodPersonalisationRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.foodpreference.data.FoodPreferenceRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.review.data.RateOrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.toprestaurant.data.TopRestaurantsRemoteDataSource
import com.yumzy.orderfood.ui.screens.offer.data.OfferRemoteDataSource
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.payment.PaymentRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRemoteDataSource
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRemoteDataSource
import com.yumzy.orderfood.ui.screens.scratchcard.data.ScratchDataSource
import com.yumzy.orderfood.ui.screens.wallet.data.MyWalletRemoteDataSource
import org.koin.dsl.module

fun dataSourceModule() = module {

    factory { LoginRemoteDataSource(get()) }

    factory { ProfileRemoteDataSource(get(), get()) }

    factory { HomeRemoteDataSource(get()) }

    factory { RestaurantRemoteDataSource(get()) }

    factory { RewardsRemoteDataSource(get()) }

    factory { MyWalletRemoteDataSource() }

    factory { OrderRemoteDataSource(get()) }

    factory { OfferRemoteDataSource(get()) }

    factory { EarnCouponRemoteDataSource(get()) }

    factory { PaymentRemoteDataSource(get()) }

    factory { ScratchDataSource(get()) }

    factory { LocationRemoteDataSource(get()) }

    factory { InviteRemoteDataSource(get()) }

    factory { FavourriteRestaurantRemoteDataSource(get()) }

    factory { FoodPreferenceRemoteDataSource(get()) }

    factory { FoodPersonalisationRemoteDataSource(get()) }

    factory { TopDishesRemoteDataSource(get()) }

    factory { TopRestaurantsRemoteDataSource(get()) }

    factory { RateOrderRemoteDataSource(get()) }
    factory { PlateRemoteDataSource(get()) }
    factory { CartRemoteDataSource(get()) }


}