package com.yumzy.orderfood.di.module

import com.example.di.module.remoteModule

fun appModule() = listOf(
    cartModule(),
    dataSourceModule(),
    localModule(),
    remoteModule(),
    repositoryModule(),
    viewModelModule()
)