package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.ui.common.cart.CartHelper
import org.koin.dsl.module

fun cartModule() = module {
    single {
        CartHelper(
            get(),
            get()
        )
    }
}