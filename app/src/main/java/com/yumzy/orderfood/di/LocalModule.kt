package com.yumzy.orderfood.di.module

import android.content.Context
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.util.IConstants
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

fun localModule() = module {

    single { androidApplication().getSharedPreferences(IConstants.SHARE_PREF_NAME, Context.MODE_PRIVATE) }

    single { SharedPrefsHelper(get()) }

    single { AppDatabase.getInstance(androidApplication()) }

    factory { get<AppDatabase>().addressDao() }

    factory { get<AppDatabase>().cartResDao() }

    factory { get<AppDatabase>().ordersDao() }

    factory { get<AppDatabase>().restaurantDao() }

    factory { get<AppDatabase>().restaurantItemDao() }

    factory { get<AppDatabase>().selectedItemDao() }

    factory { get<AppDatabase>().selectedAddressDao() }

    factory { get<AppDatabase>().userInfoDao() }

}