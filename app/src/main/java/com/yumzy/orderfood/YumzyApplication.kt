package com.yumzy.orderfood

import android.annotation.SuppressLint
import androidx.multidex.MultiDexApplication
import com.clevertap.android.sdk.ActivityLifecycleCallback
import com.clevertap.android.sdk.CleverTapAPI
import com.yumzy.orderfood.di.module.appModule
import com.yumzy.orderfood.util.IConstants
import com.zoho.salesiqembed.ZohoSalesIQ
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class YumzyApplication : MultiDexApplication() {

    @SuppressLint("MissingPermission")
    private fun sdkInitialization() {
        appCleverTap = CleverTapAPI.getDefaultInstance(this@YumzyApplication)!!
    }

    private fun configureDi() = startKoin {

        // use AndroidLogger as Koin Logger - default Level.INFO
        if (BuildConfig.DEBUG) androidLogger()

        androidContext(this@YumzyApplication)
        /*// load properties from assets/koin.properties file
        androidFileProperties()*/

        modules(appModule())
    }

    override fun onCreate() {
        ActivityLifecycleCallback.register(this)
        super.onCreate()
        sdkInitialization()
        appCleverTap.enableDeviceNetworkInfoReporting(true)
        ZohoSalesIQ.init(
            this, IConstants.AppConstant.ZOHO_APP_KEY,
            IConstants.AppConstant.ZOHO_ACCESS_KEY
        )
        ZohoSalesIQ.showLauncher(false)
        configureDi()
    }

}